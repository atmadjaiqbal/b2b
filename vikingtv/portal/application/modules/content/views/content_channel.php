<div class="container" style="margin-top: 140px;">
<?php
if($channel_detail)
{
    $player_wrap_width = "620"; $player_width = "620"; $player_height = "400";
    if($channel_detail)
    {
       $chan_channel_id = $channel_detail->channel_id;
       $chan_content_id = $channel_detail->channel_id;
       $chan_channel_name = $channel_detail->channel_name;
       $chan_alias = $channel_detail->alias;
       $chan_thumbnail = $channel_detail->thumbnail.'&w=300';
       $chan_description = $channel_detail->description;
       $chan_type_name = $channel_detail->type_name;
       $chan_status = $channel_detail->status;
       $chan_countviewer = $channel_detail->countviewer;
       $chan_order_number = $channel_detail->order_number;
       $chan_link = $channel_detail->link;
       $chan_created = $channel_detail->created;
       $chan_createdby = $channel_detail->createdby;
       $chan_apps_id = $channel_detail->apps_id;
       $chan_app_name = $channel_detail->app_name;

       if($chan_status == '0') redirect('/');
       //$player = 'http://id.ibolz.tv/assets/flash/global/index.php?w='.$player_width.'&h='.$player_height;
       $player = 'http://ply001.3d.ibolztv.net/player/flash/id/b2b.php?w='.$player_width.'&h='.$player_height;
       $player .= '&apps_id='.config_item('ibolz_app_id').'&version='.config_item('ibolz_version').'&type=channel';
       $player .= '&channel_id='.$chan_channel_id.'&vid='.$chan_content_id.'&id='.$chan_content_id.'&customer_id='.config_item('ibolz_customer_id');
?>

   <div class="bnichannel-detail">
      <h3 class="slider-title">
         <a href="#" style="cursor: default;"><span><?php echo $chan_channel_name;?></span> <i class="fa fa-angle-right"></i></a>
      </h3>
      <div class="span4" style="margin-left:0px;">
         <div class="row item-sec">
             <div class="poster-place">
               <div class="poster" style="background: url('<?php echo $chan_thumbnail;?>') no-repeat scroll center;width:300px;height:170px;"></div>
             </div>
             <div class="border-bott"></div>
         </div>
         <div class="row item-sec">
             <h3>SCHEDULE</h3>
             <table class="schedule">
<?php
if($schedule_today->schedule_list)
{
    foreach($schedule_today->schedule_list as $sche)
    {
        $sch_schedule_id = $sche->schedule_id;
        $sch_channel_id = $sche->channel_id;
        $sch_program_id = $sche->program_id;
        $sch_duration = $sche->duration;
        $sch_program_name = $sche->program_name;
        $sch_thumbnail = $sche->thumbnail;
        $sch_start_date_time = $sche->start_date_time;
        $sch_start_time_text = $sche->start_time_text;
        $sch_end_time_text = $sche->end_time_text;
        $sch_schedule_type_id = $sche->schedule_type_id;
        $sch_server_time = $sche->server_time;
        $sch_enable_record = $sche->enable_record;
        $sch_enable_remove = $sche->enable_remove;
        $sch_enable_play = $sche->enable_play;
?>
                <tr>
                   <td style="width:60px;" align="left"><?php echo $sch_end_time_text; ?></td>
                   <td style="width: 220px;"><?php echo $sch_program_name; ?></td>
                 </tr>
<?php
    }
}
?>
             </table>
         </div>
      </div>

      <div class="span8" style="margin-left:10px !important;">
          <div class="row item-sec">
             <iframe seamless wmode="Opaque" class="player bg-loader" scrolling="no"
                     style='width: <?php echo $player_width;?>px;min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;'
                     src="<?php echo $player;?>">
             </iframe>
             <div class="border-bott"></div>
          </div>

          <?php
          // if($chan_description)
          // {
          ?>
          <div class="row item-sec">
              <h3>DESCRIPTION</h3>
              <p><?php echo $chan_description; ?></p>
          </div>
          <?php
          // }
          ?>
      </div>


   </div>


<?php
    }
} else {
?>
    <div style="text-align: center;font-family: helvetica, arial, sans-serif; font-size: 28px;margin-top: 150px;margin-bottom: 200px;">
        Channel Not Available
    </div>
<?php
}
?>
</div>

