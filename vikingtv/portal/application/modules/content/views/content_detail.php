<div class="container" style="margin-top: 140px;">
<?php
if($product_detail)
{
    $player_wrap_width = "620"; $player_width = "620"; $player_height = "400";
    $crew_category_list = array(); $genre_list = array(); $photo_list = array(); $related_list = array();
    $rcode = $product_detail->rcode;
    $channel_id = $product_detail->channel_id;
    $content_id = $product_detail->content_id;
    $title = $product_detail->title;
    $video_duration = $product_detail->video_duration;
    $filename = $product_detail->filename;
    $thumbnail = $product_detail->thumbnail;
    $description = $product_detail->description;
    $prod_year = $product_detail->prod_year;
    $trailer_id = $product_detail->trailer_id;
    $price = $product_detail->price;
    $ulike = $product_detail->ulike;
    $unlike = $product_detail->unlike;
    $comment = $product_detail->comment;
    if($product_detail->crew_category_list) { $lecture = $product_detail->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}
    $genre_list = $product_detail->genre_list;
    $photo_list = $product_detail->photo_list;
    $related_list = $product_detail->related_list;
    $document_list = $product_detail->document_list;
    $player = 'http://id.ibolz.tv/assets/flash/global/index.php?w='.$player_width.'&h='.$player_height;
    $player .= '&apps_id='.config_item('ibolz_app_id').'&version='.config_item('ibolz_version').'&type=content';
    $player .= '&channel_id='.$channel_id.'&vid='.$content_id.'&id='.$content_id.'&customer_id='.$customer_id;
?>

    <div class="bnicontent-detail">
        <h3 class="slider-title">
            <a href="#" style="cursor: default;"><span><?php echo $title;?></span> <i class="fa fa-angle-right"></i></a>
        </h3>
        <div class="span4" style="margin-left:0px;">
            <div class="row item-sec">

               <div class="poster-place">
                  <div class="poster" style="background-color:#cecece;background: url('<?php echo $thumbnail;?>&w=300px') no-repeat scroll center;width:300px;height:450px;"></div>
               </div>

               <div class="social overlay">
                   <ul class="content-score">
                       <li>
                           <a class="score-btn" id='like-<?php echo $content_id; ?>' data-id='<?php echo $content_id; ?>'>
                               <img class="pull-left" src="<?php echo base_url('assets/images/like_icon_large_new.png'); ?>">
                           </a>&nbsp;&nbsp;<div id="totallike-<?php echo $content_id; ?>" data-id='<?php echo $content_id; ?>'><?php echo $ulike; ?></div>
                       </li>
                       <li>
                           <a class="score-btn" data-tipe='comment' data-id='<?php echo $content_id; ?>'>
                               <img class="pull-left" src="<?php echo base_url('assets/images/comment_icon_large_new.png'); ?>">
                           </a>&nbsp;&nbsp;<?php echo $comment; ?>
                       </li>
                   </ul>
               </div>


               <div class="border-bott"></div>
            </div>
            <div class="row">
                <h3>PHOTO</h3>
                <div class="photolist">
                <?php
                if($photo_list)
                {
                   $y = 1; $photo_filename = array();
                   foreach($photo_list as $value)
                   {
                       $photonya = $value->thumbnail.'&w=200';
                       $photofull = $value->thumbnail.'&w=300';
                       $photo_content_id[$y] = $value->content_id;

                       if(isset($value->thumbnail))
                       {
                           $photo_filename[$y] = '
                           <a href="'.$photofull.'" title="'.$title.'">
                              <div style="background: url('.$photonya.') no-repeat;background-size: 100% 100%;"></div>
                           </a>
                           ';
                       } else {
                           $photo_filename[$y] = '';
                       }

                       //$photo_filename[$y] = isset($value->thumbnail) ? 'style="background: url('.$photonya.') no-repeat;background-size: 100% 100%;"' : '';
                       $photo_thumbnail[$y] = $value->filename;
                       $y++;
                   }
                }
                ?>
                    <ul class="popup-gallery">
                        <li><div><?php echo isset($photo_filename[1]) ? $photo_filename[1] : '';?></div></li>
                        <li><div><?php echo isset($photo_filename[2]) ? $photo_filename[2] : ''?></div></li>
                        <li><div><?php echo isset($photo_filename[3]) ? $photo_filename[3] : ''?></div></li>
                        <li><div><?php echo isset($photo_filename[4]) ? $photo_filename[4] : ''?></div></li>
                        <li><div><?php echo isset($photo_filename[5]) ? $photo_filename[5] : ''?></div></li>
                        <li><div><?php echo isset($photo_filename[6]) ? $photo_filename[6] : ''?></div></li>
                        <li><div><?php echo isset($photo_filename[7]) ? $photo_filename[7] : ''?></div></li>
                        <li><div><?php echo isset($photo_filename[8]) ? $photo_filename[8] : ''?></div></li>
                        <li><div><?php echo isset($photo_filename[9]) ? $photo_filename[9] : ''?></div></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <h3>DOCUMENT</h3>
                <?php
                foreach($document_list as $docRow)
                {
                    $doc_content_id = $docRow->content_id;
                    $document_id = $docRow->document_id;
                    $doc_title = $docRow->title;
                    $doc_filename = $docRow->filename;
                    $doc_type = $docRow->type;
                    $doc_size = $docRow->size;
                ?>
                    <div class="doc-download">
                        <div class="img-apps">
                <?php

                $imgdoc = "Unknown";
                $word_mime = array('application/msword', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.ms-word.document.macroEnabled.12', 'application/vnd.ms-word.template.macroEnabled.12');
                $excel_mime = array('application/vnd.ms-excel','application/vnd.ms-excel', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.ms-excel.sheet.macroEnabled.12', 'application/vnd.ms-excel.template.macroEnabled.12',
                    'application/vnd.ms-excel.addin.macroEnabled.12', 'application/vnd.ms-excel.sheet.binary.macroEnabled.12');
                $power_mime = array('application/vnd.ms-powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    'application/vnd.openxmlformats-officedocument.presentationml.template', 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
                    'application/vnd.ms-powerpoint.addin.macroEnabled.12', 'application/vnd.ms-powerpoint.presentation.macroEnabled.12', 'application/vnd.ms-powerpoint.template.macroEnabled.12',
                    'application/vnd.ms-powerpoint.slideshow.macroEnabled.12');

                if($doc_type == 'application/pdf'){$imgdoc = base_url().'assets/images/onl-icon-pdf.png';}
                if(in_array($doc_type, $word_mime)) {$imgdoc = base_url().'assets/images/onl-icon-doc.png';}
                if(in_array($doc_type, $excel_mime)) {$imgdoc = base_url().'assets/images/onl-icon-excel.png';}
                if(in_array($doc_type, $power_mime)) {$imgdoc = base_url().'assets/images/onl-icon-ppt.png';}

                ?>
                            <img src="<?php echo $imgdoc;?>">
                        </div>
                        <div class="doc-title">
                            <h5><?php echo $doc_filename;?></h5>
                            <p><?php echo $doc_title;?></p>
                        </div>
                        <!-- div class="img-download" id="getdocument-<?php echo $document_id;?>" data-document-id="<?php echo $document_id;?>" data-filename="<?php echo $doc_filename;?>"
                             data-document-type="<?php echo $doc_type;?>" data-document-size="<?php echo $doc_size;?>" -->
                        <div class="img-download">
                            <a href="<?php echo base_url().'content/document_download?document_id='.$document_id;?>">
                            <img src="<?php echo base_url().'assets/images/onl-icon-download.png';?>">
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>


        <div class="span8" style="margin-left:10px !important;">
            <div class="row item-sec">
                <iframe seamless wmode="Opaque" class="player bg-loader" scrolling="no"
                        style='width: <?php echo $player_width;?>px;min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;'
                        src="<?php echo $player;?>">
                </iframe>
            </div>

            <div class="row item-sec">
                <h3>SESSIONS</h3>
                <div class="sessions" style="margin-bottom: 20px;">
                    <ul>
                    <?php
                    $default_r_thumb = base_url().'assets/images/no_video.png'; $i = 1;
                    if($related_list)
                    {
                       foreach($related_list as $value)
                       {
                         if($i == 3) break;
                         $other_trailer = $value->thumbnail.'&w=200';
                         $r_content_id = $value->content_id;
                         $r_content_related_id = $value->content_related_id;
                         $r_title = $value->title;
                         $r_title = strlen($r_title) > 23 ? substr($r_title, 0, 23).'...' : $r_title;
                         $r_video_duration = $value->video_duration;
                         $r_thumbnail = isset($value->thumbnail) ? $other_trailer : $default_r_thumb;

                         // base_url().'content/content_detail/'.$sub_content_id.'/'.$sub_menu_id.
                    ?>
                         <li>
                             <a href="<?php echo base_url().'content/content_detail/'.$r_content_related_id.'/'.$menu_id;?>">
                                 <div class="product-title">
                                     <span class="pull-left"><?php echo $r_title;?></span>
                                     <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                 </div>
                                 <div class="product-image">
                                     <div class="preview_product related bgfull"
                                          style="background-color:#cecece;background: url('<?php echo $r_thumbnail;?>') no-repeat;width:auto;height:90px;">
                                     </div>
                                 </div>
                             </a>
                         </li>
                    <?php
                         $i++;
                       }
                    }

                    if($i <= 3 || $i <= 2)
                    {
                        for($y = 1; $y <= (3 - $i) + 1; $y++)
                        {
                            echo '<li><div><img src="'.$default_r_thumb.'"></div></li>';
                        }
                    }
                    ?>
                    </ul>
                </div>
                <div class="clear"></div>
                <div class="border-bott"></div>
            </div>

            <div class="row item-sec">
                <h3>DESCRIPTION</h3>
                <h4><?php echo $title; ?></h4>
                <div class="content-info">
                    <dl>
                        <dt>LECTURE</dt><dd>:&nbsp;&nbsp;<?php echo $lecture;?></dd>
                        <dt>DATE</dt><dd>:&nbsp;&nbsp;<?php echo $prod_year;?></dd>
                        <dt>TIME</dt><dd>:&nbsp;&nbsp;<?php // echo $prod_year;?></dd>
                        <dt>
                            <?php
                            $_message = "Hi, saya ingin share materi ini, materi yang menarik dan bagus untuk dipelajari. %0D%0A";
                            $_message .= "Link urlnya sebagai berikut ini : %0D%0A%0D%0A";
                            $_message .= current_url();
                            $_message .= "%0D%0A%0D%0ASelamat belajar.";
                            //$_message .= "Atau %3Ca%20href%3D%22".current_url()."%22%3Eklik link ini%3C%2Fa%3E untuk langsung ke materi.";
                            ?>
                            <a href="mailto:?subject=<?php echo $title;?>&amp;body=<?php echo $_message; ?>" title="Share By Email">
                              SHARE BY EMAIL
                            </a>
                        </dt><dd></dd>
                    </dl>
                    <h3>DESCRIPTION</h3>
                    <p><?php echo $description; ?></p>
                </div>
                <div class="border-bott"></div>
            </div>

            <div class="row item-sec">
                <h3>PLAYLIST</h3>
                <?php
            if($list_content)
            {
               $carousel_default = '<div class="product-title">
                                  <a href="">
                                    <span class="pull-left"></span>
                                    <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                  </a>
                              </div>
                              <div class="product-image" style="background-color: #dddddd;">
                              </div>';


               $carouselimg1 = $carousel_default;$carouselimg2 = $carousel_default;$carouselimg3 = $carousel_default;
               $carouselimg4 = $carousel_default;$carouselimg5 = $carousel_default;$carouselimg6 = $carousel_default;$i = 1;
               foreach($list_content->product_list as $rwcontent)
               {
                  $sub_menu_id = $rwcontent->menu_id;
                  $sub_apps_id = $rwcontent->apps_id;
                  $sub_product_id = $rwcontent->product_id;
                  $sub_channel_category_id = $rwcontent->channel_category_id;
                  $sub_channel_id = $rwcontent->channel_id;
                  $sub_channel_type_id = $rwcontent->channel_type_id;
                  $sub_content_id = $rwcontent->content_id;
                  $sub_content_type = $rwcontent->content_type;
                  $sub_title = $rwcontent->title;
                  $short_title = strlen($sub_title) > 14 ? substr($sub_title, 0, 14). '...' : $sub_title;
                  $sub_thumbnail = $rwcontent->thumbnail.'&w=200px';
                  $sub_description = $rwcontent->description;
                  $sub_prod_year = $rwcontent->prod_year;
                  $sub_video_duration = $rwcontent->video_duration;
                  $sub_countlike = $rwcontent->countlike;
                  $sub_countviewer = $rwcontent->countviewer;
                  $sub_icon_size = $rwcontent->icon_size;
                  $sub_poster_size = $rwcontent->poster_size;
                  $sub_crew_category_list = $rwcontent->crew_category_list;
                  $sub_genre_list = $rwcontent->genre_list;

                  if($product_id != $sub_product_id)
                  {
                      $carouselItem = '
                      <div class="product-title">
                           <span class="pull-left">'.$short_title.'</span>
                           <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                      </div>
                      <a href="'.base_url().'content/content_detail/'.$sub_content_id.'/'.$sub_menu_id.'">
                         <div class="product-image">
                            <div class="preview_product related bgfull"
                                 style="background-color:#cecece;background: url('.$sub_thumbnail.'&w=160) no-repeat scroll center;width:160px;height:240px;"
                                 id="'.$sub_menu_id.'-'.$sub_product_id.'"
                                 data-id="'.$sub_product_id.'" data-menu-id="'.$sub_menu_id.'" src="'.$sub_thumbnail.'"></div>
                         </div>
                      </a>';
                      if($i == 1) $carouselimg1 = $carouselItem; if($i == 2) $carouselimg2 = $carouselItem;
                      if($i == 3) $carouselimg3 = $carouselItem; if($i == 4) $carouselimg4 = $carouselItem;
                      if($i == 5) $carouselimg5 = $carouselItem; if($i == 6) $carouselimg6 = $carouselItem;
                      $i++;
                  }
               }
            ?>

                   <div class="carousel slide playlist-slider">
                       <a id="playlist_left" class="left carousel-control" href="#play-move" data-slide="next"></a>
                       <a id="playlist_right" class="right carousel-control" href="#play-move" data-slide="next"></a>
                       <div id="play-move" style="height: 270px !important;">
                           <div class="carousel-inner">
                               <div class="item active">
                                   <div class="content-item"><?php echo isset($carouselimg1) ? $carouselimg1 : $carousel_default; ?></div>
                                   <div class="content-item"><?php echo isset($carouselimg2) ? $carouselimg2 : $carousel_default; ?></div>
                                   <div class="content-item"><?php echo isset($carouselimg3) ? $carouselimg3 : $carousel_default; ?></div>
                               </div>
                               <div class="item">
                                   <div class="content-item"><?php echo isset($carouselimg4) ? $carouselimg4 : $carousel_default; ?></div>
                                   <div class="content-item"><?php echo isset($carouselimg5) ? $carouselimg5 : $carousel_default; ?></div>
                                   <div class="content-item"><?php echo isset($carouselimg6) ? $carouselimg6 : $carousel_default; ?></div>
                               </div>
                           </div>
                       </div>
                   </div>
            <?php
               }
            ?>
        <div class="border-bott"></div>
    </div>

    <div class="row item-sec">
        <h3>COMMENT</h3>

        <div><textarea id='commenttext' name="comment" class="media-input" rows="3"></textarea></div>
        <div class="row button-comment" style="margin-bottom: 40px;">
            <a id="sendcomment" data-id="<?php echo $content_id; ?>">SEND</a>
        </div>

        <div id="comment" data-page="1" data-id="<?php echo $content_id; ?>" class="comment-container"></div>

        <div class="clear row loadmore-contentplace">
            <div class="text-center">
                <div id="load_more_place" class="loadmore loadmore-bg">
                    <a data-page="1" data-id="<?php echo $content_id; ?>" id="comment_load_more" class="loadmore-text" href="#">LOAD MORE</a>
                </div>
            </div>
        </div>

    </div>

<script type="text/javascript">

  $(document).ready(function() {
      $('#like-<?php echo $content_id; ?>').click(function(e){
          e.preventDefault();
          var id = $(this).data('id');
          $.post(Settings.base_url+'content/post_like/', {'content_id':id}, function(data){
              if(data.rcode != 'ok'){
                  alert(data.msg);
              } else {
                  liketotal('#totallike-<?php echo $content_id; ?>');
              }
          });

      });

      $('#sendcomment').click(function(e){
          e.preventDefault();
          var product_id = $(this).data('id');
          var comment = $('#commenttext').val();
          $.post(Settings.base_url+'content/post_comment/', {'product_id':product_id, 'comment':comment}, function(data){
              if(data.rcode == 'ok'){
                  $('#comment').prepend(data.msg);
                  $('#commenttext').val('');
              }else{
                  alert(data.msg);
              }
          });

      });


  });

</script>

<?php
}
?>
    </div>
  </div>
</div>

