<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider1, slider2, slider3 .jpg

*/

$config['nameLogo'] = "";
$config['title'] = "IBOLZ | WONDERFUL TV";
$config['warnaHeader'] = "#8FBFEE";
$config['colorMenu'] = "#4083C3";
$config['warnaCopyright'] = "#8FBFEE";
$config['warnaNavCarousel'] = "#4083C3";
$config['colorListbox'] = "blue";
$config['warnaMenuMobile'] = "#8FBFEE";
$config['footerCopyright'] = "WONDERFUL TV";
$config['jmlSlider'] = 2;

/* --- About Us --- */

$config['aboutUs'] = "<p>-</p>";

/* --- Contact Us --- */

$config['detailOffice'] = "-";
$config['address'] = "-";
$config['tlp'] = "+62 21 521 2552";
$config['fax'] = "+62 21 521 2553";
$config['contactCenter'] = "";
$config['email'] = "info.id@greenpeace.org , supporterservices.id@greenpeace.org";
$config['website'] = "";

/* --- Download --- */

$config['namaFolder'] = "wonderful";
$config['namaAPK'] = "wonderfultv-2.0.5-1445434003";

?>

