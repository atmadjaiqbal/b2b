<?php 
$config["nameLogo"] = "HAI TV";
$config["title"] = "IBOLZ | HAI TV";
$config["warnaHeader"] = "#000000";
$config["colorMenu"] = "#c4c4c4";
$config["warnaCopyright"] = "#d60a0a";
$config["warnaNavCarousel"] = "#c3c3c3";
$config["colorListbox"] = "#eb420d";
$config["warnaMenuMobile"] = "#eb420d";
$config["footerCopyright"] = "HAI TV";
$config["aboutUs"] = "ABOUT US";
$config["detailOffice"] = "OUR OFFICE";
$config["address"] = "";
$config["tlp"] = "";
$config["fax"] = "";
$config["contactCenter"] = "";
$config["email"] = "";
$config["website"] = "";
$config["namaFolder"] = "hai";
$config["namaAPK"] = "HAITV-2.0.5-1443673438.apk";
$config["jmlSlider"] = "2";
$config["ibolz_app_id"] = "com.balepoint.ibolz.hai";
$config["ibolz_customer_id"] = "-2";
$config["ibolz_device_id"] = "4";
$config["ibolz_lang"] = "en_US";
$config["ibolz_version"] = "2.0.5";
$config["ibolz_mobile"] = ""; 
?>