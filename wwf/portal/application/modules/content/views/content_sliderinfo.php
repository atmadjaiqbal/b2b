<?php
if($product_detail)
{
   $rcode = isset($product_detail->rcode) ? $product_detail->rcode : '';
   $channel_id = isset($product_detail->channel_id) ? $product_detail->channel_id : '';
   $content_id = isset($product_detail->content_id) ? $product_detail->content_id : '';

   if($menu_type == "channel")
   {
       // [channel_name] => Live - NET TV Blz
       $title = isset($product_detail->alias) ? $product_detail->alias : '';
   } else {
       $title = isset($product_detail->title) ? $product_detail->title : '';
   }

   $shorttitle = strlen($title) > 23 ? substr($title, 0, 23).' ...' : $title;
   $video_duration = isset($product_detail->video_duration) ? $product_detail->video_duration : '';
   $filename = isset($product_detail->filename) ? $product_detail->filename : '';
   $thumbnail = isset($product_detail->thumbnail) ? $product_detail->thumbnail : '';
   $description = isset($product_detail->description) ? $product_detail->description : '';
   $prod_year = isset($product_detail->prod_year) ? $product_detail->prod_year : '';
   $trailer_id = isset($product_detail->trailer_id) ? $product_detail->trailer_id : '';
   $price = isset($product_detail->price) ? $product_detail->price : '';
   $ulike = isset($product_detail->ulike) ? $product_detail->ulike : '';
   $unlike = isset($product_detail->unlike) ? $product_detail->unlike : '';
   $comment = isset($product_detail->comment) ? $product_detail->comment : '';
   $created =  isset($product_detail->created) ? $product_detail->created : '';
   // if($product_detail->crew_category_list) { $lecture = $product_detail->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}


   //$genre_list = $product_detail->genre_list;
   //$photo_list = $product_detail->photo_list;
   //$related_list = $$product_detail->related_list;

   switch($menu_type)
   {
      case 'menu' :
         $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
         break;
      case 'channel' :
         $_link = base_url().'content/content_channel/'.$menu_id.'/'.$channel_id;
         break;
      default:
         $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
         break;
   }
?>
<div id="content-info" style="float:left;width: 430px;">
  <a href="<?php echo $_link;?>">
    <h2 class="site-color"><?php echo $shorttitle; ?></h2>
  </a>
  <div class="content-descript">
     <h3>DESCRIPTION</h3>
     <p><?php echo (strlen($description) > 350 ? substr($description, 0, 350). '...' : $description); ?></p>
     <dl>
        <!-- dt>DATE</dt><dd>:&nbsp;&nbsp;< ?php echo date('l, d-M-Y', strtotime($created));?>&nbsp;TIME :&nbsp;< ?php echo date('H:i', strtotime($created));?></dd -->
     </dl>
  </div>
</div>
    <div id="content-media" style="margin-left:10px;">
    <!-- div class="social">
       <ul class="content-score">
          <li>
              <a class="score-btn" data-tipe='like' id='like-< ?php echo $content_id; ?>' data-id='< ?php echo $content_id; ?>'>
                  <img class="pull-left" src="< ?php echo base_url('assets/images/like_icon_small.png'); ?>">
                  <div id="totallike-< ?php echo $content_id; ?>" data-id='< ?php echo $content_id; ?>' style="width: 24px;">< ?php echo $ulike; ?></div>
              </a>
          </li>
          <li>
              <a class="score-btn" data-tipe='comment' data-id='< ?php echo $content_id; ?>'>
                  <img class="pull-left" src="< ?php echo base_url('assets/images/comment_icon_small.png'); ?>">
              </a>
              <div style="width: 24px;">< ?php echo $comment; ?></div>
          </li>

       </ul>
    </div -->

    <div class="views">
        <div class="button-view">
            <a href="<?php echo $_link;?>">VIEW</a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<script type="text/javascript">

 $(document).ready(function() {

    $('#like-<?php echo $content_id; ?>').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $.post(Settings.base_url+'content/post_like/', {'content_id':id}, function(data){
            if(data.rcode != 'ok'){
                alert(data.msg);
            } else {
                liketotal('#totallike-<?php echo $content_id; ?>');
            }
        });

    });


 });

</script>

<?php
}
?>

