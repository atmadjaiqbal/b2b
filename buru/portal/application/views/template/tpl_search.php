<div class="search-wrapper">
    <ul class="breadcrumb">
        <li><a href="<?php echo site_url();?>"><i class="fa fa-home fa-lg"></i></a></li>
        <?php
        $breadcrumb_base_url = '';
        if(!empty($base_url) && is_array($base_url)):
            $url_path	= '';
            foreach($base_url as $count=>$bc):
                if($bc != 'search'){
                    $url_path .= '/'.$bc;
                    $breadcrumb_base_url = $url_path;
                    if($count < count($base_url) && $count < 3): ?>
                        <li><a href="<?php echo site_url($url_path);?>"><?php echo $bc;?></a></li>
                    <?php endif;
                }
            endforeach; ?>
            <li class="active"><?php echo $base_url[count($base_url)];?></li>
        <?php endif; ?>
        <input type="hidden" id="breadcrumb_base_url" value="<?php echo ($breadcrumb_base_url == '') ? '/' : $breadcrumb_base_url; ?>" />

    </ul>

    <!-- div class="search-form pull-right">
        <div class="input-group" style="margin-top:5px;">
          <form style="margin:0px !important;" action='< ?php echo base_url(); ?>content/search' method='post'>
            <input type="text" name="term" placeholder="搜索 ..." value="" style="border:1px solid #ccc;border-radius:5px;padding-left:5px;">
            <button class="btn btn-nobg site-color" type="submit"> <i class="fa fa-search"> </i> </button>
          </form>

        </div>
    </div -->
</div>