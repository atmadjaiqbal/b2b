<div id="banner-area">
    <img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt ="" style="width: 100%;"/>
    <div class="parallax-overlay"></div>
</div>

<section id="main-container" class="portfolio-static" style="background-color: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h3 class="classic"><i class="fa fa-list-alt"></i><div class="title"></div><?php echo $menu_title;?></h3>
            </div>

            <?php
            if($list_content) {
                $crew_category_list = array(); $genre_list = array();

                foreach($list_content->product_list as $val) {
                    $menu_id = $val->menu_id;
                    $apps_id = $val->apps_id;
                    $product_id = $val->product_id;
                    $channel_category_id = $val->channel_category_id;
                    $channel_id = $val->channel_id;
                    $channel_type_id = $val->channel_type_id;
                    $content_id = $val->content_id;
                    $content_type = $val->content_type;
                    $title = $val->title;
                    $short_title = strlen($title) > 30 ? substr($title, 0, 30). '...' : $title;
                    $thumbnail = $val->thumbnail.'&w=200';
                    $description = $val->description;
                    $prod_year = $val->prod_year;
                    $video_duration = $val->video_duration;
                    $countlike = $val->countlike;
                    $countcomment = $val->countcomment;
                    $countviewer = $val->countviewer;
                    $icon_size = $val->icon_size;
                    $poster_size = $val->poster_size;
                    $crew_category_list = $val->crew_category_list;
                    $genre_list = $val->genre_list;
                    $related_list = $val->related_list;
                    if($val->crew_category_list) { $lecture = $val->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}

                    ?>

                    <?php /*
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#like-<?php echo $content_id; ?>').click(function(e){
                                e.preventDefault();
                                var id = $(this).data('id');
                                $.post(Settings.base_url+'content/post_like/', {'content_id':id}, function(data){
                                    if(data.rcode != 'ok'){
                                        alert(data.msg);
                                    } else {
                                        liketotal('#totallike-<?php echo $content_id; ?>');
                                    }
                                });
                            });
                        });
                    </script>
                    */ ?>

                    <div class="col-sm-3 col-xs-6 portfolio-static-item">
                        <a href="<?php echo base_url();?>content/content_detail/<?php echo $content_id; ?>/<?php echo $menu_id;?>/<?php echo urlencode($menu_title);?>">
                            <div class="grid" style="height: 400px;">
                                <figure class="effect-oscar" style="height: 230px;">
                                    <img src="<?php echo $thumbnail;?>" alt="">
                                    <!--bottom image-->
                                    <img style='border:0;
                                        display:block;
                                        margin:auto;
                                        -moz-transform: scale(1,-1);
                                        -o-transform: scale(1,-1);
                                        -webkit-transform: scale(1,-1);
                                        transform:scale(1,-1);
                                        opacity:0.7;'
                                        src='<?php echo $thumbnail;?>' />
                                </figure>
                                <div class="portfolio-static-desc">
                                    <h3 style="font-size: 14px;"><?php echo (strlen($short_title) > 20 ? substr($short_title, 0, 20).'...' : $short_title);?></h3>
                                    <p style="font-size: 12px;">DESCRIPTION<br>
                                    <?php echo (strlen($description) > 140 ? substr($description, 0, 140).'...' : $description);?></p>
                                    <p style="bottom: 20px;position: absolute;"><i class="fa fa-film"></i> Total Viewer : <?php echo $countviewer; ?></p>
                                </div>                  
                            </div><!--/ grid end -->
                        </a>
                    </div><!--/ item 1 end -->
                    <?php
                }
                ?>
                <div id="content_container" data-page='1' data-categories="<?php echo $menu_id;?>"></div>
                <?php
            }
            ?>
        </div><!-- Content row end -->

        <div class="text-center" style="margin-bottom: 20px;">
            <a id="content_load_more" href="#" class="btn btn-primary solid">LOAD MORE</a>
        </div>
    </div><!-- Container end -->
</section>