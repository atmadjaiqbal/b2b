<div id="member" style="margin-top: 140px;">
    <div class="profile-box" style="margin-left: 350px;">
        <div class="profile-form">
            <?php if(!empty($msgforget)){ ?>
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?php echo $msgforget; ?>.
                </div>
            <?php } ?>

            <h3>Forgot password</h3>
            <div class="member-subtitle" style="margin-left:5% !important;">
                <p style="margin-top:5px;">Enter your email address and we will send you an email that will allow you to reset your password. Don't forget to check your spam-folder.</p>
            </div>

            <form id="forgot-password" class="form" action='<?php echo base_url(); ?>member/forgotpassword' method='post' role="form" novalidate="novalidate" style="margin-top:60px !important;">
                <div class="form-elem">
                    <label for="login_email" style="margin-left:30px;margin-bottom:10px !important;font-weight: bold;">Enter email</label>
                    <input type="text" name="email" class="inputtext" id="login_email" value="" autocomplete="off" style="width: 350px;">
                </div>
                <div class="button-login" style="margin-left:210px !important;margin-top:20px !important;">
                    <a href="#" id="click-forgetpass">Send</a>
                </div>
            </form>
        </div>
    </div>
</div>
