<div class="container" style="margin-top: 140px;">
    <h3 class="slider-title">
        <span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i>
    </h3>
    <div class="row">
        
    <?php 
    $crew_category_list = array(); 
    $genre_list = array();
    if($list_content){
        foreach($list_content->menu_list as $val){
            $product_id = $val->product_id;
            $apps_id = $val->apps_id;
            $menu_id = $val->menu_id;
            $parent_menu_id = $val->parent_menu_id;
            $menu_type = $val->menu_type;
            $title = $val->title;

            $channel_id = !empty($val->channel_id) ? $val->channel_id : '';
            $channel_name = !empty($val->channel_name) ? $val->channel_name : '';
            $alias = !empty($val->alias) ? $val->alias : '';
            $thumbnail = $val->thumbnail . '&w=300';
            $description = !empty($val->description) ? $val->description : '';
            $count_product = !empty($val->count_product) ? $val->count_product : '';

            switch($menu_type){
                case 'menu' :
                    $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title);
                    break;
                case 'channel' :
                    $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                    break;
                default:
                    $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                    break;
            }
    ?>
        <div class="col-md-6" style="margin-bottom: 5px;">
            <div class="row" style="border-radius: 3px 3px 3px 3px;border: solid 1px rgba(0,0,0,0.3);padding-top: 3px;padding-bottom: 3px;">
                <div class="col-md-4">
                    <a href="<?php echo $_link;?>">
                        <img src="<?php echo $thumbnail;?>" style="width: 100%;"/>
                    </a>                        
                </div>
                <div class="col-md-8">
                    <div style="margin-left: -21px;">
                        <a href="<?php echo $_link;?>" style="font-size: 1.5em;">
                        <?php echo $title;?>
                        </a>
                        <p><?php echo $description;?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php

        }
    }?>
    </div>
    <div style="clear: both;">&nbsp;</div>

</div>
<?php /*
<div class="container" style="margin-top: 140px;">

    <div id="content-categories">
        <h3 class="slider-title">
            <span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i>
        </h3>
        <?php
        if($list_content)
        {

            $crew_category_list = array(); $genre_list = array();
        ?>
        <div id="channel-categories-list">
            <ul class="playlist">
        <?php
            foreach($list_content->menu_list as $val)
            {
                $product_id = $val->product_id;
                $apps_id = $val->apps_id;
                $menu_id = $val->menu_id;
                $parent_menu_id = $val->parent_menu_id;
                $menu_type = $val->menu_type;
                $title = $val->title;

                // $channel_id = $val->channel_id;
                //$channel_name = $val->channel_name;
                //$alias = $val->alias;
                $thumbnail = $val->thumbnail . '&w=300';
                //$description = $val->description;
                $count_product = $val->count_product;

                switch($menu_type)
                {
                    case 'menu' :
                        $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title);
                        break;
                    case 'channel' :
                        $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                        break;
                    default:
                        $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                        break;
                }

                if($title != 'Nomor Penting')
                {
        ?>
                <li>
                    <a href="<?php echo $_link;?>">
                        <div class="product-title">
                            <span class="pull-left"><?php echo $title;?></span>
                            <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                        </div>
                        <div class="product-image">
                            <div class="preview_product related bgfull"
                                 style="background-color:#cecece;background: url('<?php echo $thumbnail;?>') no-repeat scroll center;width:270px;height:160px;"
                                 id="'<?php echo $menu_id;?>'-'<?php echo $product_id;?>'"
                                 data-id="'<?php echo $product_id;?>'" data-menu-id="'<?php echo $menu_id;?>'"></div>
                        </div>
                    </a>
                </li>
        <?php
                }
            }
        ?>
           </ul>
        </div>

        <?php
        }
        ?>
    </div>
</div>
*/?>