<div class="row" style="margin-top:150px;margin-left: -15px !important;">
  <div class="col-md-12">
    <?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>     
  </div>
</div>

<?php 
if(!empty($listmenu)){
  foreach($listmenu as $row){
    $c_menu_id = $row['menu_id'];
    $c_category_name = $row['category_name'];
?>
<div class="row">
  <div class="col-md-2">&nbsp;</div>
  <div class="col-md-8">
    <div class="row">
      <div class="col-md-12">
        <h2><?php echo $c_category_name;?></h2>     
      </div>
      <div class="col-md-12">
        <div class="row">
          <?php /*
          <div class="col-md-3">
            <?php 
            # preparing initial value
            if($row['submenu']){
              $te = json_encode($row['submenu']);
              $prev = json_decode($te,true);

              $preview_product_id = $prev[0]['product_id'];
              $preview_apps_id = $prev[0]['apps_id'];
              $preview_title =  $prev[0]['title'];
              $preview_menu_id =  $prev[0]['menu_id'];
              $preview_parent_menu_id =  $prev[0]['parent_menu_id'];
              $preview_menu_type =  $prev[0]['menu_type'];
              $preview_thumbnail =  $prev[0]['thumbnail'];
              $preview_link =  $prev[0]['link'];
              switch(strtolower($preview_menu_type)){
                  case 'menu' :
                      $_previewlink = base_url('content/content_categories_list/'.$preview_menu_id.'/'.urlencode($preview_title));
                      break;
                  case 'channel' :
                      $_previewlink = base_url('content/content_channel/'.$preview_menu_id.'/'.$preview_product_id);
                      break;
                  default:
                      $_previewlink = base_url('content/content_categories/'.$preview_menu_id.'/'.urlencode($preview_title));
                      break;
              } 

              $preview_product_detail = $this->content_lib->product_detail($preview_product_id, $preview_menu_id);
              // $preview_title = !empty($preview_product_detail->title) ? $preview_product_detail->title : '';
              // $preview_channel_id = !empty($preview_product_detail->channel_id) ? $preview_product_detail->channel_id : '';
              // $preview_content_id = !empty($preview_product_detail->content_id) ? $preview_product_detail->content_id : '';
              $preview_video_duration = !empty($preview_product_detail->video_duration) ? $preview_product_detail->video_duration : '';
              // $preview_thumbnail = !empty($preview_product_detail->thumbnail) ? $preview_product_detail->thumbnail : '';
              $preview_description = !empty($preview_product_detail->description) ? $preview_product_detail->description : '';
              $preview_prod_year = !empty($preview_product_detail->prod_year) ? $preview_product_detail->prod_year : '';
            }
            ?>
            <div id="item-preview-<?php echo $preview_menu_id;?>" class="item-preview">
                <div class="caption-box-preview">
                  <div class="item-title" id="item-preview-title-<?php echo $preview_menu_id;?>"><?php echo $preview_title;?></div>
                </div>
                <img class="item-pic-preview" id="item-preview-pic-<?php echo $preview_menu_id;?>" src="<?php echo $preview_thumbnail;?>&w=300" >
            </div>          

          </div>
          */?>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12">
                <div id="owl-demo-<?php echo $c_menu_id;?>" class="owl-carousel demoowl">
                <?php 
                if($row['submenu']){
                  foreach($row['submenu'] as $rwContent){
                    $_c_product_id = $rwContent->product_id;
                    $_c_apps_id = $rwContent->apps_id;
                    $_c_menu_id = $rwContent->menu_id;
                    $_c_parent_menu_id = $rwContent->parent_menu_id;
                    $_c_menu_type = $rwContent->menu_type;
                    $_c_title = $rwContent->title;
                    $_c_thumbnail = $rwContent->thumbnail;
                    $_c_link = $rwContent->link;
                    $_c_icon_size = $rwContent->icon_size;
                    $_c_poster_size = $rwContent->poster_size;
                    $short_title = strlen($_c_title) > 14 ? substr($_c_title, 0, 14).'...' : $_c_title;
                    switch(strtolower($_c_menu_type)){
                        case 'menu' :
                            $_link = base_url('content/content_categories_list/'.$_c_menu_id.'/'.urlencode($_c_title));
                            break;
                        case 'channel' :
                            $_link = base_url('content/content_channel/'.$_c_menu_id.'/'.$_c_product_id);
                            break;
                        default:
                            $_link = base_url('content/content_categories/'.$_c_menu_id.'/'.urlencode($_c_title));
                            break;
                    }   
                    //$_link = '#';
                    $link_id = '';//$_c_menu_id.'_'.$_c_product_id;
                  ?>
                      <div class="item">
                        <div class="carousel_items">
                          <div class="caption-box">
                            <div class="item-title"><?php echo $short_title;?></div>
                          </div>
                          <a href="<?php echo $_link;?>" id="<?php echo $link_id;?>" data-id="<?php echo $_c_product_id;?>" data-menu-id="<?php echo $_c_menu_id;?>" class="preview_content">
                            <img class="lazyOwl item-pic" data-src="<?php echo $_c_thumbnail;?>&w=300" alt="<?php echo urlencode($_c_title);?>" data-id="<?php echo $_c_product_id;?>" 
                            data-menu-id="<?php echo $_c_menu_id;?>" data-menu-type="<?php echo $_c_menu_type;?>">
                          </a>
                        </div>
                      </div>
                   <?php

                  }  
                }
                ?>

                </div>
              </div>
            </div>
            
            <?php /*
            <div class="row">
              <div class="col-md-2">
                <a href="<?php echo $_previewlink;?>" id="view-button-<?php echo $c_menu_id;?>">
                <div class="view_buttons">
                  VIEW  
                </div>
                </a>
              </div>
              <div class="col-md-10">
                &nbsp;
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <?php if(!empty($preview_description)){ ?>
                <h4>DESCRIPTION</h4>
                  <div id="preview-description-<?php echo $_c_menu_id;?>"><?php echo $preview_description;?></div>
                <?php };?>
                <?php if(!empty($preview_video_duration)){ ?>
                <h4>DURATION</h4>
                  <div id="preview-video-duration-<?php echo $_c_menu_id;?>">$preview_video_duration</div>
                <?php };?>

              </div>
            </div>

            */?>

          </div>
        </div>
      </div>
    </div>    
  </div>
  <div class="col-md-2">&nbsp;</div>
</div>
<div class="clear"></div>
<div id="dummydiv"></div>
<script type="text/javascript">
 
  $("#owl-demo-<?php echo $c_menu_id;?>").owlCarousel({
    items : 4,
    lazyLoad : true,
    navigation : true
  }); 
 
</script>



<?php
  };
};
?>
<?php /*
<script type="text/javascript">

$('.item-pic').click(function(){
  var channel_id = '';
  var channel_name = '';
  var video_duration = 0;
  var thumbnail = ''; //both
  var description = ''; //both
  var prod_year = '';

  var menu_id = $(this).data('menu-id');
  var product_id = $(this).data('id');
  var menu_type = $(this).data('menu-type');
  var data = {'menu_id':menu_id,'product_id':product_id,'menu_type':menu_type};
  $.ajax({
    ajax: 'true',
    async: 'true',
    type: 'post',
    dataType: 'json',
    data: data,
    url: '/portal/content/content_info2',
    beforeSend: function(){},
    success: function(response){
      if(response.result == 1){
        channel_id = response.msg.channel_id;
        alert(JSON.stringify(response.msg));
        if(channel_id == ""){
          video_duration = response.msg.video_duration;
          description = response.msg.description;
          thumbnail = response.msg.thumbnail+'&w=300';
          $('#preview-video-duration-'+menu_id).html(video_duration);
          $('#preview-description-'+menu_id).html(description);
          $('#item-preview-pic-'+menu_id).attr('src',thumbnail);
          $('#dummydiv').html(thumbnail);
        }
        // else{

        // }
        // alert(response.msg.thumbnail);
        //alert(JSON.stringify(response.msg));
        //alert(channel_id);  
      }else{
        alert(response.msg);
      }
    },
    complete: function(){}
  });
  return false;
})  
</script>
*/?>