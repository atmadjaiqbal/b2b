<div class="container" style="margin-top: 140px;">
    <div id="faq-bni">
        <div class="faq-title-place">
            <h3>ABOUT US</h3>
        </div>
        <div class="faq-details">
        	<p><b>Greenpeace</b> adalah suatu lembaga swadaya masyarakat, organisasi lingkungan global, yang memiliki cabang di lebih dari 40 negara dengan kantor pusat di Amstrerdam, Belanda.</p>
        	<p>Didirikan di Vancouver, British Columbia, Kanada pada 1971, pada awalnya dengan nama Don't Make a Wave Committee untuk menghentikan percobaan nuklir yang dilakukan pemerintah Amerika Serikat di Amchitka, Alaska. Para aktivis mengirimkan kapal sewaan, Phyllis Cormack, yang diubah namanya menjadi Greenpeace, ke lokasi pengujian nuklir. Mereka lalu mengadopsi nama Greenpeace menjadi nama organisasi.</p>
          <p>Greenpeace dikenal menggunakan aksi langsung tanpa kekerasan, konfrontasi damai dalam melakukan kampanye untuk menghentikan berbagai aksi perusakan lingkungan seperti pengujian nuklir, penangkapan ikan paus besar-besaran, deforestasi, dan sebagainya.</p>
          <p>Organisasi global ini menerima pendanaan melalui kontribusi langsung dari individu yang diperkirakan mencapai 2,8 juta para pendukung keuangan, dan juga dari yayasan amal, tetapi tidak menerima pendanaan dari pemerintah atau korporasi.</p>
        	<br/>        	        	
        </div>
    </div>
</div>