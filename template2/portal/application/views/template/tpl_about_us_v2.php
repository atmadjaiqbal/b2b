<div class="container" style="margin-top: 140px;">
    <div id="faq-bni">
        <div class="faq-title-place">
            <h3>ABOUT US</h3>
        </div>
        <div class="faq-details">
        	<p>Kementerian Perindustrian Republik Indonesia disingkat Kemenperin RI adalah kementerian dalam Pemerintah Indonesia yang membidangi urusan perindustrian. Kementerian Perindustrian berada di bawah dan bertanggung jawab kepada Presiden, serta dipimpin oleh menteri yang sejak tanggal 27 Oktober 2014 dijabat oleh Saleh Husin, SE, M.Si.</p>
        	<p>Menteri Perindustrian Saleh Husin akan terus mendorong pengembangan industri nasional mengingat sektor tersebut merupakan tulang punggung perekonomian Indonesia, dimana sektor industri masih memberikan kotribusi yang cukup signifikan terhadap ekonomi dengan mencapai lebih dari 23% atau menjadi sektor terbesar penyumbang ekonomi nasional.</p>
        	<br/>        	        	
        <h4>TUGAS POKOK</h4>
        <p>Kementerian Perindustrian mempunyai tugas menyelenggarakan urusan di bidang perindustrian dalam pemerintahan untuk membantu Presiden dalam menyelenggarakan pemerintahan negara</p>
        <br/>
        <h4>FUNGSI</h4>
        <p>Dalam melaksanakan tugasnya, Kementerian Perindustrian menyelenggarakan fungsi:</p>
        <ol>
          <li>Perumusan, penetapan, dan pelaksanaan kebijakan di bidang perindustrian;</li>
          <li>Pelaksanaan bimbingan teknis dan supervisi atas pelaksanaan kebijakan di bidang perindustrian;</li>
          <li>Pelaksanaan penelitian dan pengembangan di bidang perindustrian;</li>
          <li>Pelaksanaan dukungan yang bersifat substantif kepada seluruh unsur organisasi di Lingkungan Kementerian Perindustrian;</li>
          <li>Pembinaan dan pemberian dukungan administrasi di Lingkungan Kementerian Perindustrian;</li>
          <li>Pengelolaan barang milik/kekayaan negara yang menjadi tanggung jawab Kementerian Perindustrian.</li>
        </ol>        	
        </div>
    </div>
</div>