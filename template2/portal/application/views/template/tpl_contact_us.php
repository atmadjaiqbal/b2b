<div class="container" style="margin-top: 140px;">
    <div id="faq-bni">
        <div class="faq-title-place">
            <h3>CONTACT US</h3>
        </div>
        <div class="faq-details">
            Terimakasih atas minat anda pada Greenpeace Asia Tenggara. Jika anda mempunyai pertanyaan, silakan hubungi kami:
        	<h4>Greenpeace</h4>
            <p>Mega Plaza Building Lt. 5, Jl. HR. Rasuna Said Kav. C3</p>
            <p>Kuningan, Jakarta Selatan, Indonesia 12920</p>
            <p>Tel : +62 21 521 2552</p>
            <p>Fax: +62 21 521 2553</p>
            <p>Email : info.id@greenpeace.org</p>
            <p>Supporter email : supporterservices.id@greenpeace.org</p>
        </div>
    </div>
</div>