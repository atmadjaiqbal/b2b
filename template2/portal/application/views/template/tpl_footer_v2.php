<?php
//if($this->session->userdata('display_name'))
//{
    $faqlink = base_url().'home/home_faq';
//} else {
//    $faqlink = base_url();
//}
?>


<div class="clear"></div>
<footer id="footer-sec">
    <div class="top-pad"></div>
    <div class="other-link">
        <a href="<?php echo base_url().'home/home_aboutus';?>">About Us</a>&nbsp;|&nbsp;
        <a href="<?php echo $faqlink; ?>">FAQ</a>&nbsp;|&nbsp;
        <a href="<?php echo '#';//base_url().'home/home_usermanual';?>">Help</a>&nbsp;|&nbsp;
        <a href="<?php echo base_url().'home/home_contactus';?>">Contact Us</a>
    </div>
    <div class="belongs">
        Copyright © 2015 TEMPLATE V2.All right reserved.
    </div>

</footer>


<script type="text/javascript">

    <?php
       $success = $this->session->flashdata('message_success');
       $error = $this->session->flashdata('message_error');
       if (!empty($success))
       {  ?>
    alertify.alert("<?php echo $success; ?>");
    alertify.log("Success", "success");
    <?php }
       if (!empty($error))
       { ?>
    alertify.alert("<?php echo $error; ?>");
    alertify.log("Error", "error");
    <?php } ?>

    var vod = {
        init: function() {},
        content_view: function(container_id, content_id, menu_id, menu_type, menu_title, callback)
        {
            $.get(Settings.base_url+'content/content_info/'+content_id+'/'+menu_id+'/'+menu_type+'/'+menu_title, function(result) {
                $(container_id).html(result);
                $(container_id).removeClass('bg-loader');
                if(callback) callback();
            });
        }
    }

    $(document).ready(function() {
    	  $('#slideheadline').carousel({interval: 12000})
        <?php /*  
        $(".link-item").tooltip({placement : 'bottom'});

        $('#click-login').click(function(e){e.preventDefault(); $('#form-login').submit();});
        $('#click-forgetpass').click(function(e){e.preventDefault(); $('#forgot-password').submit();});
        $('#account_password').keypress(function(e){if (e.which == 13) { e.preventDefault(); $('#form-login').submit();}});
        $('#click-saveprofile').click(function(e){e.preventDefault(); $('#change-profile').submit();});
        $('#click-changepass').click(function(e){e.preventDefault(); $('#change-pass').submit();});
        */?>
    });
</script>

</html>

