<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider1, slider2, slider3 .jpg

*/

$config['nameLogo'] = "AL-AZHAR TV";
$config['title'] = "IBOLZ | AL-AZHAR TV";
$config['warnaHeader'] = "#72D4FC";
$config['colorMenu'] = "#1A4876";
$config['warnaCopyright'] = "#1A4876";
$config['warnaNavCarousel'] = "#72D4FC";
$config['colorListbox'] = "#EEEEf4";
$config['warnaMenuMobile'] = "#1A4876";
$config['warnaTab'] = "#16805C";
$config['footerCopyright'] = "AL-AZHAR TV";
$config['jmlSlider'] = 2;

/* --- About Us --- */

$config['aboutUs'] = "";

/* --- Contact Us --- */

$config['contactUs'] = false;
$config['detailOffice'] = "ODY DIVE CENTER INDONESIA";
$config['address'] = "Jl. Panarukan 15 Menteng Jakarta 10310";
$config['tlp'] = "62-21-3921226";
$config['fax'] = "62-21-31905714";
$config['contactCenter'] = "";
$config['email'] = "divebox@odydive.com , supporterservices.id@greenpeace.org";
$config['website'] = "www.odydive.com";

/* --- Download --- */

$config['namaFolder'] = "allazhartv";
$config['namaAPK'] = "Al-AzharTV-2.0.5"; // tanpa .apk

?>

