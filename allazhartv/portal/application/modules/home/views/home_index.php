<?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>

<section id="main-container" class="portfolio-static">
	<div class="container">

		<div class="row">
			<div class="featured-tab clearfix">
				<ul class="nav nav-tabs nav-stacked col-md-3 col-sm-5">
					<?php
				    if($listmenu) {
				    	$i=1;
				        foreach($listmenu as $row) {
				            $c_menu_id = $row['menu_id'];
				            $c_category_name = $row['category_name'];
				            $active = ($i==1 ? 'class="active"' : '');
				            ?>
						  	<li <?php echo $active;?>>
						  		<a class="animated fadeIn" href="#tab_<?php echo $c_menu_id;?>" data-toggle="tab">
						  			<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/logo_header.png" alt="logo" style="width: 25px;float: left;margin-top: 12px;">
						  			<!-- <i class="fa fa-bank"></i> -->
						  			<div class="tab-info"><h6><?php echo $c_category_name;?></h6></div>
						  		</a>
						  	</li>
						  	<?php
						  	$i++;
				        }
				    }	
					?>
				</ul>
				<div class="tab-content col-md-9 col-sm-7">

					<?php
				    if($listmenu) {
				    	$x=1;
				        foreach($listmenu as $row) {
				        	$c_menu_id = $row['menu_id'];
				            $c_category_name = $row['category_name'];
				            $active = ($x==1 ? 'active ' : ''); ?>

				            <div class="tab-pane <?php echo $active;?>animated fadeInLeft" id="tab_<?php echo $c_menu_id;?>">

				            <?php
				            if($row['submenu']) {
	                            foreach($row['submenu'] as $rwContent) {

	                                $_c_product_id = $rwContent->product_id;
	                                $_c_apps_id = $rwContent->apps_id;
	                                $_c_menu_id = $rwContent->menu_id;
	                                $_c_parent_menu_id = $rwContent->parent_menu_id;
	                                $_c_menu_type = $rwContent->menu_type;
	                                $_c_title = $rwContent->title;
	                                $_c_thumbnail = $rwContent->thumbnail;
	                                $_c_link = $rwContent->link;
	                                $_c_icon_size = $rwContent->icon_size;
	                                $_c_poster_size = $rwContent->poster_size;
	                                $short_title = strlen($_c_title) > 14 ? substr($_c_title, 0, 14).'...' : $_c_title;

	                                switch(strtolower($_c_menu_type))
	                                {
	                                    case 'menu' :
	                                        $_link = base_url().'content/content_categories_list/'.$_c_menu_id.'/'.urlencode($_c_title);
	                                        break;
	                                    case 'channel' :
	                                        $_link = base_url().'content/content_channel/'.$_c_menu_id.'/'.$_c_product_id;
	                                        break;
	                                    default:
	                                        $_link = base_url().'content/content_categories/'.$_c_menu_id.'/'.urlencode($_c_title);
	                                        break;
	                                }
	                                ?>

							        	<div class="col-sm-3 col-xs-6 portfolio-static-item">
											<a href="<?php echo $_link;?>">
												<div class="grid">
													<figure class="effect-oscar">
														<img src="<?php echo $_c_thumbnail;?>&w=300" alt="">		
													</figure>
													<?php
													if ($row['category_name']!=="iBOLZ - Live Channel") { ?>													
														<div class="portfolio-static-desc">
															<h3><?php echo $short_title;?></h3>
															<p><?php echo $c_category_name;?></p>
														</div>
													<?php
													}
													?>					
												</div><!--/ grid end -->
											</a>
										</div><!--/ item 1 end -->

	                                <?php
	                            }
	                        }
				            ?>
				        	</div>
				            <?php
				            $x++;
				        }
				    }
					?>
				</div><!-- tab content -->
			</div><!-- Featured tab end -->
		</div>

	</div><!-- Container end -->
</section>