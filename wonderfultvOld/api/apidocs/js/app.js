/* 
	Author: htridianto
	Email: htridianto@gmail.com
*/
var mainContentScroll, navContentScroll;
var REST_API = 'http://api106.ibolz.vhost'; // 'http://china2.ibolz.tv/api106';//
var apps_id = 'com.balepoint.ibolz.demo.cn';
var logged_account_id = ''; // is dummy
var default_lang = 'en';
var config_path;
// init main application module
var MainApp = {
	app_name: 'iBolz',
	app_version: '1.6',
	routes: {},
	initialize: function () { // Called once, at app startup
		require.config({ //	------- requirejs config
			paths:{
				'underscore': 'libs/underscore/underscore',
				'backbone':'libs/backbone/backbone',				
				'i18n':'libs/i18n',
				'text':'libs/require/text',				
				'templates':'../html',	
			},
			shim:{
				'backbone' :{
					deps:['underscore'], export: 'Backbone'
				}
			}
		});			
		require(['underscore', 'backbone', 'router'],
			function(_, Backbone, Router){
			    navContentScroll = new iScroll('nav_content', {
			    	checkDOMChanges: true
			    });  
			    // mainContentScroll = new iScroll('main_content');  
				var strPath = window.location.href;
	            if(strPath.indexOf('http://') == 0 && strPath.indexOf("/apidocs") > 0){
	                REST_API = strPath.substr(0,strPath.indexOf('/apidocs'));
	            }	
				MainApp.routes = new Router();
				Backbone.history = Backbone.history || new Backbone.History({});
				Backbone.history.start();
			}
		); 
	}// endo of initialize
};
MainApp.initialize();

$.ajaxSetup({
	crossDomain: true,
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Accept-Language', default_lang);
        xhr.setRequestHeader("Authorization", "Basic "+ btoa('ibolz:kramat@123#'));        
    }
});
