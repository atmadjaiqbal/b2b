<?php defined('BASEPATH') OR exit('No direct script access allowed');
  
class Provider extends CI_Controller{
	
	function Provider(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('provider_model');
		$this->load->model('apps_model');;
        $this->output->set_content_type('application/json');
	}	
	
	
	function provider_list(){
        $apps_id = $this->input->get("apps_id");
		
		$provider_list = $this->provider_model->provider_list($apps_id);
		$response = array(
        	"provider_list" => $provider_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
	}
	
	
}
