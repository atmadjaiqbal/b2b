<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Play extends CI_Controller{
	
	function Play(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('channel_model');
		$this->load->model('apps_model');
		$this->load->model('server_model');
		$this->load->model('schedule_model');
		$this->load->model('program_model');
		$this->load->model('vod_model');
		$this->load->model('statistic_model');
        $this->output->set_content_type('application/json');
	}	
	
	

	
    function channel(){
		$subscriber_addr = $_SERVER['REMOTE_ADDR'];
        $apps_id = $this->input->post("apps_id");
        $version = $this->input->post("version");
        $device_id = $this->input->post("device_id");
        $res_id = $this->input->post("res_id");
        $latitude = $this->input->post("latitude");
        $longitude = $this->input->post("longitude");
        $customer_id = $this->input->post("customer_id");
        $channel_id = $this->input->post("channel_id");
        $screen_size = $this->input->post("screen_size");
        $remote_host = $_SERVER['REMOTE_ADDR'];
        $schedule_type_id = $this->input->post("schedule_type_id");
        $mobile_id = $this->input->post("mobile_id");
        $vcodec = $this->input->post("vcodec");
		
		$lang = $this->input->post("lang");
		$msg2= "Channel Not Available";
		$msg3= "Server Not Available";
		if($lang == "zh_CN"){
			$msg2= "通道不可用";
			$msg3= "服务器不可用";			
		}
		
		
		if($schedule_type_id == ""){
			$schedule_type_id = 1;
		}
		
        $status = 1;
		
		$channel = $this->channel_model->channel_detail($apps_id, $channel_id);
		if($channel){
			//get subscriber address
			$vid_server = $this->channel_model->channel_server_cluster($channel_id);
			//get subscriber message address
			$msg_server = $this->server_model->server_detail($channel->msg_server_id);
			if($msg_server){
				$channel->msg_subs_ip = $msg_server->public_ip;
			}
			//get channel schedule now 
			$schedule_now = $this->schedule_model->schedule_now($channel->channel_id, $schedule_type_id);
			$channel->schedule_now = $schedule_now;

			//get channel schedule next 
			$schedule_next = $this->schedule_model->schedule_next($channel->channel_id, $schedule_type_id);
			$channel->schedule_next = $schedule_next;
			
			if($vid_server){
				$channel->vid_subs_ip = $vid_server->public_ip;
				
				//Kepepet pip di paksa pake flv format
				if($vcodec == 'flv'){
					$device_id = '2';
				}
				$apps_res_list = $this->apps_model->apps_res_list($apps_id, $device_id, $lang);
				foreach($apps_res_list as $apps_res){
					$channel_res_detail = $this->channel_model->channel_res_detail($channel_id, $device_id, $apps_res->res_id);
					if(($channel_res_detail)){
						$apps_res->available = 1;
						$apps_res->connection_url = 'rtmp://' . $channel->vid_subs_ip . '/' . $channel->vid_subs_appl;
						$apps_res->stream_name = $channel->channel_id . '/'  . $schedule_type_id . '/' . $device_id . '/' . $apps_res->res_id;
					}else{
						$apps_res->available = 0;
						$apps_res->connection_url = '';
						$apps_res->stream_name = '';
					}
				}
				$channel->res_list = $apps_res_list;

				$device_id_temp = '2';
				if($device_id_temp != $device_id){
					$apps_res_list_temp = $this->apps_model->apps_res_list($apps_id, $device_id_temp, $lang);
					foreach($apps_res_list_temp as $apps_res){
						$channel_res_detail = $this->channel_model->channel_res_detail($channel_id, $device_id_temp, $apps_res->res_id);
						if(($channel_res_detail)){
							$apps_res->available = 1;
							$apps_res->connection_url = 'rtmp://' . $channel->vid_subs_ip . '/' . $channel->vid_subs_appl;
							$apps_res->stream_name = $channel->channel_id . '/'  . $schedule_type_id . '/' . $device_id_temp . '/' . $apps_res->res_id;
						}else{
							$apps_res->available = 0;
							$apps_res->connection_url = '';
							$apps_res->stream_name = '';
						}
					}
					//For smooth transition
					$channel->res_list_temp = $apps_res_list_temp;
				}

				$channel->rcode = 1;

				$status = 1;
			}else{
				$channel = array(
					"rcode"=>3,
					"message"=>$msg3
				);
				$status = 3;
			}
		}else{
			$channel = array(
				"rcode"=>2,
				"message"=>$msg2
			);
			$status = 2;
		}
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($channel));
		
		$this->statistic_model->apps_activity('play', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($channel));
		
    }
	
	

    function program(){
		
		$subscriber_addr = $_SERVER['REMOTE_ADDR'];
        $apps_id = $this->input->post("apps_id");
        $version = $this->input->post("version");
        $device_id = $this->input->post("device_id");
        $res_id = $this->input->post("res_id");
        $latitude = $this->input->post("latitude");
        $longitude = $this->input->post("longitude");
        $customer_id = $this->input->post("customer_id");
        $channel_id = $this->input->post("channel_id");
        $program_id = $this->input->post("program_id");
        $remote_host = $_SERVER['REMOTE_ADDR'];
        $screen_size = $this->input->post("screen_size");
        $mobile_id = $this->input->post("mobile_id");
        $vcodec = $this->input->post("vcodec");
		$lang = $this->input->post("lang");

        $status = 1;
        $message = '';
		
		$msg2= "Channel Not Available";
		$msg3= "Server Not Available";
		if($lang == "zh_CN"){
			$msg2= "通道不可用";
			$msg3= "服务器不可用";			
		}
		
		



		$channel = $this->channel_model->channel_detail($apps_id, $channel_id);
		
		if($channel){
			//get subscriber address
			$vid_server = $this->channel_model->channel_server_cluster($channel_id);
			//get subscriber message address
			$msg_server = $this->server_model->server_detail($channel->msg_server_id);
			if($msg_server){
				$channel->msg_subs_ip = $msg_server->public_ip;
			}
			
			$channel->channel_type_id = 3; //vod
			$channel->vid_subs_appl = 'vod';
			
			if($vid_server){ 
				$channel->vid_subs_ip = $vid_server->public_ip;
				
				//Kepepet pip di paksa pake flv format
				if($vcodec == 'flv'){
					$device_id = '2';
				}
				
				$apps_res_list = $this->apps_model->apps_res_list($apps_id, $device_id, $lang);
				$channel->content_list = $this->program_model->program_content_list($program_id);
				for($i=0; $i<count($channel->content_list); $i++){
					$channel->content_list[$i]->res_list = $apps_res_list;
					for($j=0; $j<count($channel->content_list[$i]->res_list); $j++){
						$content_res_detail = $this->program_model->content_res_detail($channel->content_list[$i]->content_id, $device_id, $channel->content_list[$i]->res_list[$j]->res_id);
						$available = 0;
						$connection_url = '';
						$stream_name = '';
						if(($content_res_detail)){
							$available = 1;
							$connection_url = 'rtmp://' . $channel->vid_subs_ip . '/' . $channel->vid_subs_appl;
							if($device_id == '2'){
								$stream_name = $content_res_detail->res_id . '/iphone/' . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							}else{
								$stream_name = $content_res_detail->res_id . '/' . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							}
							//$stream_name = substr($content_res_detail->dest_dir, strrpos($content_res_detail->dest_dir, "/", -2) + 1) . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
						}
						
						$channel->content_list[$i]->res_list[$j] = array(
							"res_id"=> $channel->content_list[$i]->res_list[$j]->res_id,
							"res_name"=> $channel->content_list[$i]->res_list[$j]->res_name,
							"available" => $available,
							"connection_url" => $connection_url,
							"stream_name" => $stream_name
						);
					}

					//res list temp
					$device_id_temp = 2;
					$channel->content_list[$i]->res_list_temp = $apps_res_list;
					for($j=0; $j<count($channel->content_list[$i]->res_list_temp); $j++){
						$content_res_detail = $this->program_model->content_res_detail($channel->content_list[$i]->content_id, $device_id_temp, $channel->content_list[$i]->res_list_temp[$j]->res_id);
						$available = 0;
						$connection_url = '';
						$stream_name = '';
						if(($content_res_detail)){
							$available = 1;
							$connection_url = 'rtmp://' . $channel->vid_subs_ip . '/' . $channel->vid_subs_appl;
							if($device_id_temp == '2'){
								$stream_name = $content_res_detail->res_id . '/iphone/' . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							}else{
								$stream_name = $content_res_detail->res_id . '/' . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							}
							//$stream_name = substr($content_res_detail->dest_dir, strrpos($content_res_detail->dest_dir, "/", -2) + 1) . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
						}
						
						$channel->content_list[$i]->res_list_temp[$j] = array(
							"res_id"=> $channel->content_list[$i]->res_list_temp[$j]->res_id,
							"res_name"=> $channel->content_list[$i]->res_list_temp[$j]->res_name,
							"available" => $available,
							"connection_url" => $connection_url,
							"stream_name" => $stream_name
						);
					}
					//end of res list temp



				}
				$channel->rcode = 1;
				$status = 1;
				
			}else{
				$channel = array(
					"rcode"=>3,
					"message"=>$msg3
				);
				$status = 3;
			}
		}else{
			$channel = array(
				"rcode"=>2,
				"message"=>$msg2
			);
			$status = 2;
		}


        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($channel));
		
		$this->statistic_model->apps_activity('play_vod', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($channel));
	}
	

	
    function content(){
		
		$subscriber_addr = $_SERVER['REMOTE_ADDR'];
        $apps_id = $this->input->post("apps_id");
        $version = $this->input->post("version");
        $device_id = $this->input->post("device_id");
        $res_id = $this->input->post("res_id");
        $latitude = $this->input->post("latitude");
        $longitude = $this->input->post("longitude");
        $customer_id = $this->input->post("customer_id");
        $channel_id = $this->input->post("channel_id");
        $content_id = $this->input->post("content_id");
        $remote_host = $_SERVER['REMOTE_ADDR'];
        $screen_size = $this->input->post("screen_size");
        $mobile_id = $this->input->post("mobile_id");
        $vcodec = $this->input->post("vcodec");
		$lang = $this->input->post("lang");

        $status = 1;
        $message = '';
		
		$msg2= "Content Not Available";
		$msg3= "Server Not Available";
		if($lang == "zh_CN"){
			$msg2= "通道不可用";
			$msg3= "服务器不可用";			
		}
		
		



		$channel = $this->channel_model->channel_detail($apps_id, $channel_id);
		
		if($channel){
			//get subscriber address
			$vid_server = $this->channel_model->channel_server_cluster($channel_id);
			//get subscriber message address
			$msg_server = $this->server_model->server_detail($channel->msg_server_id);
			if($msg_server){
				$channel->msg_subs_ip = $msg_server->public_ip;
			}
			
			$channel->channel_type_id = 3; //vod
			$channel->vid_subs_appl = 'RTMPVod';
			
			if($vid_server){ 
				$channel->vid_subs_ip = $vid_server->public_ip;
				
				//Kepepet pip di paksa pake flv format
				if($vcodec == 'flv'){
					$device_id = '2';
				}
				
				$apps_res_list = $this->apps_model->apps_res_list($apps_id, $device_id, $lang);
				$channel->content_list = $this->vod_model->play_list($apps_id, $channel_id, $content_id, $customer_id);
				for($i=0; $i<count($channel->content_list); $i++){
					$channel->content_list[$i]->res_list = $apps_res_list;
					for($j=0; $j<count($channel->content_list[$i]->res_list); $j++){
						$content_res_detail = $this->program_model->content_res_detail($channel->content_list[$i]->content_id, $device_id, $channel->content_list[$i]->res_list[$j]->res_id);
						$available = 0;
						$connection_url = '';
						$stream_name = '';
						if(($content_res_detail)){
							$available = 1;
							$connection_url = 'rtmp://' . $channel->vid_subs_ip . '/' . $channel->vid_subs_appl;
							$abs_path = $content_res_detail->dest_dir . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							$stream_name = substr($abs_path, 11);
							/*
							if($device_id == '2'){
								$stream_name = $content_res_detail->res_id . '/iphone/' . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							}else{
								$stream_name = $content_res_detail->res_id . '/' . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							}
							*/
							//$stream_name = substr($content_res_detail->dest_dir, strrpos($content_res_detail->dest_dir, "/", -2) + 1) . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
						}
						
						$channel->content_list[$i]->res_list[$j] = array(
							"res_id"=> $channel->content_list[$i]->res_list[$j]->res_id,
							"res_name"=> $channel->content_list[$i]->res_list[$j]->res_name,
							"available" => $available,
							"connection_url" => $connection_url,
							"stream_name" => $stream_name
						);
					}

					//res list temp
					$device_id_temp = 2;
					$channel->content_list[$i]->res_list_temp = $apps_res_list;
					for($j=0; $j<count($channel->content_list[$i]->res_list_temp); $j++){
						$content_res_detail = $this->program_model->content_res_detail($channel->content_list[$i]->content_id, $device_id_temp, $channel->content_list[$i]->res_list_temp[$j]->res_id);
						$available = 0;
						$connection_url = '';
						$stream_name = '';
						if(($content_res_detail)){
							$available = 1;
							$connection_url = 'rtmp://' . $channel->vid_subs_ip . '/' . $channel->vid_subs_appl;
							$abs_path = $content_res_detail->dest_dir . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							$stream_name = substr($abs_path, 11);
							
							/*
							if($device_id_temp == '2'){
								$stream_name = $content_res_detail->res_id . '/iphone/' . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							}else{
								$stream_name = $content_res_detail->res_id . '/' . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
							}
							*/
							//$stream_name = substr($content_res_detail->dest_dir, strrpos($content_res_detail->dest_dir, "/", -2) + 1) . $channel->content_list[$i]->filename . '.' . $content_res_detail->video_format;
						}
						
						$channel->content_list[$i]->res_list_temp[$j] = array(
							"res_id"=> $channel->content_list[$i]->res_list_temp[$j]->res_id,
							"res_name"=> $channel->content_list[$i]->res_list_temp[$j]->res_name,
							"available" => $available,
							"connection_url" => $connection_url,
							"stream_name" => $stream_name
						);
					}
					//end of res list temp



				}
				$channel->rcode = 1;
				$status = 1;
				
			}else{
				$channel = array(
					"rcode"=>3,
					"message"=>$msg3
				);
				$status = 3;
			}
		}else{
			$channel = array(
				"rcode"=>2,
				"message"=>$msg2
			);
			$status = 2;
		}


        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($channel));
		
		$this->statistic_model->apps_activity('play_content', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($channel));
	}
	
	function test(){
		echo substr('mnt/media/streaming/hd/', strrpos('mnt/media/streaming/hd/', "/", -2) + 1);;
	}
    	

    
}
