<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Activity extends CI_Controller{
	
	function Activity(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('activity_model');
		$this->load->helper('string');
		$this->load->library('Uuid.php');
        $this->output->set_content_type('application/json');
	}	
	
	// start action like //
    function set_act_like(){
		$output = array(
			'status' => 0
		);	
		
        $content_id = $this->input->post("content_id");
        $account_id = $this->input->post("account_id");
		$like_type = $this->input->post("like_type");

		if ( !empty($content_id) && !empty($account_id) && !empty($like_type) ){
			$datas = array(
				'content_id' => $content_id,
				'account_id' => $transaction_id,
				'like_type' => $like_type
			); 
			$datacheck = $this->activity_model->get_act_like($content_id, $account_id);
			if ( !empty($datacheck) ){
				$like_id = $datacheck['like_id'];
				$datas['like_id'] = $like_id;
				$this->activity_model->update_act_like($like_id, $datas);
			}else{
				$like_id = str_replace(array('-','{','}'), '', $this->uuid->v4());
				$datas['like_id'] = $like_id;
				$this->activity_model->set_act_like($datas);
			}
			$output['datas'] = $datas;
		}else{
			$output['status'] = 1;
			$output['message'] = "Invalid content_id and account_id";
		}
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
	// end action like //

	// start action comment //
    function get_list_comment(){
		$output = array(
			'status' => 0
		);	
		
        $content_id = $this->input->get("content_id");

		if ( !empty($content_id) ){
			$datas = $this->activity_model->get_list_comment($content_id);
			$output['datas'] = $datas;
		}else{
			$output['status'] = 1;
			$output['message'] = "Invalid content_id";
		}
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
	
    function set_act_comment(){
		$output = array(
			'status' => 0
		);	
		
        $content_id = $this->input->post("content_id");
        $account_id = $this->input->post("account_id");
		$commnet_text = $this->input->post("commnet_text");

		if ( !empty($content_id) && !empty($account_id) && !empty($commnet_text) ){
			$datas = array(
				'content_id' => $content_id,
				'account_id' => $transaction_id,
				'commnet_text' => $commnet_text,
				'comment_id' => str_replace(array('-','{','}'), '', $this->uuid->v4())
			);
			$this->activity_model->set_act_comment($datas);
			$output['datas'] = $datas;
		}else{
			$output['status'] = 1;
			if(empty($commnet_text)){
				$output['message'] = "Comment text can't empty";
			}else{
				$output['message'] = "Invalid content_id and account_id";
			}
		}
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
	
    function update_act_comment(){
		$output = array(
			'status' => 0
		);	
		
        $comment_id = $this->input->post("comment_id");
		$content_id = $this->input->post("content_id");
        $account_id = $this->input->post("account_id");
		$commnet_text = $this->input->post("commnet_text");

		if ( !empty($comment_id) && !empty($content_id) && !empty($account_id) && !empty($commnet_text) ){
			$datas = array(
				'content_id' => $content_id,
				'account_id' => $transaction_id,
				'commnet_text' => $commnet_text,
				'comment_id' => $comment_id
			);
			$this->activity_model->update_act_comment($datas);
			$output['datas'] = $datas;
		}else{
			$output['status'] = 1;
			if(empty($commnet_text)){
				$output['message'] = "Comment text can't empty";
			}else{
				$output['message'] = "Invalid content_id and account_id";
			}
		}
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
	
    function delete_act_comment(){
		$output = array(
			'status' => 0
		);	
		
        $comment_id = $this->input->get("comment_id");

		if ( !empty($comment_id) ){
			$this->activity_model->delete_act_comment($comment_id);
		}else{
			$output['status'] = 1;
			$output['message'] = "Invalid comment_id";
		}
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
	// end action comment //
	
}
