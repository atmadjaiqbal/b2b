<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Socialmedia_model extends MY_Model {

    public function getcontentlastactivity($id=0)
    {
        $_sql = "SELECT sm.*, cs.display_name, ct.content_id, ct.title FROM
             (
                SELECT * FROM
                (
                   SELECT 'comment' AS sostipe, customer_id, comment_text AS teks, content_id, created, COUNT(*) total FROM
                   (
                      SELECT * FROM content_comment cc ORDER BY created DESC
                   ) AS last_comment GROUP BY content_id UNION ALL

                   SELECT 'like' AS sostipe, customer_id, 'like' AS teks, content_id, created, COUNT(*) total FROM
                   (
                      SELECT * FROM content_like cl ORDER BY created DESC
                   ) AS last_like GROUP BY content_id UNION ALL

                   SELECT 'dislike' AS sostipe, customer_id, 'dislike' AS teks, content_id, created, COUNT(*) total FROM
                   (
                      SELECT * FROM content_dislike cd ORDER BY created DESC
                   ) AS last_dislike GROUP BY content_id

                ) AS act ORDER BY created DESC
             ) AS sm
             JOIN customer cs ON cs.account_id = sm.customer_id
             LEFT JOIN content ct ON ct.content_id = sm.content_id";

        if($id != 0) $_sql .= " WHERE sm.content_id = '$id' ";

        $query = $this->db->query($_sql);
        $rows =  $query->result_array();
        return $rows;
    }

    public function get_content_score($content_id)
    {
        $_sql = "SELECT * FROM
            (
                SELECT * FROM
                (
                    SELECT 'like' AS tipe, content_id, COUNT(`content_id`) AS total
                    FROM content_like cl WHERE content_id = '$content_id'
                )AS `like` UNION ALL

                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, content_id, COUNT(`content_id`) AS total
                    FROM content_dislike cd WHERE content_id = '$content_id'
                )AS `dislike` UNION ALL

                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, content_id, COUNT(cc.`content_id`) AS total
                    FROM content_comment cc WHERE content_id = '$content_id'
                )AS `comment`

            )AS last_count";

        $query = $this->db->query($_sql);
        $rows =  $query->result_array();
        return $rows;
    }

    public function get_content_like($select='*', $where=array())
    {
        $this->db->select($select);
        $this->db->where($where);
        $data = $this->db->get('content_like');
        return $data;
    }

    public function get_content_dislike($select='*', $where=array())
    {
        $this->db->select($select);
        $this->db->where($where);
        $data = $this->db->get('content_dislike');
        return $data;
    }

    public function insert_content_like($data=array())
    {
        $this->db->insert('content_like', $data);
    }

    public function insert_content_dislike($data=array())
    {
        $this->db->insert('content_dislike', $data);
    }

    public function update_content_like($data=array(), $where=array())
    {
        $this->db->where($where);
        $this->db->update('content_like', $data);
    }

    public function update_content_dislike($data=array(), $where=array())
    {
        $this->db->where($where);
        $this->db->update('content_dislike', $data);
    }

    public function delete_content_like($where=array())
    {
        $this->db->where($where);
        $this->db->delete('content_like');
    }

    public function delete_content_dislike($where=array())
    {
        $this->db->where($where);
        $this->db->delete('content_dislike');
    }

    public function get_content($select="*", $where=array())
    {
        $this->db->select($select);
        $this->db->where($where);
        $data = $this->db->get('content');
        return $data;
    }

    public function get_comment($content_id, $limit=0, $offset=10)
    {
        $_sql = "SELECT cc.*, cu.`display_name` FROM content_comment cc
                 LEFT JOIN customer cu ON cu.`customer_id`= cc.`customer_id`
                 WHERE cc.content_id = '".$content_id."'
                 ORDER BY cc.created DESC LIMIT $limit, $offset;";
        $query = $this->db->query($_sql);
        $rows =  $query->result_array();
        return $rows;
    }

    public function insert_comment($data=array())
    {
        $this->db->insert('content_comment', $data);
        return 'ok';
    }

    public function delete_comment($where=array())
    {
        $this->db->where($where);
        $this->db->delete('content_comment');
    }

    function get_comment_by_comment_id($comment_id)
    {
        $_sql = "SELECT cc.*, cu.`display_name` FROM content_comment cc
                 LEFT JOIN customer cu ON cu.`customer_id`= cc.`customer_id`
                 WHERE cc.comment_id = '".$comment_id."'
                 ORDER BY cc.created DESC";
        $query = $this->db->query($_sql);
        $rows =  $query->result_array();
        return $rows;
    }

}

?>