<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_model extends MY_Model {
	
	// start action like //
	public function get_act_like($content_id, $account_id) {	
		$textquery = "
			SELECT 
				like_id, content_id, account_id, created, like_type
			FROM content_like
			WHERE content_id = '$content_id'
		 	AND account_id = '$account_id' ";

		$query = $this->db->query($textquery);	
		$row = $query->row();

		return $row;
	}
	
	public function set_act_like($datas){
		if (empty($datas)) return false;
		$textquery = "
			INSERT INTO content_like (
				like_id, content_id, account_id, created, like_type
			) VALUES (
				'".mysql_real_escape_string($datas['like_id'])."', 
				'".mysql_real_escape_string($datas['content_id'])."', 
				'".mysql_real_escape_string($datas['account_id'])."', 
				now(), 
				".mysql_real_escape_string($datas['like_type']).", 
			)
		 	";
		$query = $this->db->query($textquery);	
	}
	
	public function update_act_like($row_id, $datas){
		$textquery = "
			UPDATE content_like
			SET like_type = '".mysql_real_escape_string($datas['like_type'])."', 
				created = now()
			WHERE like_id = '".$row_id."' ";
			
		$query = $this->db->query($textquery);	

		return true;
	}
	// end action like //
	
	// start action comment //
	public function get_list_comment($content_id) {	
		$textquery = "
			SELECT 
				comment_id, content_id, account_id, created, comment_text
			FROM content_comment
			WHERE content_id = '$content_id' ";
		$query = $this->db->query($textquery);	
		return $query->result();
	}
	
	public function set_act_comment($datas){
		if (empty($datas)) return false;
		$textquery = "
			INSERT INTO content_comment (
				comment_id, content_id, account_id, created, comment_text
			) VALUES (
				'".mysql_real_escape_string($datas['comment_id'])."', 
				'".mysql_real_escape_string($datas['content_id'])."', 
				'".mysql_real_escape_string($datas['account_id'])."', 
				now(), 
				'".mysql_real_escape_string($datas['comment_text'])."', 
			)
		 	";
		$query = $this->db->query($textquery);	
	}
	
	public function update_act_comment($datas){
		$textquery = "
			UPDATE content_comment
			SET comment_text = '".mysql_real_escape_string($datas['comment_text'])."', 
				created = now()
			WHERE comment_id = '".mysql_real_escape_string($datas['comment_id'])."' ";
			
		$query = $this->db->query($textquery);	

		return true;
	}
	
	public function delete_act_comment($row_id){
		$textquery = "
			DELETE content_comment
			WHERE comment_id = '".$row_id."' ";
			
		$query = $this->db->query($textquery);	

		return true;
	}
	// end action comment //
	
}

?>
