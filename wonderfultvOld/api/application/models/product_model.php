<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends MY_Model {
	
	function Product_model(){
		parent::__construct();
		$this->load->model('apps_model');
	}	
	


	public function product_list($page, $limit, $apps_id, $menu_id){
			$textquery = "
				SELECT 
					a.apps_id, a.product_id, b.content_id, b.title, b.thumbnail, 
					b.content_type, a.order_number, b.video_duration
				FROM menu_product a, vcontent b
				WHERE a.apps_id = '$apps_id'
				AND a.product_id = b.content_id
			";
					
			$query = $this->db->query($textquery);	
			$rows =  $query->result();
			foreach($rows as $row){
				if($row->thumbnail){
					$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
				}else{
					$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
				}
				
			}
			
			return $rows;
		
	}
	

}

?>
