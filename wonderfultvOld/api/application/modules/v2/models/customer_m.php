<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customer_m extends MY_Model {
	
	// ====================== add by htridianto, Nov 11 2014
	protected $table 	= 'customer';
	protected $key		= 'customer_id';

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('email');
	}

	public function signin($user_name, $password, $latitude, $longitude){
		$password = md5('mat123' . $password . 'kra');
		if(valid_email($user_name)){
			$customer = $this->find_by(array(
				'LOWER(email)' => strtolower($user_name),
				'password' => $password
			));
		}else{
			$customer = $this->find_by(array(
				'mobile_no' => $user_name,
				'password' => $password
			));
		}
		if($customer){
			$this->update($customer->customer_id, array(
				'last_login' => $this->set_date(), 
				'latitude' => $latitude, 
				'longitude' => $longitude
			));
		}
		return $customer;
	}

	public function get_account_by_mobile_no($mobile_no){
		$textquery = "
			SELECT 
				customer_id, email, mobile_id, user_id, first_name, last_name, 
				display_name, status
  			FROM customer
  			WHERE lower(mobile_no) = lower('$mobile_no')
			";
  		$query = $this->db->query($textquery);	
		return $query->row();
	}		
	// ====================== end of htridianto ==================

	public function login($email, $password, $latitude, $longitude) {	
		$password = md5('mat123' . $password . 'kra');	
		$textquery = "
			SELECT 
				mobile_id, customer_id, email, user_id, first_name, last_name, 
				display_name, status, verify_status, mobile_no, credit_balance
  			FROM customer
  			WHERE LOWER(email) = LOWER('$email')
  			AND password = '$password'
  			AND status = 1
			";
		$query = $this->db->query($textquery);	
		$row = $query->row();
		if($row){
			$row->group_list = $this->group_list($row->customer_id);
			$textquery = "
				UPDATE customer
				SET last_login=now(), 
				latitude='$latitude', 
				longitude= '$longitude'
				WHERE customer_id='$row->customer_id'
			 	";
			$this->db->query($textquery);	
		}
		return $row;
	}
	
	public function profile($customer_id) {	
		$textquery = "
			SELECT 
				mobile_id, customer_id, email, user_id, first_name, last_name, filename, '' as thumbnail,
				display_name, status, mobile_no, verify_status, credit_balance
  			FROM customer
  			WHERE customer_id = '$customer_id'
  			AND status = 1
			";
		$query = $this->db->query($textquery);	
		$row = $query->row();
		if ($row) {
			//set thumbnail
			if($row->thumbnail){
				$row->thumbnail =   "http://" . $_SERVER['SERVER_NAME']. "/media/thumbnail_customer?id=". $row->thumbnail;
			}else{
				$row->thumbnail =   "http://" . $_SERVER['SERVER_NAME']. "/media/thumbnail_customer?id=" . $this->config->item('img_not_found') ;
			}			

			$textquery = "
				SELECT * 
				FROM customer_photo 
				WHERE customer_id ='$customer_id'
			 	";
			$result = $this->db->query($textquery)->result();			
			$photo = array();
			if ($result) {
				
				foreach($result as $item){
					$file_photo = array();
					$file_photo['filename'] = $item->filename;
					$file_photo['thumbnail'] = "http://" .$_SERVER['SERVER_NAME']. "/media/thumbnail_customer?id=". $item->filename;
					$photo[] = $file_photo;
				}
			}
			$row->photo = $photo;

		} 

	return $row;
	}	

	public function signup($email, $mobile_id, $user_id, $password, $first_name, $last_name, $display_name, $status, $apps_id, $device_id, $longitude, $latitude, $mobile_no){	
		$password = md5('mat123' . $password . 'kra');	
		$customer_id = uniqid(rand(), false);
		$textquery = "
			INSERT INTO customer (customer_id, email, mobile_id, user_id, password, first_name, last_name, display_name, status, apps_id, device_id, longitude, latitude, last_login, active_date, mobile_no)
			VALUES ('$customer_id', '$email', '$mobile_id', '$user_id', '$password', '$first_name', '$last_name', '$display_name', $status, '$apps_id', '$device_id', '$longitude', '$latitude', now(), now(), '$mobile_no')
		";
		
		$query = $this->db->query($textquery);
		return $this->detail($customer_id);
	}
	
	
	function signup_create_verify($customer_id){	
		$verify_code = false;

		$textquery = "
			SELECT 
				customer_id, verify_status, verify_code, verify_date
  			FROM customer
  			WHERE customer_id = '$customer_id'
			";
  		$query = $this->db->query($textquery);	
		$customer = $query->row();
		if($customer){
			$verify_code = uniqid(rand(), false);	
			$textquery = "
				UPDATE customer
				SET verify_code = '$verify_code'
				WHERE customer_id='$customer_id'
			 	";
			$this->db->query($textquery);	
			$customer->verify_code = $verify_code;
		}
		return $customer;
	}
	
	function forget_password_create_verify($email){	
		$verify_code = false;

		$textquery = "
			SELECT 
				customer_id, verify_status, verify_code, verify_date
  			FROM customer
  			WHERE email = '$email'
			";
  		$query = $this->db->query($textquery);	
		$customer = $query->row();
		if($customer){
			$verify_code = uniqid(rand(), false);	
			$textquery = "
				UPDATE customer
				SET verify_code = '$verify_code'
				WHERE customer_id='$customer->customer_id'
			 	";
			$this->db->query($textquery);	
			$customer->verify_code = $verify_code;
		}
		return $customer;
	}
	
	public function change_password($customer_id, $password, $new_password){	
		$password = md5('mat123' . $password . 'kra');	
		$new_password = md5('mat123' . $new_password . 'kra');	

		$textquery = "
			SELECT 
				customer_id, email, mobile_id, user_id, first_name, last_name, 
				display_name, status
  			FROM customer
  			WHERE customer_id = '$customer_id'
			AND password = '$password'
			";
  		$query = $this->db->query($textquery);	
		$customer = $query->row();
		if($customer){
			$textquery = "
				UPDATE customer
				SET password = '$new_password'
				WHERE customer_id='$customer_id'
			 	";
			$this->db->query($textquery);	
		}
		return $customer;
	}

  public function upload_photo($customer_id, $filename) {
    $textquery = "Update customer set filename = '".$filename."' where customer_id = '".$customer_id."' ";
    $query = $this->db->query($textquery);


		$textquery = "
			INSERT INTO customer_photo(
				customer_id, filename
			)VALUES(
				'$customer_id', '$filename'
			)
		 	";
		$query = $this->db->query($textquery);	
  }
	
	public function update_photo($customer_id, $filename){	
		$textquery = "
			SELECT 
				customer_id, filename
  			FROM customer
  			WHERE customer_id = '$customer_id'
			";
  	$query = $this->db->query($textquery);	
		$customer = $query->row();
		if($customer){
			$textquery = "
				UPDATE customer
				SET filename = '$filename'
				WHERE customer_id='$customer_id'
			 	";
			$this->db->query($textquery);	
		}
		return $customer;
	}	
	
	
	public function update_profile($customer_id, $display_name, $first_name, $last_name, $email, $mobile_no){	
		$textquery = "
			SELECT 
				customer_id, email, mobile_id, user_id, first_name, last_name, 
				display_name, mobile_no, status
  			FROM customer
  			WHERE customer_id = '$customer_id'
			";
  		$query = $this->db->query($textquery);	
		$customer = $query->row();
		if($customer){
			$textquery = "
				UPDATE customer
				SET display_name = '$display_name', 
					mobile_no = '$mobile_no' 
				WHERE customer_id='$customer_id'
			 	";
			$this->db->query($textquery);	
		}
		return $customer;
	}
	
	
	public function detail($customer_id){
		$textquery = "
			SELECT 
				customer_id, mobile_id, email, user_id, first_name, last_name, 
				display_name, status, mobile_no
  			FROM customer
  			WHERE customer_id = '$customer_id'
			";

  	$query = $this->db->query($textquery);	
		return $query->row();
	}

	
	public function get_account_by_email($email){
		$textquery = "
			SELECT 
				customer_id, email, mobile_id, user_id, first_name, last_name, 
				display_name, status
  			FROM customer
  			WHERE lower(email) = lower('$email')
			";
  		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	
	public function get_subscription_by_msisdn($msisdn, $apps_id){
		$textquery = "
			SELECT 
				msisdn, customer_id, expiry_time, subscription_status, 
				b.product_id, b.product_name, b.product_price,  b.active_days
  		FROM subscription a 
  		LEFT JOIN subscription_product b on a.product_id = b.product_id
  		WHERE lower(msisdn) = lower('$msisdn') AND lower(apps_id) = lower('$apps_id')  
			";
  		$query = $this->db->query($textquery);	
		return $query->row();
	}

	public function get_subscription_by_customer($customer_id, $apps_id){
		$textquery = "
			SELECT 
				msisdn, customer_id, expiry_time, subscription_status, 
				b.product_id, b.product_name, b.product_price,  b.active_days
  		FROM subscription a 
  		LEFT JOIN subscription_product b on a.product_id = b.product_id
  		WHERE lower(customer_id) = lower('$customer_id') AND lower(apps_id) = lower('$apps_id')  
			";
  		$query = $this->db->query($textquery);	
		return $query->row();
	}
	

	public function get_subscription_product(){
		$textquery = "
			SELECT product_id, service_id, product_name, product_price, active_days
  		FROM subscription_product
  		ORDER BY active_days	asc
			";
  		$query = $this->db->query($textquery);	
		return $query->result();
	}

	public function get_subscription_product_by_id($product_id){
		$textquery = "
			SELECT product_id, service_id, product_name, product_price, active_days, amount
  		FROM subscription_product
  		WHERE product_id ='$product_id'
  		ORDER BY active_days	asc
			";
  		$query = $this->db->query($textquery);	
		return $query->row();
	}


	public function update_subscription($msisdn, $apps_id, $product_id, $status){

		$textquery = "
			SELECT 
				product_id, active_days
  		FROM subscription_product
  		WHERE lower(product_id) = lower('$product_id')
			";
  	$query = $this->db->query($textquery);	
		$product = $query->row();
		$active_days = $product->active_days;		


		$textquery = "
			SELECT 
				subscription_id, msisdn, customer_id, product_id, expiry_time, subscription_status 
  			FROM subscription
  			WHERE lower(msisdn) = lower('$msisdn') 
  				AND lower(apps_id) = lower('$apps_id')
			";
  	$query = $this->db->query($textquery);	
		$subscription = $query->row();
		
		
		if($subscription){
			// INSERT HISTORY
			$textquery = "
				INSERT INTO subscription_history (subscription_id, apps_id, customer_id, product_id, subscription_status, expiry_time, created) 
				SELECT subscription_id, apps_id, customer_id, product_id, subscription_status, expiry_time, created
  			FROM subscription
  			WHERE subscription_id = '". $subscription->subscription_id ."' 
			";
			$this->db->query($textquery);	
			$expire_date = 	" date_add(now(), INTERVAL ". $active_days ."  DAY) ";
			if ($status=="0") $expire_date =" null ";
			
			$is_expired = false;
			$expired = $subscription->expiry_time;

			if (!empty($expired)) {
				$timestamp = time();
				$expired = strtotime($expired);
				$diff = $timestamp - $expired;
				$expired_days = round($diff / 86400);

				if ($timestamp > $expired) {
					$is_expired= true;
				}								
			}			
			
			if ($subscription->product_id != $product_id) {
				// UPDATE
				$textquery = "
					UPDATE subscription
					SET subscription_status = '$status' ,
							product_id = 	'$product_id' ,
							expiry_time = $expire_date
					WHERE lower(msisdn) = lower('$msisdn')
						AND lower(apps_id) = lower('$apps_id')
				 	";
				$this->db->query($textquery);	
			} else {
				if ($subscription->subscription_status != $status) {
					// UPDATE
					$textquery = "
						UPDATE subscription
						SET subscription_status = '$status' ,
									product_id = 	'$product_id' ,
									expiry_time = $expire_date,
									created = now()
						WHERE lower(msisdn) = lower('$msisdn')
							AND lower(apps_id) = lower('$apps_id')
					 	";
					$this->db->query($textquery);						
				} else {
					if ($is_expired) {					
						// UPDATE
						$textquery = "
							UPDATE subscription
							SET subscription_status = '$status' ,
									product_id = 	'$product_id' ,
									expiry_time = $expire_date,
									created = now()
							WHERE lower(msisdn) = lower('$msisdn')
								AND lower(apps_id) = lower('$apps_id')
						 	";
						$this->db->query($textquery);						
					}					
				}
			}
		} else {
			$subscription_id = uniqid(rand(), false);		
			$textquery = "
				INSERT INTO subscription (subscription_id, apps_id, msisdn, product_id, subscription_status, expiry_time, created)
				VALUES ('$subscription_id', '$apps_id', '$msisdn', '$product_id',  '$status', date_add(now(), INTERVAL $active_days DAY), now())
			";
			$this->db->query($textquery);	
		}
		
		return $subscription;
	}

	public function update_subscription_payment($customer_id, $apps_id, $product_id, $status){

		$textquery = "
			SELECT 
				product_id, active_days
  		FROM subscription_product
  		WHERE lower(product_id) = lower('$product_id')
			";
  	$query = $this->db->query($textquery);	
		$product = $query->row();
		$active_days = $product->active_days;		


		$textquery = "
			SELECT 
				subscription_id, msisdn, customer_id, product_id, expiry_time, subscription_status 
  			FROM subscription
  			WHERE lower(customer_id) = lower('$customer_id') 
  				AND lower(apps_id) = lower('$apps_id')
			";
  	$query = $this->db->query($textquery);	
		$subscription = $query->row();
		
		
		if($subscription){
			// INSERT HISTORY
			$textquery = "
				INSERT INTO subscription_history (subscription_id, apps_id, customer_id, product_id, subscription_status, expiry_time, created) 
				SELECT subscription_id, apps_id, customer_id, product_id, subscription_status, expiry_time, created
  			FROM subscription
  			WHERE subscription_id = '". $subscription->subscription_id ."' 
			";
			$this->db->query($textquery);	
			$expire_date = 	" date_add(now(), INTERVAL ". $active_days ."  DAY) ";
			if ($status=="0") $expire_date =" null ";
			
			$is_expired = false;
			$expired = $subscription->expiry_time;

			if (!empty($expired)) {
				$timestamp = time();
				$expired = strtotime($expired);
				$diff = $timestamp - $expired;
				$expired_days = round($diff / 86400);

				if ($timestamp > $expired) {
					$is_expired= true;
				}								
			}

			if ($subscription->product_id != $product_id) {
				// UPDATE
				$textquery = "
					UPDATE subscription
					SET subscription_status = '$status' ,
							product_id = 	'$product_id' ,
							expiry_time = $expire_date,
							created = now()
					WHERE lower(customer_id) = lower('$customer_id')
						AND lower(apps_id) = lower('$apps_id')
				 	";
				$this->db->query($textquery);	
			} else {
				if ($subscription->subscription_status != $status) {
					// UPDATE
					$textquery = "
						UPDATE subscription
						SET subscription_status = '$status' , created = now()
						WHERE lower(customer_id) = lower('$customer_id')
							AND lower(apps_id) = lower('$apps_id')
					 	";
					$this->db->query($textquery);						
				} else {
					if ($is_expired) {					
						// UPDATE
						$textquery = "
							UPDATE subscription
							SET subscription_status = '$status' ,
									product_id = 	'$product_id' ,
									expiry_time = $expire_date,
									created = now()
							WHERE lower(customer_id) = lower('$customer_id')
								AND lower(apps_id) = lower('$apps_id')
						 	";
						$this->db->query($textquery);						
					}
				} 
			}
		} else {
			$subscription_id = uniqid(rand(), false);		
			$textquery = "
				INSERT INTO subscription (subscription_id, apps_id, customer_id, product_id, subscription_status, expiry_time, created)
				VALUES ('$subscription_id', '$apps_id', '$customer_id', '$product_id',  '$status', date_add(now(), INTERVAL $active_days DAY), now())
			";
			$this->db->query($textquery);	
		}
		
		return $subscription;
	}
	
	public function get_payment_provider($apps_id){
		$textquery = "
			SELECT payment_id, payment_name, payment_code, payment_provider, post_url, post_field, post_option, post_url_unsubscribe, post_field_unsubscribe
  		FROM payment_provider
  		WHERE apps_id = '$apps_id' and status='1'
  		ORDER BY order_number	asc
			";
  		$query = $this->db->query($textquery);	
			$rows = $query->result();
			if ($rows) {
				$this->load->helper('url');
				foreach($rows as $row){				
					if(strpos($row->post_url, '{BASE_URL}') !== false) {
						$base_url = base_url();
						$row->post_url = str_replace("{BASE_URL}",$base_url,$row->post_url);
						$row->post_url_unsubscribe = str_replace("{BASE_URL}",$base_url,$row->post_url_unsubscribe);
					}
				}
			}
			
		return $rows;
	}



	public function customer_history($apps_id, $customer_id){
		$textquery = "
			SELECT 
				mobile_id, apps_id, customer_id, content_id, content_title, res_name, 
				start_date, end_date, (end_date - start_date) as duration,
				channel_id, channel_alias, channel_type_id
			FROM content_activity 
			WHERE apps_id='$apps_id' 
			AND customer_id='$customer_id'
			ORDER BY start_date DESC
			LIMIT 0, 20
			";
  		$query = $this->db->query($textquery);	
		$rows = $query->result();
		return $rows;
	}

	public function transaction_history($apps_id, $customer_id){
		$textquery = "
			SELECT 
				apps_id, customer_id, content_id, transaction_code, product_code, transaction_date, 
				price, mobile_id, channel_id, content_title, channel_alias, channel_type_id 
			FROM d_customer_transaction 
			WHERE apps_id='$apps_id' 
			AND customer_id='$customer_id'
			ORDER BY transaction_date DESC
			LIMIT 0, 20
			";
  		$query = $this->db->query($textquery);	
		$rows = $query->result();
		return $rows;
	}


	public function summary_transaction($apps_id, $customer_id){
		$textquery = "
			SELECT 
				apps_id, customer_id, MONTHNAME(transaction_date) as month, SUM(price) as price
			FROM d_customer_transaction 
			WHERE apps_id='$apps_id' 
			AND customer_id='$customer_id'
			GROUP BY MONTHNAME(transaction_date)
			ORDER BY transaction_date DESC
		";
  		$query = $this->db->query($textquery);	
		$rows = $query->result();
		return $rows;
	}

	public function allow_share($customer_id, $customer_category_id) {
		$textquery = "		
			SELECT 
				a.customer_category_id, category_name, '1' as selected , allow_access, allow_upload, allow_share
			FROM customer_member a, customer_category b 
			WHERE a.customer_id = '$customer_id' 
			AND b.customer_category_id ='$customer_category_id'
			AND a.customer_category_id = b.customer_category_id
			AND allow_share ='1' limit 0,1
		";	
  		$query = $this->db->query($textquery);	
		$rows = $query->row();
		return $rows;

	}
	
	public function group_list($customer_id){
		$textquery = "
		select x.customer_category_id, CONCAT(c.groups_name,'/',category_name)category_name, c.groups_name,( selected)  from (
			SELECT 
				a.customer_category_id, category_name, '1' as selected 
			FROM customer_member a, customer_category b 
			WHERE a.customer_id = '$customer_id'
			AND a.customer_category_id = b.customer_category_id
		union
			Select customer_category_id, category_name, '0' as selected from customer_category
		) x
		left join groups_customer_category   b on x.customer_category_id = b.customer_category_id
		left join groups c on b.groups_id = c.groups_id		
		group by x.customer_category_id, category_name
		order by category_name
		";
  		$query = $this->db->query($textquery);	
		$rows = $query->result();
		return $rows;
	}


	public function remove_mygroups($customer_id, $customer_category_id){
		$textquery = "
			DELETE 
			FROM customer_member 
			WHERE customer_id='$customer_id' 
			AND customer_category_id='$customer_category_id'
		";
  		$query = $this->db->query($textquery);	
	}


	public function add_mygroups($customer_id, $customer_category_id){
		$textquery = "
			INSERT INTO customer_member (customer_id, customer_category_id)
			VALUES ('$customer_id', '$customer_category_id' )
		";
  		$query = $this->db->query($textquery);	
	}


	public function topup($customer_id, $amount){
		$textquery = "
			UPDATE customer
		 	SET credit_balance=$amount
			WHERE customer_id='$customer_id'
		";
  		$query = $this->db->query($textquery);	
	}




}

?>