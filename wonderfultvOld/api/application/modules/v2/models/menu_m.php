<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Zola Octanoviar
	Date: nov, 17 2014 22:20 
*/
class Menu_m extends MY_Model {
	
	protected $table 	= 'menu';
	protected $key		= 'menu_id';

	public function __construct()
	{
		parent::__construct();
	}
	
	public function menu_list($apps_id, $device_id, $menu_id='0',$menu_type='menu'){
		if ($menu_type =='menu') {
			//$order_by = ($device_id == '1') ?  " a.order_number " : " a.title " ;
			$textquery = "
				SELECT 
						b.product_id, a.apps_id, a.menu_id, a.parent_menu_id, 'menu' menu_type,  a.title, a.thumbnail, a.link , a.tab_type, a.show_tab_menu, a.show_dd_menu, 
						a.active, count(b.menu_id) count_product, c.icon_size, c.poster_size, a.created
				FROM menu a
				LEFT  JOIN menu_product b on a.menu_id = b.menu_id 
				INNER JOIN apps c on a.apps_id = c.apps_id
				WHERE  a.apps_id ='$apps_id' 
					AND parent_menu_id = '$menu_id' 
				GROUP BY a.menu_id
				ORDER BY a.order_number, b.order_number
			 ";
		}  else {
				$order_by = ($device_id == '1') ?  " b.order_number " : " c.alias" ;
				$textquery = "
					SELECT 
						b.product_id, a.apps_id, a.menu_id, a.parent_menu_id,  'channel' menu_type,  c.alias title, c.thumbnail,  c.link , 
						'0' tab_type, '0' show_tab_menu, '0' show_dd_menu, a.active, '1' count_product, d.icon_size, d.poster_size, a.created
					FROM menu a
					INNER JOIN menu_product b on a.menu_id  = b.menu_id 
					INNER JOIN channel c on b.product_id = c.channel_id 
					INNER JOIN apps d on a.apps_id = d.apps_id
					WHERE  a.apps_id ='$apps_id' 
						AND a.menu_id = '$menu_id' 
					ORDER BY a.order_number, b.order_number
				 ";
		}
		$rows = $this->db->query($textquery)->result();	
	  $results    = array();
    foreach($rows as $row) {   
    	$sub_menu = array();
    	if ($menu_type == 'menu') {
				$sub_menu = $this->menu_list($apps_id, $device_id, $row->menu_id, 'menu');    		
				$sub_menu_channel = $this->menu_list($apps_id, $device_id, $row->menu_id, 'channel');    		
				$sub_menu = array_merge($sub_menu, $sub_menu_channel);					
    	}

    	$row->sub_menu_list  = $sub_menu;
	
			$width = $row->icon_size;
			if ($device_id =='4') {
				$width = '50';
			}
    	
			$row->thumbnail = $this->get_thumbnail_url($row->thumbnail) ."&w=".$width;
    	$results[] = $row;
    }        
    return $results;		
	}	

	
}