<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: nov, 17 2014
	Authentication Application
	Original copy from signup functions.
*/

class Product extends REST_Controller{

	public function __construct(){
		parent::__construct();
    $this->load->model(array('product_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
		

		$this->apps_id = $this->input->post("apps_id");
		$this->lang = $this->input->post("lang");
		$this->customer_id = $this->input->post("customer_id");		
		$this->device_id = $this->input->post("device_id");		
				
		// checking params
    if(empty($this->apps_id)) $this->response_failed('missing_app_id');        
    if(empty($this->customer_id)) $this->response_failed('missing_customer_id');        
		if(empty($this->device_id)) $this->response_failed('missing_device_id');        
    if(empty($this->lang)) $this->lang =  'id_ID';        
		
	}	

	/**
	* List of Product
	* method: POST
	* url: /product_list
	* required parameters: apps_id, customer_id, menu_id
	*/
	public function product_list_post(){
		$menu_id = $this->input->post("menu_id");
		$device_id = $this->input->post("device_id");
				
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
		$genre_id = $this->input->post("genre_id") == "" ? "" : $this->input->post("genre_id");
		$crew_id = $this->input->post("crew_id") == "" ? "" : $this->input->post("crew_id");
		$search_key = $this->input->post("search_key") == "" ? "" : $this->input->post("search_key");

		$product_list = $this->product_m->product_list($page, $limit, $this->apps_id, $this->device_id, $menu_id, $genre_id, $crew_id, $search_key);
		$response = array(
			"product_list" => $product_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang,
				"menu_id" => $menu_id,
				"genre_id" => $genre_id,
				"crew_id" => $crew_id,
				"search_key" => $search_key	
			)			
    );
		$this->response_success($response);
	}
	
	
	public function product_detail_post(){
    $menu_id = $this->input->post("menu_id");
    $product_id = $this->input->post("product_id");

		$product = $this->product_m->product_detail($this->apps_id, $menu_id, $product_id);

		if($product && $product->trailer_id){
			$product->trailer = $this->endpoint_m->product_detail('', '', $product->trailer_id);
		}
		$this->response_success($product);
	}	
	
	public function comment_list_post(){
		$product_id = $this->input->post("product_id");
		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$comment_list = $this->product_m->comment_list($page, $limit, $product_id);
		$response = array(
			"comment_list" => $comment_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"product_id" => $product_id
			)			
    );
		$this->response_success($response);
	}	
	
  public function comment_save_post ()  {
    $product_id = $this->input->post('product_id');
    $customer_id = $this->input->post('customer_id');
    $comment_text = $this->input->post('comment_text');
		$comment_text = addslashes($comment_text);

    $response['comment_list'] = $this->product_m->comment_save($product_id, $customer_id, $comment_text);
		$this->response_success($response);
  }
	
	
	public function like_list_post(){
		$product_id = $this->input->post("product_id");
		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$comment_list = $this->product_m->like_list($page, $limit, $product_id);
		$response = array(
			"like_list" => $comment_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"product_id" => $product_id
			)			
    );
		$this->response_success($response);
	}	
	
  public function like_save_post ()  {
    $product_id = $this->input->post('product_id');
    $customer_id = $this->input->post('customer_id');

    $response['rcode'] = $this->product_m->like_save($product_id, $customer_id);
		if ($response['rcode']  == '0') {
			$response['message']  = 'Already like product';
		} else {
			$response['message']  = 'Thank you';
		}
		$this->response_success($response);
  }
	
	
	public function channel_list_post(){
		$menu_id = $this->input->post("menu_id");
		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
		$search_key = $this->input->post("search_key") == "" ? "" : $this->input->post("search_key");

		$product_list = $this->product_m->channel_list($page, $limit, $this->apps_id, $menu_id, $search_key);
		$response = array(
			"product_list" => $product_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang,
				"menu_id" => $menu_id,
				"search_key" => $search_key	
			)			
    );
		$this->response_success($response);
	}


	public function genre_list_post(){
		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$genre_list = $this->product_m->genre_list($page, $limit);
		$response = array(
			"product_list" => $genre_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang
			)			
    );
		$this->response_success($response);
	}

	public function boxoffice_list_post(){
		$menu_id = $this->input->post("menu_id");
		$device_id = $this->input->post("device_id");
				
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$product_list = $this->product_m->boxoffice_list($page, $limit, $this->apps_id, $this->device_id);
		$response = array(
			"product_list" => $product_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang
			)			
    );
		$this->response_success($response);
	}


}


