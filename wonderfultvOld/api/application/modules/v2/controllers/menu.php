<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: nov, 17 2014
	Authentication Application
	Original copy from signup functions.
*/

class Menu extends REST_Controller{

	public function __construct(){
		parent::__construct();
    $this->load->model(array('menu_m'));
		$this->load->helper('url');		
		$this->lang->load('default');

		$this->apps_id = $this->input->post("apps_id");
		$this->lang = $this->input->post("lang");
		$this->customer_id = $this->input->post("customer_id");		
		$this->device_id = $this->input->post("device_id");		
		
		// checking params
    if(empty($this->apps_id)) $this->response_failed('missing_app_id');        
    if(empty($this->lang)) $this->lang =  $this->config->item('lang');        
    if(empty($this->customer_id)) $this->response_failed('missing_customer_id');        
    if(empty($this->device_id)) $this->response_failed('missing_device_id');        

	}	

	/**
	* List of Menu
	* method: POST
	* url: /menu_list
	* required parameters: apps_id, customer_id, version
	*/
	public function menu_list_post(){
		$menu_id = $this->input->post("menu_id");
		if(empty($menu_id))  $menu_id = '0';

		$menu_list = $this->menu_m->menu_list($this->apps_id,$this->device_id,  $menu_id,  'menu');
		$response = array(
			"menu_list" => $menu_list
    );
		$this->response_success($response);
	}
	
	
	
}