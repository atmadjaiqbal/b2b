<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: nov, 17 2014
	Authentication Application
	Original copy from signup functions.
*/

class Apps extends REST_Controller{

	public function __construct(){
		parent::__construct();
    $this->load->model(array('apps_m', 'menu_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
	}	

	/**
	* Authorize application base on customer 
	* method: POST
	* url: /auth
	* required parameters: apps_id, customer_id, version
	*/
	public function auth_post(){
    $apps_id = $this->input->post("apps_id");
		$lang = $this->input->post("lang");
    $version = $this->input->post("version");
    $customer_id = $this->input->post("customer_id");
    $latitude = $this->input->post("latitude");
    $longitude = $this->input->post("longitude");
    $device_id = $this->input->post("device_id");
    $screen_size = $this->input->post("screen_size");
    $remote_host = (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

		if(empty($latitude))	$latitude = 0;
		if(empty($longitude))	$longitude = 0;


		// checking application id
    if(empty($apps_id)) $this->response_failed(lang('missing_app_id'));        
    if(empty($version)) $this->response_failed(lang('missing_app_version'));        
    if(empty($customer_id)) $this->response_failed(lang('missing_customer_id'));        

    $app = $this->apps_m->auth($apps_id, $device_id, $version, $customer_id);
    if(!$app) $this->response_failed(lang('auth_error_app_id'));
    
		if (empty($longitude))  $longitude= '0';		
		if (empty($latitude))  $latitude= '0';	
	  
	  $mobile_id = $this->input->post("mobile_id");
		if($mobile_id == ""){
			$mobile_id = substr(str_shuffle(MD5(microtime())), 0, 10);
			$this->apps_m->apps_mobile_id_save($apps_id, $version, $mobile_id, $device_id, $screen_size, $remote_host, $latitude, $longitude);
		}	

		$response = array_merge((array)$app, array(
			//'message' => lang('auth_ok'),
			'menu_list' => $this->menu_m->menu_list($apps_id, $device_id, '0',  'menu'),
			'category_list' => $this->apps_m->apps_category_list($apps_id, $customer_id, $lang),
			'schedule_type_list' => $this->apps_m->apps_schedule_type_list($apps_id, $lang),
			'feature_list' => $this->apps_m->apps_feature_list($apps_id),
			'mobile_id' => $mobile_id
			)
		);
		
		$this->response_success($response);

	}

}
