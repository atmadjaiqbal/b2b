<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 13 2014
	Customer groups controllers
*/

class Customer extends REST_Controller{
	
	public function __construct(){
		parent::__construct();
        $this->load->model(array('apps_m', 'customer_group_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
	}	
	
	/*
	* Get customer group based on Application ID
	* method: GET
	* url: /workgroups/$apps_id
	* argument: apps_id
	*/
    public function workgroups_get($app_id = FALSE){
    	if(empty($app_id)) $this->response_failed(lang('missing_app_id'));
        $app = $this->apps_m->find($app_id);
        if(!$app) $this->response_failed(lang('error_app_id'));        
    	$groups = $this->customer_group_m->get_groups($app_id);
    	$items = array();
    	if($groups){
    		foreach ($groups as $group) {
    			$items[] = array(
    				'apps_id' => $group->apps_id,
    				'workgroup_id' => $group->groups_id,
    				'workgroup_name' => $group->groups_name,
    			);
    		}
    	}    	
    	$data = array(
    		'items' => $items,
    		'count_items' => count($groups)
    	);
    	$this->response_success($data);
    }

	/*
	* Get group categories customer (they called groups) based on Application ID
	* method: GET
	* url: /groups/$apps_id/$workgroup_id
	* argument: apps_id, workgroup_id
	*/
    public function groups_get($app_id = FALSE, $workgroup_id = FALSE){
    	if(empty($app_id)) $this->response_failed(lang('missing_app_id'));
        $app = $this->apps_m->find($app_id);
        if(!$app) $this->response_failed(lang('error_app_id'));        
    	$categories = $this->customer_group_m->get_categories_groups($app_id, $workgroup_id);
    	$data = array(
    		'items' => $categories,
    		'count_items' => count($categories)
    	);
    	$this->response_success($data);
    }

}