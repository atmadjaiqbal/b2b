<?php


Class Digital_Contents extends Front_Controller {
	function __construct()
	{		
		parent::__construct();
		$this->load->model('digital_product_model');
		$this->lang->load('product');
	}	

	public function preview($id = false){
		if($this->input->get('base_url')){
			$data['base_url'] = $this->input->get('base_url');	
		}		
		$template = 'partials/digital_content_preview';		
		if($this->input->get('template')){
			$template = $this->input->get('template');	
		}
		$product = $this->Product_model->get_product($id);		
		if($product){
			if($product->images == 'false'){
				$product->images = array();
			}else{								
				$product->images = array_values((array)json_decode($product->images));
				// add to get images from digital content
				if($product->product_type == 'non-package'){
					$product->images = array();
					$file_list = $this->Digital_Product_model->get_associations_by_product($product->id);
					if($file_list && count($file_list) > 0){				
						$f = $this->Digital_Product_model->get_digital_content($file_list[0]->file_id);
						$fClass = new stdClass();
						$fClass->filename = $f->video_thumbnail;
						$fClass->primary = "true"; 
						$fClass->imgpath ="remote";
						$product->images = array($fClass);
					}
				}
			}
			$data['product'] = $product;
			$digital_products = $this->digital_product_model->get_associations_by_product($product->id);
			if(isset($product->product_type) && $product->product_type == 'non-package'){
				$content_id = ($digital_products && count($digital_products) > 0) ? $digital_products[0]->file_id : $product->sku;
				$item = $this->restAPI_get(					
					config_item('api_metadata_content'), array('content_id' => $content_id), false
				);
				if($this->input->get('simple')){
					$template = 'partials/product_info_simple';
					$data['simple'] = true;
					unset($item->genre);
					unset($item->crew);
				}
				if($item){					
					if($item->content_type == 'movie' || $item->content_type == 'seri'){
						$this->parseVideoRelated($item);
					}					
					$data['item'] = $item;
				}else{
					$item = new stdClass();
					$product->name = '<span style="text-decoration:line-through">'.$product->name.'</span>';
					$item->category_name = '<span class="site-color">The requested content could not be found.</span>';
					$data['item'] = $item;
				}				

				$this->load->view($template, $data);
				// $this->rest->debug();
			}else{
				
			}
		}else{
			echo '<h3 style="text-align:center;">'.lang('error_not_found').'</h3>';
		}
	}

	private function parseVideoRelated(&$item){
		if($item->related){
			foreach($item->related as $relate){
				$relate->slug = $this->Product_model->get_slug($relate->content_id, true);				
			}
		}
		// var_dump($item->related);exit;
	}



}