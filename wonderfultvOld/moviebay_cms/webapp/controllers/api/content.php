<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @package		Content Data
 * @subpackage	Rest Server
 * @category	Controller
 * @author		htridianto@visikreatif.com
 * @link		https://www.facebook.com/htridianto
*/

class Content extends API_Controller
{    

    function __construct(){
        parent::__construct();
        $this->load->model(array('Routes_model', 'Category_model', 'Product_model', 'Digital_Product_model', 'Social_model'));
        $this->load->helper(array('date'));
    }

    public function index_get(){        
        $public_methods = array();
        foreach (get_class_methods($this) as $key => $value) {            
            if(stripos($value, '_') > 0 && (stripos($value, '_get') > 3 || stripos($value, '_post') > 3)){
                $public_methods[] = $value;
            }
        }
    	$output = array(
    		"rcode" => "OK",
    		"api_version" => "1.0",
    		"class_name" => get_class($this),
            "methods" => $public_methods
    	);        
      	$this->response($output, 200);
    }

    /**
    *	API notification content changes
    *	$action: create, update, delete
    */
    public function event_get($action = "nothing"){
    	$content_id = $this->get("content_id");
        if($content_id){
            if($action == 'save' && $content_id){
                self::_save_product($content_id);
            }        
        }

      	$this->response_success("Event {nothings} notification received, we'll take an action for '{$content_id}'");
    }

    private function _save_product($digital_content_id = false){
        if(!$digital_content_id){
            $this->response_failed("Invalid parameter digital_content_id!");
        }
        $digital_product = $this->Digital_Product_model->get($digital_content_id);
        if(!$digital_product){
            $this->response_failed("Digital Content not found!");
        }

        $product_categories = array();
        $channels = $this->Category_model->Digital_Product_model->get_content_channels($digital_content_id);
        if($channels){
            foreach ($channels as $channel) {
                if($channel->product_category_id){
                    $product_categories[] = $channel->product_category_id;
                }
            }            
        }
        if(empty($product_categories)){
            $this->response_failed("Unknown Product Category!<br />\nPlease check relation Product Category with iBolz Channel.");
        }      

        // $category = $this->Category_model->get_category_by_channel($digital_product->content_category_id);
        // if($category){
        //     $product_categories[] = $category->id;            
        // }else{
        //     $channels = $this->Category_model->Digital_Product_model->get_content_channels($digital_content_id);
        //     $this->response_failed("Unknown Product Category!<br />\nPlease check relation Product Category with iBolz Channel. ".json_encode($channels));
        // }

        // if($category){
            // $product_categories[] = $category->id;
            // if($category->parent_id && $category->parent_id != 0){
            //     $product_categories[] = $category->parent_id;
            // }
        // }else{
            // $product_categories[] = $this->config->item('default_category_id');
        // }        

        $slug  = url_title(convert_accented_characters($digital_product->title), 'dash', TRUE);        
        $slug   = $this->Routes_model->validate_slug($slug);                
        $route['slug']  = $slug;    
        $route_id   = $this->Routes_model->save($route);

        $data['sku']                = $digital_product->id;
        $data['name']               = $digital_product->title;
        $data['description']        = $digital_product->description;
        if($digital_product->active){
            $data['enabled']            = $digital_product->active;
        }        
        $data['slug']               = $slug;
        $data['route_id']           = $route_id;
        $data['product_type']       = $this->config->item('default_product_type');
        $data['price']              = $this->config->item('default_product_price');
        $data['expired']            = $this->config->item('default_expired_date');


        $image_id = explode('.', $digital_product->video_thumbnail)[0];
        $product_images = array(
            "{$image_id}" => array(
                "filename" => $digital_product->video_thumbnail,
                "primary" => true,
                "imgpath" => "remote",                
                "alt" => "",
                "caption" => ""                    
            )
        );
        $data['images']  = json_encode($product_images);
        
        // save product 
        $options = array();
        $product_id = $this->Product_model->save($data, $options, $product_categories);        
        
        // file associations
        $this->Digital_Product_model->disassociate(false, $product_id);// clear existsing
        $this->Digital_Product_model->associate($digital_product->id, $product_id); // add file associations

        //save the route
        $route['id']    = $route_id;
        $route['slug']  = $slug;
        $route['route'] = 'cart/product/'.$product_id;            
        $this->Routes_model->save($route);            

        $data['message'] = "The product '{$digital_content_id}' has been saved";
        $this->response_success($data);
    }

    /* comment  */
    public function comment_post($action = 'nothing'){
        $id = $this->post('comment_id');        
        if($action == 'save'){
            $content_id = $this->post('content_id');
            $customer_id = $this->post('customer_id');
            $comment_text = $this->post('comment_text');
            if(!$content_id || empty($content_id) || 
                !$customer_id || empty($customer_id) || 
                !$comment_text|| empty($comment_text))
            {
                $this->response_failed("Invalid parameters passed!");
            }
            $data = array(
                'content_id' => $content_id,
                'customer_id' => $customer_id,
                'comment_text' => $comment_text
            );
            if($id){
                $this->Social_model->update($id, $data);
            }else{                
                $data['comment_id'] = random_string('alnum',20);
                $data['created'] = date('Y-m-d H:i:s', now());
                $this->Social_model->insert($data);
            }            
            $data['count_comments'] = $this->Social_model->count_by('content_id', $content_id);
            $data['message'] = "Comment has been saved, count of comments is ".$data['count_comments'];
            $this->response_success($data);
        }else if($action == 'remove'){
            if(!$id || empty($id) ){
                $this->response_failed("Invalid parameters comment_id!");
            }
            $comment = $this->Social_model->get_by('comment_id', $id);
            if(!$comment){
                $this->response_failed("Comment not found!");
            }
            
            $this->Social_model->delete_by('comment_id', $id);
            $data['count_comments'] = $this->Social_model->count_by('content_id', $comment->content_id);
            $data['message'] = "Comment has been removed, count of comments is ".$data['count_comments'];
            $this->response_success($data);
        }
        $this->response_success("Nothing action in comment function");
    }


}