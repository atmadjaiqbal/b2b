<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @package		Commons Data
 * @subpackage	Rest Server
 * @category	Controller
 * @author		htridianto@visikreatif.com
 * @link		https://www.facebook.com/htridianto
*/

class Test extends API_Controller
{    

    function __construct(){
        parent::__construct();
    }

    function index_get(){        
    	$config = array(
    		"rcode" => "OK",
    		"api_version" => "1.0",
    		"text_termcondition" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    	);        
      	$this->response($config, 200);
    }
}