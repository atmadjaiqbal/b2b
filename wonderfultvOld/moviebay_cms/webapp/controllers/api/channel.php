<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @package		Channel Data
 * @subpackage	Rest Server
 * @category	Controller
 * @author		htridianto@visikreatif.com
 * @link		https://www.facebook.com/htridianto
*/

class Channel extends API_Controller
{    

    function __construct(){
        parent::__construct();
        $this->load->model(array('Routes_model', 'Category_model', 'Product_model', 'Digital_Product_model'));
    }

    public function index_get(){       
		$public_methods = array();
        foreach (get_class_methods($this) as $key => $value) {            
            if(stripos($value, '_') > 0 && (stripos($value, '_get') > 3 || stripos($value, '_post') > 3)){
                $public_methods[] = $value;
            }
        }     
    	$output = array(
    		"rcode" => "OK",
    		"api_version" => "1.0",
    		"class_name" => get_class($this),
    		"methods" => $public_methods
    	);        
      	$this->response($output, 200);
    }

    /**
    *	API notification content changes
    *	$action: create, update, delete
    */
    public function event_get($action = "nothing"){
    	$id = $this->get("id");
        if($action == 'save'){
	    	$name = $this->get("name");
	    	$parent_id = $this->get("parent_id");        	
            self::_save_category($id, $name, $parent_id, $this->get());
        }else if($action == 'remove'){
            self::_remove_category($id);
        }        
      	$this->response_success("Event {$action} notification received, we'll take an action for channel '{$id}'");
    }

    private function _save_category($ibolz_channel_id, $name, $parent_id = false, $params = array()){
    	$save = array(
    		'name' => $name,
    		'ibolz_channel_id' => $ibolz_channel_id,
    		'sequence' => 0, 'enabled' => 1, 'display_on_menu' => 1
    	);
        if(isset($params['sequence'])) $save['sequence'] = intval($params['sequence']);
        if(isset($params['display_on_menu'])) $save['display_on_menu'] = intval($params['display_on_menu']);

    	$slug  = url_title(convert_accented_characters($name), 'dash', TRUE);
        $category = $this->Category_model->get_category_by_channel($ibolz_channel_id);
        if($category){ // is update mode
            $slug   = $this->Routes_model->validate_slug($slug, $category->route_id);
            $route = array(
            	'id' => $category->route_id,
            	'slug'  => $slug
        	);
        	$save['id'] = $category->id;
        }else{
            $slug   = $this->Routes_model->validate_slug($slug);
            $route['slug']  = $slug;        
        }		
		if(!empty($slug)) {
        	$route_id = $this->Routes_model->save($route); 
        	$save['route_id'] = intval($route_id);
        	$save['slug'] = $slug;
        }                   
        if($parent_id){        	
        	$category = $this->Category_model->get_category_by_channel($parent_id);
        	if($category){
        		$save['parent_id'] = $category->id;
        	}
        }        
        $category_id = $this->Category_model->save($save);        

        if($route_id) {
        	$route['id']    = $route_id;
        	$route['slug']  = $slug;
        	$route['route'] = $save['route'] = 'cart/category/'.$category_id.'';
        	$route_id = $this->Routes_model->save($route); 
        }
        $save['message'] = 'The category has been saved';
        $this->response_success($save);
    }

    private function _remove_category($ibolz_channel_id = false){
    	if($ibolz_channel_id){
    		$category = $this->Category_model->get_category_by_channel($ibolz_channel_id);
    		if ($category){
	            $this->Routes_model->delete($category->route_id);
	            $this->Category_model->delete($category->id);
	            return $this->response_success('The category has been deleted');
	        }
    	}
    }
}