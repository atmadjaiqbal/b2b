<?php


Class Digital_Products extends Admin_Controller {

	function __construct()
	{
		parent::__construct();
		$this->lang->load('digital_product');
		$this->load->model('digital_product_model');
	}
	
	public function index()
	{
		self::page();
	}


	private function set_query_params(&$data = null){
		$search_key = $this->input->get('search_key');
		if($search_key){
			if($data) $data['search_key'] = $search_key;
			$this->digital_product_model
				->like('id', $search_key)
				->or_like('title', $search_key)
				->or_like('description', $search_key);
		}
	}

	public function page(){
		$data['page_title'] = lang('dgtl_pr_header');
		self::set_query_params($data);
		
		$total_rows = $this->digital_product_model->count_by();		
		// Create pagination links
		$data['pagination'] = $pagination = create_pagination($this->config->item('admin_folder').'/digital_products/page', $total_rows);		
		self::set_query_params($data);
		$data['file_list'] = $this->digital_product_model							
							->limit($pagination['limit'], $pagination['offset'])
							->get_digital_contents();
		
		if($this->input->is_ajax_request()){
            echo json_encode($data['file_list']);			
		}else{
			$this->view($this->config->item('admin_folder').'/digital_products', $data);
		}						
	}	


	public function ajax_search(){
		if($this->input->get('search_key')){
			self::set_query_params();
		}
		if($this->input->get('without_digital_products')){
			$this->digital_product_model->where_not_in('id', $this->input->get('without_digital_products'));
		}
		$file_list = $this->digital_product_model							
							->limit(config_item('records_per_page'), 0)
							->get_digital_contents();		
		echo json_encode($file_list);
	}	

	// function index()
	// {
	// 	$data['page_title'] = lang('dgtl_pr_header');
	// 	$data['file_list']	= $this->digital_product_model->get_list();
		
	// 	$this->view($this->config->item('admin_folder').'/digital_products', $data);
	// }
	
	public function form($id=0){
		$this->load->helper('form_helper');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$data	= array(	 
			'id' =>'',
			'filename' =>'',
			'max_downloads'	=>'',
			'title' =>'',
			'size' =>''
		);
		if($id){
			$data= array_merge($data, (array)$this->digital_product_model->get_file_info($id));
		}
		
		$data['page_title']		= lang('digital_products_form');		
		$this->form_validation->set_rules('max_downloads', 'lang:max_downloads', 'numeric');
		$this->form_validation->set_rules('title', 'lang:title', 'trim|required');
		
		if ($this->form_validation->run() == FALSE){
			$this->view($this->config->item('admin_folder').'/digital_product_form', $data);
		} else {					
			if($id==0){
				$data['file_name'] = false;
				$data['error']	= false;
				
				$config['allowed_types'] = '*';
				$config['upload_path'] = 'uploads/digital_uploads';//$this->config->item('digital_products_path');
				$config['remove_spaces'] = true;
		
				$this->load->library('upload', $config);
				
				if($this->upload->do_upload()){
					$upload_data	= $this->upload->data();
				} else {
					$data['error']	= $this->upload->display_errors();
					$this->view($this->config->item('admin_folder').'/digital_product_form', $data);
					return;
				}				
				$save['filename']	= $upload_data['file_name'];
				$save['size']		= $upload_data['file_size'];
			} else {
				$save['id']			= $id;
			}
			
			$save['max_downloads']	= set_value('max_downloads');				
			$save['title']			= set_value('title');			
			$this->digital_product_model->save($save);			
			redirect($this->config->item('admin_folder').'/digital_products');
		}
	}
	
	public function delete($id){
		// if (file_exists($this->_path.$dirname)) {
		// 	delete_files($this->_path.$dirname, true);
		// }
		$this->digital_product_model->delete($id);		
		$this->session->set_flashdata('message', lang('message_deleted_file'));
		redirect($this->config->item('admin_folder').'/digital_products');
	}


	public function show_content_info($content_id = false){
		$result = $this->restAPI_get(
			config_item('api_metadata_content'), 
			array('content_id' => $content_id),
			false
		);		
		$this->load->view('admin/digitalcontents/content_info', array(
			'status' => $this->rest->status(),
			'error_string' => $this->rest->error_string(),
			'item' => $result
		));
		// $this->rest->debug();
	}

	public function preview($id = false){
		$show_product = $this->input->get('product');
		$item = $this->digital_product_model->get_digital_content($id);
		$this->load->view('admin/digitalcontents/preview', array(
			'status' => 'OK',
			'show_product' => (!$show_product || $show_product == 'yes') ? true : false,
			'item' => $item
		));
	}	

}