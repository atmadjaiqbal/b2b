<?php

class Categories extends Admin_Controller { 
    
    function __construct()
    {       
        parent::__construct();        
        $this->auth->check_access('Admin', true);
        $this->lang->load('category');
        $this->load->model(array('Category_model', 'Product_model'));
    }
    
    //load relation ibolz category 
    private function initIbolzChannels(&$data){
        $ibolz_channels = array();
        $ibolz_channels_tree = array();
        $result_ibolz_channels = $this->restAPI_get(
            config_item('api_channel_list'), array(), FALSE
        );
        // echo(json_encode($result_ibolz_channels));exit;
        if($result_ibolz_channels){
            $parent_id = 0;
            foreach ($result_ibolz_channels as $json_ibolz_channel) {
                $parent_id = 0;
                $ibolz_channels[$json_ibolz_channel->channel_category_id] = $ibolz_channels_tree[$parent_id][] = array(
                    "id" => $json_ibolz_channel->channel_category_id,
                    "name" => $json_ibolz_channel->category_name,
                    "parent_id" => $parent_id
                );
                if($json_ibolz_channel->channels){
                    $parent_id = $json_ibolz_channel->channel_category_id;
                    foreach ($json_ibolz_channel->channels as $json_ibolz_channel_children) {
                        $ibolz_channels[$json_ibolz_channel_children->channel_id] = $ibolz_channels_tree[$parent_id][] = array(
                            "id" => $json_ibolz_channel_children->channel_id,
                            "name" => $json_ibolz_channel_children->channel_name,
                            "parent_id" => $parent_id
                        );
                    }
                }
            }
        }
        $data['ibolz_channels'] = $ibolz_channels;
        $data['ibolz_channels_tree'] = $ibolz_channels_tree;
    }

    public function index(){
        $data['page_title'] = lang('categories');
        $data['categories'] = $this->Category_model->get_categories_tiered(true);
        $this->initIbolzChannels($data);
        $this->view($this->config->item('admin_folder').'/categories', $data);
    }
    
    public function get_channels(){
        $data = array();
        $this->initIbolzChannels($data);
        echo json_encode($data);
    }

    public function organize($id = false){
        $this->load->helper('form');
        $this->load->helper('formatting');        
        if (!$id){
            $this->session->set_flashdata('error', lang('error_must_select'));
            redirect($this->config->item('admin_folder').'/categories');
        }        
        $data['category'] = $this->Category_model->get_category($id);
        //if the category does not exist, redirect them to the category list with an error
        if (!$data['category']){
            $this->session->set_flashdata('error', lang('error_not_found'));
            redirect($this->config->item('admin_folder').'/categories');
        }
            
        $data['page_title']  = sprintf(lang('organize_category'), $data['category']->name);        
        $data['category_products']  = $this->Category_model->get_category_products_admin($id);        
        $this->view($this->config->item('admin_folder').'/organize_category', $data);
    }
    
    public function process_organization($id){
        $products   = $this->input->post('product');
        $this->Category_model->organize_contents($id, $products);
    }

    public function process_assosiation($id){
        $products = $this->input->post('products');
        if($products){
            foreach ($products as $product_id) {
                $this->Product_model->add_product_to_category($product_id, $id);
            }
        }
        if($this->input->is_ajax_request()){
            $output = array(
                'rcode' =>'OK', 
                'message' => "Products have been added into category '{$id}'<br/>",
                'redirect' => site_url($this->config->item('admin_folder').'/categories/organize/'.$id)
            );
            echo json_encode($output);
        }else{
            $this->session->set_flashdata('message', 'Products have been added into category');
            redirect($this->config->item('admin_folder').'/categories/organize'.$id);
        }
    }

    public function remove_assosiation($id){
        $product_id = $this->input->get('product_id');
        $this->Product_model->remove_product_from_category($product_id, $id);
        if($this->input->is_ajax_request()){
            $output = array(
                'rcode' =>'OK', 
                'message' => "Products have been added into category '{$id}'<br/>",
                'redirect' => site_url($this->config->item('admin_folder').'/categories/organize/'.$id)
            );
            echo json_encode($output);
        }else{
            $this->session->set_flashdata('message', 'Products have been added into category');
            redirect($this->config->item('admin_folder').'/categories/organize/'.$id);
        }        
    }
    
    public function form($id = false){
        
        $config['upload_path']      = 'uploads/images/full';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = $this->config->item('size_limit');
        $config['max_width']        = '1024';
        $config['max_height']       = '768';
        $config['encrypt_name']     = true;
        $this->load->library('upload', $config);
        
        
        $this->category_id  = $id;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $data['categories']     = $this->Category_model->get_categories();
        $data['page_title']     = lang('category_form');
        
        //default values are empty if the customer is new
        $data['id']             = '';
        $data['name']           = '';
        $data['slug']           = '';
        $data['description']    = '';        
        $data['sequence']       = '';
        $data['image']          = '';
        $data['seo_title']      = '';
        $data['meta']           = '';
        $data['parent_id']      = 0;
        $data['enabled']        = '1';
        $data['display_on_menu']    = '1';
        $data['ibolz_channel_id']    = '';
        $data['error']          = '';
        
        //create the photos array for later use
        $data['photos']     = array();
        
        $this->initIbolzChannels($data);

        if ($id){   
            $category       = $this->Category_model->get_category($id);
            //if the category does not exist, redirect them to the category list with an error
            if (!$category){
                $this->session->set_flashdata('error', lang('error_not_found'));
                redirect($this->config->item('admin_folder').'/categories');
            }            
            //helps us with the slug generation
            $this->category_name    = $this->input->post('slug', $category->slug);
            
            //set values to db values
            $data['id']             = $category->id;
            $data['name']           = $category->name;
            $data['slug']           = $category->slug;
            $data['description']    = $category->description;
            // $data['excerpt']        = $category->excerpt;
            $data['sequence']       = $category->sequence;
            $data['parent_id']      = $category->parent_id;
            $data['image']          = $category->image;
            $data['seo_title']      = $category->seo_title;
            $data['meta']           = $category->meta;
            $data['enabled']        = $category->enabled;
            $data['display_on_menu']        = $category->display_on_menu;
            $data['ibolz_channel_id']    = $category->ibolz_channel_id;
            
        }
        
        $this->form_validation->set_rules('name', 'lang:name', 'trim|required|max_length[64]');
        $this->form_validation->set_rules('slug', 'lang:slug', 'trim');
        $this->form_validation->set_rules('description', 'lang:description', 'trim');        
        $this->form_validation->set_rules('sequence', 'lang:sequence', 'trim|integer');
        $this->form_validation->set_rules('parent_id', 'parent_id', 'trim');
        $this->form_validation->set_rules('image', 'lang:image', 'trim');
        $this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
        $this->form_validation->set_rules('meta', 'lang:meta', 'trim');
        $this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');
        $this->form_validation->set_rules('display_on_menu', 'Display on Menu', 'trim|numeric');
        $this->form_validation->set_rules('ibolz_channel_id', 'Ibolz Channels', 'trim');        
        
        
        // validate the form
        if ($this->form_validation->run() == FALSE){
            $this->view($this->config->item('admin_folder').'/category_form', $data);
        }else{
            $uploaded   = $this->upload->do_upload('image');            
            if ($id){
                //delete the original file if another is uploaded
                if($uploaded){                    
                    if($data['image'] != ''){
                        $file = array();
                        $file[] = 'uploads/images/full/'.$data['image'];
                        $file[] = 'uploads/images/medium/'.$data['image'];
                        $file[] = 'uploads/images/small/'.$data['image'];
                        $file[] = 'uploads/images/thumbnails/'.$data['image'];
                        
                        foreach($file as $f){
                            //delete the existing file if needed
                            if(file_exists($f)){
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded){
                $data['error']  = $this->upload->display_errors();
                if($_FILES['image']['error'] != 4){
                    $data['error']  .= $this->upload->display_errors();
                    $this->view($this->config->item('admin_folder').'/category_form', $data);
                    return; //end script here if there is an error
                }
            }else{
                $image          = $this->upload->data();
                $save['image']  = $image['file_name'];
                
                $this->load->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/full/'.$save['image'];
                $config['new_image']    = 'uploads/images/medium/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/medium/'.$save['image'];
                $config['new_image']    = 'uploads/images/small/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                $this->image_lib->initialize($config); 
                $this->image_lib->resize();
                $this->image_lib->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/small/'.$save['image'];
                $config['new_image']    = 'uploads/images/thumbnails/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                $this->image_lib->initialize($config);  
                $this->image_lib->resize(); 
                $this->image_lib->clear();
            }
            

            //first check the slug field
            $slug = $this->input->post('slug');            
            //if it's empty assign the name field
            if(empty($slug) || $slug==''){
                $slug = $this->input->post('name');
            }            
            $slug   = url_title(convert_accented_characters($slug), 'dash', TRUE);
            
            //validate the slug
            $this->load->model('Routes_model');
            if($id){
                $slug   = $this->Routes_model->validate_slug($slug, $category->route_id);
                $route_id   = $category->route_id;
            }else{
                $slug   = $this->Routes_model->validate_slug($slug);
                $route['slug']  = $slug;    
                $route_id   = $this->Routes_model->save($route);
            }
            
            $save['id']             = $id;
            $save['name']           = $this->input->post('name');
            $save['description']    = $this->input->post('description');
            $save['parent_id']      = intval($this->input->post('parent_id'));
            $save['sequence']       = intval($this->input->post('sequence'));
            $save['seo_title']      = $this->input->post('seo_title');
            $save['meta']           = $this->input->post('meta');
            $save['enabled']        = $this->input->post('enabled');
            $save['display_on_menu']  = $this->input->post('display_on_menu');
            $save['ibolz_channel_id']    = $this->input->post('ibolz_channel_id');
            $save['route_id']       = intval($route_id);
            $save['slug']           = $slug;
            
            $category_id    = $this->Category_model->save($save);
            
            //save the route
            $route['id']    = $route_id;
            $route['slug']  = $slug;
            $route['route'] = 'cart/category/'.$category_id.'';            
            $this->Routes_model->save($route);            

            $this->session->set_flashdata('message', lang('message_category_saved'));            
            //go back to the category list
            redirect($this->config->item('admin_folder').'/categories');
        }
    }

    public function delete($id){        
        $category   = $this->Category_model->get_category($id);
        //if the category does not exist, redirect them to the customer list with an error
        if ($category){
            $this->load->model('Routes_model');
            
            $this->Routes_model->delete($category->route_id);
            $this->Category_model->delete($id);
            if($this->input->is_ajax_request()){
                $output = array(
                    'rcode' =>'OK', 'message' => lang('message_delete_category')
                );
                echo json_encode($output);
            }else{
                $this->session->set_flashdata('message', lang('message_delete_category'));
                redirect($this->config->item('admin_folder').'/categories');
            }            
        }else{
            if($this->input->is_ajax_request()){
                $output = array(
                    'rcode' =>'FAILED', 'message' => lang('error_not_found')
                );
                echo json_encode($output);
            }else{
                $this->session->set_flashdata('error', lang('error_not_found'));
            }
        }
    }
}