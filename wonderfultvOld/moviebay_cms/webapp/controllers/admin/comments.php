<?php
class Comments extends Admin_Controller
{

	function __construct(){
		parent::__construct();
		$this->auth->check_access('Admin', true);
		//load the admin language file in
		$this->load->model(array('social_model', 'customer_model'));
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');
	}

	public function index(){
		$data['page_title']	= 'Comments';
		$filter = array();
		if($this->input->post('term')){
			$filter['search_key'] = $this->input->post('term');
		}
		
		// count data
		$total_rows = $this->social_model->count_comments($filter);
		
		// create pagination
		$pagination = create_pagination(config_item('admin_folder').'/comments/index/', $total_rows);		
		$data['pagination'] = $pagination;

		// get data based on criteria
		$filter['limit'] = $pagination['limit'];
		$filter['offset'] = $pagination['offset'];
		$data['items'] = $this->social_model->get_comments($filter);

		$this->view($this->config->item('admin_folder').'/comments', $data);
	}

	public function form($id = false){
		$data = array(
			'page_title' => ($id) ? 'Update Comment' : 'Add Comment',
			'id' => '',
			'content_id' => '',
			'customer_id' => '',
			'comment_text' => '',
		);
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');		
		$this->form_validation->set_rules('content_id', 'Content', 'trim|required');
		$this->form_validation->set_rules('customer_id', 'Customer', 'trim|required');
		$this->form_validation->set_rules('comment_text', 'Comment', 'trim|required');

		if($id){
			$item = $this->social_model->get_comment($id);
			if (!$item){
				$this->session->set_flashdata('message', 'Comment not found');
				redirect($this->config->item('admin_folder').'/comments');
			}
			$data['id']	= $item->comment_id;
			$data['content_id'] = $item->content_id;
			$data['customer_id'] = $item->customer_id;
			$data['comment_text'] = $item->comment_text;
		}
		
		if ($this->form_validation->run() == FALSE){
			$data['customers'] = $this->customer_model->get_customers();
			$this->view(config_item('admin_folder').'/comment_form', $data);
		}else{
			// $save['id']		= $id;
			// $save['firstname']	= $this->input->post('firstname');
			// $save['lastname']	= $this->input->post('lastname');
			// $save['email']		= $this->input->post('email');
			// $save['username']	= $this->input->post('username');
			// $save['access']		= $this->input->post('access');			
			// if ($this->input->post('password') != '' || !$id){
			// 	$save['password']	= $this->input->post('password');
			// }			
			// $this->auth->save($save);
			$this->session->set_flashdata('message', 'Comment has been saved');			
			//go back to the customer list
			redirect(config_item('admin_folder').'/comments');
		}		
	}


	public function deletes(){		
		$check_items = $this->input->post('comment_id');
		if($check_items){
			foreach ($check_items as $id) {
				$this->social_model->delete_by('comment_id', $id);
			}
		}		

		if($this->input->is_ajax_request()){
			$output = array(
				'items_deleted' => $check_items,
				'redirect' => site_url(config_item("admin_folder").'/comments')
			);
			echo json_encode($output);
		}else{
			$this->session->set_flashdata('message', 'Comments has been removed');			
			redirect(config_item('admin_folder').'/comments');
		}		
	}	
}