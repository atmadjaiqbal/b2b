<?php
	
class Banners {
	
	var $CI;
	
	function __construct()
	{
		$this->CI =& get_instance();
		
		$this->CI->load->model(array('banner_model'));
	}
	
	function show_collection($banner_collection_id, $quantity=5, $theme='default')
	{
		$data['id']			= $banner_collection_id;
		$data['banners']	= $this->CI->banner_model->banner_collection_banners($banner_collection_id, true, $quantity);
		$this->CI->load->view('banners/'.$theme, $data);
	}

	private function getProducts($categories)
	{
		if(!empty($categories)){
			$product_collections = $this->CI->Product_model->get_products($categories, $this->CI->config->item('records_per_page'), 0);
			foreach ($product_collections as $key => &$p){
				$p->images	= (array)json_decode($p->images);

				// $p->images	= (array)json_decode($p->images);
				// add to get images from digital content
				if($p->product_type == 'non-package'){
					$p->file_list = $this->CI->Digital_Product_model->get_associations_by_product($p->id);
					if($p->file_list && count($p->file_list) > 0){
						$f = $this->CI->Digital_Product_model->get_digital_content($p->file_list[0]->file_id);
						if($f){
							$fClass = new stdClass();
							$fClass->filename = $f->video_thumbnail;
							$fClass->primary = "true"; 
							$fClass->imgpath ="remote";
							$p->images = array();
							$p->images[$f->id] = $fClass;
						}
					}
				}


			}		
			return $product_collections;		
		}
		return false;
	}

	function show_products($category_name, $product_collections = false, $theme='default_products', $slug = false)
	{
		if(!$product_collections){
			$category	= $this->CI->Category_model->get_by('slug', $slug); // ex:is tv-series
			if($category){
				$sub_categories	= $this->CI->Category_model->get_categories($category->id); // ex:is tv-series
				if($sub_categories){
					$categories = array();
					foreach ($sub_categories as $sub_category) {
						$categories[] = $sub_category->id;
					}
					$product_collections = $this->getProducts($categories);
				}else{
					$product_collections = $this->getProducts($category->id);
				}
			}
		}
		$data['category_name']	= $category_name;
		$data['products']	= $product_collections;

		$base_url	= $this->CI->uri->segment_array();
		$data['base_url'] = implode('/', $base_url);

		$data['slug'] = $slug;
		$this->CI->load->view('banners/'.$theme, $data);
	}	
	
}