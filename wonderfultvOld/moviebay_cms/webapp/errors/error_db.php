<html>
<head>
<title>Database Error</title>
<link rel="stylesheet" href="<?php echo base_url('assets/css/error-page.css'); ?>" />
<style type="text/css">
</style>
</head>
	<body class="error-page">
		<div id="error-header">
			<h1>Oops!</h1>
		</div>
		<div id="error-wrapper">	
			<div id="error-code"><?php echo $heading; ?></div>
			<div id="error-message">
				<h3>Sorry, an error happened because <?php echo $heading; ?>!</h3>				
				<?php if(isset($_SESSION['userdata']) && isset($_SESSION['userdata']['admin'])  && isset($_SESSION['userdata']['admin']['access']) == 'Developer'): ?>
					<p><?php echo $message; ?></p>
				<?php else: ?>
					<h3>Please call your Developer!</h3>
				<?php endif; ?>
			</div>			
			<div id="error-actions">
				<button type="button" onclick="history.back(-1);" style="font-size:14px;">BACK</button>
			</div>	
			<br />
		</div>
    </body>
</html>