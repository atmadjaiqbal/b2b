/*	Table OF Contents
	==========================
	Carousel
	Customs Script [List View  Grid View + Add to Wishlist Click Event + Others ]
	Custom Parallax 
	Custom Scrollbar
	Custom animated.css effect
	Equal height ( subcategory thumb)
	responsive fix
	*/
var app = {
    init: function(){
        // $('.preview_product').on('click', function(ev){
        //     ev.preventDefault();
        //     $("html, body").animate({ scrollTop: 0 }, "slow");
        //     app.preview_product($(this).data('id'));
        // });
    },
    preview_product: function(container_id, content_id, params, callback){
        $(container_id).addClass('bg-loader');
        $.get('/digital_contents/preview/'+content_id, params, function(result){
            $(container_id).html(result);
            $(container_id).removeClass('bg-loader');
            if(callback) callback();
        });
    }
}   


$(document).ready(function() {
    /*==================================
    Navigation
    ====================================*/
    $("#cmdAsideMenu, #btnHideAsideMenu, .navbar-toggle-aside-menu").click(function(){
        if($("#asideMenu").is(":visible")){
            $("#asideMenu").hide();
            $("body").removeClass("aside-menu-in");
        }else{
            $("body").addClass("aside-menu-in");
            $("#asideMenu .nav").html($('.navbar .navbar-collapse').html());
            $("#asideMenu .nav ul").removeClass();          
            $("#asideMenu").show();
        }
        return false;   
    });     

    /*==================================
	Carousel
	====================================*/
    if($('.owl-carousel').length > 0){
        $('.owl-carousel').each(function(idx, slider){
            var item_count = $(slider).data('items');
            if(item_count == undefined) item_count = 5;
            var newcontent_slider = $(slider).owlCarousel({
                navigation: true,
                pagination: false,
                items: item_count,
                itemsDesktop: [1280,5],
                itemsDesktopMedium: [1199,5],
                itemsDesktopSmall: [979,4],
                itemsTablet: [768, 3],
                itemsMobile: [479, 2],
		navigationText: '',
                //navigationText: [
                  //"<i class='fa fa-2x fa-chevron-left'></i>",
                  //"<i class='fa fa-2x fa-chevron-right'></i>"
                //],
            });
        })
    }


    $('.search-box button').on('click', function(e) {
        $('.search-full').addClass("active"); //you can list several class names         
        e.preventDefault();
    });
    $('.search-close').on('click', function(e) {
        $('.search-full').removeClass("active"); //you can list several class names 
        e.preventDefault();
    });


    /*==================================
	Customs Script
	====================================*/
    // collapse according add  active class
    // $('.collapseWill').on('click', function(e) {
    //     $(this).toggleClass("pressed"); //you can list several class names 
    //     e.preventDefault();
    // });


    // Customs tree menu script	
    // $(".dropdown-tree-a").click(function() { //use a class, since your ID gets mangled
        // $(this).parent('.dropdown-tree').toggleClass("open-tree active"); //add the class to the clicked element
    // });
	
    
    // List view and Grid view 
   //  $('.list-view').click(function(e) { //use a class, since your ID gets mangled
   //      e.preventDefault();
   //      $('.item').addClass("list-view"); //add the class to the clicked element
		 // $('.add-fav').attr("data-placement",$(this).attr("left"));
   //  });

   //  $('.grid-view').click(function(e) { //use a class, since your ID gets mangled
   //      e.preventDefault();
   //      $('.item').removeClass("list-view"); //add the class to the clicked element
   //  });



    // if (/IEMobile/i.test(navigator.userAgent)) {
    //     // Detect windows phone//..
    //     $('.navbar-brand').addClass('windowsphone');
    // }



    // top navbar IE & Mobile Device fix
    // var isMobile = function() {
    //     //console.log("Navigator: " + navigator.userAgent);
    //     return /(iphone|ipod|ipad|android|blackberry|windows ce|palm|symbian)/i.test(navigator.userAgent);
    // };



    /*==================================
	Parallax  
	====================================*/
    // if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
    //     // Detect ios User // 
    //     $('.parallax-section').addClass('isios');
    //     $('.navbar-header').addClass('isios');
    // }

    // if (/Android|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    //     // Detect Android User // 
    //     $('.parallax-section').addClass('isandroid');
    // }

    // if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    //     // Detect Mobile User // No parallax
    //     $('.parallax-section').addClass('ismobile');
    //     $('.parallaximg').addClass('ismobile');

    // } else {
    //     // All Desktop 
    //     $(window).bind('scroll', function(e) {
    //         parallaxScroll();
    //     });
    //     function parallaxScroll() {
    //         var scrolledY = $(window).scrollTop();
    //         $('.parallaximg').css('marginTop', '' + ((scrolledY * 0.3)) + 'px');
    //     }

    //     if ($(window).width() < 600) {} else {
    //         $('.parallax-image-aboutus').parallax("50%", 0, 0.2, true);
    //     }
    // }



    /*==================================
	 Custom Scrollbar for Dropdown Cart 
	====================================*/
    // $(".scroll-pane").mCustomScrollbar({
    //     advanced: {
    //         updateOnContentResize: true
    //     },
    //     scrollButtons: {
    //         enable: false
    //     },
    //     mouseWheelPixels: "200",
    //     theme: "dark-2"
    // });

    // $(".smoothscroll").mCustomScrollbar({
    //     advanced: {
    //         updateOnContentResize: true

    //     },
    //     scrollButtons: {
    //         enable: false
    //     },
    //     mouseWheelPixels: "100",
    //     theme: "dark-2"
    // });


    /*==================================
	Custom  animated.css effect
	====================================*/
    window.onload = (function() {
        if( !($('.breadcrumbDiv').hasClass('breadcrumb-fixed-top'))){
            $(window).scroll(function() {
                if ($(window).scrollTop() > $(window).height()-195) {
                    $('.breadcrumbDiv').addClass('breadcrumb-fixed-top');
                    // $('.navbar.navbar-fixed-top').addClass('one-edge-shadow-hard');
                    $('.search-box').show();
                    $('.search-full').removeClass("active");
                } else {
                    $('.breadcrumbDiv').removeClass('breadcrumb-fixed-top');
                    // $('.navbar.navbar-fixed-top').removeClass('one-edge-shadow-hard');
                    $('.search-box').hide();
                }
            })
            $('.search-box').hide();
        }else{

        }
    })


    /*==================================
	Global Plugin
	====================================*/
    // For stylish input check box 
    $(function() {
        $("input[type='radio'], input[type='checkbox']").ionCheckRadio();
        $('[rel=tooltip]').tooltip();
        $(".lazy").lazyload();
    });


    // customs select by minimalect
    // $("select").minimalect();

    // bootstrap tooltip 
	// $('.tooltipHere').tooltip('hide')    
}); // end Ready