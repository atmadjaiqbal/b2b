<div class="container main-container headerOffset">  

    <div class="row">
        <div class="col-xs-4">
            
            <div class="row">
                <div class="span4" id="primary-img">
                    <?php
                    $photo  = theme_img('no_picture.png', lang('no_image_available'));
                    $product->images    = array_values($product->images);
                    if(!empty($product->images[0])){
                        $primary    = $product->images[0];
                        foreach($product->images as $photo)
                        {
                            if(isset($photo->primary))
                            {
                                $primary    = $photo;
                            }
                        }
                        // $photo  = '<img class="responsiveImage" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                        $photo  = '<img class="img-responsive col-xs-12" src="'.(($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename).'" />';                            
                    }
                    echo $photo
                    ?>
                </div>
            </div>
            <?php if(!empty($primary->caption)):?>
            <div class="row">
                <div class="span4" id="product_caption">
                    <?php echo $primary->caption;?>
                </div>
            </div>
            <?php endif;?>
            <?php if(count($product->images) > 1):?>
            <div class="row">
                <div class="span4 product-images">
                    <?php foreach($product->images as $image):?>
                    <img class="span1" onclick="$(this).squard('390', $('#primary-img'));" src="<?php echo base_url('uploads/images/medium/'.$image->filename);?>"/>
                    <?php endforeach;?>
                </div>
            </div>
            <?php endif;?>
        </div>
        <div class="col-xs-8 pull-right">
            
            <div class="row">
                <div class="span8">
                    <div class="page-header">
                        <h2 style="font-weight:normal">
                            <?php echo $product->name;?>
                            <span class="pull-right">
                                <span class="product_price"><?php echo format_currency($product->price); ?></span>
                            </span>
                        </h2>
                    </div>
                </div>
            </div>
                    
            <div class="row" style="margin-top:15px; margin-bottom:15px;">
                <div class="span4 sku-pricing">
                    <?php if(!empty($product->sku)):?><div><?php echo lang('sku');?>: <?php echo $product->sku; ?></div><?php endif;?>&nbsp;
                </div>
                <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')):?>
                <div class="span4 out-of-stock">
                    <div><?php echo lang('out_of_stock');?></div>
                </div>
                <?php endif;?>
            </div>
            
            <div class="row">
                <div class="span8">
                    <div class="product-cart-form">
                        <?php echo form_open('cart/add_to_cart', 'class="form-horizontal"');?>
                            <input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
                            <input type="hidden" name="id" value="<?php echo $product->id?>"/>
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label"><?php echo lang('quantity') ?></label>
                                    <div class="controls">
                                        <?php if(!config_item('inventory_enabled') || config_item('allow_os_purchase') || !(bool)$product->track_stock || $product->quantity > 0) : ?>
                                            <?php if(!$product->fixed_quantity) : ?>
                                                <input class="span2" type="text" name="quantity" value=""/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php endif; ?>
                                            <button class="btn btn-primary btn-large" type="submit" value="submit"><i class="icon-shopping-cart icon-white"></i> <?php echo lang('form_add_to_cart');?></button>
                                        <?php endif;?>
                                    </div>
                                </div>                            
                            </fieldset>
                        </form>
                    </div>
        
                </div>
            </div>
            
            <div class="row" style="margin-top:15px;">
                <div class="span8">
                    <?php echo $product->description; ?>
                </div>
            </div>
            
        </div>
        
        <?php if(!empty($product->related_products)):?>
        <div class="related_products">
            <div class="row">
                <div class="span4">
                    <h3 style="margin-top:20px;"><?php echo lang('related_products_title');?></h3>
                    <ul class="thumbnails"> 
                    <?php foreach($product->related_products as $relate):?>
                        <li class="span2 product">
                            <?php
                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                            
                            
                            
                            $relate->images = array_values((array)json_decode($relate->images));
                            
                            if(!empty($relate->images[0]))
                            {
                                $primary    = $relate->images[0];
                                foreach($relate->images as $photo)
                                {
                                    if(isset($photo->primary))
                                    {
                                        $primary    = $photo;
                                    }
                                }

                                $photo  = '<img src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" alt="'.$relate->seo_title.'"/>';
                            }
                            ?>
                            <a class="thumbnail" href="<?php echo site_url($relate->slug); ?>">
                                <?php echo $photo; ?>
                            </a>
                            <h5 style="margin-top:5px;"><a href="<?php echo site_url($relate->slug); ?>"><?php echo $relate->name;?></a>
                            <?php if($this->session->userdata('admin')): ?>
                            <a class="btn" title="<?php echo lang('edit_product'); ?>" href="<?php echo  site_url($this->config->item('admin_folder').'/products/form/'.$relate->id); ?>"><i class="icon-pencil"></i></a>
                            <?php endif; ?>
                            </h5>

                            <div>
                                <?php if($relate->saleprice > 0):?>
                                    <span class="price-slash"><?php echo lang('product_reg');?> <?php echo format_currency($relate->price); ?></span>
                                    <span class="price-sale"><?php echo lang('product_sale');?> <?php echo format_currency($relate->saleprice); ?></span>
                                <?php else: ?>
                                    <span class="price-reg"><?php echo lang('product_price');?> <?php echo format_currency($relate->price); ?></span>
                                <?php endif; ?>
                            </div>
                            <?php if((bool)$relate->track_stock && $relate->quantity < 1 && config_item('inventory_enabled')) { ?>
                                <div class="stock_msg"><?php echo lang('out_of_stock');?></div>
                            <?php } ?>
                        </li>
                    <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
        <?php endif;?>
    </div>

</div>    
<script>
$(function(){ 
    $('.category_container').each(function(){
        $(this).children().equalHeights();
    }); 
});
window.onload = function(){
    $('.product').equalHeights();
}    
</script>
