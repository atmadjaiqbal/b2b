<h3 class="section-title">
	<a href="<?php echo site_url($base_url.'/'.$slug); ?>">	
		<span><?php echo ellipsize($category_name, 100); ?></span> <i class="fa fa-angle-right"></i>
	</a>
</h3>
<?php if(!isset($products) || !$products):?>
    <div class="alert alert-danger">
        <?php echo lang('no_products');?>
    </div>
<?php else: ?>    
	<?php /**
		<JAVASCRIPT>
		Call ajax to preview a product with simple mode (no crews, no genre), 
		after content loaded then the poster (available on input hidden) will be load to poster container
		--------------------------------------------------------------------------------------------------
		RELATED SCRIPT: 
			script.js, digital_content_preview.php, partials/product_info_simple.php
		--------------------------------------------------------------------------------------------------
	*/?>
    <script type="text/javascript">
    	$(document).ready(function(){
    		app.preview_product(	
    			'#content-info-<?php echo $slug; ?>', '<?php echo $products[0]->id; ?>', {simple:true, base_url:$('#breadcrumb_base_url').val()}, 
    			function(){ // is callback     				
					$('#photo-<?php echo $slug; ?> .poster').removeAttr('style').attr('style', 'background-image:url('+$('#photo_<?php echo $products[0]->id; ?>').val()+')' );
					$('#photo-<?php echo $slug; ?>').removeClass('bg-loader');
				}
    		);
    		
    		$('#products-<?php echo $slug; ?> .preview_product').on('click', function(ev){
    			ev.preventDefault();    			
    			$('#photo-<?php echo $slug; ?>').addClass('bg-loader');
    			var self = this;
    			app.preview_product(
    				'#content-info-<?php echo $slug; ?>', $(this).data('id'), {simple: true, base_url:$('#breadcrumb_base_url').val()}, 
    				function(){ // is callback 
    					$('#photo-<?php echo $slug; ?> .poster').removeAttr('style').attr('style', 'background-image:url('+$('#photo_'+$(self).data('id')).val()+')' );
						$('#photo-<?php echo $slug; ?>').removeClass('bg-loader');
					}
    			);
    		});
    	});
    </script>    
	<?php /**
		</JAVASCRIPT>
	*/?>    

<div class="product_panel">
    <div class="col-sm-3 no-padding-left bg-loader" id="photo-<?php echo $slug; ?>"><!-- style="height:480px;"  -->
		<div class="poster bgfull"></div>
	</div>
	<div class="col-sm-9 content-item no-padding-left"> <!-- style="height:480px;" -->
		<div class="row" id="content-info-<?php echo $slug; ?>"></div>
		<div class="row row-item">
		    <div id="<?php echo $slug; ?>" class="content_slider owl-carousel owl-theme" data-x-items="6">
				<?php foreach ($products as $key_index => $product): ?>
					<?php
				        $photo  = theme_img('no_picture.png');
				        $product->images    = array_values($product->images);
				        if(!empty($product->images[0])){
				            $primary    = $product->images[0];
				            foreach($product->images as $photo){
				                if(isset($photo->primary)){
				                    $primary    = $photo;
				                }
				            }
				            $photo  = (($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename);
				        }
				    ?>
				<div class="item col-xs-12">
					<div class="product_title">
						<a href="<?php echo site_url($base_url.'/'.$product->slug); ?>">
							<span class="pull-left"><?php echo ellipsize($product->name, 10); ?></span>
							<span class="pull-right"><i class="fa fa-angle-right"></i></span>
						</a>
						<div class="clearfix"></div>
					</div>
                    <img class="related bgfull preview_product" data-id="<?php echo $product->id; ?>"
                         src="<?php echo $photo; ?>?w=155&h=221" 
                         rel="tooltip" data-placement="right" data-container="body" 
                         data-original-title="<?php echo $product->name.' : '.substr(strip_tags($product->description), 0, 200); ?>" index="1">
                     
				</div>
				<?php endforeach; ?>
		    </div>
	    </div>

	</div>

</div>	
<?php endif; ?>
