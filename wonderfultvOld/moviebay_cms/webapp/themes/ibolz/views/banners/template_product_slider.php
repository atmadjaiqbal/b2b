<h3 class="section-title">
	<a href="<?php echo site_url($base_url.'/'.$slug); ?>">	
		<span><?php echo ellipsize($category_name, 100); ?></span> <i class="fa fa-angle-right"></i>
	</a>
</h3>
<?php if(!isset($products) || !$products):?>
    <div class="alert alert-danger">
        <?php echo lang('no_products');?>
    </div>
<?php else: ?>    
	<?php /**
		<JAVASCRIPT>
		Call ajax to preview a product with simple mode (no crews, no genre), 
		after content loaded then the poster (available on input hidden) will be load to poster container
		--------------------------------------------------------------------------------------------------
		RELATED SCRIPT: 
			script.js, digital_content_preview.php, partials/product_info_simple.php
		--------------------------------------------------------------------------------------------------
	*/?>
    <script type="text/javascript">
    	$(document).ready(function(){
    		app.preview_product(	
    			'#content-info-<?php echo $slug; ?>', '<?php echo $products[0]->id; ?>', {simple:true, base_url:$('#breadcrumb_base_url').val()}, 
    			function(){ // is callback     				
					$('#photo-<?php echo $slug; ?> .poster').removeAttr('style').attr('style', 'background-image:url('+$('#photo_<?php echo $products[0]->id; ?>').val()+')' );
					$('#photo-<?php echo $slug; ?>').removeClass('bg-loader');
				}
    		);
    		$('#products-<?php echo $slug; ?> .preview_product').on('click', function(ev){
    			ev.preventDefault();    			
    			$('#photo-<?php echo $slug; ?>').addClass('bg-loader');
    			var self = this;
    			app.preview_product(
    				'#content-info-<?php echo $slug; ?>', $(this).data('id'), {simple: true, base_url:$('#breadcrumb_base_url').val()}, 
    				function(){ // is callback 
    					$('#photo-<?php echo $slug; ?> .poster').removeAttr('style').attr('style', 'background-image:url('+$('#photo_'+$(self).data('id')).val()+')' );
						$('#photo-<?php echo $slug; ?>').removeClass('bg-loader');
					}
    			);
    		});
    	});
    </script>    
	<?php /**
		</JAVASCRIPT>
	*/?>    

<div class="product_panel">	
	<div class="col-sm-3 no-padding-left bg-loader" style="height:480px;" id="photo-<?php echo $slug; ?>">
		<div class="poster bgfull x-two-edge-shadow-1"></div>
	</div>
	<div class="col-sm-9" style="height:480px;">    	
		<div class="row" id="content-info-<?php echo $slug; ?>"></div>
		<div class="row" style="position:absolute;bottom:-10px;width:100%;">

			<a data-id="<?php echo $slug; ?>" class="prevContent link carousel-nav"> <i class="fa fa-angle-left"></i></a>
			<a data-id="<?php echo $slug; ?>" class="nextContent link carousel-nav"> <i class="fa fa-angle-right"></i></a>

		    <div id="<?php echo $slug; ?>" class="content_slider owl-carousel owl-theme" data-items="6">
				<?php foreach ($products as $key_index => $product): ?>
					<?php
				        $photo  = theme_img('no_picture.png');
				        $product->images    = array_values($product->images);
				        if(!empty($product->images[0])){
				            $primary    = $product->images[0];
				            foreach($product->images as $photo){
				                if(isset($photo->primary)){
				                    $primary    = $photo;
				                }
				            }
				            $photo  = (($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename);
				        }
				    ?>
				<div class="item col-xs-12">
					<div class="product_title">
						<a href="<?php echo site_url($base_url.'/'.$product->slug); ?>">
							<span class="pull-left"><?php echo ellipsize($product->name, 10); ?></span>
							<span class="pull-right"><i class="fa fa-angle-right"></i></span>
						</a>
						<div class="clearfix"></div>
					</div> 
					<div class="related potrait bgfull preview_product" data-id="<?php echo $product->id; ?>" style="background-image:url(<?php echo $photo; ?>);"></div>
					<!-- <img src="<?php echo $photo; ?>" class="preview_product related potrait img-responsive" data-id="<?php echo $product->id; ?>" /> -->
				  	<div class="overlay-bottom">
					  	<div class="hidden-xs hidden-sm"> 
						  	<span class="col-xs-4 text-center"><i class="fa fa-thumbs-o-up"></i><br />0</span>
						  	<span class="col-xs-4 text-center"><i class="fa fa-thumbs-o-down"></i><br />0</span>
						  	<span class="col-xs-4 text-center"><i class="fa fa-comments-o"></i><br />0</span>
					  	</div>
					  	<div class="clearfix"></div>
				  	</div>
				</div>
				<?php endforeach; ?>
		    </div>
	    </div>

	</div>

</div>	
<?php endif; ?>