<h3 class="section-title">
	<span><?php echo ellipsize($category_name, 100); ?> <i class="fa fa-angle-right"></i></span>
</h3>
<div class="col-md-4 no-padding-left" style="height:480px;">
	<img src="http://placehold.it/320x480" class="img-responsive" />
</div>
<div class="col-md-8" style="height:480px;">    	
	<div class="row">
		<h2 class="site-color">sample text</h2>
		<div class="col-md-9 no-padding-left">
			Dia mengalahkan para mafia. Dia menumpas para teroris. 
			Dia mengalahkan para mafia. Dia menumpas para teroris. 
			Dia mengalahkan para mafia. Dia menumpas para teroris. 
			Dia mengalahkan para mafia. Dia menumpas para teroris. 
			Dia mengalahkan para mafia. Dia menumpas para teroris. 
		</div>
		<div class="col-md-3 no-padding">
			<div class="border-title-top-bottom">
				<h3 class="section-title pull-right">IDR 1000.0</h3>
				<div class="clearfix"></div>
			</div>
			<div class="border-title">
				<span class="col-md-4"><i class="fa fa-thumbs-up"></i><br/>200</span>
		  		<span class="col-md-4"><i class="fa fa-thumbs-down"></i><br/>200</span>
		  		<span class="col-md-4"><i class="fa fa-comments"></i><br/>200</span>
		  		<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row" style="position:absolute;bottom:10px;width:100%;">
    	<div class="pull-right">
    		<a data-id="<?php echo $slug; ?>" class="prevContent link carousel-nav"> <i class="fa fa-angle-left"></i></a>
    		<a data-id="<?php echo $slug; ?>" class="nextContent link carousel-nav"> <i class="fa fa-angle-right"></i></a>
    	</div>
    	<div class="clearfix"></div>
	    <div id="<?php echo $slug; ?>" class="content_slider owl-carousel owl-theme">
			<?php for ($i=0; $i < 10; $i++): ?>
			<div class="item">
				<div class="product">
					<a href="#">
			    		<h5 class="pull-left no-padding no-margin">PRODUCT TITLE ...</h5>
			    	</a>
			    	<a class="pull-right padding-link"><i class="fa fa-film"></i></a>
			    	<div class="clearfix"></div>
					<div class="image no-padding col-md-3">
						<img src="http://placehold.it/160x200" class="img-responsive" />						
					</div>
				  <div class="action-control"> 
				  	<span class="col-md-4 box-background"><i class="fa fa-thumbs-up"></i> 200</span>
				  	<span class="col-md-4 box-background"><i class="fa fa-thumbs-down"></i> 200</span>
				  	<span class="col-md-4 box-background"><i class="fa fa-comments"></i> 200</span>
				  </div>
				  <div class="clearfix"></div>
				</div>
			</div>
			<?php endfor; ?>
	    </div><!--/.productslider-->     
    </div>
</div>