<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- 
    Smart developers always View Source.     
    This application was built using GCart (CodeIgiter), an open source framework
    for building rich e-commerce applications.     
// -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo (!empty($seo_title)) ? $seo_title .' - ' : ''; echo $this->config->item('company_name'); ?></title>
	<?php if(isset($meta)):?> 
		<?php echo $meta;?> 
	<?php else:?>
		<meta name="Keywords" content="Shopping Cart, eCommerce, Code Igniter">
		<meta name="Description" content="iBolz vod management powered by GCart ver 2.3">
	<?php endif;?>


	<link href="<?php echo theme_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/font-awesome.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/animate.min.css'); ?>" rel="stylesheet">	
	<link href="<?php echo theme_url('assets/css/style.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/skin-5.css'); ?>" rel="stylesheet">	
	<link href="<?php echo theme_url('assets/css/owl.carousel.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/owl.theme.css'); ?>" rel="stylesheet">	
	<link href="<?php echo theme_url('assets/css/ion.checkRadio.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/ion.checkRadio.cloudy.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/ibolz.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/product-panel.css'); ?>" rel="stylesheet">

	<?php /*		
	<link href="<?php echo theme_url('assets/css/jquery.mCustomScrollbar.css'); ?>" rel="stylesheet">	
	*/ ?>	
		
	<script type="text/javascript" src="<?php echo theme_url('assets/js/jquery.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_url('assets/js/plugins/jquery.blockUI.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/plugins/jquery.lazyload.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_url('assets/js/plugins/bootstrap-dialog.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/plugins/jquery.cycle2.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_url('assets/js/plugins/jquery.easing.1.3.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_url('assets/js/plugins/owl.carousel.min.js'); ?>"></script>  
	<script type="text/javascript" src="<?php echo theme_url('assets/js/plugins/ion.checkRadio.min.js'); ?>"></script> 
	

	<?php
		if(isset($additional_header_info)){
			echo $additional_header_info;
		}
	?>
</head>

<body>	
<section class="wrapper">
	<div class="modal signUpContent fade" id="ModalLogin" tabindex="-1" role="dialog" >
	  <?php $this->load->view('partials/login'); ?>
	</div>
	<div class="navbar-ibolz navbar navbar-fixed-top one-edge-shadow-hard" role="navigation">
		<div class="container">	
	  		<div class="row">
	  			<a href="<?php echo site_url('/'); ?>" class="top-logo pull-left">&nbsp;</a>
				<?php $this->load->view('partials/top-navigation'); ?>
	  		</div>	
		</div>		
	</div> 

	<?php if(isset($homepage)): ?>
	    <?php $this->banners->show_collection(1, 5, 'home_banner');?>
	<?php endif; ?>

	<?php 
		// include breadcrumb template the page consists of 'breadcrumb', 'search-box'
		$this->load->view('partials/breadcrumb');
	?>
