	
<?php 
	//include notification template
	$this->load->view('partials/notification');
?>	
	<div class="container paddTop20">	
	  <div class="row">
	  	<div class="col-md-12 text-center">
			<img src="<?php echo theme_img('partner.png'); ?>" class="img-responsive" style="display:inline" />
		</div>
	  </div>
	</div> 

	<footer>	  
	  <div class="footer-bottom">
	    <div class="container">
	    	<section class="section-block text-center paddTop10">
	          <ul class="social list-inline">
	            <li> <a href="http://facebook.com"> <i class="fa fa-facebook"> &nbsp; </i> </a> </li>
	            <li> <a href="http://twitter.com"> <i class="fa fa-twitter"> &nbsp; </i> </a> </li>
	            <li> <a href="https://plus.google.com"> <i class="fa fa-google-plus"> &nbsp; </i> </a> </li>
	            <li> <a href="http://pinterest.com"> <i class="fa fa-pinterest"> &nbsp; </i> </a> </li>
	            <li> <a href="http://youtube.com"> <i class="fa fa-youtube"> &nbsp; </i> </a> </li>
	          </ul>
	        </section>
	        <section class="section-block text-center">
	    	<?php if( isset($this->pages[0]) ):  ?>
	    		<ul class="list-inline">	    			
					<li style="padding:0px 2px"><a href="<?php echo site_url(); ?>">Home</a></li>
					<?php foreach($this->pages[0] as $menu_page):?>					
					<li style="padding:0px 2px">|</li>
					<li style="padding:0px 2px">
						<?php if(empty($menu_page->content)):?>
							<a href="<?php echo $menu_page->url;?>" <?php if($menu_page->new_window ==1){echo 'target="_blank"';} ?>><?php echo $menu_page->menu_title;?></a>
						<?php else:?>
							<a href="<?php echo site_url($menu_page->slug);?>"><?php echo $menu_page->menu_title;?></a>
						<?php endif;?>
					</li>						
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>	
			</section>
	      	<section class="section-block text-center">
	      		<small>Copyright &copy; <?= config_item('company_name') ?> 2014. All right reserved.</small>
	      	</section>
	    </div>
	  </div>
	  <!--/.footer-bottom--> 
	</footer>
</section>
	<script type="text/javascript" src="<?php echo theme_url('assets/js/script.js'); ?>"></script>
	<script>
	    paceOptions = {
	      elements: true
	    };
	</script>
	<script type="text/javascript" src="<?php echo theme_url('assets/js/plugins/pace.min.js'); ?>"></script>
</body>
</html>