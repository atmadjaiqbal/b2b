<script type="text/javascript">
    $(document).ready(function(){
        $('#detail_product_panel').empty();
        app.preview_product(    
            '#detail_product_panel', '<?php echo $product->id; ?>', 
            {template: 'partials/product_info_advanced', base_url: $('#breadcrumb_base_url').val()}
        );
    });
</script>

<div class="container main-container headerOffset bg-loader" id="detail_product_panel" style="min-height:320px;"> </div>    

<div class="container main-container hide"> 
    <?php echo form_open('cart/add_to_cart', 'id="form_add_to_cart"'); ?>
        <input type="text" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
        <input type="text" name="id" value="<?php echo $product->id?>"/>
        <button class="btn btn-primary btn-large" type="submit" value="submit"><i class="icon-shopping-cart icon-white"></i> <?php echo lang('form_add_to_cart');?></button>
    </form>
</div>    