<div class="container main-container headerOffset">  
<?php if($products): ?>
    <div class="product_panel grid poster" id="content_product_list"> 
        <?php $this->load->view('partials/product_list'); ?>
    </div>        
    <?php if($pagination['current_page'] < $pagination['total_pages']): ?>
        <div class="row padd">
            <div class="load-more-block text-center">
               <a class="btn btn-thin" href="#" id="btnLoadMore"> <i class="fa fa-plus-sign">+</i> load more products</a>
            </div>                                        
        </div>
    <?php endif ?>
<?php else: ?>                
	<div class="alert alert-danger">
		<div class="container">	
			There are currently no available products in this criteria
		</div>
	</div>	
<?php endif; ?>
</div>