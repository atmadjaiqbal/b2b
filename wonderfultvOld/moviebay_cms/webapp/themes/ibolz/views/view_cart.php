<?php if(!$this->input->is_ajax_request()): ?>
    <div class="container main-container headerOffset">  
<?php endif; ?>
<?php if ($this->go_cart->total_items()==0):?>
    <div class="alert alert-info">
        <?php echo lang('empty_view_cart');?>
    </div>
<?php else: ?>
    
    <?php if(!$this->input->is_ajax_request()): ?>
        <div class="page-header">
            <h2><?php echo lang('your_cart');?></h2>
        </div>
    <?php endif; ?>

    <?php echo form_open('cart/update_cart', array('id'=>'update_cart_form')); ?>

    <div class="row">
        <div class="col-md-12">            
            <?php include('checkout/summary.php');?>    
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <?php $payment_types = array('visa', 'mastercard', 'bca', 'mandiri'); ?>
            <div class="form-group">
                <?php foreach ($payment_types as $key => $type): ?>
                <label class="radio-inline">
                  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="<?php echo $type; ?>">
                  <img src="<?php echo theme_url("assets/img/payment/{$type}.png"); ?>" style="border:1px solid #eee;">
                </label>
                <?php endforeach; ?>
            </div>
            <div class="form-group">
                <input type="text" name="card_number" placeholder="Card Number" class="text-center" size="50">
            </div>
            <div class="form-group">
                <input type="text" name="cvv2" placeholder="Expired Date" class="text-center" size="18">
                <input type="text" name="cvv2" placeholder="CVV2" class="text-center" size="30">
            </div>
        </div>
    </div>
            
    <div class="row">
        <div class="col-xs-5 hidden">
            <label class="col-xs-12"><?php echo lang('coupon_label');?></label>
            <input type="text" name="coupon_code" class="col-xs-3" style="margin:0px;">
            <input class="btn" type="submit" value="<?php echo lang('apply_coupon');?>"/>
            
            <?php if($gift_cards_enabled):?>
                <label style="margin-top:15px;"><?php echo lang('gift_card_label');?></label>
                <input type="text" name="gc_code" class="col-xs-3" style="margin:0px;">
                <input class="btn"  type="submit" value="<?php echo lang('apply_gift_card');?>"/>
            <?php endif;?>
        </div>
        <div class="text-center col-xs-12">
            <div class="globalPaddingTop">
                <input id="redirect_path" type="hidden" name="redirect" value=""/>
                <?php if(!$this->Customer_model->is_logged_in(false,false)): ?>
                    <!-- <input class="btn btn-md btn-primary" type="submit" onclick="$('#redirect_path').val('checkout/login');" value="<?php echo lang('login');?>"/>
                    <input class="btn btn-md btn-default" type="submit" onclick="$('#redirect_path').val('checkout/register');" value="<?php echo lang('register_now');?>"/> -->
                    <a class="btn btn-md btn-primary" href="<?php echo site_url('/checkout/login'); ?>"><?php echo lang('login'); ?></a>
                    <a class="btn btn-md btn-default" href="<?php echo site_url('/secure/register'); ?>"><?php echo lang('register_now'); ?> </a>
                <?php endif; ?>
                    <input class="btn hidden" type="submit" value="<?php echo lang('form_update_cart');?>"/>                    
                <?php if ($this->Customer_model->is_logged_in(false,false) || !$this->config->item('require_login')): ?>
                    <!-- <input class="btn btn-md btn-primary col-md-8 col-xs-offset-2" type="submit" onclick="$('#redirect_path').val('checkout');" value="<?php echo lang('form_checkout');?>"/> -->
                    <a id="btnSubmitConfirm" class="btn btn-primary btn-large btn-block" href="<?php echo site_url('checkout/place_order');?>"><?php echo lang('submit_order');?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row">
        <hr class="divider paddTop10" />
        <div class="col-xs-12">
            <ul class="pager pull-left">
                <li class="next"><a href="<?php echo $this->refering_url; ?>"> &larr; Continue shop</a></li>
            </ul>
            <div class="pull-right">
                <a><img src="<?php echo theme_url("/assets/img/doku.png"); ?>"></a>
            </div>
    </div>
</form>
<?php endif; ?>

<?php if(!$this->input->is_ajax_request()): ?>
    </div>
<?php endif; ?>
<script type="text/javascript">
    $(document).ready(function(){
    });
</script>