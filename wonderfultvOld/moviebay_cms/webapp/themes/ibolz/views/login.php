<div class="container main-container headerOffset" id="loginpage">  
	<div class="row">
		<div class="col-xs-6 col-xs-offset-3">
			<?php $this->load->view('partials/login'); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		// $('#loginpage a[data-target=#ModalSignup]').removeAttr('data-target data-toggle').attr('href', '<?php echo site_url('secure/register'); ?>');
		$('.navbar-top a[data-target=#ModalLogin]').removeAttr('data-target data-toggle');
	});
</script>