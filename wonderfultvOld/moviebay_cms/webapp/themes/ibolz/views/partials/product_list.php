<?php
	if(is_array($base_url)) $base_url = implode('/', $base_url);
?>

<div class="row paddTop10">
	<?php foreach ($products as $key_index => $product): ?>
		<?php if($key_index > 0 && $key_index % 6 == 0): ?>
			</div> 
			<div class="row paddTop10">
		<?php endif ?>
		<?php
	        $photo  = theme_img('no_picture.png');
	        $product->images    = array_values($product->images);
	        if(!empty($product->images[0])){
	            $primary    = $product->images[0];
	            foreach($product->images as $photo){
	                if(isset($photo->primary)){
	                    $primary    = $photo;
	                }
	            }
	            $photo  = (($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename);	            
	        }
	    ?>
		<div class="col-xs-6 col-sm-3 col-md-2 item">
			<div class="product_title">				
				<a href="<?php echo site_url($base_url.'/'.$product->slug); ?>">
					<span class="pull-left"><?php echo ellipsize($product->name, 10); ?></span>
					<span class="pull-right"><i class="fa fa-angle-right"></i></span>
				</a>				
				<div class="clearfix"></div>
			</div>
			<?php 
			if($product->slug != 'tvri' && $product->slug != 'mnc-music-channel' &&
			   $product->slug != 'mnc-kids' && $product->slug != 'mnc-entertainment' && 
			   $product->slug != 'mnc-fashion' && $product->slug != 'mnc-infotainment' &&
			   $product->slug != 'mnc-food-travel')
			{   			
			?>
			<a href="<?php echo site_url($base_url.'/'.$product->slug); ?>"> 
		<?php } ?>	
				<?php if(isset($category) && $category->slug == "channel") { ?>						
    	    <div class="bgfull lazy" data-id="<?php echo $product->id; ?>" data-original="<?php echo $photo; ?>"></div>			
  	    <?php } else { ?>
  	    
  	      <img class="related lazy" data-id="<?php echo $product->id; ?>" data-original="<?php echo $photo; ?>?w=155&h=221"
  	      	   rel="tooltip" data-placement="right" data-container="body" 
               data-original-title="<?php echo $product->name.' : '.substr(strip_tags($product->description), 0, 200); ?>" index="1">			
  	    <?php } ?> 
			<?php 
			if($product->slug != 'tvri' && $product->slug != 'mnc-music-channel' &&
			   $product->slug != 'mnc-kids' && $product->slug != 'mnc-entertainment' && 
			   $product->slug != 'mnc-fashion' && $product->slug != 'mnc-infotainment' &&
			   $product->slug != 'mnc-food-travel')
			{   			
			?>  	     
  	  </a>
  	<?php } ?>   
		</div>
	<?php endforeach; ?>
</div> 

<!-- // is hidden pagination -->
<?php echo $pagination['links'] ?>
