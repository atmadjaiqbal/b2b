<?php if(!isset($product) && !$product) :?>
    <div class="alert alert-information">
        The requested product could not be found.
    </div>
<?php else: ?>
	<?php
	    $photo  = theme_img('no_picture.png');
	    $product->images    = array_values($product->images);
	    if(!empty($product->images[0])){
	        $primary = $product->images[0];
	        foreach($product->images as $photo){
	            if(isset($photo->primary)){
	                $primary    = $photo;
	            }
	        }
	        $photo  = (($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename);
	    }
	?>	
	<!-- <div class="col-sm-4 no-padding poster"> -->
		<!-- <img src="<?php echo $photo; ?>" class="img-responsive" /> -->
		<!-- <div class="bgfull x-two-edge-shadow-1" style="background-image:url(<?php echo $photo; ?>);"></div> -->
	<!-- </div> -->
	<div class="col-sm-3 no-padding-left" style="height:480px;">
		<div class="poster bgfull" style="background-image:url(<?php echo $photo; ?>);"></div>
	</div>	
	<div class="col-sm-8">
		<div class="row no-margin-left">
			<?php $this->load->view('partials/product_info_simple'); ?>
		</div>
	</div>
<?php endif ?>    