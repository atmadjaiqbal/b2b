    <div class="row product_panel">        
        <div class="col-xs-4 no-padding-left">
            <div class="box-panel">
                <div class="poster bgfull two-edge-shadow-1" style="background-image:url(http://placehold.it/300x420/EEEEEE/444444/&text=POSTER+300x420);"></div>
                <div class="paddTop20">
                    <h3 class="pull-left">IDR 100000</h3>
                    <a href="#" class="btn btn-primary pull-right">BUY</a>
                    <div class="clearfix"></div>
                </div>                
            </div>
            <div class="box-panel">                
                <h3 class="title">PHOTOS</h3>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-4 no-padding">
                            <img src="http://placehold.it/220x220" class="img-responsive thumb" />
                        </div>
                        <div class="col-xs-4 no-padding">
                            <img src="http://placehold.it/220x220" class="img-responsive thumb" />
                        </div>   
                        <div class="col-xs-4 no-padding">
                            <img src="http://placehold.it/220x220" class="img-responsive thumb" />
                        </div>   
                        <div class="col-xs-4 no-padding">
                            <img src="http://placehold.it/220x220" class="img-responsive thumb" />
                        </div>   
                        <div class="col-xs-4 no-padding">
                            <img src="http://placehold.it/220x220" class="img-responsive thumb" />
                        </div>   
                    </div>   
                </div>   
            </div><!-- /related -->
        </div>
        <div class="col-xs-8 no-padding">
            <div class="box-panel">
                <div class="player">
                    <img src="http://placehold.it/640x360/444444/EEEEEE/&text=PLAYER+640x360" class="img-responsive img-polaroid two-edge-shadow-1" />
                </div>
            </div>
            <div class="box-panel">                
                <div class="pull-right">
                    <a class="prevContent link carousel-nav"> <i class="fa fa-angle-left"></i></a>
                    <a class="nextContent link carousel-nav"> <i class="fa fa-angle-right"></i></a>
                </div>                 
                <h3 class="title">TRAILER</h3>               
                <div class="row">
                    <div class="col-xs-12">
                        <div class="trailer col-xs-4 bgfull" style="background-image:url(http://placehold.it/205x120/444444/EEEEEE/&text=TRAILER+205x120);"></div>
                        <div class="trailer col-xs-4 bgfull" style="background-image:url(http://placehold.it/205x120/444444/EEEEEE/&text=TRAILER+205x120);"></div>
                        <div class="trailer col-xs-4 bgfull" style="background-image:url(http://placehold.it/205x120/444444/EEEEEE/&text=TRAILER+205x120);"></div>
                    </div>
                </div>
            </div><!-- /trailer -->

            <div class="box-panel">                
                <h3 class="title">DESCRIPTION</h3>               
                <h3 class="site-color">VIDEO TITLE</h3> 
                <div class="row">
                    <div class="col-xs-12">
                        <dl class="dl-horizontal">
                            <dt>DIRECTOR</dt><dd>Sabeni</dd>
                            <dt>STARS</dt>
                            <dd>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                                <span class="label label-danger">Start 1</span>
                            </dd>
                            <dt>SYNOPSIS</dt>
                            <dd class="desc">
                                Lorem ipsum dolor sit amet, ei ius fierent detraxit. Tamquam maiestatis eam ex, id qui labitur eruditi molestie, saperet detraxit definitionem est no. Et nec laoreet volutpat, nam ea aliquid quaestio democritum. Pro vidit commodo signiferumque an. Eum clita discere aliquid id. Est in iisque efficiantur, tritani euismod veritus pri an.
                                Ne senserit conclusionemque vim, te elit fuisset scaevola sea. Ius nostro dolorem ancillae id, comprehensam signiferumque ius ut. Sit doming evertitur cu, te sed volutpat consequat. Ad putent veritus pertinax pri, ut deserunt reprehendunt ius, dolor nusquam ei nec. Ea vim sumo dolorem epicuri, per ut doming perpetua efficiantur, sed at atomorum oportere adolescens.
                                Et has nostro adipisci pericula, sed ei vivendum accommodare. Est eu putent omittam, labitur percipit quo an, mea tempor liberavisse ut. An unum torquatos cum, dictas offendit platonem id his. Idque altera probatus ad ius, id nam scriptorem reprehendunt.
                            </dd>
                        </dl>
                    </div>                                    
                </div>
            </div><!-- /description -->

            <div class="box-panel">                
                <div class="pull-right">
                    <a class="prevContent link carousel-nav"> <i class="fa fa-angle-left"></i></a>
                    <a class="nextContent link carousel-nav"> <i class="fa fa-angle-right"></i></a>
                </div>                 
                <h3 class="title">RELATED</h3>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="related col-xs-3 bgfull" style="background-image:url(http://placehold.it/160x240);"></div>
                        <div class="related col-xs-3 bgfull" style="background-image:url(http://placehold.it/160x240);"></div>
                        <div class="related col-xs-3 bgfull" style="background-image:url(http://placehold.it/160x240);"></div>
                        <div class="related col-xs-3 bgfull" style="background-image:url(http://placehold.it/160x240);"></div>
                    </div>   
                </div>   
            </div><!-- /related -->

        </div>
    </div>