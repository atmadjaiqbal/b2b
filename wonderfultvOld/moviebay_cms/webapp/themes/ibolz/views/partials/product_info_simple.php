<?php
	if(isset($simple)){
	    $photo  = theme_img('no_picture.png');
	    $product->images  = array_values($product->images);
	    if(!empty($product->images[0])){
	        $primary = $product->images[0];
	        foreach($product->images as $photo){
	            if(isset($photo->primary)){
	                $primary    = $photo;
	            }
	        }
	        $photo  = (($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename);
	    }
	    echo "<input type='hidden' id='photo_{$product->id}' value='{$photo}' />";
	}
?>	
	<div class="col-sm-8">	
		<a href="<?php echo site_url($base_url.'/'.$product->slug); ?>">
         	<h2 class="site-color"><?php echo $product->name; ?></h2> 
         </a>	
		<div class="row">
            <div class="col-sm-12">
                <dl class="dl-horizontal">
					<!-- <dt>Category</dt><dd><?php echo $item->category_name; ?>&nbsp;</dd> -->
					<?php if(isset($item->prod_year) && $item->prod_year != 0): ?>
						<dt>Year</dt><dd><?php echo $item->prod_year; ?> &nbsp;</dd>
					<?php endif ?>
					<?php if(isset($item->genre) && $item->genre): ?>
						<dt>Genre</dt>
						<dd>
							<?php foreach($item->genre as $genre): ?>
								<a href="#"><?php echo $genre->genre_name; ?></a>
							<?php endforeach ?>
							&nbsp;
						</dd>
					<?php endif ?>

					<?php if(isset($item->crew) && $item->crew): ?>
						<?php foreach($item->crew as $key => $crews): ?>
							<?php if($key != 'director' && $crews): ?>
							<dt><?php echo ucfirst($key); ?></dt>
							<dd>
								<?php foreach($crews as $crew): ?>
									<a href="#"><?php echo $crew->crew_name; ?></a>
								<?php endforeach ?>
								&nbsp;
							</dd>
							<?php endif ?>
						<?php endforeach ?>
					<?php endif ?>			
					<dt>Synopsis</dt>
					  <dd><?php echo ellipsize($product->description, (isset($simple) ? 200 : 500) ); ?> &nbsp;</dd>		
				</dl>
			</div>
		</div>
	</div>
	<div class="col-sm-3 col-sm-offset-1">
		<div class="border-title">
			<div class="col-xs-3 social">
				<i class="fa fa-thumbs-o-up"></i>
				<div><?php echo (isset($item)) ? $item->ulike : '0'; ?></div>
			</div>
	  		<div class="col-xs-3 social">
	  			<i class="fa fa-thumbs-o-down"></i>
	  			<div><?php echo (isset($item)) ? $item->unlike : '0'; ?></div>
	  		</div>
	  		<div class="col-xs-3 social">
	  			<i class="fa fa-comments"></i>
	  			<div><?php echo (isset($item)) ? $item->comment : '0'; ?></div>
	  		</div>
	  		<div class="col-xs-3 social">
	  			<i class="social fa fa-group"></i><div>share</div>
	  		</div>
	  		<div class="clearfix"></div>
		</div>
		<div class="border-title">		
			<?php if($product->price > 0): ?>
				<button type="button" class="btn btn-primary btn-md pull-right">BUY</button>
			<?php else: ?>
				<a href="<?php echo site_url($base_url.'/'.$product->slug); ?>" class="btn btn-success btn-md pull-right">WATCH</a>
			<?php endif; ?>
			<div class="clearfix"></div>
		</div>		
	</div>