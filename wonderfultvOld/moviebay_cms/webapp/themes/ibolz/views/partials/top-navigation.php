<section id="asideMenu" class="aside-menu animated">
    <a href="<?php echo site_url();?>"><h5 class="side-section-title">HOME</h5></a>
    <div class="nav"></div>    
    <br /><br />
</section>
<a class="navbar-toggle navbar-toggle-aside-menu pull-right">
    <i class="fa fa-outdent"></i>                    
</a>

<div class="pull-right hidden-xs">
	<div class="navbar-top pull-right">
		<ul class="userMenu">
			<li> 
	        	<a href="<?php echo site_url('cart/view_cart');?>" title="Cart">
	        		<i class="fa fa-shopping-cart"> </i> 
	        		<span>Cart</span>
	        	</a>					        	
        	</li>		              								
		<?php if($this->Customer_model->is_logged_in(false, false)):?>
			<li>
				<a href="<?php echo site_url('secure');?>" title="<?php echo lang('my_account') ?>">
					<i class="fa fa-user"> </i> 
					<?php echo $this->session->userdata('cart_contents')['customer']['firstname']; ?>
				</a>
			</li>							
			<li>
				<a href="<?php echo site_url('secure/logout');?>" title="<?php echo lang('logout') ?>">
					<i class="fa fa-sign-out"> </i>
					<span>Logout</span> 
				</a>
			</li>
		<?php else: ?>
      		<li> 
      			<a href="#" data-toggle="modal" data-target="#ModalLogin" title="Login">
      				<span>Login</span> 
      			</a> 
      		</li>
      		<li class="hidden-xs"> 
      			<a href="<?php echo site_url('secure/register'); ?>" title="register">
      				<span>Register</span> 
      			</a> 
      		</li>
      	<?php endif ?>
    	</ul>
  	</div>
<!-- </div>		          	
<div class="pull-right"> -->
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
      	<?php if($this->categories): ?>
	        <?php foreach($this->categories[0] as $category_root):?>
	        	<li class="dropdown -megamenu-fullwidth"> 
	        		<a onclick="window.location='<?php echo site_url($category_root->slug);?>'" class="dropdown-toggle" data-toggle="dropdown">
	        			<?php echo $category_root->name;?>
	        			<?php if( isset($this->categories[$category_root->id]) ): ?><?php endif; ?>
	        		</a>
	        		<?php if( isset($this->categories[$category_root->id]) ): ?>
			        <ul class="dropdown-menu -megamenu-dropdown-content pull-right">
	              		<?php foreach($this->categories[$category_root->id] as $category_children):?>
	              			<li><a href="<?php echo site_url($category_root->slug.'/'.$category_children->slug);?>"> <?php echo $category_children->name;?> </a> </li>
	              		<?php endforeach;?>
			        </ul>	   
			        <?php endif; ?>						        
	        	</li>
			<?php endforeach;?>
		<?php else: ?>   
			<li><a>&nbsp;</a></li>
		<?php endif; ?>   
       </ul>
	</div>	
</div>		

<script type="text/javascript">
	$('.userMenu a[title="Cart"]').on('click', function(ev){
		ev.preventDefault();
		BootstrapDialog.show({
			title: '<i class="fa fa-shopping-cart"></i> <?php echo lang("your_cart"); ?>',
			cssClass: 'cart-dialog',
			message: $('<div></div>').load('/cart/view_cart')
		});
	});
</script>

