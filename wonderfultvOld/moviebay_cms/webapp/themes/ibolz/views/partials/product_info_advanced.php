<?php

	$cat = $product->categories;
	
	$vid = $item->content_id;
	$channel_type = 'content';
	$channel_id = $cat[0]->ibolz_channel_id;
	$channel_style = '';
	$logo_height = '525';	
	if ($channel_id == '125929935654092d653330b') {
		$channel_type ='channel';
		$channel_id  = $vid;
		$channel_style = ' style="display:none;" ';
		$logo_height = '200';	
	}



	$photos = array();
    $photo  = theme_img('no_picture.png');
    $product->images    = array_values($product->images);
    if(!empty($product->images[0])){
        $primary = $product->images[0];
        foreach($product->images as $photo){        	
            $photo->filename  = (($photo->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$photo->filename) : config_item('digital_content_thumbnail_path').$photo->filename);            
            // $photos[] = $photo;
            if(isset($photo->primary)){
                $primary = $photo;
            }
        }
        $photo = $primary->filename;
    }
    if(isset($item) && isset($item->photo) && $item->photo){
        foreach ($item->photo as $value) {
            $p = new stdClass();
            $p->filename = $value;
            $photos[] = $p;
        }
    }
    $max_photo = 9;
    $blank_photo = $max_photo - count($photos);
  
    echo "<input type='hidden' id='photo_{$product->id}' value='{$photo}' />";    
?>
<div class="product_panel">        
    <div class="col-xs-4 no-padding-left">
        <div class="box-panel" id="<?php echo $item->category_name; ?>">
            <div class="poster bgfull x-two-edge-shadow-1" style="background-image:url(<?php echo $photo; ?>);min-height:<?php echo $logo_height;?>px;"></div>
            <div class="paddTop20">
                <?php if($product->price > 0): ?>
                    <h3 class="pull-left"><?php echo format_currency($product->price); ?></h3>
                <?php endif; ?>
                 <?php if($product->price > 0): ?>
                    <button id="btn_add_to_cart" type="button" class="btn btn-primary btn-md pull-right col-xs-3">BUY</button>
                <?php else: ?>
                    <!-- <a class="btn btn-success btn-md pull-right">WATCH</a> -->
                <?php endif; ?>
                <div class="clearfix"></div>
            </div>
        </div>
        <?php if($photos): ?>
        <div class="x-box-panel">
            <h3 class="title">PHOTOS</h3>
            <div class="row">
                <div class="col-xs-12">
                	<?php foreach($photos as $thumb): ?>                	
	                    <div class="col-xs-4 no-padding" > 
	                        <?php if(isset($thumb->filename)): ?>                 			
	                			<img src="<?php echo $thumb->filename; ?>" class="img-responsive thumb" onError="$(this).attr('src','<?php echo theme_url('assets/img/photo-bg.gif'); ?>');this.onError = null; return false;" />
		                	<?php else: ?>
								<img src="<?php echo theme_url('assets/img/photo-bg.gif'); ?>" class="img-responsive thumb" style="border:1px solid #AAAAAA;"/> 
		                	<?php endif ?>
	                    </div>
                	<?php endforeach ?>
									<?php if ($blank_photo > 0) :?>
										<?php for ($i = 1; $i <= $blank_photo; $i++) : ?>
	                  <div class="col-xs-4 no-padding" >
	                		<img src="<?php echo theme_url('assets/img/photo-bg.gif'); ?>" class="img-responsive thumb" style="border:1px solid #AAAAAA;width:128px;"/> 
	                  </div>
	                	<?php endfor ?>                  
                	<?php endif ?>



                </div>   
            </div>   
        </div><!-- /photos -->
        <?php endif ?>
        <?php if($item->category_name == 'Channel') : ?>
	        <div class="box-panel">
	            <h3 class="title">SCHEDULE</h3>
	            <div class="row">
	            	<div class="col-xs-12">
                 	<?php 
                  
                switch($product->name)
                {
                	case 'RCTI' :
                
                 	$sched = array( 
                 	    1 => array ('jam' => '00:00', 'acara' => 'TVM : AKANKAH BUNDA DATANG KE PERNIKAHANKU'),
                 	    2 => array ('jam' => '00:30', 'acara' => 'DELIK'),
                      3 => array ('jam' => '01:00', 'acara' => 'LIGA BBVA VILLAREAL VS REAL MADRID'),
                      4 => array ('jam' => '03:00', 'acara' => 'YUSRA & YUMNA'),
                      5 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHEM	DRAMA & MOVIES'),
                      6 => array ('jam' => '04:30', 'acara'	=> 'SEPUTAR INDONESIA PAGI	NEWS & SPORT'),
                      7 => array ('jam' => '06:00', 'acara' => 'GO SPOT	ENTERTAINMENT'),
                      8 => array ('jam' => '08:00', 'acara' => 'INTENS	ENTERTAINMENT'),
                      9 => array ('jam' => '09:00', 'acara' => 'DAHSYAT	ENTERTAINMENT'),
                      10 => array ('jam' => '11:00', 'acara' => 'SILET	ENTERTAINMENT'),
                      11 => array ('jam' => '11:30', 'acara' =>	'SEPUTAR INDONESIA SIANG	NEWS & SPORT'),
                      12 => array ('jam' => '12:00', 'acara' => 'FROM PIZZA WITH LOVE'),
                      13 => array ('jam' => '13:45', 'acara' => 'ITEUNG, I LOVE U FULL'),
                      14 => array ('jam' => '15:30', 'acara' => 'CRISPY'),
                      15 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA	NEWS & SPORT'),
                      16 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA	DRAMA & MOVIES'),
                      17 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES	DRAMA & MOVIES'),
                      18 => array ('jam' => '20:00', 'acara' => 'Catatan Hati Seorang Istri	DRAMA & MOVIES'),
                      19 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK	DRAMA & MOVIES'),
                      20 => array ('jam' => '23:00', 'acara' => 'MEGA SINEMA : CINTAKU DITANGAN HANSIP')
                  );  
                  break;
                  
                  case 'MNC TV' :
                  
                  $sched = array(
                      1 => array ('jam' => '01:15', 'acara' => 'CERITA DINI HARI'),
                      2 => array ('jam' => '02:00', 'acara' => 'KACA BENGGALA'),
                      3 => array ('jam' => '03:30', 'acara' => 'DISNEY JUNIOR: CHIP n DALE RESCUE RANGER'),
                      4 => array ('jam' => '04:00', 'acara' => 'LINTAS PAGI'),
                      5 => array ('jam' => '04:30', 'acara' => 'PELESIR'),
                      6 => array ('jam' => '05:00', 'acara' => 'SIRAMAN QALBU'),
                      7 => array ('jam' => '06:00', 'acara' => 'UPIN & IPIN THE MOVIE'),
                      8 => array ('jam' => '07:00', 'acara' => 'ADIT & SOPO JARWO THE MOVIE'),
                      9 => array ('jam' => '08:30', 'acara' => 'GREBEK NUSANTARA'),
                      10 => array ('jam' => '09:30', 'acara' => 'JENDELA'),
                      11 => array ('jam' => '10:00', 'acara' =>    'MATA PANCING'),
                      12 => array ('jam' => '10:30', 'acara' => 'ANIMASI SPESIAL'),
                      13 => array ('jam' => '12:30', 'acara' => 'FILM TV PLATINUM'),
                      14 => array ('jam' => '14:30', 'acara' => 'MNCTV FESTIVAL BANDUNG 2'),
                      15 => array ('jam' => '16:30', 'acara' => 'INSPIRASI SORE'),
                      16 => array ('jam' => '17:00', 'acara' => 'ANIMASI SPESIAL-1'),
                      17 => array ('jam' => '18:00', 'acara' => 'DI SINI ADA TUYUL'),
                      18 => array ('jam' => '19:00', 'acara' => 'MANUSIA HARIMAU'),
                      19 => array ('jam' => '20:15', 'acara' => 'RADEN KIAN SANTANG'),
                      20 => array ('jam' => '23:45', 'acara' => 'CERITA PILIHAN'),                  
                  );
                  break;
                  
                  default :
                 	$sched = array( 
                 	    1 => array ('jam' => '00:00', 'acara' => 'TVM : AKANKAH BUNDA DATANG KE PERNIKAHANKU'),
                 	    2 => array ('jam' => '00:30', 'acara' => 'DELIK'),
                      3 => array ('jam' => '01:00', 'acara' => 'LIGA BBVA VILLAREAL VS REAL MADRID'),
                      4 => array ('jam' => '03:00', 'acara' => 'YUSRA & YUMNA'),
                      5 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHEM	DRAMA & MOVIES'),
                      6 => array ('jam' => '04:30', 'acara'	=> 'SEPUTAR INDONESIA PAGI	NEWS & SPORT'),
                      7 => array ('jam' => '06:00', 'acara' => 'GO SPOT	ENTERTAINMENT'),
                      8 => array ('jam' => '08:00', 'acara' => 'INTENS	ENTERTAINMENT'),
                      9 => array ('jam' => '09:00', 'acara' => 'DAHSYAT	ENTERTAINMENT'),
                      10 => array ('jam' => '11:00', 'acara' => 'SILET	ENTERTAINMENT'),
                      11 => array ('jam' => '11:30', 'acara' =>	'SEPUTAR INDONESIA SIANG	NEWS & SPORT'),
                      12 => array ('jam' => '12:00', 'acara' => 'FROM PIZZA WITH LOVE'),
                      13 => array ('jam' => '13:45', 'acara' => 'ITEUNG, I LOVE U FULL'),
                      14 => array ('jam' => '15:30', 'acara' => 'CRISPY'),
                      15 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA	NEWS & SPORT'),
                      16 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA	DRAMA & MOVIES'),
                      17 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES	DRAMA & MOVIES'),
                      18 => array ('jam' => '20:00', 'acara' => 'Catatan Hati Seorang Istri	DRAMA & MOVIES'),
                      19 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK	DRAMA & MOVIES'),
                      20 => array ('jam' => '23:00', 'acara' => 'MEGA SINEMA : CINTAKU DITANGAN HANSIP')
                  );  
                  break;
                                    
                }    
                 	?>
	            			            		
									<table class="schedule">
									  <?php if($item->related) { ?>
		            		<?php foreach($item->related as $relate): ?>		            				            		
											<tr><td style="width:60px;" align="left"><?php echo $relate->start_time; ?></td>
												  <td><?php echo $relate->title ; ?></td></tr>
		                <?php endforeach ?>
                  <?php } else { ?>
	              		<?php foreach($sched as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
		                <?php endforeach ?>
                  <?php } ?>
			                
									</table>

		                </div>   
	                </div>   
	        </div><!-- /related -->
        <?php endif ?>        
    </div>
    <div class="col-xs-8 no-padding">
        <div class="box-panel">
            <!-- <div class="player"> -->
                <!-- <img src="http://placehold.it/800x420/444444/EEEEEE/&text=PLAYER+800x420" class="img-responsive img-polaroid two-edge-shadow-1" /> -->
            <!-- </div> -->
						<?php 
						$player_wrap_width="800";
						$player_width="800";
						$player_height="420";
						

						?>
            <iframe class="player bg-loader" scrolling="no" style="min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;" src="http://moviebay2.ibolz.tv/assets/flash/moviebay/index.php?w=<?php echo $player_width;?>&h=<?php echo $player_height;?>&apps_id=com.balepoint.ibolz.moviebay&version=1.0.6&type=<?php echo $channel_type; ?>&channel_id=<?php echo $channel_id; ?>&vid=<?php echo $vid; ?>&id=<?php echo $vid; ?>"></iframe>        
        </div>
        
        <?php if($item->category_name == 'Channel'): ?>

	        <div class="box-panel" id="<?php echo $item->category_name; ?>">
	            <h3 class="title">DESCRIPTION</h3>               
	            <div class="row">
	                <div class="col-xs-12" style="font-family: 'Harbara Mais', Helvetica, Arial; font-size: 14px; color: #888888; text-align: justify; ">	                	
	                	<?php echo nl2br($item->description); ?>
                </div>
              </div>
          </div>      	        
        
	        <div class="box-panel" id="<?php echo $item->category_name; ?>">
	            <h3 class="title">FOTO</h3>               
	            <div class="row">
	                <div class="col-xs-12">
											<!-- ?php for ($i = 1; $i <= 4 ; $i++) : ? -->
											
											<?php 
											switch($product->name)
											{
												case 'RCTI' :
											?>	
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('https://lh5.googleusercontent.com/-Ki0TPo28JL0/AAAAAAAAAAI/AAAAAAAAAAo/uLz1-kkC5ro/photo.jpg');"></div>
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-xpa1/t1.0-9/10440851_10154239694780422_1225546969160377806_n.jpg');"></div>
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('http://widikidiw.heck.in/files/bastian-stell-ini-pilihan.jpg');"></div>
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('http://www.ratingtop.com/pics/place/440308/dashyat_rcti.jpg');"></div>
                      <?php
                         break;
                         
                         case 'MNC TV' :
                      ?>   
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-xpa1/v/t1.0-9/10660352_588405597930045_209225010071304197_n.png?oh=9faefc69efc8fd03e4616880a23d6903&oe=54885FBA&__gda__=1422332039_4291fa0e55391edb59dd7d6fbd7ccd00)"></div>
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('http://intl.rakuten-static.com/t/2b081290-bb85-11e2-91e6-005056bd2fd8/20130513/ac6002ee-5e93-420a-8c2b-1bbd1241d9b4.jpg');"></div>
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('http://rahman-putra.mywapblog.com/files/ost-juna-cinta-juni.jpg');"></div>
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('http://www.mnctv.com/images/stories/program/Foto-Pemain-Pemeran-Raden-Kian-Santang-MNCTV-2013.jpg');"></div>
                      <?php    
                          break;
                         
                         case 'Global TV' :
                      ?>   
                      	<div class="toprating col-xs-4 bgfull" style="background-image:url('http://www.globaltv.co.id/program/getImage/programs/541.jpg');"></div>
                        <div class="toprating col-xs-4 bgfull" style="background-image:url('http://www.globaltv.co.id/program/getImage/programs/2346.jpg');"></div>
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('http://www.globaltv.co.id/program/getImage/programs/1473.jpg');"></div>
		                    <div class="toprating col-xs-4 bgfull" style="background-image:url('http://www.globaltv.co.id/program/getImage/programs/2302.jpg');"></div>
                      <?php    
                          break;
                         
                         default :
                      ?>                             
                                               
											  <?php for ($i = 1; $i <= 4 ; $i++) : ?>
		                      <div class="toprating col-xs-4 bgfull" style="background-image:url(<?php echo theme_url('assets/img/icon-trailer-2x.png'); ?>);"></div>
		                	  <?php endfor ?>                                                             
		                	<?php 
		                	    break;   
												
											}
											?>
             
	                </div>
	            </div>
	        </div>    
            
        <?php endif ?>
        
        <?php if(isset($item->related) && $item->related && $item->category_name != 'Channel'): ?>        
          <div class="box-panel">                
            <h3 class="title">DESCRIPTION</h3>
            <h3 class="site-color"><?php echo $product->name; ?></h3> 
            <div class="row">
                <div class="col-xs-12">
                    <dl class="dl-horizontal">
                    	<dt>SKU</dt><dd><strong><?php echo $product->sku; ?></strong>&nbsp;</dd>                    	
                    	<dt>Category</dt><dd><?php echo $item->category_name; ?>&nbsp;</dd>      
                    	<?php if(isset($item->prod_year) && $item->prod_year > 0): ?>
                    		<dt>Year</dt><dd><?php echo $item->prod_year; ?>&nbsp;</dd>
                    	<?php endif ?>
						<dt>Genre</dt>
						<dd>
							<?php if(isset($item->genre) && $item->genre): ?>
								<?php foreach($item->genre as $genre): ?>
									<?php echo $genre->genre_name; ?>
								<?php endforeach ?>
							<?php endif ?>
							&nbsp;
						</dd>

						<?php if(isset($item->crew) && $item->crew): ?>
							<?php foreach($item->crew as $key => $crews): ?>
								<dt><?php echo ucfirst($key); ?></dt>
								<dd>
									<?php foreach($crews as $crew): ?>
										<?php echo $crew->crew_name; ?>
									<?php endforeach ?>
									&nbsp;
								</dd>
							<?php endforeach ?>
						<?php endif ?>

                        <dt>SYNOPSIS</dt><dd class="desc"><?php echo nl2br($product->description); ?>&nbsp;</dd>
                    </dl>
                </div>                                    
            </div>
          </div><!-- /description -->        
        <?php endif ?>                
        
        
        <?php if($item->category_name != 'Channel'): ?>
	        <?php if(isset($item->trailer_id) && $item->trailer_id): ?>
	        <div class="box-panel" <?php echo $channel_style;?>>
	            <h3 class="title">TRAILER</h3>               
	            <div class="row">
	                <div class="col-xs-12">
						        <?php if(!empty($item->trailer)) : ?>
						        		<?php 
						        		$trailer  = $item->trailer;
						        		$trailer_image = config_item('digital_content_thumbnail_path').$trailer->video_thumbnail ;            
												?>							        
		                    <div class="trailer col-xs-4 bgfull" >
		                    	<a href="http://moviebay1.ibolz.tv/movies/trailer/<?php echo $trailer->slug;?>">
		                    		<img src="<?php echo $trailer_image ; ?>" width="135" >
		                    	</a>	 

		                    </div>
										<?php endif ?>
	                </div>
	            </div>
	        </div><!-- /trailer -->
					<?php else : ?>
	        <div class="box-panel" <?php echo $channel_style;?>>
	            <h3 class="title">TRAILER</h3>               
	            <div class="row">
	                <div class="col-xs-12">
											<?php for ($i = 1; $i <= 3 ; $i++) : ?>
		                    <div class="trailer col-xs-4 bgfull" style="background-image:url(<?php echo theme_url('assets/img/icon-trailer-2x.png'); ?>);"></div>

		                	<?php endfor ?>                  
	                </div>
	            </div>
	        </div><!-- /trailer -->				
	        <?php endif ?>

				<?php endif ?>
				
        <?php if(isset($item->related) && $item->related && $item->category_name != 'Channel'): ?>      
	        <div class="box-panel">
        			<?php if(isset($item->content_category_id) && $item->content_category_id =='185202286854093e538b8e7'): ?>	        	  
	            	<h3 class="title">EPISODE</h3>
							<?php else:?>
	            	<h3 class="title">RELATED</h3>							
							<?php endif;?>
	            <div class="row">
	            	<div class="col-xs-12">

		            	<div class="content_slider owl-carousel owl-theme" id="slider-<?php echo $product->slug; ?>">	
		            		<?php foreach($item->related as $relate): ?>
                      <?php if($relate->slug) { ?> 
			            		<a href="<?php echo site_url($base_url.'/'.$relate->slug); ?>" class="item" style="width: 169px !important;">
         							<div class="product_title" style="width: 169px !important;">
          							<span class="pull-left"><?php echo ellipsize($relate->title, 15); ?></span>
							          <!-- span class="pull-right"><?php if($relate->prod_year) echo ' - '.$relate->prod_year; ?></span -->
						            <div class="clearfix"></div>
					            </div>	
				              <img class="related bgfull" data-id="<?php echo $product->id; ?>"
				                   src="<?php echo config_item('digital_content_thumbnail_path').$relate->video_thumbnail ?>"
                           rel="tooltip" data-placement="right" data-container="body" 
                           data-original-title="<?php echo $product->name.' : '.substr(strip_tags($product->description), 0, 200); ?>" 
                           index="1"  style="width: 169px !important;">				                   
				               </a>   
				              <?php } ?>
			                <?php endforeach ?>
			              
		                </div>   
	                </div>
	                
	                   
	            </div>   
	        </div><!-- /related -->
	        
        
	        
        <?php endif ?>

    </div>
</div>

    
        <?php if($item->category_name == 'Channel'): ?>

        <div class="col-xs-12 no-padding-left"  id="<?php echo $item->category_name; ?>">                
            <h3 class="title">RECORDED TV SHOW</h3>
            <h3 class="site-color"></h3> 
            <div class="row">

              <ul class="nav nav-tabs" role="tablist">
                 <li class="active"><a href="#rec-01" role="tab" data-toggle="tab">SELASA<br/>24/09/2014</a></li>
                 <li><a href="#rec-02" role="tab" data-toggle="tab">RABU<br/>25/09/2014</a></li>
                 <li><a href="#rec-03" role="tab" data-toggle="tab">KAMIS<br/>26/09/2014</a></li>
                 <li><a href="#rec-04" role="tab" data-toggle="tab">JUMAT<br/>26/09/2014</a></li>
                 <li><a href="#rec-05" role="tab" data-toggle="tab">SABTU<br/>27/09/2014</a></li>
                 <li><a href="#rec-06" role="tab" data-toggle="tab">MINGGU<br/>28/09/2014</a></li>
                 <li><a href="#rec-07" role="tab" data-toggle="tab">SENIN<br/>29/09/2014</a></li>

              </ul>

              <div class="tab-content">
                 <div class="tab-pane active" id="rec-01">
                 	<?php 
                 	
                switch($product->name)
                {
                	case 'RCTI' :                 	
                  $sched07_01 = array(
                      1 => array ('jam' => '00:00', 'acara' => 'THE DARK KNIGHT'),
                      2 => array ('jam' => '01:30', 'acara' => 'SEPUTAR INDONESIA'),
                      3 => array ('jam' => '02:00', 'acara' => 'ASIAN GAMES 2014 FOOTBALL BABAK 16 BESAR THAILAND VS CHINA'),
                      4 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHIEM'),
                      5 => array ('jam' => '04:30', 'acara' => 'SEPUTAR INDONESIA PAGI'),
                      6 => array ('jam' => '05:30', 'acara' => 'GO SPOT'),
                      7 => array ('jam' => '06:00', 'acara' => 'LARVA'),
                      8 => array ('jam' => '06:30', 'acara' => 'AIKATSU'),
                      9 => array ('jam' => '07:00', 'acara' => 'TOY STORY'),
                      10 => array ('jam' => '07:30', 'acara' => 'KIKO'),
                  );

                  $sched07_02 = array(
                      1 => array ('jam' => '11:30', 'acara' =>    'SEPUTAR INDONESIA SIANG    NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'HOME ALONE'),
                      3 => array ('jam' => '14:45', 'acara' => 'HOME ALONE 2: LOST IN NEW YORK'),
                      4 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA'),
                      5 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA'),
                      6 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES'),
                      7 => array ('jam' => '20:00', 'acara' => 'CATATAN HATI SEORANG ISTRI'),
                      8 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK'),
                      9 => array ('jam' => '22:45', 'acara' => 'LIGA BBVA BARCELONA VS GRANADA'),
                  );           
                  break;
                  
                  case 'MNC TV' :
                  $sched07_01 = array(
                      1 => array ('jam' => '01:15', 'acara' => 'CERITA DINI HARI'),
                      2 => array ('jam' => '02:00', 'acara' => 'KACA BENGGALA'),
                      3 => array ('jam' => '03:30', 'acara' => 'BIMBINGAN ROHANI'),
                      4 => array ('jam' => '04:00', 'acara' => 'INSPIRASI PAGI'),
                      5 => array ('jam' => '04:30', 'acara' => 'PELESIR'),
                      6 => array ('jam' => '05:00', 'acara' => 'BUKA MATA'),
                      7 => array ('jam' => '05:30', 'acara' => 'SIRAMAN QALBU'),
                      8 => array ('jam' => '06:30', 'acara' => 'JENDELA'),
                      9 => array ('jam' => '07:00', 'acara' => 'MATA PANCING'),
                      10 => array ('jam' => '07:30', 'acara' => 'RASA SAYANGE'),
                  );

                  $sched07_02 = array(
                      1 => array ('jam' => '08:00', 'acara' =>    'UPIN & IPIN THE MOVIE'),
                      2 => array ('jam' => '09:30', 'acara' => 'ANIMASI SPESIAL'),
                      3 => array ('jam' => '10:30', 'acara' => 'FILM TV PLATINUM'),
                      4 => array ('jam' => '12:30', 'acara' => 'ALL FUTSAL CHAMPIONSHIP'),
                      5 => array ('jam' => '15:00', 'acara' => 'INSPIRASI SORE'),
                      6 => array ('jam' => '15:30', 'acara' => 'ANIMASI SPESIAL-1'),
                      7 => array ('jam' => '16:00', 'acara' => 'ANIMASI SPESIAL'),
                      8 => array ('jam' => '18:00', 'acara' => 'DI SINI ADA TUYUL'),
                      9 => array ('jam' => '19:00', 'acara' => 'MANUSIA HARIMAU'),
                      10 => array ('jam' => '23:45', 'acara' => 'CERITA PILIHAN'),
                  );
                  break;
                  
                  default :
                  $sched07_01 = array(
                      1 => array ('jam' => '00:00', 'acara' => 'THE DARK KNIGHT'),
                      2 => array ('jam' => '01:30', 'acara' => 'SEPUTAR INDONESIA'),
                      3 => array ('jam' => '02:00', 'acara' => 'ASIAN GAMES 2014 FOOTBALL BABAK 16 BESAR THAILAND VS CHINA'),
                      4 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHIEM'),
                      5 => array ('jam' => '04:30', 'acara' => 'SEPUTAR INDONESIA PAGI'),
                      6 => array ('jam' => '05:30', 'acara' => 'GO SPOT'),
                      7 => array ('jam' => '06:00', 'acara' => 'LARVA'),
                      8 => array ('jam' => '06:30', 'acara' => 'AIKATSU'),
                      9 => array ('jam' => '07:00', 'acara' => 'TOY STORY'),
                      10 => array ('jam' => '07:30', 'acara' => 'KIKO'),
                  );

                  $sched07_02 = array(
                      1 => array ('jam' => '11:30', 'acara' =>    'SEPUTAR INDONESIA SIANG    NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'HOME ALONE'),
                      3 => array ('jam' => '14:45', 'acara' => 'HOME ALONE 2: LOST IN NEW YORK'),
                      4 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA'),
                      5 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA'),
                      6 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES'),
                      7 => array ('jam' => '20:00', 'acara' => 'CATATAN HATI SEORANG ISTRI'),
                      8 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK'),
                      9 => array ('jam' => '22:45', 'acara' => 'LIGA BBVA BARCELONA VS GRANADA'),
                  );           
                  break;
                }                                                                        
                        	
                 ?>
                 	  <div style="float:left;width: 500px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_01 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>

                 	  <div style="float:left;width: 500px;margin-left:35px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_02 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>
                 </div>

                 <div class="tab-pane" id="rec-02">
                 	<?php 
                switch($product->name)
                {
                	case 'RCTI' :                 	                 	                 
                    $sched07_01 = array(
                      1 => array ('jam' => '00:00', 'acara' => 'LIGA BBVA BARCELONA VS GRANADA'),
                      2 => array ('jam' => '00:45', 'acara' => 'LIGA BBVA ATLETICO MADRID VS SEVILLA'),
                      3 => array ('jam' => '03:00', 'acara' => 'YUSRA DAN YUMNA'),
                      4 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHIEM'),
                      5 => array ('jam' => '04:30', 'acara' => 'SEPUTAR INDONESIA PAGI'),
                      6 => array ('jam' => '05:30', 'acara' => 'GO SPOT'),
                      7 => array ('jam' => '06:00', 'acara' => 'PHINEAS & FERB'),
                      8 => array ('jam' => '06:30', 'acara' => 'HAVE A LAUGH'),
                      9 => array ('jam' => '07:00', 'acara' => 'GRAVITY FALLS'),
                      10 => array ('jam' => '07:30', 'acara' => 'CRAYON SHINCHAN'),
                  );

                     $sched07_02 = array(
                      1 => array ('jam' => '11:30', 'acara' => 'SEPUTAR INDONESIA SIANG    NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'MUKJIZAT ITU NYATA'),
                      3 => array ('jam' => '14:30', 'acara' => 'THE BIGGEST GAME SHOW IN THE WORLD'),
                      4 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA'),
                      5 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA'),
                      6 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES'),
                      7 => array ('jam' => '20:00', 'acara' => 'CATATAN HATI SEORANG ISTRI'),
                      8 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK'),
                      9 => array ('jam' => '22:30', 'acara' => 'TVM: AKANKAH BUNDA DATANG KE PERNIKAHANKU'),
                  );        
                  break;
                  
                  case 'MNC TV' :
                  $sched07_01 = array(
                      1 => array ('jam' => '01:15', 'acara' => 'CERITA DINI HARI'),
                      2 => array ('jam' => '02:00', 'acara' => 'KACA BENGGALA'),
                      3 => array ('jam' => '03:30', 'acara' => 'DISNEY JUNIOR: CHIP n DALE RESCUE RANGER'),
                      4 => array ('jam' => '04:00', 'acara' => 'LINTAS PAGI'),
                      5 => array ('jam' => '04:30', 'acara' => 'PELESIR'),
                      6 => array ('jam' => '05:00', 'acara' => 'SIRAMAN QALBU'),
                      7 => array ('jam' => '06:00', 'acara' => 'UPIN & IPIN THE MOVIE'),
                      8 => array ('jam' => '07:00', 'acara' => 'ADIT & SOPO JARWO THE MOVIE'),
                      9 => array ('jam' => '08:30', 'acara' => 'GREBEK NUSANTARA'),
                      10 => array ('jam' => '09:30', 'acara' => 'JENDELA'),
                  );

                     $sched07_02 = array(
                      1 => array ('jam' => '10:00', 'acara' =>    'MATA PANCING'),
                      2 => array ('jam' => '10:30', 'acara' => 'ANIMASI SPESIAL'),
                      3 => array ('jam' => '12:30', 'acara' => 'FILM TV PLATINUM'),
                      4 => array ('jam' => '14:30', 'acara' => 'MNCTV FESTIVAL BANDUNG 2'),
                      5 => array ('jam' => '16:30', 'acara' => 'INSPIRASI SORE'),
                      6 => array ('jam' => '17:00', 'acara' => 'ANIMASI SPESIAL-1'),
                      7 => array ('jam' => '18:00', 'acara' => 'DI SINI ADA TUYUL'),
                      8 => array ('jam' => '19:00', 'acara' => 'MANUSIA HARIMAU'),
                      9 => array ('jam' => '20:15', 'acara' => 'RADEN KIAN SANTANG'),
                      10 => array ('jam' => '23:45', 'acara' => 'CERITA PILIHAN'),
                  ); 
                  break;
                  
                  default :
                    $sched07_01 = array(
                      1 => array ('jam' => '00:00', 'acara' => 'LIGA BBVA BARCELONA VS GRANADA'),
                      2 => array ('jam' => '00:45', 'acara' => 'LIGA BBVA ATLETICO MADRID VS SEVILLA'),
                      3 => array ('jam' => '03:00', 'acara' => 'YUSRA DAN YUMNA'),
                      4 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHIEM'),
                      5 => array ('jam' => '04:30', 'acara' => 'SEPUTAR INDONESIA PAGI'),
                      6 => array ('jam' => '05:30', 'acara' => 'GO SPOT'),
                      7 => array ('jam' => '06:00', 'acara' => 'PHINEAS & FERB'),
                      8 => array ('jam' => '06:30', 'acara' => 'HAVE A LAUGH'),
                      9 => array ('jam' => '07:00', 'acara' => 'GRAVITY FALLS'),
                      10 => array ('jam' => '07:30', 'acara' => 'CRAYON SHINCHAN'),
                  );

                     $sched07_02 = array(
                      1 => array ('jam' => '11:30', 'acara' => 'SEPUTAR INDONESIA SIANG    NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'MUKJIZAT ITU NYATA'),
                      3 => array ('jam' => '14:30', 'acara' => 'THE BIGGEST GAME SHOW IN THE WORLD'),
                      4 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA'),
                      5 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA'),
                      6 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES'),
                      7 => array ('jam' => '20:00', 'acara' => 'CATATAN HATI SEORANG ISTRI'),
                      8 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK'),
                      9 => array ('jam' => '22:30', 'acara' => 'TVM: AKANKAH BUNDA DATANG KE PERNIKAHANKU'),
                  );        
                  break;                   
                  
                }                 
                  
                  
              ?>
                 	  <div style="float:left;width: 500px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_01 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>

                 	  <div style="float:left;width: 500px;margin-left:35px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_02 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>
                 </div>

                 <div class="tab-pane" id="rec-03">
                 	<?php 
                     $sched07_01 = array(
                         1 => array ('jam' => '00:00', 'acara' => 'TVM: AKAHKAH BUNDA DATANG KE PERNIKAHANKU'),
                         2 => array ('jam' => '00:30', 'acara' => 'DELIK'),
                      3 => array ('jam' => '01:00', 'acara' => 'LIGA BBVA VILLAREAL VS REAL MADRID'),
                      4 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHIEM'),
                      5 => array ('jam' => '04:30', 'acara' => 'SEPUTAR INDONESIA PAGI'),
                      6 => array ('jam' => '06:00', 'acara' => 'GO SPOT'),
                      7 => array ('jam' => '08:00', 'acara' => 'INTENS'),
                      8 => array ('jam' => '09:00', 'acara' => 'DAHSYAT'),
                      9 => array ('jam' => '11:00', 'acara' => 'SILET'),
                  );

                     $sched07_02 = array(
                      1 => array ('jam' => '11:30', 'acara' =>    'SEPUTAR INDONESIA SIANG    NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'FROM PIZZA WITH LOVE'),
                      3 => array ('jam' => '13:45', 'acara' => 'ITEUNG, I LOVE U FULL'),
                      4 => array ('jam' => '15:30', 'acara' => 'CRISPY'),
                      5 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA'),
                      6 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES'),
                      7 => array ('jam' => '20:00', 'acara' => 'CATATAN HATI SEORANG ISTRI'),
                      8 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK'),
                      9 => array ('jam' => '23:00', 'acara' => 'MEGA SINEMA: CINTAKU DI TANGAN HANSIP'),
                  );
                 	?>
                 	  <div style="float:left;width: 500px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_01 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>

                 	  <div style="float:left;width: 500px;margin-left:35px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_02 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>
                 	</div>

                 <div class="tab-pane" id="rec-04">
                 	<?php 
                     $sched07_01 = array(
                         1 => array ('jam' => '00:00', 'acara' => 'CINTAKU DI TANGAN HANSIP'),
                         2 => array ('jam' => '01:00', 'acara' => 'SEPUTAR INDONESIA MALAM'),
                      3 => array ('jam' => '01:30', 'acara' => 'ASIAN GAMES 2014 FOOTBALL BABAK 8 BESAR'),
                      4 => array ('jam' => '03:00', 'acara' => 'ASIAN GAMES 2014 FOOTBALL BABAK 8 BESAR'),
                      5 => array ('jam' => '04:30', 'acara' => 'SEPUTAR INDONESIA PAGI'),
                      6 => array ('jam' => '05:30', 'acara' => 'GO SPOT'),
                      7 => array ('jam' => '08:00', 'acara' => 'INTENS'),
                      8 => array ('jam' => '09:00', 'acara' => 'DAHSYAT'),
                      9 => array ('jam' => '11:00', 'acara' => 'SILET'),
                  );

                     $sched07_02 = array(
                      1 => array ('jam' => '11:30', 'acara' =>    'SEPUTAR INDONESIA SIANG    NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'CRISPY'),
                      3 => array ('jam' => '13:00', 'acara' => 'AFC CUP 2014 SEMIFINAL PERSIPURA JAYAPURA VS QADSIA SC'),
                      4 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA'),
                      5 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA'),
                      6 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES'),
                      7 => array ('jam' => '20:00', 'acara' => 'CATATAN HATI SEORANG ISTRI'),
                      8 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK'),
                      9 => array ('jam' => '23:00', 'acara' => 'KUTUNGGU KAU DI PARKIRAN CINTA'),
                  );                 	?>
                 	  <div style="float:left;width: 500px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_01 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>

                 	  <div style="float:left;width: 500px;margin-left:35px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_02 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>
                 	</div>
                 	
                 <div class="tab-pane" id="rec-05">
                 	<?php 
                 	$sched07_01 = array( 
                 	    1 => array ('jam' => '00:00', 'acara' => 'TVM : AKANKAH BUNDA DATANG KE PERNIKAHANKU'),
                 	    2 => array ('jam' => '00:30', 'acara' => 'DELIK'),
                      3 => array ('jam' => '01:00', 'acara' => 'LIGA BBVA VILLAREAL VS REAL MADRID'),
                      4 => array ('jam' => '03:00', 'acara' => 'YUSRA & YUMNA'),
                      5 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHEM	DRAMA & MOVIES'),
                      6 => array ('jam' => '04:30', 'acara'	=> 'SEPUTAR INDONESIA PAGI	NEWS & SPORT'),
                      7 => array ('jam' => '06:00', 'acara' => 'GO SPOT	ENTERTAINMENT'),
                      8 => array ('jam' => '08:00', 'acara' => 'INTENS	ENTERTAINMENT'),
                      9 => array ('jam' => '09:00', 'acara' => 'DAHSYAT	ENTERTAINMENT'),
                      10 => array ('jam' => '11:00', 'acara' => 'SILET	ENTERTAINMENT'),
                  );
                 
                 	$sched07_02 = array(                       
                      1 => array ('jam' => '11:30', 'acara' =>	'SEPUTAR INDONESIA SIANG	NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'FROM PIZZA WITH LOVE'),
                      3 => array ('jam' => '13:45', 'acara' => 'ITEUNG, I LOVE U FULL'),
                      4 => array ('jam' => '15:30', 'acara' => 'CRISPY'),
                      5 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA	NEWS & SPORT'),
                      6 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA	DRAMA & MOVIES'),
                      7 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES	DRAMA & MOVIES'),
                      8 => array ('jam' => '20:00', 'acara' => 'Catatan Hati Seorang Istri	DRAMA & MOVIES'),
                      9 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK	DRAMA & MOVIES'),
                      10 => array ('jam' => '23:00', 'acara' => 'MEGA SINEMA : CINTAKU DITANGAN HANSIP')
                  );    
                 	?>
                 	  <div style="float:left;width: 500px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_01 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>

                 	  <div style="float:left;width: 500px;margin-left:35px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_02 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>
                 	</div>
                 	
                 <div class="tab-pane" id="rec-06">
                 	<?php 
                  $sched07_01 = array(
                         1 => array ('jam' => '00:00', 'acara' => 'THE DARK KNIGHT'),
                         2 => array ('jam' => '01:30', 'acara' => 'SEPUTAR INDONESIA'),
                      3 => array ('jam' => '02:00', 'acara' => 'ASIAN GAMES 2014 FOOTBALL BABAK 16 BESAR THAILAND VS CHINA'),
                      4 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHIEM'),
                      5 => array ('jam' => '04:30', 'acara' => 'SEPUTAR INDONESIA PAGI'),
                      6 => array ('jam' => '05:30', 'acara' => 'GO SPOT'),
                      7 => array ('jam' => '06:00', 'acara' => 'LARVA'),
                      8 => array ('jam' => '06:30', 'acara' => 'AIKATSU'),
                      9 => array ('jam' => '07:00', 'acara' => 'TOY STORY'),
                      10 => array ('jam' => '07:30', 'acara' => 'KIKO'),
                  );

                  $sched07_02 = array(
                      1 => array ('jam' => '11:30', 'acara' =>    'SEPUTAR INDONESIA SIANG    NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'HOME ALONE'),
                      3 => array ('jam' => '14:45', 'acara' => 'HOME ALONE 2: LOST IN NEW YORK'),
                      4 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA'),
                      5 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA'),
                      6 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES'),
                      7 => array ('jam' => '20:00', 'acara' => 'CATATAN HATI SEORANG ISTRI'),
                      8 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK'),
                      9 => array ('jam' => '22:45', 'acara' => 'LIGA BBVA BARCELONA VS GRANADA'),
                  );                 
                 	?>
                 	  <div style="float:left;width: 500px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_01 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>

                 	  <div style="float:left;width: 500px;margin-left:35px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_02 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>
                 	</div>
                 	
                 <div class="tab-pane" id="rec-07">
                 	<?php 
               switch($product->name)
                {
                	case 'RCTI' :                     	                 	
                 	$sched07_01 = array( 
                 	    1 => array ('jam' => '00:00', 'acara' => 'TVM : AKANKAH BUNDA DATANG KE PERNIKAHANKU'),
                 	    2 => array ('jam' => '00:30', 'acara' => 'DELIK'),
                      3 => array ('jam' => '01:00', 'acara' => 'LIGA BBVA VILLAREAL VS REAL MADRID'),
                      4 => array ('jam' => '03:00', 'acara' => 'YUSRA & YUMNA'),
                      5 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHEM	DRAMA & MOVIES'),
                      6 => array ('jam' => '04:30', 'acara'	=> 'SEPUTAR INDONESIA PAGI	NEWS & SPORT'),
                      7 => array ('jam' => '06:00', 'acara' => 'GO SPOT	ENTERTAINMENT'),
                      8 => array ('jam' => '08:00', 'acara' => 'INTENS	ENTERTAINMENT'),
                      9 => array ('jam' => '09:00', 'acara' => 'DAHSYAT	ENTERTAINMENT'),
                      10 => array ('jam' => '11:00', 'acara' => 'SILET	ENTERTAINMENT'),
                  );
                 
                 	$sched07_02 = array(                       
                      1 => array ('jam' => '11:30', 'acara' =>	'SEPUTAR INDONESIA SIANG	NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'FROM PIZZA WITH LOVE'),
                      3 => array ('jam' => '13:45', 'acara' => 'ITEUNG, I LOVE U FULL'),
                      4 => array ('jam' => '15:30', 'acara' => 'CRISPY'),
                      5 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA	NEWS & SPORT'),
                      6 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA	DRAMA & MOVIES'),
                      7 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES	DRAMA & MOVIES'),
                      8 => array ('jam' => '20:00', 'acara' => 'Catatan Hati Seorang Istri	DRAMA & MOVIES'),
                      9 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK	DRAMA & MOVIES'),
                      10 => array ('jam' => '23:00', 'acara' => 'MEGA SINEMA : CINTAKU DITANGAN HANSIP')
                  );   
                  break;
                  
                  case 'MNC TV' :
                     $sched07_01 = array(
                      1 => array ('jam' => '01:15', 'acara' => 'CERITA DINI HARI'),
                      2 => array ('jam' => '02:00', 'acara' => 'KACA BENGGALA'),
                      3 => array ('jam' => '03:30', 'acara' => 'DISNEY JUNIOR: CHIP n DALE RESCUE RANGER'),
                      4 => array ('jam' => '04:00', 'acara' => 'LINTAS PAGI'),
                      5 => array ('jam' => '04:30', 'acara' => 'PELESIR'),
                      6 => array ('jam' => '05:00', 'acara' => 'SIRAMAN QALBU'),
                      7 => array ('jam' => '06:00', 'acara' => 'UPIN & IPIN THE MOVIE'),
                      8 => array ('jam' => '07:00', 'acara' => 'ADIT & SOPO JARWO THE MOVIE'),
                      9 => array ('jam' => '08:30', 'acara' => 'GREBEK NUSANTARA'),
                      10 => array ('jam' => '09:30', 'acara' => 'JENDELA'),
                  );

                     $sched07_02 = array(
                      1 => array ('jam' => '10:00', 'acara' =>    'MATA PANCING'),
                      2 => array ('jam' => '10:30', 'acara' => 'ANIMASI SPESIAL'),
                      3 => array ('jam' => '12:30', 'acara' => 'FILM TV PLATINUM'),
                      4 => array ('jam' => '14:30', 'acara' => 'MNCTV FESTIVAL BANDUNG 2'),
                      5 => array ('jam' => '16:30', 'acara' => 'INSPIRASI SORE'),
                      6 => array ('jam' => '17:00', 'acara' => 'ANIMASI SPESIAL-1'),
                      7 => array ('jam' => '18:00', 'acara' => 'DI SINI ADA TUYUL'),
                      8 => array ('jam' => '19:00', 'acara' => 'MANUSIA HARIMAU'),
                      9 => array ('jam' => '20:15', 'acara' => 'RADEN KIAN SANTANG'),
                      10 => array ('jam' => '23:45', 'acara' => 'CERITA PILIHAN'),
                  );
                  break;                  
                                    
                  default:
                 	$sched07_01 = array( 
                 	    1 => array ('jam' => '00:00', 'acara' => 'TVM : AKANKAH BUNDA DATANG KE PERNIKAHANKU'),
                 	    2 => array ('jam' => '00:30', 'acara' => 'DELIK'),
                      3 => array ('jam' => '01:00', 'acara' => 'LIGA BBVA VILLAREAL VS REAL MADRID'),
                      4 => array ('jam' => '03:00', 'acara' => 'YUSRA & YUMNA'),
                      5 => array ('jam' => '04:00', 'acara' => 'QAMAR BANI HASHEM	DRAMA & MOVIES'),
                      6 => array ('jam' => '04:30', 'acara'	=> 'SEPUTAR INDONESIA PAGI	NEWS & SPORT'),
                      7 => array ('jam' => '06:00', 'acara' => 'GO SPOT	ENTERTAINMENT'),
                      8 => array ('jam' => '08:00', 'acara' => 'INTENS	ENTERTAINMENT'),
                      9 => array ('jam' => '09:00', 'acara' => 'DAHSYAT	ENTERTAINMENT'),
                      10 => array ('jam' => '11:00', 'acara' => 'SILET	ENTERTAINMENT'),
                  );
                 
                 	$sched07_02 = array(                       
                      1 => array ('jam' => '11:30', 'acara' =>	'SEPUTAR INDONESIA SIANG	NEWS & SPORT'),
                      2 => array ('jam' => '12:00', 'acara' => 'FROM PIZZA WITH LOVE'),
                      3 => array ('jam' => '13:45', 'acara' => 'ITEUNG, I LOVE U FULL'),
                      4 => array ('jam' => '15:30', 'acara' => 'CRISPY'),
                      5 => array ('jam' => '16:30', 'acara' => 'SEPUTAR INDONESIA	NEWS & SPORT'),
                      6 => array ('jam' => '17:00', 'acara' => 'BASTIAN STEEL BUKAN COWOK BIASA	DRAMA & MOVIES'),
                      7 => array ('jam' => '18:15', 'acara' => 'TUKANG BUBUR NAIK HAJI THE SERIES	DRAMA & MOVIES'),
                      8 => array ('jam' => '20:00', 'acara' => 'Catatan Hati Seorang Istri	DRAMA & MOVIES'),
                      9 => array ('jam' => '21:30', 'acara' => 'KITA NIKAH YUK	DRAMA & MOVIES'),
                      10 => array ('jam' => '23:00', 'acara' => 'MEGA SINEMA : CINTAKU DITANGAN HANSIP')
                  );   
                  break;
                                    
                }
                   
                 	?>
                 	  <div style="float:left;width: 500px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_01 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>

                 	  <div style="float:left;width: 500px;margin-left:35px;">
	  								  <table class="schedule-rec">
		              		<?php foreach($sched07_02 as $key => $rwSched): ?>
											  <tr><td style="width:60px;" align="left"><?php echo $rwSched['jam']; ?></td>
												  <td><?php echo $rwSched['acara']; ?></td></tr>
			                <?php endforeach ?>
  	 								  </table>
  	 								</div>
  	 								                 	                 	                 	
                 	</div>                  
              </div>
            </div>
        </div>  

        <?php endif ?>        
    

<script type="text/javascript">
    $(document).ready(function(){

        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes();

        var findTime = $('tr').find('td:contains('+time+')');
 
        if($('tr').find('td:contains('+time+')').length > 0) {
        	$('tr').find('td:contains('+time+')').css('font-weight', 'bold');
        } 
                

        $('dd a').on('click', function(ev){
            ev.preventDefault();
        });

        if($('.content_slider').length > 0){
            $('.content_slider').each(function(idx, slider){
                var newcontent_slider = $(slider).owlCarousel({
                    navigation: true, navigationText: false, pagination: false,
                    items: 4,
                });
            })
        }    	

        $('.thumb').on('click', function(ev){
            ev.preventDefault();
            $('.poster').removeAttr('style').attr('style', 'background-image:url('+$(this).attr('src')+')');
        });

        $('#btn_add_to_cart').on('click', function(ev){
            // $('#form_add_to_cart').submit();
            ev.preventDefault();
            var $params = {
                cartkey: '<?php echo $this->session->flashdata('cartkey'); ?>',
                id: '<?php echo $product->id?>'
            }
            $.blockUI({message: 'Processing...'});
            $.post('<?php echo site_url("/cart/add_to_cart"); ?>', $params, function(resp){
                console.log(JSON.stringify(resp));
                $.unblockUI();
                if(resp.rcode != 'OK'){
                    BootstrapDialog.alert({
                        title: 'FAILED',
                        message: 'resp.error'
                    });
                }else{
                    $('.userMenu a[title="Cart"]').trigger('click');
                }
            }, 'json');
        });

    });
</script>