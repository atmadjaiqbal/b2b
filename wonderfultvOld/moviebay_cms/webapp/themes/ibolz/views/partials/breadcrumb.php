<div class="breadcrumbDiv animated <?php if(!isset($homepage)): ?>breadcrumb-fixed-top<?php endif; ?> hidden-xs hidden-sm">
	<div class="container">	
		<div class="row">  				

			<!-- <div class="search-full text-right"> 
				<a class="pull-right search-close"> <i class=" fa fa-times-circle"> </i> </a>
				<div class="searchInputBox pull-right">
				<?php echo form_open('cart/search', 'class="navbar-search pull-right"');?>
				  <input type="search" name="term" placeholder="start typing and hit enter to search" class="search-input">
				  <button class="btn-nobg search-btn" type="submit"> <i class="fa fa-search"> </i> </button>
				 <?php echo form_close(); ?>
				</div>
			</div> -->
			<ul class="breadcrumb col-xs-9">
				<li><a href="<?php echo site_url();?>"><i class="fa fa-home fa-lg"></i></a></li>
				<?php
					$breadcrumb_base_url = '';
					if(!empty($base_url) && is_array($base_url)):
						$url_path	= '';
						foreach($base_url as $count=>$bc):
							$url_path .= '/'.$bc;
							$breadcrumb_base_url = $url_path;
							if($count < count($base_url) && $count < 3): ?>
								<li><a href="<?php echo site_url($url_path);?>"><?php echo $bc;?></a></li>								
							<?php endif;
						endforeach; ?>
						<li class="active"><?php echo $base_url[count($base_url)];?></li>
					<?php endif; ?>
				<input type="hidden" id="breadcrumb_base_url" value="<?php echo ($breadcrumb_base_url == '') ? '/' : $breadcrumb_base_url; ?>" />
			</ul>
			<div class="col-xs-3">
				<?php echo form_open('cart/search', 'class="pull-right"');?>					
					<div class="input-group" style="margin-top:5px;">
					  <input type="text" name="term" placeholder="Search ..." value="<?php echo (isset($term)) ? $term : ''; ?>" style="border:1px solid #ccc;border-radius:5px;padding-left:5px;">
					  <button class="btn btn-nobg site-color" type="submit"> <i class="fa fa-search"> </i> </button>				 
					</div>				
				<?php echo form_close(); ?>
			</div>

		</div>
	</div>
</div>
