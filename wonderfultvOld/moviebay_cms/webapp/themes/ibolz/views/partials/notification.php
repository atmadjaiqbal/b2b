<?php if ($this->session->flashdata('message')):?>

		<div class="alert alert-info" style="position:fixed;top:80px;width:100%;z-index:999">
			<div class="container">	
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('message');?>
			</div>
		</div>

<?php endif;?>

<?php if ($this->session->flashdata('error')):?>

		<div class="alert alert-warning" style="position:fixed;top:80px;width:100%;z-index:999">
			<div class="container">	
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $this->session->flashdata('error');?>
			</div>
		</div>

<?php endif;?>

<?php if (!empty($error)):?>

	<div class="alert alert-danger" style="margin:-15px 10%;">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo $error;?>
	</div>

<?php endif;?>	
