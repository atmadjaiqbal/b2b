<div class="container main-container headerOffset">  
  
    <?php if(isset($category)): ?>

        <?php if(isset($this->categories[$category->id] ) && count($this->categories[$category->id]) > 0):  ?>
            <?php foreach($this->categories[$category->id] as $subcategory):?>
                <div class="row <?php echo $category->slug; ?>" id="products-<?php echo $subcategory->slug; ?>">
                    <?php $this->banners->show_products($subcategory->name, false, 'slider_products', $subcategory->slug);?>
                </div>
            <?php endforeach;?>            
        <?php elseif($products): ?>            
            <div class="row product_panel paddTop20 bg-loader hidden" id="content_preview"></div>        
            <div class="row paddTop10">
                <h3 class="section-title">
                    <span><?php echo $category->name; ?> <i class="fa fa-angle-right"></i></span>
                </h3>
            </div>
            <div class="product_panel grid <?php echo $category->slug; ?>" id="content_product_list"> 
                <?php $this->load->view('partials/product_list'); ?>
            </div>
            
            <?php if($pagination['current_page'] < $pagination['total_pages']): ?>
                <div class="row padd">
                    <div class="load-more-block text-center">
                       <a class="btn btn-thin" href="#" id="btnLoadMore"> <i class="fa fa-plus-sign">+</i> load more products</a>
                    </div>                                        
                </div>
            <?php endif ?>

            <script type="text/javascript">
                var initPreviewProduct = function(){
                    $('.preview_product').on('click', function(ev){
                        ev.preventDefault();
                        var self = this;
                        $("body").animate({ scrollTop: $(self).offset().top-60}, "slow");
                        $('#content_preview').html('').insertAfter($(self).parent().parent()).removeClass('hidden');
                        app.preview_product('#content_preview', $(this).data('id'), {base_url:$('#breadcrumb_base_url').val() }, function(){
                            // $('#content_preview').insertAfter($(self).parent().parent());
                        });
                    });                    
                }
                $(document).ready(function(){
                    initPreviewProduct();
                    $('#btnLoadMore').on('click', function(ev){
                        ev.preventDefault();                        
                        if( $('ul.pagination > li.next > a').length > 0 ){                            
                            $.get($('ul.pagination > li.next > a').attr('href'), function(contentHtml){
                                $('div.pagination').remove();
                                $('#content_product_list').append(contentHtml);
                                if($('ul.pagination > li.next').length == 0){
                                    $('#btnLoadMore').parent().parent().hide();
                                }
                                initPreviewProduct();
                                $('.lazy').lazyload();
                            })
                        }                        
                    });

                    //adjust size
                    /*
                    var ratio = 480/320;
                    if($(window).width() >= 997){           
                        var w = parseInt($(window).width() / 6);
                        var h = parseInt(w*ratio);
                        $('.product_panel.grid .item').css('height', (h+45)+'px');
                        $('.product_panel.grid .related').css('height', h+'px');                                                                                              
                    }else if($(window).width() >= 768){           
                        var w = parseInt($(window).width() / 4);
                        var h = parseInt(w*ratio);
                        $('.product_panel.grid .item').css('height', (h+45)+'px');
                        $('.product_panel.grid .related').css('height', h+'px');
                    }
                    */
                    
                    <?php 
                    if($category->slug == 'channel') { ?>
                        $('.product_panel.grid .item').css('height', '169px');
                        $('.product_panel.grid .related').css('height', '169px');
                    <?php } ?>
                });
            </script>
        <?php else: ?>
            <div class="alert alert-danger">
                <?php echo lang('no_products');?>
            </div>        
        <?php endif; ?>

    <?php endif;?>
  

</div>


