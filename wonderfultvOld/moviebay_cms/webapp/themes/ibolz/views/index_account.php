<div class="container main-container headerOffset">  

	<div class="row">
		<div class="col-lg-9 col-md-9 col-sm-7">
		  <h1 class="section-title-inner"><span><i class="fa fa-unlock-alt"></i> <?php echo $page_title; ?> </span></h1>
		  <div class="row userInfo">
		    <div class="col-xs-12 col-sm-12">
		      <h2 class="block-title-2"><span>Welcome to your account. Here you can manage all of your personal information and orders.</span></h2>
		      <ul class="myAccountList row">
		        <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
		          	<div class="thumbnail equalheight" style="height: 135px; "> 
		          		<a title="Personal information" href="/secure/my_account"><i class="fa fa-cog"></i> Personal information</a> 
		         	</div>
		        </li>		      	
		        <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
		          	<div class="thumbnail equalheight" style="height: 135px; "> 
		          		<a title="Orders" href="#"><i class="fa fa-calendar"></i> Purchase history </a> 
		          	</div>
		        </li>
		        <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
		          	<div class="thumbnail equalheight" style="height: 135px; "> 
		          		<a title="My wishlists" href="#"><i class="fa fa-heart"></i> My wishlists </a> 
		          	</div>
		        </li>
		        <!-- <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
		          	<div class="thumbnail equalheight" style="height: 135px; "> 
		          		<a title="My addresses" href="my-address.html"><i class="fa fa-map-marker"></i> My addresses</a> 
		          	</div>
		        </li>
		        <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4  text-center ">
		          	<div class="thumbnail equalheight" style="height: 135px; "> 
		          		<a title="Add  address" href="add-address.html"> <i class="fa fa-edit"> </i> Add  address</a> 
		          	</div>
		        </li> -->
		      </ul>
		      <div class="clear clearfix"> </div>
		    </div>
		  </div>
		  <!--/row end--> 	  
		</div>
		<div class="col-lg-3 col-md-3 col-sm-5"> </div>
	</div>

</div>