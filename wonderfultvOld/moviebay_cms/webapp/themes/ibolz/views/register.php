<?php
	$company	= array('id'=>'bill_company', 'class'=>'form-control', 'name'=>'company', 'value'=> set_value('company'));
	$first		= array('id'=>'bill_firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname'));
	$last		= array('id'=>'bill_lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname'));
	$email		= array('id'=>'bill_email', 'class'=>'form-control', 'name'=>'email', 'value'=>set_value('email'));
	$phone		= array('id'=>'bill_phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone'));
?>
<div class="container main-container headerOffset">  
	<h1 class="section-title-inner"><span><i class="fa fa-lock"></i> <?php echo lang('form_register');?></span></h1>

	<?php echo form_open('secure/register', 'class="regForm" role="form" novalidate="novalidate"'); ?>
	<div class="row userInfo">      
		<div class="col-xs-12 ">
	      	<h2 class="block-title-2"> Create an account </h2>
	     </div>
	    <div class="col-xs-12 col-sm-6">
			<input type="hidden" name="submitted" value="submitted" />
			<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

			<div class="form-group">
				<label for="account_firstname"><?php echo lang('account_firstname');?></label>
				<?php echo form_input($first);?>
			</div>
		
			<div class="form-group">
				<label for="account_email"><?php echo lang('account_email');?></label>
				<?php echo form_input($email);?>
			</div>
		
			<div class="form-group">
				<label for="account_password"><?php echo lang('account_password');?></label>
				<input type="password" name="password" class="form-control" autocomplete="off" />
			</div>

			<div class="form-group">
				<label for="account_confirm"><?php echo lang('account_confirm');?></label>
				<input type="password" name="confirm" class="form-control" autocomplete="off" />
			</div>	        
	    </div>
	    <div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<label for="account_lastname"><?php echo lang('account_lastname');?></label>
				<?php echo form_input($last);?>
			</div>

			<div class="form-group">
				<label for="account_phone"><?php echo lang('account_phone');?></label>
				<?php echo form_input($phone);?>
			</div>

			<div class="form-group">
				<label for="company"><?php echo lang('account_company');?></label>
				<?php echo form_input($company);?>
			</div>						
			<div class="form-group">
				<div class="checkbox">
	              	<label>
	                	<input type="checkbox" name="email_subscribe" value="1" class="hide" />
	                	<?php echo lang('account_newsletter_subscribe'); ?>
	            	</label>
	            </div>				
			</div>	        
	    </div>
	    <div class="col-xs-12">
	    	<button type="submit" class="btn btn-primary"><i class="fa fa-user"></i> Create an account</button>		    
    	</div>
	</div>
	</form>

</div>