<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- 
    Smart developers always View Source. 
    
    This application was built using GCart (CodeIgiter), an open source framework
    for building rich e-commerce applications. 
    
    // -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo (!empty($seo_title)) ? $seo_title .' - ' : ''; echo $this->config->item('company_name'); ?></title>
	<?php if(isset($meta)):?> 
		<?php echo $meta;?> 
	<?php else:?>
		<meta name="Keywords" content="Shopping Cart, eCommerce, Code Igniter">
		<meta name="Description" content="iBolz vod management powered by GCart ver 2.3">
	<?php endif;?>


	<link href="<?php echo theme_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/style.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/skin-5.css'); ?>" rel="stylesheet" type="text/css" >
	<link href="<?php echo theme_url('assets/css/owl.carousel.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/owl.theme.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/ion.checkRadio.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/ion.checkRadio.cloudy.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/jquery.minimalect.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/jquery.mCustomScrollbar.css'); ?>" rel="stylesheet">
	<link href="<?php echo theme_url('assets/css/product-panel.css'); ?>" rel="stylesheet">

	<script type="text/javascript" src="<?php echo theme_url('assets/js/jquery/1.8.3/jquery.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/jquery.cycle2.min.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/jquery.easing.1.3.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/jquery.parallax-1.1.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/helper-plugins/jquery.mousewheel.min.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/helper-plugins/jquery.blockUI.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/helper-plugins/jquery.lazyload.min.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/jquery.mCustomScrollbar.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/ion-checkRadio/ion.checkRadio.min.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/grids.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/owl.carousel.min.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/jquery.minimalect.min.js'); ?>"></script> 
	<script type="text/javascript" src="<?php echo theme_url('assets/js/bootstrap.touchspin.js'); ?>"></script> 	
	<script type="text/javascript" src="assets/js/holder.js"></script> 	

	<?php
		if(isset($additional_header_info)){
			echo $additional_header_info;
		}
	?>
</head>

<body>	
<!-- Modal Login start -->
<div class="modal signUpContent fade" id="ModalLogin" tabindex="-1" role="dialog" >
  <?php $this->load->view('partials/login'); ?>
</div> <!-- /.Modal Login --> 	

<!-- Fixed navbar start -->
<div class="navbar navbar-tshop navbar-fixed-top x-megamenu <?php if(!isset($homepage)): ?>one-edge-shadow<?php endif; ?>" role="navigation">	
	<div class="container">	
  		<div class="row">  			
			<div class="col-xs-3 navbar-header">				      
		      <a class="navbar-brand" href="<?php echo site_url();?>">
		      	<img src="<?php echo theme_img('logo.png'); ?>" style="height:60px;"/>	      	
		      </a>
			</div>
	        <div class="col-xs-9 no-padding">
	        	<div class="navbar-top">
		        	<div class="pull-right">
						<ul class="userMenu">
							<li> 
					        	<a href="<?php echo site_url('cart/view_cart');?>" title="Cart">
					        		<i class="fa fa-shopping-cart"> </i> 
					        		<!-- <span class="cartRespons"> Cart </span> -->
					        	</a>					        	
				        	</li>		              								
						<?php if($this->Customer_model->is_logged_in(false, false)):?>
							<li>
								<a href="<?php echo site_url('secure');?>" title="<?php echo lang('my_account') ?>">
									<i class="fa fa-user"> </i> 
									<?php //echo lang('my_account') ?>
								</a>
							</li>							
							<li>
								<a href="<?php echo site_url('secure/logout');?>" title="<?php echo lang('logout') ?>">
									<i class="fa fa-sign-out"> </i> 
								</a>
							</li>
						<?php else: ?>
		              		<li> 
		              			<a href="#"  data-toggle="modal" data-target="#ModalLogin" title="Login"> 
		              				<span class="hidden-xs">Login</span> 
		              				<i class="fa fa-unlock-alt visible-xs"></i>
		              			</a> 
		              		</li>
		              		<li> 
		              			<a href="<?php echo site_url('secure/register'); ?>" title="register"> 
		              				<span class="hidden-xs">Register</span> 
		              				<i class="fa fa-unlock-alt visible-xs"></i>
		              			</a> 
		              		</li>
		              	<?php endif ?>
		            	</ul>
		          	</div>
	          	</div>		          	
	          	<div class="pull-right">
		          	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		          		<span class="sr-only"> Toggle navigation </span> 
		          		<span class="icon-bar"> </span> 
		          		<span class="icon-bar"> </span> 
		          		<span class="icon-bar"> </span> 
		          	</button>
				    <div class="navbar-collapse collapse">
				      <ul class="nav navbar-nav">
				      	<?php if($this->categories): ?>
					        <?php foreach($this->categories[0] as $category_root):?>
					        	<li class="dropdown x-megamenu-fullwidth"> 
					        		<a href="<?php echo site_url($category_root->slug);?>">
					        			<?php echo $category_root->name;?>
					        			<?php if( isset($this->categories[$category_root->id]) ): ?><b class="caret"></b><?php endif; ?>
					        		</a>
					        		<?php if( isset($this->categories[$category_root->id]) ): ?>
							        <ul class="dropdown-menu megamenu-dropdown-content pull-right">
							            <li class="megamenu-content">
							              <ul class="x-list-inline unstyled x-no-margin-left">
						              		<?php foreach($this->categories[$category_root->id] as $category_children):?>
						              			<li class="col-md-12" > <a href="<?php echo site_url($category_root->slug.'/'.$category_children->slug);?>"> <?php echo $category_children->name;?> </a> </li>
						              		<?php endforeach;?>						              	
							              </ul>
							            </li>
							        </ul>	   
							        <?php endif; ?>						        
					        	</li>
							<?php endforeach;?>
						<?php else: ?>   
							<li><a>&nbsp;</a></li>
						<?php endif; ?>   
				       </ul>
			    	</div>	
		    	</div>		
		    	       	
	        </div>	        
  		</div>	
	</div>		
</div> <!-- /.Fixed navbar  -->  

<?php if(isset($homepage)): ?>
    <?php $this->banners->show_collection(1, 5, 'home_banner');?>
<?php endif; ?>

<?php 
/**
	include breadcrumb template
	the page consists of 'breadcrumb', 'search-box'
*/
	$this->load->view('partials/breadcrumb');
?>	

<?php
/*
End header.php file
*/