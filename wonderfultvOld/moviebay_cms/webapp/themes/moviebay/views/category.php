<div class="container main-container headerOffset">  
  
    <?php if(isset($category)): ?>

        <?php if(isset($this->categories[$category->id] ) && count($this->categories[$category->id]) > 0):  ?>
            <?php foreach($this->categories[$category->id] as $subcategory):?>
                <div class="row" id="products-<?php echo $subcategory->slug; ?>">
                    <?php $this->banners->show_products($subcategory->name, false, 'slider_products', $subcategory->slug);?>
                </div>
            <?php endforeach;?>            
        <?php elseif($products): ?>
            <?php //$this->banners->show_products($category->name, $products, 'default_products', $category->slug);?>
            
            <div class="row product_panel paddTop20 bg-loader hidden" id="content_preview"></div>        
            <div class="row paddTop10">
                <h3 class="section-title">
                    <span><?php echo $category->name; ?> <i class="fa fa-angle-right"></i></span>
                </h3>
            </div>
            <div class="product_panel" id="content_product_list"> 
                <?php $this->load->view('partials/product_list'); ?>
            </div>
            
            <?php if($pagination['current_page'] < $pagination['total_pages']): ?>
                <div class="row padd">
                    <div class="load-more-block text-center">
                       <a class="btn btn-thin" href="#" id="btnLoadMore"> <i class="fa fa-plus-sign">+</i> load more products</a>
                    </div>                                        
                </div>
            <?php endif ?>

            <script type="text/javascript">
                var initPreviewProduct = function(){
                    $('.preview_product').on('click', function(ev){
                        ev.preventDefault();
                        var self = this;
                        $("body").animate({ scrollTop: $(self).offset().top-60}, "slow");
                        $('#content_preview').html('').insertAfter($(self).parent().parent()).removeClass('hidden');
                        app.preview_product('#content_preview', $(this).data('id'), {base_url:$('#breadcrumb_base_url').val() }, function(){
                            // $('#content_preview').insertAfter($(self).parent().parent());
                        });
                    });                    
                }
                $(document).ready(function(){
                    initPreviewProduct();
                    $('#btnLoadMore').on('click', function(ev){
                        ev.preventDefault();                        
                        if( $('ul.pagination > li.next > a').length > 0 ){                            
                            $.get($('ul.pagination > li.next > a').attr('href'), function(contentHtml){
                                $('div.pagination').remove();
                                $('#content_product_list').append(contentHtml);
                                if($('ul.pagination > li.next').length == 0){
                                    $('#btnLoadMore').parent().parent().hide();
                                }
                                initPreviewProduct();
                                $('.lazy').lazyload();
                            })
                        }
                        
                    });
                });
            </script>            
        <?php else: ?>
            <div class="alert alert-danger">
                <?php echo lang('no_products');?>
            </div>        
        <?php endif; ?>

    <?php endif;?>
  

</div>


