<?php if($banners): ?>
<div class="banner one-edge-shadow">
	<div class="container">

		<div class="slider-content">
			<ul id="pager-home-banner" class="container pager_slider"></ul>		
			<span class="prevControl sliderControl"> <i class="fa fa-angle-left fa-3x "></i></span> 
			<span class="nextControl sliderControl"> <i class="fa fa-angle-right fa-3x "></i></span>
		  	<div class="slider slider-v1" id="home-banner" 
		      data-cycle-swipe=true
		      data-cycle-prev=".prevControl"
		      data-cycle-next=".nextControl" data-cycle-loader="wait">      
				<?php
					$active_banner	= 'active ';
					foreach($banners as $idx => $banner):?>
						<div class="slider-item slider-item-img<?php echo $idx+1;?>">
							<?php									
							$banner_image	= '<img src="'.base_url('uploads/banners/'.$banner->image).'" class="img-responsive parallaximg sliderImg" />';
							if($banner->link){
								$target=false;
								if($banner->new_window){
									$target=' target="_blank"';
								}
								echo '<a href="'.$banner->link.'"'.$target.'>'.$banner_image.'</a>';
							}else{
								echo $banner_image;
							}
							?>
						</div>
					<?php 
				endforeach;?>		      	
			</div>
		</div>

	</div>
</div>	
<script type="text/javascript">
	$(document).ready(function () {	
		 $('#home-banner').cycle({
	        fx: 'scrollHorz', //Name of transition effect 
	        slides: '.slider-item',
	        timeout: 5000, // set time for nex slide 
	        speed: 1200,
	        easeIn: 'easeInOutExpo', // easing 
	        easeOut: 'easeInOutExpo',
	        pager: '#pager-home-banner', //Selector for element to use as pager container 
	    });

	});
</script>
<?php endif ?>