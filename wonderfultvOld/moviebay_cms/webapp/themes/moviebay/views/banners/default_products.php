<?php if(!isset($products) || count($products) == 0):?>
    <div class="alert alert-danger">
        <?php echo lang('no_products');?>
    </div>
<?php else: ?>    
    <script type="text/javascript">
    	$(document).ready(function(){
    		// app.preview_product('#content_preview .row', '<?php echo $products[0]->id; ?>', {base_url:$('#breadcrumb_base_url').val() });	
    		$('.preview_product').on('click', function(ev){
    			ev.preventDefault();
    			// $('#content_preview').attr('style="opacity:.5"');
    			// $("html, body").animate({ scrollTop: 20 }, "slow");
    			var self = this;
    			$("body").animate({ scrollTop: $(self).offset().top-60}, "slow");
    			app.preview_product('#content_preview', $(this).data('id'), {base_url:$('#breadcrumb_base_url').val() }, function(){
    				$('#content_preview').insertAfter($(self).parent().parent());//.show('slow');    				
    			});
    		});
    	});
    </script>
	<div class="row product_panel paddTop20" id="content_preview"></div>    	
	<h3 class="section-title">
		<span><?php echo $category->name; ?></span> <i class="fa fa-angle-right"></i>
	</h3>
	<div class="product_panel">	
		<?php $this->load->view('partials/product_list'); ?>
	</div>
<?php endif; ?>