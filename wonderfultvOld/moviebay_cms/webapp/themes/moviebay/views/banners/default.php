<?php if($banners): ?>
<div class="banner">
	<div class="container">
<div class="slider-content">
	<ul id="pager2" class="container"></ul>		
	<span class="prevControl sliderControl"> <i class="fa fa-angle-left fa-3x "></i></span> 
	<span class="nextControl sliderControl"> <i class="fa fa-angle-right fa-3x "></i></span>
	<!-- <div class="slider slider-v1" data-cycle-loader="wait"> -->
  	<div class="slider slider-v1" 
      data-cycle-swipe=true
      data-cycle-prev=".prevControl"
      data-cycle-next=".nextControl" data-cycle-loader="wait">      
		<?php
			$active_banner	= 'active ';
			foreach($banners as $idx => $banner):?>
				<div class="slider-item slider-item-img<?php echo $idx+1;?>"
					data-cycle-pager-template="<a href=#> <?php echo $banner->name ?> </a>"
				>
		          <!-- <div class="sliderInfo">
		            <div class="container">
		              <div class="col-lg-5 col-md-5 col-sm-5 pull-left sliderText dark alpha70 hidden-xs">
		                <div class="inner">
		                  <h1><?php echo $banner->name ?></h1>
		                  <a  href="#" class="slide-link">WATCH NOW <span class="arrowUnicode">►</span></a> </div>
		              </div>
		            </div>
		          </div> -->

					<?php									
					$banner_image	= '<img src="'.base_url('uploads/banners/'.$banner->image).'" class="img-responsive parallaximg sliderImg" />';
					if($banner->link)
					{
						$target=false;
						if($banner->new_window)
						{
							$target=' target="_blank"';
						}
						echo '<a href="'.$banner->link.'"'.$target.'>'.$banner_image.'</a>';
					}
					else
					{
						echo $banner_image;
					}
					?>

					<?php //if($banner->name): ?>
						<!-- <div class="carousel-caption">
							<h4><?php //echo $banner->name ?></h4>
						</div> -->
					<?php //endif; ?>								
				</div>
			<?php 
		endforeach;?>		      	
	</div>
</div>

	</div>
</div>	
<?php endif ?>