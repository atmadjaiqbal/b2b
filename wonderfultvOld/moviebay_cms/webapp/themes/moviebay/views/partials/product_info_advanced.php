<?php
	$photos = array();
    $photo  = theme_img('no_picture.png');
    $product->images    = array_values($product->images);
    if(!empty($product->images[0])){
        $primary = $product->images[0];
        foreach($product->images as $photo){
        	$photos[] = $photo;
            if(isset($photo->primary)){
                $primary    = $photo;
            }
        }
        $photo  = (($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename);
    }
    echo "<input type='hidden' id='photo_{$product->id}' value='{$photo}' />";
?>	
<div class="product_panel">        
    <div class="col-xs-4 no-padding-left">
        <div class="box-panel">
            <div class="poster bgfull x-two-edge-shadow-1" style="background-image:url(<?php echo $photo; ?>);"></div>
            <div class="paddTop20">
                <h3 class="pull-left">
                	<?php echo ($product->price > 0) ? format_currency($product->price) : '<span class="label label-success">FREE TO WATCH</span>'; ?>
                </h3>
                <?php if($product->price > 0): ?> 
                	<button id="btn_add_to_cart" class="btn btn-primary pull-right">BUY</button>
            	<?php endif ?>
                <div class="clearfix"></div>
            </div>
        </div>
        <?php if($photos): ?>
        <div class="x-box-panel">
            <h3 class="title">PHOTOS</h3>
            <div class="row">
                <div class="col-xs-12">
                	<?php foreach($photos as $thumb): ?>                	
	                    <div class="col-xs-4 no-padding">
	                        <?php if(isset($thumb->filename)): ?>                 			
	                			<img src="<?php echo config_item('digital_content_thumbnail_path').$thumb->filename; ?>" class="img-responsive thumb" />
		                	<?php else: ?>
		                		<img src="http://placehold.it/220x220" class="img-responsive thumb" />
		                	<?php endif ?>
	                    </div>
                	<?php endforeach ?>
                </div>   
            </div>   
        </div><!-- /photos -->
        <?php endif ?>
    </div>
    <div class="col-xs-8 no-padding">
        <div class="box-panel">
            <!-- <div class="player"> -->
                <!-- <img src="http://placehold.it/800x420/444444/EEEEEE/&text=PLAYER+800x420" class="img-responsive img-polaroid two-edge-shadow-1" /> -->
            <!-- </div> -->
            <iframe class="player bg-loader" src="http://moviebay2.ibolz.tv/assets/flash/ibolz-1.0.6/index.php?w=800&h=420&apps_id=com.balepoint.ibolz.moviebay&version=1.0.3&type=<?php echo 'channel'; ?>&id=<?php echo $product->sku; ?>"></iframe>
        </div>
        <?php if(isset($item->trailer_id) && $item->trailer_id): ?>
        <div class="box-panel">
            <h3 class="title">TRAILER</h3>               
            <div class="row">
                <div class="col-xs-12">
                    <div class="trailer col-xs-4 bgfull" style="background-image:url(http://placehold.it/205x120/444444/EEEEEE/&text=TRAILER+205x120);"></div>
                </div>
            </div>
        </div><!-- /trailer -->
        <?php endif ?>

        <div class="box-panel">                
            <h3 class="title">DESCRIPTION</h3>
            <h3 class="site-color"><?php echo $product->name; ?></h3> 
            <div class="row">
                <div class="col-xs-12">
                    <dl class="dl-horizontal">
                    	<dt>Code</dt><dd><strong><?php echo $product->sku; ?></strong>&nbsp;</dd>                    	
                    	<dt>Category</dt><dd><?php echo $item->category_name; ?>&nbsp;</dd>      
                    	<?php if(isset($item->prod_year) && $item->prod_year > 0): ?>
                    		<dt>Year</dt><dd><?php echo $item->prod_year; ?>&nbsp;</dd>
                    	<?php endif ?>
						<dt>Genre</dt>
						<dd>
							<?php if(isset($item->genre) && $item->genre): ?>
								<?php foreach($item->genre as $genre): ?>
									<a href="#"><?php echo $genre->genre_name; ?></a>
								<?php endforeach ?>
							<?php endif ?>
							&nbsp;
						</dd>

						<?php if(isset($item->crew) && $item->crew): ?>
							<?php foreach($item->crew as $key => $crews): ?>
								<dt><?php echo ucfirst($key); ?></dt>
								<dd>
									<?php foreach($crews as $crew): ?>
										<a href="#"><?php echo $crew->crew_name; ?></a>
									<?php endforeach ?>
									&nbsp;
								</dd>
							<?php endforeach ?>
						<?php endif ?>

                        <dt>SYNOPSIS</dt><dd class="desc"><?php echo $product->description; ?>&nbsp;</dd>
                    </dl>
                </div>                                    
            </div>
        </div><!-- /description -->

        <?php if(isset($item->related) && $item->related && $item->content_type != 'channel'): ?>
	        <div class="box-panel">                
				<?php if(count($item->related) > 4): ?>
					<div class="pull-right">
		    			<a data-id="slider-<?php echo $product->slug; ?>" class="prevContent link carousel-nav"> <i class="fa fa-angle-left"></i></a>
		    			<a data-id="slider-<?php echo $product->slug; ?>" class="nextContent link carousel-nav"> <i class="fa fa-angle-right"></i></a>
		    		</div>
		    	<?php endif ?>
	            <h3 class="title">RELATED</h3>
	            <div class="row">
	            	<div class="col-xs-12">
		            	<div class="content_slider owl-carousel owl-theme" id="slider-<?php echo $product->slug; ?>">
		            		<?php foreach($item->related as $relate): ?>
			            		<a href="<?php echo site_url($base_url.'/'.$relate->slug); ?>" class="item">
				                   <div class="related bgfull" style="background-image:url(<?php echo config_item('digital_content_thumbnail_path').$relate->video_thumbnail ?>);">
										<div class="overlay-bottom">
											<?php echo $relate->title; ?>
											<?php if($relate->prod_year) echo ' - '.$relate->prod_year; ?>
										</div>
				                   </div>
				                </a>   
			                <?php endforeach ?>
		                </div>   
	                </div>   
	            </div>   
	        </div><!-- /related -->
        <?php endif ?>

    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('dd a').on('click', function(ev){
            ev.preventDefault();
        });
        if($('.content_slider').length > 0){
            $('.content_slider').each(function(idx, slider){
                var newcontent_slider = $(slider).owlCarousel({
                    navigation: false, pagination: false,
                    items: 4,
                });
                $(".nextContent[data-id="+$(slider).attr('id')+"]").click(function() {
                    newcontent_slider.trigger('owl.next');
                })
                $(".prevContent[data-id="+$(slider).attr('id')+"]").click(function() {
                    newcontent_slider.trigger('owl.prev');
                })
            })
        }    	

        $('.thumb').on('click', function(ev){
            ev.preventDefault();
            $('.poster').removeAttr('style').attr('style', 'background-image:url('+$(this).attr('src')+')');
        });

        $('#btn_add_to_cart').on('click', function(){
            $('#form_add_to_cart').submit();
        });
    });
</script>