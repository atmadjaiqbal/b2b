<?php
	if(is_array($base_url)) $base_url = implode('/', $base_url);
?>

<div class="row paddTop10">
	<?php foreach ($products as $key_index => $product): ?>
		<?php if($key_index > 0 && $key_index % 6 == 0): ?>
			</div> 
			<div class="row paddTop10">
		<?php endif ?>
		<?php		
	        $photo  = theme_img('no_picture.png');
	        $product->images  = array_values($product->images);
	        if(!empty($product->images[0])){
	            $primary    = $product->images[0];
	            foreach($product->images as $photo){
	                if(isset($photo->primary)){
	                    $primary    = $photo;
	                }
	            }
	            $photo  = (($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename);
	        }
	    ?>
		<div class="col-xs-6 col-sm-3 col-md-2 item one-edge-shadow">			
			<div class="product_title">
				<a href="<?php echo site_url($base_url.'/'.$product->slug); ?>">
					<span class="pull-left"><?php echo ellipsize($product->name, 10); ?></span>
					<span class="pull-right"><i class="fa fa-angle-right"></i></span>
				</a>
				<div class="clearfix"></div>
			</div> 
			<div class="related potrait bgfull preview_product lazy" data-id="<?php echo $product->id; ?>" data-original="<?php echo $photo; ?>"></div>
		</div>
	<?php endforeach; ?>
</div> 

<!-- // is hidden pagination -->
<?php echo $pagination['links'] ?>
