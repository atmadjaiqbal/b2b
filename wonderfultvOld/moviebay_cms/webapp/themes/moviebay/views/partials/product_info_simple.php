<?php
	if(isset($simple)){
	    $photo  = theme_img('no_picture.png');
	    $product->images    = array_values($product->images);
	    if(!empty($product->images[0])){
	        $primary = $product->images[0];
	        foreach($product->images as $photo){
	            if(isset($photo->primary)){
	                $primary    = $photo;
	            }
	        }
	        $photo  = (($primary->imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$primary->filename) : config_item('digital_content_thumbnail_path').$primary->filename);
	    }
	    echo "<input type='hidden' id='photo_{$product->id}' value='{$photo}' />";
	}
?>	
	<div class="col-xs-8 no-padding-left">	
		<a href="<?php echo site_url($base_url.'/'.$product->slug); ?>">
         	<h2 class="site-color"><?php echo $product->name; ?></h2> 
         </a>	
		<div class="row">
            <div class="col-xs-12">
                <dl class="dl-horizontal">
					<!-- <dt>Category</dt><dd><?php echo $item->category_name; ?>&nbsp;</dd> -->

					<?php if(isset($item->genre) && $item->genre): ?>
						<dt>Genre</dt>
						<dd>
							<?php foreach($item->genre as $genre): ?>
								<a href="#"><?php echo $genre->genre_name; ?></a>
							<?php endforeach ?>
							&nbsp;
						</dd>
					<?php endif ?>					

					<?php if(isset($item->crew) && $item->crew): ?>
						<?php foreach($item->crew as $key => $crews): ?>
							<?php if($key != 'director' && $crews): ?>
							<dt><?php echo ucfirst($key); ?></dt>
							<dd>
								<?php foreach($crews as $crew): ?>
									<a href="#"><?php echo $crew->crew_name; ?></a>
								<?php endforeach ?>
								&nbsp;
							</dd>
							<?php endif ?>
						<?php endforeach ?>
					<?php endif ?>					

					<dt>Synopsis</dt>
					<dd class="desc"><?php echo ellipsize($product->description, (isset($simple) ? 100 : 500) ); ?> </dd>
				</dl>
			</div>
		</div>
	</div>
	<div class="col-xs-3 col-xs-offset-1">					
		<div class="border-title">
			<div class="col-xs-3 text-center"><i class="social fa fa-thumbs-o-up"></i><br/>&nbsp;<?php echo $item->ulike; ?></div>
	  		<div class="col-xs-3 text-center"><i class="social fa fa-thumbs-o-down"></i><br/>&nbsp;<?php echo $item->unlike; ?></div>
	  		<div class="col-xs-3 text-center"><i class="social fa fa-comments"></i><br/>&nbsp;<?php echo $item->comment; ?></div>
	  		<div class="col-xs-3 text-center"><i class="social fa fa-share"></i><br/>&nbsp;Share</div>
	  		<div class="clearfix"></div>
		</div>
		<div class="border-title">		
			<button type="button" class="btn btn-primary btn-lg pull-right">BUY</button>
			<!-- <div class="label <?php //echo ($product->price > 0) ? 'label-danger' : 'label-success'; ?> col-xs-12">
				<h3 class="section-title text-center">			
					<?php //echo ($product->price > 0) ? format_currency($product->price) : 'FREE'; ?>			
				</h3>		
			</div> -->
			<div class="clearfix"></div>
		</div>		
	</div>