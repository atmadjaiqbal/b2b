  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title-site text-center" > Login to <?php echo $this->config->item('company_name'); ?> </h3>
      </div>
      <div class="modal-body">
      	<?php echo form_open('secure/login'); ?>
			<input type="hidden" value="secure" name="redirect"/>
			<input type="hidden" value="submitted" name="submitted"/>
	        <div class="form-group login-username">
	          <div >
	            <input name="email" id="login-user" class="form-control input"  size="20" placeholder="Enter Email" type="text">
	          </div>
	        </div>
	        <div class="form-group login-password">
	          <div >
	            <input name="password" id="login-password" class="form-control input"  size="20" placeholder="Password" type="password" autocomplete="off">
	          </div>
	        </div>
	        <div class="form-group">
	            <div class="checkbox login-remember">
	              	<label>
	                	<input name="remember" value="true" checked="checked" type="checkbox">
	                	<?php echo lang('keep_me_logged_in');?> 
	            	</label>
	            </div>
	        </div>
	        <div >
	          <div >
	            <input name="submit" class="btn  btn-block btn-lg btn-primary" value="LOGIN" type="submit">
	          </div>
	        </div>
        </form>       
      </div>
      <div class="modal-footer">
        <p class="text-center"> 
        	Not here before? 
        	<a href="<?php echo site_url('secure/register'); ?>"> Sign Up. </a> <br>
          	<a href="<?php echo site_url('secure/forgot_password'); ?>"> Lost your password? </a> </p>
      </div>
    </div> <!-- /.modal-content -->         
  </div> <!-- /.modal-dialog -->     