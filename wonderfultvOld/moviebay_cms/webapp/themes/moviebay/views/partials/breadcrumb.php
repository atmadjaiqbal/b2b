<div class="breadcrumbDiv animated <?php if(!isset($homepage)): ?>breadcrumb-fixed-top<?php endif; ?>">
	<div class="container">	
		<div class="row">  				

			<div class="search-full text-right"> 
				<a class="pull-right search-close"> <i class=" fa fa-times-circle"> </i> </a>
				<div class="searchInputBox pull-right">
				  <input type="search" data-searchurl="search?=" name="q" placeholder="start typing and hit enter to search" class="search-input">
				  <button class="btn-nobg search-btn" type="submit"> <i class="fa fa-search"> </i> </button>
				</div>
			</div>

			<ul class="breadcrumb col-xs-9">
				<li><a href="<?php echo site_url();?>"><i class="fa fa-home fa-lg"></i></a></li>
				<?php
					$breadcrumb_base_url = '';
					if(!empty($base_url) && is_array($base_url)):
						$url_path	= '';
						$count	 	= 1;
						foreach($base_url as $bc):							
							$url_path .= '/'.$bc;
							$breadcrumb_base_url = $url_path;
							if($count == count($base_url)):?>								
								<li class="active"><?php echo $bc;?></li>
							<?php else:?>
								<li><a href="<?php echo site_url($url_path);?>"><?php echo $bc;?></a></li>
							<?php endif;
							$count++;
						endforeach;
					endif;
				?>
				<input type="hidden" id="breadcrumb_base_url" value="<?php echo ($breadcrumb_base_url == '') ? '/' : $breadcrumb_base_url; ?>" />
			</ul>
			<div class="col-xs-3">
				 <div class="search-box">
		          	<div class="input-group">
		            	<button class="btn btn-nobg site-color" type="button"> <i class="fa fa-search"> </i> </button>
		          	</div>          
		        </div>
			</div>

		</div>
	</div>
</div>
