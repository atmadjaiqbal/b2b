<?php
	$company	= array('id'=>'company', 'class'=>'form-control', 'name'=>'company', 'value'=> set_value('company', $customer['company']));
	$first		= array('id'=>'firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname', $customer['firstname']));
	$last		= array('id'=>'lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname', $customer['lastname']));
	$email		= array('id'=>'email', 'class'=>'form-control', 'name'=>'email', 'value'=> set_value('email', $customer['email']));
	$phone		= array('id'=>'phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone', $customer['phone']));
	$password	= array('id'=>'password', 'class'=>'form-control', 'name'=>'password', 'value'=>'');
	$passwordnew	= array('id'=>'password', 'class'=>'form-control', 'name'=>'password', 'value'=>'');
	$confirm	= array('id'=>'confirm', 'class'=>'form-control', 'name'=>'confirm', 'value'=>'');
?>	
<div class="container main-container headerOffset">  
	<?php if(validation_errors()):?>
		<div class="alert allert-error">
			<a class="close" data-dismiss="alert">×</a>
			<?php echo validation_errors();?>
		</div>
	<?php endif;?>
	<div class="row">
		<div class="col-xs-12">
		  <h1 class="section-title-inner"><span><i class="glyphicon glyphicon-user"></i> My personal information </span></h1>
		  <div class="row userInfo">
		    <div class="col-lg-12">
		      <h2 class="block-title-2"> Please be sure to update your personal information if it has changed. </h2>
		    </div>
		    <?php echo form_open('secure/my_account'); ?>
		      <div class="col-xs-12 col-sm-6">
		        <div class="form-group required">
		          <label for="InputName">First Name <sup>*</sup> </label>
		          <?php echo form_input($first);?>
		        </div>
		        <div class="form-group required">
		          <label for="InputLastName">Last Name <sup>*</sup> </label>
		          <?php echo form_input($last);?>
		        </div>
		        <div class="form-group">
		          <label for="InputEmail"> Email </label>
		          <?php echo form_input($email);?>
		        </div>
		        <div class="form-group">
		          <label for="InputEmail"> Phone </label>
		          <?php echo form_input($phone);?>
		        </div>
		        <div class="form-group">
		          <label>Date of Birth</label>
		          <div class="row">
		            <div class="col-xs-4">
		              <select class="form-control" id="days" name="days">
		                <option selected>-</option>
		                <?php for ($i=1; $i < 32; $i++): ?>
		                	<option value="<?php echo $i; ?>"><?php echo $i; ?>&nbsp;&nbsp;</option>
		                <?php endfor; ?>
		              </select>
		            </div>
		            <div class="col-xs-4">
		              <select class="form-control" name="months" id="months">
		                <option selected>-</option>
		                <option value="1">January&nbsp;</option>
		                <option value="2">February&nbsp;</option>
		                <option value="3">March&nbsp;</option>
		                <option value="4">April&nbsp;</option>
		                <option value="5">May&nbsp;</option>
		                <option value="6">June&nbsp;</option>
		                <option value="7">July&nbsp;</option>
		                <option value="8">August&nbsp;</option>
		                <option value="9">September&nbsp;</option>
		                <option value="10">October&nbsp;</option>
		                <option value="11">November&nbsp;</option>
		                <option selected="selected" value="12">December&nbsp;</option>
		              </select>
		            </div>
		            <div class="col-xs-4">
		              <select class="form-control" name="years" id="years">
		                <option selected>-</option>
		                <?php for ($i=2013-60; $i < 2013; $i++): ?>
		                	<option value="<?php echo $i; ?>"><?php echo $i; ?>&nbsp;&nbsp;</option>
		                <?php endfor; ?>
		              </select>
		            </div>
		          </div>
		        </div>
		      </div>
		      <div class="col-xs-12 col-sm-6">		      	
		        <div class="form-group required">
		          <label for="InputPasswordCurrent"> Password </label>
		          <?php echo form_password($password);?>
		        </div>
		        <div class="form-group required">
		          <label for="InputPasswordnew"> New Password </label>
		          <?php echo form_password($passwordnew);?>
		        </div>
		        <div class="form-group required">
		          <label for="InputPasswordnewConfirm"> Confirm Password </label>
		          <?php echo form_password($confirm);?>
		        </div>
				<p class="clearfix">
		            <sup>*</sup><small>If you do not wish to change your password, leave both fields blank.</small>
		         </p>		      			        
		      </div>
		      <div class="col-lg-12">
		        <div class="form-group ">
		          <p class=" clearfix">
		            <input type="checkbox" value="1" name="newsletter" id="newsletter">
		            <label for="newsletter">Sign up for our newsletter!</label>
		          </p>
		          <p class="clearfix">
		            <input type="checkbox"  value="1" id="optin" name="optin">
		            <label for="optin">Receive special offers from our partners!</label>
		          </p>
		        </div>
		      </div>
		      <div class="col-lg-12">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> &nbsp; <?php echo lang('form_submit');?> </button>
				<ul class="pager">
			        <li class="next"><a href="/secure"> &larr; Back to My Account</a></li>
			    </ul>		      	
		      </div>
		    </form>
		  </div>
		  <!--/row end--> 		  
		</div>
	</div>
	<!--/row-->
</div>