<?php //$this->banners->show_collection(2, 3, '3_box_row');?>

<div class="container main-container">	

  <div class="row margin-bottom-md" id="products-new-content">
  	<?php $this->banners->show_products('NEW CONTENT', false, 'slider_products', 'new-content');?>
  </div>
  <div class="row margin-bottom-md" id="products-top-movies">
  	<?php $this->banners->show_products('TOP MOVIES', false, 'slider_products', 'top-movies');?>
  </div>
  <div class="row margin-bottom-md" id="products-tv-series">
  	<?php $this->banners->show_products('TV SERIES', false, 'slider_products', 'tv-series');?>
  </div>

</div> <!--/main-container-->	
