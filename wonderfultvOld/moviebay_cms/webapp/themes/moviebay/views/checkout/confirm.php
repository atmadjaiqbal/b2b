<div class="container main-container headerOffset"> 

	<div class="page-header">
		<h2><?php echo lang('form_checkout');?></small></h2>
	</div>
		
	<?php include('order_details.php');?>
	<?php include('summary.php');?>

	<div class="row">
		<div class="span12">
			<a id="btnSubmitConfirm" class="btn btn-primary btn-large btn-block" href="<?php echo site_url('checkout/place_order');?>"><?php echo lang('submit_order');?></a>
		</div>
	</div>

</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#btnSubmitConfirm').on('click', function(ev){
			ev.preventDefault();
			$.blockUI({
				message: 'Coming soon!<br /> this feature will bring on next release.',
				timeout: 2000
			});
		})		
	});
</script>

