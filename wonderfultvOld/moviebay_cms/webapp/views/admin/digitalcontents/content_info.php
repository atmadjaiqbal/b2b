<?php if($item): ?>
<table cellpadding="10px">
	<tr>
		<td class="span4" style="vertical-align:top">
			<img src="<?php echo $item->video_thumbnail; ?>" class="img-polaroid" onError="$(this).attr('src', 'http://placehold.it/150x250'); this.onError = null;" />
		</td>
		<td class="span8" style="vertical-align:top">
			<i class="label label-important pull-right"><?php echo $item->content_id; ?></i>
			<p class="lead"><?php echo ellipsize($item->title, 20); ?></p>						
			<div>
				<dl class="dl-horizontal">	
					<dt>Type:</dt>
					<dd>
						<?php echo $item->content_type; ?> &nbsp;
					</dd>
					<dt>Category:</dt>
					<dd>
						<?php echo $item->category_name; ?><br />
						<?php if($this->auth->check_access('Developer')): ?>
							<span class="color-thin"><?php echo $item->content_category_id; ?></span>
						<?php endif; ?>
					</dd>
					<?php if($item->genre): ?>
						<dt>Genre:</dt>
						<dd>
							<?php foreach($item->genre as $genre): ?>
								<i class="label label-success"><?php echo $genre->genre_name; ?></i>
							<?php endforeach ?>
							&nbsp;
						</dd>
					<?php endif ?>					
					<?php if($item->crew): ?>
						<?php foreach($item->crew as $key => $crews): ?>
							<dt><?php echo ucfirst($key); ?>:</dt>
							<dd>
								<?php foreach($crews as $crew): ?>
									<i class="label"><?php echo $crew->crew_name; ?></i>
								<?php endforeach ?>
								&nbsp;
							</dd>
						<?php endforeach ?>
					<?php endif ?>					
					<dt>Synopsis:</dt>
					<dd><?php echo $item->description; ?>&nbsp;</dd>
				</dl>
			</div>
		</td>
	</tr>
</table>
<?php else: ?>
	<div class="alert alert-info"><?php echo $error_string; ?></div>
<?php endif; ?>