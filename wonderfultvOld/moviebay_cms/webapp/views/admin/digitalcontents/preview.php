<?php if($item): ?>
<table cellpadding="10px">
	<tr>
		<td class="span2" style="vertical-align:top">
			<img src="<?php echo config_item('digital_content_thumbnail_path').$item->video_thumbnail; ?>" class="img-rounded"  onError="$(this).attr('src', 'http://placehold.it/125x125/&text=NO+IMAGE'); this.onError = null;" />
		</td>
		<td class="span10" style="vertical-align:top">
			<div>
				<dl class="dl-horizontal">					
					<?php if($show_product && isset($item->product) && $item->product): ?>
						<?php $product = $item->product; ?>
						<dt>ID:</dt><dd><?php echo $product->id; ?>&nbsp;</dd>
						<dt>Title:</dt><dd><?php echo ellipsize($product->name, 25); ?>&nbsp;</dd>						
						<dt>Code:</dt><dd><?php echo $product->sku; ?>&nbsp;</dd>
						<dt>Price:</dt><dd><?php echo format_currency($product->price); ?>&nbsp;</dd>
						<?php if(isset($item->product_categories) && $item->product_categories): ?>
							<dt>Categories:</dt>
							<dd>
								<?php foreach ($item->product_categories as $product_category): ?>
									<span class="label label-default"><?php echo $product_category->name; ?></span>
								<?php endforeach; ?>
								&nbsp;
							</dd>
						<?php endif; ?>	
					<?php else: ?>
						<dt>ID:</dt><dd><?php echo $item->id; ?>&nbsp;</dd>
						<dt>Title:</dt><dd><?php echo $item->title; ?> &nbsp;</dd>
						<dt>Category:</dt><dd><?php echo $item->category_name; ?> &nbsp;</dd>
						<?php if($show_product): ?>
							<dt>Product:</dt><dd><span class="label label-important">Unkown</span></dd>
						<?php endif; ?>	
					<?php endif; ?>							
					<?php if($item->description): ?>
						<dt>Synopsis:</dt>					
						<dd><?php echo ellipsize($item->description, 100); ?></dd>
					<?php endif; ?>								
				</dl>
			</div>
		</td>
	</tr>
</table>
<?php else: ?>
	<div class="alert alert-info">Missing digital content!</div>
<?php endif; ?>