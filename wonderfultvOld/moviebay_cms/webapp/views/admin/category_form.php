<?php echo form_open_multipart($this->config->item('admin_folder').'/categories/form/'.$id); ?>

<div class="row-fluid">
	<div class="span12">
		<div class="widget-box">

			<div class="widget-title">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#description_tab" data-toggle="tab"><?php echo lang('description');?></a></li>
					<li><a href="#attributes_tab" data-toggle="tab"><?php echo lang('attributes');?></a></li>
					<li><a href="#seo_tab" data-toggle="tab"><?php echo lang('seo');?></a></li>
				</ul>
			</div>
			<div class="widget-content tab-content">
				<div class="tab-pane active" id="description_tab">
					
					<fieldset>
						<div class="row">
							<div class="span8">
								<label for="name"><?php echo lang('name');?></label>
								<?php
									$data	= array('name'=>'name', 'value'=>set_value('name', $name), 'class'=>'span12');
									echo form_input($data);
								?>			
							</div>
			        		<div class="span4">
								<label for="ibolz_channel_id">Ibolz Channel</label>
								<select name="ibolz_channel_id" id="ibolz_channel_id" class="span12">
									<option value="">None</option>
									<?php
										function list_ibolz_channels($parent_id, $cats, $sub='', $ibolz_channel_id) {
											foreach ($cats[$parent_id] as $cat): ?>
												<option value="<?php echo  $cat['id']; ?>" <?php if($cat['id'] == $ibolz_channel_id): ?>selected<?php endif; ?> >
													<?php echo  $sub.$cat['name']; ?> 
												</option>
												<?php
												if (isset($cats[$cat['id']]) && sizeof($cats[$cat['id']]) > 0){
													$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
													$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
													list_ibolz_channels($cat['id'], $cats, $sub2, $ibolz_channel_id);
												}
											endforeach;
										}						
										list_ibolz_channels('0', $ibolz_channels_tree, '', $ibolz_channel_id);			
									?>
								</select>
							</div>
	        			</div>

						<div class="row">
							<div class="span8">
								<label for="parent_id"><?php echo lang('parent');?> </label>
								<?php
									$data	= array(0 => lang('top_level_category'));
									foreach($categories as $parent){
										if($parent->id != $id){
											$data[$parent->id] = $parent->name;
										}
									}
									echo form_dropdown('parent_id', $data, $parent_id);
								?>		        												
		        			</div>
		        			<div class="span4">
								<label for="display_on_menu">Display On Menu</label>
				        		<?php echo form_dropdown('display_on_menu', array('1' => lang('yes'), '0' => lang('no')), set_value('display_on_menu', $display_on_menu)); ?>		        				
							</div>
	        			</div>

						<label for="description"><?php echo lang('description');?></label>
						<?php
							$data	= array('name'=>'description', 'class'=>'redactor', 'value'=>set_value('description', $description));
							echo form_textarea($data);
						?>

					</fieldset>
				</div>

				<div class="tab-pane" id="attributes_tab">
					
					<fieldset>
						<label for="enabled"><?php echo lang('enabled');?> </label>
        				<?php echo form_dropdown('enabled', array('1' => lang('enabled'), '0' => lang('disabled')), set_value('enabled',$enabled)); ?>		        				

						<label for="slug"><?php echo lang('slug');?> </label>
						<?php
							$data = array('name'=>'slug', 'value'=>set_value('slug', $slug));
							echo form_input($data);
						?>
						
						<label for="sequence"><?php echo lang('sequence');?> </label>
						<?php
						$data	= array('name'=>'sequence', 'value'=>set_value('sequence', $sequence));
						echo form_input($data);
						?>
																				
						<label for="image"><?php echo lang('image');?> </label>
						<div class="input-append">
							<?php echo form_upload(array('name'=>'image'));?>
						</div>					
						<?php if($id && $image != ''):?>				
						<div style="text-align:center; padding:5px; border:1px solid #ddd;"><img src="<?php echo base_url('uploads/images/small/'.$image);?>" alt="current"/><br/><?php echo lang('current_file');?></div>				
						<?php endif;?>
						
					</fieldset>
					
				</div>
				
				<div class="tab-pane" id="seo_tab">
					<fieldset>
						<label for="seo_title"><?php echo lang('seo_title');?> </label>
						<?php
						$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'span12');
						echo form_input($data);
						?>
						
						<label><?php echo lang('meta');?></label> 
						<?php
						$data	= array('rows'=>5, 'name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'span12');
						echo form_textarea($data);
						?>
						<p class="help-block"><?php echo lang('meta_data_description');?></p>
					</fieldset>
				</div>
			</div>

		</div>

			<div class="form-actions">
				<button type="button" class="btn btn-default" id="btnCancelForm"><?php echo lang('cancel');?></button>
				<button type="submit" class="btn btn-primary"><?php echo lang('save');?></button>
			</div>
		</form>

	</div>	
</div>
<script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>