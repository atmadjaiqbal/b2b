<div id="toolbar-page">
	<button type="button" class="btn btn-small btn-danger" onClick="generateProducts()"><i class="fa fa-inbox"></i> Generate Products</button>
	  <!-- <a class="btn" href="<?php //echo site_url($this->config->item('admin_folder').'/digital_products/form');?>"><i class="icon-plus-sign"></i> <?php //echo lang('add_file');?></a> -->	
</div>	
<div class="pull-right">
	<form class="form-search" action="<?php echo site_url($this->config->item('admin_folder').'/digital_products');?>">
	  <input type="text" name="search_key" value="<?php echo isset($search_key) ? $search_key : ''; ?>" placeholder="Search Key" class="input-medium">
	  <button type="submit" class="btn"><i class="icon icon-search"></i></button>
	  <a class="btn" href="<?php echo site_url($this->config->item('admin_folder').'/digital_products');?>"><i class="fa fa-refresh"></i></a>	  
	</form>			
</div>
<div class="clearfix"></div>

<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Poster</th>
			<th><?php echo lang('title');?></th>			
			<th>Content Category</th>
			<th>Product Related</th>
		</tr>
	</thead>
	<tbody>
	<?php echo (count($file_list) < 1)?'<tr><td style="text-align:center;" colspan="6">There are no contents</td></tr>':''?>
	<?php foreach ($file_list as $idx => $file): ?>
		<tr>
			<td class="span2" nowrap>
				<a href="#" class="detail_content" data-id="<?php echo $file->id; ?>" data-title="<?php echo ellipsize($file->title, 50); ?>" <?php if(isset($file->product) && $file->product) echo 'data-published="yes"'; ?>	 >
					<?php echo strtoupper($file->id); ?>
				</a>
			</td>
			<td class="span1">
				<img data-original="<?php echo config_item('digital_content_thumbnail_path').$file->video_thumbnail; ?>?w=150" class="lazy img-rounded" style="max-width:150px;" />
			</td>
			<td class="span4">
				<?php echo $file->title; ?>
				<?php if($file->description): ?>
				<div><strong>Synopsis:</strong></div><div><?php echo ellipsize($file->description, 200); ?></div>
				<?php endif; ?>
			</td>				
			<td class="span2">
				<?php echo $file->category_name; ?>
				<?php if($this->auth->check_access('Developer')): ?>
					<div class="color-thin"><?php echo $file->content_category_id; ?></div>
				<?php endif; ?>
			</td>		
			<td class="span3">
				<?php if(isset($file->product) && $file->product): ?>				
					<dl class="dl-horizontal" style="margin-top:0px;">
						<?php $product = $file->product; ?>
						<dt>Product:</dt><dd><?php echo ellipsize($product->name, 25); ?>&nbsp;</dd>
						<dt>ID:</dt><dd><?php echo $product->id; ?>&nbsp;</dd>
						<dt>Code:</dt><dd><?php echo $product->sku; ?>&nbsp;</dd>
						<dt>Price:</dt><dd><?php echo format_currency($product->price); ?>&nbsp;</dd>
						<?php if(isset($file->product_categories) && $file->product_categories): ?>
							<dt>Categories:</dt>
							<dd>
								<?php foreach ($file->product_categories as $product_category): ?>
									<span class="label label-default"><?php echo $product_category->name; ?></span>
								<?php endforeach; ?>
								&nbsp;
							</dd>
						<?php endif; ?>						
					</dl>					
				<?php else: ?>
					<span class="label label-important" id="save_as_product_<?php echo $file->id; ?>">Unkown</span>
				<?php endif; ?>
			</td>				
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<?php if(isset($pagination)): ?>
	<div class="row">	
		<div class="span3">
			page <?php echo $pagination['current_page'] ?> of <?php echo $pagination['total_pages'] ?> pages, 
			total records <?php echo $pagination['total_rows'] ?>
		</div>		
		<div class="span9">
			<div class="pull-right">
				<?php echo $pagination['links'] ?>
			</div>	
		</div>		
	</div>
<?php endif; ?>					

<script type="text/javascript">
	function areyousure(){
		return confirm('<?php echo lang('confirm_delete_file');?>');
	}

	var _generateProduct = function(indexProcess, id, callback){
		$.get("<?php echo site_url('/api/content/event/save'); ?>",{content_id: id}, function(resp){
    		$('div.blockUI.blockMsg').html('<strong>'+resp.rcode+'</strong><br />'+resp.message);
    		indexProcess++;
    		if(callback) callback(indexProcess, resp.rcode);
    	});		
	}

	var generateProducts = function(){		                    		
		var items = [];
		$('a.detail_content').each(function(idx, item){			
			var id = $(item).data('id');
			if($('#save_as_product_'+id).length > 0){
				items.push(item);
			}
		})
		if( items.length > 0 ){			
			var callback = function(indexProcess){
				if(indexProcess < items.length){
					_generateProduct(indexProcess, $(items[indexProcess]).data('id'), callback);
				}else{
					setTimeout(function() {
	                   window.location = window.location;
	                }, 1000);					
				}				
			};
			$.blockUI();
			_generateProduct(0, $(items[0]).data('id'), callback);			
		}else{
			alert('All contents has assosiated with product');
		}		
	}

	$(document).ready(function(){
		 $("img.lazy").lazyload({
		 	effect : "fadeIn"
		 });

		 $('a.detail_content').on('click', function(ev){
		 	ev.preventDefault();
		 	var self = this;
		 	BootstrapDialog.show({
		 		title: $(self).data('title'),
				data: {
	            	remoteContentUrl: "<?php echo site_url($this->config->item('admin_folder').'/digital_products/show_content_info'); ?>/"+$(self).data('id')
	            },
				message: function(dialog) {
	                var $message = $('<div style="min-height:250px;"></div>');
	                var pageToLoad = dialog.getData('remoteContentUrl');
	                $message.load(pageToLoad, function(){
	                	if($(self).data('published') == undefined) dialog.getButton('btn-publish').show();
	                });
	                return $message;
	            },	            
				buttons: [
					{
						id: 'btn-publish',
		                label: 'Create Product',
		                cssClass: 'btn-danger',
		                action: function(dialog) {
		                	$.blockUI();
		                    _generateProduct(0, $(self).data('id'), function(indexProcess, rcode){
		                    	setTimeout(function() {
			                    	if(rcode != 'FAILED'){
			                    		window.location = window.location;
			                    	}else{
			                    		$.unblockUI();
			                    	}	                    					                    	
			                    }, (rcode != 'FAILED') ? 1000 : 3000);
		                    });
		                }
	            	},{
		                label: 'Close',
		                action: function(dialog) {
		                    dialog.close();
	                	}
	            	}
	            ],
	            onshow: function(dialog) {
	            	dialog.getButton('btn-publish').hide();
	            },
		 	});
		 });
	});    
</script>