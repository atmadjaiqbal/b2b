<div class="pull-right">
	<a class="btn" href="<?php echo site_url(config_item('admin_folder').'/banners/banner_collection_form'); ?>"><i class="icon-plus-sign"></i> <?php echo lang('add_new_banner_collection');?></a>
</div>
<div class="clearfix"></div>
<table class="table table-striped">
	<thead>
		<tr>
			<th><?php echo lang('name');?></th>
			<th style="text-align:center">Image Size</th>
			<th></th>
		</tr>
	</thead>
	<?php echo (count($banner_collections) < 1)?'<tr><td style="text-align:center;" colspan="5">'.lang('no_banner_collections').'</td></tr>':''?>
	<?php if ($banner_collections): ?>
	<tbody>
	<?php

	foreach ($banner_collections as $banner_collection):?>
		<tr>
			<td><?php echo $banner_collection->name;?></td>
			<td style="text-align:center">
				<?php echo $banner_collection->image_width;?> x <?php echo $banner_collection->image_height;?>
			</td>
			<td>
				<div class="btn-group pull-right">					
					<a class="btn btn-small" href="<?php echo base_url(config_item('admin_folder').'/banners/banner_collection/'.$banner_collection->banner_collection_id);?>"><i class="fa fa-image"></i> <?php echo lang('banners');?></a>					
					<a class="btn btn-small" href="<?php echo site_url(config_item('admin_folder').'/banners/banner_collection_form/'.$banner_collection->banner_collection_id);?>"><i class="fa fa-pencil"></i></a>					
					<a class="btn btn-small btn-danger" href="<?php echo site_url(config_item('admin_folder').'/banners/delete_banner_collection/'.$banner_collection->banner_collection_id);?>" onclick="return areyousure();"><i class="fa fa-trash"></i></a>					
				</div>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	<?php endif;?>
</table>

<script type="text/javascript">
function areyousure(){
	return confirm('<?php echo lang('confirm_delete_banner_collection');?>');
}
</script>