<?php if($this->auth->check_access('Developer')): ?>
<div id="toolbar-page">
	<a class="btn btn-small" href="<?php echo site_url(config_item('admin_folder').'/comments/form'); ?>"><i class="icon-plus-sign"></i> Add New Comment</a>
</div>
<?php endif; ?>	

<div class="row-fluid">	
	<div class="span4">
		<?php if(isset($items) && count($items) > 0): ?>			
			<?php if($this->auth->check_access('Admin')): ?>
				<a  href="#" id="btnRemoveAll" class="btn btn-small btn-danger"><i class="icon-white icon-trash"></i></a>
			<?php endif; ?>
		<?php endif; ?>	
	</div>		
	<div class="span8">
		<div class="pull-right">
		<?php echo form_open(config_item('admin_folder').'/comments', 'class="form-inline" id="filter_form"');?>
			<input type="text" name="term" value="<?php echo ($this->input->post('term')) ? $this->input->post('term') : ''; ?>" placeholder="Search key" /> 
			<button type="submit" class="btn" value="search"><i class="fa fa-search"></i></button>
			<a class="btn" href="<?php echo site_url(config_item('admin_folder').'/comments');?>"><i class="fa fa-refresh"></i></a>
		<?php echo form_close(); ?>
		</div>
	</div>
</div>	

<div class="row-fluid">	
	<div class="span12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th class="span1"><input class="pull-left" type="checkbox" name="check_all" id="check_all" /></th>
					<th>Content</th>
					<th>User</th>
					<th class="span2">Date</th>
					<th class="span4">Comment</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php if(isset($items) && $items): ?>		
				<?php foreach ($items as $idx => $item):?>
					<tr>
						<td nowrap>					
							<input class="pull-left" type="checkbox" name="check_item[]" value="<?php echo $item->comment_id; ?>" />
							<div class="pull-left" style="margin:0px 5px">
								<?php echo (($pagination['current_page']-1)*$pagination['per_page'])+($idx+1); ?>
							</div>
						</td>
						<td><?php echo $item->title; ?></td>
						<td>
							<?php echo $item->firstname; ?> <?php echo $item->lastname; ?>
						</td>
						<td nowrap><?php echo date('M,d Y H:i', strtotime($item->created)); ?></td>
						<td><?php echo $item->comment_text; ?></td>				
						<td>
							<div class="btn-group" style="float:right;">
								<a class="btn btn-small" href="<?php echo site_url(config_item('admin_folder').'/comments/form/'.$item->comment_id);?>"><i class="fa fa-pencil"></i></a>
								<a class="btn btn-small btn-danger btn-remove" href="<?php echo $item->comment_id; ?>"><i class="fa fa-trash"></i></a>	
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>		
				<tr>
					<td colspan="6"><div class="alert">There are currently no comments</div></td>
				</tr>
			<?php endif ?>		
			</tbody>
		</table>
	</div>		
</div>
<?php if(isset($pagination) && $pagination['links']): ?>
	<div class="row-fluid">	
		<div class="span3">
			page <?php echo $pagination['current_page'] ?> of <?php echo $pagination['total_pages'] ?> pages, 
			total records <?php echo $pagination['total_rows'] ?>
		</div>		
		<div class="span9">
			<div class="pull-right">
				<?php echo $pagination['links'] ?>
			</div>	
		</div>		
	</div>
<?php endif; ?>		


<script type="text/javascript">
	$(document).ready(function(){
		$('.btn-remove').on('click', function(ev){
			ev.preventDefault();
			var comment_id = $(this).attr('href');
			BootstrapDialog.confirm('Remove comment, are you sure?', function(result){
	            if(result) {
					$.post('/api/content/comment/remove', {comment_id: comment_id}, function(json){
						if(json.rcode == 'OK'){
							window.location = '<?php echo site_url(config_item("admin_folder")."/comments"); ?>';
						}else{
							BootstrapDialog.alert({
								title: json.rcode,
								message: json.message
							});
						}
					}, 'json');
	            }
	        });
		});

		$('#check_all').on('change', function(){
			if($(this).is(':checked')){
				$('input[name="check_item[]"]').attr('checked', 'checked');
			}else{
				$('input[name="check_item[]"]').removeAttr('checked');
			}
		})

		$('#btnRemoveAll').on('click', function(ev){
			ev.preventDefault();
			var params = [];
			var check_items = $('input[name="check_item[]"]');
			if(check_items.length > 0){
				$(check_items).each(function(idx, item){
					if( $(item).is(':checked') ) params.push($(item).val());
				});								
			}
			if(params.length > 0){
				BootstrapDialog.confirm('Remove all comments, are you sure?', function(result){
	            	if(result) {
						$.blockUI();
						$.post('<?php echo site_url(config_item("admin_folder")."/comments/deletes"); ?>', {comment_id:params}, function(resp){
							window.location = resp.redirect;
						}, 'json');
					}
				});
			}
		})

	});
</script>