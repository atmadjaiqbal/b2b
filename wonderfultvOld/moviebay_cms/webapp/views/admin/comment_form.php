<div class="row-fluid">	
	<div class="span4">
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"><i class="icon-search"></i></span><h5>Search for Content</h5>
			</div>
			<div class="widget-content">
				<form class="form-vertical">
					<div class="control-group">
						<div class="input-append">
						  <input type="text" id="content_search" placeholder="Search key ...">
						  <button type="button" class="btn" onclick="$('#content_search').trigger('keyup')"><i class="fa fa-search"></i></button>
						</div>					
					</div>					
					<div class="control-group">
						<select id="contentlist" size="15" class="span11" ></select>
					</div>
				</form>
			</div>
		</div>
	</div>					
	<div class="span8">
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"><i class="icon-align-justify"></i></span><h5>Comment Form</h5>
			</div>
			<div class="widget-content nopadding">
				<?php echo form_open(config_item('admin_folder')."/comments/form/{$id}", array('id'=>'form-comment', 'class'=>'form-horizontal')); ?>
					<?php if($id): ?>
						<input type="hidden" name="comment_id" value="<?php echo $id; ?>" />
					<?php endif; ?>
					<div class="control-group">
						<label class="control-label">Content</label>
						<div class="controls">
							<input type="hidden" name="content_id" id="content_id">
							<div class="row-fluid">
								<div id="preview_container" class="span10">
									<div class="alert alert-info no-margin">Choose the content on the left side <?php echo $content_id; ?></div>
								</div>
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Customer / User</label>
						<div class="controls">
							<select name="customer_id" id="customer_id">
								<?php if(isset($customers)): ?>
									<?php foreach($customers as $key => $customer): ?>
										<option value="<?php echo $customer->ibolz_account_id; ?>" <?php if($customer_id == $customer->ibolz_account_id): ?>selected<?php endif; ?> >
											<?php echo $customer->firstname; ?> <?php echo $customer->lastname; ?>
										</option>
									<?php endforeach; ?>
								<?php endif; ?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label for="comment_text" class="control-label">Text Comment</label>
						<div class="controls">
							<textarea name="comment_text" id="comment_text" rows="4"><?php echo $comment_text; ?></textarea>
						</div>
					</div>
					<div class="form-actions">
						<button type="button" class="btn btn-default" id="btnCancelForm">Cancel</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>			
				</form>
			</div>
		</div>						
	</div>
</div>

<script type="text/javascript">
	var run_files_query = function(){
		$.getJSON("<?php echo site_url($this->config->item('admin_folder').'/digital_products/ajax_search');?>", { 
			search_key: $('#content_search').val()
		},function(data) {									
			$('#contentlist').empty();
			$.each(data, function(index, file){									
				$('#contentlist').append('<option value="'+file.id+'">'+file.title+'</option>');
			});
		});
	}
	var _add_content = function(file_id){
		var dg_container = $('<div class="well well-small nopadding" style="min-height:142px;"></div>');
		$(dg_container).attr('id', 'preview_'+file_id);
		// $.blockUI({message: 'loading digital content...'});
		$.get("<?php echo site_url($this->config->item('admin_folder').'/digital_products/preview'); ?>/"+file_id, {product:'no'}, function(content){
			$(dg_container).append(content);													
			$('#preview_container').html(dg_container);
			$('input[name="content_id"]').val(file_id);
			// $.unblockUI();
		})
	}
	var add_digital_product = function(){
		var filelist_values = $('#contentlist option:checked');
		if(filelist_values.length >0){
			$(filelist_values).each(function(idx, option){
				var file_id = $(option).val();
				_add_content(file_id);
			});								
		}
		return false;
	}

	$(document).ready(function(){
		$('#form-comment').on('submit', function(ev){
			ev.preventDefault();
			var formParams = $(this).serializeArray();
			$.post('/api/content/comment/save', formParams, function(json){
				if(json.rcode == 'OK'){
					window.location = '<?php echo site_url(config_item("admin_folder")."/comments"); ?>';
				}else{
					BootstrapDialog.alert({
						title: json.rcode,
						message: json.message
					});
				}
			}, 'json');
			return false;
		});

		$('#content_search').on('keyup', function(ev){
			ev.preventDefault();
			$('#contentlist').empty();
			run_files_query();
		});

		$('#contentlist').on('change', function(ev){
			ev.preventDefault();
			add_digital_product();
		});

		<?php if($content_id): ?>
			_add_content('<?php echo $content_id; ?>');
		<?php endif; ?>
	});
</script>