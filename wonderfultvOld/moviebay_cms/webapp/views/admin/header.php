<?php 
    $admin_url = site_url($this->config->item('admin_folder')).'/';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo config_item('company_name'); ?> <?php echo (isset($page_title))?' :: '.$page_title:''; ?></title>

<link rel="stylesheet" href="<?php echo base_url('assets/admin-bootstrap-2/css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/admin-bootstrap-2/css/bootstrap-responsive.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/admin-bootstrap-2/css/bootstrap-datetimepicker.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/admin-bootstrap-2/css/unicorn.main.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/admin-bootstrap-2/css/unicorn.grey.css'); ?>" class="skin-color" />
<link rel="stylesheet" href="<?php echo base_url('assets/admin-bootstrap-2/css/uniform.css'); ?>" />  
<link rel="stylesheet" href="<?php echo base_url('assets/admin-bootstrap-2/css/font-awesome.css'); ?>" /> 
<link rel="stylesheet" href="<?php echo base_url('assets/admin-bootstrap-2/css/animate.css'); ?>" />  
<link rel="stylesheet" href="<?php echo base_url('assets/css/redactor.css');?>" />

<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/jquery.min.js');?>"></script>
</head>
<body>
    <div id="header">
        <h1><a href="<?php echo $admin_url;?>"><?php echo config_item('company_name'); ?></a></h1>
    </div>
    <div id="user-nav" class="navbar navbar-inverse">
        <ul class="nav btn-group">            
            <?php if($this->auth->check_access('Developer')): ?>
            <li class="btn btn-inverse dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a class="sAdd" title="" href="#">new message</a></li>
                    <li><a class="sInbox" title="" href="#">inbox</a></li>
                    <li><a class="sOutbox" title="" href="#">outbox</a></li>
                    <li><a class="sTrash" title="" href="#">trash</a></li>
                </ul>
            </li>
            <!-- <li class="btn btn-inverse">
                <a href="#"><i class="icon icon-user"></i> <span class="text">Profile</span></a>
            </li> -->
            <?php endif; ?> 
            <li class="btn btn-inverse">
                <a href="<?php echo site_url($this->config->item('admin_folder').'/login/logout');?>"><i class="icon icon-share-alt"></i> <span class="text"><?php echo lang('common_log_out') ?></span></a>
            </li>
        </ul>
    </div>        
    <div id="sidebar">
        <ul>
            <li>
                <a href="<?php echo site_url($this->config->item('admin_folder').'/dashboard');?>">
                    <i class="fa fa-home"></i> <span><?php echo lang('common_dashboard') ?></span>
                </a>
            </li>            

            <li>
                <a href="<?php echo $admin_url;?>digital_products">
                    <i class="fa fa-film"></i> <span><?php echo lang('common_digital_products') ?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $admin_url;?>categories">
                    <i class="fa fa-cubes"></i> <span><?php echo lang('common_categories') ?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $admin_url;?>products">
                    <i class="fa fa-inbox"></i> <span><?php echo lang('common_products') ?></span>
                </a>
            </li>                
            <li>
                <a href="<?php echo $admin_url;?>packages">
                    <i class="fa fa-gift"></i> <span>Packages</span>
                </a>
            </li>

            <li>
                <a href="<?php echo $admin_url;?>comments">
                    <i class="fa fa-comments"></i> <span>Comments</span>
                </a>
            </li>
            
            <li class="submenu">
                <a href="#">
                    <i class="fa fa-newspaper-o"></i> <span><?php echo lang('common_content') ?></span>
                </a>
                <ul>
                    <li><a href="<?php echo $admin_url;?>banners"><?php echo lang('common_banners') ?></a></li>
                    <li><a href="<?php echo $admin_url;?>pages"><?php echo lang('common_pages') ?></a></li>
                </ul>
            </li>

            <li class="submenu">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i> <span><?php echo lang('common_sales') ?></span>
                </a>
                <ul>
                    <li><a href="<?php echo $admin_url;?>orders"><?php echo lang('common_orders') ?></a></li>
                    <?php if($this->auth->check_access('Admin')) : ?>
                        <li><a href="<?php echo $admin_url;?>customers"><?php echo lang('common_customers') ?></a></li>
                        <li><a href="<?php echo $admin_url;?>customers/groups"><?php echo lang('common_groups') ?></a></li>
                        <li><a href="<?php echo $admin_url;?>reports"><?php echo lang('common_reports') ?></a></li>
                        <?php if($this->auth->check_access('Developer')): ?>
                        <li><a href="<?php echo $admin_url;?>coupons"><?php echo lang('common_coupons') ?></a></li>
                        <li><a href="<?php echo $admin_url;?>giftcards"><?php echo lang('common_giftcards') ?></a></li>
                        <?php endif; ?>
                    <?php endif; ?>
                </ul>
            </li>    
            <?php if($this->auth->check_access('Admin')): ?>
            <li class="submenu">
                <a href="#">
                    <i class="fa fa-cogs"></i> <span><?php echo lang('common_administrative') ?></span>
                </a>
                <ul>                    
                    <li><a href="<?php echo $admin_url;?>settings"><?php echo lang('common_gocart_configuration') ?></a></li>
                    <?php if($this->auth->check_access('Developer')): ?>
                        <li><a href="<?php echo $admin_url;?>shipping"><?php echo lang('common_shipping_modules') ?></a></li>
                        <li><a href="<?php echo $admin_url;?>payment"><?php echo lang('common_payment_modules') ?></a></li>
                        <li><a href="<?php echo $admin_url;?>settings/canned_messages"><?php echo lang('common_canned_messages') ?></a></li>                    
                        <li><a href="<?php echo $admin_url;?>locations"><?php echo lang('common_locations') ?></a></li>
                    <?php endif; ?>
                    <li><a href="<?php echo $admin_url;?>admin"><?php echo lang('common_administrators') ?></a></li>
                </ul>
            </li>
            <?php endif; ?>        
        </ul>    
    </div>   
    <div id="content">
        <?php if(!empty($page_title)):?>
            <div id="content-header">
                <h1><?php echo $page_title; ?></h1>
                <div class="btn-group"></div>
            </div>
        <?php endif;?>    
        <div id="breadcrumb">
            <a href="<?php echo $admin_url; ?>" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a>
            <a class="current">Dashboard</a>
        </div>
        <div class="container-fluid">
            <?php
                if($this->session->flashdata('message')){
                    $message    = $this->session->flashdata('message');
                }        
                if($this->session->flashdata('error')){
                    $error  = $this->session->flashdata('error');
                }        
                if(function_exists('validation_errors') && validation_errors() != ''){
                    $error  = validation_errors();
                }
            ?>    
            <div class="row-fluid">
                <div id="js_error_container" class="alert alert-error" style="display:none;"> 
                    <p id="js_error"></p>
                </div>    
                <div id="js_note_container" class="alert alert-note" style="display:none;"></div>    
                <?php if (!empty($message)): ?>
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert">×</a>
                        <?php echo $message; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($error)): ?>
                    <div class="alert alert-error">
                        <a class="close" data-dismiss="alert">×</a>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>
            </div>        
            <!-- <div class="row-fluid"> -->
            
            