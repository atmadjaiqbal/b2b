			<!-- </div> -->
			<div class="row-fluid">
				<div id="footer" class="span12">
					2014 &copy; Admin Tools. Brought to you by iBolz Team
				</div>
			</div>
		</div><!-- /container-fluid -->
	</div><!-- /content -->

	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.js');?>"></script>
	<!--script type="text/javascript" src="<?php //echo base_url('assets/admin-bootstrap-2/js/jquery.ui.custom.js');?>"></script-->
	<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/jquery.uniform.js');?>"></script>	
	<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/bootstrap-datetimepicker.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/unicorn.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/bootstrap-dialog.js');?>"></script>	
	<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/jquery-plugins/jquery.blockUI.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/jquery-plugins/jquery.lazyload.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/admin-bootstrap-2/js/jquery-plugins/jquery.cooki.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/redactor.min.js');?>"></script>

	<script type="text/javascript">
	    $(document).ready(function(){
			
			// $('input[type=checkbox],input[type=radio],input[type=file]').uniform();	
			$('input[type=file]').uniform();	
			// $('select').select2();
    		// $('.datepicker').datepicker();

	        $('#btnCancelForm').on('click', function(ev){
	            window.location = '<?php echo $this->refering_url; ?>';
	        });
	        $('table.table-striped').addClass('table-bordered').css('margin-top', '10px');
	        $('.redactor').redactor({
	            minHeight: 200,
	            imageUpload: '<?php echo site_url(config_item('admin_folder').'/wysiwyg/upload_image');?>',
	            fileUpload: '<?php echo site_url(config_item('admin_folder').'/wysiwyg/upload_file');?>',
	            imageGetJson: '<?php echo site_url(config_item('admin_folder').'/wysiwyg/get_images');?>',
	            imageUploadErrorCallback: function(json){
	                alert(json.error);
	            },
	            fileUploadErrorCallback: function(json){
	                alert(json.error);
	            }
	        });
	        if( $('#toolbar-page').length > 0){
	            $('#content-header .btn-group').html( $('#toolbar-page').html() );
	            $('#toolbar-page').remove();
	            init_tooltip();
	        }
	    });
	</script>	
</body>
</html>