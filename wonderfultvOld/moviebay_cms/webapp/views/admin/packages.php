<div class="row-fluid">
	<div class="span12">
<?php
	$url_path = "packages";

//set "code" for searches ... it should be remove
$code = (!$code) ? '' : '/'.$code;

function sort_url($lang, $by, $sort, $sorder, $code, $admin_folder){
	if ($sort == $by){
		if ($sorder == 'asc'){
			$sort	= 'desc';
			$icon	= ' <i class="icon-chevron-up"></i>';
		}else{
			$sort	= 'asc';
			$icon	= ' <i class="icon-chevron-down"></i>';
		}
	}else{
		$sort	= 'asc';
		$icon	= '';
	}		
	$return = site_url($admin_folder.'/index/'.$by.'/'.$sort.'/'.$code);	
	echo '<a href="'.$return.'">'.lang($lang).$icon.'</a>';
}

	if(!empty($term)):
		$term = json_decode($term);
		if(!empty($term->term) || !empty($term->category_id)): ?>
			<div class="alert alert-info">
				<?php echo sprintf(lang('search_returned'), intval($total));?>
			</div>
		<?php endif;?>
	<?php endif;?>

	<div class="pull-right" id="toolbar-page">
		<a class="btn btn-small" href="<?php echo site_url($this->config->item('admin_folder').'/'.$url_path.'/form'); ?>"><i class="icon-plus-sign"></i> Add Package</a>		
	</div>
	<div class="row">
		<div class="span4">
			<?php if(count($products) > 0): ?>
				<?php if($this->auth->check_access('Developer')): ?>			
					<a id="btnRemoveAll" class="btn btn-small btn-danger" href="#"><i class="icon-white icon-trash"></i></a>
				<?php endif; ?>		
			<?php endif; ?>	
		</div>			
		<div class="span8">
			<div class="pull-right">
				<?php echo form_open($this->config->item('admin_folder').'/'.$url_path.'/index', 'class="form-inline" id="filter_form"');?>			
					<input type="text" name="term" value="<?php echo ($this->input->post('term')) ? $this->input->post('term') : ''; ?>" placeholder="<?php echo lang('search_term');?>" /> 
					<button type="submit" class="btn" value="search"><i class="fa fa-search"></i></button>
					<a class="btn" href="<?php echo site_url($this->config->item('admin_folder').'/'.$url_path.'/index');?>"><i class="fa fa-refresh"></i></a>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>	
	<div class="clearfix"></div>	
	<?php echo form_open($this->config->item('admin_folder').'/'.$url_path.'/bulk_save', array('id'=>'bulk_form'));?>
		<table class="table table-striped">
			<thead>
				<tr>						
					<th class="span1" style="text-align:center">
						<?php if($this->auth->check_access('Developer')): ?>
							<input type="checkbox" id="check_all_items" class="pull-left"/>
						<?php endif; ?>
					</th>						
					<th class="span2"><i class="fa fa-image"></i></th>					
					<th class="span3"><?php echo sort_url('name', 'name', $order_by, $sort_order, $code, $this->config->item('admin_folder').'/'.$url_path);?></th>
					<th class="span2"><?php echo sort_url('sku', 'sku', $order_by, $sort_order, $code, $this->config->item('admin_folder').'/'.$url_path);?></th>
					<th class="span2"><?php echo sort_url('price', 'price', $order_by, $sort_order, $code, $this->config->item('admin_folder').'/'.$url_path);?></th>
					<th class="span2"><?php echo sort_url('expired', 'expired', $order_by, $sort_order, $code, $this->config->item('admin_folder').'/'.$url_path);?></th>
					<th class="span1"><?php echo sort_url('product_promo', 'product_promo', $order_by, $sort_order, $code, $this->config->item('admin_folder').'/'.$url_path);?></th>
					<th class="span1">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			<?php echo (count($products) < 1)?'<tr><td style="text-align:center;" colspan="8"><div class="alert alert-info">'.lang('no_products').'</div></td></tr>':''?>
			<?php foreach ($products as $idx => $product): ?>					
				<tr>
					<td nowrap>							
						<?php if($this->auth->check_access('Developer')): ?>
							<input type="checkbox" name="check_item[]" value="<?php echo $product->id; ?>" />&nbsp;
						<?php endif; ?>
						<?php echo (($pagination['current_page']-1)*$pagination['per_page']) + ($idx+1); ?>
					</td>
					<td>
						<?php 
							$photo = 'http://placehold.it/125x125/&text=NO+IMAGE';
							if(isset($product->images) && !empty($product->images) && $product->images != 'false'){
								$product->images = array_values(json_decode($product->images, true));
								if(count($product->images) > 0){
									$photo = $product->images[0];
									$photo  = ($photo['imgpath'] != 'remote') ? base_url('uploads/images/thumbnails/'.$photo['filename']) : config_item('digital_content_thumbnail_path').$photo['filename'];
								}									
							}								
						?>						
						<img class="img-responsive lazy" data-original="<?php echo ($photo); ?>" />	
					</td>					
					<td>
						<?php echo $product->name; ?>
						<div class="label label-default">
							Number of contents
							<?php echo(isset($product->file_list)) ? count($product->file_list) : 0; ?>
						</div>
					</td>
					<td><?php echo $product->sku; ?></td>
					<td style="text-align:right"><?php echo $product->price; ?></td>
					<td style="text-align:center"><?php echo $product->expired; ?></td>
					<td style="text-align:center">
						<?php if($product->product_promo): ?>
							<span class="label label-important">
								<i class="icon icon-white icon-ok"></i>
							</span>
						<?php endif ?>
					</td>
					<td>
						<span class="btn-group pull-right">
							<?php if($this->auth->check_access('Developer')): ?>
								<a class="btn btn-small tip" href="<?php echo site_url($this->config->item('admin_folder').'/'.$url_path.'/form/'.$product->id.'/1');?>" title="Copy or duplicate product"><i class="fa fa-files-o"></i></a>
							<?php endif; ?>								
							<a class="btn btn-small" href="<?php echo  site_url($this->config->item('admin_folder').'/'.$url_path.'/form/'.$product->id);?>"  title="Edit product"><i class="fa fa-pencil"></i></a>
							<a class="btn btn-small btn-danger" href="<?php echo  site_url($this->config->item('admin_folder').'/'.$url_path.'/delete/'.$product->id);?>" onclick="return areyousure();"><i class="fa fa-trash"></i></a>
						</span>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	<?php echo form_close(); ?>
	</div>
</div>
<?php if(isset($pagination) && $pagination['links']): ?>
	<div class="row-fluid">	
		<div class="span3">
			page <?php echo $pagination['current_page'] ?> of <?php echo $pagination['total_pages'] ?> pages, 
			total records <?php echo $pagination['total_rows'] ?>
		</div>		
		<div class="span9">
			<div class="pull-right">
				<?php echo $pagination['links'] ?>
			</div>	
		</div>		
	</div>
<?php endif; ?>		


<script type="text/javascript">
	function areyousure(){
		return confirm('<?php echo lang('confirm_delete_product');?>');
	}

	$(document).ready(function(){
		$("img.lazy").lazyload({
		 	effect : "fadeIn"
		 });

		$('#check_all_items').on('change', function(){
			if($(this).is(':checked')){
				$('input[name="check_item[]"]').attr('checked', 'checked');
			}else{
				$('input[name="check_item[]"]').removeAttr('checked');
			}
		})

		$('#btnRemoveAll').on('click', function(ev){
			ev.preventDefault();
			$params = $('#bulk_form').serializeArray();
			$.blockUI();
			$.post('<?php echo site_url($this->config->item("admin_folder")."/{$url_path}"); ?>/deletes', $params, function(respJson){
				setTimeout(function(){
					$.unblockUI();
					window.location = respJson.redirect;
				}, 1000);
			}, 'json')
		})

		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd', startDate: new Date(),
	    	autoclose: true
		});
	});
</script>
