<script type="text/javascript">
	// Return a helper with preserved width of cells
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};

	function create_sortable(){
		$('#category_contents').sortable({
			scroll: true,
			helper: fixHelper,
			axis: 'y',
			update: function(){
				save_sortable();
			}
		});	
		$('#category_contents').sortable('enable');
	}

	function save_sortable(){
		serial = $('#category_contents').sortable('serialize');			
		$.ajax({
			url:"<?php echo site_url($this->config->item('admin_folder').'/categories/process_organization/'.$category->id); ?>",
			type:'POST',
			data:serial
		});
	}
	$(document).ready(function(){
		var firsttime = true;
		$('#product-search-form').on('submit', function(){
			$('#product_list').html('');
			$.post("<?php echo site_url($this->config->item('admin_folder').'/products/product_autocomplete/');?>", {
				name: $('#product_search').val(), 
				limit: <?php echo config_item('records_per_page'); ?>
			},function(data) {
				$('#new_product_list').empty();
				$.each(data, function(index, value){									
					if($('#content-'+index).length == 0){
						$('#new_product_list').append('<option value="'+index+'">'+value+'</option>');
					}
				});
				if(!firsttime && $('#new_product_list option').length == 0){
					$.blockUI({
						message: 'Your searched returned empty result!',
						timeout: 3000,
						onOverlayClick: function(){
							$.unblockUI();
						}
					});
				}		
				firsttime = false;			
			}, 'json');
			return false;
		});

		$('#btn-add-product').on('click', function(){
			var params = $('#product-add-form').serializeArray();
			if(params.length == 0){
				$.blockUI({
					message: 'There are no products selected to add!',
					onOverlayClick: function(){ $.unblockUI();}
				});
			}else{
				$.post('<?php echo site_url($this->config->item("admin_folder")."/categories/process_assosiation/{$category->id}");?>', params, function(data) {
					$.blockUI({
						message: data.message, timeout: 1500,
						onUnblock: function(){
							window.location = data.redirect + '?search_key='+$('#product_search').val();
						}
					});
				}, 'json');
			}
		});


		<?php if($this->input->get("search_key")): ?>
			$('#product_search').val('<?php echo $this->input->get("search_key"); ?>');
			$('#product-search-form').trigger('submit');
		<?php endif; ?>

		create_sortable();
		$("img.lazy").lazyload({
			effect : "fadeIn"
		});	
		$(document).scrollTop(5);		
	});
</script>
<div class="pull-right" id="toolbar-page">
	<a href="<?php echo site_url($this->config->item('admin_folder').'/categories'); ?>" class="btn btn-small">
		<i class="fa fa-chevron-left"></i> Back
	</a>
</div>
<div class="row-fluid">
	<div class="span12">

		<div class="widget-box">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                	<li class="active"><a data-toggle="tab" href="#tab-contents">Organize Contents</a></li>
                    <li><a data-toggle="tab" href="#tab-sequence">Organize Sequence</a></li>                    
                </ul>
            </div>
            <div class="widget-content tab-content">
                <div id="tab-contents" class="tab-pane active">
	            	<div class="row">
						<div class="span3">
							<div class="row">
								<div class="span12">
									<form id="product-search-form">
										<div class="input-append">
										  <input id="product_search" type="text" class="span11" placeholder="Search key ...">
										  <button type="submit" class="btn"><i class="fa fa-search"></i></button>
										</div>
									</form>
								</div>
							</div>
							<div class="row">
								<div class="span12">
									<form id="product-add-form">
										<select id="new_product_list" name="products[]" multiple size="15"></select>
										<button type="button" id="btn-add-product" class="btn btn-danger span11"><i class="fa fa-plus"></i> Add Products</button>
									</form>
								</div>
							</div>
						</div>
						<div class="span9">
							<table class="table table-striped">
								<thead>
									<tr>
										<th class="span1"><i class="fa fa-image"></i></th>
										<th class="span2"><?php echo lang('sku');?></th>
										<th class="span3"><?php echo lang('name');?></th>
										<th><?php echo lang('description');?></th>			
										<th class="span1">&nbsp;</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach ($category_products as $product):?>
									<tr id="content-<?php echo $product->id;?>">
										<td>
											<?php 
										        $photo  = theme_img('no_picture.png', lang('no_image_available'));
										        if($product->images && !empty($product->images)){
										            $primary = $product->images[0];
										            foreach($product->images as $image){
										                if(isset($image->primary)){
										                    $primary = $image;
										                }
										            }
										            $photo  = ($primary['imgpath'] != 'remote') ? base_url('uploads/images/thumbnails/'.$primary['filename']) : config_item('digital_content_thumbnail_path').$primary['filename'];
										        }									
											?> 
											<img class="img-responsive img-square lazy" data-original="<?php echo $photo; ?>" />
										</td>			
										<td><?php echo $product->sku;?></td>
										<td><?php echo $product->name;?></td>
										<td><?php echo ellipsize($product->description, 100);?></td>
										<td>
											<a class="btn btn-danger btn-small" href="<?php echo site_url($this->config->item("admin_folder")."/categories/remove_assosiation/{$category->id}?product_id={$product->id}"); ?>">
												<i class="icon-remove icon-white"></i>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
							<?php if(!$category_products): ?>
			                	<div class="row-fluid">
									<div class="span12">
										<div class="alert alert-danger">There are currently no product contents</div>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>                	
                </div>            	
                <div id="tab-sequence" class="tab-pane">
                	<?php if($category_products): ?>
	                	<div class="row-fluid">
							<div class="span12">
								<div class="alert alert-info"><?php echo lang('drag_and_drop');?></div>
							</div>
						</div>
					<?php endif; ?>
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="span1"><i class="fa fa-image"></i></th>
								<th class="span2"><?php echo lang('sku');?></th>
								<th class="span3"><?php echo lang('name');?></th>
								<th><?php echo lang('description');?></th>			
							</tr>
						</thead>
						<tbody id="category_contents">						
						<?php foreach ($category_products as $product):?>
							<tr id="product-<?php echo $product->id;?>" style="cursor:move;">
								<td>
									<?php 
								        $photo  = theme_img('no_picture.png', lang('no_image_available'));
								        if($product->images && !empty($product->images)){
								            $primary = $product->images[0];
								            foreach($product->images as $image){
								                if(isset($image->primary)){
								                    $primary = $image;
								                }
								            }
								            $photo  = ($primary['imgpath'] != 'remote') ? base_url('uploads/images/thumbnails/'.$primary['filename']) : config_item('digital_content_thumbnail_path').$primary['filename'];
								        }									
									?> 
									<img class="img-responsive img-square lazy" data-original="<?php echo $photo; ?>" />
								</td>			
								<td><?php echo $product->sku;?></td>
								<td><?php echo $product->name;?></td>
								<td><?php echo ellipsize($product->description, 100);?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
					<?php if(!$category_products): ?>
	                	<div class="row-fluid">
							<div class="span12">
								<div class="alert alert-danger">There are currently no product contents</div>
							</div>
						</div>
					<?php endif; ?>
                </div>

            </div>                            
        </div>		

	</div>
</div>