<style type="text/css">
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	.sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; height: 18px; }
	.sortable li>span { position: absolute; margin-left: -1.3em; margin-top:.4em; }
</style>

<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
	$(".sortable").sortable();
	$(".sortable > span").disableSelection();
	//if the image already exists (phpcheck) enable the selector
	photos_sortable();
	$("img.lazy").lazyload({
		effect : "fadeIn"
	});

	$('#expired_date').datepicker({
		format: 'yyyy-mm-dd', startDate: new Date(),
    	autoclose: true
	});
});

function add_product_image(data){
	p	= data.split('.');	
	var photo = '<?php add_image("'+p[0]+'", "'+p[0]+'.'+p[1]+'", '', '', '', 'local');?>';
	$('#gc_photos').append(photo);
	$('#gc_photos').sortable('destroy');
	photos_sortable();
}

function remove_image(img){
	if(confirm('<?php echo lang('confirm_remove_image');?>')){
		var id	= img.attr('rel');
		$.get('<?php echo base_url($this->config->item("admin_folder")."/products/product_image_delete/"); ?>', {filename:$('input[name="images['+id+'][filename]"]').val()}, function(out){
			$('#gc_photo_'+id).remove();		
		});		
	}
}

function photos_sortable(){
	$('#gc_photos').sortable({	
		handle : '.gc_thumbnail',
		items: '.gc_photo',
		axis: 'y',
		scroll: true
	});
}

function remove_option(id){
	if(confirm('<?php echo lang('confirm_remove_option');?>')){
		$('#option-'+id).remove();
	}
}
//]]>
</script>

<div class="row-fluid">
	<div class="span12">
	<?php echo form_open($this->config->item('admin_folder').'/products/form/'.$id ); ?>
		<div class="widget-box">
			<div class="widget-title">
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#product_info" data-toggle="tab"><?php echo lang('details');?></a></li>
						<li><a href="#product_categories" data-toggle="tab"><?php echo lang('categories');?></a></li>
						<li><a href="#product_downloads" data-toggle="tab"><?php echo lang('digital_content');?></a></li>
						<li><a href="#product_related" data-toggle="tab"><?php echo lang('related_products');?></a></li>
						<li><a href="#product_photos" data-toggle="tab"><?php echo lang('images');?></a></li>
						<li><a href="#product_seo" data-toggle="tab"><?php echo lang('header_information');?></a></li>
					</ul>
				</div>
			</div>
				<div class="widget-content tab-content">
					<div class="tab-pane active" id="product_info">
						<div class="row">
							<div class="span8">
								<label><?php echo lang('name');?></label>
								<?php
								$data= array('placeholder'=>lang('name'), 'name'=>'name', 'value'=>set_value('name', $name), 'class'=>'span12');
								echo form_input($data);
								?>
							</div>
							<div class="span4">
								<label><?php echo lang('sku');?></label>
								<?php
									$data	= array('placeholder'=>lang('sku'), 'name'=>'sku', 'value'=>set_value('sku', $sku));
									echo form_input($data);
								?>
							</div>
						</div>
						<div class="row">
							<div class="span4">
								<label for="price"><?php echo lang('price');?></label>
								<?php
									$data	= array('name'=>'price', 'value'=>set_value('price', $price));
									echo form_input($data);
								?>
							</div>
							<div class="span4">
								<!-- <label for="saleprice"><?php echo lang('saleprice');?></label> -->
								<?php
									// $data	= array('name'=>'saleprice', 'value'=>set_value('saleprice', $saleprice), 'class'=>'span4');
									// echo form_input($data);
								?>						
							</div>
							<div class="span4">
								<label>Status</label>
								<?php
								 	$options = array('1' => lang('enabled'), '0' => lang('disabled'));
									echo form_dropdown('enabled', $options, set_value('enabled',$enabled));
								?>
							</div>
						</div>

						<div class="row">
							<div class="span4">
								<label for="expired">Expired Date</label>
								<?php
									$data	= array('name'=>'expired', 'value'=>set_value('expired', $expired), 'class'=>'datepicker text-center span6', 'id'=>'expired_date');
									echo form_input($data);
								?>
							</div>							
							<div class="span4">
								<?php if($this->auth->check_access('Admin')): ?>
									<label>Type</label>
									<?php
								 		$optionsTypes = array('non-package'	=> 'Non-Package', 'package'	=> 'Package');
										echo form_dropdown('product_type', $optionsTypes, set_value('product_type', $product_type));
									?>
								<?php else: ?>
									<input type="hidden" name="product_type" value="<?php echo $product_type; ?>" />
								<?php endif; ?>
							</div>
							<div class="span4"></div>
						</div>						
						<div class="row">
							<div class="span12">
								<label><?php echo lang('description');?></label>
								<?php
									$data	= array('name'=>'description', 'class'=>'redactor', 'value'=>set_value('description', $description));
									echo form_textarea($data);
								?>
								
							</div>
						</div>
					</div>					
					
					<div class="tab-pane" id="product_categories">
						<div class="row">
							<div class="span12">
								<?php if(isset($categories[0])):?>
									<label><strong><?php echo lang('select_a_category');?></strong></label>
									<table class="table table-striped">
									    <thead>
											<tr>
												<th colspan="2"><?php echo lang('name')?></th>
											</tr>
										</thead>
									<?php
									function list_categories($parent_id, $cats, $sub='', $product_categories) {					
										foreach ($cats[$parent_id] as $cat):?>
										<tr>
											<td><?php echo  $sub.$cat->name; ?></td>
											<td class="span1" style="text-align:center;">
												<input type="checkbox" name="categories[]" value="<?php echo $cat->id;?>" <?php echo(in_array($cat->id, $product_categories))?'checked="checked"':'';?>/>
											</td>
										</tr>
										<?php
										if (isset($cats[$cat->id]) && sizeof($cats[$cat->id]) > 0){
											$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
												$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
											list_categories($cat->id, $cats, $sub2, $product_categories);
										}
										endforeach;
									}																
									list_categories(0, $categories, '', $product_categories);								
								?>
								</table>
							<?php else:?>
								<div class="alert"><?php echo lang('no_available_categories');?></div>
							<?php endif;?>
							</div>
						</div>
					</div>

					<div class="tab-pane" id="product_downloads">
						<div class="row">
							<div class="span3">
								<label>Search for <?php echo lang('digital_content'); ?></label>
								<div class="row">
									<div class="input-append">
									  <input type="text" id="fileslist_search" class="span11" placeholder="Search key ...">
									  <button type="button" class="btn" onclick="$('#fileslist_search').trigger('keyup')"><i class="fa fa-search"></i></button>
									</div>
									<script type="text/javascript">
										$('#fileslist_search').keyup(function(){
											$('#fileslist').html('');
											run_files_query();
										});
										function run_files_query(){
											$.getJSON("<?php echo site_url($this->config->item('admin_folder').'/digital_products/ajax_search');?>", { 
												search_key: $('#fileslist_search').val()
											},function(data) {									
												$('#fileslist').html('');									
												$.each(data, function(index, file){									
													if($('#digital_product_'+file.id).length == 0){
														$('#fileslist').append('<option id="fileitem_'+file.id+'" value="'+file.id+'">'+file.title+'</option>');
													}
												});
											});
										}
										function _add_content(file_id){
											if($('#digital_product_'+file_id).length == 0){
												// $('#fileslist option[value="'+file_id+'"]').remove();
												var dg_container = $('<div class="well well-small nopadding"></div>');
												$(dg_container).attr('id', 'digital_product_'+file_id);
												$.blockUI({message: 'loading digital content...'});
												$.get("<?php echo site_url($this->config->item('admin_folder').'/digital_products/preview'); ?>/"+file_id, {product:'no'}, function(content){
													$(dg_container).append('<input type="hidden" name="downloads[]" value="'+file_id+'" />');
													$(dg_container).append('<button type="button" class="btn btn-small btn-danger pull-right" onclick="remove_digital_product(\''+file_id+'\');"><i class="fa fa-remove"></i></button>');
													$(dg_container).append(content);													
													<?php if($product_type == 'non-package'): ?>
														$('#digital_products_container').html(dg_container);
														$('input[name="sku"]').val(file_id);
													<?php else: ?>																
														$('#digital_products_container').append(dg_container);
													<?php endif; ?>
													$.unblockUI();
													$('#fileslist').focus();
												})
												if(!$('#msg-no-files').hasClass('hide')) $('#msg-no-files').addClass('hide');
											}							
										}
										function add_digital_product(){
											var filelist_values = $('#fileslist option:checked');
											if(filelist_values.length >0){
												$(filelist_values).each(function(idx, option){
													var file_id = $(option).val();
													_add_content(file_id);
												});								
											}
											return false;
										}
										function remove_digital_product(file_id){
											if($('#digital_product_'+file_id).length > 0){
												$('#digital_product_'+file_id).remove();
											}
											if($('#digital_products_container').html() == ''){
												if($('#msg-no-files').hasClass('hide')) $('#msg-no-files').removeClass('hide');
											}											
										}
									</script>									
									<select id="fileslist" size="15"></select>
								</div>
								<div class="row">
									<div class="span12" style="margin-top:8px;">
										<a href="#" onclick="return add_digital_product();" class="btn">Add <?php echo lang('digital_content'); ?></a>
									</div>
								</div>								
							</div>
							<div class="span9">
								<div id="digital_products_container"></div>
								<?php if(isset($product_files) && $product_files): ?>
									<script type="text/javascript">
										$(document).ready(function(){
											<?php foreach($product_files as $file_id): ?>
												_add_content('<?php echo $file_id; ?>');
											<?php endforeach; ?>										
										});
									</script>
								<?php else: ?>
									<div class="alert alert-info no-margin" id="msg-no-files"><?php echo lang('no_files'); ?></div>
								<?php endif; ?>
							</div>
						</div>
					</div>
			
					<div class="tab-pane" id="product_related">
						<div class="row">
							<div class="span3">
								<label><?php echo lang('select_a_product');?></label>
								<div class="row">
									<div class="input-append">
									  <input type="text" id="product_search" class="span11" placeholder="Search key ...">
									  <button type="button" class="btn"><i class="fa fa-search"></i></button>
									</div>
									<script type="text/javascript">
										$('#product_search').keyup(function(){
											$('#product_list').html('');
											run_product_query();
										});								
										function run_product_query(){
											$.post("<?php echo site_url($this->config->item('admin_folder').'/products/product_autocomplete/');?>", { name: $('#product_search').val(), limit:10},
												function(data) {									
													$('#product_list').html('');									
													$.each(data, function(index, value){									
														if($('#related_product_'+index).length == 0){
															$('#product_list').append('<option id="product_item_'+index+'" value="'+index+'">'+value+'</option>');
														}
													});
											
											}, 'json');
										}
									</script>
									<select id="product_list" size="10"></select>
								</div>
								<div class="row">
									<div class="span12" style="margin-top:8px;">
										<a href="#" onclick="add_related_product();return false;" class="btn" title="Add Related Product"><?php echo lang('add_related_product');?></a>
									</div>
								</div>
							</div>
							<div class="span9" style="padding-top:10px;">
								<table class="table table-striped">
									<tbody id="product_items_container">
									<?php
										if($related_products){
											foreach($related_products as $rel){
												echo related_items($rel->id, $rel->name);
											}
										}else{
											echo '<tr id="related-empty-msg"><td><div class="alert alert-info no-margin">No related product</div></td></tr>';
										}									
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
			
					<div class="tab-pane" id="product_photos">
						<div class="row">
							<iframe id="iframe_uploader" src="<?php echo site_url($this->config->item('admin_folder').'/products/product_image_form');?>" class="span12" style="height:75px; border:0px;"></iframe>
						</div>
						<div class="row">
							<div class="span12">								
								<div id="gc_photos">									
								<?php
									foreach($images as $photo_id=>$photo_obj){
										if(!empty($photo_obj)){
											$photo = (array)$photo_obj;
											add_image($photo_id, $photo['filename'], $photo['alt'], $photo['caption'], isset($photo['primary']), isset($photo['imgpath']) ? $photo['imgpath'] : 'local' );
										}
									}
								?>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane" id="product_seo">
						<div class="row">
							<div class="span12">
								<!-- <fieldset> -->
									<!-- <legend><?php //echo lang('header_information');?></legend> -->
									<!-- <div class="row" style="padding-top:10px;"> -->
										<!-- <div class="span8"> -->
											
										<label for="slug"><?php echo lang('slug');?> </label>
										<?php
										$data	= array('name'=>'slug', 'value'=>set_value('slug', $slug), 'class'=>'span8');
										echo form_input($data);?>
										
										<label for="seo_title"><?php echo lang('seo_title');?> </label>
										<?php
										$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'span8');
										echo form_input($data);
										?>

										<label for="meta"><?php echo lang('meta');?> <i><?php echo lang('meta_example');?></i></label> 
										<?php
										$data	= array('name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'span12');
										echo form_textarea($data);
										?>
										<!-- </div> -->
									<!-- </div> -->
								<!-- </fieldset> -->
							</div>
						</div>				
					</div>					
			</div>
		</div>
		<div class="form-actions">
			<button type="button" class="btn btn-default" id="btnCancelForm"><?php echo lang('cancel');?></button>
			<button type="submit" class="btn btn-primary"><?php echo lang('save');?></button>		
		</div>
	</form>
	</div>
</div>

<?php
function add_image($photo_id, $filename, $alt, $caption, $primary=false, $imgpath = "local"){
	ob_start();
?>
	<div class="row-fluid gc_photo" id="gc_photo_<?php echo $photo_id;?>" style="background-color:#fff; border-bottom:1px solid #ddd; padding-bottom:20px; margin-bottom:20px;">
		<div class="span2">
			<input type="hidden" name="images[<?php echo $photo_id;?>][filename]" value="<?php echo $filename;?>"/>
			<input type="hidden" name="images[<?php echo $photo_id;?>][imgpath]" value="<?php echo $imgpath;?>"/>
			<img class="gc_thumbnail" src="<?php echo ($imgpath != 'remote') ? base_url('uploads/images/thumbnails/'.$filename) : config_item('digital_content_thumbnail_path').$filename; ?>" style="padding:5px; border:1px solid #ddd"/>
		</div>
		<div class="span8">
			<div class="row-fluid">
				<div class="span4">
					<input name="images[<?php echo $photo_id;?>][alt]" value="<?php echo $alt;?>" placeholder="<?php echo lang('alt_tag');?>" />
				</div>
				<div class="span4">
					<input type="radio" name="primary_image" value="<?php echo $photo_id;?>" <?php if($primary) echo 'checked="checked"';?>/> <?php echo lang('primary');?>
				</div>
				<div class="span4">
					<a onclick="return remove_image($(this));" rel="<?php echo $photo_id;?>" class="btn btn-danger" style="float:right; font-size:9px;"><i class="icon-trash icon-white"></i> <?php echo lang('remove');?></a>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<label><?php echo lang('caption');?></label>
					<textarea name="images[<?php echo $photo_id;?>][caption]" class="span6" rows="3"><?php echo $caption;?></textarea>
				</div>
			</div>
		</div>
	</div>

	<?php
	$stuff = ob_get_contents();

	ob_end_clean();
	
	echo replace_newline($stuff);
}


//this makes it easy to use the same code for initial generation of the form as well as javascript additions
function replace_newline($string) {
  return trim((string)str_replace(array("\r", "\r\n", "\n", "\t"), ' ', $string));
}
?>
<script type="text/javascript">
//<![CDATA[
function add_related_product(){
	//if the related product is not already a related product, add it
	if($('#related_product_'+$('#product_list').val()).length == 0 && $('#product_list').val() != null){
		<?php $new_item	 = str_replace(array("\n", "\t", "\r"),'',related_items("'+$('#product_list').val()+'", "'+$('#product_item_'+$('#product_list').val()).html()+'"));?>
		var related_product = '<?php echo $new_item;?>';
		$('#product_items_container').append(related_product);
		if( !$('#related-empty-msg').hasClass('hide') ){
			$('#related-empty-msg').addClass('hide')
		}
		run_product_query();
	}else{
		if($('#product_list').val() == null){
			alert('<?php echo lang('alert_select_product');?>');
		}else{
			alert('<?php echo lang('alert_product_related');?>');
		}
	}
}

function remove_related_product(id){
	if(confirm('<?php echo lang('confirm_remove_related');?>')){
		$('#related_product_'+id).remove();
		if($('#product_items_container tr').length == 1){
			if( $('#related-empty-msg').hasClass('hide') ){
				$('#related-empty-msg').removeClass('hide')
			}
		}
		run_product_query();
	}
}

function photos_sortable(){
	$('#gc_photos').sortable({	
		handle : '.gc_thumbnail',
		items: '.gc_photo',
		axis: 'y',
		scroll: true
	});
}
//]]>
</script>
<?php
	function related_items($id, $name) {
		return '
			<tr id="related_product_'.$id.'">
				<td>
					<input type="hidden" name="related_products[]" value="'.$id.'"/>
					'.$name.'
				</td>
				<td class="span1">
					<a class="btn btn-danger pull-right btn-mini" href="#" onclick="remove_related_product('.$id.'); return false;"><i class="fa fa-remove"></i></a>
				</td>
			</tr>
		';
	}
?>