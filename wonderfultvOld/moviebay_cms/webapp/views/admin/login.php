<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Admin Panel <?php echo (isset($page_title))?' :: '.$page_title:''; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin Login Page">
    <meta name="author" content="htridianto@gmail.com">
    
    <link rel="stylesheet" href="<?php echo base_url('/assets/admin-bootstrap-2/css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('/assets/admin-bootstrap-2/css/bootstrap-responsive.min.css'); ?>" />   
    <link rel="stylesheet" href="<?php echo base_url('/assets/admin-bootstrap-2/css/font-awesome.css'); ?>" /> 
    <link rel="stylesheet" href="<?php echo base_url('/assets/admin-bootstrap-2/css/unicorn.login.css'); ?>" />    
    
</head>
<body>    
    <div class="logo">
        <i class="fa fa-lock"></i> Admin Panel 
    </div>
    <div id="loginbox">     
            <?php
            if($this->session->flashdata('error')){
                $error  = $this->session->flashdata('error');
            }    
            ?>    
            <?php if (!empty($error)): ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <a class="close" data-dismiss="alert">×</a>
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>

            <?php echo form_open($this->config->item('admin_folder').'/login', 'class="form-vertical"') ?> <!-- Login form -->
                <p>Enter username and password to continue.</p>
                <input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
                <input type="hidden" value="submitted" name="submitted"/>     
                <input type="hidden" value="true" name="remember"/>     

                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <?php echo form_input(array('name'=>'username', 'placeholder'=>'User Name')); ?>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span>
                            <?php echo form_password(array('name'=>'password', 'placeholder'=>'Password')); ?>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <!-- <span class="pull-right"> -->
                        <input class="btn btn-danger" type="submit" value="<?php echo lang('login');?>"/>
                        <button type="reset" class="btn btn-default">Reset</button>
                    <!-- </span> -->
                </div>
            <?php echo  form_close(); ?>                
    </div>                
    <div class="widget-foot text-center">
        <small>Copyright &copy; 2014 | iBolz <?php echo config_item('company_name'); ?></small>
    </div>

    <script src="<?php echo base_url('assets/admin-bootstrap-2/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin-bootstrap-2/js/jquery-plugins/jquery.cooki.js');?>"></script>    
    <script src="<?php echo base_url('assets/admin-bootstrap-2/js/unicorn.login.js'); ?>"></script>
</body>
</html>