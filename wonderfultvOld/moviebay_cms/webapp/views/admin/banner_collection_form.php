
<?php echo form_open_multipart(config_item('admin_folder').'/banners/banner_collection_form/'.$banner_collection_id); ?>
	<label for="title"><?php echo lang('name');?> </label>
	<?php 
		echo form_input(array('name'=>'name', 'value' => set_value('name', $name))); 
	?>

	<label for="image_width">Image Size</label>
	<?php echo form_input(array('name'=>'image_width', 'value' => set_value('image_width', $image_width), 'class'=>'span1')); ?>
	x
	<?php echo form_input(array('name'=>'image_height', 'value' => set_value('image_height', $image_height), 'class'=>'span1')); ?>

	<div class="form-actions">
		<input class="btn btn-default" type="button" value="<?php echo lang('cancel');?>" id="btnCancelForm" />
		<input class="btn btn-primary" type="submit" value="<?php echo lang('save');?>"/>
	</div>
</form>