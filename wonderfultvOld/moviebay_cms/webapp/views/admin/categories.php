<div class="row-fluid">
	<div class="span12">		
		<div id="toolbar-page">
			<?php if($this->auth->check_access('Admin')): ?>
				<a class="btn btn-small" href="<?php echo site_url($this->config->item('admin_folder').'/categories/form'); ?>">
					<i class="icon-plus-sign"></i> <?php echo lang('add_new_category');?>
				</a>
			<?php endif ?>
			<?php if($this->auth->check_access('Developer')): ?>
				<a href="#sync" class="btn btn-small tip-bottom" title="iBolz Channels Synchronization">
					<i class="fa fa-refresh"></i> Synchronization
				</a>				
				<?php if(count($categories) > 0): ?>
					<a href="#deleteall" class="btn btn-small btn-danger tip-bottom" title="iBolz Channels Synchronization">
						<i class="fa fa-trash"></i> Remove All
					</a>
				<?php endif; ?>				
			<?php endif ?>
		</div>
		<div class="clearfix"></div>				
		<table class="table table-striped" id="table-categories">
		    <thead>
				<tr>
					<th><i class="fa fa-list"></i></th>
					<th><?php echo lang('name')?></th>								
					<th class="span3" style="text-align:center"><?php echo lang('slug');?></th>
					<th class="span3">Ibolz Channel</th>
					<th class="span2" style="text-align:center">Contents</th>
					<th class="span2"></th>
				</tr>
			</thead>
			<tbody>
				<?php echo (count($categories) < 1)?'<tr><td style="text-align:center;" colspan="6">'.lang('no_categories').'</td></tr>':''?>
				<?php
					function list_categories($parent_id, $cats, $sub='', $ibolz_channels, $has_access) {			
						foreach ($cats[$parent_id] as $cat):
					?>
						<tr class="<?php echo ($cat->enabled == '1') ? '' : 'error'; ?> row-category" data-id="<?php echo  $cat->id; ?>" data-name="<?php echo  $sub.$cat->name; ?>">
							<td style="text-align:center"><?php echo ($cat->display_on_menu == '1') ? '<i class="fa fa-check"></i>' :'&nbsp;'; ?></td>
							<td>
								<?php echo  $sub.$cat->name; ?> 
								<?php if($has_access): ?>
									<small class="color-thin">(<?php echo $cat->id; ?>)</small>
								<?php endif; ?>
							</td>							
							<td><?php echo $cat->slug; ?></td>
							<td>					
								<?php 
									if($cat->ibolz_channel_id && isset($ibolz_channels[$cat->ibolz_channel_id]) && $ibolz_channels[$cat->ibolz_channel_id]) {
										echo "<span class='label label-info'>".$ibolz_channels[$cat->ibolz_channel_id]['name']."</span>";
									}else if($cat->ibolz_channel_id){
										echo "<span class='label label-important'>Unknown Channel<input type='hidden' name='unkown_channel' value='{$cat->id}' /></span>";
									}
								?>
								<?php if($has_access): ?>
									<div class="color-thin"><?php echo $cat->ibolz_channel_id; ?></div>
								<?php endif; ?>
							</td>
							<td style="text-align:center">
								<?php if($cat->products_count > 0): ?>
									<span class="badge"><?php echo $cat->products_count; ?></span>
								<?php endif; ?>
								&nbsp;
							</td>
							<td>
								<div class="btn-group" style="float:right">
									<a class="btn btn-small" href="<?php echo site_url(config_item('admin_folder').'/categories/organize/'.$cat->id); ?>" title="<?php echo lang('organize');?>"><i class="fa fa-cogs"></i></a>
									<a class="btn btn-small" href="<?php echo site_url(config_item('admin_folder').'/categories/form/'.$cat->id); ?>" title=" <?php echo lang('edit');?>"><i class="icon-pencil"></i></a>									
									<?php if(empty($cat->ibolz_channel_id) || $has_access): ?>
										<a class="btn btn-small btn-danger btn-remove" href="<?php echo site_url(config_item('admin_folder').'/categories/delete/'.$cat->id);?>" title="<?php echo lang('delete');?>" onclick="return areyousure();"><i class="fa fa-trash"></i></a>
									<?php endif ?>
								</div>
							</td>
						</tr>
						<?php
						if (isset($cats[$cat->id]) && sizeof($cats[$cat->id]) > 0){
							$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
								$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
							list_categories($cat->id, $cats, $sub2, $ibolz_channels, $has_access);
						}
						endforeach;
					}		
					if(isset($categories[0])) {
						list_categories(0, $categories, '',$ibolz_channels, $this->auth->check_access('Developer'));
					}		
				?>
			</tbody>
		</table>

	</div>
</div>	

<script type="text/javascript">
	function areyousure(){
		return confirm('<?php echo lang('confirm_delete_category');?>');
	}

	var channels = <?php echo json_encode($ibolz_channels_tree); ?>;
	var channelsLength = <?php echo count($ibolz_channels); ?>;
	var channelsProcessCount = 0;
	var prosesID;
	var do_channels_sync = function(channel){
		if(channel){
			$(channel).each(function(idx, item){
				if($('div.blockUI.blockMsg').length == 0) $.blockUI();
				item.sequence = idx+1;				
				$.getJSON("<?php echo site_url('/api/channel/event/save'); ?>", item, function(resp){
	                $('div.blockUI.blockMsg').html(resp.message+'... ['+item.name+'] <strong>'+resp.rcode+'</strong>');
	                channelsProcessCount++;               	
                	if(prosesID != null){
                		clearTimeout(prosesID);
                	}
                	prosesID = setTimeout(function() {
                		window.location = window.location;	                		
                	}, 2500);             	
	                if(channels[item.id] != null){
						do_channels_sync(channels[item.id]);
					}					
	        	});
			});
		}		
	}

	var do_sync = function(channel){
		var unkown_channels = $('input[name="unkown_channel"]');
		var dataLength = unkown_channels.length;
		var number_of_process = 0;
		if(dataLength > 0){
			$(unkown_channels).each(function(idx, item){			
				var id = $(item).val();
				$.blockUI();
				$.getJSON("<?php echo site_url(config_item('admin_folder').'/categories/delete'); ?>/"+id, function(resp){                
	                $('div.blockUI.blockMsg').html(resp.message+'... ['+id+'] <strong>'+resp.rcode+'</strong>');
	                $('#table-categories .row-category[data-id='+id+']').fadeOut();
	                number_of_process++;
	                if(number_of_process >= dataLength){
	                	do_channels_sync(channel);
	                }
	        	});
	        });		
		}else{
			do_channels_sync(channel);
		}
	};

	var do_delete = function(){
		var dataLength = $('#table-categories .row-category').length;
		var number_of_process = 0;
        $('#table-categories .row-category').each(function(idx, item){
			var id = $(item).data('id');
			var title = $(item).data('name');
			if($('div.blockUI.blockMsg').length == 0) $.blockUI();
			$.getJSON("<?php echo site_url(config_item('admin_folder').'/categories/delete'); ?>/"+id, function(resp){                
                $('div.blockUI.blockMsg').html(resp.message+'... ['+title+'] <strong>'+resp.rcode+'</strong>');
                $('#table-categories .row-category[data-id='+id+']').fadeOut();
                number_of_process++;
                if(number_of_process >= dataLength){
                	setTimeout(function() {
                		window.location = window.location;             		
                	}, 500);
                }
        	});
        });		
	}

	$(document).ready(function(){
		$('a[href^=#sync]').live('click', function(ev){
			ev.preventDefault();
			BootstrapDialog.confirm('Are you sure, you want to sync categories with ibolz channel?<br /><br /><div class="alert alert-danger">All categories has related with ibolz channel will be removes before sync.</div>', function(result){
	            if(result) {
	            	do_sync(channels[0]);
	            }
	        });	
		})

		$('a[href^=#deleteall]').live('click', function(ev){
			ev.preventDefault();
			BootstrapDialog.confirm('Are you sure, you want to remove all categories ?', function(result){
	            if(result) {
	            	if($('#table-categories .row-category').length > 0){
	            		do_delete();
	            	}    
	            }
	        });	
		})
	});

</script>

