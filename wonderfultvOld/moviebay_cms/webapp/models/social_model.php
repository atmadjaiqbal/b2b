<?php
Class Social_model extends MY_Model{

	protected $_table = 'content_comment';

	function __construct(){
		parent::__construct();
	}
	
	private function _do_filter($filter){
		if($filter){
			if(isset($filter['search_key'])){
				$this->db
					->like('content_comment.comment_text',  $filter['search_key'])
					->or_like('customers.firstname',  $filter['search_key'])
					->or_like('digital_products.title',  $filter['search_key']);
			}
		}
	}

	public function count_comments($filter = array()){
		$this->_do_filter($filter);
		return $this->db
				->join('digital_products', 'content_comment.content_id = digital_products.id')
				->join('customers', 'content_comment.customer_id = customers.ibolz_account_id', 'LEFT')
				->count_all_results($this->_table);
	}

	public function get_comments($filter = array()){
		$this->_do_filter($filter);

		if(isset($filter['limit'])){
			$this->db->limit($filter['limit']);
		} 
		if(isset($filter['offset'])){
			$this->db->offset($filter['offset']);
		} 
		$res = $this->db
			->select('content_comment.*')
			->select('digital_products.title, digital_products.video_thumbnail')
			->select('customers.firstname, customers.lastname')
			->join('digital_products', 'content_comment.content_id = digital_products.id')
			->join('customers', 'content_comment.customer_id = customers.ibolz_account_id', 'LEFT')
			->order_by('content_comment.created', 'desc')
			->get('content_comment');
		return $res->result();
	}
	
	public function get_comment($id){
		$res = $this->db->where('comment_id', $id)->get('content_comment');
		return $res->row();
	}
	
	// function save_message($data)
	// {
	// 	if($data['id'])
	// 	{
	// 		$this->db->where('id', $data['id'])->update('canned_messages', $data);
	// 		return $data['id'];
	// 	}
	// 	else 
	// 	{
	// 		$this->db->insert('canned_messages', $data);
	// 		return $this->db->insert_id();
	// 	}
	// }
	
	// function delete_message($id)
	// {
	// 	$this->db->where('id', $id)->delete('canned_messages');
	// 	return $id;
	// }
	
	
}