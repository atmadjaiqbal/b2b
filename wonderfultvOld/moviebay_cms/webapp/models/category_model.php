<?php
Class Category_model extends MY_Model
{

    protected $_table = 'categories';
    
    function __construct(){
        parent::__construct();
    }

    public function get_categories($parent = false){
        if ($parent !== false){
            $this->db->where('parent_id', $parent);
        }
        $this->db->select('id');
        $this->db->order_by('categories.sequence', 'ASC');        
        //this will alphabetize them if there is no sequence
        $this->db->order_by('name', 'ASC');
        $result = $this->db->get('categories');        
        $categories = array();
        foreach($result->result() as $cat){
            $categories[]   = $this->get_category($cat->id);
        }        
        return $categories;
    }

    public function get_categories_tiered($admin = false, $display_on_menu = false){
        if(!$admin) $this->db->where('enabled', 1);
        
        $this->db->order_by('sequence');
        $this->db->order_by('name', 'ASC');
        $categories = $this->db->get('categories')->result();        
        $results    = array();
        foreach($categories as $category) {            
            if($category->image == null){
                unset($category->image);
            }            
            $category->active = ($this->uri->segment(1) == $category->slug);
            if(!$display_on_menu || ($display_on_menu && $category->display_on_menu == 1)){
                $results[$category->parent_id][$category->id] = $category;
            }
            if($admin){
                $category->products_count = $this->db
                    ->where('category_id', $category->id)
                    ->count_all_results('category_products');
            }
        }        
        return $results;
    }
    
    public function get_category($id){
        return $this->db->get_where('categories', array('id'=>$id))->row();
    }

    public function get_category_by_channel($channel_id){
        return $this->db->get_where('categories', array('ibolz_channel_id'=>$channel_id))->row();
    }
    
    public function get_category_products_admin($id){
        $contents   = array();
        $table = $this->db
                ->order_by('sequence', 'ASC')
                ->get_where('category_products', array('category_id'=>$id));
        $result = $table->result();                
        foreach ($result as $product){
            $product = $this->db
                        ->where('id', $product->product_id)
                        ->get('products')
                        ->row();
            if($product){
                if($product->images){
                    $images = json_decode($product->images, true);
                    $product->images = array_values($images);
                }                
                $contents[] = $product;
            }            
        }        
        return $contents;
    }
    
    public function get_category_products($id, $limit, $offset){
        $this->db->order_by('sequence', 'ASC');
        $result = $this->db->get_where('category_products', array('category_id'=>$id), $limit, $offset);
        $result = $result->result();        
        $contents   = array();
        $count      = 1;
        foreach ($result as $product){
            $result2    = $this->db->get_where('products', array('id'=>$product->product_id));
            $result2    = $result2->row();            
            $contents[$count]   = $result2;
            $count++;
        }        
        return $contents;
    }
    
    public function organize_contents($id, $products){
        //first clear out the contents of the category
        $this->db->where('category_id', $id);
        $this->db->delete('category_products');        
        //now loop through the products we have and add them in
        $sequence = 0;
        foreach ($products as $product){
            $this->db->insert('category_products', array('category_id'=>$id, 'product_id'=>$product, 'sequence'=>$sequence));
            $sequence++;
        }
    }
    
    public function save($category){
        if (isset($category['id']) && $category['id']){
            $this->db->where('id', $category['id']);
            $this->db->update('categories', $category);            
            return $category['id'];
        }else{
            $this->db->insert('categories', $category);
            return $this->db->insert_id();
        }
    }
    
    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('categories');
        
        //delete references to this category in the product to category table
        $this->db->where('category_id', $id);
        $this->db->delete('category_products');
    }
}