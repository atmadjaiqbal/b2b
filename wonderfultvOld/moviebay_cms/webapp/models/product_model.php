<?php
Class Product_model extends MY_Model
{

    protected $_table = 'products';
    
    function __construct(){
        parent::__construct();
    }
	
	public function product_autocomplete($name, $limit){
		return	$this->db->like('name', $name)->get('products', $limit)->result();
	}
	
	public function products($data=array(), $return_count = false){
		if(empty($data)){
			//if nothing is provided return the whole shabang
			$this->get_all_products();
		}else{
			if(isset($data['product_type']) && !empty($data['product_type'])){				
				if($data['product_type'] == 'non-package'){
					$this->db->where('product_type', $data['product_type']);

					//$this->db->join('products_files', 'products_files.product_id = products.id');
					//$this->db->join('digital_products', 'products_files.file_id = digital_products.id');
				}				
			}			
			//grab the limit
			if(!empty($data['rows'])){
				$this->db->limit($data['rows']);
			}			
			//grab the offset
			if(!empty($data['page'])){
				$this->db->offset($data['page']);
			}			
			//do we order by something other than category_id?
			if(!empty($data['order_by'])){
				//if we have an order_by then we must have a direction otherwise KABOOM
				$this->db->order_by($data['order_by'], $data['sort_order']);
			}			
			//do we have a search submitted?
			if(!empty($data['term'])){
				$search	= json_decode($data['term']);
				//if we are searching dig through some basic fields
				if(!empty($search->term)){
					$this->db->like('name', $search->term);
					$this->db->or_like('description', $search->term);
					$this->db->or_like('sku', $search->term);
				}				
				if(!empty($search->category_id)){
					//lets do some joins to get the proper category products
					$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
					$this->db->where('category_products.category_id', $search->category_id);
					$this->db->order_by('sequence', 'ASC');
				}
			}			
			if($return_count){
				return $this->db->count_all_results('products');
			}else{
				return $this->db->get('products')->result();
			}
			
		}
	}
	
	public function get_all_products(){
		//sort by alphabetically by default
		$this->db->order_by('name', 'ASC');
		$result	= $this->db->get('products');
		return $result->result();
	}
	
	public function get_filtered_products($product_ids, $limit = false, $offset = false){		
		if(count($product_ids)==0){
			return array();
		}		
		$this->db->select('id, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('products');		
		if(count($product_ids)>1){
			$querystr = '';
			foreach($product_ids as $id){
				$querystr .= 'id=\''.$id.'\' OR ';
			}		
			$querystr = substr($querystr, 0, -3);			
			$this->db->where($querystr, null, false);			
		} else {
			$this->db->where('id', $product_ids[0]);
		}		
		$result	= $this->db->limit($limit)->offset($offset)->get()->result();
		//die($this->db->last_query());
		$contents	= array();
		$count		= 0;
		foreach ($result as $product){
			$contents[$count]	= $this->get_product($product->id);
			$count++;
		}
		return $contents;
		
	}
	
	public function get_products($category_id = false, $limit = false, $offset = false, $by='category_products.sequence', $sort='asc'){
		//if we are provided a category_id, then get products according to category
		if ($category_id){
			$this->db
				->select('category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)
				->from('category_products')
				->join('products', 'category_products.product_id=products.id')
				->join('categories', 'category_products.category_id=categories.id')
				->where('products.enabled', 1);
			if(is_numeric($category_id)){
				$this->db->where('category_id', $category_id);
			}else if(is_string($category_id)){
				$this->db->where('categories.slug', $category_id);
			}else if(is_array($category_id)){
				$this->db->where_in('category_id', $category_id);
			}
			$this->db->order_by($by, $sort);
			$this->db->group_by('products.sku');			
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();			
			return $result;
		}else{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$result	= $this->db->get('products')->result();
			return $result;
		}
	}
	
	public function count_all_products(){
		return $this->db->count_all_results('products');
	}
	
	public function count_products($id){
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled'=>1))->count_all_results();
	}

	public function get_product($id, $related=true){
		$result	= $this->db->get_where('products', array('id'=>$id))->row();
		if(!$result){
			return false;
		}
		$related = json_decode($result->related_products);		
		if(!empty($related)){
			$where = array();
			foreach($related as $r){
				$where[] = '`id` = '.$r;
			}
			$this->db->where('('.implode(' OR ', $where).')', null);
			// $this->db->where('enabled', 1);
			$result->related_products	= $this->db->get('products')->result();
		}else{
			$result->related_products	= array();
		}
		$result->categories	= $this->get_product_categories($result->id);
		return $result;
	}

	public function get_product_categories($id){
		return $this->db->where('product_id', $id)->join('categories', 'category_id = categories.id')->get('category_products')->result();
	}

	public function get_slug($id, $string = false){
		if(is_numeric($id) && !$string){
			$row = $this->db->get_where('products', array('id'=>$id))->row();
		}else if(!empty($id)){
			$row = $this->db->get_where('products', array('sku'=>$id))->row();
			if(!$row){
				$row = $this->db->get_where('products', array('id'=>$id))->row();	
			}
		}
		return ($row) ? $row->slug : null;
	}

	public function check_slug($str, $id=false){
		$this->db->select('slug');
		$this->db->from('products');
		$this->db->where('slug', $str);
		if ($id){
			$this->db->where('id !=', $id);
		}
		$count = $this->db->count_all_results();

		if ($count > 0){
			return true;
		}else{
			return false;
		}
	}

	public function save($product, $options=false, $categories=false){
		if (isset($product['id']) && $product['id']){
			$this->db->where('id', $product['id']);
			$this->db->update('products', $product);
			$id	= $product['id'];
		}else{
			$this->db->insert('products', $product);
			$id	= $this->db->insert_id();
		}

		//loop through the product options and add them to the db
		if($options !== false){
			$obj =& get_instance();
			$obj->load->model('Option_model');
			// wipe the slate
			$obj->Option_model->clear_options($id);
			// save edited values
			$count = 1;
			foreach ($options as $option){
				$values = $option['values'];
				unset($option['values']);
				$option['product_id'] = $id;
				$option['sequence'] = $count;
				$obj->Option_model->save_option($option, $values);
				$count++;
			}
		}
		
		if($categories !== false){
			if (isset($product['id']) && $product['id']){
				//get all the categories that the product is in
				$cats	= $this->get_product_categories($id);				
				//generate cat_id array
				$ids	= array();
				foreach($cats as $c){
					$ids[]	= $c->id;
				}
				//eliminate categories that products are no longer in
				foreach($ids as $c){
					if(!in_array($c, $categories)){
						$this->db->delete('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}				
				//add products to new categories
				foreach($categories as $c){
					if(!in_array($c, $ids)){
						$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
			}else{
				//new product add them all
				foreach($categories as $c){
					$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
				}
			}
		}				
		//return the product id
		return $id;
	}
	
	public function delete_product($id){
		// delete product 
		$this->db->where('id', $id);
		$this->db->delete('products');

		//delete references in the product to category table
		$this->db->where('product_id', $id);
		$this->db->delete('category_products');

		//delete references in the product to category products_files (digital products)
		$this->db->where('product_id', $id);
		$this->db->delete('products_files');
		
		// delete coupon reference
		$this->db->where('product_id', $id);
		$this->db->delete('coupons_products');

	}

	public function add_product_to_category($product_id, $category_id, $sequence = false){
		$this->db->insert('category_products', array('product_id'=>$product_id, 'category_id'=>$category_id, 'sequence'=>$sequence));
	}

	public function remove_product_from_category($product_id, $category_id){
		$this->db->where('product_id', $product_id);
		$this->db->where('category_id', $category_id);
		$this->db->delete('category_products');
	}

	public function search_products_count($term){
		// $this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		return $this->db->count_all_results('products');
	}

	public function search_products($term, $limit=false, $offset=false, $by=false, $sort=false){
		$results		= array();
		
		// $this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one counts the total number for our pagination
		// $this->db->where('enabled', 1);
		// $this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		// $results['count']	= $this->db->count_all_results('products');

		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one gets just the ones we need.
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		
		if($by && $sort){
			$this->db->order_by($by, $sort);
		}
		
		$results['products']	= $this->db->get('products', $limit, $offset)->result();		
		return $results;
	}

	// Build a cart-ready product array
	public function get_cart_ready_product($id, $quantity=false){
		$product	= $this->db->get_where('products', array('id'=>$id))->row();
		
		//unset some of the additional fields we don't need to keep
		if(!$product){
			return false;
		}
		
		$product->base_price	= $product->price;		
		if ($product->saleprice != 0.00){ 
			$product->price	= $product->saleprice;
		}
				
		// Some products have n/a quantity, such as downloadables
		//overwrite quantity of the product with quantity requested
		if (!$quantity || $quantity <= 0 || $product->fixed_quantity==1){
			$product->quantity = 1;
		}else{
			$product->quantity = $quantity;
		}		
		// attach list of associated downloadables
		$product->file_list	= $this->Digital_Product_model->get_associations_by_product($id);		
		return (array)$product;
	}
}