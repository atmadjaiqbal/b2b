<?php

Class Digital_Product_Model extends MY_Model {
	
	protected $_table = 'digital_products';

	function __construct()
	{
		parent::__construct();
		$this->lang->load('digital_product');
	}
	
	// Return blank record array
	function new_file(){
		return array(
			'id'=>'',
			'filename'=>'',
			'max_downloads'=>'',
			'title'=>'',
			'description'=>'',
			'size'=>''
		);
	}

	// Get files list
	public function get_digital_contents(){				
		$list = $this->get_all();		
		foreach($list as &$file){
			// identify if the record is missing it's file content
			$file->product = self::_associations_product($file->id);
			if($file->product){
				$file->product_categories = self::_associations_product_categories($file->id);
			}			
		}		
		return $list;
	}

	public function get_digital_content($id){
		$file = $this->get($id);
		// identify if the record is missing it's file content
		if($file){
			$file->product = self::_associations_product($file->id);
			if($file->product){
				$file->product_categories = self::_associations_product_categories($file->id);
			}						
		}
		return $file;
	}

	// Verify product content, 1 digital content = 1 product
	private function _associations_product($id){
		return $this->db
			->select('products.*')
			->where('products_files.file_id', $id)
			->where('products.product_type', 'non-package')
			->join('products', 'products.id = products_files.product_id')
			->get('products_files')->row();
	}

	// Verify product content, 1 digital content = 1 product
	private function _associations_product_categories($id){
		return $this->db
			->select('categories.*')
			->where('products_files.file_id', $id)
			->where('products.product_type', 'non-package')
			->join('products', 'products.id = products_files.product_id')
			->join('category_products', 'products.id = category_products.product_id', 'left')
			->join('categories', 'category_products.category_id = categories.id', 'left')
			->group_by('categories.id')
			->get('products_files')->result();
	}	

	// Get assosiated channels 
	public function get_content_channels($id){
		return $this->db
			->select('*')
			->where('content_id', $id)
			->get('digital_product_channels')->result();
	}

	// Get files list
	public function get_list(){		
		$list = $this->get_all();		
		return $list;
	}
	
	// Get file record
	function get_file_info($id){
		return $this->db->where('id', $id)->get('digital_products')->row();
	}
	
	// Verify upload path
	function verify_file_path(){
		return is_writable('uploads/digital_products');
	}
	
	// Verify file content
	function verify_content($filename){
		return file_exists('uploads/digital_products'.'/'.$filename);
	}
	
	// Save/Update file record
	public function save($data){
		if(isset($data['id'])){
			$this->db->where('id', $data['id'])->update('digital_products', $data);
			return $data['id'];
		} else {
			$data['id'] = generate_code(25);
			$this->db->insert('digital_products', $data);
			return $this->db->insert_id();
		}
	}
	
	// Add product association
	public function associate($file_id, $product_id){
		$this->db->insert('products_files', array('product_id'=>$product_id, 'file_id'=>$file_id));
	}
	
	// Remove product association (all or by product)
	public function disassociate($file_id, $product_id=false){		
		if($product_id){
			$data['product_id'] = $product_id;
		}
		if($file_id){
			$data['file_id']	= $file_id;
		}
		$this->db->where($data)->delete('products_files');
	}
	
	public function get_associations_by_file($id){
		return $this->db->where('file_id', $id)->get('products_files')->result();
	}
	
	public function get_associations_by_product($product_id){
		return $this->db->where('product_id', $product_id)->get('products_files')->result();
	}
	
	// Delete file record & content
	public function delete($id){
		$this->load->model('product_model');		
		$info = $this->get_file_info($id);		
		if(!$info){
			return false;
		}
		
		// remove file
		if($this->verify_content($info->filename)){
			unlink('uploads/digital_products/'.$info->filename);
		}
		
		// Disable products that are associated with this file
		//  to prevent users purchasing a product with deleted media
		$product_associations  = $this->get_associations_by_file($id);
		foreach($product_associations as $p){
			$save['id']			= $p->product_id;
			$save['enabled']	= 0;
			$this->product_model->save($save);
		}
		
		// Remove db associations
		$this->db->where('id', $id)->delete('digital_products');
		$this->disassociate($id);
	}
	
	// Accepts an array of file lists for products purchased
	//  and sets up the list of available downloads for the customer
	//  uses customer id if available, also creates a package code
	//  that can be sent to non registered customers
	public function add_download_package($package, $order_id){
		// get customer stuff
		$customer = $this->go_cart->customer();
		if(!empty($customer['id'])){
			$new_package['customer_id'] = $customer['id'];
		} else {
			$new_package['customer_id'] = 0;
		}
		
		$new_package['order_id'] = $order_id;
		$new_package['code']	= generate_code();
		
		// save master package record
		$this->db->insert('download_packages',$new_package);		
		$package_id = $this->db->insert_id();		
		// save the db data here
		$files_list = array();		
		// use this to prevent inserting duplicates
		// in case a file is shared across products
		$ids = array();		
		// build files records list
		foreach($package as $product_list){
			foreach($product_list as $f){
				if(!isset($ids[$f->file_id])){
					$file['package_id'] = $package_id;
					$file['file_id'] = $f->file_id;
					$file['link'] = md5($f->file_id . time() . $new_package['customer_id']); // create a unique download key for each file					
					$files_list[] = $file;
				}
			}
		}
		
		$this->db->insert_batch('download_package_files', $files_list);
		
		// save the master record to include links in the order email
		$this->go_cart->save_order_downloads($new_package);
	}
	
	// Retrieve user's download packages
	//  send back an array indexed by order number
	public function get_user_downloads($customer_id){
		$result = $this->db->where('customer_id', $customer_id)->get('download_packages')->result();
		$downloads = array();
		foreach($result as $r)
		{
			$downloads[$r->order_id] = $this->get_package_files($r->id); 
		}
		return $downloads;
	}
	
	// Retrieve non-member download by code
	//   format array exactly as by user
	public function get_downloads_by_code($code){
		$row =  $this->db->where('code', $code)->get('download_packages')->row();
		
		if($row)
		{
			return array(	
				$row->order_id => $this->get_package_files($row->id)
			);
		}
	}
	
	// get the files in a package
	public function get_package_files($package_id){		
		return $this->db->select('*')
						->from('download_package_files as a')
						->join('digital_products as b', 'a.file_id=b.id')
						->where('package_id', $package_id)
						->get()
						->result();
	}
	
	// get file info for download by the link code
	//  increment the download counter
	public function get_file_info_by_link($link){		
		$record = $this->db->from('digital_products as a')
						->join('download_package_files as b', 'a.id=b.file_id')
						->where('link', $link)
						->get()
						->row();	
		return $record;
		
	}
		
	public function touch_download($link){
		$this->db->where('link', $link)->set('downloads','downloads+1', false)->update('download_package_files');
	}
}