<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['ibolz_app_id'] = 'com.balepoint.ibolz.moviebay';
$config['ibolz_api'] = 'http://moviebay2.ibolz.tv/';

$config['api_channel_list'] = '/api/metadata/channel_list';
$config['api_metadata_content'] = '/api/metadata/get_content';

// PATH RESOURCES
$config['digital_content_thumbnail_path'] = 'http://moviebay2.ibolz.tv/admin/media/thumbnail/';