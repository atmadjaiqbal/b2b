<?php defined('BASEPATH') OR exit('No direct script access allowed');


$config['default_product_price'] = '0';
$config['default_product_type'] = 'non-package';

$config['default_category_id'] = '1'; // un-used
$config['default_category_name'] = 'non-package';  // un-used

$config['default_expired_date'] = date("Y-m-d", strtotime('+3 month'));