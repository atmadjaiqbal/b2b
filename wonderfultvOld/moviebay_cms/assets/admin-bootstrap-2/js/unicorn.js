// === Tooltips === //
function init_tooltip(){
	$('.tip').tooltip();	
	$('.tip-left').tooltip({ placement: 'left' });	
	$('.tip-right').tooltip({ placement: 'right' });	
	$('.tip-top').tooltip({ placement: 'top' });	
	$('.tip-bottom').tooltip({ placement: 'bottom' });	
}

$(document).ready(function(){		
	// === Sidebar navigation === //	
	$('.submenu > a').click(function(e){
		e.preventDefault();
		var submenu = $(this).siblings('ul');
		var li = $(this).parents('li');
		var submenus = $('#sidebar li.submenu ul');
		var submenus_parents = $('#sidebar li.submenu');
		if(li.hasClass('open')){
			if(($(window).width() > 768) || ($(window).width() < 479)) {
				submenu.slideUp();
			} else {
				submenu.fadeOut(250);
			}
			li.removeClass('open');
			$.cookie('app.moviebay.menuopen', null, _cookie_path);
		} else {
			if(($(window).width() > 768) || ($(window).width() < 479)) {
				submenus.slideUp();			
				submenu.slideDown();
			} else {
				submenus.fadeOut(250);			
				submenu.fadeIn(250);
			}
			submenus_parents.removeClass('open');		
			li.addClass('open');	
			$.cookie('app.moviebay.menuopen', li.index(), _cookie_path);
		}
	});

	$('#sidebar a').click(function(e){		
		var li = $(this).parent('li');
		if(!li.hasClass('submenu')){
			$.cookie('app.moviebay.menuactive', $(this).text().trim(), _cookie_path);
		}
	});

	var _cookie_path = {path: '/admin'};
	var menuopen = $.cookie('app.moviebay.menuopen');
	var menuactive = $.cookie('app.moviebay.menuactive');
	if(menuopen){
		$('#sidebar > ul > li:eq('+menuopen+')').addClass('open active');
		$('#sidebar > ul > li:eq('+menuopen+') > ul').css('display', 'block');
	}
	if(menuactive){
		var li = $('#sidebar li a:contains('+menuactive+')').parent('li');
		li.each(function(idx, item){
			if( $('a', $(item)).text().trim() == menuactive){
				$(item).addClass('active');
			}
		});		
		if(li.parent().parent('li').length == 0){
			$('li.open.active').removeClass('active');
		}			

		$('#breadcrumb .current').html(menuactive);
	}	

	var ul = $('#sidebar > ul');	
	// $('#sidebar > a').click(function(e)
	// {
	// 	e.preventDefault();
	// 	var sidebar = $('#sidebar');
	// 	if(sidebar.hasClass('open'))
	// 	{
	// 		sidebar.removeClass('open');
	// 		ul.slideUp(250);
	// 	} else 
	// 	{
	// 		sidebar.addClass('open');
	// 		ul.slideDown(250);
	// 	}
	// });
	
	// === Resize window related === //
	$(window).resize(function()
	{
		if($(window).width() > 479)
		{
			ul.css({'display':'block'});	
			$('#content-header .btn-group').css({width:'auto'});		
		}
		if($(window).width() < 479)
		{
			ul.css({'display':'none'});
			fix_position();
		}
		if($(window).width() > 768)
		{
			$('#user-nav > ul').css({width:'auto',margin:'0'});
            $('#content-header .btn-group').css({width:'auto'});
		}
	});
	
	if($(window).width() < 468){
		ul.css({'display':'none'});
		fix_position();
	}
	if($(window).width() > 479){
	   $('#content-header .btn-group').css({width:'auto'});
		ul.css({'display':'block'});
	}
	
	init_tooltip();
	
	// === Fixes the position of buttons group in content header and top user navigation === //
	function fix_position()
	{
		var uwidth = $('#user-nav > ul').width();
		$('#user-nav > ul').css({width:uwidth,'margin-left':'-' + uwidth / 2 + 'px'});        
        var cwidth = $('#content-header .btn-group').width();
        $('#content-header .btn-group').css({width:cwidth,'margin-left':'-' + uwidth / 2 + 'px'});
	}
	
	// ==== jquery plugin select2 ==== //
	// $("select").select2();

	$('#content').css('min-height', $(window).height()+'px');

	$('.container-fluid .row').each(function(idx, row){
		$(row).removeClass('row').addClass('row-fluid');
	});
});
