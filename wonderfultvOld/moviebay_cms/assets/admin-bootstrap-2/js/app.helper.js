// JavaScript Document	
$(document).ready(function(){
	window.app.ajax_setup();	

	if($('#page-title').length){
		$('#page-title').appendTo( '#content-header' );
	}

	if($('#page-shortcut').length){
		$('#page-shortcut').appendTo( '#content-header' );
	}

	if($('.alert-splash').length > 0){
	    window.setTimeout(function(){
	      $('.alert-splash').addClass('fade').hide('slow');
	    }, 2500);    
	}
	
	$('select').select2();
});
window.app = {
	ajax_setup: function(){
		$(document).ajaxSend(function(e, xhr, options) {
			if(options.showAjaxLoader){							
				$.blockUI({ 
					css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					}
				}); 
			}
		}).ajaxComplete(function(e, xhr, options) {			
		  	$.unblockUI();
		  	$.ajaxSettings.showAjaxLoader = false;
			/*window.history.pushState(null, null, '#'+options['url']);*/
		});	
	},
	helper: {
		build_error_message: function(err_code, err_msg){
			return '<div class="alert alert-error">the server responded with a status of '+err_code+' (' +err_msg+')</div>';
		},

		generate_slug: function(input_form, output_form, space_character, disallow_dashes){
		    space_character = space_character || '-';
		    $(input_form).slugify({ slug: output_form, type: space_character });
		}

	},	
	confirm_delete: function($url){
		bootbox.confirm("Are you sure you want to delete this? It cannot be undone.", function(result) {		  
		  if(result){
		  	console.log("do delete "+$url);
		  	window.location.href = $url 			
		  }
		}); 
	}
}