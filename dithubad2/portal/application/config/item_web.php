<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider01.jpg & slider02.jpg

*/

$config['nameLogo'] = "DITHUBAD TV";
$config['title'] = "IBOLZ | DITHUBAD TV";
$config['warnaHeader'] = "#777777";
$config['colorMenu'] = "#777777";
$config['warnaCopyright'] = "#777777";
$config['warnaNavCarousel'] = "#777777";
$config['colorListbox'] = "green";
$config['warnaMenuMobile'] = "#777777";
$config['footerCopyright'] = "DITHUBAD TV";

/* --- About Us --- */

$config['aboutUs'] = "<p><b>DITHUBAD</b></p><p>Direktorat Perhubungan TNI Angkatan Darat disingkat Dithubad bertugas menyelenggarakan pembinaan personel dan menyelenggarakan fungsi perhubungan dalam rangka mendukung tugas TNI Angkatan Darat.</p>
            <p>Dithubad dipimpin oleh Direktur Perhubungan TNI Angkatan Darat disingkat Dirhubad yang berkedudukan di bawah dan bertanggung jawab kepada Kasad, dalam pelaksanaan tugas sehari-hari dikoordinasikan oleh Wakasad.</p>
        	";

/* --- Contact Us --- */

$config['detailOffice'] = "Jika anda mempunyai pertanyaan, silakan hubungi kami :";
$config['address'] = "Markas Besar Angkatan Darat, Jl. Veteran No. 5 Jakarta Pusat 10110";
$config['tlp'] = "Telp. 021-3456838";
$config['fax'] = "";
$config['contactCenter'] = "";
$config['email'] = "";
$config['website'] = "";

/* --- Download --- */

$config['namaFolder'] = "dithubad";
$config['namaAPK'] = "DithubadTV-2.0.7-1446727134";

?>

