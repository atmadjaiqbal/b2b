<?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>

<section id="main-container" class="portfolio-static">
	<div class="container">
		<div class="row">
			<?php
		    if($listmenu) {
		        foreach($listmenu as $row) {
		            $c_menu_id = $row['menu_id'];
		            $c_category_name = $row['category_name'];

		            if ((strpos($c_category_name, "Property") !== FALSE) OR (strpos($c_category_name, "Training") !== FALSE)) {
		            	continue;
					}
		            ?>
		            <hr style="margin-top: 0px !important;"/>
					<div class="col-md-12 heading">
						<h3 class="classic"><i class="fa fa-list-alt"></i><div class="title"></div><?php echo $c_category_name;?></h3>
					</div>

					<?php

                    if($row['submenu']) {
                        $i=0;
                        foreach($row['submenu'] as $rwContent) {
                            if($rwContent->title != 'Nomor Penting') {

                                // echo "<pre>";var_dump($listmenu);echo "</pre>";

                                $_c_product_id = $rwContent->product_id;
                                $_c_apps_id = $rwContent->apps_id;
                                $_c_menu_id = $rwContent->menu_id;
                                $_c_parent_menu_id = $rwContent->parent_menu_id;
                                $_c_menu_type = $rwContent->menu_type;
                                $_c_title = $rwContent->title;
                                $_c_thumbnail = $rwContent->thumbnail;
                                $_c_link = $rwContent->link;
                                $_c_icon_size = $rwContent->icon_size;
                                $_c_poster_size = $rwContent->poster_size;
                                $short_title = strlen($_c_title) > 14 ? substr($_c_title, 0, 14).'...' : $_c_title;

                                switch(strtolower($_c_menu_type))
                                {
                                    case 'menu' :
                                        $_link = base_url().'content/content_categories_list/'.$_c_menu_id.'/'.urlencode($_c_title);
                                        break;
                                    case 'channel' :
                                        $_link = base_url().'content/content_channel/'.$_c_menu_id.'/'.$_c_product_id;
                                        break;
                                    default:
                                        $_link = base_url().'content/content_categories/'.$_c_menu_id.'/'.urlencode($_c_title);
                                        break;
                                }
                                ?>
                                        
                                        <?php
                                        if ((strtoupper($row['category_name'])==strtoupper("IBOLZ - Live Channel")) OR 
                                            (strtoupper($row['category_name'])==strtoupper("IBOLZ - Musika")) OR
                                            (strtoupper($row['category_name'])==strtoupper("IBOLZ - Movie & Trailer")) OR
                                            (strtoupper($row['category_name'])==strtoupper("IBOLZ - Live Channels"))) { ?>
                                            <div class="col-sm-2 col-xs-6 portfolio-static-item">
                                                <a href="<?php echo $_link;?>">
                                                    <div class="grid" style="border: none;">
                                                        <figure class="effect-oscar">
                                                            <img src="<?php echo $_c_thumbnail;?>&w=300" alt="">
                                                        </figure>
                                                    </div>
                                                </a>
                                            </div><!--/ item 1 end -->
                                        <?php
                                        }
                                        else { 
                                            foreach ($row['channel_detail'][$i] as $value) {
                                                $viewer = !empty($value->countviewer) ? $value->countviewer : '';
                                                $description = !empty($value->description) ? $value->description : '';
                                            } ?>
                                            <div class="col-sm-3 col-xs-6 portfolio-static-item">
                                                <a href="<?php echo $_link;?>">
                                                    <div class="grid" style="height: 250px;overflow: hidden;">
                                                        <figure class="effect-oscar">
                                                            <span class="layer-block"><?php echo $short_title;?></span> 
                                                            <img src="<?php echo $_c_thumbnail;?>&w=300" alt="">
                                                            <span class="layer-viewer"><i class="fa fa-eye"></i> &nbsp<?php echo $viewer; ?> viewer</span>    
                                                        </figure>
                                                        <div class="portfolio-static-desc">
                                                            <p><?php echo $description;?></p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div><!--/ item 1 end -->
                                        <?php }
                                        ?>

                            <?php
                            }
                            $i++;
                        }
                    }
		            ?>
		            <div class="clearfix"></div>
		            <?php
		        }
		    }
		?>
				
		</div><!-- Content row end -->
	</div><!-- Container end -->
</section>