<div id="banner-area">
    <img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt ="" style="width: 100%;"/>
    <div class="parallax-overlay"></div>
</div>

<section id="main-container" class="portfolio-static" style="background-color: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h3 class="classic"><i class="fa fa-list-alt"></i><div class="title"></div><?php echo $menu_title;?></h3>
            </div>

            <?php
            // echo "<pre>";var_dump($detail);echo "</pre>";

            if($list_content) {
                $crew_category_list = array(); $genre_list = array();
                $i=0;
                foreach($list_content->menu_list as $val) {
                    $product_id = $val->product_id;
                    $apps_id = $val->apps_id;
                    $menu_id = $val->menu_id;
                    $parent_menu_id = $val->parent_menu_id;
                    $menu_type = $val->menu_type;
                    $title = strlen($val->title) > 14 ? substr($val->title, 0, 14).'...' : $val->title;

                    $thumbnail = $val->thumbnail . '&w=300';
                    $count_product = $val->count_product;

                    switch($menu_type)
                    {
                        case 'menu' :
                            $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title);
                            break;
                        case 'channel' :
                            $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                            break;
                        default:
                            $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                            break;
                    }

                    if($title != 'Nomor Penting') { ?>

                        <?php
                        if (
                            strtoupper($menu_title) == strtoupper('Live Channel')
                            OR
                            strtoupper($menu_title) == strtoupper('Live Channels')
                        ) { ?>
                            <div class="col-sm-2 col-xs-6 portfolio-static-item">
                                <a href="<?php echo $_link;?>">
                                    <div class="grid" style="border: none;">
                                        <figure class="effect-oscar">
                                            <img src="<?php echo $thumbnail;?>" alt="">
                                        </figure>
                                    </div>
                                </a>
                            </div><!--/ item 1 end -->
                        <?php
                        }
                        else {                            
                            $viewer = (isset($detail[$i]['channel_detail']->countviewer) ? $detail[$i]['channel_detail']->countviewer : '0');
                            $description = (isset($detail[$i]['channel_detail']->description) ? $detail[$i]['channel_detail']->description : $title);
                            ?>
                            <div class="col-sm-3 col-xs-6 portfolio-static-item">
                                <a href="<?php echo $_link;?>">
                                    <div class="grid" style="height: 250px;overflow: hidden;">
                                        <figure class="effect-oscar">
                                            <span class="layer-block"><?php echo $title;?></span> 
                                            <img src="<?php echo $thumbnail;?>" alt="">
                                            <span class="layer-viewer"><i class="fa fa-eye"></i> &nbsp<?php echo $viewer; ?> viewer</span>    
                                        </figure>
                                        <div class="portfolio-static-desc" style="padding: 0px 5px;">
                                            <p><?php echo $description;?></p>
                                        </div>
                                    </div>
                                </a>
                            </div><!--/ item 1 end -->
                        <?php
                        }
                    }
                    $i++;
                }
            }
            ?>
        </div><!-- Content row end -->
    </div><!-- Container end -->
</section>