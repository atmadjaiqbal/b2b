var host = '';
$(function(){

    content_container('#content_container');
    $('#content_load_more').click(function(e){
        e.preventDefault();
        content_container('#content_container');
    });

    comment_list('#comment');

});

function content_container(holder)
{
    var the_loader = '<div id="loadmore-loader" class="loader"></div>';
    //$(holder).append(the_loader);
    $('#content_load_more').text('');
    $('#load_more_place').append(the_loader);

    $('#loadmore-loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var categories = $(holder).data('categories');

    $.get(Settings.base_url+'content/get_content_categories/'+categories+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);
        $('#loadmore-loader').remove();
        $('#content_load_more').text('Load More');
    });
}


function vod_list(holder)
{
    var the_loader = '<div id="loadmore-loader" class="loader"></div>';
    //$(holder).append(the_loader);
    $('#content_load_more').text('');
    $('#load_more_place').append(the_loader);

    $('#loadmore-loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var channel_id = $(holder).data('channelid');

    $.get(Settings.base_url+'content/vodlist/'+channel_id+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#loadmore-loader').remove();
        $('#content_load_more').text('Load More');

    });

}

function liketotal(holder)
{
    var id = $(holder).data('id');
    var uri = Settings.base_url+'content/total_like/'+ id;
    $.get(uri, function(data){
        $(holder).html('');
        $(holder).append(data);
        console.log(id+' total : '+data);
    });
}

function comment_list(holder)
{
    var id = $(holder).data('id');
    var page = $(holder).data('page');
    var uri = Settings.base_url+'content/content_comment/'+ id + '/'+page;
    $.get(uri, function(data){
        $(holder).append(data);
        $(holder).data('page', parseInt(page) + 1);
    });

}