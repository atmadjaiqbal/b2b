<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <title><?php echo config_item('title'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    $_metadesc = 'Ibolz.';
    ?>
    <meta name="description" content="<?php echo $_metadesc; ?>"/>
    <meta name="keywords" content="ibolz, streaming, television, mobile, vod, video on demand, ibolz"/>
    <meta name="author" content="Ibolz Team" />
    <meta name="webcrawlers" content="all" />
    <meta name="rating" content="general" />
    <meta name="spiders" content="all" />
    <meta name="robots" content="index,follow">

    <meta name="google-site-verification" content="" />
    <meta charset="utf-8" />

    <meta property="og:title" content="Ibolz Manulife"/>
    <meta property="og:type" content="media"/>
    <meta property="og:url" content="http://www.ibolz.tv"/>
    <meta property="og:image" content=""/>
    <meta property="og:site_name" content="www.ibolz.tv"/>
    <meta property="og:description" content=""/>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validation/jquery.validationEngine.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validation/jquery.validationEngine-en.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jquery.validation/validationEngine.jquery.css" type="text/css" />

  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <!-- Template styles-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
  <!-- Responsive styles-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
  <!-- FontAwesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Animation -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
  <!-- Prettyphoto -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/prettyPhoto.css">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.theme.css">
  <!-- Flexslider -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flexslider.css">
  <!-- Flexslider -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cd-hero.css">
  <!-- PopUp Gallery -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
  <!-- Style Swicther -->
  <link id="style-switch" href="<?php echo base_url(); ?>assets/css/preset3.css" media="screen" rel="stylesheet" type="text/css">
  <!-- Font -->
  <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

  <!-- Style Custom -->
  <style type="text/css">
    .header-bgnone{
      background: <?php echo config_item('warnaHeader'); ?>;
    } 

    .dropdown-menu > ul > li>a:hover, 
    .dropdown-menu > ul > li>a:focus{
      background: none;
      color: <?php echo config_item('colorMenu'); ?> !important;
    }
    #copyright{
      padding: 30px 0;
      background: <?php echo config_item('warnaCopyright'); ?>;
      color: #fff;
    }
    #main-slide .carousel-control .fa-angle-right:hover, #main-slide .carousel-control .fa-angle-left:hover{
      background-color: <?php echo config_item('warnaNavCarousel'); ?> !important;
    }
    .portfolio-static-item .grid{
      border: 1px solid <?php echo config_item('colorListbox'); ?>;
        padding: 3px;
        height: auto;
    }
    .navbar-toggle{
      background: <?php echo config_item('warnaMenuMobile'); ?>;
    }
  </style>

  <!-- Base URL JS -->
  <script type="text/javascript">
        var Settings = <?php
        if(str_replace("http://","",current_url()) == $_SERVER['SERVER_ADDR'])
        {
           $settings = array('base_url' => $_SERVER['SERVER_ADDR']);
        } else {
         $settings = array('base_url' => base_url());
        }
      echo json_encode($settings);
      ?>;
    </script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

  <!-- Javascript Files
  ================================================== -->
  <link rel="icon" href="<?php echo base_url(); ?>assets/images/header_logo.png" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/header_logo.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/header_logo.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/header_logo.png">
 
  <!-- initialize jQuery Library -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
  <!-- Bootstrap jQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  <!-- Style Switcher -->
  <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/style-switcher.js"></script>
  <!-- PopUp Gallery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.magnific-popup.js"></script>
  <!-- Owl Carousel -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.js"></script>
  <!-- PrettyPhoto -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js"></script>
  <!-- Bxslider -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
  <!-- CD Hero slider -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/cd-hero.js"></script>
  <!-- Isotope -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/isotope.js"></script>
  
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ini.isotope.js"></script>
  <!-- Wow Animation -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
  <!-- SmoothScroll -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
  <!-- Eeasing -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>
  <!-- Waypoints -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/waypoints.min.js"></script>
  <!-- Template custom -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>

    <?php if (!empty($scripts)) : ?>
        <?php foreach ($scripts as $script) : ?>
            <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>


    <script type="text/javascript">
        var Settings = <?php
        if(str_replace("http://","",current_url()) == $_SERVER['SERVER_ADDR'])
        {
           $settings = array('base_url' => $_SERVER['SERVER_ADDR']);
        } else {
         $settings = array('base_url' => base_url());
        }
      echo json_encode($settings);
      ?>;
    </script>
    <script type="text/javascript">if(jQuery(window).width()>1024){document.write("<"+"script src='<?php echo base_url(); ?>assets/js/jquery.preloader.js'></"+"script>");} </script>
    <script type="text/javascript" language="javascript">
        /*     jQuery(window).load(function() {
         $x = $(window).width();
         if($x > 1024)
         {
         jQuery("#content .row").preloader();    }

         jQuery('.magnifier').touchTouch();
         jQuery('.spinner').animate({'opacity':0},1000,'easeOutCubic',function (){jQuery(this).css('display','none')});
         }); */
    </script>


    <!--[if lt IE 8]>
<!--    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>-->
    <!--[endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <!--<![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/docs.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ie.css" type="text/css" media="screen">
    <![endif]-->

</head>

<body>
<div class="header-wrapper">
   <div class="body-inner">

  <!-- Header start -->
    <?php
    if (isset($header_custom)) { ?>
      <header id="header" class="navbar-fixed-top header3" role="banner" style="background-color:#895B3A;">
      <?php
    }
    else { ?>
      <header id="header" class="navbar-fixed-top header3" role="banner">
      <?php
    }

    ?>  
    <div class="container">
      <div class="row">
        <!-- Logo start -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand" style="padding: 0px 15px !important;">
              <a href="<?php echo base_url(); ?>">
                <img class="img-responsive" src="<?php echo base_url(); ?>assets/images/logo_header.png" alt="logo">
                <?php if (!empty(config_item('nameLogo'))) echo "<h2>".config_item('nameLogo')."</h2>"; ?>
              </a> 
            </div>                   
        </div><!--/ Logo end -->
        <nav class="collapse navbar-collapse clearfix" role="navigation">
          <ul class="nav navbar-nav">
            <?php
               if($this->session->userdata('display_name'))
               {
                   $homelink = base_url().'home/index';
               } else {
                   $homelink = base_url();
               }
               ?>
            <li><a href="<?php echo $homelink;?>">Home</a></li>
            <?php foreach($headermenu->menu_list as $val) {
              if ((strpos($val->title, "Maps") !== FALSE)) {
                      continue;
              }
                  ?>
            <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo strtoupper($val->title);?>&nbsp;<i class="fa fa-angle-down"></i></a>
                          <?php if(count($val->sub_menu_list) > 0) { ?>
                <div class="dropdown-menu">
                <div class="ul">
                  <?php foreach($val->sub_menu_list as $submenu) { ?>
                                  <?php
                                  $head_product_id = $submenu->product_id;
                                  $head_menu_id = $submenu->menu_id;
                                  $head_menu_type = $submenu->menu_type;
                                  $head_title = $submenu->title;

                                  $_link = '';
                                  switch($submenu->menu_type)
                                    {
                                        case 'menu' :
                                            $_link = base_url().'content/content_categories_list/'.$head_menu_id.'/'.urlencode($submenu->title);
                                            break;
                                        case 'channel' :
                                            $_link = base_url().'content/content_channel/'.$head_menu_id.'/'.$head_product_id;
                                            break;
                                        default:
                                            $_link = base_url().'content/content_categories/'.$head_menu_id.'/'.urlencode($submenu->title);
                                            break;
                                    }
                                ?>
                                  <div class="li"><a href="<?php echo $_link; ?>"><?php echo strtoupper($submenu->title);?></a></div>
                                <?php } ?>
                            </div>
                        </div>
                  <?php } ?>
                      </li>
                  <?php } ?>
                  <li><a href="<?php echo base_url().'home/home_contactus';?>">Contact Us</a></li>
                  <li><a href="<?php echo base_url().'home/home_aboutus';?>">About Us</a></li>
                </ul>
        </nav><!--/ Navigation end -->
      </div><!--/ Row end -->
    </div><!--/ Container end -->
  </header><!--/ Header end -->
</div>