<div id="popular">
    <div class="title-place">POPULAR</div>
    <div class="content-place">
        <?php
        if($popular->product_list)
        {
            $pop = 0; foreach($popular->product_list as $rowpop)
        {
            $pop_menu_id = $rowpop->menu_id; $pop_apps_id = $rowpop->apps_id;
            $pop_product_id = $rowpop->product_id; $pop_channel_category_id = $rowpop->channel_category_id;
            $pop_channel_id = $rowpop->channel_id; $pop_channel_type_id = $rowpop->channel_type_id;
            $pop_content_id = $rowpop->content_id; $pop_content_type = $rowpop->content_type;
            $pop_title = $rowpop->title; $pop_thumbnail = $rowpop->thumbnail. '&w=100';
            $pop_description = $rowpop->description; $pop_prod_year = $rowpop->prod_year;
            $pop_video_duration = $rowpop->video_duration; $pop_countlike = $rowpop->countlike;
            $pop_countviewer = $rowpop->countviewer; $pop_icon_size = $rowpop->icon_size;
            $pop_poster_size = $rowpop->poster_size; $pop_crew_category_list = $rowpop->crew_category_list;
            if($rowpop->crew_category_list) { $pop_lecture = $rowpop->crew_category_list[0]->crew_list[0]->crew_name;} else { $pop_lecture = '';}
            if($pop == 0)
            {
                $pop_shorttitle = strlen($pop_title) > 15 ? substr($pop_title, 0, 15).'...' : $pop_title;
                ?>
                <div class="popular-item-1">
                    <div class="img-place">
                        <img src="<?php echo $pop_thumbnail;?>">
                        <!-- div style="background-color:#EEEEEE;background: url(< ? php echo $pop_thumbnail;?>) no-repeat scroll center rgba(0, 0, 0, 0);width:90px;height:90px;'"></div -->
                    </div>
                    <div class="content-place-1">
                        <h3><a href="<?php echo base_url();?>content/content_detail/<?php echo $pop_content_id; ?>/<?php echo $pop_menu_id;?>/<?php echo urlencode($menu_title);?>">
                            <?php echo $pop_shorttitle;?>
                        </a></h3>
                        <dl>
                            <dt>DATE</dt><dd>:&nbsp;&nbsp;<?php echo $pop_prod_year;?></dd>
                            <dt>LECTURE</dt><dd>:&nbsp;&nbsp;<?php echo (strlen($pop_lecture) > 15 ? substr($pop_lecture, 0, 15) . '...' : $pop_lecture);?></dd>
                        </dl>
                        <p class="sub-title">DESCRIPTION</p>
                        <p class="sub-description"><?php echo (strlen($pop_description) > 70 ? substr($pop_description, 0, 70).'...' : $pop_description);?></p>
                    </div>

                </div>
            <?php
            } else {
                $pop_shorttitle = strlen($pop_title) > 30 ? substr($pop_title, 0, 30).'...' : $pop_title;
                ?>
                <div class="popular-item-2">
                    <div class="content-place-2">
                        <h3><a href="<?php echo base_url();?>content/content_detail/<?php echo $pop_content_id; ?>/<?php echo $pop_menu_id;?>/<?php echo urlencode($menu_title);?>"><?php echo $pop_shorttitle;?></a></h3>
                        <p class="sub-lecture">LECTURE :&nbsp;&nbsp;<?php echo $pop_lecture;?></p>
                        <p class="sub-lecture pull-right" style="margin-right: 20px;margin-top:-35px;"><strong><?php echo $pop_countviewer;?></strong></p>
                    </div>
                </div>
            <?php
            }
            $pop++;
        }
        }
        ?>
    </div>
</div>
