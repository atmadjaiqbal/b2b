<div class="container">

<h3>LASTEST ARTICLES</h3>
        <!-- Page Features -->
        <div class="row text-center">
<?php
    if($result)
    {
        foreach($result->product_list as $rwcontent)
        {
            $sub_menu_id = $rwcontent->menu_id;
            $sub_apps_id = $rwcontent->apps_id;
            $sub_product_id = $rwcontent->product_id;
            $sub_channel_category_id = $rwcontent->channel_category_id;
            $sub_channel_id = $rwcontent->channel_id;
            $sub_channel_type_id = $rwcontent->channel_type_id;
            $sub_content_id = $rwcontent->content_id;
            $sub_content_type = $rwcontent->content_type;
            $sub_title = $rwcontent->title;
            $short_title = strlen($sub_title) > 14 ? substr($sub_title, 0, 14).'...' : $sub_title;
            $sub_thumbnail = $rwcontent->thumbnail.'&w=200px';
            $sub_description = $rwcontent->description;
            $sub_prod_year = $rwcontent->prod_year;
            $sub_video_duration = $rwcontent->video_duration;
            $sub_countlike = $rwcontent->countlike;
            $sub_countviewer = $rwcontent->countviewer;
            $sub_icon_size = $rwcontent->icon_size;
            $sub_poster_size = $rwcontent->poster_size;
            $sub_crew_category_list = $rwcontent->crew_category_list;
            $sub_genre_list = $rwcontent->genre_list;

    ?>
            <div class="col-md-3 col-sm-12 hero-feature" style="padding-bottom:20px;">
                <a href="<?php echo base_url();?>content/content_detail/<?php echo $sub_content_id; ?>/<?php echo $sub_menu_id;?>">
                <div class="thumbnail itemV" style="padding-top:5px;">
                    <img style="width:98%; height:140px;"  src="<?php echo $sub_thumbnail;?>&w=160" alt="" id="'<?php echo $sub_menu_id;?>'-'<?php echo $sub_product_id;?>'"
                     data-id="'<?php echo $sub_product_id;?>'" data-menu-id="'<?php echo $sub_menu_id;?>'">
                    <div class="caption">
                        <h5 style="font-weight:300;"><?php echo $short_title;?></h5>
                        
                    </div>
                </div>
                </a>
            </div>

    <?php } ?>
        </div>

    <?php } ?>
</div>