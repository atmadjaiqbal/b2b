<div class="container" style="margin-top:140px; margin-bottom:10px; background:#eee; padding:10px 10px; border-radius:10px;" align="center">
<h3>
    <a style="cursor: default;"><span><?php echo $menu_title;?></span></a>
</h3>
</div>
<div class="container" style="margin-bottom:10px; background:#eee; padding:20px 10px; border-radius:10px;">
<?php
        if($list_content)
        {
            $crew_category_list = array(); $genre_list = array();
            foreach($list_content->product_list as $val)
            {
                $menu_id = $val->menu_id;
                $apps_id = $val->apps_id;
                $product_id = $val->product_id;
                $channel_category_id = $val->channel_category_id;
                $channel_id = $val->channel_id;
                $channel_type_id = $val->channel_type_id;
                $content_id = $val->content_id;
                $content_type = $val->content_type;
                $title = $val->title;
                $short_title = strlen($title) > 70 ? substr($title, 0, 70). '...' : $title;
                $thumbnail = $val->thumbnail.'&w=200';
                $description = $val->description;
                $prod_year = $val->prod_year;
                $video_duration = $val->video_duration;
                $countlike = $val->countlike;
                $countcomment = $val->countcomment;
                $countviewer = $val->countviewer;
                $icon_size = $val->icon_size;
                $poster_size = $val->poster_size;
                $crew_category_list = $val->crew_category_list;
                $genre_list = $val->genre_list;
                $related_list = $val->related_list;
                if($val->crew_category_list) { $lecture = $val->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}

                ?>


        <script type="text/javascript">
            $(document).ready(function() {
                $('#like-<?php echo $content_id; ?>').click(function(e){
                    e.preventDefault();
                    var id = $(this).data('id');
                    $.post(Settings.base_url+'content/post_like/', {'content_id':id}, function(data){
                        if(data.rcode != 'ok'){
                            alert(data.msg);
                        } else {
                            liketotal('#totallike-<?php echo $content_id; ?>');
                        }
                    });
                });
            });
        </script>

        <div class="artic-box" style="padding:10px 0px;">
            <div class="row" style="padding:10px 30px;">
                <div class="col-md-3">
                    <div class="row">
                        <div style="height:auto; background:#cecece;">
                            <img src="<?php echo $thumbnail;?>" style="width:100%; height:auto;">
                        </div>
                        <div style="height:auto;">
                            <?php
                        $r_thumbnail = base_url().'assets/images/no_video.png';
                        if($related_list)
                        {
                            $r_content_id = $related_list[0]->content_id;
                            $r_content_related_id = $related_list[0]->content_related_id;
                            $r_title = $related_list[0]->title;
                            $r_title = strlen($r_title) > 14 ? substr($r_title, 0, 14).'...' : $r_title;
                            $r_video_duration = $related_list[0]->video_duration;
                            $r_thumbnail = $related_list[0]->thumbnail.'&w=240';
                        ?>
                            <a href="<?php echo base_url().'content/content_detail/'.$r_content_related_id.'/'.$menu_id;?>">
                                <div class="product-title">
                                    <span class="pull-left"><?php echo $r_title;?></span>
                                    <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                </div>
                                <div class="product-image">
                                    <div class="preview_product related bgfull"
                                         style="background-color:#cecece;background: url('<?php echo $r_thumbnail;?>') no-repeat;width:auto;height:90px;">
                                    </div>
                                </div>
                            </a>
                        <?php
                        } else {
                        ?>
                            <div align="center" vlign="middle"><img src="<?php echo $r_thumbnail;?>" style="width:100%; height:auto;"></div>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div style="max-height:140px; height:auto;">
                        <h4><a  href="<?php echo base_url();?>content/content_detail/<?php echo $content_id; ?>/<?php echo $menu_id;?>/<?php echo urlencode($menu_title);?>"><span><?php echo $short_title;?></span></a></h4>
                        <h5 style="font-weight:bold;">DESCRIPTION</h5>
                        <p style="font-size:13px;"><?php echo (strlen($description) > 135 ? substr($description, 0, 135).'...' : $description);?></p>
                    </div>
                    <div class="border-bott"></div>
                    <div style="height:auto;">
                        <div class="row">
                            <div class="col-md-8" style="padding-top:20px;">
                                <table width="100%">
                                    <tr>
                                        <td width="20%;" style="max-width:10%;">LECTURE</td>
                                        <td width="10px;">:</td>
                                        <td style="max-width:100%;"><?php echo $lecture;?></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" style="max-width:10%;">DATE</td>
                                        <td width="10px;">:</td>
                                        <td style="max-width:100%;"><?php echo $prod_year;?></td>
                                    </tr>
                                    <tr>
                                        <td width="20%;" style="max-width:10%;">TIME</td>
                                        <td width="10px;">:</td>
                                        <td style="max-width:100%;">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-4" align="center" vlign="middle" style="padding:10px; padding-top:20px;">
                                <div>
                                    <a class="btn btn-warning" style="border-radius:2px; font-size:20px; width:90%;" href="<?php echo base_url();?>content/content_detail/<?php echo $content_id; ?>/<?php echo $menu_id;?>/<?php echo urlencode($menu_title);?>">VIEW</a>
                                </div>
                                <div class="border-bott"></div>
                                <div style="width:98%; padding:10px;">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <a class="score-btn" style="cursor:pointer;" data-tipe='like' id='like-<?php echo $content_id; ?>' data-id='<?php echo $content_id; ?>'>
                                                <img class="pull-left" src="<?php echo base_url('assets/images/like_icon_small.png'); ?>">
                                            <p id="totallike-<?php echo $content_id; ?>" data-id='<?php echo $content_id; ?>'><?php echo $countlike; ?></p>
                                            </a>
                                        </div>
                                        <div class="col-sm-4">
                                            <a class="score-btn" style="cursor:pointer;" data-tipe='comment' data-id='<?php echo $content_id; ?>'>
                                                <img class="pull-left" src="<?php echo base_url('assets/images/comment_icon_small.png'); ?>">
                                            <p><?php echo $countcomment; ?></p>
                                            </a>

                                        </div>
                                        <div class="col-sm-4">
                                            <?php
                                            $_message = "Hi, saya ingin share materi ini, materi yang menarik dan bagus untuk dipelajari. %0D%0A";
                                            $_message .= "Link urlnya sebagai berikut ini : %0D%0A%0D%0A";
                                            $_message .= current_url();
                                            $_message .= "%0D%0A%0D%0ASelamat belajar.";
                                            //$_message .= "Atau %3Ca%20href%3D%22".current_url()."%22%3Eklik link ini%3C%2Fa%3E untuk langsung ke materi.";
                                            ?>
                                            <a style="cursor:pointer;" href="mailto:?subject=<?php echo $title;?>&amp;body=<?php echo $_message; ?>" title="Share By Email" class="score-btn">
                                                <img src="<?php echo base_url('assets/images/share_by_email.png'); ?>" style="height:25px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><br>

<?php
            }
        }
        ?>
<!-- conteinr -->
</div>

<div class="container" style="padding:20px 0px;">
 <div class="row">
    <div id="content_container" data-page='1' data-categories="<?php echo $menu_id;?>"></div>

    <div class="clear row loadmore-contentplace">
        <div class="text-center">
            <div id="load_more_place" class="loadmore loadmore-bg">
                <a id="content_load_more" class="btn btn-warning" href="#">LOAD MORE</a>
            </div>
        </div>
    </div>
 </div>
</div>