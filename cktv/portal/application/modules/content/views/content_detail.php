<div class="container" style="margin-top:120px; margin-bottom:10px; background:#eee; padding:5px 10px; border-radius:10px;">
<?php
if($product_detail)
{
    $player_wrap_width = "620"; $player_width = "720"; $player_height = "500";
    $crew_category_list = array(); $genre_list = array(); $photo_list = array(); $related_list = array();
    $rcode = $product_detail->rcode;
    $channel_id = $product_detail->channel_id;
    $content_id = $product_detail->content_id;
    $title = $product_detail->title;
    $video_duration = $product_detail->video_duration;
    $filename = $product_detail->filename;
    $thumbnail = $product_detail->thumbnail;
    $description = $product_detail->description;
    $prod_year = $product_detail->prod_year;
    $trailer_id = $product_detail->trailer_id;
    $price = $product_detail->price;
    $ulike = $product_detail->ulike;
    $unlike = $product_detail->unlike;
    $comment = $product_detail->comment;
    if($product_detail->crew_category_list) { $lecture = $product_detail->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}
    $genre_list = $product_detail->genre_list;
    $photo_list = $product_detail->photo_list;
    $related_list = $product_detail->related_list;
    $document_list = $product_detail->document_list;
    $player = 'http://id.ibolz.tv/assets/flash/global/index.php?w='.$player_width.'&h='.$player_height;
    $player .= '&apps_id='.config_item('ibolz_app_id').'&version='.config_item('ibolz_version').'&type=content';
    $player .= '&channel_id='.$channel_id.'&vid='.$content_id.'&id='.$content_id.'&customer_id='.$customer_id;
?>

    <div class="row">
      <div class="col-sm-12" style="padding:0px 20px;">
        <h4>
            <a style="cursor: default;"><span><?php echo $title;?></span></a>
        </h4>
      </div>
    </div>
</div>

<div class="container" style="margin-bottom:10px; background:#eee; padding:20px 10px; border-radius:10px;">
    <div class="row" style="padding:10px;">
      <div class="col-md-4" style="background:#eee;">
          <div align="center" vlign="middle" style="height:auto; max-height:500px; overflow-Y:auto;">
            <img src="<?php echo $thumbnail;?>&w=300px" style="width:100%; height:auto;">  
          </div>
          <div style="background:#333; font-size:16px; color:#fff; padding:5px 10px;">
           <div class="row" align="center">
              <div class="col-sm-6" align="center">
                <a style="cursor:pointer;" id='like-<?php echo $content_id; ?>' data-id='<?php echo $content_id; ?>'>
                       <img src="<?php echo base_url('assets/images/like_icon_large_new.png'); ?>">
                       <span id="totallike-<?php echo $content_id; ?>" data-id='<?php echo $content_id; ?>'><?php echo $ulike; ?></span>
                   </a>&nbsp;
              </div>
              <div class="col-sm-6" align="center">
                <a class="score-btn" data-tipe='comment' data-id='<?php echo $content_id; ?>'>
                       <img src="<?php echo base_url('assets/images/comment_icon_large_new.png'); ?>">
                       <?php echo $comment; ?>
                   </a>&nbsp;
              </div>
           </div>
          </div>

          <div class="border-bott"></div>
          <div><h4>PHOTO</h4></div>
          <div>

            <div class="row">
              <?php
                if($photo_list)
                {
                   $y = 1; $photo_filename = array();
                   foreach($photo_list as $value)
                   {
                       $photonya = $value->thumbnail.'&w=200';
                       $photofull = $value->thumbnail.'&w=300';
                       $photo_content_id[$y] = $value->content_id;

                       if(isset($value->thumbnail))
                       {
                           $photo_filename[$y] = '
                           <a href="'.$photofull.'" title="'.$title.'">
                              <div style="background: url('.$photonya.') no-repeat;background-size: 100% 100%;"></div>
                           </a>
                           ';
                       } else {
                           $photo_filename[$y] = '';
                       }

                       //$photo_filename[$y] = isset($value->thumbnail) ? 'style="background: url('.$photonya.') no-repeat;background-size: 100% 100%;"' : '';
                       $photo_thumbnail[$y] = $value->filename;
                       $y++;
                   }
                }
                ?>

              <div class="col-md-4 col-sm-12 hero-feature">
                <div class="thumbnail">
                  <img src="<?php echo isset($photo_filename[1]) ? $photo_filename[1] : '';?>">
                </div>
              </div>

            </div>

          </div>

          <div class="border-bott"></div>
          <div style="padding-bottom:10px; max-height:500px; overflow-Y:auto;"><h4>DOCUMENT</h4></div>
          <div>
          <?php
                foreach($document_list as $docRow)
                {
                    $doc_content_id = $docRow->content_id;
                    $document_id = $docRow->document_id;
                    $doc_title = $docRow->title;
                    $doc_filename = $docRow->filename;
                    $doc_type = $docRow->type;
                    $doc_size = $docRow->size;
                ?>

                    <div style="border-bottom:1px solid #ddd; color:#999;">
                <?php

                $imgdoc = "Unknown";
                $word_mime = array('application/msword', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.ms-word.document.macroEnabled.12', 'application/vnd.ms-word.template.macroEnabled.12');
                $excel_mime = array('application/vnd.ms-excel','application/vnd.ms-excel', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.ms-excel.sheet.macroEnabled.12', 'application/vnd.ms-excel.template.macroEnabled.12',
                    'application/vnd.ms-excel.addin.macroEnabled.12', 'application/vnd.ms-excel.sheet.binary.macroEnabled.12');
                $power_mime = array('application/vnd.ms-powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    'application/vnd.openxmlformats-officedocument.presentationml.template', 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
                    'application/vnd.ms-powerpoint.addin.macroEnabled.12', 'application/vnd.ms-powerpoint.presentation.macroEnabled.12', 'application/vnd.ms-powerpoint.template.macroEnabled.12',
                    'application/vnd.ms-powerpoint.slideshow.macroEnabled.12');

                if($doc_type == 'application/pdf'){$imgdoc = base_url().'assets/images/onl-icon-pdf.png';}
                if(in_array($doc_type, $word_mime)) {$imgdoc = base_url().'assets/images/onl-icon-doc.png';}
                if(in_array($doc_type, $excel_mime)) {$imgdoc = base_url().'assets/images/onl-icon-excel.png';}
                if(in_array($doc_type, $power_mime)) {$imgdoc = base_url().'assets/images/onl-icon-ppt.png';}

                ?>
                        <span><?php echo $doc_filename;?></span>
                        <span class="pull-right">
                          <a href="<?php echo base_url().'content/document_download?document_id='.$document_id;?>">
                          <img src="<?php echo base_url().'assets/images/onl-icon-download.png';?>">
                          </a>
                        </span>
                        <p><?php echo $doc_title;?></p>
                    </div>
                <?php
                }
                ?>
          </div>
      </div>
      <div class="col-md-8">
        <div style="height:510px;" vlign="middle" align="center">
          <iframe seamless wmode="Opaque" class="player bg-loader" scrolling="no"
                        style='width: <?php echo $player_width;?>px;min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;'
                        src="<?php echo $player;?>">
          </iframe>
        </div>
        <div>&nbsp;</div>
        <div><h4>SESSION</h4></div>

        <div style="height:auto;">
          <div class="row">
              <?php
                $default_r_thumb = base_url().'assets/images/no_video.png'; $i = 1;
                if($related_list)
                {
                   foreach($related_list as $value)
                   {
                     if($i == 3) break;
                     $other_trailer = $value->thumbnail.'&w=200';
                     $r_content_id = $value->content_id;
                     $r_content_related_id = $value->content_related_id;
                     $r_title = $value->title;
                     $r_title = strlen($r_title) > 23 ? substr($r_title, 0, 23).'...' : $r_title;
                     $r_video_duration = $value->video_duration;
                     $r_thumbnail = isset($value->thumbnail) ? $other_trailer : $default_r_thumb;

                     // base_url().'content/content_detail/'.$sub_content_id.'/'.$sub_menu_id.
              ?>

              <div class="col-md-4 col-sm-12 hero-feature">
                <a href="<?php echo base_url().'content/content_detail/'.$r_content_related_id.'/'.$menu_id;?>">
                <div class="thumbnail">
                  <img src="<?php echo $r_thumbnail;?>" style="cursor:pointer; width:100%; height:40px;" title="<?php echo $r_title;?>">
                </div>
                </a>
              </div>

                    <?php
                         $i++;
                       }
                    }

                    if($i <= 3 || $i <= 2)
                    {
                        for($y = 1; $y <= (3 - $i) + 1; $y++)
                        {
                            echo '<div class="col-md-4 col-sm-12 hero-feature"><div class="thumbnail"><img src="'.$default_r_thumb.'" style="width:100%;"></div></div>';
                        }
                    }
                    ?>

          </div>
        </div>
        <div class="border-bott"></div><br>
        <div><h4>DESCRIPTION</h4></div>
        <div style="height:300px; background:#fff; overflow-Y:auto; padding:10px;">
          <h4><a style="cursor: default;"><span><?php echo $title;?></span></a></h4>
                <table width="100%" style="font-size:12px;">
                    <tr>
                        <td width="20%;" style="max-width:10%;">LECTURE</td>
                        <td width="10px;">:</td>
                        <td style="max-width:100%;"><?php echo $lecture;?></td>
                    </tr>
                    <tr>
                        <td width="20%" style="max-width:10%;">DATE</td>
                        <td width="10px;">:</td>
                        <td style="max-width:100%;"><?php echo $prod_year;?></td>
                    </tr>
                    <tr>
                        <td width="20%;" style="max-width:10%;">TIME</td>
                        <td width="10px;">:</td>
                        <td style="max-width:100%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="20%;" style="max-width:10%;">
                          <?php
                            $_message = "Hi, saya ingin share materi ini, materi yang menarik dan bagus untuk dipelajari. %0D%0A";
                            $_message .= "Link urlnya sebagai berikut ini : %0D%0A%0D%0A";
                            $_message .= current_url();
                            $_message .= "%0D%0A%0D%0ASelamat belajar.";
                            //$_message .= "Atau %3Ca%20href%3D%22".current_url()."%22%3Eklik link ini%3C%2Fa%3E untuk langsung ke materi.";
                            ?>
                            <a href="mailto:?subject=<?php echo $title;?>&amp;body=<?php echo $_message; ?>" title="Share By Email">
                              SHARE BY EMAIL
                            </a>
                        </td>
                        <td width="10px;"></td>
                        <td style="max-width:100%;"></td>
                    </tr>
                </table>
                <h5 style="font-weight:bold;">DESCRIPTION</h5>
                <p style="font-size:13px;"><?php echo $description; ?></p>
        </div>
        <div class="border-bott"></div><br>
        <div><h4>PLAYLIST</h4></div>
        <div style="height:auto;">
          

<?php
            if($list_content)
            {
               $carousel_default = '';


               $carouselimg1 = $carousel_default;$carouselimg2 = $carousel_default;$carouselimg3 = $carousel_default;
               $carouselimg4 = $carousel_default;$carouselimg5 = $carousel_default;$carouselimg6 = $carousel_default;$i = 1;
               foreach($list_content->product_list as $rwcontent)
               {
                  $sub_menu_id = $rwcontent->menu_id;
                  $sub_apps_id = $rwcontent->apps_id;
                  $sub_product_id = $rwcontent->product_id;
                  $sub_channel_category_id = $rwcontent->channel_category_id;
                  $sub_channel_id = $rwcontent->channel_id;
                  $sub_channel_type_id = $rwcontent->channel_type_id;
                  $sub_content_id = $rwcontent->content_id;
                  $sub_content_type = $rwcontent->content_type;
                  $sub_title = $rwcontent->title;
                  $short_title = strlen($sub_title) > 24 ? substr($sub_title, 0, 24). '...' : $sub_title;
                  $sub_thumbnail = $rwcontent->thumbnail.'&w=200px';
                  $sub_description = $rwcontent->description;
                  $sub_prod_year = $rwcontent->prod_year;
                  $sub_video_duration = $rwcontent->video_duration;
                  $sub_countlike = $rwcontent->countlike;
                  $sub_countviewer = $rwcontent->countviewer;
                  $sub_icon_size = $rwcontent->icon_size;
                  $sub_poster_size = $rwcontent->poster_size;
                  $sub_crew_category_list = $rwcontent->crew_category_list;
                  $sub_genre_list = $rwcontent->genre_list;

                  if($product_id != $sub_product_id)
                  {
                      $carouselItem = '
                      <div class="col-md-4 col-sm-12 hero-feature" style="padding-bottom:20px;">
                      <a href="'.base_url().'content/content_detail/'.$sub_content_id.'/'.$sub_menu_id.'">
                      <div class="thumbnail itemV" style="padding-top:5px;">
                        <img style="width:98%; height:140px;" src="'.$sub_thumbnail.'&w=160" style="cursor:pointer;" id="'.$sub_menu_id.'-'.$sub_product_id.'"
                                 data-id="'.$sub_product_id.'" data-menu-id="'.$sub_menu_id.'" src="'.$sub_thumbnail.'">
                          <div class="caption">
                              <h6 style="font-weight:300;">'.$short_title.'</h6>
                          </div>
                      </div>
                      </a>
                      </div>';
                      if($i == 1) $carouselimg1 = $carouselItem; if($i == 2) $carouselimg2 = $carouselItem;
                      if($i == 3) $carouselimg3 = $carouselItem; if($i == 4) $carouselimg4 = $carouselItem;
                      if($i == 5) $carouselimg5 = $carouselItem; if($i == 6) $carouselimg6 = $carouselItem;
                      $i++;
                  }
               }
            ?>

                   <div class="row" align="center">
                   <?php echo isset($carouselimg1) ? $carouselimg1 : $carousel_default; ?>
                   <?php echo isset($carouselimg2) ? $carouselimg2 : $carousel_default; ?>
                   <?php echo isset($carouselimg3) ? $carouselimg3 : $carousel_default; ?>
                   <?php echo isset($carouselimg4) ? $carouselimg4 : $carousel_default; ?>
                   <?php echo isset($carouselimg5) ? $carouselimg5 : $carousel_default; ?>
                   <?php echo isset($carouselimg6) ? $carouselimg6 : $carousel_default; ?>
                   </div>
                   
            <?php
               }
            ?>

          </div>

        </div>
        <!-- <div class="border-bott"></div><br>
        <div><h4>COMMENT</h4></div>
        <div style="height:auto;">
          <div><textarea class="texa" style="width:100%; max-width:100%; height:auto; max-height:200px; padding:10px; border:1px solid #ddd; border-radius:3px;" placeholder="Write your comment here.."></textarea></div><br>
          <div><button class="btn btn-warning">SEND</button></div>
        </div>

                <div><textarea id='commenttext' name="comment" class="media-input" rows="3"></textarea></div>
        <div class="row button-comment" style="margin-bottom: 40px;">
            <a id="sendcomment" data-id="<?php echo $content_id; ?>">SEND</a>
        </div>

        <div id="comment" data-page="1" data-id="<?php echo $content_id; ?>" class="comment-container"></div>

        <div class="clear row loadmore-contentplace">
            <div class="text-center">
                <div id="load_more_place" class="loadmore loadmore-bg">
                    <a data-page="1" data-id="<?php echo $content_id; ?>" id="comment_load_more" class="loadmore-text" href="#">LOAD MORE</a>
                </div>
            </div>
        </div>

        -->
        <script type="text/javascript">

  $(document).ready(function() {
      $('#like-<?php echo $content_id; ?>').click(function(e){
          e.preventDefault();
          var id = $(this).data('id');
          $.post(Settings.base_url+'content/post_like/', {'content_id':id}, function(data){
              if(data.rcode != 'ok'){
                  alert(data.msg);
              } else {
                  liketotal('#totallike-<?php echo $content_id; ?>');
              }
          });

      });

      $('#sendcomment').click(function(e){
          e.preventDefault();
          var product_id = $(this).data('id');
          var comment = $('#commenttext').val();
          $.post(Settings.base_url+'content/post_comment/', {'product_id':product_id, 'comment':comment}, function(data){
              if(data.rcode == 'ok'){
                  $('#comment').prepend(data.msg);
                  $('#commenttext').val('');
              }else{
                  alert(data.msg);
              }
          });

      });


  });

</script>

<?php
}
?>
      </div>
    </div>
</div>