<div class="container" style="margin-top:140px; margin-bottom:10px; background:#eee; padding:10px 10px; border-radius:10px;" align="center">
<h3>
    <a style="cursor: default;"><?php echo $menu_title;?></span></a>
</h3>
</div>
<div class="container" style="margin-bottom:10px; background:#eee; padding:20px 10px; border-radius:10px;">

        <div class="row text-center">
        <?php
        if($list_content)
        {
            $crew_category_list = array(); $genre_list = array();
            foreach($list_content->channel_list as $val)
            {
                $product_id = $val->product_id;
                $apps_id = $val->apps_id;
                $menu_id = $val->menu_id;
                $parent_menu_id = $val->parent_menu_id;
                $menu_type = $val->menu_type;
                $channel_id = $val->channel_id;
                $channel_name = $val->channel_name;
                $alias = $val->alias;
                $thumbnail = $val->thumbnail . '&w=300';
                $description = $val->description;
                $status = $val->status;
                $countviewer = $val->countviewer;
                $link = $val->link;
                $created = $val->created;
                $tab_type = $val->tab_type;
                $show_tab_menu = $val->show_tab_menu;
                $show_dd_menu = $val->show_dd_menu;
                $active = $val->active;
                $count_product = $val->count_product;
                switch($menu_type)
                {
                    case 'menu' :
                        $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title);
                        break;
                    case 'channel' :
                        $_link = base_url().'content/content_channel/'.$menu_id.'/'.$channel_id;
                        break;
                    default:
                        $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                        break;
                }

        ?>
            <div class="col-md-3 col-sm-6 hero-feature" style="padding-bottom:20px;">
                <a href="<?php echo $_link;?>">
                <div class="thumbnail itemV" style="padding-top:5px;">
                    <img style="width:98%;" src="<?php echo $thumbnail;?>" id="'<?php echo $menu_id;?>'-'<?php echo $product_id;?>'" data-id="'<?php echo $product_id;?>'" data-menu-id="'<?php echo $menu_id;?>'">
                    <div class="caption">
                        <h5 style="font-weight:300;"><?php echo $alias;?></h5>
                    </div>
                </div>
                </a>
            </div>
        <?php } ?>
        </div>
        <?php } ?>
</div>