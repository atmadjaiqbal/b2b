<div id="member" style="margin-top: 140px;">
   <div class="profile-box">
       <div class="profile-menu">

           <a href="<?php echo base_url() ?>member/memberupdate">
               <div class="personal-info-box">
                   <i class="fa fa-user"> </i>&nbsp;&nbsp;&nbsp;PERSONAL INFO
               </div>
           </a>

           <div class="history-box">
               <i class="fa fa-bars"> </i>&nbsp;&nbsp;&nbsp;ACTIVITY HISTORY
           </div>
       </div>

       <div class="profile-form">
           <div style="width: 440px;height: 20px;background-color: #00525D;padding: 5px;color: #ffffff;">
              <div style="font-size: 14px;width: 170px;float: left;">Title</div>
              <div style="font-size: 14px;width: 50px;float: left;">Res</div>
              <div style="font-size: 14px;width: 70px;float: left;">Start Date</div>
              <div style="font-size: 14px;width: 70px;float: left;">End Date</div>
              <div style="font-size: 14px;width: 60px;float: left;">Duration</div>
           </div>
           <?php
          if($result)
          {
              foreach($result as $key => $val)
              {
?>
           <div style="width: 440px;height: 30px;padding: 5px;color: #00525D;border-bottom:1px dashed #cecece;">
               <div style="font-size: 12px;width: 170px;float: left;"><p style="line-height: 12px;"><?php echo $val->content_title;?></p></div>
               <div style="font-size: 12px;width: 50px;float: left;"><?php echo $val->res_name;?></div>
               <div style="font-size: 12px;width: 70px;float: left;"><?php echo $val->start_date;?></div>
               <div style="font-size: 12px;width: 70px;float: left;"><?php echo $val->end_date;?></div>
               <div style="font-size: 12px;width: 60px;float: left;"><?php echo $val->duration;?></div>
           </div>
<?php
              }
          }
?>
       </div>


   </div>
</div>
