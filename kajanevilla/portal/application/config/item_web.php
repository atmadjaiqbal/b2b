<?php 
$config["nameLogo"] = "";
$config["title"] = "IBOLZ | KAJANE TV";
$config["warnaHeader"] = "#000000";
$config["colorMenu"] = "#c4c4c4";
$config["warnaCopyright"] = "#000000";
$config["warnaNavCarousel"] = "#c4c4c4";
$config["colorListbox"] = "#c4c4c4";
$config["warnaMenuMobile"] = "#eb420d";
$config["footerCopyright"] = "KAJANE TV";
$config["aboutUs"] = "about us";
$config["detailOffice"] = "our office";
$config["address"] = "";
$config["tlp"] = "";
$config["fax"] = "";
$config["contactCenter"] = "";
$config["email"] = "";
$config["website"] = "";
$config["namaFolder"] = "kajanevilla";
$config["namaAPK"] = "KajaneVillaTV-2.0.8-1447238301.apk";
$config["jmlSlider"] = "3";
$config["ibolz_app_id"] = "com.balepoint.ibolz.kajane";
$config["ibolz_customer_id"] = "-2";
$config["ibolz_device_id"] = "4";
$config["ibolz_lang"] = "en_US";
$config["ibolz_version"] = "2.0.8";
$config["ibolz_mobile"] = ""; 
?>