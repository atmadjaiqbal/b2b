<div id="banner-area">
    <img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt ="" style="width: 100%;"/>
    <div class="parallax-overlay"></div>
</div>

<section id="main-container">
    <div class="container">

        <div class="row">
            <div class="col-md-7 col-xs-12" style="padding: 30px;">
                <img src="<?php echo base_url(); ?>assets/images/logo_header.png" class="img-responsive" style="max-height: 300px;margin: 0 auto;">
            </div>
            <div class="col-md-5 col-xs-12" style="margin-bottom: 30px;">
                <div class="contact-info">
                    <h3>Contact Details</h3>
                    <?php if (!empty(config_item('detailOffice'))) echo "<p>".config_item('detailOffice')."</p>"; ?>
                    <br>
                    <p><i class="fa fa-home info"></i>  <?php echo config_item('address'); ?></p>
                    <p><i class="fa fa-phone info"></i>  <?php echo config_item('tlp'); ?> , Fax. <?php echo config_item('fax'); ?></p>
                    <?php if (!empty(config_item('contactCenter'))) echo "<p><i class='fa fa-phone info'></i>  Contact Center : ".config_item('contactCenter')."</p>"; ?>
                    <p><i class="fa fa-envelope-o info"></i>  <?php echo config_item('email'); ?></p>
                    <?php if (!empty(config_item('website'))) echo "<p><i class='fa fa-globe info'></i>  ".config_item('website')."</p>"; ?>
                </div>
            </div>
        </div>

    </div><!--/ container end -->

</section>