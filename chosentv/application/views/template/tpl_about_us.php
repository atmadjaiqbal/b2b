<div id="banner-area">
    <img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt ="" style="width: 100%;"/>
    <div class="parallax-overlay"></div>
</div>

<section id="main-container">
    <div class="container">

        <div class="row">
            <div class="col-md-12 col-xs-12" style="margin-bottom: 30px;">
                <div class="contact-info">
                  <h2>ABOUT US</h2>
                  <?php echo config_item('aboutUs'); ?>
                </div>
            </div>
        </div>

    </div><!--/ container end -->

</section>