<?php
if($row)
{
?>
<div class="section-template">
    <?php
    $_y = 1; $carouselimg1 = '';$carouselimg2 = '';$carouselimg3 = '';$carouselimg4 = '';$carouselimg5 = '';
    $carouselimg6 = '';$carouselimg7 = '';$carouselimg8 = '';$carouselimg9 = '';$carouselimg10 = '';

    foreach($row as $val)
    {
        $imgthumb = $val->thumbnail.'&w=500px';
        $contentID = $val->channel_id;
        $title = (strlen($val->channel_name) > 25) ? substr($val->channel_name,0,24).' ...' : $val->channel_name;
        $carouselitem = '<div class="content-item view-info-'.$content_category_id.'"
                              data-img="'.$val->thumbnail.'"
                              data-contentid="'.$val->channel_id.'"
                              >
                            <div class="slider-latest">
                                <img src="'.$imgthumb.'"><div class="ico-play-head"></div>
                            </div>
                            <div class="lates-overlay">
                                <p class="overlay-text">'.$title.'</p>
                            </div>
                        </div>
        ';

        if($_y == 1) {$carouselimg1 = $carouselitem; $poster = $val->thumbnail.'&w=500px';; }
        if($_y == 2) $carouselimg2 = $carouselitem; if($_y == 3) $carouselimg3 = $carouselitem;
        if($_y == 4) $carouselimg4 = $carouselitem; if($_y == 5) $carouselimg5 = $carouselitem;
        if($_y == 6) $carouselimg6 = $carouselitem; if($_y == 7) $carouselimg7 = $carouselitem;
        if($_y == 8) $carouselimg8 = $carouselitem; if($_y == 9) $carouselimg9 = $carouselitem;
        if($_y == 10) $carouselimg10 = $carouselitem; $_y++;
    }


    ?>

    <!-- poster Area --->
    <div class="section-content-poster">
        <div class="poster-<?php echo $content_category_id;?> bgfull" style="background: url(<?php echo $poster;?>) no-repeat scroll 0 0 rgba(0, 0, 0, 0);width:278px;height:420px;"></div>
    </div>

    <!-- Slider Area --->
    <div class="section-content-slider">
        <!-- Content Inormation Section ---->
        <div class="row" id="content-info"></div>
        <!-- Content Slider Section --->
        <div class="row row-item">

            <div class="panelslider" class="carousel slide">
                <a id="movie_caro_left" class="left carousel-control" href="#movieCarousel-<?php echo $content_category_id;?>" data-slide="next"></a>
                <a id="movie_caro_right" class="right carousel-control" href="#movieCarousel-<?php echo $content_category_id;?>" data-slide="next"></a>
                <div id="movieCarousel-<?php echo $content_category_id;?>">
                  <div class="carousel-inner">
                     <div class="item active">
                         <div class="content-item"><?php echo isset($carouselimg1) ? $carouselimg1 : ''; ?></div>
                         <div class="content-item"><?php echo isset($carouselimg2) ? $carouselimg2 : ''; ?></div>
                         <div class="content-item"><?php echo isset($carouselimg3) ? $carouselimg3 : ''; ?></div>
                         <div class="content-item"><?php echo isset($carouselimg4) ? $carouselimg4 : ''; ?></div>
                         <div class="content-item"><?php echo isset($carouselimg5) ? $carouselimg5 : ''; ?></div>
                     </div>
                     <div class="item">
                         <div class="content-item"><?php echo isset($carouselimg6) ? $carouselimg6 : ''; ?></div>
                         <div class="content-item"><?php echo isset($carouselimg7) ? $carouselimg7 : ''; ?></div>
                         <div class="content-item"><?php echo isset($carouselimg8) ? $carouselimg8 : ''; ?></div>
                         <div class="content-item"><?php echo isset($carouselimg9) ? $carouselimg9 : ''; ?></div>
                         <div class="content-item"><?php echo isset($carouselimg10) ? $carouselimg10 : ''; ?></div>
                     </div>
                  </div>
                </div>
            </div>

        </div>


    </div>



</div>
<?php
}
?>


<script>

    $(document).ready(function() {

        $('.view-info-<?php echo $content_category_id;?>').click(function(e){
            e.preventDefault();
            var img = $(this).data('img');
            $('.poster-<?php echo $content_category_id;?>').removeAttr('style').attr('style', 'background: url('+img+') no-repeat scroll 0 0 rgba(0, 0, 0, 0);width:278px;height:420px;');





/*
            $('#loadmore-loader').css('visibility', 'visible');
            var page = $(holder).data('page');
            var setfilter = $(holder).data('filter');

            $.get(Settings.base_url+'home/content_list/'+page, function(data){
                pageg = $(holder).data('page') + 1;
                $(holder).data('page', pageg);
                $(holder).append(data);
                $('#loadmore-loader').remove();
                $('#content_load_more').text('Load More');

            });
*/



        });


    });

</script>