<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider1, slider2, slider3 .jpg

*/

$config['nameLogo'] = "";
$config['title'] = "IBOLZ | ANTON SURATTO TV";
$config['warnaHeader'] = "#5872CF";
$config['colorMenu'] = "#619DFC";
$config['warnaCopyright'] = "#5872CF";
$config['warnaNavCarousel'] = "#619DFC";
$config['colorListbox'] = "green";
$config['warnaMenuMobile'] = "#619DFC";
$config['footerCopyright'] = "ANTON SURATTO TV";
$config['jmlSlider'] = 2;

/* --- About Us --- */

$config['aboutUs'] = "";

/* --- Contact Us --- */

$config['detailOffice'] = "";
$config['address'] = "";
$config['tlp'] = "";
$config['fax'] = "";
$config['contactCenter'] = "";
$config['email'] = "";
$config['website'] = "";

/* --- Download --- */

$config['namaFolder'] = "anton";
$config['namaAPK'] = "AntonSurattoTV-2.0.5-1444124710";

?>

