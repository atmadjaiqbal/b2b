var host = '';
$(function(){

    $('#user-menu').collapse("hide");
    $('.user-collapse').on('click', function(e){
        e.preventDefault();
        var dt_target = $(this).data('target');
        var cl = $(dt_target).attr('class');
        if(cl == 'collapse'){
            $(this).attr('class', 'icon-chevron-up pull-right user-collapse collapsed');
        }else{
            $(this).attr('class', 'icon-chevron-down pull-right user-collapse collapsed');
        }

    });

    $('.user-profile').on('click', function(e){
        e.preventDefault();
        $.ajax({
            url: Settings.base_url+'member/get_profile',
            type: 'POST',
            dataType: "json",
            success: function(data)
            {
                if(data.rcode == 'ok')
                {
                    $('#usermobile_id').val(data.mobile_id);
                    $('#usercustomer_id').val(data.customer_id);
                    $('#useremail').val(data.email);
                    $('#useruser_id').val(data.user_id);
                    $('#userfirstname').val(data.first_name);
                    $('#userlastname').val(data.last_name);
                    $('#userdisplay').val(data.display_name);
                    $('#userstatus').val(data.status);
                    $('#userverify'),val(data.verify_status);
                    $('#usermobile_no').val(data.mobile_no);
                    $('#usercredit_balance').val(data.credit_balance);
                    $('#userphoto').val(data.photo);
                    $('#userthumbnail').val(data.thumbnail);
                } else {

                }

            }
        });

    });

    $('.update-account').on('click', function(e){
        var firstname = $('#userfirstname').val();
        var lastname = $('#userlastname').val();
        var email = $('#useremail').val();
        var mobileno = $('#usermobile_no').val();

        e.preventDefault();
        $.ajax({
            url: Settings.base_url+'member/update_profile',
            type: 'POST',
            data: {'firstname': firstname, 'lastname': lastname, 'email': email, 'mobileno': mobileno},
            dataType: "json",
            success: function(data)
            {
                if(data.rcode == 'ok')
                {
                    $('.notify').css('display', 'block');
                    $('.message').text(data.msg);
                } else {
                    $('.notify').css('display', 'block');
                    $('.message').text(data.msg);
                }
            }
        })
    });

    $('.create-account').click(function(e){
        var firstname = $('#firstname').val();
        var lastname = $('#lastname').val();
        var isemail = $('#isemail').val();
        var password = $('#password').val();
        var confirmpass = $('#confirmpass').val();

        $.ajax({
            url: Settings.base_url+'member/do_register',
            type: 'POST',
            data: {'firstname': firstname, 'lastname': lastname, 'email': isemail, 'password': password, 'confirmpass': confirmpass},
            dataType: "json",
            success: function(data)
            {
                if(data.rcode == 'bad'){
                    $('.notify').css('display', 'block');
                    $('.message').text(data.msg);
                } else {
                    location.reload();
                }
                console.log(data.rcode);
            }
        });
    });

    $('.forgotPassword').click(function(e){
        var isemail = $('#isemail').val();
        $.ajax({
            url: Settings.base_url+'member/do_forgot_password',
            type: 'POST',
            data: {'email': isemail},
            dataType: "json",
            success: function(data)
            {
                $('.notify').css('display', 'block');
                $('.message').text(data.msg);
                console.log(data.rcode);
            }
        });

    });

    content_list('#content_container');
    $('#content_load_more').click(function(e){
        e.preventDefault();
        var setfilter = $("#content_container").data('filter');
        content_list('#content_container');
    });

    channel_list('#channel-places');
    $('#channel_load_more').click(function(e){
        e.preventDefault();
        channel_list('#channel-places');
    });

    channel_smalllist('#channel-smplaces');
    $('#channelsm_load_more').click(function(e){
        e.preventDefault();
        channel_smalllist('#channel-smplaces');
    });

    $('.chkGenre').change(function() {
        var sort_by = $( "#sortGenre" ).val();
        if($(this).is(":checked")) {
            content_filter(sort_by, $(this).val(), 'add');
        } else {
            content_filter(sort_by, $(this).val(), 'remove');

            var allCheckOff = $('input:checkbox:checked').length;
            if(allCheckOff == 0) {location.reload();}

            $("#frmsort").find(".chkGenre").each(function(){
                if ($(this).prop('checked')==false){
                    console.log("all close");
                }
            });

        }
    });

    $('#channel-filter').change(function() {
        var filter = $(this).val();
        channel_filter(filter)
    });


});


function content_list(holder)
{
    var the_loader = '<div id="loadmore-loader" class="loader"></div>';
    //$(holder).append(the_loader);
    $('#content_load_more').text('');
    $('#load_more_place').append(the_loader);

    $('#loadmore-loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var setfilter = $(holder).data('filter');

    $.get(Settings.base_url+'home/content_list/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);
        $('#loadmore-loader').remove();
        $('#content_load_more').text('Load More');

        setTimeout(function(){
                var container = document.querySelector('#content_container');
                var msnry = new Masonry( container, {
                    columnWidth: 10,
                    gutterWidth: 10,
                    position: 'relative',
                    itemSelector: '.contentitem'
                });
            },
            350);
    });

}

function channel_list(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'home/channel_list/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });
}

function channel_smalllist(holder)
{
    var the_loader = '<div id="loadmore-loader" class="loader"></div>';
    //$(holder).append(the_loader);
    //$('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    $.get(Settings.base_url+'home/channel_smalllist/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);
        //$('#wall_loader').remove();
    });
}

function content_filter(sortby, genre, type)
{
    var holder = '#content_container';
    $(holder).empty();
    //var the_loader = '<div id="loadmore-loader" class="loader"></div>';
//    $('#content_load_more').text('');
    //$('#load_more_place').append(the_loader);
//    $('#loadmore-loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    $.get(Settings.base_url+'search/index/'+sortby+'/'+genre+'/'+type, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);
        $('.loadmore-contentplace').css('display', 'none');

        var container = document.querySelector('#content_container');
        var msnry = new Masonry( container, {
            columnWidth: 5,
            gutterWidth: 25,
            itemSelector: '.contentitem'
        });

    });
}

function channel_filter(category)
{
    var holder = '#channel-places';
    var page = $(holder).data('page');
    $.get(Settings.base_url+'search/channel_filter/'+category, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);
    });

}

function liketotal(holder)
{
    var id = $(holder).data('id');
    var uri = Settings.base_url+'timeline/total_like/'+ id;
    $.get(uri, function(data){
        $(holder).html('');
        $(holder).append(data);
    });
}

function commenttotal(holder)
{
    var id = $(holder).data('id');
    var uri = Settings.base_url+'timeline/total_comment/'+ id;
    $.get(uri, function(data){
        $(holder).append(data);
    });
}

function comment_list(holder)
{
    var id = $(holder).data('id');
    var page = $(holder).data('page');
    var uri = Settings.base_url+'timeline/content_comment/'+ id + '/'+page;
    $.get(uri, function(data){
        $(holder).append(data);
        $(holder).data('page', parseInt(page) + 1);
    });

}

function sendcomment(content_id)
{
    var comment = message;
    console.log(comment);
    $.post(Settings.base_url+'timeline/post_comment/', {'product_id':content_id, 'comment':message}, function(data){
        if(data.rcode == 'ok'){
            $('#comment').prepend(data.msg);
        }else{
            alert(data.msg);
        }
    });
}

