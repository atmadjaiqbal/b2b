<?php 
$config["nameLogo"] = "JakCloth TV";
$config["title"] = "IBOLZ | JakCloth TV";
$config["warnaHeader"] = "#EE1D25";
$config["colorMenu"] = "#000000";
$config["warnaCopyright"] = "#EE1D25";
$config["warnaNavCarousel"] = "#eb420d";
$config["colorListbox"] = "#eb420d";
$config["warnaMenuMobile"] = "#eb420d";
$config["footerCopyright"] = "JakCloth TV";
$config["aboutUs"] = "ABOUT US";
$config["detailOffice"] = "OUR OFFICE";
$config["address"] = "";
$config["tlp"] = "";
$config["fax"] = "";
$config["contactCenter"] = "";
$config["email"] = "";
$config["website"] = "";
$config["namaFolder"] = "jakcloth";
$config["namaAPK"] = "JakClothTV-2.1.2-1448537550.apk";
$config["jmlSlider"] = "2";
$config["ibolz_app_id"] = "com.balepoint.ibolz.jakclothtv";
$config["ibolz_customer_id"] = "-2";
$config["ibolz_device_id"] = "4";
$config["ibolz_lang"] = "en_US";
$config["ibolz_version"] = "2.1.2";
$config["ibolz_mobile"] = ""; 


$config["warnaMenuTopNav"] = "#ffffff";
?>