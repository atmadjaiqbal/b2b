<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Application {

    public function __construct()
    {
        parent::__construct();
        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->library('home_lib');
        $this->load->library('content/content_lib');
        $this->load->library('breadcrumbs');
    }

    public function member_session() {
        if($this->session->userdata('email')) {
            return true;
        } else {
            return false;
        }
    }   

    public function index()
    {
        //if(!$this->member_session()) { redirect('member/login');}
        $data = $this->data;
        $data['scripts'] = array('ibolz.onl.js');
        $breadcrumb = array();
        $data['breadcrumb'] = $breadcrumb;
        $data['headermenu'] = $this->home_lib->header_menu();
        $data['listmenu'] = $this->getheadermenu();
        $data['bannerslider'] = $this->banner_slider();
        $data['contentslider'] = $this->content_slider();

        $html['html']['content']  = $this->load->view('home/home_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }


    function banner_slider()
    {
        $data['banner'] = $this->home_lib->headslider(5, 0);
        // return $this->load->view('home/home_slider', $data, true);
        return $this->load->view('home/home_slider', $data, true);
    }

    function content_slider()
    {
        $data['scripts']     = ''; //array('ibolz.chn.js');
        $listcontent = array();$i=0;
        $menulist = $this->home_lib->header_menu();

        foreach($menulist->menu_list as $val)
        {
            $product_id = $val->product_id;
            $apps_id = $val->apps_id;
            $menu_id = $val->menu_id;
            $parent_menu_id = $val->parent_menu_id;
            $menu_type = $val->menu_type;
            $title = $val->title;
            $thumbnail = $val->thumbnail;
            $link = $val->link;
            $tab_type = $val->tab_type;
            $show_tab_menu = $val->show_tab_menu;
            $show_dd_menu = $val->show_dd_menu;
            $active = $val->active;
            $count_product = $val->count_product;
            $icon_size = $val->icon_size;
            $poster_size = $val->poster_size;
            $listcontent[$i]['created'] = $val->created;
            $listcontent[$i]['title'] = $title;
            $listcontent[$i]['thumbnail'] = $thumbnail;
            $listcontent[$i]['menu_type'] = $menu_type;
            $listcontent[$i]['menu_id'] = $menu_id;
            $listcontent[$i]['parent_menu_id'] = $parent_menu_id;
            $listcontent[$i]['product_id'] = $product_id;
            //$listcontent[$i]['list_content'] = $this->content_lib->product_menu_list($menu_id);
            //$data['channel_detail'] = $this->content_lib->channel_list();
            $listcontent[$i]['list_content'] = $val->sub_menu_list;
            $i++;
        }

        $data['listcontent'] = $listcontent;
        return $this->load->view('home/home_content', $data, true);
    }

    public function home_faq()
    {
        //if(!$this->member_session()) { redirect('member/login');}
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();
        $faq = $this->home_lib->faq(); $faqresult = array(); $i = 0;
        $this->breadcrumbs->add('Frequently Asked Questions','#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        if($faq)
        {
            foreach($faq->faq_categories as $key => $row)
            {
                $faq_categories_id = $row->faq_categories_id;
                $faqresult[$i]['faq_title'] = $row->faq_title;
                $faqresult[$i]['faq_description'] = $row->faq_description;
                $faqresult[$i]['faq_detail'] = $this->home_lib->faq_detail($faq_categories_id);
                $i++;
            }
        }
        $data['faqresult'] = $faqresult;
        $data['header_custom'] = "true";
        $html['html']['content']  = $this->load->view('template/tpl_faq', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function home_aboutus()
    {
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();
        $this->breadcrumbs->add('About Us','#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $html['html']['content']  = $this->load->view('template/tpl_about_us', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function home_contactus()
    {
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();
        $this->breadcrumbs->add('Contact Us','#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $html['html']['content']  = $this->load->view('template/tpl_contact_us', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function home_usermanual()
    {
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();
        $this->breadcrumbs->add('Help','#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $html['html']['content']  = $this->load->view('template/tpl_usermanual', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    function getheadermenu()
    {
        $data['scripts']     = ''; //array('ibolz.chn.js');
        $listcontent = array();$i=0;
        $menulist = $this->home_lib->header_menu();
        if($menulist->menu_list)
        {
            foreach($menulist->menu_list as $row)
            {
                $m_product_id = $row->product_id;
                $m_apps_id = $row->apps_id;
                $m_menu_id = $row->menu_id;
                $m_parent_menu_id = $row->parent_menu_id;
                $m_menu_type = $row->menu_type;
                $m_title = $row->title;
                $m_thumbnail = $row->thumbnail;
                $m_link = $row->link;
                $m_active = $row->active;
                $m_count_product = $row->count_product;
                $m_icon_size = $row->icon_size;
                $m_poster_size = $row->poster_size;

                if($row->sub_menu_list)
                {
                	//if(strtolower($row->title) != 'pengaduan')
                	//{
                    foreach($row->sub_menu_list as $val)
                    {
                        $c_product_id = $val->product_id;
                        $c_apps_id = $val->apps_id;
                        $c_menu_id = $val->menu_id;
                        $c_parent_menu_id = $val->parent_menu_id;
                        $c_title = $val->title;
                        $c_thumbnail = $val->thumbnail;
                        $c_active = $val->active;
                        $c_category_name = $row->title .' - ' . $val->title;
                        $c_sub_menu_list = $val->sub_menu_list;
                        if(strtolower($c_category_name) != 'pengaduan - lapor' && strtolower($c_category_name) != 'pengaduan - statistik pengaduan' )
                        {
                            $arrMenu[] = array("menu_id" => $c_menu_id,
                                "category_name" => $c_category_name,
                                "submenu" => $c_sub_menu_list
                            );
                        }
                    }
                  //}
                }
            }
        }

        return $arrMenu;
    }

}