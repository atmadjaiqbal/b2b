<?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>

<section id="main-container" class="portfolio-static">
	<div class="container">
		<div class="row">
			<?php
		    if($listmenu) {
		        foreach($listmenu as $row) {
		            $c_menu_id = $row['menu_id'];
		            $c_category_name = $row['category_name'];
                    if(preg_match('/DISTRO/i',$c_category_name)){
                        continue;
                    }elseif(preg_match('/EVENT/i',$c_category_name)){
                        continue;
                    }
            ?>
            <hr style="margin-top: 0px !important;"/>
			<div class="col-md-12 heading">
				<h3 class="classic"><i class="fa fa-list-alt"></i><div class="title"></div><?php echo $c_category_name;?></h3>
			</div>
			<?php
            if($row['submenu']) {
                foreach($row['submenu'] as $rwContent) {

                    $_c_product_id = $rwContent->product_id;
                    $_c_apps_id = $rwContent->apps_id;
                    $_c_menu_id = $rwContent->menu_id;
                    $_c_parent_menu_id = $rwContent->parent_menu_id;
                    $_c_menu_type = $rwContent->menu_type;
                    $_c_title = $rwContent->title;
                    $_c_thumbnail = $rwContent->thumbnail;
                    $_c_link = $rwContent->link;
                    $_c_icon_size = $rwContent->icon_size;
                    $_c_poster_size = $rwContent->poster_size;
                    $short_title = strlen($_c_title) > 14 ? substr($_c_title, 0, 14).'...' : $_c_title;


                    switch(strtolower($_c_menu_type))
                    {
                        case 'menu' :
                            $_link = base_url('content/content_categories_list/'.$_c_menu_id.'/'.urlencode($_c_title));
                            break;
                        case 'channel' :
                            $_link = base_url('content/content_channel/'.$_c_menu_id.'/'.$_c_product_id);
                            break;
                        default:
                            $_link = base_url('content/content_categories/'.$_c_menu_id.'/'.urlencode($_c_title));
                            break;
                    }

                    $info = '';
                    if(!preg_match('/LIVE CHANNEL/i',$c_category_name)){
                        $info = '<div class="portfolio-static-desc">';
                        $info .= '<h3>'.$short_title.'</h3>';
                        $info .= '<p>'.$c_category_name.'</p>';
                        $info .= '</div>';                  
                    };
            ?>

				<div class="col-sm-2 col-xs-6 portfolio-static-item">
					<a href="<?php echo $_link;?>">
						<div class="grid">
							<figure class="effect-oscar">
								<img src="<?php echo $_c_thumbnail;?>&w=300" alt="">		
							</figure>
							<?php echo $info;?>					
						</div><!--/ grid end -->
					</a>
				</div><!--/ item 1 end -->

            <?php
                }
            }
            ?>
            <div class="clearfix"></div>
            <?php
        }
    }
		?>
				
		</div><!-- Content row end -->
	</div><!-- Container end -->
</section>