<div id="banner-area">
    <img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt ="" style="width: 100%;"/>
    <div class="parallax-overlay"></div>
</div>

<section id="main-container" class="portfolio-static" style="margin-top: -65px;background-color: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h3 class="classic"><i class="fa fa-list-alt"></i><div class="title"></div><?php echo $menu_title;?></h3>
            </div>

            <?php
            if($list_content) {
                $crew_category_list = array(); $genre_list = array();

                foreach($list_content->menu_list as $val) {
                    $product_id = $val->product_id;
                    $apps_id = $val->apps_id;
                    $menu_id = $val->menu_id;
                    $parent_menu_id = $val->parent_menu_id;
                    $menu_type = $val->menu_type;
                    $title = strlen($val->title) > 14 ? substr($val->title, 0, 14).'...' : $val->title;

                    $thumbnail = $val->thumbnail . '&w=300';
                    $count_product = $val->count_product;

                    switch($menu_type)
                    {
                        case 'menu' :
                            $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title);
                            break;
                        case 'channel' :
                            $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                            break;
                        default:
                            $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                            break;
                    }

                    $info = '';

                    if(!preg_match('/LIVE CHANNEL/i',$menu_title)){
                        $info = '<div class="portfolio-static-desc">';
                        $info .= '<h3>'.$title.'</h3>';
                        $info .= '<span>'.$menu_title.'</span>';
                        $info .= '</div>';                  
                    };

                    if($title != 'Nomor Penting') { ?>

                        <div class="col-sm-2 col-xs-6 portfolio-static-item">
                            <a href="<?php echo $_link;?>">
                                <div class="grid">
                                    <figure class="effect-oscar">
                                        <img src="<?php echo $thumbnail;?>" alt="">        
                                    </figure>
                                    <?php echo $info; ?>                  
                                </div><!--/ grid end -->
                            </a>
                        </div><!--/ item 1 end -->
                        <?php
                    }
                }
            }
            ?>
        </div><!-- Content row end -->
    </div><!-- Container end -->
</section>