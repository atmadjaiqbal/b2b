var host = '';
$(function(){

    vod_list('#content_vod_list');
    $('#channel_load_more').click(function(e){
        e.preventDefault();
        vod_list('#content_vod_list');
    });

});



function vod_list(holder)
{
    var the_loader = '<div id="loadmore-loader" class="loader"></div>';
    //$(holder).append(the_loader);
    $('#content_load_more').text('');
    $('#load_more_place').append(the_loader);

    $('#loadmore-loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var channel_id = $(holder).data('channelid');

    $.get(Settings.base_url+'content/vodlist/'+channel_id+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#loadmore-loader').remove();
        $('#content_load_more').text('Load More');

    });

}
