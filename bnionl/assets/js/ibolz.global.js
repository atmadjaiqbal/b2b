var IbolzExtranet = IbolzExtranet || {};

IbolzExtranet.Global = (function($){
	var _today;
	var self = {
		init: function(){
			self.setNavigation();
			self.liveEdit();
			
			$('.popup .btn-cancel').live('click',function(){
				$('.popup').qtip('hide');
				return false;
			});
			$('.popup .btn-confirm, .popup .btn-delete, .popup .btn-save').live('click',function(){
				$('.popup').qtip('hide');
				var trigger = $(this).data('trigger');
				$('body').trigger(trigger);
				return false;
			});

			// global submit
			$('.btn-submit').click(function(){
				$(this).closest('form').submit();
				return false;
			});

			$('.timestamp-convert').each(function(){
				var timestamp = $(this).data('timestamp');
				var dayNames = $.datepicker.regional[''].dayNames;
				var monthNames = $.datepicker.regional[''].monthNames;
				var nd = self.convertLocalTime(timestamp);

				var D = (nd.getDate() < 10) ? '0'+nd.getDate() : nd.getDate();
				var H = (nd.getHours() < 10) ? '0'+nd.getHours() : nd.getHours();
				var M = (nd.getMinutes() < 10) ? '0'+nd.getMinutes() : nd.getMinutes();
				var S = (nd.getSeconds() < 10) ? '0'+nd.getSeconds() : nd.getSeconds();


				var displayDate = dayNames[nd.getDay()]+', '+D+' '+monthNames[nd.getMonth()]+' '+ nd.getFullYear() +' - '+H+':'+M+':'+S;
				$(this)
					.empty()
					.html(displayDate);
			});
		},
		convertLocalTime: function(phpTimestamp){
			var sourceDate = new Date(phpTimestamp * 1000);
			var localDate = new Date();
			var offset = localDate.getTimezoneOffset()/60*-1;

			var utc = sourceDate.getTime() + (sourceDate.getTimezoneOffset() * 60000);
			var nd = new Date(utc + (3600000*offset));
			return nd;
		},
		url: function(a,b,c){
			var url = Settings.baseurl;
			if(Settings.rewrite){
				if(a) url += escape(a);
				if(b) url += "/" + escape(b);
				if(c) url += "?" + c;
			}else{
				if(a) url += '?c=' + escape(a);
				if(b) url += "&m=" + escape(b);
				if(c) url += "&" + c;
			}
			return url;
		},
		dateFromTo: function(from, to) {

			var dateFrom = from.datepicker('getDate').getDate();
			var monthFrom = from.datepicker('option', 'monthNames')[from.datepicker('getDate').getMonth()];
			var yearFrom = from.datepicker('getDate').getFullYear();

			var rangeFrom = dateFrom+' '+monthFrom+' '+yearFrom;

			var dateTo = to.datepicker('getDate').getDate();
			var monthTo = to.datepicker('option', 'monthNames')[to.datepicker('getDate').getMonth()];
			var yearTo = to.datepicker('getDate').getFullYear();

			var rangeTo = dateTo+' '+monthTo+' '+yearTo;

			var dateRange = rangeFrom+' - '+rangeTo;

			return dateRange;
		},
		placeholder: function(){
			$('input[data-placeholder]').each(function(){
				//console.info(this);
			});
		},
		showPopup: function(title, content, width, button, blur, classes){
			button  = (button) ? button : false;
			blur    = (blur) ? blur : true;
			classes = (classes) ? classes : 'popup';
			$('<div />').qtip({
				content: {
					text: content,
					title: {
						text: title,
						button: button
					}
				},
				position: {
					my: 'center',
					at: 'center',
					target: $(window)
				},
				show: {
					ready: true,
					modal: {
						on: true,
						blur: blur
					}
				},
				hide: false,
				style: {
					classes: classes,
					width: width
				},
				events: {
					// Hide the tooltip when any buttons in the dialogue are clicked
					render: function(event, api) {
						$('button', api.elements.content).click(api.hide);
					},
					// Destroy the tooltip once it's hidden as we no longer need it!
					hide: function(event, api) { api.destroy(); }
				}
			});
		},
		setNavigation: function(){
			$('#main-nav > ul > li')
				.hover(
					function(){ $(this).addClass('hover');},
					function(){ $(this).removeClass('hover');}
				)
				.each(function(){
					$(this).css('width',$(this).width());
				});
		},
		liveEdit: function(){
			var wrapper = $('.live-edit-wrapper');

			wrapper.find('.live-edit')
				.click(function(){
					if($(this).hasClass('edit')) return false;
					$(this).addClass('edit');
					$(this).find('input, textarea, select').focus();
					self.updateLiveEditAction();
				});

			wrapper.find('.live-edit input')
				.keydown(function(e){
					if(e.keyCode == 27){
						var parent = $(this).parent();
						var ori = parent.find('.ori').data('ori');
						$(this).val(ori);
						parent.removeClass('edit');
						self.updateLiveEditAction();
					}
				});
			wrapper.find('.live-edit textarea')
				.keydown(function(e){
					if(e.keyCode == 27){
						var parent = $(this).parent();
						var ori = parent.find('.ori').data('ori');
						$(this).val(ori);
						parent.removeClass('edit');
						self.updateLiveEditAction();
					}
				});
			wrapper.find('input:checkbox').click(function(){
				var ori = $(this).data('ori');
				if( (ori == 'y' && this.checked) || (ori == 'n' && !this.checked ) ){
					$(this).parent().removeClass('edit');
				}else{
					$(this).parent().addClass('edit');
				}
				self.updateLiveEditAction();
			});

			self.liveEditReset(wrapper);
		},
		liveEditReset: function(wrapper){
			wrapper.find('.live-edit input, .live-edit select, .live-edit textarea')
				.each(function(){
					var parent = $(this).parent();
					var ori = parent.find('.ori').data('ori');
					$(this).val(ori);
					parent.removeClass('edit');
				});

			wrapper.find('input:checkbox[data-ori]')
				.each(function(){
					$(this).parent().removeClass('edit');
					this.checked = ($(this).data('ori') == 'y') ? true : false;
				});
			
			self.updateLiveEditAction();
		},
		updateLiveEditAction: function(){
			
			$('form').each(function(){
				var liveEdit = $(this).find('.live-edit-action');
				if($(this).find('.live-edit-wrapper .edit').length){
					if(liveEdit.css('display') == 'none')
						$(liveEdit).slideDown();
				}else if( liveEdit.css('display') != 'none' ) {
					$(liveEdit).slideUp();
				}
			});
			
		},
		number_format: function(number, decimals, dec_point, thousands_sep){
			
			if(isNaN(number) || number === '') number = 0;

			decimals      = (decimals !== undefined) ? decimals : 2;
			dec_point     = (dec_point !== undefined) ? dec_point : '.';
			thousands_sep = (thousands_sep !== undefined) ? thousands_sep : ' ';
			var negative  = (number < 0);

			if (negative) number *= -1;

			var left = parseInt(number, 10),
			right    = Math.round(parseFloat(number.toString().replace(/^d+./, '0.')) * Math.pow(10, decimals));

			left  = left.toString().split('').reverse().join('')
				.match(/d{3}|d{1,2}/g)
				.join(thousands_sep)
				.split('').reverse().join('');

			right = (right / Math.pow(10,decimals)).toString().replace(/^d+./, '').toString();

			if (right.length < decimals){
				for (var iRight=right.length; iRight < decimals; iRight++) right += '0';
			}

			return (negative?'-':'')+left+dec_point+right;
		}
	};
	return self;
})(jQuery);

(function($) {
    IbolzExtranet.Global.init();
})(jQuery);