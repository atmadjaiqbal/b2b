<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0014)about:internet -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"> 
    <head>
        <title></title>
        <meta name="google" value="notranslate" />         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css" media="screen">
            html, body  { height:100%; }
            body { margin:0; padding:0; overflow:auto; text-align:center; 
                   background-color: #000000; }   
            object:focus { outline:none; }
            #flashContent { display:none; }
        </style>
        
        <link rel="stylesheet" type="text/css" href="history/history.css" />
        <script type="text/javascript" src="history/history.js"></script>

        <script type="text/javascript" src="swfobject.js"></script>
        <script type="text/javascript">
            // For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. 
            var swfVersionStr = "12.0.0";
            // To use express install, set to playerProductInstall.swf, otherwise the empty string. 
            var xiSwfUrlStr = "playerProductInstall.swf";
            var flashvars = {apps_id:"<?php echo $_REQUEST['apps_id'] ?>",version:"1.0.6",type:"<?php echo $_REQUEST['type'] ?>",channel_id:"<?php echo $_REQUEST['channel_id'] ?>",id:"<?php echo $_REQUEST['id'] ?>",vid:"<?php echo $_REQUEST['vid'] ?>",customer_id:"<?php echo $_REQUEST['customer_id'] ?>"};
            var params = {};
            params.wmode ="opaque";
            params.quality = "high";
            params.bgcolor = "#000000";
            params.allowscriptaccess = "*";
            params.allowfullscreen = "true";
            params.allowFullScreenInteractive = "true";
            var attributes = {};
            attributes.id = "iBolz";
            attributes.name = "iBolz";
            attributes.align = "middle";
            swfobject.embedSWF(
                "iBolz.swf", "flashContent",
                "<?php echo $_REQUEST['w'] ?>", "<?php echo $_REQUEST['h'] ?>",
                swfVersionStr, xiSwfUrlStr, 
                flashvars, params, attributes);
            // JavaScript enabled so display the flashContent div in case it is not replaced with a swf object.
            swfobject.createCSS("#flashContent", "display:block;text-align:left;");
        </script>
    </head>
    <body>
        <!-- SWFObject's dynamic embed method replaces this alternative HTML content with Flash content when enough 
             JavaScript and Flash plug-in support is available. The div is initially hidden so that it doesn't show
             when JavaScript is disabled.
        -->
        <div id="flashContent">
            <p>
                To view this page ensure that Adobe Flash Player version 
                12.0.0 or greater is installed. 
            </p>
            <script type="text/javascript"> 
                var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
                document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                                + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
            </script> 
        </div>
        
        <noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="<?php echo $_REQUEST['w'] ?>" height="<?php echo $_REQUEST['h'] ?>" id="iBolz">
                <param name="movie" value="iBolz.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#000000" />
                <param name="allowScriptAccess" value="*" />
                <param name="allowFullScreen" value="true" />
                <param name="allowFullScreenInteractive" value="true" />
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="iBolz.swf" width="<?php echo $_REQUEST['w'] ?>" height="<?php echo $_REQUEST['h'] ?>">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#000000" />
                    <param name="allowScriptAccess" value="*" />
                    <param name="allowFullScreen" value="true" />
                    <param name="allowFullScreenInteractive" value="true" />
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                    <p> 
                        Either scripts and active content are not permitted to run or Adobe Flash Player version
                        12.0.0 or greater is not installed.
                    </p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        </noscript>     
   </body>
</html>
