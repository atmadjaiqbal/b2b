<div class="clear"></div>
<footer>
    <div class="top-pad"></div>
    <div class="other-link">
        <a href="#">About Us</a>&nbsp;|&nbsp;
        <a href="#">FAQ</a>&nbsp;|&nbsp;
        <a href="#">Help</a>&nbsp;|&nbsp;
        <a href="#">Contact Us</a>
    </div>
    <div class="belongs">
        Copyright © 2014 BNI ONL.All right reserved.
    </div>

</footer>

<script>

</script>

</html>


<script type="text/javascript">

    var vod = {
        init: function() {},
        content_view: function(container_id, content_id, menu_id, callback)
        {
            $.get(Settings.base_url+'content/content_info/'+content_id+'/'+menu_id, function(result) {
                $(container_id).html(result);
                $(container_id).removeClass('bg-loader');
                if(callback) callback();
            });
        }
    }

    $(document).ready(function() {
        $(".link-item").tooltip({placement : 'bottom'});


        $('.owl-carousel').each(function(idx, slider){
            var item_count = $(slider).data('items');
            if(item_count == undefined) item_count = 3;
            var newcontent_slider = $(slider).owlCarousel({
                navigation: true,
                pagination: false,
                items: item_count,
                itemsDesktop: [1280,3],
                itemsDesktopMedium: [1199,3],
                itemsDesktopSmall: [979,3],
                itemsTablet: [768, 3],
                itemsMobile: [479, 2],
                navigationText: [
                "<i class='fa fa-2x fa-chevron-left'></i>",
                "<i class='fa fa-2x fa-chevron-right'></i>"
                ]
            });
        })



    });
</script>
