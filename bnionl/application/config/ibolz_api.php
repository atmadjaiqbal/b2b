<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['ibolz_app_id'] = 'com.balepoint.ibolz.bni';
$config['ibolz_customer_id'] = '2';
$config['ibolz_device_id'] = '4';
$config['ibolz_lang'] = 'en_US';
$config['ibolz_version'] = '1.0.6';
$config['ibolz_mobile'] = '';
$config['ibolz_api'] = 'http://localhost/ntmctv/api106/v2/';
//$config['ibolz_api'] = 'http://bni.ibolz.tv/api106/v2/';

/* --- API config item ----- */
$config['api_menu'] = $config['ibolz_api'] . 'menu/menu_list';
$config['api_banner'] = $config['ibolz_api'] .'product/boxoffice_list';
$config['api_categories'] = $config['ibolz_api'] . 'menu/menu_list';
$config['api_product_bymenuid'] = $config['ibolz_api'] . 'product/product_list';
$config['api_product_detail'] = $config['ibolz_api'] . 'product/product_detail';



$config['api_banner_slider'] = $config['ibolz_api'] .'product/boxoffice_list';
$config['api_auth'] = $config['ibolz_api']  . 'apps/auth';
$config['api_content'] = $config['ibolz_api']  . 'play/content';

$config['api_content_category_list'] = $config['ibolz_api']  . 'content/content_category_list';
$config['api_content_list'] = $config['ibolz_api']  . 'content/content_list';

$config['api_vod_category_list'] = $config['ibolz_api']  . 'vod/channel_category_list';
$config['api_vod_list'] = $config['ibolz_api']  . 'vod/content_vod_list';
$config['api_content_detail'] = $config['ibolz_api']  . 'vod/content_detail';
$config['api_content_play'] = $config['ibolz_api']  . 'play/content';

$config['api_register'] = $config['ibolz_api']  . 'customer/signup';
$config['api_login'] = $config['ibolz_api'] . 'customer/login';
$config['api_forgot_password'] = $config['ibolz_api'] . 'customer/forget_password';
$config['api_customer_profile'] = $config['ibolz_api'] . 'customer/profile';
$config['api_update_profile'] = $config['ibolz_api'] . 'customer/update_profile';
$config['api_update_profile'] = $config['ibolz_api'] . 'customer/update_profile';
$config['api_change_password'] = $config['ibolz_api'] . 'customer/change_password';

$config['api_menu_list'] = $config['ibolz_api'] . 'menu/menu_list/';
$config['api_product_list'] = $config['ibolz_api'] . 'product/product_list/';
$config['api_channel_list'] = $config['ibolz_api'] . 'channel/channel_list/';

