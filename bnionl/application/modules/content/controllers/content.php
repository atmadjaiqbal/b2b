<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Application {

    public function __construct()
    {
        parent::__construct();

        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->library('home/home_lib');
        $this->load->library('content_lib');
    }


    public function index()
    {
        redirect('/');
    }


    public function content_info()
    {
        $product_id = $this->uri->segment(3);
        $data['menu_id'] = $this->uri->segment(4);
        $data['product_detail'] = $this->content_lib->product_detail($product_id, $data['menu_id']);
        $this->load->view('content/content_sliderinfo', $data);
    }

    public function content_detail()
    {
        $data = $this->data;
        $product_id = $this->uri->segment(3);
        $menu_id = $this->uri->segment(4);
        $data['product_detail'] = $this->content_lib->product_detail($product_id, $menu_id);
        $data['headermenu'] = $this->home_lib->header_menu();
        $html['html']['content']  = $this->load->view('content/content_detail', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function search()
    {
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $filter = $this->input->post('term');
            $data['listcontent'] = $this->content_lib->search($filter);
        }
        $html['html']['content']  = $this->load->view('content/content_search', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);

    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }


}