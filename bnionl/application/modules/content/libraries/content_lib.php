<?php
class Content_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    function get_listcontent()
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '4',
        );
        $api_url = config_item('api_categories');
        $response = $this->CI->rest->post($api_url, $post_params);
        $resmerge = array_merge($response->menu_list[0]->sub_menu_list, $response->menu_list[1]->sub_menu_list);
        return $resmerge;
    }

    function product_menu_list($menu_id, $page=0, $limit=6) {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '1',
            'menu_id' => $menu_id,
            'page' => $page,
            'limit' => $limit
        );

        $api_url = config_item('api_product_bymenuid');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function product_detail($product_id, $menu_id)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'product_id' => $product_id,
            'menu_id' => $menu_id
        );

        $api_url = config_item('api_product_detail');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }


}
