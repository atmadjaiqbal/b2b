<?php


?>
<div class="full-container fixed-container">
    <?php $this->load->view('template/tpl_search');?>
</div>

<div class="full-container">
    <div id="section-slider">
        <h3 class="slider-title">
            <a href="#"><span><?php echo urldecode($channel_categories_name);?></span> <i class="fa fa-angle-right"></i></a>
        </h3>

        <div class="section-content-info">
            <div class="poster bgfull" style="background: url() no-repeat scroll 0 0 rgba(0, 0, 0, 0);width:278px;height:180px;"></div>
            <div class="poster-info">
                <a href="#">
                    <h2 class="site-color"><span id="poster-info-title"></span></h2>
                </a>
                <div class="row">
                    <div class="col-sm-12">
                        <dl class="dl-horizontal">
                            <dt>Year</dt><dd>2014 &nbsp;</dd>
                            <dt>Genre</dt>
                            <dd><a href="#">Comedy, Horor, Drama</a></dd>
                            <dt>Stars</dt>
                            <dd>Master Kungfu, Zhao Ling</dd>
                            <dt>Synopsis</dt>
                            <dd>The stories about something testing lorem ipsum dolor siamet, lorem ipsum rores lowest high tall width high definition.</dd>
                        </dl>
                    </div>
                </div>
            </div>

        </div>
        <div class="clear"></div>
        <div id="content_vod_list" class="content-vod-list" data-page='1' data-channelid="<?php echo $channel_id;?>"><div>

    </div>
</div>
