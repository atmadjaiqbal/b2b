<?php

?>
<div class="full-container fixed-container">
    <?php $this->load->view('template/tpl_search');?>
</div>

<div class="full-container">
    <div id="section-slider">
        <h3 class="slider-title">
            <a href="#" style="cursor:none;"><span>Search </span> <i class="fa fa-angle-right"></i></a>
        </h3>
    <?php
    if($listcontent->content_list)
    {
    ?>
        <div id="vod-list">
            <ul>
    <?php
        foreach($listcontent->content_list as $row)
        {
            $apps_id = $row->apps_id;
            $channel_category_id = $row->channel_category_id;
            $channel_id = $row->channel_id;
            $channel_type_id = $row->channel_type_id;
            $content_id = $row->content_id;
            $content_type = $row->content_type;
            $title = $row->title;
            $thumbnail = $row->thumbnail;
            $description = $row->description;
            $prod_year = $row->prod_year;
            $video_duration = $row->video_duration;
            $crew_category_list = $row->crew_category_list;
    ?>
            <li class="item-movie">
                <a href="<?php echo base_url().'content/contentvoddetail/'.$content_type.'/'.$channel_id.'/'.$content_id; ?>" class="link-item"
                   data-toggle="tooltip" data-original-title="<?php echo $description;?>">
                    <img src="<?php echo $thumbnail; ?>"/>
                    <p class="overlay-text"><?php echo $title;?></p>
                </a>
            </li>
    <?php
        }
    ?>
            </ul>
        </div>
    <?php
    } else {
    ?>
        <div id="section-movie-detail" style="text-align: center !important;margin-top:70px;">
            <h3>找不到 | Not found </h3>
        </div>
    <?php
    }
    ?>

    </div>
</div>