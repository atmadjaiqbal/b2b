<?php
if($product_detail)
{
   $rcode = isset($product_detail->rcode) ? $product_detail->rcode : '';
   $channel_id = isset($product_detail->channel_id) ? $product_detail->channel_id : '';
   $content_id = isset($product_detail->content_id) ? $product_detail->content_id : '';
   $title = isset($product_detail->title) ? $product_detail->title : '';
   $video_duration = isset($product_detail->video_duration) ? $product_detail->video_duration : '';
   $filename = isset($product_detail->filename) ? $product_detail->filename : '';
   $thumbnail = isset($product_detail->thumbnail) ? $product_detail->thumbnail : '';
   $description = isset($product_detail->description) ? $product_detail->description : '';
   $prod_year = isset($product_detail->prod_year) ? $product_detail->prod_year : '';
   $trailer_id = isset($product_detail->trailer_id) ? $product_detail->trailer_id : '';
   $price = isset($product_detail->price) ? $product_detail->price : '';
   $ulike = isset($product_detail->ulike) ? $product_detail->ulike : '';
   $unlike = isset($product_detail->unlike) ? $product_detail->unlike : '';
   $comment = isset($product_detail->comment) ? $product_detail->comment : '';
   /*
   $crew_category_list = $product_detail->crew_category_list;
   $genre_list = $product_detail->genre_list;
   $photo_list = $product_detail->photo_list;
   $related_list = $$product_detail->related_list;
   */
?>

<a href="<?php echo base_url();?>content/content_detail/<?php echo $content_id; ?>/<?php echo $menu_id;?>">
    <h2 class="site-color"><?php echo $title; ?></h2>
</a>
<div class="content-info">
  <h3>DESCRIPTION</h3>
  <p><?php echo $description; ?></p>
  <dl class="dl-horizontal">
     <dt>LECTURE : </dt><dd><?php
          if($product_detail->crew_category_list)
          {
              $crewlist = '';
              foreach($product_detail->crew_category_list->crew_list as $key => $crew)
              {
                  $crewlist .= $crew->crew_name . ', ';
              }
              echo $crewlist;
          }
          ?></dd>
     <dt>DATE : </dt><dd><?php echo $prod_year;?></dd>
  </dl>
  </div>
</div>
<?php
}
?>