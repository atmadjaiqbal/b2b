<div class="container" style="margin-top: 140px;">
<?php
/*
echo "<pre>";
print_r($product_detail);
echo "</pre>";
*/

if($product_detail)
{
    $player_wrap_width = "620"; $player_width = "620"; $player_height = "400";
    $crew_category_list = array(); $genre_list = array(); $photo_list = array(); $related_list = array();
    $rcode = $product_detail->rcode;
    $channel_id = $product_detail->channel_id;
    $content_id = $product_detail->content_id;
    $title = $product_detail->title;
    $video_duration = $product_detail->video_duration;
    $filename = $product_detail->filename;
    $thumbnail = $product_detail->thumbnail;
    $description = $product_detail->description;
    $prod_year = $product_detail->prod_year;
    $trailer_id = $product_detail->trailer_id;
    $price = $product_detail->price;
    $ulike = $product_detail->ulike;
    $unlike = $product_detail->unlike;
    $comment = $product_detail->comment;
    $crew_category_list = $product_detail->crew_category_list;
    $genre_list = $product_detail->genre_list;
    $photo_list = $product_detail->photo_list;
    $related_list = $product_detail->related_list;

    $player = 'http://china2.ibolz.tv/seeme/assets/flash/iBolz/index.php?w='.$player_width.'&h='.$player_height;
    $player .= '&apps_id='.config_item('ibolz_app_id').'&version='.config_item('ibolz_version').'&type=content';
    $player .= '&channel_id='.$channel_id.'&vid='.$content_id.'&id='.$content_id.'&customer_id='.config_item('ibolz_customer_id');
?>

    <div class="bnicontent-detail">
        <h3 class="slider-title">
            <a href="#" style="cursor: default;"><span><?php echo $title;?></span> <i class="fa fa-angle-right"></i></a>
        </h3>
        <div class="span4">
            <img class="poster bgfull" src="<?php echo $thumbnail;?>&w=300px" style="width:300px;height:400px;">
        </div>


        <div class="span8" style="margin-left:5px !important;">
            <div class="row" class="item-sec">
                <iframe seamless wmode="Opaque" class="player bg-loader" scrolling="no"
                        style='width: <?php echo $player_width;?>px;min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;'
                        src="<?php echo $player;?>">
                </iframe>
            </div>

            <div class="row item-sec">
                <h3>SESSIONS</h3>
                <div class="sessions">
                    <ul>
                    <?php
                    $default_r_thumb = base_url().'assets/images/no_video.png'; $i = 1;
                    if($related_list)
                    {
                       foreach($related_list as $value)
                       {
                         if($i == 3) break;
                         $r_content_id = $value->content_id;
                         $r_content_related_id = $value->content_related_id;
                         $r_title = $value->title;
                         $r_video_duration = $value->video_duration;
                         $r_thumbnail = isset($value->thumbnail) ? $value->thumbnail : $default_r_thumb;
                    ?>
                         <li><div><img src='<?php echo $r_thumbnail;?>'></div></li>
                    <?php
                         $i++;
                       }
                    }

                    if($i <= 3 || $i <= 2)
                    {
                        for($y = 1; $y <= (3 - $i) + 1; $y++)
                        {
                            echo '<li><div><img src="'.$default_r_thumb.'"></div></li>';
                        }
                    }
                    ?>
                    </ul>
                </div>
            </div>

            <div class="row item-sec">
                <h3>DESCRIPTION</h3>
                <h4><?php echo $title; ?></h4>
                <div class="content-info">
                    <dl class="dl-horizontal">
                        <dt>LECTURE : </dt><dd><?php
                            if($product_detail->crew_category_list)
                            {
                                $crewlist = '';
                                foreach($product_detail->crew_category_list->crew_list as $key => $crew)
                                {
                                    $crewlist .= $crew->crew_name . ', ';
                                }
                                echo $crewlist;
                            }
                            ?></dd>
                        <dt>DATE : </dt><dd><?php echo $prod_year;?></dd>
                    </dl>
                    <h3>DESCRIPTION</h3>
                    <p><?php echo $description; ?></p>
                </div>
            </div>

            <div class="row item-sec">
                <h3>PLAYLIST</h3>

            </div>

        </div>

    </div>

<?php
}
?>
</div>