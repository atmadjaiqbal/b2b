<?php
class Home_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    function get_auth()
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'lang' => config_item('ibolz_lang'),
            'version' => config_item('ibolz_version')
        );

        $api_url = config_item('api_auth');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function header_menu()
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '4',
        );
        $api_url = config_item('api_menu');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function headslider($limit=5, $offset=0)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '4',
            'page' => $offset,
            'limit' => $limit
        );
        $api_url = config_item('api_banner');
        $response = $this->CI->rest->post($api_url, $post_params);
/*
        echo "<pre>";
        print_r($response);
        echo "</pre>";
        exit;
*/

        return $response;
    }

}
