<div class="container">
<?php
if($listcontent)
{
    foreach($listcontent as $val)
    {
        $menu_id = $val['menu_id'];
        $menu_title = $val['title'];
        $menu_thumbnail = $val['thumbnail'];
        $menu_type = $val['menu_type'];
        $product_id = $val['product_id'];

        if($val['list_content']->product_list)
        {
?>

 <div id="products-<?php echo $menu_id;?>" class="home-content">
   <h3 class="slider-title">
      <a href="#"><span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i></a>
   </h3>
   <?php if(!isset($val['list_content']) || !$val['list_content']) { ?>
   <div class="alert alert-danger">
       <?php echo lang('no_products');?>
   </div>
   <?php } else { ?>

   <div class="span4 poster-content bg-loader" id="photo-<?php echo $menu_id; ?>">
       <!-- div class="poster bgfull" style="background: url() no-repeat scroll 0 0 rgba(0, 0, 0, 0);width:250px;height:320px;background-color:#000000;"></div -->
       <img class="poster bgfull" style="width:300px;height:400px;">

   </div>
   <div class="span8 content-item">
       <div class="row" id="content-info-<?php echo $menu_id; ?>"></div>
          <?php
          $carouselimg1 = '';$carouselimg2 = '';$carouselimg3 = '';$carouselimg4 = '';$carouselimg5 = '';$carouselimg6 = '';$i = 1;
          foreach($val['list_content']->product_list as $rwcontent)
          {
             if($i == 1)
             {
                 $default_menu_id = $rwcontent->menu_id;
                 $default_product_id = $rwcontent->product_id;
                 $default_thumbnail = $rwcontent->thumbnail;
             }
             $sub_menu_id = $rwcontent->menu_id;
             $sub_apps_id = $rwcontent->apps_id;
             $sub_product_id = $rwcontent->product_id;
             $sub_channel_category_id = $rwcontent->channel_category_id;
             $sub_channel_id = $rwcontent->channel_id;
             $sub_channel_type_id = $rwcontent->channel_type_id;
             $sub_content_id = $rwcontent->content_id;
             $sub_content_type = $rwcontent->content_type;
             $sub_title = $rwcontent->title;
             $short_title = strlen($sub_title) > 12 ? substr($sub_title, 0, 12) : $sub_title;
             $sub_thumbnail = $rwcontent->thumbnail.'&w=200px';
             $sub_description = $rwcontent->description;
             $sub_prod_year = $rwcontent->prod_year;
             $sub_video_duration = $rwcontent->video_duration;
             $sub_countlike = $rwcontent->countlike;
             $sub_countviewer = $rwcontent->countviewer;
             $sub_icon_size = $rwcontent->icon_size;
             $sub_poster_size = $rwcontent->poster_size;
             $sub_crew_category_list = $rwcontent->crew_category_list;
             $sub_genre_list = $rwcontent->genre_list;
          ?>
          <script type="text/javascript">
              $(document).ready(function(){
                  vod.content_view(
                      '#content-info-<?php echo $menu_id;?>', '<?php echo $default_product_id;?>', '<?php echo $default_menu_id; ?>',
                      function()
                      {
                          //$('#photo-<?php echo $menu_id; ?> .poster').removeAttr('style').attr('style', 'background: url(<?php echo $default_thumbnail;?>) no-repeat scroll 0 0 rgba(0, 0, 0, 0);width:250px;height:320px;background-color:#000000;');
                          $('#photo-<?php echo $menu_id; ?> .poster').attr('src', '<?php echo $default_thumbnail;?>');
                          $('#photo-<?php echo $menu_id; ?>').removeClass('bg-loader');
                      }
                  );

                  $('#<?php echo $sub_menu_id; ?>-<?php echo $sub_product_id; ?>').on('click', function(ev) {
                      ev.preventDefault();
                      $('#photo-<?php echo $menu_id; ?>').removeClass('bg-loader');
                      var product_id = $(this).data('id');
                      var menu_id = $(this).data('menu-id');
                      vod.content_view(
                          '#content-info-<?php echo $menu_id;?>', product_id, menu_id,
                          function(){
                              $('#photo-<?php echo $menu_id; ?> .poster').attr('src', '<?php echo $sub_thumbnail;?>');
                              //$('#photo-<?php echo $menu_id; ?> .poster').removeAttr('style').attr('style', 'background: url(<?php echo $sub_thumbnail;?>) no-repeat scroll 0 0 rgba(0, 0, 0, 0);width:250px;height:320px;background-color:#000000;');
                              $('#photo-<?php echo $menu_id; ?>').removeClass('bg-loader');
                          }
                      );

                  });

              });
          </script>

          <?php
             $carouselItem = '<div class="product-title">
                                  <a href="">
                                    <span class="pull-left">'.$short_title.'</span>
                                    <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                  </a>
                              </div>
                              <div class="product-image">
                                  <img class="preview_product related bgfull" id="'.$sub_menu_id.'-'.$sub_product_id.'"
                                       data-id="'.$sub_product_id.'" data-menu-id="'.$sub_menu_id.'" src="'.$sub_thumbnail.'">
                              </div>';
              if($i == 1) $carouselimg1 = $carouselItem; if($i == 2) $carouselimg2 = $carouselItem;
              if($i == 3) $carouselimg3 = $carouselItem; if($i == 4) $carouselimg4 = $carouselItem;
              if($i == 5) $carouselimg5 = $carouselItem; if($i == 6) $carouselimg6 = $carouselItem;
              $i++;
          ?>


          <?php
          }
          ?>

          <div class="carousel slide panelslider">
              <a id="movie_caro_left" class="left carousel-control" href="#movieCarousel-<?php echo $menu_id;?>" data-slide="next"></a>
              <a id="movie_caro_right" class="right carousel-control" href="#movieCarousel-<?php echo $menu_id;?>" data-slide="next"></a>
              <div id="movieCarousel-<?php echo $menu_id;?>">
                  <div class="carousel-inner">
                      <div class="item active">
                          <div class="content-item"><?php echo isset($carouselimg1) ? $carouselimg1 : ''; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg2) ? $carouselimg2 : ''; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg3) ? $carouselimg3 : ''; ?></div>
                      </div>
                      <div class="item">
                          <div class="content-item"><?php echo isset($carouselimg4) ? $carouselimg4 : ''; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg5) ? $carouselimg5 : ''; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg6) ? $carouselimg6 : ''; ?></div>
                      </div>
                  </div>
              </div>
          </div>
   </div>

   <?php
   }
   ?>
</div>
<?php
        }
    }
}
?>
</div>