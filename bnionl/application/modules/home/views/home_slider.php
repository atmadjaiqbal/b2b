<div id="slideheadline" class="carousel slide">
    <div class="carousel-inner">
        <?php
        $i = 0;foreach($banner->product_list as $key => $val)
        {
            $i++;
            if($key == 0) { $activ = "active"; } else { $activ = ""; }
            $img = str_replace("&w=","w=960px",$val->thumbnail);
        ?>
            <div class="item <?php echo $activ; ?> slideimg">
                <img src="<?php echo $val->thumbnail;?>" alt="" style="width: 100%; height: auto;">
            </div>
        <?php
        }
        ?>
    </div>

    <ol class="carousel-indicators">
    <?php
    for($y = 0; $y < $i; $y++)
    {
        if($y == 0) { $act = 'class="active"'; } else {$act = ""; }
    ?>
        <li data-target="#slideheadline" data-slide-to="<?php echo $y; ?>" <?php echo $act; ?>></li>
    <?php
    }
    ?>
    </ol>

</div>

