<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Application {

    public function __construct()
    {
        parent::__construct();

        $data = $this->data;

        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->library('home_lib');
        $this->load->library('content/content_lib');
    }


    public function index()
    {
        $data = $this->data;
        $breadcrumb = array();
        $data['breadcrumb'] = $breadcrumb;
        $data['headermenu'] = $this->home_lib->header_menu();

        $data['bannerslider'] = $this->banner_slider();
        $data['contentslider'] = $this->content_slider();

        $html['html']['content']  = $this->load->view('home/home_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }


    function banner_slider()
    {
        $data['banner'] = $this->home_lib->headslider(5, 0);
        return $this->load->view('home/home_slider', $data, true);
    }

    function content_slider()
    {
        $data['scripts']     = ''; //array('ibolz.chn.js');
        $listcontent = array();$i=0;
        $menulist = $this->content_lib->get_listcontent();
        foreach($menulist as $val)
        {
            $product_id = $val->product_id;
            $apps_id = $val->apps_id;
            $menu_id = $val->menu_id;
            $parent_menu_id = $val->parent_menu_id;
            $menu_type = $val->menu_type;
            $title = $val->title;
            $thumbnail = $val->thumbnail;
            $link = $val->link;
            $tab_type = $val->tab_type;
            $show_tab_menu = $val->show_tab_menu;
            $show_dd_menu = $val->show_dd_menu;
            $active = $val->active;
            $count_product = $val->count_product;
            $icon_size = $val->icon_size;
            $poster_size = $val->poster_size;
            $created = $val->created;
            $listcontent[$i]['title'] = $title;
            $listcontent[$i]['thumbnail'] = $thumbnail;
            $listcontent[$i]['menu_type'] = $menu_type;
            $listcontent[$i]['menu_id'] = $menu_id;
            $listcontent[$i]['parent_menu_id'] = $parent_menu_id;
            $listcontent[$i]['product_id'] = $product_id;
            $listcontent[$i]['list_content'] = $this->content_lib->product_menu_list($menu_id);
            $i++;
        }
        $data['listcontent'] = $listcontent;
        return $this->load->view('home/home_content', $data, true);
    }


}