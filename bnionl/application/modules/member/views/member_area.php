<?php
if($profile)
{
    $mobile_id = $profile->mobile_id;
    $customer_id = $profile->customer_id;
    $email = $profile->email;
    $user_id = $profile->user_id;
    $first_name = $profile->first_name;
    $last_name = $profile->last_name;
    $filename = $profile->filename;
    $thumbnail = $profile->thumbnail;
    $display_name = $profile->display_name;
    $status = $profile->status;
    $mobile_no = $profile->mobile_no;
    $verify_status = $profile->verify_status;
    $credit_balance = $profile->credit_balance;
    $qq_number = $profile->qq_number;
    $photo = $profile->photo;
?>
<div class="full-container fixed-container">
    <?php $this->load->view('template/tpl_search');?>
</div>

<div class="full-container">
    <div id="section-member">
        <h3 class="member-title">
            <a href="#"><i class="fa fa-unlock"></i>&nbsp;<span>我的帳戶 | My Account</span></a>
        </h3>
        <div class="member-subtitle">
            <p style="border-bottom: solid 1px #FFA500;width: 85%;">欢迎到您的帐户。在这里您可以管理您的所有配置文件信息和密码 | Welcome to your account. Here you can manage all of your profile information and password.</p>
        </div>
        <div class="member-subtitle">
            <p style="font-size: 14px;font-weight: bold;">您的配置文件 | Your profile</p>
        </div>

        <?php if(!empty($msg)){ ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $msg; ?>.
            </div>
        <?php } ?>
        <form class="form" action='<?php echo base_url(); ?>member/memberupdate' method='post' role="form" novalidate="novalidate">
            <input type="hidden" name="submitted" value="submitted" />
            <input type="hidden" name="areaform" value="profile"/>
            <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>"/>
            <div style="float:left;width:40%;margin-left:10%;">
                <div class="form-group">
                    <label for="fullname">全名 | Full Name</label>
                    <input type="text" name="fullname" class="inputtext" id="fullname" value="<?php echo $display_name;?>" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="email">电子邮件 | Email</label>
                    <input type="text" name="email" class="inputtext" id="email" value="<?php echo $email; ?>" autocomplete="off">
                </div>
            </div>

            <div style="float:left;width:40%;margin-left:2%;">
                <div class="form-group">
                    <label for="phone">电话 | Phone</label>
                    <input type="text" name="phone" class="inputtext" id="phone" value="<?php echo $mobile_no;?>" autocomplete="off">
                </div>
            </div>

            <div style="clear: both;margin-top:20px;margin-left: 10%;">
                <button type="submit" class="btn btn-primary"><i class="fa fa-user"></i> Update profile</button>
            </div>
        </form>

        <div class="member-subtitle" style="margin-top:25px;">
            <p style="font-size: 14px;font-weight: bold;">更改密碼 | Change password</p>
        </div>
        <form class="form" action='<?php echo base_url(); ?>member/memberupdate' method='post' role="form" novalidate="novalidate">
            <input type="hidden" name="submitted" value="submitted" />
            <input type="hidden" name="areaform" value="passchange"/>
            <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>"/>

            <div style="float:left;width:40%;margin-left:10%;">
                <div class="form-group">
                    <label for="password">密碼 | Password</label>
                    <input type="password" name="password" class="inputtext" id="password" autocomplete="off" />
                </div>
            </div>

            <div style="float:left;width:40%;margin-left:2%;">
                <div class="form-group">
                    <label for="newpass">新密碼 | New Password</label>
                    <input type="password" name="newpass" class="inputtext" id="newpass" autocomplete="off" />
                </div>
                <div class="form-group">
                    <label for="confirmpassnew">確認新密碼 | Confirm New Password</label>
                    <input type="password" name="confirmpassnew" class="inputtext" id="confirmpassnew" autocomplete="off" />
                </div>
            </div>

            <div style="clear: both;margin-top:20px;margin-left: 10%;">
                <button type="submit" class="btn btn-primary"><i class="fa fa-lock"></i> Change</button>
            </div>
        </form>

    </div>
</div>

<?php
}
?>