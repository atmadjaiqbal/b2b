<?php

?>
<div class="full-container fixed-container">
    <?php $this->load->view('template/tpl_search');?>
</div>

<div class="full-container">
    <div id="section-member">
        <h3 class="member-title">
            <a href="#"><i class="fa fa-lock"></i>&nbsp;<span>登錄 | Login</span></a>
        </h3>
        <div class="member-subtitle">
            <p>登录到看我TV | Login to seeme tv</p>
        </div>

        <?php if(!empty($msg)){ ?>
            <div class="alert" style="margin-right: 63% !important;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Sorry! </strong> <?php echo $msg; ?>.
            </div>
        <?php } ?>

        <form class="form" action='<?php echo base_url(); ?>member/login' method='post' role="form" novalidate="novalidate">
            <div style="float:left;width:40%;margin-left:10%;">
                <div class="form-group">
                    <label for="login_email">輸入的電子郵件 | Enter email</label>
                    <input type="text" name="email" class="inputtext" id="login_email" value="" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="account_password">密碼 | Password</label>
                    <input type="password" name="password" class="inputtext" id="account_password" autocomplete="off" />
                </div>
            </div>

            <div style="clear: both;margin-top:20px;margin-left: 10%;">
                <button type="submit" class="btn btn-primary">Login</button>
                &nbsp;&nbsp;<a href="<?php echo base_url(); ?>member/forgotpassword" class="btn btn-primary">Forgot Password</a>
            </div>

        </form>


    </div>
</div>