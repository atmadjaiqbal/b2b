<?php

?>
<div class="full-container fixed-container">
    <?php $this->load->view('template/tpl_search');?>
</div>

<div class="full-container">
    <div id="section-member">
        <h3 class="member-title">
            <a href="#"><i class="fa fa-lock"></i>&nbsp;<span>忘記了密碼 | Forgot password</span></a>
        </h3>
        <div class="member-subtitle">
            <p>輸入您的電子郵件地址，我們會送你一封電子郵件，使您可以重置您的密碼。別忘了檢查您的垃圾郵件資料夾。</p>
            <p style="margin-top:-25px;">Enter your email address and we will send you an email that will allow you to reset your password. Don't forget to check your spam-folder.</p>
        </div>

        <?php if(!empty($msg)){ ?>
            <div class="alert" style="margin-right: 63% !important;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $msg; ?>.
            </div>
        <?php } ?>

        <form class="form" action='<?php echo base_url(); ?>member/forgotpassword' method='post' role="form" novalidate="novalidate">
            <div style="float:left;width:40%;margin-left:10%;">
                <div class="form-group">
                    <label for="login_email">輸入的電子郵件 | Enter email</label>
                    <input type="text" name="email" class="inputtext" id="login_email" value="" autocomplete="off">
                </div>
            </div>

            <div style="clear: both;margin-top:20px;margin-left: 10%;">
                <button type="submit" class="btn btn-primary">發送 | Send</button>
            </div>

        </form>


    </div>
</div>