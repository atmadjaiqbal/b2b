<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* ===== 27 Nopember 2014          =====
   ===== Author : Nyoman Adi Putra ===== */

class Member extends Application {

    public function __construct()
    {
        parent::__construct();
        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->library('form_validation');
        $this->load->library('member_lib');
        $this->load->library('home/home_lib');
        $this->load->library('content/content_lib');
    }

    public function index()
    {
        redirect('/');
    }

    public function member_session() {
        if($this->session->userdata('email')) {
            return true;
        } else {
            return false;
        }
    }

    public function signup()
    {
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();
        if(!$this->session->userdata('display_name'))
        {
            $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|callback_check_database');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|matches[confirm]');
            $this->form_validation->set_rules('confirm', 'Password Confirmation', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                $data['msg'] = (validation_errors() ? validation_errors() : ($this->session->flashdata('message')));
            } else {

                $_insert['name'] = $this->input->post('fullname');
                $_insert['email'] = $this->input->post('email');
                $_insert['password'] = $this->input->post('password');
                $_insert['mobile_no'] = $this->input->post('phone');

                $resp = $this->member_lib->insert_member($_insert);
                //echo "<pre>"; print_r($resp); echo "</pre>";exit;

                if($resp->rcode == 1) {
                    $msg = 'Successfull register';
                    $this->session->flashdata('message', $msg);
                } else {
                    $msg = $resp->message;
                    $this->session->flashdata('message', $msg);
                }
            }
        }

        $html['html']['content']  = $this->load->view('member/member_signup', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function signout()
    {
        if(!$this->member_session()) {
            redirect('/');
        } else {
            $this->session->sess_destroy();
            redirect('/');
        }

    }

    public function login()
    {
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();

        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            if(isset($_POST['email']) && isset($_POST['password']))
            {
                if(!is_null($_POST['email']) && !is_null($_POST['password']))
                {
                    $email = $this->input->post('email');
                    $password = $this->input->post('password');
                    $response = $this->member_lib->get_by_email_password($email, $password);

                    if($response->rcode == 0) {
                        $data['msg'] = $response->message;
                        $this->session->flashdata('message', $data['msg']);
                    } else {
                        foreach($response as $key => $val) {
                            $this->session->set_userdata($key, $val);
                        }
                        redirect('/');
                    }
                }
            }
        }

        $html['html']['content']  = $this->load->view('member/member_login', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function forgotpassword()
    {
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|callback_check_database');
        if($this->form_validation->run() == FALSE)
        {
           $data['msg'] = (validation_errors() ? validation_errors() : ($this->session->flashdata('message')));
        } else {
            $email = $this->input->post('email');
            $resp = $this->member_lib->get_forget_password($email);
            if($resp)
            {
              if($resp->rcode == 1) {
                 $data['msg'] = 'Please check your email for reset password';
                 $this->session->flashdata('message', $data['msg']);
              } else {
                 $data['msg'] = $resp->message;
                 $this->session->flashdata('message', $data['msg']);
              }
            } else {
                $data['msg'] = 'Please check your email for reset password';
                $this->session->flashdata('message', $data['msg']);
            }
        }

        $html['html']['content']  = $this->load->view('member/forgot_password', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function memberupdate()
    {
        $data = $this->data;
        if(!$this->member_session()) { redirect('/');}
        $customer_id = $this->session->userdata('customer_id');
        $data['headermenu'] = $this->home_lib->header_menu();

        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            if($_POST['areaform'] == 'profile')
            {
                $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|xss_clean');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|callback_check_database');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean');
                if($this->form_validation->run() == FALSE)
                {
                    $data['msg'] = (validation_errors() ? validation_errors() : ($this->session->flashdata('message')));
                } else {
                    $_insert['customer_id'] = $this->input->post('customer_id');
                    $_insert['name'] = $this->input->post('fullname');
                    $_insert['email'] = $this->input->post('email');
                    $_insert['mobile_no'] = $this->input->post('phone');
                    $resp = $this->member_lib->update_profile($_insert);
                    if($resp->rcode == 1) {
                        $data['msg'] = 'Successfull update';
                        $this->session->flashdata('message', $data['msg']);
                    } else {
                        $data['msg'] = $resp->message;
                        $this->session->flashdata('message', $data['msg']);
                    }

                }

            } else {
                $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
                $this->form_validation->set_rules('newpass', 'New Password', 'trim|required|xss_clean|matches[confirmpassnew]');
                $this->form_validation->set_rules('confirmpassnew', 'Password Confirmation', 'required');
                if($this->form_validation->run() == FALSE)
                {
                    $data['msg'] = (validation_errors() ? validation_errors() : ($this->session->flashdata('message')));
                } else {
                    $_insert['customer_id'] = $this->input->post('customer_id');
                    $_insert['password'] = $this->input->post('password');
                    $_insert['new_password'] = $this->input->post('newpass');
                    $resp = $this->member_lib->change_password($_insert);
                    if($resp)
                    {
                        if($resp->rcode == 1)
                        {
                            $data['msg'] = 'Successfull update your password';
                            $this->session->flashdata('message', $data['msg']);
                        } else {
                            $data['msg'] = 'Failed update password';
                            $this->session->flashdata('message', $data['msg']);
                        }
                    }
                }

            }
        }

        $data['profile'] = $this->member_lib->get_profile($customer_id);

        $html['html']['content']  = $this->load->view('member/member_area', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function history() {
        if(!$this->member_session()) {
            // ga ada sessioin
        } else {

        }
    }

}

/* End of file member.php */
/* Location: ./application/controllers/member.php */