<?php
class Member_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    function get_by_email_password($email, $password)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'lang' => config_item('ibolz_lang'),
            'version' => config_item('ibolz_version'),
            'email' => $email,
            'password' => $password
        );
        $api_url = config_item('api_login');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function insert_member($data)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'lang' => config_item('ibolz_lang'),
            'version' => config_item('ibolz_version'),
            'email' => $data['email'],
            'name' => $data['name'],
            'mobile_no' => $data['mobile_no'],
            'password' => $data['password']
        );
        $api_url = config_item('api_register');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function get_forget_password($email) {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'lang' => config_item('ibolz_lang'),
            'version' => config_item('ibolz_version'),
            'email' => $email
        );
        $api_url = config_item('api_forgot_password');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function get_profile($customer_id) {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'lang' => config_item('ibolz_lang'),
            'version' => config_item('ibolz_version'),
            'customer_id' => $customer_id
        );
        $api_url = config_item('api_customer_profile');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function update_profile($data)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'lang' => config_item('ibolz_lang'),
            'version' => config_item('ibolz_version'),
            'customer_id' => $data['customer_id'],
            'email' => $data['email'],
            'name' 	=> $data['name'],
            'mobile_no' => 	$data['mobile_no']
        );
        $api_url = config_item('api_update_profile');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }


    function change_password($data) {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'lang' => config_item('ibolz_lang'),
            'version' => config_item('ibolz_version'),
            'customer_id' => $data['customer_id'],
            'password' => $data['password'],
            'new_password' => $data['new_password']
        );
        $api_url = config_item('api_change_password');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }
}