<?php


$mobiles = array(
    // legacy array, old values commented out
    'mobileexplorer'	=> 'Mobile Explorer',
//					'openwave'			=> 'Open Wave',
    'opera mini'		=> 'Opera Mini',
    'operamini'			=> 'Opera Mini',
//					'elaine'			=> 'Palm',
    'palmsource'		=> 'Palm',
//					'digital paths'		=> 'Palm',
//					'avantgo'			=> 'Avantgo',
//					'xiino'				=> 'Xiino',
    'palmscape'			=> 'Palmscape',
//					'nokia'				=> 'Nokia',
//					'ericsson'			=> 'Ericsson',
//					'blackberry'		=> 'BlackBerry',
//					'motorola'			=> 'Motorola'

    // Phones and Manufacturers
    'motorola'			=> "Motorola",
    'nokia'				=> "Nokia",
    'palm'				=> "Palm",
    'iphone'			=> "Apple iPhone",
    // 'ipad'				=> "iPad",
    'ipod'				=> "Apple iPod Touch",
    'sony'				=> "Sony Ericsson",
    'ericsson'			=> "Sony Ericsson",
    'blackberry'		=> "BlackBerry",
    'cocoon'			=> "O2 Cocoon",
    'blazer'			=> "Treo",
    'lg'				=> "LG",
    'amoi'				=> "Amoi",
    'xda'				=> "XDA",
    'mda'				=> "MDA",
    'vario'				=> "Vario",
    'htc'				=> "HTC",
    'samsung'			=> "Samsung",
    'sharp'				=> "Sharp",
    'sie-'				=> "Siemens",
    'alcatel'			=> "Alcatel",
    'benq'				=> "BenQ",
    'ipaq'				=> "HP iPaq",
    'mot-'				=> "Motorola",
    'playstation portable'	=> "PlayStation Portable",
    'hiptop'			=> "Danger Hiptop",
    'nec-'				=> "NEC",
    'panasonic'			=> "Panasonic",
    'philips'			=> "Philips",
    'sagem'				=> "Sagem",
    'sanyo'				=> "Sanyo",
    'spv'				=> "SPV",
    'zte'				=> "ZTE",
    'sendo'				=> "Sendo",

    // Operating Systems
    'symbian'				=> "Symbian",
    'SymbianOS'				=> "SymbianOS",
    'elaine'				=> "Palm",
    'palm'					=> "Palm",
    'series60'				=> "Symbian S60",
    'windows ce'			=> "Windows CE",
    'android'	=> "Android",

    // Browsers
    'obigo'					=> "Obigo",
    'netfront'				=> "Netfront Browser",
    'openwave'				=> "Openwave Browser",
    'mobilexplorer'			=> "Mobile Explorer",
    'operamini'				=> "Opera Mini",
    'opera mini'			=> "Opera Mini",

    // Other
    'digital paths'			=> "Digital Paths",
    'avantgo'				=> "AvantGo",
    'xiino'					=> "Xiino",
    'novarra'				=> "Novarra Transcoder",
    'vodafone'				=> "Vodafone",
    'docomo'				=> "NTT DoCoMo",
    'o2'					=> "O2",

    // Fallback
    'mobile'				=> "Generic Mobile",
    'wireless'				=> "Generic Mobile",
    'j2me'					=> "Generic Mobile",
    'midp'					=> "Generic Mobile",
    'cldc'					=> "Generic Mobile",
    'up.link'				=> "Generic Mobile",
    'up.browser'			=> "Generic Mobile",
    'smartphone'			=> "Generic Mobile",
    'cellphone'				=> "Generic Mobile"
);

$user_agent = $_SERVER['HTTP_USER_AGENT'];

$path = $_SERVER['REQUEST_URI'];

$is_mobile = false;

foreach($mobiles as $key=>$value) {
    if(strpos(strtolower($user_agent),$key)){
        $is_mobile = true;
        break;
    }
}

if($is_mobile == true){
    if(!strpos(strtolower($user_agent),'ipad')){
        header("Location:http://m.seeme-download.tv".$path);
        exit;
    }
}


define('ENVIRONMENT', 'development');

if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			error_reporting(E_ALL);
		break;
	
		case 'testing':
		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}
}

$system_path = 'system';
$application_folder = 'application';

if (defined('STDIN'))
{
	chdir(dirname(__FILE__));
}

if (realpath($system_path) !== FALSE)
{
	$system_path = realpath($system_path).'/';
}

$system_path = rtrim($system_path, '/').'/';

if ( ! is_dir($system_path))
{
	exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
}
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
	define('EXT', '.php');
	define('BASEPATH', str_replace("\\", "/", $system_path));
	define('FCPATH', str_replace(SELF, '', __FILE__));
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
    }
else
{
	if ( ! is_dir(BASEPATH.$application_folder.'/'))
	{
		exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
	}

	define('APPPATH', BASEPATH.$application_folder.'/');
}

require_once BASEPATH.'core/CodeIgniter.php';

