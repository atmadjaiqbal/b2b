<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider1, slider2, slider3 .jpg

*/

$config['nameLogo'] = "";
$config['title'] = "IBOLZ | GREENPEACE TV";
$config['warnaHeader'] = "#4BA100";
$config['colorMenu'] = "#4BA100";
$config['warnaCopyright'] = "#4BA100";
$config['warnaNavCarousel'] = "#4BA100";
$config['colorListbox'] = "green";
$config['warnaMenuMobile'] = "#4BA100";
$config['footerCopyright'] = "GREENPEACE TV";
$config['jmlSlider'] = 2;

/* --- About Us --- */

$config['aboutUs'] = "<p><b>Greenpeace</b> adalah suatu lembaga swadaya masyarakat, organisasi lingkungan global, yang memiliki cabang di lebih dari 40 negara dengan kantor pusat di Amstrerdam, Belanda.</p><p>Didirikan di Vancouver, British Columbia, Kanada pada 1971, pada awalnya dengan nama Don't Make a Wave Committee untuk menghentikan percobaan nuklir yang dilakukan pemerintah Amerika Serikat di Amchitka, Alaska. Para aktivis mengirimkan kapal sewaan, Phyllis Cormack, yang diubah namanya menjadi Greenpeace, ke lokasi pengujian nuklir. Mereka lalu mengadopsi nama Greenpeace menjadi nama organisasi.</p><p>Greenpeace dikenal menggunakan aksi langsung tanpa kekerasan, konfrontasi damai dalam melakukan kampanye untuk menghentikan berbagai aksi perusakan lingkungan seperti pengujian nuklir, penangkapan ikan paus besar-besaran, deforestasi, dan sebagainya.</p><p>Organisasi global ini menerima pendanaan melalui kontribusi langsung dari individu yang diperkirakan mencapai 2,8 juta para pendukung keuangan, dan juga dari yayasan amal, tetapi tidak menerima pendanaan dari pemerintah atau korporasi.</p>";

/* --- Contact Us --- */

$config['detailOffice'] = "Terimakasih atas minat anda pada Greenpeace Asia Tenggara. Jika anda mempunyai pertanyaan, silakan hubungi kami :";
$config['address'] = "Mega Plaza Building Lt. 5, Jl. HR. Rasuna Said Kav. C3, Kuningan, Jakarta Selatan, Indonesia 12920";
$config['tlp'] = "+62 21 521 2552";
$config['fax'] = "+62 21 521 2553";
$config['contactCenter'] = "";
$config['email'] = "info.id@greenpeace.org , supporterservices.id@greenpeace.org";
$config['website'] = "";

/* --- Download --- */

$config['namaFolder'] = "greenpeace";
$config['namaAPK'] = "GreenpeaceTV-2.0.5-1445434003";

?>

