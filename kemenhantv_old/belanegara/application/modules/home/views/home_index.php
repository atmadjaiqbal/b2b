<?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>

<section id="main-container" class="portfolio-static">
	<div class="container">
		<div class="row">
			<?php
		    if($listmenu) {
		        foreach($listmenu as $row) {
		            $c_menu_id = $row['menu_id'];
		            $c_category_name = $row['category_name'];

		            if ((strpos($c_category_name, "Property") !== FALSE) OR (strpos($c_category_name, "Training") !== FALSE)) {
		            	continue;
					}
		            ?>
		            <hr style="margin-top: 0px !important;"/>
					<div class="col-md-12 heading">
						<h3 class="classic"><i class="fa fa-list-alt"></i><div class="title"></div><?php echo $c_category_name;?></h3>
					</div>

					<?php

                    if($row['submenu']) {
                        $i=0;$isFrist = true;
                        foreach($row['submenu'] as $rwContent) {
                            if($rwContent->title == 'Nomor Penting') {
                                continue;
                            }

                            // echo "<pre>";var_dump($listmenu);echo "</pre>";

                            $_c_product_id = $rwContent->product_id;
                            $_c_apps_id = $rwContent->apps_id;
                            $_c_menu_id = $rwContent->menu_id;
                            $_c_parent_menu_id = $rwContent->parent_menu_id;
                            $_c_menu_type = $rwContent->menu_type;
                            $_c_title = $rwContent->title;
                            $_c_thumbnail = $rwContent->thumbnail;
                            $_c_link = $rwContent->link;
                            $_c_icon_size = $rwContent->icon_size;
                            $_c_poster_size = $rwContent->poster_size;
                            $short_title = strlen($_c_title) > 30 ? substr($_c_title, 0, 30).'...' : $_c_title;

                            switch(strtolower($_c_menu_type))
                            {
                                case 'menu' :
                                    $_link = base_url().'content/content_categories_list/'.$_c_menu_id.'/'.urlencode($_c_title);
                                    break;
                                case 'channel' :
                                    $_link = base_url().'content/content_channel/'.$_c_menu_id.'/'.$_c_product_id;
                                    break;
                                default:
                                    $_link = base_url().'content/content_categories/'.$_c_menu_id.'/'.urlencode($_c_title);
                                    break;
                            }
                            ?>
                                    
                            <?php
                            if ((preg_match('/IBOLZ - Live Channel/i',$row['category_name'])) OR (preg_match('/IBOLZ - Musik/i',$row['category_name'])) OR (preg_match('/IBOLZ - Movie & Trailer/i',$row['category_name']))) { ?>
                                <div class="col-sm-2 col-xs-6 portfolio-static-item">
                                    <a href="<?php echo $_link;?>">
                                        <div class="grid" style="border: none;height: 100px;">
                                            <figure class="effect-oscar">
                                                <img src="<?php echo $_c_thumbnail;?>&w=300" alt="">
                                            </figure>
                                        </div>
                                    </a>
                                </div><!--/ item 1 end -->
                            <?php
                            }
                            else { 
                                foreach ($row['channel_detail'][$i] as $value) {
                                    $viewer = $value->countviewer;
                                    $description = $value->description;
                                    $short_description = strlen($description) > 260 ? substr($description, 0, 260).'...' : $description;
                                } 

                                $col = $isFrist ? 'col-sm-6 col-xs-12' : 'col-sm-3 col-xs-6';
                                $height = $isFrist ? 'height: 175px;' : 'height: 90px;';
                                $p = $isFrist ? '' : 'font-size: 12px;';
                                $h = $isFrist ? 'h4' : 'h5';
                                $fontH = $isFrist ? '' : 'font-size: 12px;';
                                $short_title2 = $isFrist ? $short_title : substr($_c_title, 0, 12).'...';
                                $short_description = $isFrist ? $short_description : substr($description, 0, 45). '...';
                                ?>
                                <div class="<?php echo $col; ?> portfolio-static-item" style="padding-right: 0px;padding-bottom: 0px;">
                                    <a href="<?php echo $_link;?>">
                                        <div class="grid">
                                            <figure class="effect-oscar" style="float: left;width: 50%;<?php echo $height;?>">
                                                <img src="<?php echo $_c_thumbnail;?>&w=300" alt="">
                                            </figure>
                                            <div class="portfolio-static-desc" style="float: left;width: 50%;overflow: hidden;<?php echo $height;?>">
                                                <<?php echo $h;?> style="<?php echo $fontH;?>margin: 0px;color: black;font-weight: bold;"><?php echo $short_title2;?></<?php echo $h;?>> 
                                                <p style="<?php echo $p;?>margin-bottom: 0px;"><i class="fa fa-eye"></i> &nbsp<?php echo $viewer; ?> viewer<br>
                                                <?php echo $short_description;?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div><!--/ item 1 end -->
                            <?php }
                            ?>

                        <?php
                        $i++;$isFrist = false;
                        }
                    }
		            ?>
		            <div class="clearfix"></div>
		            <?php
		        }
		    }
		?>
				
		</div><!-- Content row end -->
	</div><!-- Container end -->
</section>