<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends Application {

    public function __construct()
    {
        parent::__construct();

        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->library('home/home_lib');
        $this->load->library('content_lib');
        $this->load->library('breadcrumbs');
        $this->load->module('member/member');
    }

    public function index()
    {
        redirect('/');
    }

    public function member_session() {
        if($this->session->userdata('email')) {
            return true;
        } else {
            return false;
        }
    }


    public function content_info()
    {
        $channel_id = $this->uri->segment(3);
        $data['menu_id'] = $this->uri->segment(4);
        $data['menu_type'] = $this->uri->segment(5);
        $data['menu_title'] = urldecode($this->uri->segment(6));
        $data['product_detail'] = $this->content_lib->channel_detail($channel_id);
        //$data['product_detail'] = $this->content_lib->product_detail($product_id, $data['menu_id']);
        $this->load->view('content/content_sliderinfo', $data);
    }

    public function content_detail()
    {
        $data = $this->data;
        $data['scripts'] = array('ibolz.onl.js');
        $data['product_id'] = $this->uri->segment(3);
        $data['menu_id'] = $this->uri->segment(4);
        $data['menu_title'] = urldecode($this->uri->segment(5));
        $data['product_detail'] = $this->content_lib->product_detail($data['product_id'], $data['menu_id']);

        if($data['product_detail']->privacy == 1)
        {
            if(!$this->member_session()) { redirect('member/login');}
        }

        if($this->member_session()) { $data['customer_id'] = $this->session->userdata('customer_id');} else {$data['customer_id'] = config_item('ibolz_customer_id');}
        $data['list_content'] = $this->content_lib->product_menu_list($data['menu_id']);
        $data['headermenu'] = $this->home_lib->header_menu();
        $this->breadcrumbs->add($data['menu_title'], base_url().'content/content_categories/'.$data['menu_id'].'/'.$data['menu_title']);
        $this->breadcrumbs->add($data['product_detail']->title, '#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $data['header_custom'] = "true";
        $html['html']['content']  = $this->load->view('content/content_detail', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function content_categories_list()
    {
        $data = $this->data;
        $data['scripts'] = array('ibolz.onl.js');
        $data['menu_id'] = $this->uri->segment(3);
        $data['menu_title'] = urldecode($this->uri->segment(4));
        $data['headermenu'] = $this->home_lib->header_menu();
        $data['menucat'] = $this->home_lib->header_menu($data['menu_id']);
        $damenucat = $this->home_lib->header_menu($data['menu_id']);
        if($damenucat)
        {
            $data['list_content'] = @$this->content_lib->product_menu_list($damenucat->menu_list[0]->menu_id, 0, 5);
        } else {
            $data['list_content'] = @$this->content_lib->product_menu_list($data['menu_id'], 0, 5);
        }

        $data['content_popular'] = $this->content_popular($data['menu_id']);
        $data['content_latest'] = $this->content_latest($data['menu_id']);

        $this->breadcrumbs->add($data['menu_title'], '#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $html['html']['content']  = $this->load->view('content/content_categories', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function content_categories()
    {
        $data = $this->data;
        $data['scripts'] = array('ibolz.onl.js');
        $data['menu_id'] = $this->uri->segment(3);
        $data['menu_title'] = urldecode($this->uri->segment(4));
        $data['headermenu'] = $this->home_lib->header_menu();
        $data['list_content'] = $this->home_lib->header_menu($data['menu_id'], 'detail');

        foreach($data['list_content']->menu_list as $val) {
            $channel_detail = $this->content_lib->channel_detail($val->product_id);
            $arrMenu[] = array(
                "channel_detail" => $channel_detail
            );
        }
        $data['detail'] = $arrMenu;

        $this->breadcrumbs->add($data['menu_title'], '#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $html['html']['content']  = $this->load->view('content/content_channel_list', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function content_channel()
    {
        $data = $this->data;
        $data['scripts'] = array('ibolz.onl.js');
        $data['menu_id'] = $this->uri->segment(3);
        $data['list_content'] = $this->home_lib->header_menu($data['menu_id'], 'detail');
        $data['product_id'] = $this->uri->segment(4);
        $title = $this->uri->segment(5);
        $data['headermenu'] = $this->home_lib->header_menu();
        $data['channel_detail'] = $this->content_lib->channel_detail($data['product_id']);
        $data['schedule_today'] = $this->content_lib->schedule_today($data['product_id']);
        $this->breadcrumbs->add($title, '#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $data['header_custom'] = "true";
        $html['html']['content']  = $this->load->view('content/content_channel', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    function content_popular($menu_id)
    {
        $data['menu_title'] = urldecode($this->uri->segment(4));
        $data['popular'] = $this->content_lib->product_popular("popular", 5, 0);
        return $this->load->view('content/content_popular', $data, true);
    }

    function content_latest($menu_id)
    {
        $data['menu_title'] = urldecode($this->uri->segment(4));
        $data['latest'] =  $this->content_lib->product_popular("latest", 5, 0);
        return $this->load->view('content/content_latest', $data, true);
    }

    public function get_content_categories()
    {
        $menu_id = $this->uri->segment(3);
        $page = $this->uri->segment(4); $limit = 8;
        $offset = (($limit*intval($page)) - $limit) + 5;
        $list_content = $this->content_lib->product_menu_list($menu_id, $offset, $limit);
        $data['result'] = $list_content;
        $this->load->view('content/content_categories_item', $data);
    }


    public function document_download()
    {
        $document_id = $this->input->get('document_id');
        $list_content = $this->content_lib->download_document($document_id);
        $filename = $list_content->document_list->filename;
        $document_type = $list_content->document_list->type;
        $document_size = $list_content->document_list->size;
        $file = $this->config->item('download_path') . $filename;
        header('Content-Description: File Transfer');
        //header('Content-Type: application/octet-stream');
        header('Content-Type: '.$document_type);
        header('Content-Disposition: attachment; filename='.$filename);
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }

    public function search()
    {
        $data = $this->data;
        $data['headermenu'] = $this->home_lib->header_menu();
        $this->breadcrumbs->add("Search", '#');
        if($this->input->post('term')) $this->breadcrumbs->add($this->input->post('term'), '#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $filter = $this->input->post('term');
            $data['listcontent'] = $this->content_lib->search($filter);
        }
        $html['html']['content']  = $this->load->view('content/content_search', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);

    }

    function post_like()
    {
        if(!$this->session->userdata('email')) {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
        } else {
            $customer_id = $this->session->userdata('customer_id');
            $id = $this->input->post('content_id');
            $result = $this->content_lib->post_like($id, $customer_id);
            if($result)
            {
                if($result->rcode == '1')
                {
                    $data['rcode'] = 'ok';
                    $data['msg'] = $result->message;
                } else {
                    $data['rcode'] = 'bad';
                    $data['msg'] = 'You have done vote like this lesson';
                }
            } else {
                $data['rcode'] = 'bad';
                $data['msg'] = 'connection problem !';
            }
        }
        $this->message($data);
    }

    function total_like($content_id)
    {
        $result = $this->content_lib->totallike($content_id);
        $product_id = $result->product_id;
        $total_like = $result->total_like;
        echo $total_like;
    }

    function content_comment($content_id='', $page=1)
    {
        $limit = 10;
        if(empty($content_id)){ echo ''; return;}
        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){ $offset += (intval($page) - 1); }
        $zrange = $limit + $offset;
        $result = $this->content_lib->get_comment($content_id);
        $data['result'] = $result;
        $this->load->view('template/tpl_comment', $data);
    }

    function post_comment()
    {
        if(!$this->session->userdata('email')) {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        } else {

            $customer_id = $this->session->userdata('customer_id');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('comment', '', 'required|trim|xss_clean');

            if ($this->form_validation->run() == FALSE)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'Komentar harus di isi';
                $this->message($data);
            }

            $id = $this->input->post('product_id');
            $val = $this->input->post('comment', true);

            if(is_bool($id) || is_bool($val)){
                $data['rcode'] = 'bad';
                $data['msg'] = 'Anda harus login !';
                $this->message($data);
            }
            $result = $this->content_lib->post_comment($id, $customer_id, $val);
            $htm = '<div class="user-comment">
                       <h3>'.$this->session->userdata('display_name').'&nbsp;|&nbsp;<span class="comment-date">'.date('d, F Y').'</span></h3>
                       <p>'.$val.'</p>
                    </div>';

            $data['rcode'] = 'ok';
            $data['msg'] = $htm;
            $this->message($data);

        }
    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }


}