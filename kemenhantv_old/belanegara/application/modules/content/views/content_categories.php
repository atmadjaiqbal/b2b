<div id="banner-area">
    <img src="<?php echo base_url(); ?>assets/images/slider1.jpg" alt ="" style="width: 100%;"/>
    <div class="parallax-overlay"></div>
</div>

<section id="main-container" class="portfolio-static" style="background-color: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h3 class="classic"><i class="fa fa-list-alt"></i><div class="title"></div><?php echo $menu_title;?></h3>
            </div>

            <?php
            if($list_content) {
                $crew_category_list = array(); $genre_list = array();$isFrist = true;

                foreach($list_content->product_list as $val) {
                    $menu_id = $val->menu_id;
                    $apps_id = $val->apps_id;
                    $product_id = $val->product_id;
                    $channel_category_id = $val->channel_category_id;
                    $channel_id = $val->channel_id;
                    $channel_type_id = $val->channel_type_id;
                    $content_id = $val->content_id;
                    $content_type = $val->content_type;
                    $title = $val->title;
                    $short_title = strlen($title) > 30 ? substr($title, 0, 30). '...' : $title;
                    $thumbnail = $val->thumbnail.'&w=200';
                    $description = $val->description;
                    $short_description = strlen($description) > 260 ? substr($description, 0, 260).'...' : $description;
                    $prod_year = $val->prod_year;
                    $video_duration = $val->video_duration;
                    $countlike = $val->countlike;
                    $countcomment = $val->countcomment;
                    $countviewer = $val->countviewer;
                    $icon_size = $val->icon_size;
                    $poster_size = $val->poster_size;
                    $crew_category_list = $val->crew_category_list;
                    $genre_list = $val->genre_list;
                    $related_list = $val->related_list;
                    if($val->crew_category_list) { $lecture = $val->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}

                    $col = $isFrist ? 'col-sm-6 col-xs-12' : 'col-sm-3 col-xs-6';
                    $height = $isFrist ? 'height: 220px;' : 'height: 100px;';
                    $p = $isFrist ? '' : 'font-size: 12px;';
                    $h = $isFrist ? 'h4' : 'h5';
                    $fontH = $isFrist ? '' : 'font-size: 12px;';
                    $short_title2 = $isFrist ? $short_title : strlen($title) > 10 ? substr($title, 0, 10). '...' : $title;
                    $short_description = $isFrist ? $short_description : substr($description, 0, 45). '...';
                    /*
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#like-<?php echo $content_id; ?>').click(function(e){
                                e.preventDefault();
                                var id = $(this).data('id');
                                $.post(Settings.base_url+'content/post_like/', {'content_id':id}, function(data){
                                    if(data.rcode != 'ok'){
                                        alert(data.msg);
                                    } else {
                                        liketotal('#totallike-<?php echo $content_id; ?>');
                                    }
                                });
                            });
                        });
                    </script>
                    */ ?>

                    <div class="<?php echo $col; ?> portfolio-static-item" style="padding-right: 0px;">
                        <a href="<?php echo base_url();?>content/content_detail/<?php echo $content_id; ?>/<?php echo $menu_id;?>/<?php echo urlencode($menu_title);?>">
                            <div class="grid">
                                <figure class="effect-oscar" style="float: left;width: 50%;<?php echo $height;?>">
                                    <img src="<?php echo $thumbnail;?>" alt="">
                                    <!--bottom image-->
                                    <img style='border:0;
                                        display:block;
                                        margin:auto;
                                        -moz-transform: scale(1,-1);
                                        -o-transform: scale(1,-1);
                                        -webkit-transform: scale(1,-1);
                                        transform:scale(1,-1);
                                        opacity:0.7;'
                                        src='<?php echo $thumbnail;?>' />
                                </figure>
                                <div class="portfolio-static-desc" style="float: left;width: 50%;overflow: hidden;<?php echo $height;?>">
                                    <<?php echo $h;?> style="<?php echo $fontH;?>margin: 0px;color: black;font-weight: bold;"><?php echo $short_title2;?></<?php echo $h;?>> 
                                    <p style="<?php echo $p;?>margin-bottom: 0px;"><i class="fa fa-eye"></i> &nbsp<?php echo $countviewer; ?> viewer<br>
                                    <?php echo $short_description;?></p>
                                </div>
                            </div>
                        </a>
                    </div><!--/ item 1 end -->

                    
                    <?php
                    $isFrist = false;
                }
                ?>
                <div class="clearfix"></div>
                <div id="content_container" data-page='1' data-categories="<?php echo $menu_id;?>"></div>
                <?php
            }
            ?>
        </div><!-- Content row end -->

        <div class="text-center" style="margin-bottom: 20px;">
            <a id="content_load_more" href="#" class="btn btn-primary solid">LOAD MORE</a>
        </div>
    </div><!-- Container end -->
</section>