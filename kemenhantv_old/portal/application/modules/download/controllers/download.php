<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends Application {

    public function __construct()
    {
        parent::__construct();
        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->library('download_lib');
        $this->load->library('home/home_lib');
        $this->load->library('content/content_lib');
        $this->load->library('breadcrumbs');
    }


    public function index(){
        // if(!$this->member_session()) { redirect('member/login');}
        $data = $this->data;
        $data['scripts'] = array('ibolz.content.js');
        $breadcrumb = array();
        $this->breadcrumbs->add('Download Apps','#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $data['headermenu'] = $this->home_lib->header_menu();
        $data['appdownload'] = $this->download_lib->list_apps_download();
        $html['html']['content']  = $this->load->view('download/download_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }


    public function install(){
        $data = $this->data;
        $breadcrumb = array();
        $this->breadcrumbs->add('Download Apps','#');
        $data['breadcrumb'] = $this->breadcrumbs->output();
        $data['headermenu'] = $this->home_lib->header_menu();
        $apps_upload_id = $this->input->get("apps_upload_id");
        $apps = $this->download_lib->download_detail($apps_upload_id);
        //$file = $apps->app_download->target_path . $apps->app_download->nfilename; //not public folder               
        $file = "/var/www/html/kemenhantv/portal/assets/apps/Kemenhan.apk";
        // echo $file . "<br/>"; exit;

        if (file_exists($file)) {
            // $this->download_lib->download_count($apps_upload_id, $apps->app_download->countdownload + 1);
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.android.package-archive');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }else{
            $data['notfound'] = "file not exist";
            $html['html']['content']  = $this->load->view('download/download_notfound', $data, true);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
            $this->load->view('template/tpl_one_column', $html, false);

        }
    }


    public function testing(){
        $apps_id = $this->input->get("apps_id");

        $data['apps_upload_list'] =  $this->apps_model->apps_upload_list($apps_id, 1);
        $this->load->view('apps_upload_list', $data);

    }


    public function demo(){
        $apps_id = $this->input->get("apps_id");

        $data['apps_upload_list'] =  $this->apps_model->apps_upload_list($apps_id, 2);
        $this->load->view('apps_upload_list', $data);

    }

    public function member_session() {
        if($this->session->userdata('email')) {
            return true;
        } else {
            return false;
        }
    }

}