<div id="latest">
    <div class="title-place">LATEST</div>
    <div class="content-place">
        <?php
        if($latest->product_list)
        {
            $last = 0; foreach($latest->product_list as $rowlast)
        {
            $last_menu_id = $rowlast->menu_id; $last_apps_id = $rowlast->apps_id;
            $last_product_id = $rowlast->product_id; $last_channel_category_id = $rowlast->channel_category_id;
            $last_channel_id = $rowlast->channel_id; $last_channel_type_id = $rowlast->channel_type_id;
            $last_content_id = $rowlast->content_id; $last_content_type = $rowlast->content_type;
            $last_title = $rowlast->title; $last_thumbnail = $rowlast->thumbnail. '&w=100';
            $last_description = $rowlast->description; $last_prod_year = $rowlast->prod_year;
            $last_video_duration = $rowlast->video_duration; $last_countlike = $rowlast->countlike;
            $last_countviewer = $rowlast->countviewer; $last_icon_size = $rowlast->icon_size;
            $last_poster_size = $rowlast->poster_size; $last_crew_category_list = $rowlast->crew_category_list;
            if($rowlast->crew_category_list) { $last_lecture = $rowlast->crew_category_list[0]->crew_list[0]->crew_name;} else { $last_lecture = '';}
            if($last == 0)
            {
                $last_shorttitle = strlen($last_title) > 15 ? substr($last_title, 0, 15).'...' : $last_title;
                ?>
                <div class="latest-item-1">
                    <div class="img-place">
                        <img src="<?php echo $last_thumbnail;?>">
                        <!-- div style="background-color:#EEEEEE;background: url(< ? php echo $last_thumbnail;?>) no-repeat scroll center rgba(0, 0, 0, 0);width:90px;height:90px;'"></div -->
                    </div>
                    <div class="content-place-1">
                        <h3><a href="<?php echo base_url();?>content/content_detail/<?php echo $last_content_id; ?>/<?php echo $last_menu_id;?>/<?php echo urlencode($menu_title);?>"><?php echo $last_shorttitle;?></a></h3>
                        <dl>
                            <dt>DATE</dt><dd>:&nbsp;&nbsp;<?php echo $last_prod_year;?></dd>
                            <dt>LECTURE</dt><dd>:&nbsp;&nbsp;<?php echo (strlen($last_lecture) > 15 ? substr($last_lecture, 0, 15) . '...' : $last_lecture);?></dd>
                        </dl>
                        <p class="sub-title">DESCRIPTION</p>
                        <p class="sub-description"><?php echo (strlen($last_description) > 70 ? substr($last_description, 0, 70).'...' : $last_description);?></p>
                    </div>

                </div>
            <?php
            } else {
                $last_shorttitle = strlen($last_title) > 30 ? substr($last_title, 0, 30).'...' : $last_title;
                ?>
                <div class="latest-item-2">
                    <div class="content-place-2">
                        <h3><a href="<?php echo base_url();?>content/content_detail/<?php echo $last_content_id; ?>/<?php echo $last_menu_id;?>/<?php echo urlencode($menu_title);?>"><?php echo $last_shorttitle;?></a></h3>
                        <p class="sub-lecture">LECTURE :&nbsp;&nbsp;<?php echo $last_lecture;?></p>
                        <p class="sub-lecture pull-right" style="margin-right: 20px;margin-top:-35px;"><strong><?php echo $last_countviewer;?></strong></p>
                    </div>
                </div>
            <?php
            }
            $last++;
        }
        }
        ?>
    </div>
</div>
