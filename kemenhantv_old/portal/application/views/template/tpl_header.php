<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    $_metadesc = 'Ibolz BNI.';
    ?>
    <meta name="description" content="<?php echo $_metadesc; ?>"/>
    <meta name="keywords" content="streaming, television, mobile, vod, video on demand, ibolz"/>
    <meta name="author" content="Ibolz Team" />
    <meta name="webcrawlers" content="all" />
    <meta name="rating" content="general" />
    <meta name="spiders" content="all" />
    <meta name="robots" content="index,follow">

    <meta name="google-site-verification" content="" />
    <meta charset="utf-8" />

    <!-- Open Graph Tag -->
    <meta property="og:title" content="Ibolz"/>
    <meta property="og:type" content="media"/>
    <meta property="og:url" content="http://www.ibolz.tv"/>
    <meta property="og:image" content=""/>
    <meta property="og:site_name" content="www.ibolz.tv"/>
    <meta property="og:description" content=""/>
    <!-- End Graph Tag --->

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" CONTENT="private, max-age=5400, pre-check=5400"/>
    <meta http-equiv="Expires" CONTENT="<?php echo date(DATE_RFC822,strtotime("1 day")); ?>"/>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/ibolz.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/magnific-popup.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/lazyload/jquery.lazyload.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/lazyload/jquery.scrollstop.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.imagesloaded.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js" ></script>
    <!-- alertify
    <script src="<?php echo base_url(); ?>assets/js/alertify/alertify.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/alertify/themes/alertify.core.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/alertify/themes/alertify.default.css" id="toggleCSS" />
    -->
    <!-- Jquery Validation -->
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validation/jquery.validationEngine.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validation/jquery.validationEngine-en.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jquery.validation/validationEngine.jquery.css" type="text/css" />
    <!-- script type="text/javascript" src="<?php echo base_url(); ?>assets/flash/iBolz/swfobject.js"></script -->

    <?php if (!empty($scripts)) : ?>
        <?php foreach ($scripts as $script) : ?>
            <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>


    <script type="text/javascript">
        var Settings = <?php
        if(str_replace("http://","",current_url()) == $_SERVER['SERVER_ADDR'])
        {
           $settings = array('base_url' => $_SERVER['SERVER_ADDR']);
        } else {
	       $settings = array('base_url' => base_url());
        }
	    echo json_encode($settings);
	    ?>;
    </script>
    <script type="text/javascript">if(jQuery(window).width()>1024){document.write("<"+"script src='<?php echo base_url(); ?>assets/js/jquery.preloader.js'></"+"script>");}	</script>
    <script type="text/javascript" language="javascript">
        /*     jQuery(window).load(function() {
         $x = $(window).width();
         if($x > 1024)
         {
         jQuery("#content .row").preloader();    }

         jQuery('.magnifier').touchTouch();
         jQuery('.spinner').animate({'opacity':0},1000,'easeOutCubic',function (){jQuery(this).css('display','none')});
         }); */
    </script>


    <!--[if lt IE 8]>
<!--    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>-->
    <!--[endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <!--<![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/docs.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ie.css" type="text/css" media="screen">
    <![endif]-->

</head>

<body>
<div class="header-wrapper">
   <header>
       <div class="nav-section">

           <div class="bni-logo">
               <img src="<?php echo base_url().'assets/images/logo_header.png';?>" class="top-logo">
           </div>

           <div class="bni-menu">
               <div class="top-menu pull-right">

                   <ul class="nav">
                       <?php if($this->session->userdata('display_name')): ?>
                       <li class="nav-login" style="width: 126px !important;">
                           <a title="My Account" href="<?php echo base_url() ?>member/memberupdate">
                           <i class="fa fa-user"> </i>
                           <?php
                           if(strlen($this->session->userdata('display_name')) > 12)
                           {
                               $_hiUser = substr($this->session->userdata('display_name'), 0, 12) . '...';
                           } else {
                               $_hiUser = $this->session->userdata('display_name');
                           }
                           echo strtoupper($_hiUser);
                           ?>
                           </a>
                       </li>
                       <li class="nav-signup">
                           <a title="Sign Out" href="<?php echo base_url() ?>member/signout">Log out</a>
                       </li>
                       <?php else: ?>
                       <!-- li class="nav-login">
                           <a title="Login to ONL BNI" href="< ?php echo base_url() ?>member/login">Log in</a>
                       </li -->

                       <?php endif; ?>
                       <!-- li class="nav-signup">
                           < ?php
                           $member = 0;
                           if($this->session->userdata('display_name')):
                           ?>
                           <a title="Sign Out" href="< ?php echo base_url() ?>member/signout">Signout</a>
                           < ?php else: ?>
                           <a title="Signup to seeme" href="< ?php echo base_url() ?>member/signup">Register</a>
                           < ?php endif;?>
                       </li -->
                   </ul>



               </div>

               <div class="clear ibolz-menu pull-right">
                  <ul class="nav">
                     <?php
                     if($this->session->userdata('display_name'))
                     {
                         $homelink = base_url().'home/index';
                     } else {
                         $homelink = base_url();
                     }
                     ?>
                     <li><a href="<?php echo $homelink;?>">HOME</a></li>
                     <li><a href="<?php echo base_url().'home/home_aboutus';?>">ABOUT US</a></li>
                     <?php // if($this->session->userdata('display_name')): ?>
                     <?php foreach($headermenu->menu_list as $val) { ?>
                     <li class="dropdown">
                        <a href="<?php echo base_url().$val->menu_id; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo strtoupper($val->title);?> <b class="caret"></b></a>
                        <?php if(count($val->sub_menu_list) > 0) { ?>
                        <ul class="dropdown-menu">
                            <?php foreach($val->sub_menu_list as $submenu) { ?>
                            <?php
                            $head_product_id = $submenu->product_id;
                            $head_menu_id = $submenu->menu_id;
                            $head_menu_type = $submenu->menu_type;
                            $head_title = $submenu->title;
                            $_link = '';
                            switch($submenu->menu_type)
                            {
                                case 'menu' :
                                    $_link = base_url().'content/content_categories_list/'.$head_menu_id.'/'.urlencode($submenu->title);
                                    break;
                                case 'channel' :
                                    $_link = base_url().'content/content_channel/'.$head_menu_id.'/'.$head_product_id;
                                    break;
                                default:
                                    $_link = base_url().'content/content_categories/'.$head_menu_id.'/'.urlencode($submenu->title);
                                    break;
                            }
                            ?>
                            <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($submenu->title);?></a></li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                     </li>
                     <?php } ?>
                     <?php // endif; ?>
                     <li><a href="<?php echo base_url().'download';?>">DOWNLOAD</a></li>
                     <li><a href="<?php echo base_url().'home/home_contactus';?>">CONTACT US</a></li>
                  </ul>
               </div>
           </div>

           <!-- div class="bni-logo46 pull-right">
               <a href="< ?php echo site_url('/'); ?>">
                   <img src="< ?php echo base_url().'assets/images/logobni46.png';?>" class="top-logo">
               </a>
           </div -->

       </div>

  </header>


    <div class="search-wrapper">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url();?>"><i class="fa fa-home fa-lg"></i></a></li>
            <?php echo (!empty($breadcrumb)? $breadcrumb : '');?>
            <?php if(!empty($msg)){ ?>
              <li>&nbsp;&nbsp;<?php echo $msg; ?></li>
            <?php } ?>
        </ul>

        <?php if($this->session->userdata('display_name')): ?>
        <div class="search-form pull-right">
            <div class="input-group" style="margin-top:5px;">
                <form style="margin:0px !important;" action='<?php echo base_url(); ?>content/search' method='post'>
                    <input type="text" name="term" placeholder="Search ..." value="" style="border:1px solid #ccc;border-radius:5px;padding-left:5px;">
                    <button class="btn btn-nobg site-color" type="submit"> <i class="fa fa-search"> </i> </button>
                </form>

            </div>
        </div>
        <?php endif; ?>

    </div>


</div>
