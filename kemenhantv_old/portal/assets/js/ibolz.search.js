var host = '';
$(function(){

    content_list('#content_container');
    $('#content_load_more').click(function(e){
        e.preventDefault();
        content_list('#content_container');
    });

    channel_list('#channel-places');
    $('#channel_load_more').click(function(e){
        e.preventDefault();
        channel_list('#channel-places');
    });

});


function content_list(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    console.log(Settings.base_url+'home/content_list/'+page);

    $.get(Settings.base_url+'home/content_list/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });

}

function channel_list(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    console.log(Settings.base_url+'home/channel_list/'+page);

    $.get(Settings.base_url+'home/channel_list/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });
}
