<div class="container" style="margin-top: 140px;">

    <div id="content-categories">
        <h3 class="slider-title">
            <span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i>
        </h3>
        <?php
        if($list_content)
        {
            $crew_category_list = array(); $genre_list = array();
        ?>
        <div id="channel-categories-list">
            <ul class="playlist">
        <?php
            foreach($list_content->channel_list as $val)
            {
                $product_id = $val->product_id;
                $apps_id = $val->apps_id;
                $menu_id = $val->menu_id;
                $parent_menu_id = $val->parent_menu_id;
                $menu_type = $val->menu_type;
                $channel_id = $val->channel_id;
                $channel_name = $val->channel_name;
                $alias = $val->alias;
                $thumbnail = $val->thumbnail . '&w=300';
                $description = $val->description;
                $status = $val->status;
                $countviewer = $val->countviewer;
                $link = $val->link;
                $created = $val->created;
                $tab_type = $val->tab_type;
                $show_tab_menu = $val->show_tab_menu;
                $show_dd_menu = $val->show_dd_menu;
                $active = $val->active;
                $count_product = $val->count_product;
                switch($menu_type)
                {
                    case 'menu' :
                        $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title);
                        break;
                    case 'channel' :
                        $_link = base_url().'content/content_channel/'.$menu_id.'/'.$channel_id;
                        break;
                    default:
                        $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                        break;
                }

        ?>
                <li>
                    <a href="<?php echo $_link;?>">
                        <div class="product-title">
                            <span class="pull-left"><?php echo $alias;?></span>
                            <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                        </div>
                        <div class="product-image">
                            <div class="preview_product related bgfull"
                                 style="background-color:#cecece;background: url('<?php echo $thumbnail;?>') no-repeat scroll center;width:270px;height:160px;"
                                 id="'<?php echo $menu_id;?>'-'<?php echo $product_id;?>'"
                                 data-id="'<?php echo $product_id;?>'" data-menu-id="'<?php echo $menu_id;?>'"></div>
                        </div>
                    </a>
                </li>
        <?php
            }
        ?>
           </ul>
        </div>

        <?php
        }
        ?>
    </div>
</div>
