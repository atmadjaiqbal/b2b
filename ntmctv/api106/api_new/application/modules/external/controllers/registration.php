<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 13 2014
	Registration controllers
*/
class Registration extends REST_Controller{
	
	public function __construct(){
		parent::__construct();
        $this->load->model(array('apps_m', 'customer_group_m', 'customer_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
	}	

    /**
    * Registration process based on mobile number   
    * method: POST
    * url: /registration
    * required parameters: apps_id, group_id, mobile_no, password, first_name, last_name
    */
    public function index_post(){        
        
        $app_id = $this->input->post("apps_id");
        $group_id = $this->input->post("group_id");
        // $email = $this->input->post("email");
        $mobile_no = $this->input->post("mobile_no");
        $password = $this->input->post("password");
        $first_name = $this->input->post("first_name");
        $last_name = $this->input->post("last_name");

        // checking application id
        if(empty($app_id)) $this->response_failed(lang('missing_app_id'));        
        $app = $this->apps_m->find($app_id);
        if(!$app) $this->response_failed(lang('error_app_id'));        

        // checking customer group id
        if(empty($group_id)) $this->response_failed(lang('missing_group_id'));
        $customerGroup = $this->customer_group_m->find($group_id);
        if(!$customerGroup) $this->response_failed(lang('error_group_id'));        

        if(empty($mobile_no)) $this->response_failed(lang('missing_mobile_no'));
        if(empty($password) || strlen($password) < 6) $this->response_failed(lang('missing_password'));        
        if(empty($first_name)) $this->response_failed(lang('missing_first_name'));
        if(empty($last_name)) $this->response_failed(lang('missing_last_name'));

        $display_name = $this->input->post("name");  
        if(empty($display_name)) $display_name = $first_name . ' ' . $last_name;

        $mobile_id = $this->input->post("mobile_id");
        $device_id = $this->input->post("device_id");        
        $longitude = $this->input->post("longitude");
        $latitude = $this->input->post("latitude"); 
        $version = $this->input->post("version");   
        $screen_size = $this->input->post("screen_size");       
        $remote_host = $_SERVER['REMOTE_ADDR'];
        if (empty($mobile_id))  $mobile_id= 'webversion_1';     
        if (empty($device_id))  $device_id= '1';        
        if (empty($longitude))  $longitude= '0';        
        if (empty($latitude))  $latitude= '0';
        $status = 1;
        $user_id = $mobile_no;
        $email = "{$mobile_no}@ibolz.tv";

        $allow_access = $this->input->post("allow_access"); // 0 or 1
        $allow_upload = $this->input->post("allow_upload"); // 0 or 1
        $allow_share = $this->input->post("allow_share"); // 0 or 1
        if ($allow_access == 'true') $allow_access = '1';       
        if ($allow_upload == 'true') $allow_upload = '1';       
        if ($allow_share == 'true') $allow_share = '1';         
        $allow_access = (int)$allow_access;
        $allow_upload = (int)$allow_upload;
        $allow_share = (int)$allow_share;

        $customer = $this->customer_m->get_account_by_mobile_no($mobile_no);
        if($customer){
            $this->response_failed(lang('error_existing_mobile_no'));
        }else{ 
            $customer = $this->customer_m->signup(
                $email, $mobile_id, $user_id, $password, 
                $first_name, $last_name, $display_name, 
                $status, $app_id, $device_id, 
                $longitude, $latitude, $mobile_no
            );
            if($customer){
                // do insert customer into member of group
                $this->customer_group_m->add_customer_member(
                    $group_id, $customer->customer_id,
                    $allow_access, $allow_upload, $allow_share
                );
                
                // statistic additional features
                // $this->statistic_model->apps_activity(
                //     'signup', $mobile_id, $app_id, $version, 
                //     $latitude, $longitude, $device_id, 
                //     $customer->customer_id, $screen_size, $remote_host, 
                //     $status, json_encode($customer)
                // );

                $customerGroups = $this->customer_group_m->get_member_of_group($app_id, $customer->customer_id);         
                $out = array(
                    'message' => lang('register_successed'),
                    'customer_id' => $customer->customer_id,
                    'groups' => $customerGroups

                );
                $this->response_success($out);
            }else{
                $this->response_failed(lang('register_failed'));
            }
        }               
    }

}