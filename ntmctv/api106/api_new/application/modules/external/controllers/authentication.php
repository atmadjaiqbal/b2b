<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 13 2014
	Authentication controllers
	Original copy from signup functions.
	Removing Email field, Change to Mobile Number	
*/

class Authentication extends REST_Controller{
	
	public function __construct(){
		parent::__construct();
        $this->load->model(array('apps_m', 'customer_group_m', 'customer_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
	}	

	/**
	* Login process based on mobile number or email	
	* method: POST
	* url: /login
	* required parameters: apps_id, email or mobile_no, password
	*/
	public function index_post(){
		$app_id = $this->input->post("apps_id");
		$email = $this->input->post("email");
	    $mobile_no = $this->input->post("mobile_no");
	    $password = $this->input->post("password");
		$longitude = $this->input->post("longitude");
		$latitude = $this->input->post("latitude");    

		// checking application id
	    if(empty($app_id)) $this->response_failed(lang('missing_app_id'));        
	    $app = $this->apps_m->find($app_id);
	    if(!$app) $this->response_failed(lang('error_app_id'));

	    // checking password
	    if(empty($password)) $this->response_failed(lang('missing_password'));        

	    // checking email and mobile_no
	    if(empty($email) && empty($mobile_no)){
	    	if(empty($password)) $this->response_failed(lang('missing_email_or_mobile_no'));        
	    }    
	    $user_id = $email;
	    if(empty($user_id)) $user_id = $mobile_no;        

		if (empty($longitude))  $longitude= '0';		
		if (empty($latitude))  $latitude= '0';	
	  	$customer = $this->customer_m->login($user_id, $password, $latitude, $longitude);		
		if(!$customer){			
			$this->response_failed(lang('error_login'));
		}else{
			$customerGroups = $this->customer_group_m->get_member_of_group($app_id, $customer->customer_id);			
		    
		 //    $version = $this->input->post("version");    
		 //    $device_id = $this->input->post("device_id");
		 //    $screen_size = $this->input->post("screen_size");
		 //    $mobile_id = $this->input->post("mobile_id");
		 //    $remote_host = $_SERVER['REMOTE_ADDR'];
		 //    $status = 1;		
			// $this->statistic_model->apps_activity(
			// 	'login', $mobile_id, $app_id, $version, 
			// 	$latitude, $longitude, 
			// 	$device_id, $customer->customer_id, $screen_size, $remote_host, 
			// 	$status, json_encode($customer)
			// );
			$out = array(
				'message' => lang('auth_successed'),
				'apps_id' => $app_id,
				'customer_id' => $customer->customer_id,
				'groups' => $customerGroups,
				'customer' => array(
					'customer_id' => $customer->customer_id,
			        // 'mobile_id' => $customer->mobile_id,
			        // 'apps_id' => $customer->apps_id,
			        // 'email' => $customer->email,
			        'mobile_no' => $customer->mobile_no,	
			        'user_id' => $customer->user_id,
			        'first_name' => $customer->first_name,
			        'last_name' => $customer->last_name,
			        'display_name' => $customer->display_name,
			        'last_login' => $customer->last_login,
			        'credit_balance' => $customer->credit_balance,			        
				),
				
			);
			$this->response_success($out);
		}
	}

}