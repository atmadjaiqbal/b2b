<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: jurigs bin ontohod
	Date: Jan, 30 2015
	Schedule Channel
*/

class Schedule extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model(array('schedule_m'));
        $this->load->helper('url');
        $this->lang->load('default');

        $this->apps_id = $this->input->post("apps_id");
        $this->lang = $this->input->post("lang");
        $this->customer_id = $this->input->post("customer_id");
        $this->device_id = $this->input->post("device_id");

        // checking params
        if(empty($this->apps_id)) $this->response_failed('missing_app_id');
        if(empty($this->lang)) $this->lang =  $this->config->item('lang');
        if(empty($this->customer_id)) $this->response_failed('missing_customer_id');
        if(empty($this->device_id)) $this->response_failed('missing_device_id');

    }


    public function today_post(){
        /*
        $channel_id = $this->input->post("channel_id");
        $schedule_type_id = $this->input->post("schedule_type_id");

        $vid_server = $this->channel_model->channel_server_cluster($channel_id);
        $rtmp_addr = $vid_server->public_ip;

        $payload = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/RtmpClient/channel_schedule.php?rtmp_addr=" . $rtmp_addr . "&channel_id=" . $channel_id . "&schedule_type_id=" . $schedule_type_id);

        $this->output->set_content_type('application/json');
        $this->output->set_output($payload);
        */

        date_default_timezone_set('Asia/Jakarta');

        $serverAddr = $_SERVER["HTTP_HOST"];
        $start_row = ($this->input->post("page") ? $this->input->post("page") : 1) - 1 ;
        $limit = $this->input->post("limit") ? $this->input->post("limit") : 10 ;

        $channel_id = $this->input->post("channel_id");
        $schedule_type_id = $this->input->post("schedule_type_id");
        $customer_id = $this->input->post("customer_id");

        $start_date = $this->input->post("start_date") ? $this->input->post("start_date") : date("Y-m-d H:i:s") ;
        $end_date = $this->input->post("end_date") ? $this->input->post("end_date") : date("Y-m-d H:i:s", strtotime ( '1 day', strtotime (date("Y-m-d H:i:s")))) ;

        $rows = $this->schedule_m->schedule_today($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id);
        $response = array(
            "schedule_list" => $rows
        );
        $this->response_success($response);
    }


    function record(){
        $apps_id = $this->input->post("apps_id");
        $channel_id = $this->input->post("channel_id");
        $schedule_id = $this->input->post("schedule_id");
        $customer_id = $this->input->post("customer_id");

        $this->schedule_model->record($apps_id, $channel_id, $schedule_id, $customer_id);
        $response = array(
            "rcode"=>"1",
            "message"=>"Successfully"
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));

    }


}