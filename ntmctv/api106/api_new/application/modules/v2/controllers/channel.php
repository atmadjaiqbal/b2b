<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: Jurigs Kehed Bin Ontohod
	Date: March, 14 2015
	Channels API
*/

class Channel extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model(array('product_m'));
        $this->load->model(array('channel_m'));
        $this->load->helper('url');
        $this->lang->load('default');

        $this->apps_id = $this->input->post("apps_id");
        $this->lang = $this->input->post("lang");
        $this->customer_id = $this->input->post("customer_id");
        $this->device_id = $this->input->post("device_id");

        // checking params
        if(empty($this->apps_id)) $this->response_failed('missing_app_id');
        if(empty($this->customer_id)) $this->response_failed('missing_customer_id');
        if(empty($this->device_id)) $this->response_failed('missing_device_id');
        if(empty($this->lang)) $this->lang =  'id_ID';

    }

    public function channel_detail_post()
    {
        $menu_id = $this->input->post("menu_id");
        $channel_id = $this->input->post("channel_id");
        if(empty($menu_id)) $menu_id = null;
        if(empty($channel_id)) $channel_id = null;
        $channel = $this->channel_m->channel_detail($this->apps_id, $menu_id, $channel_id);
        $this->response_success($channel);
    }

    public function channel_list_post()
    {
        $menu_id = $this->input->post("menu_id");
        $device_id = $this->input->post("device_id");
        if(empty($menu_id)) $menu_id = null;
        $channel_list = $this->channel_m->channel_list($this->apps_id, $menu_id);
        $response = array(
            "channel_list" => $channel_list
        );
        $this->response_success($response);
    }

    public function content_list_post()
    {
        $menu_id = $this->input->post("menu_id");
        $device_id = $this->input->post("device_id");
        if(empty($menu_id)) $menu_id = null;
        $content_list = $this->channel_m->content_list($this->apps_id, $menu_id);
        $response = array(
            "channel_list" => $content_list
        );
        $this->response_success($response);
    }


}
