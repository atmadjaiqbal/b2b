<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
    Author: Jurigs Kehed Bin Ontohod
	Date: March, 14 2015
	Channels Models
*/

class Channel_m extends MY_Model {

    protected $table 	= 'menu_product';
    protected $key		= 'menu_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function channel_detail($apps_id=null, $menu_id=null, $channel_id=null) {
        $where = "WHERE 1 = 1 ";
        $sql = "SELECT ch.channel_id, ch.channel_name, ch.alias, ch.thumbnail, ch.channel_descr AS 'description', ct.type_name,
                       ch.status, ch.countviewer, ch.order_number, ch.link, ch.created, ch.createdby, ch.`channel_type_id`, ap.`apps_id`, ap.`app_name`
                FROM channel ch
                INNER JOIN channel_type ct ON ch.`channel_type_id` = ct.`channel_type_id`
                INNER JOIN apps_channel ac ON ch.`channel_id` = ac.`channel_id`
                INNER JOIN apps ap ON ac.`apps_id` = ap.`apps_id`
                INNER JOIN menu mn ON ap.`apps_id` = mn.`apps_id` ";
        if($apps_id) { $where .= "AND ap.apps_id = '$apps_id' "; }
        if($menu_id) { $where .= "AND mn.menu_id = '$menu_id' "; }
        if($channel_id) { $where .= "AND ac.channel_id = '$channel_id' "; }
        $sql .= $where;
        $query = $this->db->query($sql);
        $row =  $query->row();
        $row->thumbnail  = $this->get_thumbnail_url($row->thumbnail) ."&w=170";
        return $row;
    }

    public function channel_list($apps_id=null, $menu_id=null)
    {
        $where = "WHERE 1 = 1 ";
        $sql = "SELECT b.product_id, a.apps_id, a.menu_id, a.parent_menu_id,  'channel' menu_type,
                       ch.channel_id, ch.channel_name, ch.alias, ch.thumbnail, ch.channel_descr AS 'description',
                       ch.status, ch.countviewer, ch.order_number, ch.link, ch.created, ch.createdby,'0' tab_type, '0' show_tab_menu,
                       '0' show_dd_menu, a.active, '1' count_product, d.icon_size, d.poster_size, a.created
                FROM menu a
                INNER JOIN menu_product b ON a.menu_id  = b.menu_id
                INNER JOIN channel ch ON b.product_id = ch.channel_id
                INNER JOIN apps d ON a.apps_id = d.apps_id ";
        if($apps_id) { $where .= "AND a.apps_id = '$apps_id' "; }
        if($menu_id) { $where .= "AND a.menu_id = '$menu_id' "; }
        $sql .= $where;
        $sql .= "ORDER BY a.order_number, b.order_number";
        $query = $this->db->query($sql);
        $rows =  $query->result();
        foreach($rows as $row){
            $row->thumbnail  = $this->get_thumbnail_url($row->thumbnail) ."&w=170";
        }
        return $rows;
    }

    public function content_list($apps_id=null, $menu_id=null)
    {
        $where = "WHERE 1 = 1 ";
        $sql = "SELECT b.product_id, a.apps_id, a.menu_id, a.parent_menu_id, 'menu' menu_type,  a.title AS 'alias', a.thumbnail, a.link , a.tab_type,
                       ct.content_id AS 'channel_id', ct.title AS 'channel_name', ct.description AS 'description', ct.status, ct.countviewer, ct.created,
                       a.show_tab_menu, a.show_dd_menu, a.active, COUNT(b.menu_id) count_product, c.icon_size, c.poster_size, a.created
                FROM menu a
                LEFT  JOIN menu_product b ON a.menu_id = b.menu_id
                INNER JOIN content ct ON b.product_id = ct.content_id
                INNER JOIN apps c ON a.apps_id = c.apps_id ";
        if($apps_id) { $where .= "AND a.apps_id = '$apps_id' "; }
        if($menu_id) { $where .= "AND parent_menu_id = '$menu_id' "; }
        $sql .= $where;
        $sql .= "GROUP BY a.menu_id ORDER BY a.order_number, b.order_number";
        $query = $this->db->query($sql);
        $rows =  $query->result();
        foreach($rows as $row){
            $row->thumbnail  = $this->get_thumbnail_url($row->thumbnail) ."&w=170";
        }
        return $rows;
    }

}