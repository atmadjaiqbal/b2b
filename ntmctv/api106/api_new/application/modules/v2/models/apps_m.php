<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Author: htridianto
	Date: nov, 13 2014 22:20 
*/
class Apps_m extends MY_Model {

protected $table 	= 'apps';
	protected $key		= 'apps_id';

	public function __construct()
	{
		parent::__construct();
	}


	public function auth($apps_id, $device_id, $version, $customer_id) {	
		$sql = "
			SELECT 
				a.apps_id, app_name, thumbnail, descr, version, status, message,download_page,  iscom, product_id, subs_url, icon_size, font_size, time_trial
			FROM apps a, apps_version b
			WHERE a.apps_id = b.apps_id
			 	AND a.apps_id = '$apps_id'
			 	AND version = '$version'
		 	";
		$query = $this->db->query($sql);	
		$row = $query->row();
		
		if($row){
			$row->thumbnail = $this->get_thumbnail_url($row->thumbnail .'&w='.$row->icon_size);
		}
		return $row;
	}	
	
	public function apps_mobile_id_save($apps_id, $version, $mobile_id, $device_id, $screen_size, $remote_host, $latitude, $longitude){
		$sql = "
			INSERT INTO apps_mobile_id (
				apps_id, version, mobile_id, created, device_id, screen_size, remote_host, latitude, longitude
			) VALUES (
				'$apps_id', '$version', '$mobile_id', now(), '$device_id', '$screen_size', '$remote_host', '$latitude', '$longitude'
			)
		 	";
		$query = $this->db->query($sql);	
	}	
	
	/*
	public function menu_list($apps_id, $parent_menu_id, $customer_id, $lang){
		$sql_title = ($lang == "zh_CN") ? " title_chn as title " : " title ";
		
		$sql = "
			SELECT 
				menu_id, $sql_title, thumbnail, link , tab_type, show_tab_menu, show_dd_menu
			FROM menu
			WHERE active = '1'
				AND apps_id = '$apps_id'
				AND (parent_menu_id is null  or parent_menu_id ='$parent_menu_id')
				
			ORDER BY order_number, title
		 	";
		$query = $this->db->query($sql);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = $this->config->item('thumbnail_url') .  $row->thumbnail;
			}else{
				$row->thumbnail = $this->config->item('thumbnail_url') .  $this->config->item('img_not_found') ;
			}
			$row->sub_menu_list = $this->menu_list($apps_id, $row->menu_id, $customer_id, $lang);
		}
		return $rows;
	}
	*/
	
	public function menu_list($apps_id, $menu_id='0',$menu_type='menu'){
		//$menu_type = ($menu_id =='0') ? 'menu' : 'product';
		
		if ($menu_type =='menu') {
			$textquery = "
				SELECT 
						a.apps_id, a.menu_id, 'menu' menu_type,  a.title, a.thumbnail, a.link , a.tab_type, a.show_tab_menu, a.show_dd_menu
				FROM menu a
				LEFT  JOIN menu_product b on a.menu_id = b.menu_id 
				WHERE  a.apps_id ='$apps_id' 
					AND parent_menu_id = '$menu_id' 
				GROUP BY a.menu_id
			 ";
		}  else {
				$textquery = "
					SELECT 
						a.apps_id, a.menu_id, 'channel' menu_type,  c.title, c.video_thumbnail thumbnail,  c.message_url  link , 
						'0' tab_type, '0' show_tab_menu, '0' show_dd_menu
					FROM menu a
					INNER JOIN menu_product b on a.menu_id  = b.menu_id 
					INNER JOIN content c on b.product_id = c.content_id  and c.status='1'
					WHERE  a.apps_id ='$apps_id' 
						AND a.menu_id = '$menu_id' 
				 ";
		}

		$rows = $this->db->query($textquery)->result();	
	  $results    = array();
    foreach($rows as $row) {   
    	$sub_menu = array();
    	if ($menu_type == 'menu') {
				$sub_menu = $this->menu_list($apps_id, $row->menu_id, 'menu');    		
				$sub_menu_channel = $this->menu_list($apps_id, $row->menu_id, 'channel');    		
				$sub_menu = array_merge($sub_menu, $sub_menu_channel);					
    	}

    	$row->sub_menu_list  = $sub_menu;
			$row->thumbnail = $this->get_thumbnail_url($row->thumbnail) ."?w=50";
    	$results[] = $row;
    }        
    return $results;		
	}	


	public function apps_category_list($apps_id, $customer_id, $lang){
		$sql_name = ($lang == "zh_CN") ? " category_name_chn as category_name " : " category_name ";
		
		$sql = "
			SELECT 
				b.channel_category_id, $sql_name, thumbnail, b.tab_type, b.link, show_tab_menu, show_dd_menu
			FROM apps_category a, channel_category b
			WHERE apps_id = '$apps_id'
			AND a.channel_category_id = b.channel_category_id
			ORDER BY a.order_number, category_name
		 	";
			
		
		$query = $this->db->query($sql);	
		$rows =  $query->result();
		foreach($rows as $row){
			$row->thumbnail = $this->get_thumbnail_url($row->thumbnail);

			$row->channel_list = $this->apps_channel_category_list($apps_id, $row->channel_category_id, $customer_id, $customer_id);
			if ($row->channel_category_id == '-2') {
				$row->channel_list = $this->apps_groups_list($apps_id, $row->channel_category_id);				
			}
		}
		return $rows;
	}	
	
	public function apps_groups_list($apps_id, $channel_category_id ){
		$sql = "
			SELECT 
				b.groups_id channel_id, b.groups_name channel_name, b.groups_name  alias, 
				(SELECT thumbnail from channel_category  where channel_category_id = '$channel_category_id') thumbnail, '1' status, '3' channel_type_id, '3' channel_category_id, '0' order_number, now() as server_time, '' link
			FROM groups_apps a
				INNER JOIN groups b on a.groups_id = b.groups_id
			WHERE a.apps_id = '$apps_id'
			ORDER BY b.groups_name
		 	";
			
		$query = $this->db->query($sql);	
		$rows =  $query->result();
		foreach($rows as $row){
			$row->thumbnail = $this->get_thumbnail_url($row->thumbnail);
		}
		return $rows;
	}


	public function apps_channel_category_list($apps_id, $channel_category_id, $customer_id){
		$textquery = "
			SELECT 
				a.channel_id, channel_name, alias, thumbnail, status, channel_type_id, a.channel_category_id, d.order_number, now() as server_time, d.link
			FROM apps_channel_category a, apps_channel b, apps_category c, channel d
			WHERE a.apps_id = '$apps_id'
			AND a.channel_category_id = '$channel_category_id'
			AND d.status = 1
			AND a.apps_id = b.apps_id
			AND a.apps_id = c.apps_id
			AND a.channel_id = b.channel_id
			AND a.channel_category_id = c.channel_category_id
			AND a.channel_id = d.channel_id
		 	";
			
			
		if(!empty($customer_id) && $channel_category_id == '-1'){
			$textquery = $textquery . "
				UNION ALL
				SELECT 
					a.channel_id, channel_name, alias, thumbnail, status, channel_type_id, '-1' as channel_category_id, b.order_number, now() as server_time, b.link
				FROM 
				channel_favorite a, channel b
				WHERE a.channel_id = b.channel_id
				AND a.apps_id = '$apps_id'
				AND a.customer_id = '$customer_id'
				";
		}
		$textquery = $textquery . " ORDER BY order_number, channel_name ";
		
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			$row->thumbnail = $this->get_thumbnail_url($row->thumbnail);
		}
		return $rows;
	}

	public function apps_schedule_type_list($apps_id, $lang){
		$sql_name = ($lang == "zh_CN") ? " schedule_type_name_chn as schedule_type_name " : " schedule_type_name ";

		$sql = "
			SELECT 
				b.schedule_type_id, schedule_type_name, thumbnail
			FROM apps_schedule_type a, schedule_type b
			WHERE apps_id = '$apps_id'
			AND a.schedule_type_id = b.schedule_type_id
			ORDER BY schedule_type_id
		 	";

		$query = $this->db->query($sql);	
		$rows =  $query->result();
		foreach($rows as $row){
			$row->thumbnail = $this->get_thumbnail_url($row->thumbnail);
		}
		return $rows;
	}		
	
	public function apps_feature_list($apps_id){
		$sql = "
			SELECT 
				b.feature_id
			FROM apps_feature a, feature b
			WHERE apps_id = '$apps_id'
			AND a.feature_id = b.feature_id
		 	";
		$query = $this->db->query($sql);	
		$rows =  $query->result();
		return $rows;
	}	
	
	
    public function apps_upload_list($apps_id, $apps_status){
        $textquery = "
			SELECT
				apps_upload_id, a.apps_id, b.app_name, device_id, version, a.descr, upload_date, ofilename, nfilename, target_path, apps_status, countdownload
			FROM apps_upload a, apps b
			WHERE a.apps_id = b.apps_id
			AND apps_status = $apps_status
			ORDER BY upload_date desc
		 	";

        if($apps_id){
            $textquery = "
				SELECT
					apps_upload_id, a.apps_id, b.app_name, device_id, version, a.descr, upload_date, ofilename, nfilename, target_path, apps_status, countdownload
				FROM apps_upload a, apps b
				WHERE a.apps_id = b.apps_id
				AND a.apps_id = '$apps_id'
				AND apps_status = $apps_status
				ORDER BY upload_date desc
			 	";
        }

        $query = $this->db->query($textquery);
        $rows = $query->result();
        return $rows;
    }

    public function update_apps_download($apps_upload_id, $countdownload){
        $textquery = "
			UPDATE apps_upload
				SET countdownload = $countdownload
			WHERE apps_upload_id = '$apps_upload_id'
		 	";

        $query = $this->db->query($textquery);
    }
		
    public function apps_upload_detail($apps_upload_id){
        $textquery = "
			SELECT
				apps_upload_id, a.apps_id, b.app_name, device_id, version, a.descr, upload_date, ofilename, nfilename, target_path, apps_status, countdownload
			FROM apps_upload a, apps b
			WHERE a.apps_id = b.apps_id
			AND a.apps_upload_id = '$apps_upload_id'
		 	";

        $query = $this->db->query($textquery);
        $row = $query->row();
        return $row;
    }					
	
}