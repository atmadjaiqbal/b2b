<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Package_model extends MY_Model {
	
	public function package_list($start_row, $limit, $package_type) {		
		$textquery = 
			" SELECT package_id, package_nm, package_desc, package_img, package_rate 
			, package_curency, interval_value, interval_unit
			FROM t_package ";
		if(!empty($package_type)){
			$textquery .= " WHERE package_type IN ('".$package_type."') ";
		}
		if(!empty($start_row) && !empty($limit)){
			$textquery .= " LIMIT " . $start_row . ", " .$limit;
		}

		$query = $this->db->query($textquery);	
		return $query->result();
	}

	public function package_list_userid($start_row=0, $limit=0, $user_id="", $package_id="", $package_type=""){
		$query = "	SELECT p.*, tp.`customer_id` FROM
					t_package p LEFT JOIN `t_user_package` tp ON tp.`package_id` = p.package_id 
					AND tp.`customer_id` = '" . $user_id . "' ";
		if(!empty($package_type)){
			$query .= " WHERE p.package_type IN ('".$package_type."') ";
		}
		if(!empty($package_id)){
			$query .= " AND p.package_id = '".$package_id."' ";
		}
		if($limit != 0){
			$query .= " LIMIT " . $limit . " OFFSET " . $start_row;
		}
		$data = $this->db->query($query);
		return $data;

	}
	
	public function get_package($package_id) {		
		$textquery = 
			" SELECT package_id, package_nm, package_desc, package_img, package_rate 
			, package_curency, interval_value, interval_unit
			FROM t_package ";
		if(!empty($package_id)){
			$textquery .= " WHERE package_id = '".$package_id."'";
		}

		$query = $this->db->query($textquery);	
		return $query->row();
	}

	
	public function package_channel($package_id) {		
		$textquery = 
			" SELECT a.package_id, a.package_nm
			, b.channel_id, c.channel_name as channel_nm, c.channel_descr as channel_desc, 
			c.thumbnail as channel_img
			FROM t_package a JOIN t_package_channel b ON (a.package_id = b.package_id)
			JOIN channel c ON (b.channel_id = c.channel_id) 
			WHERE a.package_id = '".$package_id."'";
		$query = $this->db->query($textquery);	
		return $query->result();
	}

	
	function check_valid_voucher($voucher_no, $voucher_type) {
		$textquery = "SELECT voucher_id, voucher_no, voucher_curency, voucher_value, voucher_status, voucher_type
					FROM t_voucher
					WHERE voucher_status = 1 
					AND voucher_no = '".$voucher_no."' AND voucher_type = '".$voucher_type."' ";
		$query = $this->db->query($textquery);	
		return $query->row();
	}

	function submit_topup($datas) {
		if (empty($datas)) return false;
		$textquery = "	INSERT INTO t_topup (topup_id, customer_id, voucher_type, topup_date, value_of_money, voucher_no)
						VALUES
						( '".$datas['topup_id']."', '".$datas['user_id']."', '".$datas['voucher_type']."', 
						now(), '".$datas['value_of_money']."', '".$datas['voucher_no']."' ) ";
		$query = $this->db->query($textquery);	
		return false;
	}

	function update_voucher($voucher_no) {
		
		$textquery = "UPDATE t_voucher SET voucher_status = 2
					WHERE voucher_no = '".$voucher_no."' ";
		$query = $this->db->query($textquery);	
		return false;
	}
	
	function submit_balance($datas) {
		if (empty($datas)) return false;
		$textquery = "	INSERT INTO t_user_balance (transaction_id, customer_id, transaction_type, transaction_date, 
						value_of_money, transaction_reference)
						VALUES
						( '".$datas['transaction_id']."', '".$datas['user_id']."', '".$datas['transaction_type']."', 
						now(), '".$datas['value_of_money']."', '".$datas['transaction_reference']."' ) ";
		$query = $this->db->query($textquery);	
		return false;
	}
	
	public function getUserDetail($user_id) {		
		$textquery = 
			' select '. 
			' customer_id as user_id, mobile_id, display_name as user_name, email, '. 
			' password as passwd, credit_balance '.
			' FROM customer '.
	  		' where customer_id = "'. $user_id . '"';
		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	function update_balance($user_id, $credit_balance) {
		if(empty($credit_balance)) $credit_balance = 0;
		$textquery = "UPDATE customer SET credit_balance = '".$credit_balance."'
					WHERE customer_id = '".$user_id."' ";
		$query = $this->db->query($textquery);	
		return false;
	}
	
	function submit_package($datas) {
		if (empty($datas)) return false;
		$textquery = "	INSERT INTO t_user_package (submit_id, customer_id, package_id, submit_date, 
						expired_date, payment_method, package_paid)
						VALUES
						( '".$datas['submit_id']."', '".$datas['user_id']."', '".$datas['package_id']."', '".$datas['submit_date']."', 
						'".$datas['expired_date']."', '".$datas['payment_method']."', '".$datas['package_paid']."' ) ";
		$query = $this->db->query($textquery);	
		return false;
	}

	function check_valid_package($user_id, $package_id) {
		$textquery = "SELECT a.customer_id as user_id, b.package_id
					FROM t_user_package a JOIN t_package b ON (a.package_id = b.package_id) ";
		if(!empty($user_id) && !empty($package_id)){
			$textquery .= " WHERE a.customer_id = '".$user_id."' AND a.package_id = '".$package_id."' ";
		}
		$textquery .= " AND now() <= a.expired_date ";
		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	function check_valid_channel($user_id, $channel_id) {
		$textquery = "SELECT a.customer_id, b.package_id, c.channel_id 
					FROM t_user_package a JOIN t_package b ON (a.package_id = b.package_id)
					JOIN t_package_channel c ON (b.package_id = c.package_id) ";
		if(!empty($user_id) && !empty($channel_id)){
			$textquery .= " WHERE a.customer_id = '".$user_id."' AND c.channel_id = '".$channel_id."' ";
		}
		$textquery .= " AND now() <= a.expired_date ";
		$query = $this->db->query($textquery);	
		return $query->row();
	}

	public function package_list_user($user_id='', $select="*", $where=array(), $where_in=array(), $limit=0, $offset=0)
	{
		$this->db->select($select);
		$this->db->from('t_package p');
		$this->db->join("t_user_package tp", "tp.package_id = p.package_id AND tp.customer_id = '" . $user_id . "'", 'left');
		//$this->db->where($where);
		$this->db->where_in("p.package_type", $where_in );

		if($limit != 0){
			$this->db->limit($limit, $offset);
		}
		
		$data = $this->db->get();
		
		return $data;
	}

	public function get_channel($select="tc.*", $where=array(), $limit=0, $offset=0)
	{
		$this->db->select($select);
		$this->db->from('channel tc');
		$this->db->join('t_package_channel tpc', 'tpc.channel_id = tc.channel_id', 'left');
		$this->db->where($where);
		if($limit != 0){
			$this->db->limit($limit, $offset);
		}
		$data = $this->db->get();
		return $data;
	}
	
	public function get_vod($select="tc.*", $where=array(), $channel_id="", $apps_id="", $limit=0, $offset=0)
	{
		$this->db->select($select);
		$this->db->from('program_vod tc');
		$this->db->join('t_package_vod tpc', 'tpc.program_id = tc.program_id', 'left');
		$this->db->where($where);
		if(!empty($channel_id)){
			$this->db->where("tc.channel_id", $channel_id);
		}
		if(!empty($apps_id)){
			$this->db->where("tc.apps_id", $apps_id);
		}
		if($limit != 0){
			$this->db->limit($limit, $offset);
		}
		$data = $this->db->get();
		return $data;
	}
	
}

?>