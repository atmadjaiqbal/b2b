<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends MY_Model {
	

	public function payment_type_list(){
		$textquery = "
			SELECT 
				payment_type_id, payment_name, type_id
  			FROM payment_type
			";
  		$query = $this->db->query($textquery);	
		return $query->result();
	}
	
	
	public function voucher_detail($voucher_number){
		$textquery = "
			SELECT 
				voucher_number, amount
  			FROM voucher_dummy
			WHERE voucher_number = '$voucher_number'
			";
  		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	
	public function customer_balance_save($customer_id, $amount){
		$textquery = "
			SELECT 
				customer_id, balance
  			FROM customer_balance
			WHERE customer_id = '$customer_id'
			";
  		$query = $this->db->query($textquery);	
		$customer_balance = $query->row();
		if($customer_balance){
			$textquery = "
				UPDATE customer_balance
				SET balance = balance + $amount
				WHERE customer_id = '$customer_id'
			 	";
			$query = $this->db->query($textquery);	
		}else{
			$textquery = "
				INSERT INTO customer_balance (customer_id, balance) 
				VALUE ('$customer_id', $amount)
			 	";
			$query = $this->db->query($textquery);	
		}
		
		return $this->customer_balance_detail($customer_id);
	}
	
	
	public function customer_balance_detail($customer_id){
		$textquery = "
			SELECT 
				customer_id, balance
  			FROM customer_balance
			WHERE customer_id = '$customer_id'
			";
  		$query = $this->db->query($textquery);	
		return $query->row();
	}
	

}

?>