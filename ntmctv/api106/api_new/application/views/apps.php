<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Apps extends CI_Controller{
	
	function Apps(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('apps_model');
		$this->load->model('statistic_model');
		$this->load->helper('string');
        $this->output->set_content_type('application/json');
	}	
	
	
    function auth(){
        $apps_id = $this->input->post("apps_id");
		$lang = $this->input->post("lang");
        $version = $this->input->post("version");
        $customer_id = $this->input->post("customer_id");
        $latitude = $this->input->post("latitude");
        $longitude = $this->input->post("longitude");
        $device_id = $this->input->post("device_id");
        $screen_size = $this->input->post("screen_size");
        $remote_host = $_SERVER['REMOTE_ADDR'];
		
        $mobile_id = $this->input->post("mobile_id");
		if($mobile_id == ""){
			$mobile_id = substr(str_shuffle(MD5(microtime())), 0, 10);
			$this->apps_model->apps_mobile_id_save($apps_id, $version, $mobile_id, $device_id, $screen_size, $remote_host, $latitude, $longitude);
		}
		
        $status = 0;
        $message = '';
		
    	$apps = $this->apps_model->auth($apps_id, $version, $customer_id);
		
		if(!$apps){
	        $status = 0;
	        $message = "auth;Invalid Application ID";
			$apps = array(
				"status" => $status,
				"message" => $message
			);
		}else{
	        $status = 1;
	        $message = "auth;OK";
			$apps->category_list = $this->apps_model->apps_category_list($apps_id, $customer_id, $lang);
			$apps->schedule_type_list = $this->apps_model->apps_schedule_type_list($apps_id, $lang);
			$apps->feature_list = $this->apps_model->apps_feature_list($apps_id);
			$apps->mobile_id = $mobile_id;
		}

		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($apps));
		
		
		$this->statistic_model->apps_activity('auth', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($apps));
    }
	

    function apps_detail(){
        $apps_id = $this->input->get("apps_id");
        $version = $this->input->get("version");
        
    	$apps = $this->apps_model->auth($apps_id, $version);
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($apps));
    }
	
	
	function test(){
		//echo random_string('alnum',10);
		echo substr(str_shuffle(MD5(microtime())), 0, 10);
	}
	

}
