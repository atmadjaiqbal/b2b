<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Supported Languages
|--------------------------------------------------------------------------
|
| Contains all languages your site will store data in. Other languages can
| still be displayed via language files, thats totally different.
|
| Check for HTML equivilents for characters such as � with the URL below:
|    http://htmlhelp.com/reference/html40/entities/latin1.html
|
|
|    array('en'=> 'English', 'fr'=> 'French', 'de'=> 'German')
|
*/
$config['header_languages'] = array(
    'en' => 'en', 'english' => 'en', 'en-us' => 'en',
    'cn' => 'cn', 'chinese' => 'cn', 'zh-cn' => 'cn',
    'id' => 'id', 'indonesian' => 'id', 'id-id' => 'id'
);

$config['supported_languages'] = array(
    'en' => array(
        'name'        => 'English',
        'folder'    => 'english',
        'direction'    => 'ltr',
        'codes'        => array('en', 'english', 'en_US')
    ),
    'cn' => array(
        'name'        => '简体中文',
        'folder'    => 'chinese',
        'direction'    => 'ltr',
        'codes'        => array('cn', 'chinese', 'zh_CN')
    ),
    'id' => array(
        'name'        => 'Bahasa Indonesia',
        'folder'    => 'indonesian',
        'direction'    => 'ltr',
        'codes'        => array('id', 'indonesian' ,'id_ID')
    )
);
