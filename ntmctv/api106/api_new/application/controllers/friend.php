<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Friend extends CI_Controller{
	
	function Friend(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('friend_model');
        $this->output->set_content_type('application/json');
	}	
	
	
	function friend_list(){
		$friend_list = $this->friend_model->friend_list();
		$response = array(
        	"friend_list" => $friend_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	

	


    
}
