<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends CI_Controller{
	
	function Package(){
		parent::__construct();
		$this->load->model('package_model');
		$this->load->library('Uuid.php');
	}	
	
	function package_list(){
        $start_row = $this->input->get("start_row") ? $this->input->get("start_row") : 0 ;
		$limit = $this->input->get("limit") ? $this->input->get("limit") : 10 ;
        $user_id = $this->input->get("id") ? $this->input->get("id") : '' ;
		$package_type = $this->input->get("package_type") ? $this->input->get("package_type") : '' ;
		
		//Just for vod
		$channel_id = $this->input->get("channel_id") ? $this->input->get("channel_id") : '' ;
		$apps_id = $this->input->get("apps_id") ? $this->input->get("apps_id") : '' ;

		$output = array(
			'rcode' => 'OK'
		);
		
		$select = "p.*, tp.customer_id";
		$where = array();
		$where_in = explode(",", $package_type);
		$where['customer_id'] = $user_id;

		$result = array();
		$rows = $this->package_model->package_list_user($user_id, $select, $where, $where_in, $limit, $start_row);
		//die("size : ".sizeof($rows));
		foreach ($rows->result_array() as $key => $value) {
			$where = array('tpc.package_id' => $value['package_id']);
			if (in_array(0, $where_in) || in_array(1, $where_in)) {
				$value['channel'] = $this->package_model->get_channel($select="tc.*", $where)->result_array();
			}else if(in_array(2, $where_in) || in_array(3, $where_in)){
				$value['vod'] = $this->package_model->get_vod($select="tc.*", $where, $channel_id, $apps_id)->result_array();
			}
			$result[$key] = $value;
		}

		if(count($result) == 0){
			$output['rcode'] = "FAILED";
			$output['message'] = "Data not found.";
		}else{
			$output['package_list'] = $result;
		}
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
        
	function package_detail(){
        $package_id = $this->input->get("package_id") ;
		$output = array(
			'rcode' => 'OK'
		);    	
        $package = $this->package_model->get_package($package_id);
		if(sizeof($package) > 0){
			$output['package_data'] = $package;
		}else{
			$output['rcode'] = "FAILED";
			$output['message'] = "Data not found.";
		}        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
    
 	function package_channel(){
        $package_id = $this->input->get("package_id") ;
		$output = array(
			'rcode' => 'OK'
		);
		
    	$rows = $this->package_model->package_channel($package_id);
		if(sizeof($rows) > 0){
			$output['channel_list'] = $rows;
		}else{
			$output['rcode'] = "FAILED";
			$output['message'] = "Data not found.";
		}     
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }   
	
	function submit_topup(){
		$output = array(
			'rcode' => 'OK'
		);	
		
		$voucher_type = $this->input->post("voucher_type");
		$voucher_no = $this->input->post("voucher_no");

		$voucher = $this->package_model->check_valid_voucher($voucher_no, $voucher_type);
		
		if(!empty($voucher)){
			$output['voucherData'] = $voucher;
			$user_id = $this->input->post("user_id");
			$topup_id = str_replace(array('-','{','}'), '', $this->uuid->v4());
			$transaction_id = str_replace(array('-','{','}'), '', $this->uuid->v4());
			if ( !empty($user_id) && !empty($voucher_no) ){
				$data = array(
					'topup_id' => $topup_id,
					'transaction_id' => $transaction_id,
					'user_id' => $user_id,
					'voucher_type' => $voucher_type,
					'transaction_type' => 1,
					'value_of_money' => $voucher->voucher_value,
					'voucher_no' => $voucher_no,
					'transaction_reference' => $voucher_no
				);
				
				$this->package_model->submit_topup($data);
				$this->package_model->update_voucher($voucher_no);
				$this->package_model->submit_balance($data);
				
				$user = $this->package_model->getUserDetail($user_id);
				$this->package_model->update_balance($user_id, $user->credit_balance + $voucher->voucher_value);
				$output['TopupData'] = $data;
				
			}else{
				$output['rcode'] = 'FAILED';
				$output['message'] = 'Invalid or empty User.';
			}
		}else{
			$output['rcode'] = "FAILED";
			$output['message'] = "Voucher not valid.";
		} 		
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
	
	function submit_package(){
		$output = array(
			'rcode' => 'OK'
		);	
		$user_id = $this->input->post("user_id");
		$package_id = $this->input->post("package_id");
		$package_paid = $this->input->post("package_paid");
		$payment_method = $this->input->post("payment_method");
		$submit_id = str_replace(array('-','{','}'), '', $this->uuid->v4());
		$transaction_id = str_replace(array('-','{','}'), '', $this->uuid->v4());
		
		if ( !empty($user_id) && !empty($package_id) ){
			$today = time();
			$twoMonthsLater = strtotime("+2 months", $today);
			$data = array(
				'submit_id' => $submit_id,
				'transaction_id' => $transaction_id,
				'user_id' => $user_id,
				'package_id' => $package_id,
				'package_paid' => $package_paid,
				'value_of_money' => $package_paid,
				'payment_method' => $payment_method,
				'transaction_type' => 0,
				'transaction_reference' => $submit_id,
				'submit_date' => mysql_datetime(),
				'expired_date' => mysql_datetime($twoMonthsLater)
			);
			
			$user = $this->package_model->getUserDetail($user_id);
			if(!empty($user->credit_balance) && $user->credit_balance >= $package_paid){
				// $this->package_model->submit_package($data);
				// $this->package_model->submit_balance($data);
				
				// $this->package_model->update_balance($user_id, $user->credit_balance - $package_paid);
				$package_detail = $this->package_model->package_list_userid(0, 0, $user_id, $package_id);
				$output['package_type'] = $package_detail->row_array();
			}else{
				$output['rcode'] = 'FAILED';
				$output['message'] = 'There is not enough credit on your Ibolz account';				
			}		
		}else{
			$output['rcode'] = 'FAILED';
			$output['message'] = 'Invalid or empty User or Package';
		}
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
	
	function check_valid_package(){
        $user_id = $this->input->get("user_id") ;
		$package_id = $this->input->get("package_id") ;
		$output = array(
			'rcode' => 'OK'
		);    	
        $package = $this->package_model->check_valid_package($user_id, $package_id);
		if(!empty($package)){
			$output['packageData'] = $package;
		}else{
			$output['rcode'] = "FAILED";
			$output['message'] = "Data not found.";
		}        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }
	
	function check_valid_channel(){
        $user_id = $this->input->get("user_id") ;
		$channel_id = $this->input->get("channel_id") ;
		$output = array(
			'rcode' => 'OK'
		);    	
        $channel = $this->package_model->check_valid_channel($user_id, $channel_id);
		if(!empty($channel)){
			$output['channel'] = $channel;
		}else{
			$output['rcode'] = "FAILED";
			$output['message'] = "Data not found.";
		}        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
    }

}