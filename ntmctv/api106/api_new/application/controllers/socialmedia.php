<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Socialmedia extends CI_Controller{

    var $like = '';
    var $dislke = '';
    var $comment = '';

    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('socialmedia_model');
        $this->load->helper('string');
        $this->output->set_content_type('application/json; charset=utf-8');
    }

    function index(){
        echo 'iBolz API - Social Media ver.1.0';
    }

    /* View last activity, total like, dislike and comment */
    function last($id=0, $result='false')
    {
        $data['msg'] = 'bad';
        $rsLastActivity = $this->socialmedia_model->getcontentlastactivity($id);
        $data['line'] = json_decode($rsLastActivity, true);
        if($rsLastActivity != NULL) {
            $data['msg'] = 'ok';
        }
        $this->output->set_output(json_encode($data));
    }

    /* Viewing last score, like, dislike by content_id */
    function last_score($content_id, $customer_id, $result='false')
    {
        $_score = $this->socialmedia_model->get_content_score($content_id);
        if(count($_score) > 0)
        {
            foreach($_score as $row => $val)
            {
                switch($val['tipe'])
                {
                    case 'like' :
                        $select = 'count(*) as total';
                        $where = array('customer_id' => $customer_id, 'content_id' => $content_id);
                        $rslike = $this->socialmedia_model->get_content_like($select, $where)->row_array(0);
                        $islike = $rslike['total'];
                        $val['isvote'] = (intval($islike) == 0 ) ? false : true;
                        break;

                    case 'dislike' :
                        $select = 'count(*) as total';
                        $where = array('customer_id' => $customer_id, 'content_id' => $content_id);
                        $rsdislike = $this->socialmedia_model->get_content_dislike($select, $where)->row_array(0);
                        $isdislike = $rsdislike['total'];
                        $val['isvote'] = (intval($isdislike) == 0) ? false : true;
                        break;
                }
            }
        }

        $data['content_id'] = $content_id;
        $data['count'] = $_score;

        $this->output->set_output(json_encode($data));
    }

    /* function for vote like, dislike */
    function content_vote($content_id, $customer_id, $tipe)
    {
        switch($tipe)
        {
            case 'like' :

                $select = 'count(*) as total';
                $where = array('customer_id' => $customer_id, 'content_id' => $content_id);

                $rslike = $this->socialmedia_model->get_content_like($select, $where)->row_array(0);
                $islike = $rslike['total'];

                $rsdislike = $this->socialmedia_model->get_content_dislike($select, $where)->row_array(0);
                $isdislike = $rsdislike['total'];

                if(intval($islike) > 0){
                    $this->socialmedia_model->delete_content_like($where);
                    $lastscore = $this->socialmedia_model->get_content_score($content_id);
                    $total_like = $lastscore[1]['total'];
                    $data['rcode'] = 'ok';
                    $data['msg'] = $lastscore;

                } elseif(intval($isdislike) > 0) {

                    $this->socialmedia_model->delete_content_dislike($where);
                    $data_insert = array(
                        'customer_id' => $customer_id,
                        'content_id' => $content_id,
                        'created' => date('Y-m-d H:i:s')
                    );
                    $this->socialmedia_model->insert_content_like($data_insert);

                    $lastscore = $this->socialmedia_model->get_content_score($content_id);
                    $total_like = $lastscore[1]['total'];

                    $data['rcode'] = 'ok';
                    $data['msg'] = $lastscore; // $total_like;

                } else {

                    $data_insert = array(
                        'customer_id' => $customer_id,
                        'content_id' => $content_id,
                        'created' => date('Y-m-d H:i:s')
                    );
                    $this->socialmedia_model->insert_content_like($data_insert);
                    $lastscore = $this->socialmedia_model->get_content_score($content_id);
                    $total_like = $lastscore[1]['total'];
                    $data['rcode'] = 'ok';
                    $data['msg'] = $lastscore; // $total_like;
                }
                break;


            case 'dislike' :

                $select = 'count(*) as total';
                $where = array('customer_id' => $customer_id, 'content_id' => $content_id);
                $rslike = $this->socialmedia_model->get_content_like($select, $where)->row_array(0);
                $islike = $rslike['total'];

                $rsdislike = $this->socialmedia_model->get_content_dislike($select, $where)->row_array(0);
                $isdislike = $rsdislike['total'];

                if(intval($islike) > 0){
                    $this->socialmedia_model->delete_content_like($where);
                    $data_insert = array(
                        'customer_id' => $customer_id,
                        'content_id' => $content_id,
                        'created' => date('Y-m-d H:i:s')
                    );
                    $this->socialmedia_model->insert_content_dislike($data_insert);
                    $lastscore = $this->socialmedia_model->get_content_score($content_id);
                    $total_dislike = $lastscore[2]['total'];
                    $data['rcode'] = 'ok';
                    $data['msg'] = $lastscore; // $total_dislike;

                } elseif(intval($isdislike) > 0) {

                    $this->socialmedia_model->delete_content_dislike($where);
                    $lastscore = $this->socialmedia_model->get_content_score($content_id);
                    $total_dislike = $lastscore[2]['total'];
                    $data['rcode'] = 'ok';
                    $data['msg'] = $lastscore; // $total_dislike;

                } else {

                    $data_insert = array(
                        'customer_id' => $customer_id,
                        'content_id' => $content_id,
                        'created' => date('Y-m-d H:i:s')
                    );
                    $this->socialmedia_model->insert_content_dislike($data_insert);
                    $lastscore = $this->socialmedia_model->get_content_score($content_id);
                    $total_dislike = $lastscore[2]['total'];
                    $data['rcode'] = 'ok';
                    $data['msg'] = $lastscore; // $total_dislike;
                }
                break;
        }

        $this->output->set_output(json_encode($data));
    }

    public function get_comment($content_id='', $limit=0, $offset=10)
    {
        $_row = $this->socialmedia_model->get_comment($content_id, $limit, $offset);
        $this->output->set_output(json_encode($_row));
    }

    public function get_comment_by_id($comment_id)
    {
        $_row = $this->socialmedia_model->get_comment_by_comment_id($comment_id);
        $this->output->set_output(json_encode($_row));
    }

    public function insert_comment()
    {
        $comment_id = $this->input->post('id');
        $content_id = $this->input->post('content_id');
        $customer_id = $this->input->post('customer_id');
        $comment_text = $this->input->post('text');
        $data_insert = array(
            'comment_id' => $comment_id,
            'content_id' => $content_id,
            'customer_id' => $customer_id,
            'comment_text' => addslashes($comment_text),
            'created' => date('Y-m-d H:i:s'),
        );
        $data['rcode'] = 'ok';
        $data['commentid'] = $comment_id;
        $data['msg'] = $this->socialmedia_model->insert_comment($data_insert);
        $this->output->set_output(json_encode($data));
    }

    public function delete_comment($content_id, $customer_id)
    {
        $where = array('content_id' => $content_id, 'customer_id' => $customer_id);
        $this->socialmedia_model->delete_comment($where);
    }

    public function post_comment()
    {

    }

}

