<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Customer extends CI_Controller{
	
	function Customer(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('customer_model');
		$this->load->model('payment_model');
		$this->load->model('statistic_model');
		//$this->output->set_content_type('application/json');
	}	
	
	
  function login(){
    $email = $this->input->post("email");
    $password = $this->input->post("password");
		$longitude = $this->input->post("longitude");
		$latitude = $this->input->post("latitude");
    
    $apps_id = $this->input->post("apps_id");
    $version = $this->input->post("version");
    $customer_id = $this->input->post("customer_id");
    $device_id = $this->input->post("device_id");
    $screen_size = $this->input->post("screen_size");
    $remote_host = $_SERVER['REMOTE_ADDR'];
    $mobile_id = $this->input->post("mobile_id");
    $status = 1;

		if (empty($longitude))  $longitude= '0';		
		if (empty($latitude))  $latitude= '0';									
	
  	$customer = $this->customer_model->login($email, $password, $latitude, $longitude);
	
		if(!$customer){
			$customer = array(
				"rcode" => 0,
				"message" => "Invalid User ID"
			);
		}else{
			/*
			if($customer->verify_status == 0){
				$customer = array(
					"rcode" => 0,
					"message" => "Please verify your email"
				);
			}
			*/
			/*
			$customer = array(
				"rcode" => 1,
				"message" => "Successfully Login"
			);
			*/
			$customer->rcode = 1;
			$customer->message = "Successfully Login";
		}
	
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($customer));
		$this->statistic_model->apps_activity('login', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
  }

    function signup(){
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $display_name = $this->input->post("name");
		$mobile_no = $this->input->post("mobile_no");

        $apps_id = $this->input->post("apps_id");
        $version = $this->input->post("version");
		$device_id = $this->input->post("device_id");
		$longitude = $this->input->post("longitude");
		$latitude = $this->input->post("latitude");
		
		$user_id = $email;
		
		$first_name = $this->input->post("first_name");
		$last_name = $this->input->post("last_name");
		$customer_id = $this->input->post("customer_id");
		$screen_size = $this->input->post("screen_size");
		$mobile_id = $this->input->post("mobile_id");
		$remote_host = $_SERVER['REMOTE_ADDR'];


		if (empty($mobile_id))  $mobile_id= 'webversion';		
		if (empty($device_id))  $device_id= '1';		
		if (empty($first_name))  $first_name = $display_name;		
		if (empty($last_name))  $last_name = $display_name;	
		if (empty($longitude))  $longitude= '0';		
		if (empty($latitude))  $latitude= '0';									

		$status = 1;
				
				
				
		$customer = $this->customer_model->get_account_by_email($email);
		if($customer){
			$customer = array(
				"rcode" => 0,
				"message" => "Email already exist"
			);
		}else{ 
	    	$customer = $this->customer_model->signup($email, $mobile_id, $user_id, $password, $first_name, $last_name, $display_name, $status, $apps_id, $device_id, $longitude, $latitude, $mobile_no);
			if($customer){
				$customer->rcode = 1;	
				$customer->message = "Successfully Registered";


				//verification email
				//$customer->rcode = 0;
				$customer->message = "Check your email to verify";
	
				$verify = $this->customer_model->signup_create_verify($customer->customer_id);

				$mailer_url = 	$this->config->item('mailer_url') .'send_verification' ;					
				$mailer_data = array(
					'name' => $customer->display_name,
					'email' => $customer->email,
					'url' => $this->config->item('apps_url').'customer/signup_verify?id='.$customer->customer_id.'&vc='.$verify->verify_code
				);

				$this->load->library('curl'); 
				

				$this->curl->option('TIMEOUT', 180, 'OPT');
				$this->curl->option('FOLLOWLOCATION',  FALSE, 'OPT');
				$this->curl->option('RETURNTRANSFER', FALSE, 'OPT');
				$customer->mailer= $this->curl->simple_post($mailer_url, $mailer_data);
				
				//shell_exec('2>&1 /opt/applications/send_mail.sh signup_verify ' . $customer->customer_id);
				
			}else{
				$customer = array(
					"rcode" => 0,
					"message" => "Failed"
				);
			}
		}
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($customer));
		
		$this->statistic_model->apps_activity('signup', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
			/*
			$rawData = file_get_contents("php://input");
	    ob_start();
	    echo '\r\n';
	    print_r($rawData);
	    $c = ob_get_contents();
	    ob_end_clean();
	    $fp = fopen("application/logs/customer_signup.txt", "a");
	    fwrite($fp, $c);
	    fclose($fp);		
			*/
    }


  function change_password(){
    $customer_id = $this->input->post("customer_id");
    $password = $this->input->post("password");
    $new_password = $this->input->post("new_password");

    $customer = $this->customer_model->change_password($customer_id, $password, $new_password);
		if($customer){
			$customer = array(
				"rcode" => 1,
				"message" => "Updated"
			);
		}else{
			$customer = array(
				"rcode" => 0,
				"message" => "Failed"
			);
		}
	
    $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($customer));
		//$this->statistic_model->apps_activity('signup', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
  }


  function forget_password(){
    $email = $this->input->post("email");
		$customer = $this->customer_model->get_account_by_email($email);

		if($customer){
			//forgot password link email
			$customer->rcode = 1;
			$customer->message = "Check your email to reset your password";

			$verify = $this->customer_model->forget_password_create_verify($email);

			$mailer_url = 	$this->config->item('mailer_url') .'send_forgot_password' ;					
			$mailer_data = array(
				'name' => $customer->display_name,
				'email' => $customer->email,
				'url' => $this->config->item('apps_url').'customer/reset_password?id='.$customer->customer_id.'&vc='.$verify->verify_code
			);


			$this->load->library('curl'); 

			$this->curl->option('USERAGENT', $_SERVER['HTTP_USER_AGENT'], 'OPT');
			$this->curl->option('TIMEOUT', 180, 'OPT');
			$this->curl->option('FOLLOWLOCATION',  FALSE, 'OPT');
			$this->curl->option('RETURNTRANSFER', FALSE, 'OPT');
			$customer->mailer = $this->curl->simple_post($mailer_url, $mailer_data);
			//$this->curl->debug();
			//shell_exec('2>&1 /opt/applications/send_mail.sh forget_password_verify ' . $customer->customer_id);
		}else{
			$customer = array(
				"rcode" => 0,
				"message" => "Can't find email account"
			);
		}
	
    $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($customer));
  }

  function profile(){
		$customer_id = $this->input->post("customer_id");
  	$customer = $this->customer_model->profile($customer_id);
	
		if(!$customer){
			$customer = array(
				"rcode" => 0,
				"message" => "Can not find customer"
			);
		}else{
			$customer->rcode = 1;
			$customer->message = "Successfully get profile";
			//set thumbnail
			if($customer->filename){
				$customer->thumbnail = "http://" . $_SERVER['SERVER_NAME']. "/media/thumbnail_customer?id=". $customer->filename;
			}else{
				$customer->thumbnail = "http://" . $_SERVER['SERVER_NAME'] . "/media/thumbnail_customer?id=" . $this->config->item('img_not_found') ;
			}			
		}
	
    $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($customer));
  }


  function update_profile(){
    $customer_id = $this->input->post("customer_id");
    $display_name = $this->input->post("name");
    $email = $this->input->post("email");
    $mobile_no = $this->input->post("mobile_no");
        
		$first_name = $display_name;
		$last_name = $display_name;	

    $customer = $this->customer_model->update_profile($customer_id, $display_name, $first_name, $last_name, $email, $mobile_no);
		if($customer){
			$customer = array(
				"rcode" => 1,
				"message" => "Updated"
			);
		}else{
			$customer = array(
				"rcode" => 0,
				"message" => "Failed"
			);
		}
	
    $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($customer));
		//$this->statistic_model->apps_activity('signup', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
  }




  function payment_type_list(){
    $payment_type_list = $this->payment_model->payment_type_list();
		$data = array(
			"payment_type_list" => $payment_type_list
		);
	
	  $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
		//$this->statistic_model->apps_activity('signup', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
  }

	
	function save_payment_method(){
        $customer_id = $this->input->post("customer_id");
        $type_id = $this->input->post("type_id");
        $voucher_number = $this->input->post("voucher_number");
        $version = $this->input->post("version");
        $device_id = $this->input->post("device_id");
        $screen_size = $this->input->post("screen_size");
        $apps_id = $this->input->post("apps_id");
        $longitude = $this->input->post("longitude");
        $latitude = $this->input->post("latitude");
        $mobile_id = $this->input->post("mobile_id");

		$rcode = 0;
		$message = "";
		$balance = 0;
		if($type_id == "2"){
			//voucher validation
			$voucher = $this->payment_model->voucher_detail($voucher_number);
			if($voucher){
				$amount = $voucher->amount;
				$customer_balance = $this->payment_model->customer_balance_save($customer_id, $amount);
				$balance = $customer_balance->balance;
				$rcode = 1;
				$message = "Your balance = $customer_balance->balance";
			}else{
				$rcode = 0;
				$message = "Invalid Voucher Number";
			}
			
		}
		
		$data = array(
			"rcode"=>$rcode,
			"balance"=>$balance,
			"message"=>$message
		);
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
		
		
	}
	
	
	function send_email(){
        $customer_id = $this->input->get("customer_id");
        $display_name = $this->input->get("display_name");
        $email = $this->input->get("email");
        $verify_code = $this->input->get("verify_code");
        $password = $this->input->get("password");

    	ini_set("SMTP", "smtp.189gameshop.com");
    	ini_set("sendmail_from", "ibolz@189gameshop.com");
		echo ' ';
		
		$url = "http://124.232.130.81:18000/download/verify.php?email=$email&vc=$verify_code";

		$to      = $email;
		$subject = 'Welcome to SeeMe, Verify your email';

    	$message = "Hi $display_name,<br /><br />
    						Please click on this link to verify your email address and complete your registration on SeeMe.<br />
    						<a href=\"$url\"><strong>Verify $email</strong></a><br /><br />

    						Your account information is below -- please save this information for future reference.<br />
    						Email: $email<br />
    						Password: $password<br /><br />

    						If you can't click on the link above, you can verify your email address by cutting and pasting (or typing) the following address into your browser: <br /><br />
    						$url<br />
    						<br /><br />
    						(c) 2014 SeeMe .<br />";

		$headers  = 'MIME-Version: 1.0' . "\r\n".
								'Content-type: text/html; charset=utf-8' . "\r\n".
								'From: ibolz@189gameshop.com' . "\r\n" .
		           	'Reply-To: ibolz@189gameshop.com' . "\r\n" .
		           	'X-Mailer: PHP/' . phpversion();
		$return = ' ,Email Server failed..';
		if(mail($to, $subject, $message, $headers)){
		    $return = " ,Mail Sent, Check your email now.... ";
		}else{
		    $return = " ,Failed Sending Email..";
		}
		return $return;
	}


	function send_forget_password_email(){
        $customer_id = $this->input->get("customer_id");
        $display_name = $this->input->get("display_name");
        $email = $this->input->get("email");
        $verify_code = $this->input->get("verify_code");

		//echo $customer_id . ' - ' . $display_name . ' - ' . $email . ' - ' . $verify_code;

    	ini_set("SMTP", "smtp.189gameshop.com");
    	ini_set("sendmail_from", "ibolz@189gameshop.com");
		echo ' ';

		$url = "http://124.232.130.81:18000/download/reset_password.php?email=$email&vc=$verify_code";

		$to      = $email;
		$subject = 'SeeMe, Reset your password';

    	$message = "Hi $display_name,<br /><br />
    						Please click on this link to verify your email address and reset your password on SeeMe.<br />
    						<a href=\"$url\"><strong>Verify $email</strong></a><br /><br />

    						If you can't click on the link above, you can verify your email address by cutting and pasting (or typing) the following address into your browser: <br /><br />
    						$url<br />
    						<br /><br />
    						(c) 2014 SeeMe .<br />";

		$headers  = 'MIME-Version: 1.0' . "\r\n".
								'Content-type: text/html; charset=utf-8' . "\r\n".
								'From: ibolz@189gameshop.com' . "\r\n" .
		           	'Reply-To: ibolz@189gameshop.com' . "\r\n" .
		           	'X-Mailer: PHP/' . phpversion();
		$return = ' ,Email Server failed..';
		if(mail($to, $subject, $message, $headers)){
		    $return = " ,Mail Sent, Check your email now.... ";
		}else{
		    $return = " ,Failed Sending Email..";
		}
		//echo $return;
		return $return;
	}

	function signup_dummy(){
		 $this->output->set_content_type('text/html;charset=utf-8');
	 	 $html=$this->load->view('signup_dummy', array(), true );
	 	 echo $html;
	}		
	
	
	function subscribe(){
		
		$soapUrl 		= "http://41.206.4.162:8310/SubscribeManageService/services/SubscribeManage";
		$spId 			= '234012000002449';
		$timestamp 	= date('YmdHis');
		$password 	= 'bmeB500';
		$hash 			= $spId . $password . $timestamp;
		$spPassword = md5($hash);
		$apps_id 		= $this->input->post("apps_id");
		$product_id = $this->input->post("product_id");
		$user_id 		= $this->input->post("msisdn");
		if (empty($user_id))  $user_id = isset($_SERVER['HTTP_MSISDN']) ? $_SERVER['HTTP_MSISDN'] : '';

		/*
		// DEBUG MODE		
		$apps_id = 'com.balepoint.ibolz.mtn.ng';
		$product_id = '23401220000002110'; //23401220000002111
		$user_id ='2348149900009';  //2348137256805 2348165168786 
		$this->customer_model->update_subscription($user_id, $apps_id, $product_id,  '1');
		*/
		
		$raw_xml = '
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
		                  xmlns:loc="http://www.csapi.org/schema/parlayx/subscribe/manage/v1_0/local">
		    <soapenv:Header>
		        <tns:RequestSOAPHeader xmlns:tns="http://www.huawei.com.cn/schema/common/v2_1">
		            <tns:spId>'.$spId.'</tns:spId>
		            <tns:spPassword>'.$spPassword.'</tns:spPassword>
		            <tns:timeStamp>'.$timestamp.'</tns:timeStamp>
		            <oauth_token></oauth_token>
		        </tns:RequestSOAPHeader>
		    </soapenv:Header>
		    <soapenv:Body>
		        <loc:subscribeProductRequest>
		            <loc:subscribeProductReq>
		                <userID>
		                    <ID>'.$user_id.'</ID>
		                    <type>0</type>
		                </userID>
		                <subInfo>
		                    <productID>'.$product_id.'</productID>
		                    <operCode>id</operCode>
		                    <isAutoExtend>0</isAutoExtend>
		                    <channelID>99</channelID>
		                </subInfo>
		            </loc:subscribeProductReq>
		        </loc:subscribeProductRequest>
		    </soapenv:Body>
		</soapenv:Envelope>
		';
		
		$headers = array(
		    'Content-type: text/xml; charset="utf-8"',
		    'SOAPAction: ""',
		    'Accept: text/xml',
		    'Cache-Control: no-cache',
		    'Pragma: no-cache',
		    'Content-length: '.strlen($raw_xml),
		);
		
		// show xml request
		//echo $raw_xml;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $soapUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 3600
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $raw_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$response = curl_exec($ch);
		$rcode = 0;
		$message = "Unable to subscribe.";
		$xml = '';
		if(curl_errno($ch)) {
		  //print curl_error($ch);
			$message .= 'No connection to SDP';
		} else {
		  curl_close($ch);
			
			if (!empty($response)) {
				// show xml response
				//var_dump($response);	
				
				$xml = new SimpleXMLElement($response);
				$xml->registerXPathNamespace('ns1','http://www.csapi.org/schema/parlayx/subscribe/manage/v1_0/local');
				$data = $xml->xpath("//ns1:subscribeProductRsp");
	
				if (!empty($data)) {
					//var_dump($data);
					$rcode = $data[0]->result;
					$message = $data[0]->resultDescription;

					if ($rcode =='22007201' || $rcode =='0' || $rcode =='00000000') {
							$message = "Success.";
							$this->customer_model->update_subscription_payment($user_id, $apps_id, $product_id,  '1');
					} else {
							$rcode = '0';	
					}
				}
			}
		}

		$data = array(
			"rcode"=>$rcode,
			"message"=>$message
		);
		
		//log 
		$log_date = date('d-m-Y h:i:s a', time());
    ob_start();
		echo "";    
		echo "";    
		echo "========================================================================";
    echo "DATE : $log_date ";
		echo "XML_REQUEST :";     
    echo ($raw_xml);
		echo "";
		echo "XML_RESPONSE:";     
    echo ($xml);
    $c = ob_get_contents();
    ob_end_clean();
    $fp = fopen("application/logs/customer_subscribe.txt", "a");
    fwrite($fp, $c);
    fclose($fp);		
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
	}



	function unsubscribe(){
		
		$soapUrl 		= "http://41.206.4.162:8310/SubscribeManageService/services/SubscribeManage";
		$spId 			= '234012000002449';
		$timestamp 	= date('YmdHis');
		$password 	= 'bmeB500';
		$hash 			= $spId . $password . $timestamp;
		$spPassword = md5($hash);
		//$product_id = '23401220000001721';
		$apps_id 		= $this->input->post("apps_id");
		$product_id = $this->input->post("product_id");
		$user_id 		= $this->input->post("msisdn");
		if (empty($user_id))  {
					$user_id = isset($_SERVER['HTTP_MSISDN']) ? $_SERVER['HTTP_MSISDN'] : '';
		}

		
		$raw_xml = '
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
		                  xmlns:loc="http://www.csapi.org/schema/parlayx/subscribe/manage/v1_0/local">
		    <soapenv:Header>
		        <tns:RequestSOAPHeader xmlns:tns="http://www.huawei.com.cn/schema/common/v2_1">
		            <tns:spId>'.$spId.'</tns:spId>
		            <tns:spPassword>'.$spPassword.'</tns:spPassword>
		            <tns:timeStamp>'.$timestamp.'</tns:timeStamp>
		            <oauth_token></oauth_token>
		        </tns:RequestSOAPHeader>
		    </soapenv:Header>
		    <soapenv:Body>
		        <loc:unSubscribeProductRequest>
		            <loc:unSubscribeProductReq>
		                <userID>
		                    <ID>'.$user_id.'</ID>
		                    <type>0</type>
		                </userID>
		                <subInfo>
		                    <productID>'.$product_id.'</productID>
		                    <operCode>id</operCode>
		                    <isAutoExtend>0</isAutoExtend>
		                    <channelID>99</channelID>
		                </subInfo>
		            </loc:unSubscribeProductReq>
		        </loc:unSubscribeProductRequest>
		    </soapenv:Body>
		</soapenv:Envelope>
		';
		
		$headers = array(
		    'Content-type: text/xml; charset="utf-8"',
		    'SOAPAction: ""',
		    'Accept: text/xml',
		    'Cache-Control: no-cache',
		    'Pragma: no-cache',
		    'Content-length: '.strlen($raw_xml),
		);
		
		// show xml request
		// echo $raw_xml;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $soapUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); //3600
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $raw_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$response = curl_exec($ch);
		$rcode = 0;
		$message = "Unable to unsubscribe.";
		
		if(curl_errno($ch)) {
		  //print curl_error($ch);
		  $message .= 'No connection to SDP';
		} else {
		  curl_close($ch);
			
			if (!empty($response)) {
				// show xml response
				//var_dump($response);	
				
				$xml = new SimpleXMLElement($response);
				$xml->registerXPathNamespace('ns1','http://www.csapi.org/schema/parlayx/subscribe/manage/v1_0/local');
				$data = $xml->xpath("//ns1:unSubscribeProductRsp");		

				if (!empty($data)) {
					//var_dump($data);
					$rcode = $data[0]->result;
					$message = $data[0]->resultDescription;
					if ($rcode =='0' || $rcode =='00000000') {
						$this->customer_model->update_subscription($user_id, $apps_id, $product_id, '0');
					}					
				}
			}
		}

		$data = array(
			"rcode"=>$rcode,
			"message"=>$message
		);
		
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
		
		//log 
		$log_date = date('d-m-Y h:i:s a', time());
    ob_start();
		echo "";    
		echo "";    
		echo "========================================================================";
    echo "DATE : $log_date ";
		echo "XML_REQUEST :";     
    echo ($raw_xml);
		echo "";
		echo "XML_RESPONSE:";     
    echo ($xml);
    $c = ob_get_contents();
    ob_end_clean();
    $fp = fopen("application/logs/customer_unsubscribe.txt", "a");
    fwrite($fp, $c);
    fclose($fp);		
		
		
	}

 	function subscription(){
		
		$expired_days = '0';
		$msisdn 		= isset($_SERVER['HTTP_MSISDN']) ? $_SERVER['HTTP_MSISDN'] : '';
		$apps_id 			= $this->input->post("apps_id");
		//$apps_id = 'com.balepoint.ibolz.mtn.ng';
		//$apps_id = 'com.balepoint.ibolz.moviebay';
		
		$data = array(
			"msisdn"=>$msisdn,
			"rcode"=> '2',
			"message"=> 'Not subscribe.',
			"expired"=> '0 days',
			'product' => array(),
			'params' => array('msisdn' => $msisdn)
		);
		
		if ($apps_id == 'com.balepoint.ibolz.mtn.ng') {
			//$msisdn 			= $this->input->post("msisdn");
			//$msisdn ='62819089627721';  //6281908962772 //2348137256805 = test raphael, 6281908962772 = yogie
			$subscriber = $this->customer_model->get_subscription_by_msisdn($msisdn, $apps_id);

		} else {
			$customer_id = $this->input->post("customer_id");
			//$customer_id = '111607530952b6b24289534';
			$subscriber = $this->customer_model->get_subscription_by_customer($customer_id, $apps_id);
		}		

		if (!empty($subscriber)) {
			$data['product'] = array(
				'product_id' => $subscriber->product_id,
				'product_name' => $subscriber->product_name,
				'product_price' => $subscriber->product_price,
				'active_days' => $subscriber->active_days
			);
			
			$status = $subscriber->subscription_status;
			$expired = $subscriber->expiry_time;
			if (!empty($expired)) {
				$timestamp = time();
				$expired = strtotime($expired);
				$diff = $timestamp - $expired;
				$expired_days = round($diff / 86400);

				if ($timestamp > $expired) {
						//echo 'expired <br>';
						$data['rcode'] = '3';
				} else {
					//echo 'not expired<br>';
					$data['rcode'] = $status;
				}								
				
			} else {
				$data['rcode'] = $status;
			}
			
			if ($data['rcode'] == '1') {
				$data['message'] = 'Success';
				$data['expired'] = abs($expired_days) . ' days';

			} elseif ($data['rcode'] == '3') {
				$data['message'] = 'Expired';
				$data['expired'] = $expired_days . ' days';
			}
			if ($status == '0') $data['message'] = 'Inactive';
		}
		
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
 	}

 	function subscription_product(){
		$apps_id 			= $this->input->post("apps_id");
		//$apps_id = 'com.balepoint.ibolz.mtn';
		//$apps_id = 'com.balepoint.ibolz.moviebay';
		$data			= array();
		
		$data['products'] = $this->customer_model->get_subscription_product();
		$data['payment_provider'] = $this->customer_model->get_payment_provider($apps_id);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
 	}

	function subscription_test(){
		$response =$this->customer_model->update_subscription_payment('2348149900009','com.balepoint.ibolz.mtn.ng', '23401220000002111',  '1');
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));

	}

	public function get_video_duration($filename){
		$xyz = shell_exec($this->config->item('ffmpeg') . " -i \"{$filename}\"  2>&1");
		$search='/Duration: (.*?),/';
		preg_match($search, $xyz, $matches);
		$explode = explode(':', $matches[1]);
		
		$hour = intval($explode[0]) * 3600;
		$minute = intval($explode[1]) * 60;
		$second = intval($explode[2]);		
		$duration = $hour + $minute + $second;
		return $duration;
	}


  function upload_photo(){
    $customer_id = $this->input->post("customer_id");
    $target_path = $this->config->item('customer_media_path').'/customer/thumbnails/';


    $response = array(
        "rcode" => 0,
        "message" => "Error on upload",
        "thumbnail" => "http://" . $_SERVER['SERVER_NAME']. "/media/thumbnail_customer?id=" . $this->config->item('img_not_found')
    );
		$is_file = 0;

		if(isset($_FILES['file']['tmp_name']) && $_FILES['file']['tmp_name']) {
			$is_file = 1;
			if(!empty($_FILES['file']['error'])) {
				$file_error = $_FILES['file']['error'];
				switch($file_error) {
					case '1':
					case '2':
						$response['message'] = 'File too big to upload';
						break;
					case '3':
						$response['message'] = 'File partially uploaded';
						break;
					case '4':
						$response['message'] = 'No file was uploaded.';
						break;
					case '6':
						$response['message'] = 'Missing a temporary folder';
						break;
					case '7':
						$response['message'] = 'Failed to save file';
						break;
					default:
						$response['message'] = 'Failed to upload, unknown error';
						break;
				}
			}
		}


		if($is_file) {

      $filename = basename( $_FILES['file']['name']);
      $newFileName = uniqid(rand(), false) . substr($filename, strrpos($filename, ".", -2));
      $target_path = $target_path . $newFileName;
      if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
          $this->customer_model->upload_photo($customer_id, $newFileName);
          $response = array(
              "rcode" => 1,
              "message" => "Successfully Uploaded",
              "thumbnail" => "http://" . $_SERVER["SERVER_NAME"] . "/media/thumbnail_customer?id=" . $newFileName
          );
      } else {
	      $response['rcode'] = "0";
	      $response['message'] = "unable to copy file to server.";
      }
    } else {
	      $response['rcode'] = "0";
	      $response['message'] = "No file to upload.";
    }
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($response));
  }
	
	function update_photo(){
    $customer_id = $this->input->post("customer_id");		
    $filename = $this->input->post("filename");	
    $response = array(
        "rcode" => 1,
        "message" => "Success"
    );


		$this->customer_model->update_photo($customer_id, $filename);
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($response));
	}


  function upload_video(){
    $customer_id = $this->input->post("customer_id");
    $customer_category_id = $this->input->post("customer_category_id");
    $apps_id = $this->input->post("apps_id");
    $amount = $this->input->post("amount");


    $customer_path = $this->config->item('customer_media_path');
    $streaming_path = $customer_path  . 'streaming/';
		$transcode_path = $this->config->item('customer_transcode_path');

    $response = array(
        "rcode" => 0,
        "message" => "Error on upload"
    );
		$is_file = 0;

		if(isset($_FILES['video']['tmp_name']) && $_FILES['video']['tmp_name']) {
			$is_file = 1;
			if(!empty($_FILES['video']['error'])) {
				$file_error = $_FILES['video']['error'];
				switch($file_error) {
					case '1':
					case '2':
						$response['message'] = 'File too big to upload';
						break;
					case '3':
						$response['message'] = 'File partially uploaded';
						break;
					case '4':
						$response['message'] = 'No file was uploaded.';
						break;
					case '6':
						$response['message'] = 'Missing a temporary folder';
						break;
					case '7':
						$response['message'] = 'Failed to save file';
						break;
					default:
						$response['message'] = 'Failed to upload, unknown error';
						break;
				}
			}
		}


		if($is_file) {

      $filename = basename( $_FILES['video']['name']);
      $file_info = (object)pathinfo($_FILES['video']['name']);
      $uniq_filename = uniqid(rand(), false) .'-'. $_FILES['video']['name'];
			$new_filename = (preg_replace('/[^a-zA-Z0-9-]/', '', strtoupper($customer_path == "" ? "" : $customer_path . "-") . $uniq_filename));
      $new_file = $transcode_path . $new_filename .'.'.$file_info->extension;
      
      if(move_uploaded_file($_FILES['video']['tmp_name'], $new_file)) {
					
          //$this->customer_model->upload_video($customer_id, $newFileName);
          $response = array(
              "rcode" => 1,
              "message" => "Successfully Uploaded"
          );
          
					
					//generate thumbnail
					$input_filename = $new_file;
					$thumbnail_script = $this->config->item('ffmpeg') . ' ' . $this->config->item('thumbnail_script');
					$thumbnail_script = str_replace('__input__', $input_filename, $thumbnail_script);
					$thumbnail_script = str_replace('__output__', ($this->config->item('thumbnail_path') . $new_filename . '.jpg'), $thumbnail_script);
					
					$xyz = shell_exec($thumbnail_script . '  2>&1');
					
					//get duration
					$duration = ($this->get_video_duration($new_file));
					
					$status = 10;
					$thumbnail = $new_filename . '.jpg';
					$filesize = filesize($input_filename);
					
					$this->load->model('content_model');
					$script_list = $this->content_model->script_file_list();
					$content_status ='0';
					$content = $this->content_model->content_save($customer_id, $filename , $filename, $new_filename,'9', $content_status, $duration, $thumbnail, '', '9');


					foreach($script_list as $script){				
						$device_id = $script->device_id;
						$res_id = $script->res_id;
						$script_file = $script->script_file;
						
						$this->content_model->content_res_save($content->content_id, $res_id, $device_id, $input_filename, $content_status, $script_file, $filesize, 'file', $streaming_path);
					}
					// update content status = 10, waiting for transcode
					$this->content_model->content_status_update($content->content_id, '10');
					$this->content_model->content_vod_save($apps_id, '-2', $content->content_id, '', $amount, '');
					$allow_share = $this->customer_model->allow_share($customer_id, $customer_category_id) ;
					if ($allow_share) {
						$this->content_model->content_customer_category_add($content->content_id, $customer_category_id);
					}

      } else {
	      $response = array(
	          "rcode" => 0,
	          "message" => "unable to copy file to server."
	      );	      	
      }
    } else {
      $response = array(
          "rcode" => 0,
          "message" => "No file to upload."
      );
    }
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($response));
  }


	function my_video_list() {
		$this->load->model('content_model');
		
		$customer_id = $this->input->post("customer_id");
		$video_list = $this->content_model->content_my_list('', '', '', $customer_id, $list_type='my');
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($video_list));
	}

	function share_video_list() {
		$this->load->model('content_model');
		
		$customer_id = $this->input->post("customer_id");
		$video_list = $this->content_model->content_my_list('', '', '', $customer_id, $list_type='share');
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($video_list));
	}

	function share_video_add() {
		$this->load->model('content_model');
		
		$customer_id = $this->input->post("customer_id");
		$content_id = $this->input->post("content_id");
    $response = array(
        "rcode" => 1,
        "message" => "Success"
    );
		$this->content_model->content_customer_category_save($customer_id, $content_id);
    $this->output->set_content_type('application/json');
    $this->output->set_output(json_encode($response));
	}	
	
	
	function customer_history(){
        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");

		$history_list =$this->customer_model->customer_history($apps_id, $customer_id);
		$response = array(
			"history_list"=>$history_list
        );
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));

	}

	function transaction_history(){
        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");

		$transaction_list =$this->customer_model->transaction_history($apps_id, $customer_id);
		$response = array(
			"transaction_list"=>$transaction_list
        );
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));

	}
	
	function summary_transaction(){
        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");

		$transaction_list =$this->customer_model->summary_transaction($apps_id, $customer_id);
		$response = array(
			"transaction_list"=>$transaction_list
        );
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));

	}
	
	function mygroup_list(){
        $customer_id = $this->input->post("customer_id");

		$group_list =$this->customer_model->group_list($customer_id);
		$response = array(
			"group_list"=>$group_list
        );
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));

	}
	

	function remove_mygroups(){
        $customer_category_id = $this->input->post("customer_category_id");
        $customer_id = $this->input->post("customer_id");

		$group_list =$this->customer_model->remove_mygroups($customer_id, $customer_category_id);
		$response = array(
			"rcode"=>$group_list,
			"rmessage"=>"OK Lah"
        );
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	
	function add_mygroups(){
        $customer_category_id = $this->input->post("customer_category_id");
        $customer_id = $this->input->post("customer_id");

		$group_list =$this->customer_model->add_mygroups($customer_id, $customer_category_id);
		$response = array(
			"rcode"=>$group_list,
			"rmessage"=>"OK Lah"
        );
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));

	}
	

function topup(){
        $customer_id = $this->input->post("customer_id");
        $amount = $this->input->post("amount");

		$amount =$this->customer_model->topup($customer_id, $amount);
		$response = array(
			"rcode"=>1,
			"rmessage"=>$amount
        );
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));

	}

}




