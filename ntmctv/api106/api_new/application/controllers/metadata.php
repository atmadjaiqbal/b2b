﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Metadata extends CI_Controller{
	
	var $limit = 10;	
	var $offset = 0;	
	var $sort_type = 'ASC';	

	function __construct(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('metadata_model');
		$this->load->helper('string');
    $this->output->set_content_type('application/json; charset=utf-8');
	}	
	
	function index(){
		echo 'iBolz API - Metadata';	
	}
	
	
	function get_content() {
		$content_id = $this->input->get("content_id");
		$response = $this->metadata_model->get_content($content_id);
	
		$this->output->set_output(json_encode($response));
	}

	function content_list() {
		$search_key = (int)($this->input->get("searchkey") == "" ? "" : $this->input->get("searchkey"));
		$sort_key = (int)($this->input->get("sortkey") == "" ? "title" : $this->input->get("sortkey"));
		$sort_type = (int)($this->input->get("sorttype") == "" ? $this->sort_type : $this->input->get("sorttype"));
		$limit = (int)($this->input->get("limit") == "" ? $this->limit : $this->input->get("limit"));
		$offset = (int)($this->input->get("offset") == "" ? $this->offset : $this->input->get("offset"));
				
		

		$response = $this->metadata_model->content_list($search_key, $sort_key, $sort_type, $offset, $limit);
	
		$this->output->set_output(json_encode($response));
	}

	function crew_list() {
		$limit = (int)($this->input->get("limit") == "" ? $this->limit : $this->input->get("limit"));
		$offset = (int)($this->input->get("offset") == "" ? $this->offset : $this->input->get("offset"));
		
		$response = $this->metadata_model->crew_list($offset, $limit);
	
		$this->output->set_output(json_encode($response));
	}



	function content_list_crew() {
		$crew_id = $this->input->get("crew_id");
		
		$limit = (int)($this->input->get("limit") == "" ? $this->limit : $this->input->get("limit"));
		$offset = (int)($this->input->get("offset") == "" ? $this->offset : $this->input->get("offset"));
		
		$response = $this->metadata_model->content_list_crew($crew_id, $offset, $limit);
	
		$this->output->set_output(json_encode($response));
	}


	function genre_list() {
		$limit = (int)($this->input->get("limit") == "" ? $this->limit : $this->input->get("limit"));
		$offset = (int)($this->input->get("offset") == "" ? $this->offset : $this->input->get("offset"));
		
		$response = $this->metadata_model->genre_list($offset, $limit);
	
  	$this->output->set_output(json_encode($response));
	}


	function content_list_genre() {
		$genre_id = $this->input->get("genre_id");
		$limit = (int)($this->input->get("limit") == "" ? $this->limit : $this->input->get("limit"));
		$offset = (int)($this->input->get("offset") == "" ? $this->offset : $this->input->get("offset"));
		
		$response = $this->metadata_model->content_list_genre($genre_id, $offset, $limit);
	
  	$this->output->set_output(json_encode($response));
	}

	function channel_list() {
		
		$response = $this->metadata_model->channel_list();
  	$this->output->set_output(json_encode($response));
	}
}
