﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Mailer extends CI_Controller{
	
	function Mailer(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('customer_model');
		$this->load->helper('string');
 		$this->mailer_email = "ibolz.mailer@gmail.com";
    $this->email_config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'ssl://smtp.gmail.com',
		  'smtp_port' => 465,
		  'smtp_user' => $this->mailer_email, 
		  'smtp_pass' => 'ibolz123', 
		  'mailtype' => 'html',
		  'charset' => 'iso-8859-1',
		  'wordwrap' => TRUE		
		);
	}
		
	
	function index(){
		echo 'iBolz Mailer';	
	}

	function send_verification(){
		$name	= $this->input->post("name");
		if (empty($name))  $name = 'zola';

		$email = $this->input->post("email");
		if (empty($email))  $email 		= 'zola.octanoviar@gmail.com';;

		$url	= $this->input->post("url");
		if (empty($url))  $url= 'http://ibolz.tv/apps/customer/verify_email';;
    
    $response = array(
    	'rcode' => '0', 
    	'message' => 'Failed sent email verification'
    );
    
  	$message 	= "Hi $name,<br /><br />
  						Please click on this link to verify your email address and complete your registration.<br />
  						<a href=\"$url\"><strong>Verify $email</strong></a><br /><br />
  						If you can't click on the link above, you can verify your email address by cutting and pasting (or typing) the following address into your browser: <br /><br />
  						$url<br /><br /><br />
  						(c) Best Regards,<br /> iBolz Admin";

		$this->load->library('email', $this->email_config);		
	  $this->email->set_newline("\r\n");
	  $this->email->from($this->mailer_email); 
	  $this->email->to($email);
	  $this->email->subject('iBolz email verify');
	  $this->email->message($message);
	  
    if($this->email->send()) {
      //echo 'Email sent.';
	    $response = array(
	    	'rcode' => '1', 
	    	'message' => 'Email verification sent'
	    );

    } else {
    	$response['message'] = $this->email->print_debugger();
    }		
    
		//$this->output->set_content_type('application/json');
		//$this->output->set_output(json_encode($response));
		
    return json_encode($response);		
    
	}


	function send_forgot_password(){

		$email = $this->input->post("email");
		if (empty($email))  $email 		= 'zola.octanoviar@gmail.com';;
		
		$url	= $this->input->post("url");
		if (empty($url))  $url= 'http://ibolz.tv/apps/customer/forgot_password';;

    
    $response = array(
    	'rcode' => '0', 
    	'message' => 'Failed sent forgot password'
    );
    
  	$message 	= "Hi,<br /><br />
  						Please click on this link to reset your password.<br />
  						<a href=\"$url\"><strong>Reset Password</strong></a><br /><br />
  						If you can't click on the link above, you can reset your passwordby cutting and pasting (or typing) the following address into your browser: <br /><br />
  						$url<br /><br /><br />
  						(c) Best Regards,<br /> iBolz Admin";

		$this->load->library('email', $this->email_config);		
	  $this->email->set_newline("\r\n");
	  $this->email->from($this->mailer_email); 
	  $this->email->to($email);
	  $this->email->subject('iBolz forget password');
	  $this->email->message($message);
	  
    if($this->email->send()) {
      //echo 'Email sent.';
	    $response = array(
	    	'rcode' => '1', 
	    	'message' => 'Forgot password sent'
	    );
    } else {
    	$response['message_debug'] = $this->email->print_debugger();
    	
    }		
    
		//$this->output->set_content_type('application/json');
		//$this->output->set_output(json_encode($response));
    return json_encode($response);
    
	}

}

