<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Schedule extends CI_Controller{
	
	function Schedule(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('schedule_model');
		$this->load->model('channel_model');
	}	
	
	
	
	
	function today(){
		/*
        $channel_id = $this->input->post("channel_id");
        $schedule_type_id = $this->input->post("schedule_type_id");
		
		$vid_server = $this->channel_model->channel_server_cluster($channel_id);
		$rtmp_addr = $vid_server->public_ip;

		$payload = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/RtmpClient/channel_schedule.php?rtmp_addr=" . $rtmp_addr . "&channel_id=" . $channel_id . "&schedule_type_id=" . $schedule_type_id);

        $this->output->set_content_type('application/json');
		$this->output->set_output($payload);
		*/
		
		
		
		date_default_timezone_set('America/New_York');
		
		$serverAddr = $_SERVER["HTTP_HOST"];
		$start_row = ($this->input->post("page") ? $this->input->post("page") : 1) - 1 ;
        $limit = $this->input->post("limit") ? $this->input->post("limit") : 10 ;

        $channel_id = $this->input->post("channel_id");
        $schedule_type_id = $this->input->post("schedule_type_id");
        $customer_id = $this->input->post("customer_id");
		
        $start_date = $this->input->post("start_date") ? $this->input->post("start_date") : date("Y-m-d H:i:s") ;
        $end_date = $this->input->post("end_date") ? $this->input->post("end_date") : date("Y-m-d H:i:s", strtotime ( '1 day', strtotime (date("Y-m-d H:i:s")))) ;

    	$rows = $this->schedule_model->schedule_today($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id);
		$response = array(
        	"schedule_list" => $rows
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
 	}
	
	
	function record(){
        $apps_id = $this->input->post("apps_id");
        $channel_id = $this->input->post("channel_id");
        $schedule_id = $this->input->post("schedule_id");
        $customer_id = $this->input->post("customer_id");
		
		$this->schedule_model->record($apps_id, $channel_id, $schedule_id, $customer_id);
		$response = array(
			"rcode"=>"1",
			"message"=>"Successfully"
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
	}
    
    


}
