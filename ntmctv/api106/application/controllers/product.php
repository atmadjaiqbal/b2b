<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Product extends CI_Controller{
	
	function Product(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('product_model');
        $this->output->set_content_type('application/json');
	}	
	
	
	function product_list(){
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
        $apps_id = $this->input->post("apps_id");
		$lang = $this->input->post("lang");
        $menu_id = $this->input->post("menu_id");
        $search_key = $this->input->post("search_key");
		
		
		$product_list = $this->product_model->product_list($page, $limit, $apps_id, $menu_id);
		$response = array(
        	"product_list" => $product_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}



	
	
	

    	

    
}
