<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Media extends CI_Controller{
	
	function Media(){
		parent::__construct();
		$this->load->library('SimpleImage');
		//$this->load->library('image_lib');
	}	
	
	
	
	function thumbnail($image_id=null){
		header('Content-Type: image/jpeg');
		$image = new SimpleImage();
		
		$media_base_path = $this->config->item('media_base_path');
		$filename =  $media_base_path . $image_id;
		
		if(file_exists($filename)){
   			$image->load($filename);
   			if(($this->input->get("w"))){
   				$image->resizeToWidth((int)$this->input->get("w"));
   			}

		}else{
   			//$image->load($media_base_path . $this->config->item('img_not_found'));
		}
		
   		$image->output();
	}

	function thumbnail_customer($image_id=null){
		header('Content-Type: image/jpeg');
		$image = new SimpleImage();
		
		
		$media_base_path = $this->config->item('customer_media_path').'thumbnails/';
		$filename =  $media_base_path . $image_id;
		
		if(file_exists($filename)){
   			$image->load($filename);
   			if(($this->input->get("w"))){
   				$image->resizeToWidth((int)$this->input->get("w"));
   			}

		}else{
   			//$image->load($media_base_path . $this->config->item('img_not_found'));
		}
		
   		$image->output();
	}
    
}