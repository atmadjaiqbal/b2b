<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 13 2014 22:20 
*/
class Apps_m extends MY_Model {
	
	protected $table 	= 'apps';
	protected $key		= 'apps_id';

	public function __construct()
	{
		parent::__construct();
	}

	// get channel categories
	public function get_channel_categories($apps_id){
		$sql = "
			SELECT 
				a.apps_id, a.app_name,
				b.channel_category_id, 	
				c.category_name, c.category_name_chn, 
				c.thumbnail, c.tab_type, c.link,
				b.show_tab_menu, b.show_dd_menu
			FROM 
				apps a 
				JOIN apps_category b ON (a.apps_id = b.apps_id)
				JOIN channel_category c ON (b.channel_category_id = c.channel_category_id)
			WHERE a.apps_id = '{$apps_id}'
			ORDER BY b.order_number, c.category_name
		";
  		$query = $this->db->query($sql);	
		return $query->result();
	}

	// get channels
	public function get_channels($apps_id, $channel_category_id = false){
		$sql = "
			SELECT 
				a.apps_id, b.channel_category_id, cc.category_name,
				a.channel_id, d.channel_name, 
				d.alias AS display_name, d.channel_type_id,
				d.thumbnail, d.status, d.link
			FROM 
				apps_channel_category a
				JOIN apps_category b ON (a.apps_id = b.apps_id AND a.channel_category_id = b.channel_category_id)	
				JOIN apps_channel c ON (a.apps_id = c.apps_id AND a.channel_id = c.channel_id)
				JOIN channel_category cc ON (a.channel_category_id = cc.channel_category_id)				
				JOIN channel d ON (c.channel_id = d.channel_id)
			WHERE 
				a.apps_id = '{$apps_id}' AND d.status = 1			
		";
		if($channel_category_id){
			$sql += "
				AND b.channel_category_id = '{$channel_category_id}'
			";
		}
		$sql += "ORDER BY ";
		if(!$channel_category_id){
			$sql += "cc.category_name, ";
		}
		$sql += "a.order_number, d.channel_name";		
  		$query = $this->db->query($sql);
		return $query->result();
	}


	// this function for mapping customer group as channels
	public function get_groups_as_channels($apps_id, $channel_category_id, $customer_id = false){
		$channel_category_id = '3';
		$category_name = 'VOD';
		$thumbnail = '';
		// get thumbnail ... original copy from query of 'apps_model->apps_groups_list'
		$sql = "
			SELECT * 
			FROM channel_category  
			WHERE channel_category_id = '{$channel_category_id}'
		";
		$query = $this->db->query($sql);
		$channel_category = $query->row();		
		if($channel_category){
			$channel_category_id = $channel_category->channel_category_id;
			$category_name = $channel_category->category_name;
			$thumbnail = $channel_category->thumbnail;
		}

		$sql = "
			SELECT 
				a.apps_id, '{$channel_category_id}' AS channel_category_id, '{$category_name}' AS category_name, 
				b.groups_id AS channel_id, b.groups_name AS channel_name, 
				b.groups_name AS display_name, '3' AS channel_type_id, 
				'{$thumbnail}' AS thumbnail, '1' AS `status`, '' AS link
			FROM 
				groups_apps a 
				JOIN groups b ON (a.groups_id = b.groups_id)
			WHERE a.apps_id = '{$apps_id}'
			ORDER BY b.groups_name
		";
  		$query = $this->db->query($sql);	
		return $query->result();
	}

	// this function for mapping customer category group as channels
	public function get_category_groups_as_channels($apps_id, $channel_category_id, $customer_id = false){
		$groups = array();
		if($customer_id){
			$workgroups = $this->customer_group_m->get_member_of_group($apps_id, $customer_id);
			if($workgroups){
				foreach ($workgroups as $row) {
					if($item->allow_access == 'true'){
						$groups[] = $item->group_id; // is mapping of customer_category_id 
					}
				}
			}
		}
		
		$channel_category_id = '3';
		$category_name = 'VOD';
		$thumbnail = '';
		// get thumbnail ... original copy from query of 'apps_model->apps_groups_list'
		$sql = "
			SELECT * 
			FROM channel_category  
			WHERE channel_category_id = '{$channel_category_id}'
		";
		$query = $this->db->query($sql);
		$channel_category = $query->row();		
		if($channel_category){
			$channel_category_id = $channel_category->channel_category_id;
			$category_name = $channel_category->category_name;
			$thumbnail = $channel_category->thumbnail;
		}

		$sql = "
			SELECT
			a.apps_id, {$channel_category_id} AS channel_category_id, {$category_name} AS category_name, 
			b.groups_id AS workgroup_id, b.groups_name AS workgroup_name, 
			d.customer_category_id AS channel_id, d.category_name AS channel_name, 
			d.category_name AS display_name, '3' AS channel_type_id, 
			'{$thumbnail}' AS thumbnail, '1' AS `status`, '' AS link
			FROM
				groups_apps a 
				JOIN groups b ON (a.groups_id = b.groups_id)
				JOIN groups_customer_category c ON (b.groups_id = c.groups_id)
				JOIN customer_category d ON (c.customer_category_id = d.customer_category_id)
			WHERE a.apps_id = '{$apps_id}'			
		";
		if(!empty($groups)){
			$sql .= " AND d.customer_category_id IN ('" . implode("','", $groups) . "') ";
		}
		$sql .= "ORDER BY b.groups_name";
  		$query = $this->db->query($sql);	
		return $query->result();
	}

/*
	SELECT
	'$apps_id' apps_id, '3' channel_category_id, '-2' channel_id, '3' channel_type_id, b.customer_category_id content_id, 
	'content' content_type, b.category_name title, 
	(SELECT thumbnail from channel_category  where channel_category_id = '-2') thumbnail, '' description, '' prod_year, '' video_duration			
	FROM
		groups_customer_category a
	LEFT JOIN customer_category b ON a.customer_category_id = b.customer_category_id
	WHERE
		groups_id = '$channel_id'
	ORDER BY
		b.category_name
*/
/*
	SELECT 
		'-2' channel_id, c.content_id, c.title, c.video_length as video_duration, c.filename, c.video_thumbnail as thumbnail,
		c.description, c.prod_year, c.trailer_id, d.price,
		'0' as ulike, '0' as unlike, '0' as comment				
	from content c 
	LEFT JOIN content_vod d on c.content_id = d.content_id 
	WHERE c.content_id in 
		(
			SELECT content_id
			FROM
			content_customer_category
			WHERE customer_category_id ='$content_id'
		)			
	order by c.created desc
*/	
}