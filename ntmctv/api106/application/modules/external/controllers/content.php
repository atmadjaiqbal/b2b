<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 17 2014
	Channel controllers
	package: /external
*/

class Content extends REST_Controller{
	
	private $thumbnail_path;
	public function __construct(){
		parent::__construct();
        $this->load->model(array('apps_m', 'channel_m', 'content_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
		$this->config->load('config');

		$this->thumbnail_path = config_item('thumbnail_url');
	}	
	
	/*
	* Get contents list
	* method: GET
	* url: /content/list/$apps_id
	* argument: apps_id
	* parameters: channel_id, customer_id
	*/
    public function lists_get($app_id){
    	if(empty($app_id)) $this->response_failed(lang('missing_app_id'));
        $app = $this->apps_m->find($app_id);
        if(!$app) $this->response_failed(lang('error_app_id'));
        $channel_id = $this->input->get('channel_id');
        $customer_id = $this->input->get('customer_id');

         // get channel for channel_category_id == -2
		$stream_channel_id = $this->channel_m->get_channel_customercontent($app_id);

		// get contents
        $contents = $this->content_m->get_contents($app_id, $channel_id, $customer_id);
        $items = array();
        foreach ($contents as $row) {
        	$item = $row;
        	if($row->channel_category_id == '-2'){
        		$row->channel_id = $stream_channel_id; 
        	}

        	$item->thumbnail = $this->thumbnail_path . '?id='. ($row->thumbnail ? $row->thumbnail : config_item('img_not_found'));
        	unset($item->order_number);
        	$items[] = $item;
		}
    	$data = array(
    		'items' => $items,
    		'count_items' => count($items)
    	);
    	$this->response_success($data);
    }

}