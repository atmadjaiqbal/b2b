<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Zola Octanoviar
	Date: nov, 17 2014 22:20 
*/
class Product_m extends MY_Model {
	
	protected $table 	= 'menu_product';
	protected $key		= 'menu_id';

	public function __construct()
	{
		parent::__construct();
	}

	public function product_list($page, $limit, $apps_id, $device_id,  $menu_id, $genre_id, $crew_id, $search_key){
			$sql = "
				SELECT 
					menu_id, p.apps_id, p.product_id,'' channel_category_id, '' channel_id, '' channel_type_id, c.content_id, 
					IF(c.status = '1','channel','content') content_type,
					title, video_thumbnail thumbnail,  description, prod_year, video_length video_duration,
					(SELECT count(content_id) FROM content_like WHERE content_id=c.content_id AND ulike=1) as countlike,			
					(SELECT count(content_id) FROM content_comment WHERE content_id=c.content_id) as countcomment,				
					countviewer,d.icon_size, d.poster_size, c.created
				FROM 
					menu_product p 
				INNER JOIN content c on p.product_id = c.content_id and c.active='1'
				INNER JOIN apps d on p.apps_id = d.apps_id
				WHERE active='1' AND p.apps_id = '$apps_id'
			";
					
			if(!empty($menu_id)){
				$sql .= " AND p.menu_id = '$menu_id' ";
			}
		
			if(!empty($genre_id)){
				$sql .= " 
					AND p.product_id in (
						SELECT content_id FROM content_genre y
						WHERE y.genre_id = '$genre_id'
					) 
				";
			}
		
			if(!empty($crew_id)){
				$sql .= " 
					AND p.product_id in (
						SELECT content_id FROM content_crew y
						WHERE y.crew_id = '$crew_id'
					) 
				";
			}
		
			if(!empty($search_key)){
				$sql .= " 
					AND (
						LOWER(title) like LOWER('%$search_key%')
						OR content_id in (
							SELECT content_id FROM content_crew x, crew y
							WHERE LOWER(y.crew_name) like LOWER('%$search_key%') 
							AND x.crew_id = y.crew_id
						) 
					)
				";
			}
		
			if($device_id == '4'){
				//$sql .= " group by content_id ";
			}
			$sql .= " group by content_id ";
			
			$sql .= " ORDER BY c.created desc LIMIT $page, $limit ";
			$query = $this->db->query($sql);	
			$rows =  $query->result();
			foreach($rows as $row){
				$width = $row->poster_size;
				if ($device_id =='4') {
					$width = ((int)$row->countviewer >= 100) ? "350"  : "170";
				}
				
				$row->thumbnail  = $this->get_thumbnail_url($row->thumbnail) ."&w=".$width;
				
					$row->crew_category_list = $this->content_actor_list($row->content_id);
					$row->genre_list = $this->content_genre($row->content_id);
					$row->related_list = $this->content_related_list($row->content_id);
			}
			
			return $rows;
	}

	public function boxoffice_list($page, $limit, $apps_id, $device_id  ){
			$sql = "
				SELECT 
					m.menu_id, p.apps_id, p.product_id,'' channel_category_id, '' channel_id, '' channel_type_id, c.content_id, 
					IF(c.status = '1','channel','content') content_type,
					c.title, video_thumbnail thumbnail,  description, prod_year, video_length video_duration,
					(SELECT count(content_id) FROM content_like WHERE content_id=c.content_id AND ulike=1) as countlike,			
					countviewer,d.icon_size, d.poster_size
				FROM 
					menu m
        INNER JOIN menu_product p on m.menu_id = p.menu_id
				INNER JOIN content c on p.product_id = c.content_id and m.apps_id = p.apps_id 
				INNER JOIN apps d on p.apps_id = d.apps_id
				WHERE c.active='1' AND m.apps_id = '$apps_id'
			";
					
		
			$sql .= " group by content_id ";
			
			$sql .= " ORDER BY c.countviewer desc LIMIT $page, $limit ";
			$query = $this->db->query($sql);	
			$rows =  $query->result();
			foreach($rows as $row){
				$width = $row->poster_size;
				if ($device_id =='4') {
					$width = ((int)$row->countviewer >= 100) ? "350"  : "170";
				}
				
				$row->thumbnail  = $this->get_thumbnail_url($row->thumbnail) ."&w=".$width;
				
					$row->crew_category_list = $this->content_actor_list($row->content_id);
					$row->genre_list = $this->content_genre($row->content_id);
			}
			
			return $rows;
	}

  public function product_popular($page, $limit, $apps_id, $device_id, $sort="popular") {
		$sql = "SELECT m.menu_id, p.apps_id, p.product_id,'' channel_category_id, '' channel_id, '' channel_type_id, 
		        c.content_id, IF(c.status = '1','channel','content') content_type, c.title, video_thumbnail thumbnail,  
		        description, prod_year, video_length video_duration,
					  (SELECT count(content_id) FROM content_like WHERE content_id=c.content_id AND ulike=1) as countlike,			
					  countviewer,d.icon_size, d.poster_size
				    FROM menu m
            INNER JOIN menu_product p on m.menu_id = p.menu_id
				    INNER JOIN content c on p.product_id = c.content_id and m.apps_id = p.apps_id 
				    INNER JOIN apps d on p.apps_id = d.apps_id
				    WHERE c.active='1' AND m.apps_id = '$apps_id'";
							
		$sql .= " group by content_id ";			
		if($sort == "popular")
		{
			$sql .= " ORDER BY c.countviewer DESC ";
		} else {
			$sql .= " ORDER BY c.created DESC ";
		}
		$sql .= " LIMIT $page, $limit ";
		$query = $this->db->query($sql);	
		$rows =  $query->result();
		foreach($rows as $row){
				$width = $row->poster_size;
				if ($device_id =='4') {$width = ((int)$row->countviewer >= 100) ? "350"  : "170";}				
				$row->thumbnail  = $this->get_thumbnail_url($row->thumbnail) ."&w=".$width;				
				$row->crew_category_list = $this->content_actor_list($row->content_id);
				$row->genre_list = $this->content_genre($row->content_id);
		}			
		return $rows;  	
  } 

	public function product_detail($apps_id, $menu_id, $content_id) {		
		$sql_join = "";
		if (!empty($apps_id) && !empty($menu_id)) {
			$sql_join = " LEFT JOIN menu_product p On p.product_id = c.content_id and p.menu_id = '$menu_id' and p.apps_id = '$apps_id' ";
		}
		$textquery = "
			SELECT 
				'' channel_id, c.content_id, c.title, c.video_length as video_duration, c.filename, c.video_thumbnail as thumbnail,
				c.description, c.privacy, c.prod_year, c.created, c.trailer_id, cv.price,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_id' AND ulike=1) as ulike,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_id' AND ulike=-1) as unlike,
				(SELECT count(content_id) FROM content_comment WHERE content_id='$content_id') as comment				
			FROM content c 
			LEFT  JOIN content_vod cv on c.content_id = cv.content_id
			$sql_join 
			WHERE c.active='1' 
				AND c.content_id = '$content_id'
		 	";
		$query = $this->db->query($textquery);	
		$row =  $query->row();
		if($row){
			$row->thumbnail  = $this->get_thumbnail_url($row->thumbnail);
			$row->crew_category_list = $this->content_actor_list($row->content_id);
			$row->genre_list = $this->content_genre($row->content_id);
			$row->photo_list = $this->content_photo($row->content_id);
			$row->related_list = $this->content_related_list($row->content_id);
			$row->document_list = $this->content_document($row->content_id);
		}
		return $row;
	}

	public function comment_list($page, $limit, $product_id ){
			$sql = "
				SELECT 	comment_id, content_id, display_name customer_name, comment_text, cc.status, created
				FROM 
					content_comment cc 
				LEFT JOIN customer c on cc.customer_id = c.customer_id
				WHERE cc.content_id = '$product_id'
				ORDER BY cc.created desc LIMIT $page, $limit 
			";
			$query = $this->db->query($sql);	
			$rows =  $query->result();
			return $rows;
	}

	public function comment_save($product_id, $customer_id, $comment_text){
		$comment_id = uniqid(rand(), false);
		$sql = "
			INSERT INTO content_comment(comment_id, content_id, customer_id, comment_text, status, created)
			VALUES ('$comment_id', '$product_id', '$customer_id', '$comment_text', '1', now())
		 	";
		$query = $this->db->query($sql);	
		$response = $this->comment_list('0','12','$product_id');
		return $response;
	}

	public function like_list($page, $limit, $product_id ){
			$sql = "
				SELECT 	content_id, display_name customer_name, created
				FROM 
					content_like cl
				LEFT JOIN customer c on cl.customer_id = c.customer_id
				WHERE cl.content_id = '$product_id' and ulike ='1'
				ORDER BY cl.created desc LIMIT $page, $limit 
			";
			$query = $this->db->query($sql);	
			$rows =  $query->result();
			return $rows;
	}
	
	public function like_total($product_id){
		$sql = "SELECT	COUNT(content_id) AS 'total_like'
		        FROM content_like cl
            WHERE cl.content_id = '$product_id' AND ulike ='1'";
    $query = $this->db->query($sql);
    $rows = $query->row();
    return $rows->total_like;        		
	}

	public function like_save($product_id, $customer_id){
		$sql  = "
			SELECT content_id
			FROM content_like
			WHERE content_id='$product_id' AND customer_id = '$customer_id' 
		 	";
		$rows = $this->db->query($sql)->row();	
		if(!$rows){
			$sql = "
				INSERT INTO content_like(content_id, customer_id, ulike, created)
				VALUES ('$product_id', '$customer_id', '1', now())
			 	";
			$query = $this->db->query($sql);	
			$response = '1';
		} else {
			$response =  '0';
		}
		return $response;
	}


	public function channel_list($page, $limit, $apps_id, $channel_category_id){
			$sql = "
				SELECT '' channel_category_id, c.channel_id, c.channel_type_id,
					c.channel_id content_id, 'channel' content_type, alias title, c.thumbnail,
					substr(channel_descr, 0, 100) description, YEAR(CURDATE()) prod_year, '86400' video_duration
				FROM
					menu_product p
				INNER join menu m on p.menu_id = m.menu_id 
				INNER JOIN channel c on p.product_id = c.channel_id 
				WHERE c.status ='1' AND p.apps_id = '$apps_id' 
			";
	
			if(!empty($channel_category_id)){
				$sql .= " AND c.channel_id in (
					SELECT acc.channel_id FROM apps_channel_category acc  WHERE acc.apps_id='$apps_id'  AND acc.channel_category_id = '$channel_category_id')
				";
			}
			
			$sql .= " 
				GROUP BY c.channel_id 
				ORDER BY p.order_number LIMIT $page, $limit 
			";
			$query = $this->db->query($sql);	
			$rows =  $query->result();
			foreach($rows as $row){
				$row->thumbnail  = $this->get_thumbnail_url($row->thumbnail) ."&w=170";
			}
			
			return $rows;
	}
	

	public function channel_category_list($apps_id){
			$sql = "
				SELECT ac.apps_id, ac.channel_category_id, cc.category_name
				FROM apps_category ac
				INNER JOIN channel_category cc on ac.channel_category_id = cc.channel_category_id
				WHERE ac.apps_id='$apps_id'
				ORDER BY ac.order_number
			";
					
			$query = $this->db->query($sql);	
			$rows =  $query->result();
			return $rows;
	}
	
	
	public function content_actor_list($content_id) {
		$textquery = "
			SELECT 
				a.crew_category_id, b.category_name FROM content_crew a, crew_category b
			WHERE a.content_id='$content_id'
			AND a.crew_category_id = b.crew_category_id
			GROUP BY a.crew_category_id, b.category_name
			ORDER BY b.order_number;
		 	";
		
		$query = $this->db->query($textquery);	
		$crew_category_rows =  $query->result();

		foreach($crew_category_rows as $crew_category){
			$textquery = "
				SELECT 
					b.crew_id, b.crew_name 
				FROM content_crew a, crew b
				WHERE a.content_id='$content_id'
				AND a.crew_category_id = '$crew_category->crew_category_id'
				AND a.crew_id = b.crew_id
			 	";
		
			$query = $this->db->query($textquery);	
			$crew_rows =  $query->result();
			$crew_category->crew_list = $crew_rows;
		}
		

		return $crew_category_rows;				
	}
	
	
	public function content_related_list($content_id) {
		$textquery = "
			SELECT 
				a.content_id, a.content_related_id, b.title, b.video_length as video_duration, b.video_thumbnail as thumbnail
			FROM content_related a, content b
			WHERE a.content_id='$content_id'
			AND a.content_related_id = b.content_id
			ORDER BY a.order_number, b.title;
		 	";
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();

		foreach($rows as $row){
			$row->thumbnail  = $this->get_thumbnail_url($row->thumbnail);
		}
		return $rows;				
	}
	
	public function content_genre($content_id) {
		$textquery = "
			SELECT a.genre_id, b.genre_name
			FROM content_genre a, genre b
			WHERE a.content_id = '$content_id'
			AND a.genre_id = b.genre_id
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;				
	}
	
	
	public function content_photo($content_id) {
		$textquery = "
			SELECT a.content_id, a.filename
			FROM content_photo a
			WHERE a.content_id = '$content_id'
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			$row->thumbnail  = $this->get_thumbnail_url($row->filename);
		}
		return $rows;				
	}
	
	public function genre_list($page, $limit) {
		$textquery = "
			SELECT genre_id, genre_name 
			FROM genre
			ORDER BY genre_name LIMIT $page, $limit 
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;				
	}

  public function content_document($content_id) {
      $sql = "SELECT cd.content_id, cd.document_id, d.apps_id, d.title, d.filename, d.type, d.size
              FROM content_document cd INNER JOIN documents d ON cd.document_id = d.document_id
              WHERE cd.content_id = '$content_id'";
      $query = $this->db->query($sql);
      $rows = $query->result();
      return $rows;
  }
	
	public function get_document($document_id) {
      $sql = "SELECT cd.content_id, cd.document_id, d.apps_id, d.title, d.filename, d.type, d.size
              FROM content_document cd INNER JOIN documents d ON cd.document_id = d.document_id
              WHERE cd.document_id = '$document_id'";
      $query = $this->db->query($sql);
      $rows = $query->row();
      return $rows;		
	}

}