<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: nov, 17 2014
	Authentication Application
	Original copy from signup functions.
*/

class Menu extends REST_Controller{

	public function __construct(){
		parent::__construct();
    $this->load->model(array('menu_m'));
    $this->load->model(array('apps_m'));
		$this->load->helper('url');		
		$this->lang->load('default');

		$this->apps_id = $this->input->post("apps_id");
		$this->lang = $this->input->post("lang");
		$this->customer_id = $this->input->post("customer_id");		
		$this->device_id = $this->input->post("device_id");		
		
		// checking params
    if(empty($this->apps_id)) $this->response_failed('missing_app_id');        
    if(empty($this->lang)) $this->lang =  $this->config->item('lang');        
    if(empty($this->customer_id)) $this->response_failed('missing_customer_id');        
    if(empty($this->device_id)) $this->response_failed('missing_device_id');        
	}	

	/**
	* List of Menu
	* method: POST
	* url: /menu_list
	* required parameters: apps_id, customer_id, version
	*/
	public function menu_list_post(){
		$menu_id = $this->input->post("menu_id");
    $menu_type = $this->input->post("menu_type");
    $page = $this->input->post("page");
    $limit = $this->input->post("limit");
		if(empty($menu_id))  $menu_id = '0';
    if(empty($menu_type))  $menu_type = 'menu';
    if(empty($page))  $page = 0;
    if(empty($limit))  $limit = 0;

		$menu_list = $this->menu_m->menu_list($this->apps_id,$this->device_id,  $menu_id,  $menu_type, $page, $limit);
		$response = array(
			"menu_list" => $menu_list
    );
		$this->response_success($response);
	}

	public function menu_listing_post(){
		$menu_id = $this->input->post("menu_id");
    $menu_type = $this->input->post("menu_type");
    $page = $this->input->post("page");
    $limit = $this->input->post("limit");
		if(empty($menu_id))  $menu_id = '0';
    if(empty($menu_type))  $menu_type = 'menu';
    if(empty($page))  $page = 0;
    if(empty($limit))  $limit = 0;

		$menu_list = $this->menu_m->menu_list2($this->apps_id,$this->device_id,  $menu_id,  $menu_type, $page, $limit);
		$response = array(
			"menu_list" => $menu_list
    );
		$this->response_success($response);
	}
	
	public function apps_faq_post()
	{
		// $apps_id = $this->input->post("apps_id");		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));		
		$faqHead = $this->menu_m->freqaskquest($this->apps_id);
		$response = array(
		   "faq_categories" => $faqHead
		);
		$this->response_success($response);
	}
	
	public function faq_detail_post()
	{
		$cat_id = $this->input->post("faq_categories_id");		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));		
	  $faqdetail = $this->menu_m->freqdetail($cat_id);			
		$response = array(
		   "faq_detail" => $faqdetail
		);
		$this->response_success($response);				
	}	

    public function apps_upload_list_post()
    {
        $downapps = $this->apps_m->apps_upload_list($this->apps_id, 3);
        $response = array(
          "list_download" => $downapps
        );
        $this->response_success($response);
    }

    public function update_apps_download_post()
    {
    	  $upload_id = $this->input->post("uploadid");
        $countdown = $this->input->post("countdown");
        $countdownload = $this->apps_m->update_apps_download($upload_id, $countdown);
        $response = array(
          "count_download" => $countdownload
        );
        $this->response_success($response);
    }	
    
    public function apps_upload_detail_post()
    {
        $upload_id = $this->input->post("uploadid");
        $appdownload = $this->apps_m->apps_upload_detail($upload_id);
        $response = array(
            "app_download" => $appdownload
        );
        $this->response_success($response);
    }	
	
}