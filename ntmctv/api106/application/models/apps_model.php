<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apps_model extends MY_Model {
	
	// ====================== add by htridianto, Nov 11 2014
	protected $table 	= 'apps';
	protected $key		= 'apps_id';

	public function __construct()
	{
		parent::__construct();
	}
	// ====================== end of htridianto ==================
	
	public function auth($apps_id, $version, $customer_id) {	
		$textquery = "
			SELECT 
				a.apps_id, app_name, thumbnail, descr, version, status, message,download_page,  iscom, product_id, subs_url, icon_size, font_size, time_trial
			FROM apps a, apps_version b
			WHERE a.apps_id = b.apps_id
		 	AND a.apps_id = '$apps_id'
		 	AND version = '$version'
		 	";

		$query = $this->db->query($textquery);	
		$row = $query->row();
		
		if($row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $row->thumbnail ;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $row;
	}
	
	
	public function apps_category_list($apps_id, $customer_id, $lang){
		
		$textquery = "
			SELECT 
				b.channel_category_id, category_name, thumbnail, b.tab_type, b.link, show_tab_menu, show_dd_menu
			FROM apps_category a, channel_category b
			WHERE apps_id = '$apps_id'
			AND a.channel_category_id = b.channel_category_id
			ORDER BY a.order_number, category_name
		 	";
			
		if($lang == "zh_CN"){
			$textquery = "
				SELECT 
					b.channel_category_id, category_name_chn as category_name, thumbnail, b.tab_type, b.link, show_tab_menu, show_dd_menu
				FROM apps_category a, channel_category b
				WHERE apps_id = '$apps_id'
				AND a.channel_category_id = b.channel_category_id
				ORDER BY a.order_number, category_name
			 	";
		}
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
			$row->channel_list = $this->apps_channel_category_list($apps_id, $row->channel_category_id, $customer_id, $customer_id);
			if ($row->channel_category_id == '-2') {
				$row->channel_list = $this->apps_groups_list($apps_id, $row->channel_category_id);				
			}

		}
		return $rows;
	}
	
	
	public function apps_channel_category_list($apps_id, $channel_category_id, $customer_id){
		$textquery = "
			SELECT 
				a.channel_id, channel_name, alias, thumbnail, status, channel_type_id, a.channel_category_id, d.order_number, now() as server_time, d.link
			FROM apps_channel_category a, apps_channel b, apps_category c, channel d
			WHERE a.apps_id = '$apps_id'
			AND a.channel_category_id = '$channel_category_id'
			AND d.status = 1
			AND a.apps_id = b.apps_id
			AND a.apps_id = c.apps_id
			AND a.channel_id = b.channel_id
			AND a.channel_category_id = c.channel_category_id
			AND a.channel_id = d.channel_id
		 	";
			
			
		if(!empty($customer_id) && $channel_category_id == '-1'){
			$textquery = $textquery . "
				UNION ALL
				SELECT 
					a.channel_id, channel_name, alias, thumbnail, status, channel_type_id, '-1' as channel_category_id, b.order_number, now() as server_time, b.link
				FROM 
				channel_favorite a, channel b
				WHERE a.channel_id = b.channel_id
				AND a.apps_id = '$apps_id'
				AND a.customer_id = '$customer_id'
				";
		}
		$textquery = $textquery . " ORDER BY order_number, channel_name ";
		
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;
	}

	public function apps_groups_list($apps_id, $channel_category_id ){
		$textquery = "
			SELECT 
				b.groups_id channel_id, b.groups_name channel_name, b.groups_name  alias, 
				(SELECT thumbnail from channel_category  where channel_category_id = '$channel_category_id') thumbnail, '1' status, '3' channel_type_id, '3' channel_category_id, '0' order_number, now() as server_time, '' link
			FROM groups_apps a
				INNER JOIN groups b on a.groups_id = b.groups_id
			WHERE a.apps_id = '$apps_id'
			ORDER BY b.groups_name
		 	";
			
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;
	}
	
	
	public function apps_schedule_type_list($apps_id, $lang){
		$textquery = "
			SELECT 
				b.schedule_type_id, schedule_type_name, thumbnail
			FROM apps_schedule_type a, schedule_type b
			WHERE apps_id = '$apps_id'
			AND a.schedule_type_id = b.schedule_type_id
			ORDER BY schedule_type_id
		 	";

		if($lang == "zh_CN"){
			$textquery = "
				SELECT 
					b.schedule_type_id, schedule_type_name_chn as schedule_type_name, thumbnail
				FROM apps_schedule_type a, schedule_type b
				WHERE apps_id = '$apps_id'
				AND a.schedule_type_id = b.schedule_type_id
				ORDER BY schedule_type_id
			 	";
		}

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;
	}
	
	
	public function apps_res_list($apps_id, $device_id, $lang){
		$textquery = "
			SELECT 
				b.res_id, res_name, bw_min
			FROM apps_res a, res b
			WHERE apps_id = '$apps_id'
			AND device_id = '$device_id'
			AND b.res_id = a.res_id
			ORDER BY order_number;
		 	";

		if($lang == "zh_CN"){
			$textquery = "
				SELECT 
					b.res_id, res_name_cn as res_name, bw_min
				FROM apps_res a, res b
				WHERE apps_id = '$apps_id'
				AND device_id = '$device_id'
				AND b.res_id = a.res_id
				ORDER BY order_number;
				 ";
		}


		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;
	}
	
	/*
	public function apps_feature_list($apps_id){
		$textquery = "
			SELECT 
				b.res_id, res_name, bw_min
			FROM apps_res a, res b
			WHERE apps_id = '$apps_id'
			AND device_id = '$device_id'
			AND b.res_id = a.res_id
			ORDER BY order_number;
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;
	}
	*/
	
	public function apps_mobile_id_save($apps_id, $version, $mobile_id, $device_id, $screen_size, $remote_host, $latitude, $longitude){
		$textquery = "
			INSERT INTO apps_mobile_id (
				apps_id, version, mobile_id, created, device_id, screen_size, remote_host, latitude, longitude
			) VALUES (
				'$apps_id', '$version', '$mobile_id', now(), '$device_id', '$screen_size', '$remote_host', '$latitude', '$longitude'
			)
		 	";
		$query = $this->db->query($textquery);	
	}
	
	
	public function apps_feature_list($apps_id){
		$textquery = "
			SELECT 
				b.feature_id
			FROM apps_feature a, feature b
			WHERE apps_id = '$apps_id'
			AND a.feature_id = b.feature_id
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;
	}
	
	
	
	public function menu_list($apps_id, $customer_id, $lang){
		$textquery = "
			SELECT 
				menu_id, title, thumbnail 
			FROM menu
			WHERE apps_id = '$apps_id'
			AND parent_menu_id is null
			ORDER BY order_number, title
		 	";
			
		if($lang == "zh_CN"){
			$textquery = "
				SELECT 
					menu_id, title_chn as title, thumbnail 
				FROM menu
				WHERE apps_id = '$apps_id'
				ORDER BY order_number, title
			 	";
		}
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
			$row->sub_menu_list = $this->sub_menu_list($apps_id, $row->menu_id, $customer_id, $lang);
		}
		return $rows;
	}
	
	
	public function sub_menu_list($apps_id, $menu_id, $customer_id, $lang){
		$textquery = "
			SELECT 
				menu_id, title, thumbnail 
			FROM menu
			WHERE apps_id = '$apps_id'
			AND parent_menu_id = '$menu_id'
			ORDER BY order_number, title
		 	";
			
		if($lang == "zh_CN"){
			$textquery = "
				SELECT 
					menu_id, title_chn as title, thumbnail 
				FROM menu
				WHERE apps_id = '$apps_id'
				AND parent_menu_id = '$menu_id'
				ORDER BY order_number, title
			 	";
		}
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;
	}
	
	
}

?>
