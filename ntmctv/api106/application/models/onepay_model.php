﻿<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Onepay_model extends MY_Model {
	
	
	
	public function get_content($content_id){
		$textquery = "
			SELECT content_id, title, description, video_thumbnail, content_category_id, prod_house_id, prod_year, created
			FROM content 
			WHERE content_id = '$content_id'
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->row();
		$rows->crew = array();
		$rows->genre = array();
		$rows->video_thumbnail = 'http://alpha.moviebay.com/uploads/'.$rows->video_thumbnail ;
		if ($rows) {
			$textquery = "
				SELECT 
						a.crew_id, b.crew_name, a.crew_category_id, c.category_name
			FROM content_crew a 
					INNER JOIN crew b on a.crew_id = b.crew_id 
					INNER JOIN crew_category c on a.crew_category_id = c.crew_category_id
				WHERE content_id = '$content_id'
			 	";
			$query = $this->db->query($textquery);	
			$crew =  $query->result();
			$rows->crew = $crew;

			$textquery = "
				SELECT 
					a.genre_id, b.genre_name
				FROM content_genre a 
					INNER JOIN genre b on a.genre_id = b.genre_id 
				WHERE content_id = '$content_id'
			 	";
			$query = $this->db->query($textquery);	
			$genre =  $query->result();
			$rows->genre = $genre;
		}


		return $rows;
	}
	
	function content_list($limit){
		$textquery = "
			SELECT 
					content_id, title, description, video_thumbnail, content_category_id, prod_house_id, prod_year, created 
			FROM content
			WHERE prod_house_id = '99'
			LIMIT 0, $limit
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();		
		if ($rows) {
			foreach($rows as $row ){
				$textquery = "
					SELECT 
							a.crew_id, b.crew_name, a.crew_category_id, c.category_name
					FROM content_crew a 
						INNER JOIN crew b on a.crew_id = b.crew_id 
						INNER JOIN crew_category c on a.crew_category_id = c.crew_category_id
					WHERE content_id = '".$row->content_id."'
				 	";
				$query = $this->db->query($textquery);	
				$crew =  $query->result();
				$row->crew = $crew;

				$textquery = "
					SELECT 
						a.genre_id, b.genre_name
					FROM content_genre a 
						INNER JOIN genre b on a.genre_id = b.genre_id 
					WHERE content_id = '".$row->content_id."'
				 	";
				$query = $this->db->query($textquery);	
				$genre =  $query->result();
				$row->genre = $genre;
			}			
		}
		
		return $rows;		
	}

	function crew_list($limit){
		$textquery = "
			SELECT 
					a.crew_id, b.crew_name, a.crew_category_id, c.category_name
			FROM content_crew a 
				INNER JOIN crew b on a.crew_id = b.crew_id 
				INNER JOIN crew_category c on a.crew_category_id = c.crew_category_id
			ORDER BY b.crew_name 
			LIMIT 0, $limit
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		return $rows;		
	}
		
	function content_list_crew($crew_id, $limit){
		$textquery = "
			SELECT 
					a.crew_id, b.content_id, b.title, left(b.description,125) description 
		FROM content_crew a 
				INNER JOIN content b on a.content_id = b.content_id 
				INNER JOIN crew c on a.crew_id = c.crew_id 
			WHERE a.crew_id = '$crew_id'
			LIMIT 0, $limit
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();		
		return $rows;		
	}


	function genre_list($limit){
		$textquery = "
			SELECT genre_id, genre_name 
			FROM genre  
			ORDER BY genre_name 
		";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		return $rows;		
	}
		
		
	function content_list_genre($genre_id, $limit){
		$textquery = "
			SELECT 
					a.genre_id, b.content_id, b.title, left(b.description,125) description 
		FROM content_genre a 
				INNER JOIN content b on a.content_id = b.content_id 
				INNER JOIN genre c on a.genre_id = c.genre_id 
			WHERE a.genre_id = '$genre_id'
			LIMIT 0, $limit
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();		
		return $rows;		
	}


	function test(){
		return 'xx';
	}	
}

?>
