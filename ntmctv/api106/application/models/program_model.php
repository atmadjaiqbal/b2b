<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Program_model extends MY_Model {
	


	public function program_vod_category_list() {		
		$textquery = "
			SELECT 1 as order_number, '' as program_category_id, 'All' as category_name
			UNION
			SELECT 2 as order_number,  'myvod' as program_category_id, 'My VOD' as category_name
			UNION
			SELECT 
				 3 as order_number, program_category_id, category_name
			FROM program_category 
			ORDER BY order_number, category_name
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;
	}
	

	public function program_vod_list($page, $limit, $apps_id, $channel_id, $program_category_id, $search_key) {		
		$textquery = "
			SELECT 
				a.program_id, program_name, program_descr, program_category_id, thumbnail, 
				(select count(program_content_id) from program_content where program_id = a.program_id) as countofvideos,
				IFNULL((SELECT (sum(video_length)) FROM program_content x, content y where x.program_id = a.program_id AND x.content_id = y.content_id), 0) as duration,
				(select category_name from program_category where program_category_id = b.program_category_id) as category_name,
				created, a.currency, a.price
			FROM program_vod a, program b 
			WHERE apps_id='$apps_id'
			AND channel_id='$channel_id'
			AND a.program_id = b.program_id
			AND LOWER(program_name) like LOWER('%$search_key%')
		 	";
			
		if($program_category_id != ""){
			$textquery = $textquery . " AND program_category_id = '$program_category_id'";
		}
		$textquery = $textquery . "	ORDER BY created DESC ";
		
		if(!empty($page) || !empty($limit)){
			$textquery .= "LIMIT $page, $limit";
		}

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;
	}
	

	public function program_content_list($program_id) {		
		$textquery = "
			SELECT 
				b.content_id, title, video_length as video_duration, c.filename, c.video_thumbnail as thumbnail
			from program a, program_content b, content c 
			WHERE a.program_id='$program_id'
			AND a.program_id = b.program_id
			AND b.content_id = c.content_id
			ORDER BY b.order_number
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;
	}
	

	public function content_res_detail($content_id, $device_id, $res_id) {	
		$textquery = "
			SELECT 
				* 
			FROM content_res 
			WHERE content_id='$content_id' 
			AND device_id='$device_id' 
			AND res_id='$res_id'
		 	";

		$query = $this->db->query($textquery);	
		$row =  $query->row();
		return $row;
	}

	

	
}

?>
