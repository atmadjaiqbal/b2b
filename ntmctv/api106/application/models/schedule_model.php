<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Schedule_model extends MY_Model {
	

	public function schedule_today($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id) {		
		$textquery = "
			SELECT 
				a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
				b.thumbnail, a.start_date_time,
				CONCAT(SUBSTRING(DATE_FORMAT(a.start_date_time, '%W'), 1, 3), ' ', DATE_FORMAT(a.start_date_time, '%h:%i %p')) as start_time_text,
				DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
			FROM schedule a, program b
			WHERE a.program_id = b.program_id 
			AND a.channel_id = '$channel_id'
			AND ( a.end_date_time between now() and DATE_ADD(now(),INTERVAL 1 DAY) )
			AND	schedule_type_id = $schedule_type_id
		 	";


		if(!empty($customer_id) && $schedule_type_id == '3'){
			$textquery = $textquery . "
				UNION ALL
				SELECT 
					a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
					b.thumbnail, c.start_date_time,
					DATE_FORMAT(DATE_ADD(c.created,INTERVAL 1 DAY), '%b %d') as start_time_text,
					'+7 days' as end_time_text,
					schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 0 as enable_record, 0 as enable_remove, 1 enable_play
				FROM schedule a, program b, schedule_record c
				WHERE a.schedule_id = c.schedule_id 
				AND a.channel_id = c.channel_id
				AND a.program_id = b.program_id
				AND c.customer_id = '$customer_id'
				AND (now() between c.created AND DATE_ADD(c.created,INTERVAL 7 DAY))
				";
		}
		$textquery = $textquery . " ORDER BY start_date_time ";


		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		
		return $rows;
	}
	



	public function schedule_list($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id) {		
		$textquery = "
			SELECT 
				a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
				b.thumbnail, a.start_date_time,
				DATE_FORMAT(a.start_date_time, '%W %h:%i %p') as start_time_text, DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
			FROM schedule a, program b
			WHERE a.program_id = b.program_id 
			AND a.channel_id = '$channel_id'
			AND ( a.end_date_time between '$start_date' and '$end_date' )
			AND	schedule_type_id = $schedule_type_id
		 	";


		if(!empty($customer_id) && $schedule_type_id == '3'){
			$textquery = $textquery . "
				UNION ALL
				SELECT 
					a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
					b.thumbnail, c.start_date_time,
					DATE_FORMAT(c.start_date_time, '%W %h:%i %p') as start_time_text, DATE_FORMAT(c.end_date_time, '%h:%i %p') as end_time_text,
					schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 0 as enable_record, 1 as enable_remove, 1 enable_play
				FROM schedule a, program b, schedule_record c
				WHERE a.schedule_id = c.schedule_id 
				AND a.channel_id = c.channel_id
				AND a.program_id = b.program_id
				AND c.customer_id = '$customer_id'
				";
		}
		$textquery = $textquery . " ORDER BY start_date_time ";


		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		
		return $rows;
	}
	
	
	public function schedule_now($channel_id, $schedule_type_id) {		
		$textquery = "
			SELECT 
				a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
				b.thumbnail, a.start_date_time,
				DATE_FORMAT(a.start_date_time, '%h:%i %p') as start_time_text, DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
			FROM schedule a, program b
			WHERE a.program_id = b.program_id 
			AND a.channel_id = '$channel_id'
			AND a.schedule_type_id = $schedule_type_id
			AND now() between start_date_time and end_date_time
			ORDER BY a.created
			LIMIT 0, 1
		 	";

		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	
	public function schedule_next($channel_id, $schedule_type_id) {		
		$textquery = "
			SELECT 
				a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
				b.thumbnail, a.start_date_time,
				DATE_FORMAT(a.start_date_time, '%h:%i %p') as start_time_text, DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
			FROM schedule a, program b
			WHERE a.program_id = b.program_id 
			AND a.channel_id = '$channel_id'
			AND a.schedule_type_id = $schedule_type_id
			AND start_date_time > now() 
			ORDER BY a.created
			LIMIT 0, 1
		 	";

		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	
	public function record($apps_id, $channel_id, $schedule_id, $customer_id){
		$textquery = "
			INSERT INTO schedule_record (apps_id, channel_id, schedule_id, customer_id, start_date_time, end_date_time, status, created) 
			VALUES ('$apps_id', '$channel_id', '$schedule_id', '$customer_id', now(), now(), 0, now())
			";
		$query = $this->db->query($textquery);	
	}
	
	
	
	
	
	/*
	public function content_list($program_id) {	
		$textquery = "
			select 
				a.content_id, title, video_length as duration, status, video_codec, video_thumbnail,
				filename
			from program_content a, content b
			where a.content_id = b.content_id
			and a.program_id = '$program_id'
			order by a.order_number
		 	";
		$query = $this->db->query($textquery);	
		return $query->result();
	}
	*/
	
	
	

	
}

?>
