var host = '';
$(function(){

    content_cctv('#content_menu');
    $('#cctv_load_more').click(function(e){
        e.preventDefault();
        content_cctv('#content_menu');
    });

    content_container('#content_container');
    $('#content_load_more').click(function(e){
        e.preventDefault();
        content_container('#content_container');
    });

    comment_list('#comment');

});

function content_cctv(holder)
{
    var the_loader = '<div id="loadmore-loader" class="loader"></div>';
    //$(holder).append(the_loader);
    $('#cctv_load_more').text('');
    $('#load_more_place').append(the_loader);

    $('#loadmore-loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var menu_id = $(holder).data('menu_id');

    $.get(Settings.base_url+'content/get_cctv_categories/'+menu_id+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);
        $('#loadmore-loader').remove();
        $('#cctv_load_more').text('Load More');
    });
}

function view_peta(link)
{
    $('#listsubmenu').html('');
    $('#listsubmenu').append('<iframe style="width: 100%;height: 500px;" src="'+link+'"></iframe>');
    $('.view-page').html('');
    $('.view-page').append('<iframe style="width: 100%;height: 500px;" src="'+link+'"></iframe>');
}

function get_content_submenu(menu_id, menutype)
{
    $.get(Settings.base_url+'content/get_submenu/'+menu_id+'/'+menutype, function(data){
        $('#listsubmenu').html('');
        $('#listsubmenu').append(data);
    });
}

function get_content_menu(menu_id, menutype)
{
    $.get(Settings.base_url+'content/get_menu/'+menu_id+'/'+menutype, function(data){
        $('.sidebar-left').html('');
        $('.sidebar-left').append(data);
    });
}

function replace_page(menu_id, menutype)
{
    $.get(Settings.base_url+'content/get_fullpage/'+menu_id+'/'+menutype, function(data){
        $('.view-page').html('');
        $('.view-page').append(data);
    });
}

function get_content_submenu2(menu_id, menutype)
{
    $.get(Settings.base_url+'content/get_submenu2/'+menu_id+'/'+menutype, function(data){
        $('#listsubmenu2').html('');
        $('#listsubmenu2').append(data);
    });
}

function content_container(holder)
{
    var the_loader = '<div id="loadmore-loader" class="loader"></div>';
    //$(holder).append(the_loader);
    $('#content_load_more').text('');
    $('#load_more_place').append(the_loader);

    $('#loadmore-loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var categories = $(holder).data('categories');

    $.get(Settings.base_url+'content/get_content_categories/'+categories+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);
        $('#loadmore-loader').remove();
        $('#content_load_more').text('Load More');
    });
}


function vod_list(holder)
{
    var the_loader = '<div id="loadmore-loader" class="loader"></div>';
    //$(holder).append(the_loader);
    $('#content_load_more').text('');
    $('#load_more_place').append(the_loader);

    $('#loadmore-loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var channel_id = $(holder).data('channelid');

    $.get(Settings.base_url+'content/vodlist/'+channel_id+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#loadmore-loader').remove();
        $('#content_load_more').text('Load More');

    });

}

function liketotal(holder)
{
    var id = $(holder).data('id');
    var uri = Settings.base_url+'content/total_like/'+ id;
    $.get(uri, function(data){
        $(holder).html('');
        $(holder).append(data);
        console.log(id+' total : '+data);
    });
}

function comment_list(holder)
{
    var id = $(holder).data('id');
    var page = $(holder).data('page');
    var uri = Settings.base_url+'content/content_comment/'+ id + '/'+page;
    $.get(uri, function(data){
        $(holder).append(data);
        $(holder).data('page', parseInt(page) + 1);
    });

}