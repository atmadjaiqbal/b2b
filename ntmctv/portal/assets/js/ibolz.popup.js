/*
 author: istockphp.com
 */
jQuery(function($) {

    $("a.addnew-form").click(function() {
        loading(); // loading
        setTimeout(function(){ // then show popup, deley in .5 second
            showaddnew_form(); // function show popup
        }, 500); // .5 second
        return false;
    });

    $("a.edit-form").click(function() {
        loading(); // loading

        var $this = $(this);
        var id = $(this).data('id');
        var name = $(this).data('name');
        var desc = $(this).data('desc');
        var set_active = $(this).data('active');

        console.log(name);

        $('#cat_id').val(id);
        $('#cat_name').val(name);
        $('#cat_desc').val(desc);
        if(set_active == '1') { $('#cat_active').attr("checked","checked"); } else { $('#cat_active').attr("checked",false); }

        setTimeout(function(){ // then show popup, deley in .5 second
            showedit_form(); // function show popup
        }, 500); // .5 second
        return false;
    });


    /* event for close the popup ---
     $("div.close").hover(
     function() {
     $('span.ecs_tooltip').show();
     },
     function () {
     $('span.ecs_tooltip').hide();
     }
     );
     */

    $("div.close").click(function() {
        disablePopup();  // function close pop up
    });

    $(this).keyup(function(event) {
        if (event.which == 27) { // 27 is 'Ecs' in the keyboard
            disablePopup();  // function close pop up
        }
    });

    $('a.livebox').click(function() {
        alert('Hello World!');
        return false;
    });


    /************** start: functions. **************/
    function loading() {
        $("div.loader").show();
    }
    function closeloading() {
        $("div.loader").fadeOut('normal');
    }

    var popupStatus = 0; // set value

    function showaddnew_form() {
        if(popupStatus == 0) { // if value is 0, show popup
            closeloading(); // fadeout loading
            $("#addnew-form").fadeIn(0500); // fadein popup div
            $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);
            popupStatus = 1; // and set value to 1
        }
    }

    function showedit_form() {
        if(popupStatus == 0) { // if value is 0, show popup
            closeloading(); // fadeout loading
            $("#edit-form").fadeIn(0500); // fadein popup div
            $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);
            popupStatus = 1; // and set value to 1
        }
    }

    function disablePopup() {
        if(popupStatus == 1) { // if value is 1, close popup
            $("#addnew-form").fadeOut("normal");
            $("#edit-form").fadeOut("norml");

            $("#info-popup").fadeOut("normal");
            $("#backgroundPopup").fadeOut("normal");
            popupStatus = 0;  // and set value to 0
        }
    }
    /************** end: functions. **************/
}); // jQuery End