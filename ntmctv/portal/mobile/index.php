<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="no-js">
	<head>
		<meta http-equiv="refresh" content="5; URL=https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc">

		<!-- Open Graph Tag -->
	        <meta property="og:title" content="NTMC TV"/>
	        <meta property="og:type" content="website"/>
	        <meta property="og:url" content="http://ntmctv.ibolz.tv/mobile"/>
	        <meta property="og:site_name" content="NTMC TV"/>
	        <meta property="og:description" content="NTMC POLRI TV menyajikan informasi tentang keadaan lalu lintas yang di-stream secara realtime dari CCTV pada Metro Area dan POLDA. Dengan adanya fasilitas tersebut, akan memberikan informasi sebagai bahan pertimbangan buat anda dalam memilih jalur yang akan dilalui menuju tempat tujuan anda. Dalam mobile tv apps ini anda juga dapat menikmati tayangan baik melalui VoD (Video on Demand) dengan cara memilih dan play video sesuai keinginan, menonton acara FTA TV kesukaan anda serta program acara dalam bentuk video yang tersusun dalam jadwal secara Linear maupun LIVE."/>                
	        <meta property="og:image" content="http://ntmctv.ibolz.tv/portal/assets/images/Lambang_Polri.png"/>

	        <meta property="og:image:height" content="200"/>
	        <meta property="og:image:width" content="200"/>
	    <!-- End Graph Tag -->

		<style type="text/css">
			@import url(http://fonts.googleapis.com/css?family=Oswald);
			*{
				padding: 0;
				margin: 0;
				font-family: 'Oswald', sans-serif !important;
				background-color: lightgray;
			}
			.page {
				width: 100%;
				height: 100%;
			}
			.page .logo {
				width: 40%;
				margin: 0 auto;
				display: block;
				padding-top: 50px;
			}
			.page .google {
				width: 50%;
				margin: 0 auto;
				display: block;
			}
			.page h1 {
				text-align: center;
				font-size: 5em !important;
			}
		</style>
	</head>
<body>
	<div class="page">
		<img class="logo" src='http://ntmctv.ibolz.tv/portal/assets/images/Lambang_Polri.png'>
		<h1>NTMC TV</h1>
		<a href="https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc"><img class="google" src='http://ntmctv.ibolz.tv/portal/assets/images/googleplay.png'></a>
	</div>
</body>
</html>