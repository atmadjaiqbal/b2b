<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['ibolz_app_id'] = 'com.balepoint.ibolz.ntmc.prod';
$config['ibolz_customer_id'] = '-2';
$config['ibolz_device_id'] = '4';
$config['ibolz_lang'] = 'en_US';
$config['ibolz_version'] = '1.1.23';
$config['ibolz_mobile'] = '';

//$config['ibolz_api'] = 'http://apib2b.ibolztv.net/api106/v2/';
//$config['ibolz_api'] = 'http://ntmctv.ibolz.tv/api106/v2/';
//$config['ibolz_api'] = 'http://localhost/ntmctv/api106/v2/';
$config['ibolz_api'] = 'http://172.16.2.102/ntmctv/api106/v2/';

/* --- API config item ----- */
$config['api_menu'] = $config['ibolz_api'] . 'menu/menu_list';
$config['api_banner'] = $config['ibolz_api'] .'product/boxoffice_list';
$config['api_categories'] = $config['ibolz_api'] . 'menu/menu_list';
$config['api_product_bymenuid'] = $config['ibolz_api'] . 'product/product_list';
$config['api_product_detail'] = $config['ibolz_api'] . 'product/product_detail';
$config['api_product_popular'] = $config['ibolz_api'] . 'product/product_popular';
$config['api_channel_list'] = $config['ibolz_api'] . 'product/channel_list';
$config['api_channel2_list'] = $config['ibolz_api'] . 'channel/channel_list';
$config['api_channel_detail'] = $config['ibolz_api'] . 'channel/channel_detail';
$config['api_content_list'] = $config['ibolz_api'] . 'channel/content_list';
$config['api_schedule_today'] = $config['ibolz_api'] . 'schedule/today';
$config['api_login'] = $config['ibolz_api'] . 'customer/login';
$config['api_forgot_password'] = $config['ibolz_api'] . 'customer/forget_password';
$config['api_customer_profile'] = $config['ibolz_api'] . 'customer/profile';
$config['api_update_profile'] = $config['ibolz_api'] . 'customer/update_profile';
$config['api_change_password'] = $config['ibolz_api'] . 'customer/change_password';
$config['api_customer_history'] = $config['ibolz_api'] . 'customer/customer_history';
$config['api_like_save'] = $config['ibolz_api'] . 'product/like_save';
$config['api_total_like'] = $config['ibolz_api'] . 'product/total_like';
$config['api_comment_list'] = $config['ibolz_api'] . 'product/comment_list';
$config['api_comment_save'] = $config['ibolz_api'] . 'product/comment_save';
$config['api_faq'] = $config['ibolz_api'] . 'menu/apps_faq';
$config['api_faq_detail'] = $config['ibolz_api'] . 'menu/faq_detail';
$config['api_donwload_document'] = $config['ibolz_api'] . 'product/document_download';

/* ---- Apps Dowwnload ----- */
$config['api_donwload_apps'] = $config['ibolz_api'] . 'menu/apps_upload_list';
$config['api_donwload_count'] = $config['ibolz_api'] . 'menu/update_apps_download';
$config['api_donwload_detail'] = $config['ibolz_api'] . 'menu/apps_upload_detail';
