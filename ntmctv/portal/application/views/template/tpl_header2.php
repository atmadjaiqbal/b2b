<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    $_metadesc = 'Ibolz NTMC.';
    ?>
    <meta name="description" content="<?php echo $_metadesc; ?>"/>
    <meta name="keywords" content="NTMC, streaming, television, mobile, vod, video on demand, ibolz"/>
    <meta name="author" content="Ibolz Team" />
    <meta name="webcrawlers" content="all" />
    <meta name="rating" content="general" />
    <meta name="spiders" content="all" />
    <meta name="robots" content="index,follow">

    <meta name="google-site-verification" content="NDRXgAvCOkBcqq7-oKWIwO6XplA-4Y2xtR98DLMSGoU" />
    <meta charset="utf-8" />

    <!-- Open Graph Tag -->
    <?php
        $width = !empty($og_image) ? 600 : 200;
        $height = !empty($og_image) ? 315 : 200;
        ?>
        <meta property="og:title" content="<?php echo (isset($og_title) && !empty($og_title) ? $og_title : 'NTMC TV');?>"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="<?php echo (isset($og_url) && !empty($og_url) ? $og_url : 'http://ntmctv.ibolz.tv/portal');?>"/>
        <meta property="og:site_name" content="NTMC TV"/>
        <meta property="og:description" content="<?php echo (isset($og_desc) ? substr($og_desc, 0, 150) : 'NTMC POLRI TV menyajikan informasi tentang keadaan lalu lintas yang di-stream secara realtime dari CCTV pada Metro Area dan POLDA. Dengan adanya fasilitas tersebut, akan memberikan informasi sebagai bahan pertimbangan buat anda dalam memilih jalur yang akan dilalui menuju tempat tujuan anda. Dalam mobile tv apps ini anda juga dapat menikmati tayangan baik melalui VoD (Video on Demand) dengan cara memilih dan play video sesuai keinginan, menonton acara FTA TV kesukaan anda serta program acara dalam bentuk video yang tersusun dalam jadwal secara Linear maupun LIVE.');?>"/>                
        <meta property="og:image" content="<?php echo (isset($og_image) && !empty($og_image) ? $og_image : 'http://ntmctv.ibolz.tv/portal/assets/images/Lambang_Polri.png');?>"/>

        <meta property="og:image:height" content="<?php echo $height; ?>"/>
        <meta property="og:image:width" content="<?php echo $width; ?>"/> 
        <?php
    ?>
    <!-- End Graph Tag -->

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" CONTENT="private, max-age=5400, pre-check=5400"/>
    <meta http-equiv="Expires" CONTENT="<?php echo date(DATE_RFC822,strtotime("1 day")); ?>"/>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/ibolz.polri.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/magnific-popup.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/lazyload/jquery.lazyload.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/lazyload/jquery.scrollstop.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.imagesloaded.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js" ></script>
    <!-- alertify
    <script src="<?php echo base_url(); ?>assets/js/alertify/alertify.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/alertify/themes/alertify.core.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/alertify/themes/alertify.default.css" id="toggleCSS" />
    -->
    <!-- Jquery Validation -->
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validation/jquery.validationEngine.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validation/jquery.validationEngine-en.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jquery.validation/validationEngine.jquery.css" type="text/css" />
    <!-- script type="text/javascript" src="<?php echo base_url(); ?>assets/flash/iBolz/swfobject.js"></script -->

    <?php if (!empty($scripts)) : ?>
        <?php foreach ($scripts as $script) : ?>
            <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>


    <script type="text/javascript">
        var Settings = <?php
        if(str_replace("http://","",current_url()) == $_SERVER['SERVER_ADDR'])
        {
           $settings = array('base_url' => $_SERVER['SERVER_ADDR']);
        } else {
         $settings = array('base_url' => base_url());
        }
      echo json_encode($settings);
      ?>;
    </script>
    <script type="text/javascript">if(jQuery(window).width()>1024){document.write("<"+"script src='<?php echo base_url(); ?>assets/js/jquery.preloader.js'></"+"script>");} </script>
    <script type="text/javascript" language="javascript">
        /*     jQuery(window).load(function() {
         $x = $(window).width();
         if($x > 1024)
         {
         jQuery("#content .row").preloader();    }

         jQuery('.magnifier').touchTouch();
         jQuery('.spinner').animate({'opacity':0},1000,'easeOutCubic',function (){jQuery(this).css('display','none')});
         }); */

    </script>


    <!--[if lt IE 8]>
<!--    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>-->
    <!--[endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <!--<![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/docs.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ie.css" type="text/css" media="screen">
    <![endif]-->

    <!-- Tracking Code Google -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-63879208-5', 'auto');
      ga('require', 'displayfeatures');
      ga('send', 'social', 'socialNetwork', 'socialAction', 'socialTarget', {'page': 'optPagePath'});
      ga('send', 'pageview');		
		</script>
</head>

<body>
<div class="header-wrapper" style="margin-bottom: 20px;">
  <header>
    <div class="nav-section">
      <div class="container">
        <div class="logo-top">
          <?php
          if($this->session->userdata('display_name'))
          {
             $homelink = base_url().'home/index';
          } else {
             $homelink = base_url();
          }
          ?>
          <a href="<?php echo $homelink;?>"><p>NTMC TV</p></a>
        </div>
        <ul class="nav nav-tabs" style="float: right;">
            <li class="dropdown">
                <a href="<?php echo base_url(); ?>" class="dropdown-toggle">HOME</a>
            </li>
          <?php // if($this->session->userdata('display_name')): ?>
            <?php foreach($headermenu->menu_list as $val) { 
                // echo "<pre>";print_r($val);echo "</pre>";
                ?>
              <li class="dropdown">
                <?php
                if ($val->title == "Map") { ?>
                    <a data-link="<?php echo $val->link; ?>" id="fullpeta" class="menumap"><?php echo strtoupper($val->title);?></a>
                    <?php
                }
                else { ?>
                <a id="left-menu<?php echo $val->menu_id;?>" data-menuid="<?php echo $val->menu_id;?>" data-mtitle="<?php echo urlencode($val->title);?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo strtoupper($val->title);?> <b class="caret"></b></a>
                <?php } ?>
                    <script type="text/javascript">
                        $('#left-menu<?php echo $val->menu_id;?>').click(function() {
                            // e.preventDefault();
                            var menuid = $(this).data('menuid');
                            var mtitle = $(this).data('mtitle');
                            get_content_menu(menuid, mtitle);
                            replace_page(menuid, mtitle);
                        });
                        $('#fullpeta').click(function() {
                            // e.preventDefault();
                            var link = $(this).data('link');
                            view_peta(link);
                        });
                    </script>
                <?php if(count($val->sub_menu_list) > 0) { ?>
                    <ul class="dropdown-menu">
                        <?php foreach($val->sub_menu_list as $submenu) { ?>
                            <?php
                            $head_product_id = $submenu->product_id;
                            $head_menu_id = $submenu->menu_id;
                            $head_menu_type = $submenu->menu_type;
                            $head_title = $submenu->title;
                            $_link = '';

                            switch($submenu->menu_type)
                            {
                                case 'menu' :
                            ?>
                                <li>
                                  <a id="toggle-menu<?php echo $submenu->menu_id;?>" data-menuid="<?php echo $submenu->menu_id;?>" data-mtitle="<?php echo urlencode($submenu->title);?>">
                                    <?php echo strtoupper($submenu->title);?>
                                  </a>
                                </li>

                                <script type="text/javascript">
                                    $('#toggle-menu<?php echo $submenu->menu_id;?>').click(function() {
                                        // e.preventDefault();
                                        var menuid = $(this).data('menuid');
                                        var mtitle = $(this).data('mtitle');
                                        get_content_submenu(menuid, mtitle);
                                        // get_content_submenu2(menuid, mtitle);
                                        return false;
                                    });

                                </script>

                                <?php
                                // $_link = base_url().'content/content_categories_list/'.$head_menu_id.'/'.urlencode($val->title).'/'.urlencode($submenu->title);
                                break;
                                case 'channel' :
                                    $_link = base_url().'content/content_channel/'.$head_menu_id.'/'.$head_product_id;
                                ?>
                                <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($submenu->title);?></a></li>

                            <?php
                                    break;
                                default:
                            ?>
                                <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($submenu->title);?></a></li>
                            <?php
                                    $_link = base_url().'content/content_categories/'.$head_menu_id.'/'.urlencode($submenu->title);
                                    break;
                            }
                            ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
              </li>
             <?php } ?>
        </ul>
      </div>
    </div>

  </header>
</div>
