<div class="container">
    <div class="row-fluid">
        <div id="faq-bni"></div><div class="clear"></div>
        <div class="faq-title-place">
            <h3>ABOUT US</h3>
        </div>
        <div class="faq-details">
            <p style="margin-top:20px;"><strong>Kakorlantas: Polantas Harus Bantu Masyarakat Tanpa Pamrih</strong></p>
            <p>Kewajiban sebagai anggota polisi adalah membantu dan melayani masyarakat tanpa adanya imbalan apapun, jangan pernah berharap ada balasan dari orang yang kita bantu, biar Tuhan yang membalas karena Dia tidak tidur.</p>
            <p>Kakorlantas juga memberikan dua pedoman penting dalam menjalankan tugas dilapangan. Pertama, anggota dilapangan haruslah berbuat baik tanpa memilih siapa yang kita bantu dan layani. Kedua, meningkatkan dua pokok yang sudah berjalan selama ini, yakni utamakan keselamatan berlalu lintas bagi para anggota polantas, dan tingkatkan pelayanan publik dengan sebaik-baiknya.</p>
            <p>Selain menjadi Inspektur Upacara, Kakorlantas juga memberikan penghargaan kepada anggota Polantas Polda Metro Jaya yang berprestasi dalam bertugas. Selain itu, Kakorlantas juga memberi apresiasi kepada jajaran Polantas Polda Metro Jaya yang telah berkembang dengan baik tingkat kedisiplinannya dari tahun ke tahun.</p>
            <p>Ini merupakan salah satu cara yang bagus dalam memotivasi anggota dilapangan dalam melaksanakan tugas setiap harinya, sehingga mereka merasa dihargai</p>
        </div>
    </div>
</div>