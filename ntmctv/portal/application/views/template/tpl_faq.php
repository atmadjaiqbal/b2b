<div class="container">
    <div class="row-fluid">
        <div id="faq-bni">
        <?php
        if($faqresult)
        {
            foreach($faqresult as $key => $row)
            {
                $faq_title = $row['faq_title'];
                $faq_description = $row['faq_description'];
                $faq_detail = $row['faq_detail']->faq_detail;
        ?>
                <div id="faq-bni">
                    <div class="faq-title-place">
                        <h3><?php echo $faq_title;?></h3>
                    </div>
                    <?php
                    if($faq_detail)
                    {
                      foreach($faq_detail as $key => $rwDetail)
                      {
                    ?>
                    <!-- i class="icon-chevron-right mycolaps" data-toggle="collapse" data-target="#ans-< ?php echo $rwDetail->faq_details_id; ?>" -->
                    <div class="faq-details">
                        <p><?php echo $rwDetail->question;?></p>
                        <p id="ans-<?php echo $rwDetail->faq_details_id; ?>"><?php echo $rwDetail->answer;?></p>
                    </div>
                    <?php
                      }
                    }
                    ?>

                </div>





            <?php
            }
        }
        ?>
        </div>
    </div>
</div>


<script>

    // $('.mycolaps').collapse("hide");

    $('.mycolaps').on('click', function(e){
        e.preventDefault();
        var dt_target = $(this).data('target');
        var cl = $(dt_target).attr('class');
        if(cl == 'collapse'){
            $(this).attr('class', 'icon-chevron-down mycolaps collapsed');
        }else{
            $(this).attr('class', 'icon-chevron-right mycolaps collapsed');
        }
    });
</script>