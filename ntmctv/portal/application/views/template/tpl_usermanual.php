<div class="container">
    <div class="row-fluid">
        <div id="faq-bni">
            <div class="faq-title-place">
                <h3>HELP</h3>
            </div>
            <div class="faq-details">
                <div style="text-align: center;">
                    <img src="<?php echo base_url().'assets/images/ibolz-indonesia.png';?>" style="width: auto;height: 100px;">
                </div>

                <h2 style="font-size: 20px;font-family: helvetica, arial, sans-serif;">USER MANUAL</h2>
                <p style="font-size: 16px;font-family: helvetica, arial, sans-serif;margin-top:-18px;">ONL-TV</p>
                <hr style="color: #cecece;"/>
                <div style="margin-top:20px;" id="help">
                    <ul class="toc">
                        <li><a href="#pendahuluan">PENDAHULUAN.</a></li>
                        <li><a href="#mengenal">MENGENAL APLIKASI ONL-TV</a></li>
                        <li><a href="#memulai">MEMULAI APLIKASI ONL-TV</a></li>
                        <li><a href="#halutama">HALAMAN UTAMA</a></li>
                        <li><a href="#kategori">KATEGORI CHANNEL</a>
                            <ol class="toc">
                                <li style="margin-left:15px;"><a href="#onltv">ONL TV</a></li>
                                <li style="margin-left:15px;"><a href="#learning">Learning</a></li>
                                <li style="margin-left:15px;"><a href="#learning">Favorite</a></li>
                            </ol>
                        </li>
                        <li><a href="#chat">CHAT</a>
                            <ol class="toc">
                                <li style="margin-left:15px;"><a href="#public">Public</a></li>
                                <li style="margin-left:15px;"><a href="#friend">Friends</a></li>
                                <li style="margin-left:15px;"><a href="#group">Group</a></li>
                                <li style="margin-left:15px;"><a href="#invite">Invite</a></li>
                            </ol>
                        </li>
                        <li><a href="#profile">PROFILE</a>
                            <ol class="toc">
                                <li style="margin-left:15px;"><a href="#profile1">My Activity</a></li>
                                <li style="margin-left:15px;"><a href="#profile1">Setting</a></li>
                                <li style="margin-left:15px;"><a href="#profile1">About</a></li>
                            </ol>
                        </li>
                        <li><a href="#search">SEARCH VOD HALAMAN UTAMA</a>
                        <li><a href="#info">CONTENT INFO</a>
                    </ul>
                </div>
                <hr style="color: #cecece;"/>

                <div id="pendahuluan">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">PENDAHULUAN</h2>
                    <p>Panduan Pengunaan ONL TV adalah tata cara pemakaian aplikasi ONL TV untuk user yang berfungsi untuk mempermudah user dalam pengunaan aplikasi ONL TV dari awal user masuk kedalam aplikasi sampai keluar dari aplikasi ONL TV.</p>
                </div>

                <div id="mengenal">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">MENGENAL APLIKASI ONL-TV</h2>
                    <p>ONL TV merupakan aplikasi tv mobile yang berfungsi untuk mempermudah sharing ilmu dalam bidang perbankan</p>
                </div>

                <div id="memulai">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">MEMULAI APLIKASI ONL-TV</h2>
                    <p>Pada saat user pertama kali membuka aplikasi ONL TV akan muncul splash screen dengan logo BNI 46 dan IBOLZ Indonesia.</p>
                    <img src="<?php echo base_url().'assets/images/userman-splash.png';?>" style="width: auto;height: 130px;">
                </div>

                <div id="halutama">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">HALAMAN UTAMA</h2>
                    <p>Kemudian setelah masuk pada aplikasi BNI ONL akan muncul halaman utama pada layar smart phone seperti pada gambar berikut.</p>
                    <img src="<?php echo base_url().'assets/images/userman-halutama.jpg';?>" style="width: auto;height: 130px;">
                    <p>Pada halaman utama ini terdapat 4 fungsi utama yang tampil, yaitu menu: Channel, Chat, Profile, Search VOD dan Content Info.</p>
                </div>

                <div id="kategori">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">KATEGORI CHANNEL</h2>
                    <p>Kategori terdiri dari 3 (tiga) fungsi yaitu Favorite, ONL TV dan Learning.</p>
                    <ol>
                        <li><b>ONL-TV</b></li>
                        <img src="<?php echo base_url().'assets/images/userman-onltv.png';?>" style="width: auto;height: 130px;">
                        <p>ONL TV terdiri ke dalam dua sub sub menu yaitu  ONL TV update dan ONL TV channel.</p>
                        <ol style="list-style-type:lower-latin;">
                            <li><b>ONL TV update</b></li>
                            <p>ONL TV update merupakan on channel yang berisikan kumpulan content yang terbaru (list).Untuk menampilkan conten pada channel ini user harus menekan tombol content list <img src="<?php echo base_url().'assets/images/userman-icon-player-list.png';?>" style="width: 18px;height: 18px;">.</p>
                            <p><img src="<?php echo base_url().'assets/images/userman-icon-player-list.png';?>" style="width: 18px;height: 18px;">&nbsp;&nbsp;<b>Content List</b></p>
                            <img src="<?php echo base_url().'assets/images/userman-contentlist.png';?>" style="height: 130px;width: auto;">
                            <p>Menu ini berisikan kumpulan content (VOD), pada content list terdapat beberapa tombol,  yaitu:
                            <ul style="list-style: circle;">
                                <li><b>Tombol Like</b></li>
                                <p style="line-height: 10px;">Tombol like berfungsi untuk memberikan like pada VOD yang di sukai user dengan menekan tombol tersebut. Setiap user hanya bias sekali menekan tombol like</p>
                                <li><b>Comment</b></li>
                                <p style="line-height: 10px;">Tombol comment berfungsi untuk memberikan comment pada vod.</p>
                                <li><b>Back</b></li>
                                <p style="line-height: 10px;">Tombol back berfungsi untuk kembali ke halaman sebelumnya.</p>
                                <li><b>Play</b></li>
                                <p style="line-height: 10px;">Tombol play berfungsi untuk memulai menonton vod.</p>
                                <li><b>PIP (Picture In Picture)</b></li>
                                <p style="line-height: 10px;">Tombol PIP berfungsi untuk memulai menonton dalam betuk kotak kecil pada sebelah kanan.</p>
                                <li><b>Menu searching per kategori</b></li>
                                <p style="line-height: 10px;">Menu ini berfungsi untuk mencari semua content VOD perkategori dalam aplikasi ini berdasarkan kata kunci yang di inputkan user.</p>
                                <li><b>Exit</b></li>
                                <p>Menu ini berfungsi untuk mengeluarkan menu VOD list.</p>
                            </ul></p>
                            <li><b>ONL TV channel</b></li>
                            <p>ONL TV channel merupakan channel yang menampilkan semua conten yang ada pada ONL TV, yang diatur dalam Jadwal perhari. Channel memiliki empat menu, yaitu :
                            <ul style="list-style: circle;">
                                <li><img src="<?php echo base_url().'assets/images/userman-icon-player-play.png';?>" style="width: 18px;height: 18px;">&nbsp;<b>Play</b></li>
                                <p>Tombol ini berfungsi untuk memulai  menonton channel.Pada saat user menonton channel maka akan muncul menu Stop yang berfungsi untuk memberhentikan dan video resolusi untuk mengatur format resolusi channel kedalam empat format, yaitu format low, medium, sd dan hd yang bisa di setting manual atau otomatis dengan mengubah resolusi sesuai dengan bandwidth dengan menekan tombol auto on (berwarna merah)maka secara otomatis format berpindah dan bila tombol muncul off maka format harus dipindahkan secara manual.</p>
                                <li><img src="<?php echo base_url().'assets/images/userman-icon-player-pip.png';?>" style="width: 18px;height: 18px;">&nbsp;<b>PIP (Picture In Picture)</b></li>
                                <p>Tombol ini berfungsi untuk menonton channel dalam</p>
                                <li><img src="<?php echo base_url().'assets/images/userman-icon-player-favorite.png';?>" style="width: 18px;height: 18px;">&nbsp;<b>Favorite</b></li>
                                <p>Tombol ini berfungsi untuk menambahkan channel sebagai channel favorite.</p>
                                <li><img src="<?php echo base_url().'assets/images/userman-icon-player-jadwal.png';?>" style="width: 18px;height: 18px;">&nbsp;<b>Jadwal</b></li>
                                <p>Tombol ini berfungsi untuk menampilkan Jadwal program yang ditampilkan pada channel setiap harinya, tombol reg <img src="<?php echo base_url().'assets/images/userman-icon-player-jadwal-reg.png';?>" style="width: 18px;height: 18px;"> berfungsi untuk menampilkan Jadwal hari ini, pada menu ini terdapat menu record untuk program yang belum tampil yang ada pada jadwal hari ini. Tombol VOD (Video On Demand) <img src="<?php echo base_url().'assets/images/userman-icon-player-jadwal-vod.png';?>" style="width: 18px;height: 18px;"> berfungsi untuk menampilkan Jadwal tujuh hari kebelakang dan tombol my <img src="<?php echo base_url().'assets/images/userman-icon-player-jadwal-my.png';?>" style="width: 18px;height: 18px;"> berfungsi untuk menampilkan hasil recording dari menu reg <img src="<?php echo base_url().'assets/images/userman-icon-player-jadwal-rec.png';?>" style="width: 18px;height: 18px;">.</p>
                            </ul></p>
                        </ol>
                        <li><b>LEARNING</b></li>
                        <img src="<?php echo base_url().'assets/images/userman-learning.png';?>" style="width: auto;height: 130px;">
                        <p>Menu learning terdiri dari lima channel, yaitu :</p>
                        <ol style="list-style-type:lower-latin;">
                            <li><b>Business Banking</b></li>
                            <p>Business banking merupakan on channel yang berisikan kumpulan content (list).Untuk menampilkan conten pada channel ini user harus menekan tombol <img src="<?php echo base_url().'assets/images/userman-icon-player-list.png';?>" style="width: 18px;height: 18px;">&nbsp;content list.</p>
                            <li><b>Consumer & Retail Banking</b></li>
                            <p>Consumer & retail banking merupakan on channel yang berisikan kumpulan content (list).Untuk menampilkan conten pada channel ini user harus menekan tombol <img src="<?php echo base_url().'assets/images/userman-icon-player-list.png';?>" style="width: 18px;height: 18px;">&nbsp;content list.</p>
                            <li><b>Corporate Core Function</b></li>
                            <p>Corporate core function merupakan on channel yang berisikan kumpulan content (list).Untuk menampilkan conten pada channel ini user harus menekan tombol <img src="<?php echo base_url().'assets/images/userman-icon-player-list.png';?>" style="width: 18px;height: 18px;">&nbsp;content list.</p>
                            <li><b>Treasury & Global Banking</b></li>
                            <p>Treasury & global banking merupakan on channel yang berisikan kumpulan content (list).Untuk menampilkan conten pada channel ini user harus menekan tombol <img src="<?php echo base_url().'assets/images/userman-icon-player-list.png';?>" style="width: 18px;height: 18px;">&nbsp;content list.</p>
                            <li><b>Transactional Banking</b></li>
                            <p>Transactional banking merupakan on channel yang berisikan kumpulan content (list).Untuk menampilkan conten pada channel ini user harus menekan tombol <img src="<?php echo base_url().'assets/images/userman-icon-player-list.png';?>" style="width: 18px;height: 18px;">&nbsp;content list.</p>
                        </ol>

                        <li><b>FAVORITE</b></li>
                        <img src="<?php echo base_url().'assets/images/userman-favorite.png';?>" style="width: auto;height: 130px;">
                        <p>Menu ini berisikan channel favorite yang di pilih user dengan menekan tombol add favorite pada menu ONL TV channel. Terdapat tombol dalam aplikasi ini, yaitu :</p>
                        <ol style="list-style-type:lower-latin;">
                            <li><img src="<?php echo base_url().'assets/images/userman-icon-player-play.png';?>" style="width: 18px;height: 18px;">&nbsp;<b>Play</b></li>
                            <p>Tombol ini berfungsi untuk menonton channel / VOD maka akan muncul menu setting untuk mengatur resolusi tampilan dari channel / VOD yang di tonton</p>
                            <li><img src="<?php echo base_url().'assets/images/userman-icon-player-pip.png';?>" style="width: 18px;height: 18px;">&nbsp;<b>PIP (Picture In Picture)</b></li>
                            <br/><img src="<?php echo base_url().'assets/images/userman-pip.png';?>" style="width: auto;height: 130px;">
                            <p>Tombol ini berfungsi untuk menonton channel dalam ukuran kecil pada sebelah kanan layar device, VOD yang di play dalam menu pip tidak mengeluarkan suara. Dalam menu pip ada tiga buah menu yaitu :
                            <ul style="list-style: circle;">
                                <li><b>Switch</b></li>
                                <p>Menu switch berfungsi untuk menukar VOD yang berada pada menu PIP ke pada VOD yang sedang di play.</p>
                                <li><b>Fullscreen</b></li>
                                <p>Menu fullscreen berfungsi untuk membuat VOD yang diplay dalam menu pip menjadi fullscreen.</p>
                                <li><b>Close</b></li>
                                <p>Menu close berfungsi untuk menutup VOD yang di play dalam menu PIP.</p>
                            </ul></p>
                            <li><img src="<?php echo base_url().'assets/images/userman-icon-unfavorite.png';?>" style="width: 18px;height: 18px;">&nbsp;<b>Unfavorite</b></li>
                            <p>Tombol ini berfungsi untuk mengeluarkan channel dari channel favorite.</p>
                            <li><img src="<?php echo base_url().'assets/images/userman-icon-player-jadwal.png';?>" style="width: 18px;height: 18px;">&nbsp;<b>Jadwal</b></li>
                            <p>Tombol ini berfungsi untuk menampilkan Jadwal program yang ditampilkan pada channel setiap harinya, tombol reg   berfungsi untuk menampilkan Jadwal hari ini, pada menu ini terdapat menu record untuk program yang belum tampil yang ada pada Jadwal hari ini. Tombol VOD berfungsi untuk menampilkan Jadwal tujuh hari kebelakang dan tombol my  berfungsi untuk menampilkan hasil recording dari menu reg .</p>
                        </ol>
                    </ol>
                </div>

                <div id="chat">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">CHAT</h2>
                    <img src="<?php echo base_url().'assets/images/userman-chat.png';?>" style="width: auto;height: 130px;">
                    <p>Kategori terdiri dari 3 (tiga) fungsi yaitu Favorite, ONL TV dan Learning.</p>
                    <ol>
                        <li><b>Public</b></li>
                        <p>Public chat merupakan menu chatting untuk semua user pada aplikasi ONL TV yang menonton channel yang sama.</p>
                        <li><b>Friends</b></li>
                        <p>Public chat merupakan menu chatting untuk user yang sudah menjadi teman pada aplikasi ONL TV. Pada aplikasi ONL TV terdapat menu chating berfungsi untuk mengobrol dengan friends dalam aplikasi ONL TV dan Delet untuk menghapus pertemanan.</p>
                        <li><b>Group</b></li>
                        <img src="<?php echo base_url().'assets/images/userman-chatgroup.png';?>" style="width: auto;height: 130px;">
                        <p>Grup chat merupakan menu chatting berkelompok, dalam group chat terdapat menu create untuk membuat group, terdapat menu group title  untuk member judul group, description untuk memberikan keterangan tentang group, tombol create untuk membuat group dan cancel untuk menggagalkan pembuatan group. Setelah user memiliki grup, pada grup terdapat dua menu, yaitu menu chat untuk chatting dengan group dan menu home untuk meng update group dan menambah member dengan add member.</p>
                        <li><b>Invite</b></li>
                        <p>Invite chat merupakan menu untuk mencari user lain untuk di add menjadi friends dengan menekan tombol add friends dan apabila ada user lain yang menginvite akan muncul pada sub menu invitation </p>
                    </ol>
                </div>

                <div id="profile">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">PROFILE</h2>
                    <img src="<?php echo base_url().'assets/images/userman-profile.png';?>" style="width: auto;height: 130px;">
                    <p>Menu profile berisi profile user yang telah log in pada aplikasi ONL TV. Dalam menu ini terdapat nama user, tombol log out untuk keluar dari account, menu update profile berfungsi untuk menganti profil berupa nama user, nomer telephone user dan menu change password berfungsi untuk mengganti password dengan memasukkan password lama dengan yang baru dan di masukkan ke dalam form password dan dimasukkan kembali dalam form verify password.Password yang dimasukkan harus sama dengan yang di masukkan ke dalam verify.</p>
                    <ol>
                        <li><b>My activity</b></li>
                        <img src="<?php echo base_url().'assets/images/userman-myactivity.png';?>" style="width: auto;height: 130px;">
                        <p>Menu ini berisi riwayat VOD yang telah di tonton oleh user sehingga user dapat menonton kembali VOD yang telah di tonton. Pada menu ini terdapat tombol <img src="<?php echo base_url().'assets/images/userman-icon-player-play.png';?>" style="width: 18px;height: 18px;"> play dan <img src="<?php echo base_url().'assets/images/userman-icon-player-pip.png';?>" style="width: 18px;height: 18px;"> pip untuk menonton VOD pada my activity.</p>
                        <li><b>Setting</b></li>
                        <img src="<?php echo base_url().'assets/images/userman-settings.png';?>" style="width: auto;height: 130px;">
                        <p>Menu ini berfungsi untuk mensetting tampilan dan soud pada aplikasi adapun sub menu yang terdapat pada menu setting, yaitu :
                        <ul style="list-style: circle;">
                            <li><b>Font Size</b></li>
                            <p>Menu ini berfungsi untuk mengatur ukuran font pada aplikasi.</p>
                            <li><b>Icon Size</b></li>
                            <p>Menu ini berfungsi untuk mengatur ukuran icon pada aplikasi.</p>
                            <li><b>Click Sound</b></li>
                            <p>Menu ini berfungsi untuk mengatur agar suara tombol aktif atau mati.</p>
                            <li><b>Language</b></li>
                            <p>Menu ini berfungsi untuk mengatur pergantian bahasa dalam aplikasi, dalam aplikasi ini terdapat tiga bahas yaitu bahasa inggris, china dan Indonesia.</p>
                            <li><b>Video Format</b></li>
                            <p>Menu ini berfungsi untuk perubahan format video dengan resolusi layar ada format high dan low.</p>
                        </ul></p>
                        <li><b>About</b></li>
                        <p>Menu ini berisi tentang versi dari aplikasi ONL TV dan Id dari smartphone user.</p>
                    </ol>
                </div>

                <div id="search">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">SEARCH VOD HALAMAN UTAMA</h2>
                    <p>Menu ini berfungsi untuk mencari semua content VOD dalam aplikasi ini berdasarkan kata kunci yang di inputkan user.</p>
                </div>

                <div id="info">
                    <h2 style="font-size: 18px;font-family: helvetica, arial, sans-serif;">CONTENT INFO</h2>
                    <p>Menu ini berisi keterangan dari content yang sedang di tonton, apabila content yang ditonton berupa channel maka yang muncul adalah informasi Jadwal dari channel tersebut sedangkan bila yang di tonton user adalah VOD maka yang muncul adalah halam deskripsi dari VOD tersebut disertai, tombol like dan tombol comment.</p>
                </div>


            </div>
        </div>
    </div>
</div>