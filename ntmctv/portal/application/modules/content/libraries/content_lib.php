<?php
class Content_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    function get_listcontent()
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '4',
        );
        $api_url = config_item('api_categories');
        $response = $this->CI->rest->post($api_url, $post_params);
        $resmerge = array_merge($response->menu_list[0]->sub_menu_list, $response->menu_list[1]->sub_menu_list);
        return $resmerge;
    }

    function product_menu_list($menu_id, $page=0, $limit=6) {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'menu_id' => $menu_id,
            'page' => $page,
            'limit' => $limit
        );
        $api_url = config_item('api_product_bymenuid');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function product_detail($product_id, $menu_id)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'product_id' => $product_id,
            'menu_id' => $menu_id
        );

        $api_url = config_item('api_product_detail');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function product_popular($sort, $limit=0, $offset=0)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '4',
            'page' => $offset,
            'limit' => $limit,
            'sort_by' => $sort
        );

        $api_url = config_item('api_product_popular');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function channel_list()
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'channel_category_id' => ''
        );
        $api_url = config_item('api_channel_list');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function channel_list2($menu_id=null, $channel_id=null)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'menu_id' => $menu_id,
            'channel_id' => $channel_id
        );
        $api_url = config_item('api_channel2_list');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function content_list2($menu_id=null)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'menu_id' => $menu_id
        );
        $api_url = config_item('api_content_list');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;


    }

    function channel_detail($channel_id=null, $menu_id=null)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'menu_id' => $menu_id,
            'channel_id' => $channel_id
        );
        $api_url = config_item('api_channel_detail');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

// $listcontent[$i]['channel_detail'] = $this->content_lib->channel_detail($product_id);
    function schedule_today($channel_id)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'channel_id' => $channel_id,
            'schedule_type_id' => '1'
        );
        $api_url = config_item('api_schedule_today');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function post_like($poduct_id="", $cust_id="")
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'product_id' => $poduct_id,
            'customer_id' => $cust_id
        );
        $api_url = config_item('api_like_save');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function totallike($product_id)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'product_id' => $product_id
        );

        $api_url = config_item('api_total_like');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function get_comment($product_id, $limit=0, $offset=0)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'product_id' => $product_id
        );

        $api_url = config_item('api_comment_list');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function post_comment($content_id="", $customer_id="", $comment_text="")
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'device_id' => config_item('ibolz_device_id'),
            'product_id' => $content_id,
            'comment_text' => $comment_text,
            'customer_id' => $customer_id
        );

        $api_url = config_item('api_comment_save');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function download_document($document_id)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'document_id' => $document_id
        );
        $api_url = config_item('api_donwload_document');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function search($filter)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => config_item('ibolz_device_id'),
            'lang' => config_item('ibolz_lang'),
            'version' => config_item('ibolz_version'),
            'search_key' => $filter
        );
        $api_url = config_item('api_product_bymenuid');
        $response = $this->CI->rest->post($api_url,$post_params);
        return $response;
    }

    function ismobile()
    {
        $mobiles = array(
            // legacy array, old values commented out
            'mobileexplorer'	=> 'Mobile Explorer',
//					'openwave'			=> 'Open Wave',
            'opera mini'		=> 'Opera Mini',
            'operamini'			=> 'Opera Mini',
//					'elaine'			=> 'Palm',
            'palmsource'		=> 'Palm',
//					'digital paths'		=> 'Palm',
//					'avantgo'			=> 'Avantgo',
//					'xiino'				=> 'Xiino',
            'palmscape'			=> 'Palmscape',
//					'nokia'				=> 'Nokia',
//					'ericsson'			=> 'Ericsson',
//					'blackberry'		=> 'BlackBerry',
//					'motorola'			=> 'Motorola'

            // Phones and Manufacturers
            'motorola'			=> "Motorola",
            'nokia'				=> "Nokia",
            'palm'				=> "Palm",
            'iphone'			=> "Apple iPhone",
            // 'ipad'				=> "iPad",
            'ipod'				=> "Apple iPod Touch",
            'sony'				=> "Sony Ericsson",
            'ericsson'			=> "Sony Ericsson",
            'blackberry'		=> "BlackBerry",
            'cocoon'			=> "O2 Cocoon",
            'blazer'			=> "Treo",
            'lg'				=> "LG",
            'amoi'				=> "Amoi",
            'xda'				=> "XDA",
            'mda'				=> "MDA",
            'vario'				=> "Vario",
            'htc'				=> "HTC",
            'samsung'			=> "Samsung",
            'sharp'				=> "Sharp",
            'sie-'				=> "Siemens",
            'alcatel'			=> "Alcatel",
            'benq'				=> "BenQ",
            'ipaq'				=> "HP iPaq",
            'mot-'				=> "Motorola",
            'playstation portable'	=> "PlayStation Portable",
            'hiptop'			=> "Danger Hiptop",
            'nec-'				=> "NEC",
            'panasonic'			=> "Panasonic",
            'philips'			=> "Philips",
            'sagem'				=> "Sagem",
            'sanyo'				=> "Sanyo",
            'spv'				=> "SPV",
            'zte'				=> "ZTE",
            'sendo'				=> "Sendo",

            // Operating Systems
            'symbian'				=> "Symbian",
            'SymbianOS'				=> "SymbianOS",
            'elaine'				=> "Palm",
            'palm'					=> "Palm",
            'series60'				=> "Symbian S60",
            'windows ce'			=> "Windows CE",
            'android'	=> "Android",

            // Browsers
            'obigo'					=> "Obigo",
            'netfront'				=> "Netfront Browser",
            'openwave'				=> "Openwave Browser",
            'mobilexplorer'			=> "Mobile Explorer",
            'operamini'				=> "Opera Mini",
            'opera mini'			=> "Opera Mini",

            // Other
            'digital paths'			=> "Digital Paths",
            'avantgo'				=> "AvantGo",
            'xiino'					=> "Xiino",
            'novarra'				=> "Novarra Transcoder",
            'vodafone'				=> "Vodafone",
            'docomo'				=> "NTT DoCoMo",
            'o2'					=> "O2",

            // Fallback
            'mobile'				=> "Generic Mobile",
            'wireless'				=> "Generic Mobile",
            'j2me'					=> "Generic Mobile",
            'midp'					=> "Generic Mobile",
            'cldc'					=> "Generic Mobile",
            'up.link'				=> "Generic Mobile",
            'up.browser'			=> "Generic Mobile",
            'smartphone'			=> "Generic Mobile",
            'cellphone'				=> "Generic Mobile",

            'facebookexternalhit/1.0 (+http://www.facebook.com/externalhit_uatext.php)' => 'facebook',
            'facebookexternalhit/1.1 (+https://www.facebook.com/externalhit_uatext.php)' => "facebook",
            'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)' => "facebook"
        );

        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $path = $_SERVER['REQUEST_URI'];
        $is_mobile = FALSE;

        foreach($mobiles as $key=>$value) {
            if(strpos(strtolower($user_agent), $key)){
                $is_mobile = TRUE;
                break;
            }
        }
        return $is_mobile;
    }
    
}
