<div class="view-page">
<div class="container">
    <div class="row-fluid">
        <?php
        if($channel_detail)
        {
            $player_wrap_width = "620"; $player_width = "620"; $player_height = "400";
            $chan_channel_id = $channel_detail->channel_id;
            $chan_content_id = $channel_detail->channel_id;
            $chan_channel_name = $channel_detail->channel_name;
            $chan_alias = $channel_detail->alias;
            $chan_thumbnail = str_replace('http://media.ibolz.tv/thumbnail?', 'http://tmb001.3d.ibolztv.net/media/thumbnail.php?', $channel_detail->thumbnail);
            $chan_thumbnail = str_replace('w=170', 'w=300', $chan_thumbnail);
            $chan_description = $channel_detail->description;
            $chan_type_name = $channel_detail->type_name;
            $chan_status = $channel_detail->status;
            $chan_countviewer = $channel_detail->countviewer;
            $chan_order_number = $channel_detail->order_number;
            $chan_link = $channel_detail->link;
            $chan_created = $channel_detail->created;
            $chan_createdby = $channel_detail->createdby;
            $chan_apps_id = $channel_detail->apps_id;
            $chan_app_name = $channel_detail->app_name;

            $player = 'http://ply001.3d.ibolztv.net/player/flash/id/b2b.php?w='.$player_width.'&h='.$player_height;
            $player .= '&apps_id='.config_item('ibolz_app_id').'&version='.config_item('ibolz_version').'&type=channel';
            $player .= '&channel_id='.$chan_channel_id.'&vid='.$chan_content_id.'&id='.$chan_content_id.'&customer_id='.config_item('ibolz_customer_id');
            ?>

            <script type="text/javascript">
                $(document).ready(function(){
                    $('.gettoplay').click(function(e){
                        $.ajax({
                            url: '<?php echo base_url();?>content/check_apk/<?php echo $chan_content_id;?>/channel/<?php echo $chan_alias;?>',
                            success: function(data)
                            {
                                window.location = 'com.balepoint.ibolz.ntmc://path?<?php echo $chan_content_id;?>;channel;<?php echo $chan_alias;?>';
                            },
                            error: function() {
                                window.location = 'https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc.prod&hl=en2';
                            }
                        });
                    });
                })
            </script>

            <h3 class="slider-title">
                <a href="#" style="cursor: default;"><span><?php echo $chan_channel_name;?></span></a>
            </h3>
            <div class="span4" style="margin-left: 0px;">
                <!-- <div class="poster" style="background: url('<?php echo $chan_thumbnail;?>') no-repeat scroll top;width:100%;height:185px;"></div> -->
                <h4>DESCRIPTION</h4>
                <p class="more" style="text-align: justify;"><?php echo $chan_description; ?></p>
            </div>
            <div class="span8">
            <?php if($chan_status != '0') {  ?>
                <?php
            // if($isMobile) {
            ?>

               <!-- div style='width: 620px;height:400px;background-color:#000000;text-align: center;'>
                  <a href="com.balepoint.ibolz.ntmc://path?<?php echo $chan_content_id;?>;channel;<?php echo $chan_alias;?>" title="Link Apps">
                     <img src="<?php echo base_url();?>assets/images/asset__movie_play.png" style="width: 75px;height: auto;margin-top:25%;">
                     <div style="position: relative;">Klik untuk melihat content</div>
                   </a>
               </div -->

            <?php // } else { ?>

                <iframe seamless wmode="Opaque" class="player bg-loader" scrolling="no"
                        style='width: <?php echo $player_width;?>px;min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;'
                        src="<?php echo $player;?>">
                </iframe>
            <?php // }  ?>
            <?php } else { ?>
                <div>
                    <p style="text-align: center;font-family: helvetica, arial, sans-serif; font-size: 28px;margin-top: 150px;margin-bottom: 200px;">Channel Not Active</p>
                </div>
            <?php } ?>

                <div id="listsubmenu2" style="margin-top: 10px;"></div>
            </div>

            <?php
        } else {
            ?>
          <div>
            <p style="text-align: center;font-family: helvetica, arial, sans-serif; font-size: 28px;margin-top: 150px;margin-bottom: 200px;">Channel Not Available</p>
          </div>
        <?php
        }
        ?>
    </div>
</div>
</div>
