<div class="view-page">
    <div class="container">

        <div class="span8" id="content-categories">
            <h3 class="slider-title">
                <span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i>
            </h3>
            <?php
            if($list_content)
            {
                $crew_category_list = array(); $genre_list = array();
                foreach($list_content as $val)
                {
                    $product_id = $val->product_id;
                    $apps_id = $val->apps_id;
                    $menu_id = $val->menu_id;
                    $parent_menu_id = $val->parent_menu_id;
                    $menu_type = $val->menu_type;
                    $title = $val->title;
                    $short_title = strlen($title) > 30 ? substr($title, 0, 30). '...' : $title;
                    $thumbnail = str_replace('http://media.ibolz.tv/thumbnail?', 'http://tmb001.3d.ibolztv.net/media/thumbnail.php?', $val->thumbnail);
                    $thumbnail = str_replace('w=50', 'w=160', $thumbnail);
                    $tab_type = $val->tab_type;
                    $show_tab_menu = $val->show_tab_menu;
                    $show_dd_menu = $val->show_dd_menu;
                    $active = $val->active;
                    $count_product = $val->count_product;
                    $icon_size = $val->icon_size;
                    $poster_size = $val->poster_size;
                    $created = $val->created;
                    $sub_menu_list = $val->sub_menu_list;

                    switch($menu_type)
                    {
                        case 'menu' :
                              $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title).'/'.urlencode($title);
                              break;
                        case 'channel' :
                              $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                              break;
                        default:
                              $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                              break;
                    }

                    ?>
                    <div class="categories-item">
                        <div class="image-place">
                            <div style="width: 160px;height: 99px;background: url(<?php echo $thumbnail;?>) no-repeat scroll center;background-color: #85584d;"></div>
                        </div>
                        <div class="detail-place">
                            <div class="content">
                                <h3><?php echo $title;?></h3>
                                <p class="sub-title">DESCRIPTION</p>
                                <p class="sub-description"><?php echo (strlen($title) > 135 ? substr($title, 0, 135).'...' : $title);?></p>
                            </div>
                            <div class="content-other">
                                <div class="sosmed-sec">
                                    <div class="button-view">
                                        <a href="<?php echo $_link; ?>">VIEW</a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                <?php
                }
            }
            ?>

        </div>

        <div class="span4">
            <div class="row" style="height: 50px;">

            </div>

            <div class="row">
                <?php if(isset($content_popular)) echo $content_popular; ?>
            </div>

            <div class="row">
                <?php if(isset($content_latest)) echo $content_latest; ?>
            </div>

        </div>

    </div>

    <div class="container">
        <div class="row">
            <div id="content_menu" data-page='1' data-menu_id="<?php echo $menu_id;?>"></div>

            <div class="clear row loadmore-contentplace">
                <div class="text-center">
                    <div id="load_more_place" class="loadmore loadmore-bg">
                        <a id="cctv_load_more" class="loadmore-text" href="#">LOAD MORE</>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>