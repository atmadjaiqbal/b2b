<div class="container">
    <div class="home-content">
        <h3 class="slider-title">
            <span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i>
        </h3>

        <div class="container">
            <div class="row">
                <div id="content_menu" data-page='1' data-menu_id="<?php echo $menu_id;?>"></div>

                <div class="clear row loadmore-contentplace">
                    <div class="text-center">
                        <div id="load_more_place" class="loadmore loadmore-bg">
                            <a id="cctv_load_more" class="loadmore-text" href="#">LOAD MORE</>
                        </div>
                    </div>
                </div>
            </div>
        </div>



   </div>
</div>