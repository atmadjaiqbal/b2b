<ul class="nav nav-list">
  <li class="nav-header"><h4><?php echo str_replace("+", " ", $head_title); ?></h4></li>
  <?php foreach($list_content as $val) { 
    $head_product_id = $val->product_id;
    $head_menu_id = $val->menu_id;
    $head_menu_type = $val->menu_type;
    $head_title = $val->title;
    $_link = '';

    switch($val->menu_type)
    {
        case 'menu' :
    ?>
        <li>
          <a id="toggle2-menu<?php echo $val->menu_id;?>" data-menuid="<?php echo $val->menu_id;?>" data-mtitle="<?php echo urlencode($val->title);?>">
            <?php echo strtoupper($val->title);?>
          </a>
        </li>
        <script type="text/javascript">
          $('#toggle2-menu<?php echo $val->menu_id;?>').click(function() {
              // e.preventDefault();
              var menuid = $(this).data('menuid');
              var mtitle = $(this).data('mtitle');
              get_content_submenu(menuid, mtitle);
              return false;
          });

        </script>

        <?php
        // $_link = base_url().'content/content_categories_list/'.$head_menu_id.'/'.urlencode($val->title).'/'.urlencode($val->title);
        break;
        case 'channel' :
            $_link = base_url().'content/content_channel/'.$head_menu_id.'/'.$head_product_id;
        ?>
        <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($val->title);?></a></li>

            <?php
            break;
        default:
            ?>
        <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($val->title);?></a></li>
            <?php
            $_link = base_url().'content/content_categories/'.$head_menu_id.'/'.urlencode($val->title);
            break;
    }
    ?>
  <?php } ?>
</ul>