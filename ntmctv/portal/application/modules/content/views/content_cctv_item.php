<div class="view-page">
<div id="channel-categories-list">
    <ul class="playlist">
        <?php
        if($list_content->menu_list)
        {
            foreach($list_content->menu_list as $val)
            {
                $product_id = $val->product_id;
                $apps_id = $val->apps_id;
                $menu_id = $val->menu_id;
                $parent_menu_id = $val->parent_menu_id;
                $menu_type = $val->menu_type;
                $title = $val->title;
                $thumbnail = str_replace('http://media.ibolz.tv/thumbnail?', 'http://tmb001.3d.ibolztv.net/media/thumbnail.php?', $val->thumbnail);
                $thumbnail = str_replace('&w=50', '&w=160', $thumbnail);
                $link = $val->link;
                $tab_type = $val->tab_type;
                $show_tab_menu = $val->show_tab_menu;
                $show_dd_menu = $val->show_dd_menu;
                $active = $val->active;
                $count_product = $val->count_product;
                $icon_size = $val->icon_size;
                $poster_size = $val->poster_size;
                $created = $val->created;
                $sub_menu_list = $val->sub_menu_list;

                switch($menu_type)
                {
                    case 'menu' :
                        $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title);
                        break;
                    case 'channel' :
                        $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                        break;
                    default:
                        $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                        break;
                }
                ?>

                <li>
                    <a href="<?php echo $_link;?>" title="<?php echo $title;?>">
                        <div class="product-title">
                            <span class="pull-left"><?php echo (strlen($title) > 18 ? substr($title, 0, 15) . '...' : $title);?></span>
                            <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                        </div>
                        <div class="product-image">
                            <div class="preview_product related bgfull"
                                 style="background-color:#cecece;background: url('<?php echo $thumbnail;?>') no-repeat scroll center;width:160px;height:90px;"
                                 id="'<?php echo $menu_id;?>'-'<?php echo $product_id;?>'"
                                 data-id="'<?php echo $product_id;?>'" data-menu-id="'<?php echo $menu_id;?>'"></div>
                        </div>
                    </a>
                </li>
            <?php
            }
        }
        ?>
    </ul>
</div>
</div>