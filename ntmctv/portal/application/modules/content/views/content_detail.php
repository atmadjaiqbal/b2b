<div class="view-page">
  <div class="container">
  <div class="row-fluid">
    <?php
    if($product_detail)
    {
        $player_wrap_width = "620"; $player_width = "620"; $player_height = "400";
        $crew_category_list = array(); $genre_list = array(); $photo_list = array(); $related_list = array();
        $rcode = $product_detail->rcode;
        $channel_id = $product_detail->channel_id;
        $content_id = $product_detail->content_id;
        $title = $product_detail->title;
        $video_duration = $product_detail->video_duration;
        $filename = $product_detail->filename;
        $thumbnail = str_replace('http://media.ibolz.tv/thumbnail?', 'http://tmb001.3d.ibolztv.net/media/thumbnail.php?', $product_detail->thumbnail);
        $thumbnail = str_replace('w=170', 'w=300', $thumbnail);
        $description = $product_detail->description;
        $prod_year = $product_detail->prod_year;
        $trailer_id = $product_detail->trailer_id;
        $price = $product_detail->price;
        $ulike = $product_detail->ulike;
        $unlike = $product_detail->unlike;
        $comment = $product_detail->comment;
        if($product_detail->crew_category_list) { $lecture = $product_detail->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}
        $genre_list = $product_detail->genre_list;
        $photo_list = $product_detail->photo_list;
        $related_list = $product_detail->related_list;
        $document_list = $product_detail->document_list;
        $player = 'http://ply001.3d.ibolztv.net/player/flash/id/b2b.php?w='.$player_width.'&h='.$player_height;
        $player .= '&apps_id='.config_item('ibolz_app_id').'&version='.config_item('ibolz_version').'&type=content';
        $player .= '&channel_id='.$channel_id.'&vid='.$content_id.'&id='.$content_id.'&customer_id='.$customer_id;
    ?>

        <div class="bnicontent-detail">
            <h3 class="slider-title">
                <a href="#" style="cursor: default;"><span><?php echo $title;?></span></a>
            </h3>
            <div class="span4" style="margin-left:0px;">
                <div class="row item-sec">

                   <div class="poster-place">
                      <div class="poster" style="background-color:#cecece;background: url('<?php echo $thumbnail;?>&w=300px') no-repeat scroll top;width:300px;height:400px;"></div>
                   </div>

                   <div class="social overlay">
                       <ul class="content-score">
                           <li>
                               <a class="score-btn" id='like-<?php echo $content_id; ?>' data-id='<?php echo $content_id; ?>'>
                                   <img class="pull-left" src="<?php echo base_url('assets/images/like_icon_large_new.png'); ?>">
                               </a>&nbsp;&nbsp;<div id="totallike-<?php echo $content_id; ?>" data-id='<?php echo $content_id; ?>'><?php echo $ulike; ?></div>
                           </li>
                           <li>
                               <a class="score-btn" data-tipe='comment' data-id='<?php echo $content_id; ?>'>
                                   <img class="pull-left" src="<?php echo base_url('assets/images/comment_icon_large_new.png'); ?>">
                               </a>&nbsp;&nbsp;<?php echo $comment; ?>
                           </li>
                       </ul>
                   </div>

                  <h3 style="margin-top: 50px;">PHOTO</h3>
                  <div class="photolist">
                  <?php
                  if($photo_list)
                  {
                     $y = 1; $photo_filename = array();
                     foreach($photo_list as $value)
                     {
                         $photonya = $value->thumbnail.'&w=200';
                         $photofull = $value->thumbnail.'&w=300';
                         $photo_content_id[$y] = $value->content_id;

                         if(isset($value->thumbnail))
                         {
                             $photo_filename[$y] = '
                             <a href="'.$photofull.'" title="'.$title.'">
                                <div style="background: url('.$photonya.') no-repeat;background-size: 100% 100%;"></div>
                             </a>
                             ';
                         } else {
                             $photo_filename[$y] = '';
                         }

                         //$photo_filename[$y] = isset($value->thumbnail) ? 'style="background: url('.$photonya.') no-repeat;background-size: 100% 100%;"' : '';
                         $photo_thumbnail[$y] = $value->filename;
                         $y++;
                     }
                  }
                  ?>
                      <ul class="popup-gallery">
                          <li><div><?php echo isset($photo_filename[1]) ? $photo_filename[1] : '';?></div></li>
                          <li><div><?php echo isset($photo_filename[2]) ? $photo_filename[2] : ''?></div></li>
                          <li><div><?php echo isset($photo_filename[3]) ? $photo_filename[3] : ''?></div></li>
                          <li><div><?php echo isset($photo_filename[4]) ? $photo_filename[4] : ''?></div></li>
                          <li><div><?php echo isset($photo_filename[5]) ? $photo_filename[5] : ''?></div></li>
                          <li><div><?php echo isset($photo_filename[6]) ? $photo_filename[6] : ''?></div></li>
                          <li><div><?php echo isset($photo_filename[7]) ? $photo_filename[7] : ''?></div></li>
                          <li><div><?php echo isset($photo_filename[8]) ? $photo_filename[8] : ''?></div></li>
                          <li><div><?php echo isset($photo_filename[9]) ? $photo_filename[9] : ''?></div></li>
                      </ul>
                  </div>
                </div>
              </div>


            <div class="span8" style="margin-left:10px !important;">
                <div class="row item-sec">
                    <iframe seamless wmode="Opaque" class="player bg-loader" scrolling="no"
                            style='width: <?php echo $player_width;?>px;min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;'
                            src="<?php echo $player;?>">
                    </iframe>
                </div>

                <div class="row item-sec">

                <div class="row item-sec">
                    <h3>DESCRIPTION</h3>
                    <h4><?php echo $title; ?></h4>
                    <div class="content-info">
                        <dl>
                           <dt>Year</dt><dd>:&nbsp;&nbsp;<?php echo $prod_year;?></dd>
                           <dd></dd>
                        </dl>
                        <h3>DESCRIPTION</h3>
                        <p><?php echo $description; ?></p>
                    </div>
                    <div class="border-bott"></div>
                </div>

                <div class="row item-sec">
                    <h3>PLAYLIST</h3>
                    <?php
                if($list_content)
                {
                   $carousel_default = '<div class="product-title">
                                      <a href="">
                                        <span class="pull-left"></span>
                                        <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                      </a>
                                  </div>
                                  <div class="product-image" style="background-color: #dddddd;">
                                  </div>';


                   $carouselimg1 = $carousel_default;$carouselimg2 = $carousel_default;$carouselimg3 = $carousel_default;
                   $carouselimg4 = $carousel_default;$carouselimg5 = $carousel_default;$carouselimg6 = $carousel_default;$i = 1;
                   foreach($list_content->product_list as $rwcontent)
                   {
                      $sub_menu_id = $rwcontent->menu_id;
                      $sub_apps_id = $rwcontent->apps_id;
                      $sub_product_id = $rwcontent->product_id;
                      $sub_channel_category_id = $rwcontent->channel_category_id;
                      $sub_channel_id = $rwcontent->channel_id;
                      $sub_channel_type_id = $rwcontent->channel_type_id;
                      $sub_content_id = $rwcontent->content_id;
                      $sub_content_type = $rwcontent->content_type;
                      $sub_title = $rwcontent->title;
                      $short_title = strlen($sub_title) > 14 ? substr($sub_title, 0, 14). '...' : $sub_title;
                      $sub_thumbnail = $rwcontent->thumbnail.'&w=200px';
                      $sub_description = $rwcontent->description;
                      $sub_prod_year = $rwcontent->prod_year;
                      $sub_video_duration = $rwcontent->video_duration;
                      $sub_countlike = $rwcontent->countlike;
                      $sub_countviewer = $rwcontent->countviewer;
                      $sub_icon_size = $rwcontent->icon_size;
                      $sub_poster_size = $rwcontent->poster_size;
                      $sub_crew_category_list = $rwcontent->crew_category_list;
                      $sub_genre_list = $rwcontent->genre_list;

                      if($product_id != $sub_product_id)
                      {
                          $carouselItem = '
                          <div class="product-title">
                               <span class="pull-left">'.$short_title.'</span>
                               <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                          </div>
                          <a href="'.base_url().'content/content_detail/'.$sub_content_id.'/'.$sub_menu_id.'">
                             <div class="product-image">
                                <div class="preview_product related bgfull"
                                     style="background-color:#cecece;background: url('.$sub_thumbnail.'&w=160) no-repeat scroll center;width:160px;height:170px;"
                                     id="'.$sub_menu_id.'-'.$sub_product_id.'"
                                     data-id="'.$sub_product_id.'" data-menu-id="'.$sub_menu_id.'" src="'.$sub_thumbnail.'"></div>
                             </div>
                          </a>';
                          if($i == 1) $carouselimg1 = $carouselItem; if($i == 2) $carouselimg2 = $carouselItem;
                          if($i == 3) $carouselimg3 = $carouselItem; if($i == 4) $carouselimg4 = $carouselItem;
                          if($i == 5) $carouselimg5 = $carouselItem; if($i == 6) $carouselimg6 = $carouselItem;
                          $i++;
                      }
                   }
                ?>

                       <div class="carousel slide playlist-slider">
                           <a id="playlist_left" class="left carousel-control" href="#play-move" data-slide="next"></a>
                           <a id="playlist_right" class="right carousel-control" href="#play-move" data-slide="next"></a>
                           <div id="play-move" style="height: 270px !important;">
                               <div class="carousel-inner">
                                   <div class="item active">
                                       <div class="content-item"><?php echo isset($carouselimg1) ? $carouselimg1 : $carousel_default; ?></div>
                                       <div class="content-item"><?php echo isset($carouselimg2) ? $carouselimg2 : $carousel_default; ?></div>
                                       <div class="content-item"><?php echo isset($carouselimg3) ? $carouselimg3 : $carousel_default; ?></div>
                                   </div>
                                   <div class="item">
                                       <div class="content-item"><?php echo isset($carouselimg4) ? $carouselimg4 : $carousel_default; ?></div>
                                       <div class="content-item"><?php echo isset($carouselimg5) ? $carouselimg5 : $carousel_default; ?></div>
                                       <div class="content-item"><?php echo isset($carouselimg6) ? $carouselimg6 : $carousel_default; ?></div>
                                   </div>
                               </div>
                           </div>
                       </div>
                <?php
                   }
                ?>
            <div class="border-bott"></div>
        </div>

        <!-- div class="row item-sec">
            <h3>COMMENT</h3>

            <div><textarea id='commenttext' name="comment" class="media-input" rows="3"></textarea></div>
            <div class="row button-comment" style="margin-bottom: 40px;">
                <a id="sendcomment" data-id="< ?php echo $content_id; ?>">SEND</a>
            </div>

            <div id="comment" data-page="1" data-id="< ?php echo $content_id; ?>" class="comment-container"></div>

            <div class="clear row loadmore-contentplace">
                <div class="text-center">
                    <div id="load_more_place" class="loadmore loadmore-bg">
                        <a data-page="1" data-id="< ?php echo $content_id; ?>" id="comment_load_more" class="loadmore-text" href="#">LOAD MORE</a>
                    </div>
                </div>
            </div>

        </div -->

    <script type="text/javascript">

      $(document).ready(function() {
          $('#like-<?php echo $content_id; ?>').click(function(e){
              e.preventDefault();
              var id = $(this).data('id');
              $.post(Settings.base_url+'content/post_like/', {'content_id':id}, function(data){
                  if(data.rcode != 'ok'){
                      alert(data.msg);
                  } else {
                      liketotal('#totallike-<?php echo $content_id; ?>');
                  }
              });

          });

          $('#sendcomment').click(function(e){
              e.preventDefault();
              var product_id = $(this).data('id');
              var comment = $('#commenttext').val();
              $.post(Settings.base_url+'content/post_comment/', {'product_id':product_id, 'comment':comment}, function(data){
                  if(data.rcode == 'ok'){
                      $('#comment').prepend(data.msg);
                      $('#commenttext').val('');
                  }else{
                      alert(data.msg);
                  }
              });

          });


      });

    </script>

    <?php
    }
    ?>
          </div>
        </div>
      </div>
  </div>
</div>
</div>