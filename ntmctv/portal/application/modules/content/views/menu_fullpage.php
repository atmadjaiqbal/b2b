<div class="container">
  <div class="row-fluid">
    <div class="span3">
      <div class="well sidebar-nav logo">
        <div class="bni-logo">
          <img src="<?php echo base_url().'assets/images/Lambang_Polri.png';?>" class="left-logo">
          <p>NTMC TV</p>
          <p style="font-size: 14px;">Donwload Aplikasinya di</p>
          <a href="https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc" style="display: block;text-align: center;" target="_blank"><img style="margin: 0 auto;height: 45px;" class="google" src='http://ntmctv.ibolz.tv/portal/assets/images/googleplay.png'></a>
        </div>
      </div>
      <div class="well sidebar-nav sidebar-left">
        <ul class="nav nav-list">
          <li class="nav-header"><h4><?php echo str_replace("+", " ", $head_title); ?></h4></li>
          <?php foreach($list_content as $val) { 
            $head_product_id = $val->product_id;
            $head_menu_id = $val->menu_id;
            $head_menu_type = $val->menu_type;
            $head_title = $val->title;
            $_link = '';

            switch($val->menu_type)
            {
                case 'menu' :
            ?>
                <li>
                  <a id="toggle2-menu<?php echo $val->menu_id;?>" data-menuid="<?php echo $val->menu_id;?>" data-mtitle="<?php echo urlencode($val->title);?>">
                    <?php echo strtoupper($val->title);?>
                  </a>
                </li>
                <script type="text/javascript">
                  $('#toggle2-menu<?php echo $val->menu_id;?>').click(function() {
                      // e.preventDefault();
                      var menuid = $(this).data('menuid');
                      var mtitle = $(this).data('mtitle');
                      get_content_submenu(menuid, mtitle);
                      return false;
                  });

                </script>

                <?php
                // $_link = base_url().'content/content_categories_list/'.$head_menu_id.'/'.urlencode($val->title).'/'.urlencode($val->title);
                break;
                case 'channel' :
                    $_link = base_url().'content/content_channel/'.$head_menu_id.'/'.$head_product_id;
                ?>
                <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($val->title);?></a></li>

                    <?php
                    break;
                default:
                    ?>
                <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($val->title);?></a></li>
                    <?php
                    $_link = base_url().'content/content_categories/'.$head_menu_id.'/'.urlencode($val->title);
                    break;
            }
            ?>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="span9">
      <div style="margin-bottom: 10px;">
        <?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>
      </div>
      <div class="row-fluid">
        <div id="listsubmenu">
          <ul class="nav nav-list">
              <?php
              foreach($list_content as $val) { ?>

                  <li class="nav-header title-nav-right"><h4><?php echo $val->title; ?>
                  <?php
                  foreach ($val->sub_menu_list as $value) {
                    if (strrpos($value->title, "CCTV")) { ?>
                        <font style="float: right;cursor: pointer;" onclick="toggle_peta('peta1');">PETA</font>
                        <?php
                    }
                  }
                  break;
              }
              ?>
              
            </h4></li>
          </ul>
          <script type="text/javascript">
              function toggle_peta(id) {
                 var e = document.getElementById(id);
                 if(e.style.display == 'block')
                    e.style.display = 'none';
                 else
                    e.style.display = 'block';
              }
          </script>
          <?php
          if($list_content) {
              $crew_category_list = array(); $genre_list = array();
              foreach($list_content as $value)
              {
                  $i = 0;$first = true;
                  foreach ($value->sub_menu_list as $val) {
                    $product_id = $val->product_id;
                    $apps_id = $val->apps_id;
                    $menu_id = $val->menu_id;
                    $parent_menu_id = $val->parent_menu_id;
                    $menu_type = $val->menu_type;
                    $title = $val->title;
                    $short_title = strlen($title) > 30 ? substr($title, 0, 30). '...' : $title;
                    if (strrpos($val->link, "cctv") || strrpos($val->link, "other")) {
                      $thumbnail = $val->link."high/1.jpg";
                    }
                    else {          
                      $thumbnail = str_replace('http://media.ibolz.tv/thumbnail?', 'http://tmb001.3d.ibolztv.net/media/thumbnail.php?', $val->thumbnail);
                      $thumbnail = str_replace('w=50', 'w=160', $thumbnail);
                    }
                    $tab_type = $val->tab_type;
                    $show_tab_menu = $val->show_tab_menu;
                    $show_dd_menu = $val->show_dd_menu;
                    $active = $val->active;
                    $count_product = $val->count_product;
                    $icon_size = $val->icon_size;
                    $poster_size = $val->poster_size;
                    $created = $val->created;
                    $sub_menu_list = $val->sub_menu_list;

                    if ($i==0 || $i%4==0) { $margin = "margin: 0;"; } else { $margin = ""; }

                    if (strrpos($val->title, "CCTV")) {
                      $linkpeta = $val->link;
                      continue;
                    }

                    switch($menu_type)
                    {
                        case 'menu' :
                              $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title).'/'.urlencode($title);
                              break;
                        case 'channel' :
                              $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                              break;
                        default:
                              $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                              break;
                    }
                    // echo "<pre>";print_r($value);echo "</pre>";
                    // exit();
                    ?>
                    <div id="peta1"><iframe src="<?php echo $linkpeta; ?>"></iframe></div>
                    <div id="content<?php echo $i; ?>" class="span3" style="<?php echo $margin; ?>">    
                        <div class="categories-item">
                            <div class="image-place">
                                <a href="<?php echo $_link; ?>"><div style="width: 100%;height: 99px;background: url(<?php echo $thumbnail;?>) no-repeat scroll center;background-size: 155px 100px;"></div></a>
                            </div>
                            <div class="detail-place">
                                <div class="content">
                                    <p style="text-align: center;"><a href="<?php echo $_link; ?>" style="color: black !important;font-size: 14px !important;"><?php echo $title;?></a></p>
                                </div>
                            </div>
                            <a href="<?php echo $_link; ?>"><img src="<?php echo base_url();?>assets/images/asset__movie_play_header.png" class="play"></a>
                        </div>
                    </div>
                <?php
              $i++;
              }
              break;
            }
          }
          ?>
        </div>
      </div><!--/row-->
    </div><!--/span-->
  </div><!--/row-->
</div>