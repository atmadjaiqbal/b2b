<div class="view-page">
<div class="container">
    <div class="row-fluid">
        <div class="span8" id="content-categories">
            <h3 class="slider-title">
              <span><?php echo $menu_name;?></span>
            </h3>
            <?php
    /*
            echo "<pre>";
            print_r($list_content);
            echo "</pre>";
            exit;
    */
            if($list_content->product_list)
            {
                $crew_category_list = array(); $genre_list = array();
                foreach($list_content->product_list as $val)
                {

                    $menu_id = $val->menu_id;
                    $apps_id = $val->apps_id;
                    $product_id = $val->product_id;
                    $channel_category_id = $val->channel_category_id;
                    $channel_id = $val->channel_id;
                    $channel_type_id = $val->channel_type_id;
                    $content_id = $val->content_id;
                    $content_type = $val->content_type;
                    $title = $val->title;
                    $short_title = strlen($title) > 30 ? substr($title, 0, 30). '...' : $title;
                    $thumbnail = str_replace('http://media.ibolz.tv/thumbnail?', 'http://tmb001.3d.ibolztv.net/media/thumbnail.php?', $val->thumbnail);
                    $thumbnail = str_replace('w=50', 'w=160', $thumbnail);
                    $description = $val->description;
                    $prod_year = $val->prod_year;
                    $video_duration = $val->video_duration;
                    $countlike = $val->countlike;
                    $countcomment = $val->countcomment;
                    $countviewer = $val->countviewer;
                    $icon_size = $val->icon_size;
                    $poster_size = $val->poster_size;
                    $crew_category_list = $val->crew_category_list;
                    $genre_list = $val->genre_list;
                    $related_list = $val->related_list;
                    if($val->crew_category_list) { $lecture = $val->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}
                    $_link = base_url().'content/content_detail/'.$product_id.'/'.$menu_id.'/'.urlencode($title);

                    ?>
                    <div class="categories-item">
                        <div class="image-place">
                            <img src="<?php echo $thumbnail;?>">
                        </div>
                        <div class="detail-place">
                            <h3><?php echo $title;?></h3>
                            <p class="sub-title">DESCRIPTION</p>
                            <p class="sub-description"><?php echo (strlen($title) > 135 ? substr($title, 0, 135).'...' : $title);?></p>
                            <a href="<?php echo $_link; ?>"><div class="button-view">VIEW</div></a>
                        </div>
                    </div>


            <?php
                }
            }
            ?>

        </div>

        <div class="span4">
            <?php if(isset($content_popular)) echo $content_popular; ?>
            <?php if(isset($content_latest)) echo $content_latest; ?>
        </div>

        <div class="container">
            <div class="row">
                <div id="content_menu" data-page='1' data-menu_id="<?php echo $menu_id;?>"></div>

                <div class="clear row loadmore-contentplace">
                    <div class="text-center">
                        <div id="load_more_place" class="loadmore loadmore-bg">
                            <a id="cctv_load_more" class="loadmore-text" href="#">LOAD MORE</>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>