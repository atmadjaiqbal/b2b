<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="no-js">
	<head>
		<meta http-equiv="refresh" content="10; URL=https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc">

		<style type="text/css">
			@import url(http://fonts.googleapis.com/css?family=Oswald);
			*{
				padding: 0;
				margin: 0;
				font-family: 'Oswald', sans-serif !important;
				background-color: lightgray;
			}
			.page {
				width: 100%;
				height: 100%;
			}
			.page .logo {
				width: 40%;
				margin: 0 auto;
				display: block;
				padding-top: 50px;
			}
			.page .google {
				width: 50%;
				margin: 0 auto;
				display: block;
			}
			.page h1 {
				text-align: center;
				font-size: 5em !important;
			}
		</style>
	</head>
<body>
	<div class="page">
		<img class="logo" src="<?php echo base_url().'assets/images/Lambang_Polri.png';?>">
		<h1>NTMC TV</h1>
		<a href="https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc"><img class="google" src="<?php echo base_url().'assets/images/googleplay.png';?>"></a>
	</div>
</body>
</html>