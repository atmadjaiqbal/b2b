<div class="container">
<?php
if($listcontent)
{
    foreach($listcontent as $val)
    {
        $menu_id = $val['menu_id'];
        $menu_title = $val['title'];
        $menu_thumbnail = $val['thumbnail'];
        $menu_type = $val['menu_type'];
        $product_id = $val['product_id'];

        if($val['list_content'])
        {
?>

 <div id="products-<?php echo $menu_id;?>" class="home-content">
   <h3 class="slider-title">
      <a href="<?php echo base_url();?>content/content_categories/<?php echo $menu_type;?>/<?php echo $menu_id;?>/<?php echo urlencode($menu_title);?>"><span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i></a>
   </h3>
   <?php if(!isset($val['list_content']) || !$val['list_content']) { ?>
   <div class="alert alert-danger">
       <?php echo lang('no_products');?>
   </div>
   <?php } else { ?>

   <div class="content-item">

       <div class="row">
           <?php
           if(strtolower($menu_title) != 'vod' )
           {
           ?>
           <div class="span4 poster-content bg-loader" id="photo-<?php echo $menu_id; ?>">
             <div class="poster bgfull" style="background-color:#cecece;background: url() no-repeat scroll center;width:300px;height:450px;"></div>
             <!-- img class="poster bgfull" style="width:300px;height:450px;border: 1px solid #dddddd;"-->
          </div>
          <div class="span8" id="content-info-<?php echo $menu_id; ?>" style="height: 150px;"></div>
          <?php
           }
          ?>
       </div>
          <?php

          $carousel_default = '<div class="product-title">
                                  <a href="">
                                    <span class="pull-left"></span>
                                    <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                  </a>
                              </div>
                              <div class="product-image" style="border: 1px solid #FFEBD1;background-color:#FCF7EF;">
                              </div>';


          $carouselimg1 = $carousel_default;$carouselimg2 = $carousel_default;$carouselimg3 = $carousel_default;
          $carouselimg4 = $carousel_default;$carouselimg5 = $carousel_default;$carouselimg6 = $carousel_default;
          $carouselimg7 = $carousel_default;$carouselimg8 = $carousel_default;$carouselimg9 = $carousel_default;
          $carouselimg10 = $carousel_default;
          $i = 1;

          foreach($val['list_content'] as $rwcontent)
          {
             if($i == 1)
             {
                 $default_menu_id = $rwcontent->menu_id;
                 $default_product_id = $rwcontent->product_id;
                 $default_thumbnail = $rwcontent->thumbnail;
             }
             $sub_menu_id = $rwcontent->menu_id;
             $sub_menu_type = $rwcontent->menu_type;
             $sub_apps_id = $rwcontent->apps_id;
             $sub_product_id = $rwcontent->product_id;
             // $sub_channel_category_id = $rwcontent->channel_category_id;
             //$sub_channel_id = $rwcontent->channel_id;
             //$sub_channel_type_id = $rwcontent->channel_type_id;
             //$sub_content_id = $rwcontent->content_id;
             //$sub_content_type = $rwcontent->content_type;
             $sub_title = $rwcontent->title;
             $short_title = strlen($sub_title) > 14 ? substr($sub_title, 0, 14).'...' : $sub_title;
             $sub_thumbnail = $rwcontent->thumbnail;
             //$sub_description = $rwcontent->description;
             //$sub_prod_year = $rwcontent->prod_year;
             //$sub_video_duration = $rwcontent->video_duration;
             //$sub_countlike = $rwcontent->countlike;
             //$sub_countviewer = $rwcontent->countviewer;
             //$sub_icon_size = $rwcontent->icon_size;
             //$sub_poster_size = $rwcontent->poster_size;
             //$sub_crew_category_list = $rwcontent->crew_category_list;
             //$sub_genre_list = $rwcontent->genre_list;
          ?>
          <script type="text/javascript">
              $(document).ready(function(){
                  vod.content_view(
                      '#content-info-<?php echo $menu_id;?>', '<?php echo $default_product_id;?>', '<?php echo $default_menu_id; ?>', '<?php echo $sub_menu_type; ?>', '<?php echo urlencode($menu_title);?>',
                      function()
                      {
                          $('#photo-<?php echo $menu_id; ?> .poster').removeAttr('style').attr('style', 'background-color:#cecece;background: url(<?php echo $default_thumbnail.'&w=300';?>) no-repeat scroll center;width:300px;height:175px;');
                          //$('#photo-<?php echo $menu_id; ?> .poster').attr('src', '<?php echo $default_thumbnail;?>');
                          $('#photo-<?php echo $menu_id; ?>').removeClass('bg-loader');
                      }
                  );

                  $('#<?php echo $sub_menu_id; ?>-<?php echo $sub_product_id; ?>').on('click', function(ev) {
                      ev.preventDefault();
                      //  $('#photo-< ?php echo $menu_id; ?>').removeClass('bg-loader');
                      var product_id = $(this).data('id');
                      var menu_id = $(this).data('menu-id');
                      vod.content_view(
                          '#content-info-<?php echo $menu_id;?>', product_id, menu_id, '<?php echo $sub_menu_type; ?>', '<?php echo urlencode($menu_title);?>',
                          function(){
                              // $('#photo-<?php echo $menu_id; ?> .poster').attr('src', '<?php echo $sub_thumbnail;?>');
                              $('#photo-<?php echo $menu_id; ?> .poster').removeAttr('style').attr('style', 'background-color:#cecece;background: url(<?php echo $sub_thumbnail.'&w=300';?>) no-repeat scroll center;width:300px;height:175px;');
                              $('#photo-<?php echo $menu_id; ?>').removeClass('bg-loader');
                          }
                      );

                  });



              });
          </script>

          <?php
/*
              <img class="preview_product related bgfull" id="'.$sub_menu_id.'-'.$sub_product_id.'"
                                       data-id="'.$sub_product_id.'" data-menu-id="'.$sub_menu_id.'" src="'.$sub_thumbnail.'">
*/
              if(strtolower($menu_title) != 'vod' )
              {
                  $carouselItem = '<div class="product-title">
                                     <span class="pull-left">'.$short_title.'</span>
                                     <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                  </div>
                                  <div class="product-image">
                                       <div class="preview_product related bgfull"
                                            style="border: 1px solid #FFEBD1;background-color:#FCF7EF;background: url('.$sub_thumbnail.'&w=160) no-repeat scroll center;width:160px;height:98px;"
                                            id="'.$sub_menu_id.'-'.$sub_product_id.'"
                                            data-id="'.$sub_product_id.'" data-menu-id="'.$sub_menu_id.'" src="'.$sub_thumbnail.'"></div>
                                  </div>';
              } else {
                  $carouselItem = '<div class="product-title">
                                     <span class="pull-left">'.$short_title.'</span>
                                     <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                 </div>
                                 <div class="product-image">
                                   <a href="'.base_url().'content/content_categories_list/'.$sub_menu_id.'/'.urlencode($sub_title).'">
                                   <div class="preview_product related bgfull"
                                        style="border: 1px solid #FFEBD1;background-color:#FCF7EF;background: url('.$sub_thumbnail.'&w=160) no-repeat scroll center;width:160px;height:98px;"
                                        data-id="'.$sub_product_id.'" data-menu-id="'.$sub_menu_id.'" src="'.$sub_thumbnail.'"></div>
                                   </a>
                                 </div>';
              }

              if($i == 1) $carouselimg1 = $carouselItem; if($i == 2) $carouselimg2 = $carouselItem;
              if($i == 3) $carouselimg3 = $carouselItem; if($i == 4) $carouselimg4 = $carouselItem;
              if($i == 5) $carouselimg5 = $carouselItem; if($i == 6) $carouselimg6 = $carouselItem;
              if($i == 7) $carouselimg7 = $carouselItem; if($i == 8) $carouselimg8 = $carouselItem;
              if($i == 9) $carouselimg9 = $carouselItem; if($i == 10) $carouselimg10 = $carouselItem;
              $i++;
          }
          ?>

          <div class="row carousel slide panelslider">
              <a id="movie_caro_left" class="left carousel-control" href="#movieCarousel-<?php echo $menu_id;?>" data-slide="next"></a>
              <a id="movie_caro_right" class="right carousel-control" href="#movieCarousel-<?php echo $menu_id;?>" data-slide="next"></a>
              <div id="movieCarousel-<?php echo $menu_id;?>" style="height: 150px !important;">
                  <div class="carousel-inner">
                      <div class="item active">
                          <div class="content-item"><?php echo isset($carouselimg1) ? $carouselimg1 : $carousel_default; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg2) ? $carouselimg2 : $carousel_default; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg3) ? $carouselimg3 : $carousel_default; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg4) ? $carouselimg4 : $carousel_default; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg5) ? $carouselimg5 : $carousel_default; ?></div>
                      </div>
                      <div class="item">
                          <div class="content-item"><?php echo isset($carouselimg6) ? $carouselimg6 : $carousel_default; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg7) ? $carouselimg7 : $carousel_default; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg8) ? $carouselimg8 : $carousel_default; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg9) ? $carouselimg9 : $carousel_default; ?></div>
                          <div class="content-item"><?php echo isset($carouselimg10) ? $carouselimg10 : $carousel_default; ?></div>
                      </div>
                  </div>
              </div>
          </div>
   </div>

   <?php
   }
   ?>
</div>
<?php
        }
    }
}
?>
</div>