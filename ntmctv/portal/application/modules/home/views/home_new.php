<div class="container">
  <div class="row-fluid">
    <div class="span3">
      <div class="well sidebar-nav logo">
        <div class="bni-logo">
          <img src="<?php echo base_url().'assets/images/Lambang_Polri.png';?>" class="left-logo">
          <p>NTMC TV</p>
          <p style="font-size: 14px;">Donwload Aplikasinya di</p>

          <div class="apps-dowload">
             <a href="https://itunes.apple.com/us/app/ibolz-ntmc-tv/id1003063279?mt=8" target="_blank">
                <div class="ico-apps ico-ios"></div>
             </a>
             <a href="https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc" target="_blank">
                <div class="ico-apps ico-android"></div>
             </a>
          </div>

        </div>
      </div>
      <div class="well sidebar-nav sidebar-left">
        <ul class="nav nav-list">
        <?php foreach($headermenu->menu_list as $val) { ?>
          <li class="nav-header"><h4><?php echo strtoupper($val->title);?></h4></li>
            <?php if(count($val->sub_menu_list) > 0) { ?>
              <?php foreach($val->sub_menu_list as $submenu) { ?>
                  <?php
                  $head_product_id = $submenu->product_id;
                  $head_menu_id = $submenu->menu_id;
                  $head_menu_type = $submenu->menu_type;
                  $head_title = $submenu->title;
                  $_link = '';

                  switch($submenu->menu_type)
                  {
                      case 'menu' :
                  ?>
                      <li>
                        <a id="toggle2-menu<?php echo $submenu->menu_id;?>" data-menuid="<?php echo $submenu->menu_id;?>" data-mtitle="<?php echo urlencode($submenu->title);?>">
                          <?php echo strtoupper($submenu->title);?>
                        </a>
                      </li>
                      <script type="text/javascript">
                        $('#toggle2-menu<?php echo $submenu->menu_id;?>').click(function() {
                            // e.preventDefault();
                            var menuid = $(this).data('menuid');
                            var mtitle = $(this).data('mtitle');
                            get_content_submenu(menuid, mtitle);
                            return false;
                        });

                      </script>

                      <?php
                      // $_link = base_url().'content/content_categories_list/'.$head_menu_id.'/'.urlencode($val->title).'/'.urlencode($submenu->title);
                      break;
                      case 'channel' :
                          $_link = base_url().'content/content_channel/'.$head_menu_id.'/'.$head_product_id;
                      ?>
                      <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($submenu->title);?></a></li>

                          <?php
                          break;
                      default:
                          ?>
                      <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($submenu->title);?></a></li>
                          <?php
                          $_link = base_url().'content/content_categories/'.$head_menu_id.'/'.urlencode($submenu->title);
                          break;
                  }
                  ?>
              <?php } ?>
            <?php } 
            break;
            ?>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="span9">
      <div style="margin-bottom: 10px;">
        <?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>
      </div>
      <div class="row-fluid">
        <div id="listsubmenu">
          <?php foreach($headermenu->menu_list as $val) { ?>
              <?php
              if(count($val->sub_menu_list) > 0) {

                foreach($val->sub_menu_list as $submenu) { 
                  ?>
                  <ul class="nav nav-list">
                    <li class="nav-header title-nav-right"><h4><?php echo $submenu->title; ?><font style="float: right;cursor: pointer;" onclick="toggle_peta('peta1');">PETA</font></h4></li>
                  </ul>
                  <script type="text/javascript">
                      function toggle_peta(id) {
                         var e = document.getElementById(id);
                         if(e.style.display == 'block')
                            e.style.display = 'none';
                         else
                            e.style.display = 'block';
                      }
                  </script>
                  <?php
                  if(count($submenu->sub_menu_list) > 0) {
                    $i = -1;$first = true;
                    foreach($submenu->sub_menu_list as $submenu) {

                        // echo "<pre>";print_r($submenu);echo "</pre>";

                        $menu_id = $submenu->menu_id;
                        $product_id = $submenu->product_id;
                        $menu_type = $submenu->menu_type;
                        $title = $submenu->title;
                        $short_title = strlen($title) > 30 ? substr($title, 0, 30). '...' : $title;
                        // $thumbnail = str_replace('http://media.ibolz.tv/thumbnail?', 'http://tmb001.3d.ibolztv.net/media/thumbnail.php?', $submenu->thumbnail);
                        // $thumbnail = str_replace('w=50', 'w=160', $thumbnail);
                        $thumbnail = $submenu->link."high/1.jpg";
                        if ($i==0 || $i%4==0) { $margin = "margin: 0;"; } else { $margin = ""; }

                        switch($menu_type)
                        {
                            case 'menu' :
                                  $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title).'/'.urlencode($title);
                                  break;
                            case 'channel' :
                                  $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                                  break;
                            default:
                                  $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                                  break;
                        }
                        ?>
                        <script type="text/javascript">
                            document.getElementById("konten-1").style.display = "none";
                        </script>
                        <div id="peta1"><iframe src="<?php echo $submenu->link; ?>"></iframe></div>
                        <div id="konten<?php echo $i; ?>" class="span3" style="<?php echo $margin; ?>">  
                          <div class="categories-item">
                              <div class="image-place">
                                  <a href="<?php echo $_link; ?>"><div style="width: 100%;height: 99px;background: url(<?php echo $thumbnail;?>) no-repeat scroll center;background-size: 155px 100px;"></div></a>
                              </div>
                              <div class="detail-place">
                                  <div class="content">
                                      <p style="text-align: center;"><a href="<?php echo $_link; ?>" style="color: black !important;font-size: 14px !important;"><?php echo $title;?></a></p>
                                  </div>
                              </div>
                              <a href="<?php echo $_link; ?>"><img src="<?php echo base_url();?>assets/images/asset__movie_play_header.png" class="play"></a>
                          </div>
                        </div>
                    <?php
                    $i++;
                    } 
                    break;
                  }
                }
                break;
              }
            } ?>
        </div>
      </div><!--/row-->
    </div><!--/span-->
  </div><!--/row-->
</div>