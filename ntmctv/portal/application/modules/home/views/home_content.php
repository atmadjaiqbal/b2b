<div class="container">
<?php
if($listcontent)
{
   foreach($listcontent as $val)
   {
      $menu_id = $val['menu_id'];
      $menu_title = $val['title'];
      $menu_thumbnail = $val['thumbnail'];
      $menu_type = $val['menu_type'];
      $product_id = $val['product_id'];
      if($val['list_content'])
      {
?>

  <div id="products-<?php echo $menu_id;?>" class="home-content">
    <h3 class="slider-title">
        <span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i>
    </h3>
    <?php if(!isset($val['list_content']) || !$val['list_content']) { ?>
        <div class="alert alert-danger">
            <?php echo lang('no_products');?>
        </div>
    <?php } else { ?>

    <div id="channel-categories-list">
        <ul class="playlist">
<?php
          $i = 1;
          foreach($val['list_content'] as $rwcontent)
          {
              $product_id = $rwcontent->product_id;
              $apps_id = $rwcontent->apps_id;
              $menu_id = $rwcontent->menu_id;
              $parent_menu_id = $rwcontent->parent_menu_id;
              $menu_type = $rwcontent->menu_type;
              $title = $rwcontent->title;
              $thumbnail = str_replace('http://media.ibolz.tv/thumbnail?', 'http://tmb001.3d.ibolztv.net/media/thumbnail.php?', $rwcontent->thumbnail);
              $thumbnail = str_replace('&w=50', '&w=160', $thumbnail);
              $link = $rwcontent->link;
              $tab_type = $rwcontent->tab_type;
              $show_tab_menu = $rwcontent->show_tab_menu;
              $show_dd_menu = $rwcontent->show_dd_menu;
              $active = $rwcontent->active;
              $count_product = $rwcontent->count_product;
              $icon_size = $rwcontent->icon_size;
              $poster_size = $rwcontent->poster_size;
              $created = $rwcontent->created;
              $sub_menu_list = $rwcontent->sub_menu_list;

              switch($menu_type)
              {
                  case 'menu' :
                      $_link = base_url().'content/content_categories_list_home/'.$menu_id.'/'.urlencode($menu_title).'/'.urlencode($title);
                      break;
                  case 'channel' :
                      $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                      break;
                  default:
                      $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                      break;
              }
?>

              <li>
                  <a href="<?php echo $_link;?>">
                      <div class="product-title">
                          <span class="pull-left"><?php echo (strlen($title) > 18 ? substr($title, 0, 15) . '...' : $title);?></span>
                          <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                      </div>
                      <div class="product-image">
                          <div class="preview_product related bgfull"
                               style="background-color:#cecece;background: url('<?php echo $thumbnail;?>') no-repeat scroll center;width:160px;height:90px;"
                               id="'<?php echo $menu_id;?>'-'<?php echo $product_id;?>'"
                               data-id="'<?php echo $product_id;?>'" data-menu-id="'<?php echo $menu_id;?>'"></div>
                      </div>
                  </a>
              </li>


          <?php
          }
?>
        </ul>
    </div>

<?php
         }
?>
  </div>

<?php
      }

   }

}
?>

</div>