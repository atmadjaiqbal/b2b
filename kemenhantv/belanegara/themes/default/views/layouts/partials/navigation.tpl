        {assign "privilege_list" $smarty.session.user_account->privilege_list}

        <ul class="sidebar-menu">
            <li class="header">Main Navigation</li>
            <li {if ($navigation_id=="dashboard")}class="active"{/if}>
                <a href="{base_url()}">
                    <i class="fa fa-th"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview{if ($navigation_id=="account")} active{/if}" id="account">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Account</span> <i class="fa fa-angle-down pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    {if $privilege_list['account_view'] == 1}
                    <li {if ($navigation_menu) == "account_list"}class="active"{/if}><a href="{base_url('account/account_list')}"><i class="fa fa-circle-o"></i> Account</a></li>
                    {/if}
                    {if $privilege_list['role_view'] == 1}
                    <li {if ($navigation_menu) == "role"}class="active"{/if}><a href="{base_url('account/role')}"><i class="fa fa-circle-o"></i> Role</a></li>
                    {/if}
                    {if $privilege_list['privilege_view'] == 1}
                    <li {if ($navigation_menu) == "privilege"}class="active"{/if}><a href="{base_url('account/privilege')}"><i class="fa fa-circle-o"></i> Privilege</a></li>
                    {/if}
                    {if $privilege_list['account_group_view'] == 1}
                    <li {if ($navigation_menu) == "account_group"}class="active"{/if}><a href="{base_url('account/group')}"><i class="fa fa-circle-o"></i> Group</a></li>
                    {/if}
                </ul>
            </li>
            <li class="treeview{if ($navigation_id=="master")} active{/if}" id="master">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Master Data</span> <i class="fa fa-angle-down pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    {if $privilege_list['province_view'] == 1}
                    <li {if ($navigation_menu) == "province"}class="active"{/if}><a href="{base_url('master/province')}"><i class="fa fa-circle-o"></i> Province</a></li>
                    {/if}
                    {if $privilege_list['city_view'] == 1}
                    <li {if ($navigation_menu) == "city"}class="active"{/if}><a href="{base_url('master/city')}"><i class="fa fa-circle-o"></i> City</a></li>
                    {/if}
                    {if $privilege_list['pusdiklat_view'] == 1}
                    <li {if ($navigation_menu) == "pusdiklat"}class="active"{/if}><a href="{base_url('master/pusdiklat')}"><i class="fa fa-circle-o"></i> Pusdiklat</a></li>
                    {/if}
                    {if $privilege_list['institution_view'] == 1}
                    <li {if ($navigation_menu) == "institution"}class="active"{/if}><a href="{base_url('master/institution')}"><i class="fa fa-circle-o"></i> Institution</a></li>
                    {/if}
                    {if $privilege_list['certificate_type_view'] == 1}
                    <li {if ($navigation_menu) == "certificate_type_list"}class="active"{/if}><a href="{base_url('master/certificate/type_list')}"><i class="fa fa-circle-o"></i> Certificate Type</a></li>
                    {/if}
                    {if $privilege_list['certificate_template_view'] == 1}
                    <li {if ($navigation_menu) == "certificate_template_list"}class="active"{/if}><a href="{base_url('master/certificate/template_list')}"><i class="fa fa-circle-o"></i> Template</a></li>
                    {/if}
                    {if $privilege_list['certificate_author_view'] == 1}
                    <li {if ($navigation_menu) == "certificate_authority"}class="active"{/if}><a href="{base_url('master/certificate/certificate_authority')}"><i class="fa fa-circle-o"></i> Authority</a></li>
                    {/if}
                </ul>
            </li>
            <li class="treeview{if ($navigation_id=="administration")} active{/if}" id="administration">
                <a href="#">
                    <i class="fa fa-gift"></i> <span>Administration</span> <i class="fa fa-angle-down pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <!-- Customer -->
                    <li {if ($navigation_menu) == "customer_register"}class="active"{/if}><a href="{base_url('administration/customer/registration_form')}"><i class="fa fa-circle-o"></i> Register Customer</a></li>
                    <li {if ($navigation_menu) == "customer_list"}class="active"{/if}><a href="{base_url('administration/customer')}"><i class="fa fa-circle-o"></i> Customer List</a></li>
                    <!-- End of Customer -->
                    <!-- Certificate -->
                    <li {if ($navigation_menu) == "certificate_request_list"}class="active"{/if}><a href="{base_url('administration/certificate/request_list')}"><i class="fa fa-circle-o"></i> Certificate Request</a></li>
                    <li {if ($navigation_menu) == "certificate_list"}class="active"{/if}><a href="{base_url('administration/certificate/')}"><i class="fa fa-circle-o"></i> Certificate List</a></li>
                </ul>
            </li>
        </ul>