<!-- FAVICON -->
<link rel="shortcut icon" href="{assets_path('img/favicon.ico')}" />
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" type="text/css" href="{assets_path('js/bootstrap/css/bootstrap.min.css')}" media="screen"/>
<!-- Theme style -->
{css("AdminLTE.min.css")}
<!-- Font Awesome -->
{css("font-awesome.min.css")}
<!-- Ionicons -->
<!-- {css("ionicons.min.css")} -->

<!-- AdminLTE Skins -->
{css("skins/skin-red.min.css")}
<!-- iCheck -->
<!--<link rel="stylesheet" type="text/css" href="{assets_path('js/iCheck/square/yellow.css')}" media="screen"/>-->
<!-- Morris chart -->
<!--<link rel="stylesheet" type="text/css" href="{assets_path('js/morris/morris.css')}" media="screen"/>-->
<!-- jvectormap -->
<!--<link rel="stylesheet" type="text/css" href="{assets_path('assets/js/jvectormap/jquery-jvectormap-1.2.2.css')}" media="screen"/>-->
<!-- Date Picker -->
<!--<link rel="stylesheet" type="text/css" href="{assets_path('assets/js/datepicker/datepicker3.css')}" media="screen"/>-->
<link rel="stylesheet" type="text/css" href="{assets_path('js/select2/css/select2.min.css')}" media="screen"/>
<link rel="stylesheet" type="text/css" href="{assets_path('js/iCheck/square/blue.css')}" media="screen"/>
<link rel="stylesheet" type="text/css" href="{assets_path('js/datatables/dataTables.bootstrap.css')}" media="screen"/>




<!-- Main Style -->
<link rel="stylesheet" type="text/css" href="{base_url('themes/default/assets/css/main.css?version=1.0')}" media="screen"/>
<!-- {css("main.css?v=1.0")} -->

<!-- Javascript Collections -->
<!-- jQuery 2.1.4 -->
{js("jQuery/jQuery-2.1.4.min.js")}
<!-- jQuery UI 1.11.4 -->
<!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!--
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
-->
<!-- Bootstrap 3.3.5 -->
{js("bootstrap/js/bootstrap.min.js")}
<!-- Morris.js charts -->
<!--{js("raphael-min.js")}
{js("morris/morris.min.js")}-->

{js("app.min.js")}

{js("datatables/jquery.dataTables.min.js")}
{js("datatables/dataTables.bootstrap.min.js")}
{js("select2/js/select2.full.min.js")}
{js("iCheck/icheck.min.js")}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- {js("pages/dashboard.js")} -->
<!-- AdminLTE for demo purposes -->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

