<script type="text/javascript">
	$(document).ready(function(){
        $.unblockUI();
            var now = new Date();
            var cal_daterangepicker = $('#cal').daterangepicker({ 
                startDate: '{$this->session->userdata('start_date_period')|date_format:'%m-%d-%Y'}',
                endDate: '{$this->session->userdata('end_date_period')|date_format:'%m-%d-%Y'}'
            },
            function(start, end) {
                if (start && end) {
                    $.blockUI({
                        css: { 
                                border: 'none', 
                                padding: '30px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff',
                                'font-size' : '20px'
                        }
                    });
                    $.getJSON('setting/apply', { 
                            startDate_day: start.format("DD"),
                            startDate_month: start.format("M"),
                            startDate_year: start.format("YYYY"),
                            endDate_day: end.format("DD"),
                            endDate_month: end.format("M"),
                            endDate_year: end.format("YYYY")
                        }, function(data){
                            $('#container').load(window.location.href);
                        }
                    );
                }
             }
             );
	});
</script>


<div class="timeline">
  <h5>from {$this->session->userdata('start_date_period')|date_format:'%d%B.%Y'} to 
  {$this->session->userdata('end_date_period')|date_format:'%d%B.%Y'}<span id="cal" class="cal" ><i class="icon-sort"></i></span>	</h5>
  <!--<img src="{theme_url("assets/js/holder.js/1000x80")}" class="img-polaroid">-->
<hr />
</div>
