<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bela Negara | {$navigation_title}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        {include file="partials/metadata.tpl"}
        
    </head>
    <body class="hold-transition skin-red sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="{base_url()}" class="logo">
                    <!-- {image('kemhan_logo_header.png')} -->
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <!-- <span class="logo-mini"><b>Admin</b></span> -->
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Bela</b>Negara</span>
                </a>
                {include file="partials/headnavigation.tpl"}

            </header>

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    {include file="partials/navigation.tpl"}
                </section>
                <!-- /.sidebar -->
            </aside>

            {include file="$template_view"}

            {include file="partials/footer.tpl"}

        </div><!-- ./wrapper -->
    </body>
</html>