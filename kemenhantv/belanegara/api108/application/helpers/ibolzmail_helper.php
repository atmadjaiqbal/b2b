<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * iBOLZ Army
 * 
 * iBOLZ Army API
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!function_exists('get_mail_config')) {

    function get_mail_config() {
        return Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.ibolztv.net',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@ibolztv.net',
            'smtp_pass' => 'Jagoan-123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
    }

}

if (!function_exists('send_async_email')) {

    function send_async_email($ci_instance, $to, $subject, $message, $config = NULL) {
        if ($config == NULL) {
            $ci_instance->load->library('email', get_mail_config());
        } else {
            $ci_instance->load->library('email', $config);
        }

        $ci_instance->email->to($to);
        $ci_instance->email->from('noreply@ibolztv.net', 'iBOLZ');
        $ci_instance->email->subject($subject);
        $ci_instance->email->message($message);

        if ($ci_instance->email->send()) {
            $response = array(
                'rcode' => 1,
                'message' => 'Email sent'
            );
        } else {
            $response = array(
                'rcode' => 0,
                'message' => escapeshellarg($ci_instance->email->print_debugger())
            );
        }

        return $response;
    }

}


if (!function_exists('send_army_register')) {

    /**
     * An Helper to send email about army registration
     * 
     * @param type $ci_instance
     * @param type $to
     * @param type $customer_name
     * @param type $army_code
     * @return type
     */
    function send_army_register($ci_instance, $to, $customer_name, $army_code) {

        $message = "
                Selamat datang prajurit $customer_name<br><br>
                    Selamat bergabung di iBOLZ Army.<br>
                    Kumpulkan point sebanyak-banyak nya untuk mendapatkan reward dan juga hadiah langsung menarik <br>
                    Reward akan diakumulasikan berdasarkan jumlah point. Setiap point nya bernilai Rp.500,00 dan akan ditransfer<br>
                    setiap bulan ke rekening yang telah kamu masukan. Minimum point yg akan ditransfer adalah 20 point setiap bulan nya.<br><br>
                    Pastikan kamu telah mengisi semua data dengan benar termasuk no rekening dan nama pemilik rekening.<br><br>
                    Hadiah menarik langsung berupa Hand Phone, Laptop, TV, dan Mobil sudah siap buat kamu yang lebih dulu mencapai pangkat tertentu.<br><br>
                    
                    Army code kamu adalah : <b><strong>$army_code</strong></b><br><br><br>
                    Terima kasih,<br><br>Jendral iBOLZ Army";

        return send_async_email($ci_instance, $to, 'Selamat datang di iBOLZ Army', $message);
    }

}


if (!function_exists('send_army_shot')) {

    /**
     * An helper to send email about army shots
     * 
     * @param type $ci_instance
     * @param type $to
     * @param type $customer_name
     * @param type $army_code
     * @param type $total_point
     * @param type $total_reward
     * @return type
     */
    function send_army_shot($ci_instance, $to, $customer_name, $army_code, $total_point, $total_reward) {
        date_default_timezone_set("Asia/Jakarta");
        $now = date('F jS, Y H:i:s');

        $message = "
                Hallo $customer_name<br><br>
                Selamat. Kamu telah berhasil mendapat 1 point di iBolz Army pada $now.<br>
                Total Point : <b>$total_point</b><br>
                Total Reward: <b>$total_reward</b><br><br><br>
                Kumpulkan point sebanyak-banyak nya untuk mendapatkan reward dan juga hadiah langsung menarik <br>
                Reward akan diakumulasikan berdasarkan jumlah point. Setiap point nya bernilai Rp.500,00 dan akan ditransfer<br>
                setiap bulan ke rekening yang telah kamu masukan. Minimum point yg akan ditransfer adalah 20 point setiap bulan nya.<br><br>
                Pastikan kamu telah mengisi semua data dengan benar termasuk no rekening dan nama pemilik rekening.<br><br>
                Hadiah menarik langsung berupa Hand Phone, Laptop, TV, dan Mobil sudah siap buat kamu yang lebih dulu mencapai pangkat tertentu.<br><br>
                Army Code kamu adalah : <b><strong>$army_code</strong></b><br><br><br>
                Terima kasih,<br><br>Jendral iBOLZ Army";

        return send_async_email($ci_instance, $to, 'Selamat. Kamu berhasil mendapat point', $message);
    }

}


if (!function_exists('exec_get')) {


    function exec_get_async($url) {

        //server run on windows
//        $runCommand = '"C:\Program Files (x86)\GnuWin32\bin\wget" -b ' . '"' . $url . '"';
//        $WshShell = new COM("WScript.Shell");
//        $oExec = $WshShell->Run($runCommand, 7, false);

        //server run on linux
        exec('wget -b -qO- '. $url . ' &> /dev/null &');
    }

    function exec_post_async($url, $data = null) {

        //server run on windows
//        $runCommand = '"C:\Program Files (x86)\GnuWin32\bin\wget" -b "' . $url . '" --post-data "' . (!empty($data) ? $data : '') . '"';
//        $WshShell = new COM("WScript.Shell");
//        $oExec = $WshShell->Run($runCommand, 7, false);

        //server run on linux
        exec('wget -b -qO- ' . '"' . $url .'" --post-data "' . (!empty($data) ? $data : '') .' &> /dev/null &');
    }

}