<?php

/******************************************
US English
Authentication Language
******************************************/

$lang["missing_app_id"] = "Ibolz Application ID is required";
$lang["missing_app_version"] = "Ibolz Application version is required";
$lang["missing_menu_id"] = "Ibolz Menu ID is required";

$lang["error_app_id"] = "Invalid Ibolz Application ID";
$lang["missing_group_id"] = "Customer Group ID is required";
$lang["error_group_id"] = "Invalid Customer Group";
$lang["missing_customer_id"] = "Customer ID is required";
$lang["missing_mobile_no"] = "Invalid Mobile Number";
$lang["missing_email"] = "Invalid Email address";
$lang["missing_email_or_mobile_no"] = "Email or Mobile Number is required";
$lang["missing_password"] = "Invalid Password, minimum 6 characters required";
$lang["missing_first_name"] = "First Name is required!";
$lang["missing_last_name"] = "Last Name is required!";

$lang["error_existing_email"] = "Email address already exist!";
$lang["error_existing_mobile_no"] = "Mobile Number already exist!";

$lang["register_successed"] = "Registration succeeded";
$lang["register_failed"] = "Registration failed";
$lang["auth_successed"] = "Authentication succeeded";

$lang["error_login"] = "Account not found or Wrong password!";

$lang["auth_error_app_id"] = "auth;" . $lang["error_app_id"] ;
$lang["auth_ok"] = "auth;OK";
