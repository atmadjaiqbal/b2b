<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Vod extends CI_Controller{
	
	function Vod(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('vod_model');
		$this->load->model('channel_model');
		$this->load->model('apps_model');
		$this->load->model('server_model');
		$this->load->model('schedule_model');
		$this->load->model('program_model');
		$this->load->model('statistic_model');
        $this->output->set_content_type('application/json');
	}	
	
	
	function channel_category_list(){
        $apps_id = $this->input->post("apps_id");
		$lang = $this->input->post("lang");
		$customer_id = $this->input->post("customer_id");
		
		$channel_category_list = $this->vod_model->channel_category_list($apps_id, $customer_id, $lang);
		$response = array(
        	"channel_category_list" => $channel_category_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}


	function content_list(){
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
        $apps_id = $this->input->post("apps_id");
		$lang = $this->input->post("lang");
        $parent_category_id = $this->input->post("parent_category_id");
        $channel_category_id = $this->input->post("channel_category_id");
        $genre_id = $this->input->post("genre_id");
        $crew_id = $this->input->post("crew_id");
        $search_key = $this->input->post("search_key");
		
		
		$channel_category_list = $this->vod_model->content_list($page, $limit, $apps_id, $parent_category_id, $channel_category_id, $genre_id, $crew_id, $search_key, $lang);
		$response = array(
        	"channel_category_list" => $channel_category_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}


	function content_vod_list(){
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
        $apps_id = $this->input->post("apps_id") == "" ? "" : $this->input->post("apps_id");
		$lang = $this->input->post("lang") == "" ? "" : $this->input->post("lang");
        $channel_category_id = $this->input->post("channel_category_id") == "" ? "" : $this->input->post("channel_category_id");
        $channel_id = $this->input->post("channel_id") == "" ? "" : $this->input->post("channel_id");
        $genre_id = $this->input->post("genre_id") == "" ? "" : $this->input->post("genre_id");
        $crew_id = $this->input->post("crew_id") == "" ? "" : $this->input->post("crew_id");
        $search_key = $this->input->post("search_key") == "" ? "" : $this->input->post("search_key");
		$customer_id = $this->input->post("customer_id") == "" ? "" : $this->input->post("customer_id");
		
		
		$content_vod_list = $this->vod_model->content_vod_list($page, $limit, $apps_id, $channel_category_id, $channel_id, $genre_id, $crew_id, $search_key, $lang, $customer_id);
		$response = array(
        	"content_list" => $content_vod_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $lang,
				"channel_id" => $channel_id,
				"channel_category_id" => $channel_category_id,
				"genre_id" => $genre_id,
				"crew_id" => $crew_id,
				"search_key" => $search_key	
			)
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}


	function content_channel_list(){
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
        $apps_id = $this->input->post("apps_id");
		$lang = $this->input->post("lang");
        $parent_category_id = $this->input->post("parent_category_id");
        $channel_category_id = $this->input->post("channel_category_id");
        $genre_id = $this->input->post("genre_id");
        $crew_id = $this->input->post("crew_id");
        $search_key = $this->input->post("search_key");
		
		
		$content_channel_list = $this->vod_model->content_channel_list($page, $limit, $apps_id, $parent_category_id, $channel_category_id, $genre_id, $crew_id, $search_key, $lang);
		$response = array(
        	"content_list" => $content_channel_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}





	//New
	/*
	function channel_category_list(){
        $apps_id = $this->input->get("apps_id");
		$lang = $this->input->get("lang");
		
		$channel_category_list = $this->vod_model->channel_category_list($apps_id, $lang);
		$response = array(
        	"channel_category_list" => $channel_category_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	*/
	

	
	
	
	///Before
	
	function channel_list(){
        $apps_id = $this->input->post("apps_id");
		$channel_list = $this->vod_model->channel_list($apps_id, 3);
		
		$response = array(
        	"channel_list" => $channel_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
		
	}
	
	
	function content_category_list(){
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

        $apps_id = $this->input->post("apps_id");
        $channel_id = $this->input->post("channel_id");
        $content_category_id = $this->input->post("content_category_id");
        $search_key = $this->input->post("search_key");
		
		
		$content_category_list = $this->vod_model->content_category_list($page, $limit, $apps_id, $channel_id, $content_category_id, $search_key);
		
		$response = array(
        	"content_category_list" => $content_category_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	
	function vod_list(){
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

        $apps_id = $this->input->post("apps_id");
        $channel_id = $this->input->post("channel_id");
        $content_category_id = $this->input->post("content_category_id");
        $search_key = $this->input->post("search_key");
		
		
		$vod_list = $this->vod_model->vod_content_list($page, $limit, $apps_id, $channel_id, $content_category_id, $search_key);
		
		$response = array(
        	"content_list" => $vod_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	
	function search_vod_list(){
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

        $apps_id = $this->input->post("apps_id");
        $search_key = $this->input->post("search_key");

        $genre_id = $this->input->post("genre_id");
        $crew_id = $this->input->post("crew_id");
				
		$vod_list = $this->vod_model->search_vod_list($page, $limit, $apps_id, $search_key, $genre_id, $crew_id);
		
		$response = array(
        	"content_list" => $vod_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	
	function vod_content_list(){
        $program_id = $this->input->post("program_id");
		$content_list = $this->vod_model->vod_content_list($program_id);
		$response = array(
        	"content_list" => $content_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
		
	}
	
	
	function channel_detail(){
        $apps_id = $this->input->post("apps_id");
        $channel_id = $this->input->post("channel_id");
        $timezone = $this->input->post("timezone");
        if (empty($timezone)) $timezone = "+07:00";

			$channel = $this->vod_model->channel_detail($apps_id, $channel_id, $timezone);

        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($channel));
		
		
	}
	
	
	function content_detail(){
        $apps_id = $this->input->post("apps_id");
        $channel_id = $this->input->post("channel_id");
        $content_id = $this->input->post("content_id");
		$content = $this->vod_model->content_detail($apps_id, $channel_id, $content_id);

		if($content && $content->trailer_id){
			$content->trailer = $this->vod_model->trailer_detail($content->trailer_id);
		}
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($content));
		
		
	}
	
	
	function content_related_detail(){
        $content_id = $this->input->post("content_id");
        $content_related_id = $this->input->post("content_related_id");
		$content = $this->vod_model->content_related_detail($content_id, $content_related_id);

		if($content && $content->trailer_id){
			$content->trailer = $this->vod_model->trailer_detail($content->trailer_id);
		}
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($content));
		
		
	}
	
	
	/*
	function content_list(){
		$content_list = $this->vod_model->content_list();
		$response = array(
        	"content_list" => $content_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
		
	}
	*/
	
	
	function content_like(){
        $content_id = $this->input->post("content_id");
        $customer_id = $this->input->post("customer_id");
        $like = $this->input->post("like");

		$content = $this->vod_model->content_like($content_id, $customer_id, $like);

        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($content));
		
	}
	
	
	function content_comment(){
        $content_id = $this->input->post("content_id");
        $customer_id = $this->input->post("customer_id");
        $comment = $this->input->post("comment");

		$content = $this->vod_model->content_comment($content_id, $customer_id, $comment);

        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($content));
		
	}
	
	
	function content_comment_list(){
        $content_id = $this->input->post("content_id");
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$comment_list = $this->vod_model->content_comment_list($page, $limit, $content_id);

		$response = array(
        	"comment_list" => $comment_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
	}
	
	
	function vod_buy(){
        $apps_id = $this->input->post("apps_id");
        $content_id = $this->input->post("content_id");
        $channel_id = $this->input->post("channel_id");
        $customer_id = $this->input->post("customer_id");
        $password = $this->input->post("password");
		$mobile_id = $this->input->post("mobile_id");
		$longitude = $this->input->post("longitude");
		$latitude = $this->input->post("latitude");

		$customer = $this->vod_model->customer_balance($customer_id, $password);
		if($customer){
			if($customer->credit_balance == 0){
				$response = array(
		        	"rcode" => 0,
					"message" => "Point anda tidak cukup, saldo 0"
		        );
			}else{
				$vod = $this->vod_model->vod_price($apps_id, $channel_id, $content_id);
				if($customer->credit_balance < $vod->price){
					$response = array(
			        	"rcode" => 0,
						"message" => "Point anda tidak cukup, saldo " . $customer->credit_balance
			        );
				}else{
					$transaction_code = $this->vod_model->vod_buy($apps_id, $content_id, $customer_id, $mobile_id);
					$response = array(
			        	"rcode" => 1,
						"message" => "Thank you, transaction ID : " . $transaction_code
			        );
				}
			}
		}else{
			$response = array(
	        	"rcode" => 0,
				"message" => "Invalid Password"
	        );
			
		}

        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
	}
	
	
	
	
	
	function test(){
		for ($x=-6; $x<=0; $x++) {
			$schedule_date = date("Y-m-d", strtotime ( $x . ' day', strtotime (date("Y-m-d")))) ;
			echo($schedule_date . ';');
		} 
		
		//$schedule_date = date("Y-m-d", strtotime ( '0 day', strtotime (date("Y-m-d")))) ;
		
		//echo($schedule_date);

	}
	
	
	
	
	

    	

    
}
