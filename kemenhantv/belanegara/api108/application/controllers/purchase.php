<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Purchase extends CI_Controller{
	
	function Purchase(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('purchase_model');
	}	
	


 	function subscription(){
		
		$expired_days = '0';
		$msisdn 		= isset($_SERVER['HTTP_MSISDN']) ? $_SERVER['HTTP_MSISDN'] : '';
		$apps_id 			= $this->input->post("apps_id");
		
		$data = array(
			"msisdn"=>$msisdn,
			"rcode"=> '2',
			"message"=> 'Not subscribe.',
			"expired"=> '0 days',
			'product' => array(),
			'params' => array('msisdn' => $msisdn, 'customer_products' => array()),			
		);
		
		if ($apps_id == 'com.balepoint.ibolz.mtn.ng') {
			$subscriber = $this->customer_model->get_subscription_by_msisdn($msisdn, $apps_id);

		} else {
			$customer_id = $this->input->post("customer_id");
			$subscriber = $this->customer_model->get_subscription_by_customer($customer_id, $apps_id);
		}		

		if (!empty($subscriber)) {
			$data['product'] = array(
				'product_id' => $subscriber->product_id,
				'product_name' => $subscriber->product_name,
				'product_price' => $subscriber->product_price,
				'active_days' => $subscriber->active_days
			);
			
			$status = $subscriber->subscription_status;
			$expired = $subscriber->expiry_time;
			if (!empty($expired)) {
				$timestamp = time();
				$expired = strtotime($expired);
				$diff = $timestamp - $expired;
				$expired_days = round($diff / 86400);

				if ($timestamp > $expired) {
						//echo 'expired <br>';
						$data['rcode'] = '3';
				} else {
					//echo 'not expired<br>';
					$data['rcode'] = $status;
				}								
				
			} else {
				$data['rcode'] = $status;
			}
			
			if ($data['rcode'] == '1') {
				$data['message'] = 'Success';
				$data['expired'] = abs($expired_days) . ' days';

			} elseif ($data['rcode'] == '3') {
				$data['message'] = 'Expired';
				$data['expired'] = $expired_days . ' days';
			}
			if ($status == '0') $data['message'] = 'Inactive';
		}

		$customer_products = $this->customer_model->customer_products($customer_id, $apps_id);
		if (!empty($customer_products)) {	
			$data['params']['customer_products'] = $customer_products;
		}
		
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
 	}

 	function provider(){
		$apps_id 			= $this->input->post("apps_id");
		
		$data['payment_provider'] = $this->purchase_model->provider_list($apps_id);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));
 	}

}




