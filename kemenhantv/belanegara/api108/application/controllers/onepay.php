<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Onepay extends CI_Controller{
	
	function Onepay(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('customer_model');
		$this->load->model('purchase_model');
		$this->load->helper('string');
	}	
	
	function index(){
		echo 'iBolz OnePay';	
		
	}


	
	function notify_payment() {
		
		$rawData = file_get_contents("php://input");
    ob_start();
    echo "<br><hr>iOnePay Notify Payment<br>";
    print_r($rawData);
    $c = ob_get_contents();
    ob_end_clean();
    $fp = fopen("application/logs/onepay_notify.txt", "a");
    fwrite($fp, $c);
    fclose($fp);		
		
		echo $c;
	}
	
	function  sent_payment(){
		$apps_id 			= $this->input->post("apps_id");
		$customer_id 	= $this->input->post("customer_id");
		$product_id 	= $this->input->post("product_id");
		$payment_code = $this->input->post("payment_code");
		$amount 			= 0;

		$active_days = 0;
		$order_code = uniqid(rand(), false);
		$rcode = '0';
		$message = "Unable to pay.";
		$xml_payment ='';
		

		$product = $this->customer_model->get_subscription_product_by_id($product_id);
		if ($product) {
			$amount = $product->amount;
			$active_days = $product->active_days;
		}		
		
		
		
		$user=array();
		if ($payment_code =='VISA') {
			
			$user['email']				= $this->input->post('Email');
			$user['phone'] 				= $this->input->post('Phone');
			$user['card_number'] 	= $this->input->post('CardNumber');
			$user['expiry_month'] = $this->input->post('ExpiryMonth');
			$user['expiry_year'] 	= $this->input->post('ExpiryYear');
			$user['cvv2'] 				= $this->input->post('CVV2');
			$user['card_holder'] 	= $this->input->post('CardHolderName');
			$user['test_url'] 		= '';

			
			$xml_payment = 
'<Parameter name="Email">'.$user['email'].'</Parameter>
<Parameter name="Phone">'.$user['phone'] .'</Parameter>
<Parameter name="CardNumber">'. $user['card_number'] .'</Parameter>
<Parameter name="ExpiryMonth">'. $user['expiry_month'] .'</Parameter>
<Parameter name="ExpiryYear">'. $user['expiry_year'] .'</Parameter>
<Parameter name="CVV2">'.$user['cvv2'] .'</Parameter>
<Parameter name="CardHolderName">'. $user['card_holder']  .'</Parameter>
<Parameter name="test url">'.$user['test_url'] .'</Parameter>';
		}		
		
		$merchant = $this->get_merchant();
		
		$xml_request = 
'<?xml version="1.0" encoding="UTF-8"?>
<OrderRequest>
	<Currency>NGN</Currency>
	 <MerchantKey>'.$merchant['MERCHANTKEY'].'</MerchantKey>
	 <MerchantCode>'.$merchant['MERCHANTCODE'].'</MerchantCode>
	 <MerchantName>'.$merchant['MERCHANTNAME'].'</MerchantName>
	 <ServiceCode>'.$merchant['SERVICECODE'].'</ServiceCode>
	 <Amount>'.$amount.'</Amount>
	 <UserDefinedField>'. $order_code .'</UserDefinedField>
	 <Parameters>
'.$xml_payment.'
	 </Parameters>
</OrderRequest>';
		$xml = $this->get_response($xml_request);

		$data = $this->to_array($xml);
		if (!empty ($data)) {
			if (isset($data['OrderRequestResult']['Status']['value'])) {
				if ($data['OrderRequestResult']['Status']['value'] == '50') {
					$rcode = '1';
					$message = "Success.";
					$this->customer_model->update_subscription_payment($customer_id, $apps_id, $product_id,  '1');
				}	else {
					if (isset($data['OrderRequestResult']['Message']['value'])) $message = $data['OrderRequestResult']['Message']['value'];
				}			
			}
		}	
		$data = array(
			"rcode"=>$rcode,
			"message"=>$message
		);
		
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));		
	}

	function  purchase(){
		$apps_id 			= $this->input->post("apps_id");
		$customer_id 	= $this->input->post("customer_id");
		$product_id 	= $this->input->post("product_id");
		$amount 			= $this->input->post("amount");		
		$payment_code = $this->input->post("payment_code");	
		$active_days = 0;
		$order_code = uniqid(rand(), false);
		$rcode = '0';
		$message = "Unable to pay.";
		$xml_payment ='';
		

		
		
		$user=array();
		if ($payment_code =='VISA') {
			
			$user['email']				= $this->input->post('Email');
			$user['phone'] 				= $this->input->post('Phone');
			$user['card_number'] 	= $this->input->post('CardNumber');
			$user['expiry_month'] = $this->input->post('ExpiryMonth');
			$user['expiry_year'] 	= $this->input->post('ExpiryYear');
			$user['cvv2'] 				= $this->input->post('CVV2');
			$user['card_holder'] 	= $this->input->post('CardHolderName');
			$user['test_url'] 		= '';
			
			
			$xml_payment = 
'<Parameter name="Email">'.$user['email'].'</Parameter>
<Parameter name="Phone">'.$user['phone'] .'</Parameter>
<Parameter name="CardNumber">'. $user['card_number'] .'</Parameter>
<Parameter name="ExpiryMonth">'. $user['expiry_month'] .'</Parameter>
<Parameter name="ExpiryYear">'. $user['expiry_year'] .'</Parameter>
<Parameter name="CVV2">'.$user['cvv2'] .'</Parameter>
<Parameter name="CardHolderName">'. $user['card_holder']  .'</Parameter>
<Parameter name="test url">'.$user['test_url'] .'</Parameter>';
		}		
		
		$merchant = $this->get_merchant();
		
		$xml_request = 
'<?xml version="1.0" encoding="UTF-8"?>
<OrderRequest>
	<Currency>NGN</Currency>
	 <MerchantKey>'.$merchant['MERCHANTKEY'].'</MerchantKey>
	 <MerchantCode>'.$merchant['MERCHANTCODE'].'</MerchantCode>
	 <MerchantName>'.$merchant['MERCHANTNAME'].'</MerchantName>
	 <ServiceCode>'.$merchant['SERVICECODE'].'</ServiceCode>
	 <Amount>'.$amount.'</Amount>
	 <UserDefinedField>'. $order_code .'</UserDefinedField>
	 <Parameters>
'.$xml_payment.'
	 </Parameters>
</OrderRequest>';

		$xml = $this->get_response($xml_request);


		$data = $this->to_array($xml);
		if (!empty ($data)) {
			if (isset($data['OrderRequestResult']['Status']['value'])) {
				if ($data['OrderRequestResult']['Status']['value'] == '50') {
					$rcode = '1';
					$message = "Success.";
					$this->purchase_model->update_customer_product($customer_id, $apps_id, $product_id, $amount);
				}	else {
					if (isset($data['OrderRequestResult']['Message']['value'])) $message = $data['OrderRequestResult']['Message']['value'];
				}			
			}
		}	
		$data = array(
			"rcode"=>$rcode,
			"message"=>$message
		);
		

		$subscriber = $this->customer_model->get_subscription_by_customer($customer_id, $apps_id);

		if (!empty($subscriber)) {
			$data['product'] = array(
				'product_id' => $subscriber->product_id,
				'product_name' => $subscriber->product_name,
				'product_price' => $subscriber->product_price,
				'active_days' => $subscriber->active_days
			);
			
			$status = $subscriber->subscription_status;
			$expired = $subscriber->expiry_time;
			if (!empty($expired)) {
				$timestamp = time();
				$expired = strtotime($expired);
				$diff = $timestamp - $expired;
				$expired_days = round($diff / 86400);

				if ($timestamp > $expired) {
						$data['rcode'] = '3';
				} else {
					$data['rcode'] = $status;
				}								
			} else {
				$data['rcode'] = $status;
			}
			
			if ($data['rcode'] == '1') {
				$data['message'] = 'Success';
				$data['expired'] = abs($expired_days) . ' days';

			} elseif ($data['rcode'] == '3') {
				$data['message'] = 'Expired';
				$data['expired'] = $expired_days . ' days';
			}
			if ($status == '0') $data['message'] = 'Inactive';
		}

		$customer_products = $this->customer_model->customer_products($customer_id, $apps_id);
		if (!empty($customer_products)) {	
			$data['params']['customer_products'] = $customer_products;
		}
		$data['current_product_id'] = $product_id;		


		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));		
	}


	function  cancel(){
		$apps_id 			= $this->input->post("apps_id");
		$customer_id 	= $this->input->post("customer_id");
		$product_id 	= $this->input->post("product_id");
		
		if (empty($apps_id))  $apps_id = '00';
		if (empty($customer_id))  $customer_id = '00';
		if (empty($product_id))  $product_id = '23401220000002110';


		$rcode = '1';
		$message = "Success.";
		$this->customer_model->update_subscription_payment($customer_id, $apps_id, $product_id,  '0');
		$data = array(
			"rcode"=>$rcode,
			"message"=>$message
		);
		
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));		
	}

	
	private function get_merchant() {
		$merchant['MERCHANTKEY']	= '92a8c839-d732-4b0c-b9c3-4211ef71d507';
		$merchant['MERCHANTCODE']	= 'MER-NG-35448';
		$merchant['MERCHANTNAME']	= 'ibolz-tv';
		$merchant['SERVICECODE']	= 'MER_svc_104';
		
		return $merchant;
	}
	
	
	private function get_response($xml) {
		if (!function_exists('curl_init')) {
			die('Onepay needs the CURL PHP extension.');
		}

		if (empty($xml))		{
			die('empty xml');
		}			
	
		 		
		$url = 'http://95.138.183.217:8080/iOnePayNGWeb/payapi/v1/order/';
		$response = $this->curl($url,$xml);
		return $response;			
	}

	function curl($url, $xml) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/xml; charset=UTF-8'));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		
		$output = curl_exec($ch);

		if(curl_errno($ch))
		  //print curl_error($ch);
		  $output = 'No connection to Payment Provider';

		else
		  curl_close($ch);

		//print_r($output);  
		return $output;		
	}
	
	

	function to_array($contents, $getAttributes=1, $priority = 'attribute') {
	    if(!$contents) return array();

	    if(!function_exists('xml_parser_create')) {
	        //print "'xml_parser_create()' function not found!";
	        return array();
	    }

	    //Get the XML parser of PHP - PHP must have this module for the parser to work
	    $parser = xml_parser_create('');
	    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); 
	    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
	    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
	    xml_parse_into_struct($parser, trim($contents), $xml_values);
	    xml_parser_free($parser);

	    if(!$xml_values) return;//Hmm...

	    //Initializations
	    $xml_array = array();
	    $parents = array();
	    $opened_tags = array();
	    $arr = array();

	    $current = &$xml_array; //Refference

	    //Go through the tags.
	    $repeated_tag_index = array();//Multiple tags with same name will be turned into an array
	    foreach($xml_values as $data) {
	        unset($attributes,$value);//Remove existing values, or there will be trouble

	        //This command will extract these variables into the foreach scope
	        // tag(string), type(string), level(int), attributes(array).
	        extract($data);//We could use the array by itself, but this cooler.

	        $result = array();
	        $attributes_data = array();
	        
	        if(isset($value)) {
	            if($priority == 'tag') $result = $value;
	            else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
	        }

	        //Set the attributes too.
	        if(isset($attributes) and $getAttributes) {
	            foreach($attributes as $attr => $val) {
	                if($priority == 'tag') $attributes_data[$attr] = $val;
	                else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
	            }
	        }

	        //See tag status and do the needed.
	        if($type == "open") {//The starting of the tag '<tag>'
	            $parent[$level-1] = &$current;
	            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
	                $current[$tag] = $result;
	                if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
	                $repeated_tag_index[$tag.'_'.$level] = 1;

	                $current = &$current[$tag];

	            } else { //There was another element with the same tag name

	                if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
	                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
	                    $repeated_tag_index[$tag.'_'.$level]++;
	                } else {//This section will make the value an array if multiple tags with the same name appear together
	                    $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
	                    $repeated_tag_index[$tag.'_'.$level] = 2;
	                    
	                    if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
	                        $current[$tag]['0_attr'] = $current[$tag.'_attr'];
	                        unset($current[$tag.'_attr']);
	                    }

	                }
	                $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
	                $current = &$current[$tag][$last_item_index];
	            }

	        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
	            //See if the key is already taken.
	            if(!isset($current[$tag])) { //New Key
	                $current[$tag] = $result;
	                $repeated_tag_index[$tag.'_'.$level] = 1;
	                if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

	            } else { //If taken, put all things inside a list(array)
	                if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

	                    // ...push the new element into that array.
	                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
	                    
	                    if($priority == 'tag' and $getAttributes and $attributes_data) {
	                        $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
	                    }
	                    $repeated_tag_index[$tag.'_'.$level]++;

	                } else { //If it is not an array...
	                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
	                    $repeated_tag_index[$tag.'_'.$level] = 1;
	                    if($priority == 'tag' and $getAttributes) {
	                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
	                            
	                            $current[$tag]['0_attr'] = $current[$tag.'_attr'];
	                            unset($current[$tag.'_attr']);
	                        }
	                        
	                        if($attributes_data) {
	                            $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
	                        }
	                    }
	                    $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
	                }
	            }

	        } elseif($type == 'close') { //End of tag '</tag>'
	            $current = &$parent[$level-1];
	        }
	    }
	    
	    return($xml_array);
	} 
	
	function generate_id() {
		$id = uniqid(rand(), false);			
		echo $id;
	}
		
	function generate_json() {
		$data =  array();
		// CC VISA
		/*
		$data[] = array('field_name' => 'Email', 'label' => 'Email', 'type' => 'string');
		$data[] = array('field_name' => 'Phone', 'label' => 'Phone', 'type' => 'number');
		$data[] = array('field_name' => 'CardNumber', 'label' => 'Card Number', 'type' => 'number');
		$data[] = array('field_name' => 'ExpiryMonth', 'label' => 'Expire Month', 'type' => 'number');
		$data[] = array('field_name' => 'ExpiryYear', 'label' => 'Expire Year', 'type' => 'number');
		$data[] = array('field_name' => 'CVV2', 'label' => 'CardNumber', 'CVV' => 'number');
		$data[] = array('field_name' => 'CardHolderName', 'label' => 'Card Holder Name', 'type' => 'string');
		*/
		
		// InterSwitch 
		
		$data[] = array('field_name' => 'CardNumber', 'label' => 'Card Number', 'type' => 'number');
		$data[] = array('field_name' => 'Pin', 'label' => 'Pin', 'type' => 'string');
		$data[] = array('field_name' => 'ExpiryYear', 'label' => 'ExpiryDate', 'type' => 'string');
		$data[] = array('field_name' => 'CVV', 'label' => 'CVV', 'CVV' => 'number');
		
	echo json_encode($data );
	}		
	
	function test() {
		$date = date('d-m-Y h:i:s a', time());
		echo $date;
	}

	function form_subscription_product() {
	 echo '
				<form action="http://172.16.106.65/api/customer/subscription_product" method="post" name="loginForma" id="loginForm" class="form-horizontal">
    				<div class="control-group">
        				<input type="text" placeholder="apps_id" name="apps_id" value="com.balepoint.ibolz.mtn.ng">
    				</div>
    				<div class="control-group">
    					<button type="submit" class="btn btn-primary">submit</button>
    				</div>
    			</form>';    				 
	}

	function form_subscription_subscribe() {
	 echo '
				<form action="http://172.16.106.65/api/customer/subscribe" method="post" name="loginForma" id="loginForm" class="form-horizontal">
    				<div class="control-group">
        				<input type="text" placeholder="apps_id" name="apps_id" value="com.balepoint.ibolz.mtn.ng">
    				</div>
    				<div class="control-group">
        				<input type="text" placeholder="product_id" name="product_id" value="23401220000002110">
    				</div>
    				<div class="control-group">
        				<input type="text" placeholder="msisdn" name="msisdn" value="6285959892211">
    				</div>

    				<div class="control-group">
    					<button type="submit" class="btn btn-primary">submit</button>
    				</div>
    			</form>';    				 
	}

}

