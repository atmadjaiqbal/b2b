﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer extends CI_Controller {

    function Mailer() {
        parent::__construct();
        $this->load->helper('url');

        $this->load->helper('string');
        $this->mailer_email = "noreply@ibolztv.net";

        $this->email_config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.ibolztv.net',
            'smtp_port' => 465,
            'smtp_user' => $this->mailer_email,
            'smtp_pass' => 'Jagoan-123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
    }

    function index() {
        echo 'iBolz Mailer';
    }

    public function send_verification($email_data) {
        $apps_id = $email_data["apps_id"];
        $apps_name = $email_data["apps_name"];
        $name = $email_data["name"];
        $email = $email_data["email"];
        $url = $email_data["url"];
        if (empty($url))
            $url = 'http://ibolz.tv/apps/customer/verify_email';

        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent email verification'
        );

        $subject = "Welcome to $apps_name, email address verification";
        $sender = $this->mailer_email;
        //$apps_name = "iBolz";



        if ($apps_id == "com.balepoint.ibolz.diamond") {
            $apps_name = "Diamond Bank TV";
            $subject = "Welcome to $apps_name";
            $sender = "diamondbankTV@diamondbank.com";
        }
        $message = "
				Dear $name,<br>
				Welcome to $apps_name.<br> 
				To complete your registration, click on the verification link below <br>
				<a href=\"$url\"><strong>link</strong></a><br /><br />
				Should in case you encounter any difficulties using this link; you can also copy and paste the link into your browser address bar <br>
				$url<br /><br /><br />
				Regards,<br>
				$apps_name Team";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            $response = array(
                'rcode' => '1',
                'message' => 'Email verification sent'
            );
        } else {
            $response['message'] = $this->email->print_debugger();
        }

        //$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode($response));

        return json_encode($response);
    }

    public function send_forgot_password($email_data) {
        $apps_id = $email_data["apps_id"];
        $apps_name = $email_data["apps_name"];
        $name = $email_data["name"];
        $email = $email_data["email"];
        $url = $email_data["url"];

        if (empty($url))
            $url = 'http://ibolz.tv/apps/customer/forgot_password';;

        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent forgot password'
        );

        $subject = "Reset Your $apps_name Password";
        $sender = $this->mailer_email;


        if ($apps_id == "com.balepoint.ibolz.diamond") {
            $apps_name = "Diamond Bank TV";
            $subject = "Reset Your $apps_name Password";
            $sender = "diamondbankTV@diamondbank.com";
        }


        $message = "
				Dear $name,<br><br>

				Click this link to reset your password. <br>
				<a href=\"$url\"><strong>link</strong></a><br /><br />
				Should in case you encounter any difficulties using this link; you can also copy and paste the link into your browser address bar <br>
				$url<br /><br /><br />
				Regards,<br>
				$apps_name Team";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            $response = array(
                'rcode' => '1',
                'message' => 'Email forgot password sent'
            );
        } else {
            $response['message'] = $this->email->print_debugger();
        }

        //$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode($response));

        return json_encode($response);
    }

    public function send_verification_code($email_data) {
        $apps_id = $email_data["apps_id"];
        $apps_name = $email_data["apps_name"];
        $name = $email_data["name"];
        $email = $email_data["email"];
        $url = $email_data["url"];
        if (empty($url))
            $url = 'http://ibolz.tv/apps/customer/verify_email';;

        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent email verification'
        );

        $subject = "Email address verification code.";
        $sender = $this->mailer_email;
        //$apps_name = "iBolz";



        if ($apps_id == "com.balepoint.ibolz.diamond") {
            $apps_name = "Diamond Bank TV";
            $sender = "diamondbankTV@diamondbank.com";
        }
        $message = "
				Dear $name,<br>

				To complete your registration, click on the verification link below <br>
				<a href=\"$url\"><strong>link</strong></a><br /><br />
				Should in case you encounter any difficulties using this link; you can also copy and paste the link into your browser address bar <br>
				$url<br /><br /><br />
				Regards,<br>
				$apps_name Team";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            $response = array(
                'rcode' => '1',
                'message' => 'Email verification sent'
            );
        } else {
            $response['message'] = $this->email->print_debugger();
        }

        //$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode($response));

        return json_encode($response);
    }

    function test_send_verify() {
        $email_data = array(
            'apps_id' => 'com.balepoint.ibolz.diamond',
            'name' => 'Zola',
            'email' => 'zola.octanoviar@gmail.com',
            'url' => 'detik.com'
        );
        $this->send_verification($email_data);
    }

    function test_send_forgot() {
        $email_data = array(
            'apps_id' => 'com.balepoint.ibolz.diamond',
            'name' => 'Zola',
            'email' => 'zola.octanoviar@gmail.com',
            'url' => 'detik.com'
        );
        $this->send_forgot($email_data);
    }

    function send_verification_rest() {
        $name = $this->input->post("name");
        if (empty($name))
            $name = 'zola';

        $email = $this->input->post("email");
        if (empty($email))
            $email = 'zola.octanoviar@gmail.com';;

        $url = $this->input->post("url");
        if (empty($url))
            $url = 'http://ibolz.tv/apps/customer/verify_email';;

        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent email verification'
        );

        $message = "Hi $name,<br /><br />
  						Please click on this link to verify your email address and complete your registration.<br />
  						<a href=\"$url\"><strong>Verify $email</strong></a><br /><br />
  						If you can't click on the link above, you can verify your email address by cutting and pasting (or typing) the following address into your browser: <br /><br />
  						$url<br /><br /><br />
  						(c) Best Regards,<br /> iBolz Admin";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mailer_email);
        $this->email->to($email);
        $this->email->subject('iBolz email verify');
        $this->email->message($message);

        if ($this->email->send()) {
            //echo 'Email sent.';
            $response = array(
                'rcode' => '1',
                'message' => 'Email verification sent'
            );
        } else {
            $response['message'] = $this->email->print_debugger();
        }

        //$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode($response));

        return json_encode($response);
    }

    function send_forgot_password_rest() {

        $email = $this->input->post("email");
        if (empty($email))
            $email = 'zola.octanoviar@gmail.com';;

        $url = $this->input->post("url");
        if (empty($url))
            $url = 'http://ibolz.tv/apps/customer/forgot_password';;


        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent forgot password'
        );

        $message = "Hi,<br /><br />
  						Please click on this link to reset your password.<br />
  						<a href=\"$url\"><strong>Reset Password</strong></a><br /><br />
  						If you can't click on the link above, you can reset your passwordby cutting and pasting (or typing) the following address into your browser: <br /><br />
  						$url<br /><br /><br />
  						(c) Best Regards,<br /> iBolz Admin";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mailer_email);
        $this->email->to($email);
        $this->email->subject('iBolz forget password');
        $this->email->message($message);

        if ($this->email->send()) {
            //echo 'Email sent.';
            $response = array(
                'rcode' => '1',
                'message' => 'Failed sent forgot password'
            );
        } else {
            $response['message_debug'] = $this->email->print_debugger();
        }

        return json_encode($response);
    }
    
    
    function send_referral_register($email_data) {
        
        $email = $email_data["email"];
        $customer_name = $email_data["customer_name"];
        $reference_code = $email_data["reference_code"];
        
        if (empty($email))
            $email = 'amrizal@ibolz.tv';
        
        if (empty($reference_code)) {
            $reference_code = "TEST REFERENCE CODE";
        }
        
        $apps_name = $this->input->post("apps_name");
        if (empty($apps_name)) {
            $apps_name = "iBolz";
        }
            
        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent forgot password'
        );

        $message = "Hi $customer_name<br /><br />
  						You have successfully registered as iBolz Army on <b>$apps_name</b>.<br />
                                                Now you can use your reference number below to redeem your point<br>
  						<b><strong>$reference_code</strong></b><br /><br />
  						<br />
  						(c) Best Regards,<br /> iBolz Admin";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mailer_email);
        $this->email->to($email);
        $this->email->subject('iBolz Army');
        $this->email->message($message);

        if ($this->email->send()) {
            //echo 'Email sent.';
            $response = array(
                'rcode' => '1',
                'message' => 'Email sent'
            );
        } else {
            $response['message_debug'] = $this->email->print_debugger();
        }
        
        return json_encode($response);
    }
    
    
    function send_referral_submition($email_data) {
        
        date_default_timezone_set("Asia/Jakarta");

        $email = $email_data["email"];
        $customer_name = $email_data["customer_name"];
        $reference_code = $email_data["reference_code"];
        $total_download = $email_data["total_download"];
        $total_point = $email_data["total_point"];
        
        $now = date('F jS, Y H:i:s');
        
        
        if (empty($email))
            $email = 'amrizal@ibolz.tv';
        
        if (empty($reference_code)) {
            $reference_code = "TEST REFERENCE CODE";
        }
        
        $apps_name = $this->input->post("apps_name");
        if (empty($apps_name)) {
            $apps_name = "iBolz";
        }
        
        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent forgot password'
        );

        $message = "Hi $customer_name<br /><br />
  						You have 1 new customer used your reference as iBolz Army on $now.<br />
                                                Total Download : <b>$total_download</b><br>
                                                Total Point    : <b>$total_point</b><br>
                                                <br>
  						Your reference number <b><strong>$reference_code</strong></b><br /><br />
  						<br />
  						(c) Best Regards,<br /> iBolz Admin";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mailer_email);
        $this->email->to($email);
        $this->email->subject('iBolz Army');
        $this->email->message($message);

        if ($this->email->send()) {
            //echo 'Email sent.';
            $response = array(
                'rcode' => '1',
                'message' => 'Email sent'
            );
        } else {
            $response['message_debug'] = $this->email->print_debugger();
        }

        return json_encode($response);
    }
    
    
    /*
     * =========== Async email executed by httpservice ===========
     * 
     */
    
    public function send_verification_async() {
        $apps_id = $this->input->post("apps_id");
        $apps_name = $this->input->post("apps_name");
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $url = $this->input->post("url");
        
        if (empty($url))
            $url = 'http://ibolz.tv/apps/customer/verify_email';

        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent email verification'
        );
        
        $subject = "Welcome to $apps_name, email address verification";
        $sender = $this->mailer_email;
        //$apps_name = "iBolz";



        if ($apps_id == "com.balepoint.ibolz.diamond") {
            $apps_name = "Diamond Bank TV";
            $subject = "Welcome to $apps_name";
            $sender = "diamondbankTV@diamondbank.com";
        }
        $message = "
				Dear $name,<br>
				Welcome to $apps_name.<br> 
				To complete your registration, click on the verification link below <br>
				<a href=\"$url\"><strong>link</strong></a><br /><br />
				Should in case you encounter any difficulties using this link; you can also copy and paste the link into your browser address bar <br>
				$url<br /><br /><br />
				Regards,<br>
				$apps_name Team";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            $response = array(
                'rcode' => '1',
                'message' => 'Email verification sent'
            );
        } else {
            $response['message'] = $this->email->print_debugger();
        }

        //$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode($response));

        return json_encode($response);
        
    }
    
    public function send_forgot_password_async() {
        $apps_id = $this->input->post("apps_id");
        $apps_name = $this->input->post("apps_name");
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $url = $this->input->post("url");

        if (empty($url))
            $url = 'http://ibolz.tv/apps/customer/forgot_password';;

        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent forgot password'
        );

        $subject = "Reset Your $apps_name Password";
        $sender = $this->mailer_email;


        if ($apps_id == "com.balepoint.ibolz.diamond") {
            $apps_name = "Diamond Bank TV";
            $subject = "Reset Your $apps_name Password";
            $sender = "diamondbankTV@diamondbank.com";
        }

        $message = "
				Dear $name,<br><br>

				Click this link to reset your password. <br>
				<a href=\"$url\"><strong>link</strong></a><br /><br />
				Should in case you encounter any difficulties using this link; you can also copy and paste the link into your browser address bar <br>
				$url<br /><br /><br />
				Regards,<br>
				$apps_name Team";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            $response = array(
                'rcode' => '1',
                'message' => 'Email forgot password sent'
            );
        } else {
            $response['message'] = $this->email->print_debugger();
        }

        //$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode($response));

        return json_encode($response);
    }
    
    public function send_verification_code_async() {
        $apps_id = $this->input->post("apps_id");
        $apps_name = $this->input->post("apps_name");
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $url = $this->input->post("url");
        
        if (empty($url))
            $url = 'http://ibolz.tv/apps/customer/verify_email';;

        $response = array(
            'rcode' => '0',
            'message' => 'Failed sent email verification'
        );

        $subject = "Email address verification code.";
        $sender = $this->mailer_email;
        //$apps_name = "iBolz";



        if ($apps_id == "com.balepoint.ibolz.diamond") {
            $apps_name = "Diamond Bank TV";
            $sender = "diamondbankTV@diamondbank.com";
        }
        $message = "
				Dear $name,<br>

				To complete your registration, click on the verification link below <br>
				<a href=\"$url\"><strong>link</strong></a><br /><br />
				Should in case you encounter any difficulties using this link; you can also copy and paste the link into your browser address bar <br>
				$url<br /><br /><br />
				Regards,<br>
				$apps_name Team";

        $this->load->library('email', $this->email_config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            $response = array(
                'rcode' => '1',
                'message' => 'Email verification sent'
            );
        } else {
            $response['message'] = $this->email->print_debugger();
        }

        //$this->output->set_content_type('application/json');
        //$this->output->set_output(json_encode($response));

        return json_encode($response);
    }

}
