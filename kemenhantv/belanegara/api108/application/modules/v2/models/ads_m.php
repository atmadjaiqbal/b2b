<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Zola Octanoviar
	Date: nov, 17 2014 22:20 
*/
class Ads_m extends MY_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function ads_list($apps_id, $channel_id){
			$sql = "
				SELECT
					ads_schedule_id, apps_id, channel_id, publish_date, a.duration, 
					now() as server_time, a.ads_id, b.ads_title, b.thumbnail, b.message
				FROM ads_schedule a, ads b
				WHERE a.ads_id = b.ads_id 
				AND apps_id = '$apps_id' 
				AND channel_id = '$channel_id'
				ORDER BY publish_date
			";
					
			$query = $this->db->query($sql);	
			$rows =  $query->result();
			return $rows;
	}
	

	


}
