<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Schedule_model extends MY_Model {
	

	public function schedule_today($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id, $channel_type_id) {	
		
		if ($channel_type_id == '1') {
			$textquery = "
				SELECT 
					a.schedule_id, a.channel_id, a.program_id, a.duration, a.program_name,
					a.start_date_time,
					CONCAT(SUBSTRING(DATE_FORMAT(a.start_date_time, '%W'), 1, 3), ' ', DATE_FORMAT(a.start_date_time, '%h:%i %p')) as start_time_text,
					DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text, table_ref,
					schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 
					CASE 
						WHEN now() <  start_date_time THEN 1
						ELSE 0
					END as enable_record,
				    0 as enable_remove, 
					0 as enable_play,
					CASE 
						WHEN now() between start_date_time and DATE_ADD(start_date_time, INTERVAL a.duration MINUTE)  THEN 1
						ELSE 0
					END as isnow
				FROM vschedule a
				WHERE a.channel_id = '$channel_id'
				AND DATE_FORMAT(a.start_date_time, '%Y-%m-%d') = DATE_FORMAT(now(),  '%Y-%m-%d')
				AND	schedule_type_id = $schedule_type_id
				ORDER BY start_date_time
			 ";
		} elseif  ($channel_type_id == '2')  {
			/*
			$textquery = "
				SELECT 
					a.epg_id schedule_id, a.channel_id, a.content_id program_id, a.duration, a.title  program_name,
					a.start_date_time,
					CONCAT(SUBSTRING(DATE_FORMAT(a.start_date_time, '%W'), 1, 3), ' ', DATE_FORMAT(a.start_date_time, '%h:%i %p')) as start_time_text,
					DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text, 'epg' table_ref,
					'1' schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 
					CASE 
						WHEN now() <  start_date_time THEN 1
						ELSE 0
					END as enable_record,
				    0 as enable_remove, 
					0 as enable_play,
					CASE 
						WHEN now() between start_date_time and DATE_ADD(start_date_time, INTERVAL a.duration MINUTE)  THEN 1
						ELSE 0
					END as isnow
				FROM epg  a
				WHERE a.channel_id = '$channel_id'
				AND DATE_FORMAT(a.start_date_time, '%Y-%m-%d') = DATE_FORMAT(now(),  '%Y-%m-%d')
				ORDER BY start_date_time
			";	*/
			
			$textquery = "
			SELECT
			a.epg_id schedule_id, a.channel_id, a.content_id program_id, a.duration, a.title  program_name,
			a.start_date_time,
			CONCAT(SUBSTRING(DATE_FORMAT(a.start_date_time, '%W'), 1, 3), ' ', DATE_FORMAT(a.start_date_time, '%h:%i %p')) as start_time_text,
			DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text, 'epg' table_ref,
			'1' schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time,
			0 as enable_record,
			0 as enable_remove,
			0 as enable_play,
			CASE
			WHEN now() between start_date_time and DATE_ADD(start_date_time, INTERVAL a.duration MINUTE)  THEN 1
			ELSE 0
			END as isnow
			FROM epg  a
			WHERE a.channel_id = '$channel_id'
			AND DATE_FORMAT(a.start_date_time, '%Y-%m-%d') = DATE_FORMAT(now(),  '%Y-%m-%d')
			ORDER BY start_date_time
			";
		}
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		/*
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		*/
		return $rows;
	}
	



	public function schedule_list($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id) {		
		$textquery = "
			SELECT 
				a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
				b.thumbnail, a.start_date_time,
				DATE_FORMAT(a.start_date_time, '%W %h:%i %p') as start_time_text, DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
			FROM schedule a, program b
			WHERE a.program_id = b.program_id 
			AND a.channel_id = '$channel_id'
			AND ( a.end_date_time between '$start_date' and '$end_date' )
			AND	schedule_type_id = $schedule_type_id
		 	";


		if(!empty($customer_id) && $schedule_type_id == '3'){
			$textquery = $textquery . "
				UNION ALL
				SELECT 
					a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
					b.thumbnail, c.start_date_time,
					DATE_FORMAT(c.start_date_time, '%W %h:%i %p') as start_time_text, DATE_FORMAT(c.end_date_time, '%h:%i %p') as end_time_text,
					schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 0 as enable_record, 1 as enable_remove, 1 enable_play
				FROM schedule a, program b, schedule_record c
				WHERE a.schedule_id = c.schedule_id 
				AND a.channel_id = c.channel_id
				AND a.program_id = b.program_id
				AND c.customer_id = '$customer_id'
				";
		}
		$textquery = $textquery . " ORDER BY start_date_time ";


		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		
		return $rows;
	}
	
	
	public function schedule_now($channel_id, $schedule_type_id) {		
		$textquery = "
			SELECT 
				a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
				b.thumbnail, a.start_date_time,
				DATE_FORMAT(a.start_date_time, '%h:%i %p') as start_time_text, DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
			FROM schedule a, program b
			WHERE a.program_id = b.program_id 
			AND a.channel_id = '$channel_id'
			AND a.schedule_type_id = $schedule_type_id
			AND now() between start_date_time and end_date_time
			ORDER BY a.created
			LIMIT 0, 1
		 	";

		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	
	public function schedule_next($channel_id, $schedule_type_id) {		
		$textquery = "
			SELECT 
				a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
				b.thumbnail, a.start_date_time,
				DATE_FORMAT(a.start_date_time, '%h:%i %p') as start_time_text, DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
			FROM schedule a, program b
			WHERE a.program_id = b.program_id 
			AND a.channel_id = '$channel_id'
			AND a.schedule_type_id = $schedule_type_id
			AND start_date_time > now() 
			ORDER BY a.created
			LIMIT 0, 1
		 	";

		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	
	public function record($apps_id, $channel_id, $schedule_id, $customer_id){
		$textquery = "
			INSERT INTO schedule_record (apps_id, channel_id, schedule_id, customer_id, start_date_time, end_date_time, status, created) 
			VALUES ('$apps_id', '$channel_id', '$schedule_id', '$customer_id', now(), now(), 0, now())
			";
		$query = $this->db->query($textquery);	
	}
	
	
	
	
	public function tvod_list($apps_id, $channel_id, $channel_type_id) {	
		if ($channel_type_id == '1') {		
			$textquery = "
				select start_date as tvod_date from vschedule
				where start_date < now()
				and channel_id='$channel_id'
				group by start_date
				";
		} elseif  ($channel_type_id == '2')  {
			$textquery = "
				select start_date as tvod_date from epg
				where start_date < now()
				and channel_id='$channel_id' 
				and start_date  != '0000-00-00'
				and start_date >= DATE(NOW()) - INTERVAL 7 DAY
				
				group by start_date
				";
		}
		$query = $this->db->query($textquery);	
		$rows = $query->result();
		foreach($rows as $row){
	    	$row->schedule_list = $this->tvod_list_by_date($channel_id, $row->tvod_date, $channel_type_id);
		}
		return $rows;
		
		/*	
		$textquery = "
			SELECT 
				a.channel_id, channel_name, alias, thumbnail, channel_type_id, status, channel_descr
		 	FROM apps_channel a, channel b WHERE a.channel_id = '$channel_id'
			AND apps_id = '$apps_id'
			AND a.channel_id = b.channel_id
			";
		$query = $this->db->query($textquery);	
		$row = $query->row();
		if($row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
			
			
	        $start_date = date("Y-m-d", strtotime ( '-7 day', strtotime (date("Y-m-d")))) ;
	        $end_date = date("Y-m-d") ;

	    	$row->schedule_list = $this->channel_schedule($channel_id, 1, $start_date, $end_date);
			
		}
		
		return $row;
		*/
	}
	
	public function tvod_list_by_date($channel_id, $start_date, $channel_type_id) {		
		if ($channel_type_id == '1') {		
		$textquery = "
			select 
				a.schedule_id, a.channel_id, a.program_id, a.duration, a.program_name,
				a.start_date_time,
				DATE_FORMAT(a.start_date_time, '%h:%i %p') as start_time_text,
				DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text, table_ref,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 0 as enable_record, 0 as enable_remove, 1 enable_play
			from vschedule a
			where  channel_id='$channel_id'
			and start_date='$start_date'
			and start_date_time < now()
			order by start_time
		 	";
		} elseif  ($channel_type_id = '2')  {
		$textquery = "
			select 
				a.epg_id schedule_id, a.channel_id, a.content_id program_id, a.duration, a.title program_name,
				a.start_date_time,
				DATE_FORMAT(a.start_date_time, '%h:%i %p') as start_time_text,
				DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text, 'epg' table_ref,
				'1' schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 0 as enable_record, 0 as enable_remove, 1 enable_play
			from epg a
			where  channel_id='$channel_id'
			and start_date='$start_date'
			and start_date_time < now()
			order by start_time
		 	";

		}
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;
	}
	
	public function channel_schedule($channel_id, $schedule_type_id, $start_date, $end_date) {		
		$textquery = "
			SELECT 
				distinct DATE_FORMAT(start_date_time, '%Y-%m-%d') as sdate, DATE_FORMAT(start_date_time, '%d %M %Y') as schedule_date 
			FROM schedule a
			WHERE a.channel_id = '$channel_id'
			AND (DATE_FORMAT(start_date_time, '%Y-%m-%d') between '$start_date' and '$end_date')
			AND	schedule_type_id = $schedule_type_id
		 	";

		$textquery = $textquery . " ORDER BY start_date_time DESC";

		$query = $this->db->query($textquery);	
		$schedule_list =  $query->result();
		foreach($schedule_list as $schedule){
			$textquery = "
				SELECT 
					a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
					b.thumbnail, a.start_date_time,
					CONCAT(SUBSTRING(DATE_FORMAT(a.start_date_time, '%W'), 1, 3), ' ', DATE_FORMAT(a.start_date_time, '%h:%i %p')) as start_time_text,
					DATE_FORMAT(a.end_date_time, '%h:%i %p') as end_time_text,
					schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
				FROM schedule a, program b
				WHERE a.program_id = b.program_id 
				AND a.channel_id = '$channel_id'
				AND DATE_FORMAT(start_date_time, '%Y-%m-%d') = '$schedule->sdate'
				AND	schedule_type_id = $schedule_type_id
			 	";

			$textquery = $textquery . " ORDER BY start_date_time ";

			$query = $this->db->query($textquery);	
			$rows =  $query->result();
			$schedule->schedule_list = $rows;

		}
		return $schedule_list;
	}
	
	/*
	public function content_list($program_id) {	
		$textquery = "
			select 
				a.content_id, title, video_length as duration, status, video_codec, video_thumbnail,
				filename
			from program_content a, content b
			where a.content_id = b.content_id
			and a.program_id = '$program_id'
			order by a.order_number
		 	";
		$query = $this->db->query($textquery);	
		return $query->result();
	}
	*/
	
	public function myschedule_list($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id) {	

		$textquery = "
			SELECT 
				a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
				b.thumbnail, c.start_date_time,
				DATE_FORMAT(DATE_ADD(c.created,INTERVAL 1 DAY), '%b %d') as start_time_text,
				'+7 days' as end_time_text,
				schedule_type_id, DATE_FORMAT(now(), '%W %h:%i %p') as server_time, 0 as enable_record, 0 as enable_remove, 
				CASE 
					WHEN now() >  DATE_ADD(c.created,INTERVAL 1 DAY) THEN 1
					ELSE 0
				END as enable_play
			FROM schedule a, program b, schedule_record c
			WHERE a.schedule_id = c.schedule_id 
			AND a.channel_id = c.channel_id
			AND a.program_id = b.program_id
			AND c.customer_id = '$customer_id'
			AND (now() between c.created AND DATE_ADD(c.created,INTERVAL 7 DAY))
			";

		$textquery = $textquery . " ORDER BY start_date_time ";


		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		/*
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		*/
		return $rows;
	}
	
	
public function schedule_detail($schedule_id, $channel_type_id) {
		
		$textquery = "";
		if ($channel_type_id == '1') {
			/*
			 $textquery = "
			 SELECT s.schedule_id, s.start_date_time, s.end_date_time, s.duration,
			 ch.channel_id, ch.channel_type_id, ch.channel_name, ch.alias,
			 p.program_id, p.program_name, p.program_descr,
			 ct.content_id, ct.title, ct.description
			 FROM SCHEDULE s
			 JOIN channel ch ON ch.channel_id = s.channel_id
			 JOIN program p ON s.program_id=p.program_id
			 JOIN program_content pc ON pc.program_id = p.program_id
			 JOIN content ct ON ct.content_id = pc.content_id
			 WHERE s.schedule_id = '$schedule_id'
			 ORDER BY pc.order_number
			 ";
			 */
			$textquery = "
			SELECT s.schedule_id, s.start_date_time, s.end_date_time, s.duration,
			CONCAT(COALESCE(DATE_FORMAT(s.start_date_time, '%a, %d %b %Y'), ''), ' | ', COALESCE(DATE_FORMAT(s.start_date_time, '%H:%i'), ''), ' - ', COALESCE(DATE_FORMAT(s.end_date_time, '%H:%i'), ''), ' | ', p.program_name) AS schedule_title,
			ch.channel_id, ch.channel_type_id, ch.channel_name, ch.alias,
			p.program_id, p.program_name, p.program_descr as description
			FROM schedule s
			JOIN channel ch ON ch.channel_id = s.channel_id
			JOIN program p ON s.program_id=p.program_id
			WHERE s.schedule_id = '$schedule_id'
			";
		} else if  ($channel_type_id == '2')  {
			$textquery = "
			SELECT s.epg_id AS schedule_id, s.start_date_time, s.end_date_time, s.duration,
			CONCAT(COALESCE(DATE_FORMAT(s.start_date_time, '%a, %d %b %Y'), ''), ' | ', COALESCE(DATE_FORMAT(s.start_date_time, '%H:%i'), ''), ' - ', COALESCE(DATE_FORMAT(s.end_date_time, '%H:%i'), ''), ' | ', s.title) AS schedule_title,
			ch.channel_id, ch.channel_type_id, ch.channel_name, ch.alias,
			0 AS program_id, s.title AS program_name, '' AS description,
			ct.content_id, ct.title, ct.description, ct.video_length
			FROM epg s
			JOIN channel ch ON ch.channel_id = s.channel_id
			JOIN content ct ON ct.content_id = s.content_id
			WHERE s.epg_id = '$schedule_id'
			";
		}
	
		$query = $this->db->query($textquery);
		$row = $query->row();
	
		return $row;
	
	}
	
	//public function program_contents($schedule_id, $program_id, $content_id_now=0) {
	public function program_contents($schedule_id, $content_id_now=0) {
		
		/*
		$textquery = "
		SELECT ct.content_id, ct.title, ct.description,
		video_length as duration, status, video_thumbnail,
		CASE
		WHEN ct.content_id='$content_id_now'  THEN 1
		ELSE 0
		END as isnow
		FROM schedule s
		JOIN program p ON s.program_id=p.program_id
		JOIN program_content pc ON pc.program_id = p.program_id
		JOIN content ct ON ct.content_id = pc.content_id
		WHERE s.schedule_id = '$schedule_id'
		ORDER BY pc.order_number
		";
		*/

		/*
		$textquery = "
		SELECT play_starttime, play_endtime, content_id, title, description,
		video_length as duration, status, video_thumbnail,
		(CASE WHEN content_id='$content_id_now' AND NOW() BETWEEN DATE_ADD(play_starttime, INTERVAL -1 MINUTE) AND DATE_ADD(play_endtime, INTERVAL 1 MINUTE) THEN 1
		ELSE 0 END) AS isnow
		FROM
		(
		SELECT
		(DATE_ADD(s.start_date_time, INTERVAL @last_duration SECOND)) AS play_starttime,
		(DATE_ADD(DATE_ADD(s.start_date_time, INTERVAL @last_duration SECOND), INTERVAL video_length SECOND)) AS play_endtime,
		ct.content_id, ct.title, ct.description, ct.video_length, ct.video_thumbnail, ct.status,
		(@last_duration := @last_duration + ct.video_length ) AS tempvar
		FROM schedule s, program p, program_content pc, content ct, (SELECT @last_duration:=0 AS just_var) t_var
		WHERE s.program_id=p.program_id AND pc.program_id = p.program_id AND ct.content_id = pc.content_id
		AND s.schedule_id = '$schedule_id'
		ORDER BY pc.order_number
		) t_temp
		";
		*/
		
		$textquery = "
		SELECT play_starttime, play_endtime, content_id, title, description,
		video_length as duration, status, video_thumbnail,
		(CASE WHEN content_id='$content_id_now' THEN 1 ELSE 0 END) AS isnow
		FROM
		(
		SELECT
		(DATE_ADD(s.start_date_time, INTERVAL @last_duration SECOND)) AS play_starttime,
		(DATE_ADD(DATE_ADD(s.start_date_time, INTERVAL @last_duration SECOND), INTERVAL video_length SECOND)) AS play_endtime,
		ct.content_id, ct.title, ct.description, ct.video_length, ct.video_thumbnail, ct.status,
		(@last_duration := @last_duration + ct.video_length ) AS tempvar
		FROM schedule s, program p, program_content pc, content ct, (SELECT @last_duration:=0 AS just_var) t_var
		WHERE s.program_id=p.program_id AND pc.program_id = p.program_id AND ct.content_id = pc.content_id
		AND s.schedule_id = '$schedule_id'
		ORDER BY pc.order_number
		) t_temp
		";
		
				
		$query = $this->db->query($textquery);
		return $query->result();
	}
	
	
	//public function get_now_playing($schedule_id) {
	public function get_now_playing($schedule_id, $content_id_now=0) {		
		$textquery = "
			SELECT * FROM
			(
				SELECT
					(DATE_ADD(s.start_date_time, INTERVAL @last_duration SECOND)) AS play_starttime,
					(DATE_ADD(DATE_ADD(s.start_date_time, INTERVAL @last_duration SECOND), INTERVAL video_length SECOND)) AS play_endtime,
					ct.content_id, ct.title, ct.description, ct.video_length, ct.video_thumbnail,
					(@last_duration := @last_duration + ct.video_length ) AS tempvar
					FROM schedule s, program p, program_content pc, content ct, (SELECT @last_duration:=0 AS just_var) t_var
					WHERE s.program_id=p.program_id AND pc.program_id = p.program_id AND ct.content_id = pc.content_id
					AND s.schedule_id = '$schedule_id'
					ORDER BY pc.order_number
			) t_temp
			WHERE content_id = '$content_id_now'";	
		
			//WHERE now() BETWEEN play_starttime AND play_endtime //BUG There is Delay (Gap) between schedule
			//";
	
			//if($content_id_now && $content_id_now > 0){
			//	$textquery += " WHERE content_id = '$content_id_now'";	
			//}else {
			//	$textquery += " WHERE now() BETWEEN play_starttime AND play_endtime";
			//}	
					
			$query = $this->db->query($textquery);
			return $query->row();
	
	}
}

?>
