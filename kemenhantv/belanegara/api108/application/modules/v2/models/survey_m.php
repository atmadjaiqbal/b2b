<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Zola Octanoviar
	Date: nov, 17 2014 22:20 
*/
class Survey_m extends MY_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function survey_list($apps_id, $channel_id, $mobile_id){
			$sql = "
				SELECT survey_id, survey_text FROM survey a
				WHERE apps_id = '$apps_id'
				AND channel_id = '$channel_id'
				AND a.status = 1
				AND now() between start_date and end_date
				AND survey_id not in (select survey_id from survey_user where survey_id=a.survey_id AND mobile_id='$mobile_id')
				ORDER BY end_date DESC
				LIMIT 1
			";
					
			$query = $this->db->query($sql);	
			$row =  $query->row();
			if($row){
				$row->quest_list = $this->survey_quest($row->survey_id);
			}
			return $row;
	}
	
	public function survey_quest($survey_id){
			$sql = "
				SELECT order_number, quest_text FROM survey_quest
				WHERE survey_id='$survey_id' 
				ORDER BY order_number
			";
					
			$query = $this->db->query($sql);	
			$rows =  $query->result();
			return $rows;
	}
	

	public function survey_save($survey_id, $mobile_id, $quest_number){
		$sql  = "
			SELECT survey_id, mobile_id
			FROM survey_user
			WHERE survey_id='$survey_id' AND mobile_id = '$mobile_id' 
		 	";
		$rows = $this->db->query($sql)->row();	
		
		if(!$rows){
			$sql = "
				INSERT INTO survey_user (survey_id, mobile_id, quest_number, created) 
				VALUES ('$survey_id', '$mobile_id', $quest_number, now())
			 	";
			$query = $this->db->query($sql);	
			$response = '1';
		} else {
			$response =  '0';
		}
		return $response;
	}
	
	
	


}
