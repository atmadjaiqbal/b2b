<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Zola Octanoviar
	Date: nov, 17 2014 22:20 
*/
class Parental_m extends MY_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function parental_list(){
		$sql = "
			SELECT
				parental_id, parental_name
			FROM parental
			WHERE active = '1'
			ORDER BY parental_name
		";
				
		$rows  = $this->db->query($sql)->result();
		return $rows;
	}
	

	public function parental_save($apps_id, $customer_id, $parental_id){
		$textquery = "
			UPDATE customer
			SET parental_id = '$parental_id'
			WHERE customer_id='$customer_id'
		 ";
		$this->db->query($textquery);	
	
	}

	public function parental_detail($apps_id, $customer_id){
		$sql = "
			SELECT
				customer.parental_id
			FROM customer
			INNER JOIN parental on customer.parental_id = parental.parental_id 
			WHERE apps_id = '$apps_id' AND customer_id='$customer_id'
		";
				
		$rows  = $this->db->query($sql)->row();
		$parental_id = '';	
		if ($rows) $parental_id = $rows->parental_id;
		return $parental_id;
	}
	


}
