<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Referral_m extends MY_Model {

    public function __construct() {
        parent::__construct();
        
        $this->load->helper(array('string'));
    }

    private function generate_army_code_by_phone($length, $suffix, $appendix = 'A') {
        $new_key = $suffix . $appendix;
        if (self::key_exists($new_key)) {
            $new_key = self::generate_reference_number($length, $suffix, ++$appendix);
        }
        return $new_key;
    }

    private function generate_army_code($length) {
        $new_key = '';
        do {
//            $salt = do_hash(time() . mt_rand());
            $salt = random_string('unique', $length);
            $new_key = strtoupper(substr($salt, 0, $length));
            
        } while (self::key_exists($new_key));
        
        return $new_key;
    }

    private function key_exists($key) {
        return $this->db->where('reference_number', $key)->count_all_results('invitation_referral_user') > 0;
    }

    public function generateReference($length = 6, $suffix = "") {
//        return $this->generate_reference_number($length, $suffix);
        return $this->generate_army_code($length);
    }

    public function getInstalledMobileID($apps_id, $mobile_id) {
        $sql = "
                SELECT
                    `mobile_id`
                FROM 
                    `apps_mobile_id`
                WHERE
                    `apps_id` = '$apps_id'
                    AND `mobile_id` = '$mobile_id'
                ";
        $rows = $this->db->query($sql)->row();
        return $rows;
    }

    public function getReferenceLog($apps_id, $mobile_id, $referral_number) {
        $sql = "
                SELECT
                    `log_id`,
                    `log_date`,
                    `apps_id`,
                    `invitation_id`,
                    `referral_number`,
                    `customer_id`,
                    `mobile_id`,
                    `status`
                FROM 
                    `invitation_logs`
                WHERE
                    `apps_id` = '$apps_id'
                    AND `mobile_id` = '$mobile_id'
                ";
        if (empty($referral_number)) {
            $sql .= " AND `referral_number` = '$referral_number' ";
        }

        $rows = $this->db->query($sql)->row();
        return $rows;
    }

    public function getReferenceHistory($apps_id, $reference_number, $limit, $offset) {
        $sql = "
            SELECT
                `log_id`,
                `log_date`,
                `apps_id`,
                a.invitation_id,
                `referral_number`,
                `customer_id`,
                `mobile_id`,
                `status`,
                b.reward_type,
                b.reward_value
            FROM 
                `invitation_logs` a LEFT JOIN `invitation` b ON a.invitation_id = b.invitation_id
            WHERE
                `apps_id` = '$apps_id'
                AND `referral_number` = '$reference_number' 
            ORDER BY log_date ASC
        ";

        if (!empty($limit)) {
            $sql .= " LIMIT $limit ";
        }
        if ($offset != null && $offset != "") {
            $sql .= " OFFSET $offset ";
        }

        $rows = $this->db->query($sql)->result();
        return $rows;
    }

    public function countReferenceHistory($apps_id, $reference_number) {
        $sql = "
            SELECT COUNT(log_id) AS data_count
            FROM 
                `invitation_logs`
            WHERE
                `apps_id` = '$apps_id'
                AND `referral_number` = '$reference_number' 
        ";

        $rows = $this->db->query($sql)->row()->data_count;
        return $rows;
    }

    public function getReferralInvitation() {
        $sql = "
            SELECT
                `invitation_id`,
                `invitation_type`,
                `created_date`,
                `start_date`,
                `end_date`,
                `reward_type`,
                `reward_value`
            FROM `invitation`
            WHERE
                start_date <= NOW()
                AND end_date >= NOW()
            LIMIT 1;";

        $row = $this->db->query($sql)->row();
        return $row;
    }

    public function saveReferenceLog($log_id, $apps_id, $mobile_id, $referral_number, $invitation_id, $customer_id) {

        $sql = "
                INSERT INTO `invitation_logs`
                    (`log_id`,
                     `log_date`,
                     `apps_id`,
                     `invitation_id`,
                     `referral_number`,
                     `customer_id`,
                     `mobile_id`,
                     `status`)
                VALUES (
                    '$log_id',
                    NOW(),
                    '$apps_id',
                    '$invitation_id',
                    '$referral_number',
                    '$customer_id',
                    '$mobile_id',
                    1
                );";

        $this->db->query($sql);
    }

    public function getReferralUser($reference_number) {
        $sql = "
            SELECT
                `referral_id`,
                `reference_number`,
                `customer_id`,
                `id_type`,
                `id_number`,
                `bank_code`,
                `bank_account_number`,
                `bank_account_name`,
                `register_date`,
                `status`
            FROM 
                `invitation_referral_user`
            WHERE
                `reference_number` = '$reference_number';";

        $row = $this->db->query($sql)->row();
        return $row;
    }

    public function getReferalUserByCustomerId($customer_id) {
        $sql = "
                SELECT 
                    `referral_id`,
                    `reference_number`,
                    `customer_id`,
                    `id_type`,
                    `id_number`,
                    `bank_code`,
                    `bank_account_number`,
                    `bank_account_name`,
                    `register_date`,
                    `status`
                FROM 
                    invitation_referral_user
                WHERE 
                    customer_id='$customer_id'
                ";
        $row = $this->db->query($sql)->row();
        return $row;
    }

    public function registerReferrerUser($customer_id, $reference_number, $id_type, $id_number, $bank_code, $bank_account_number, $bank_account_name) {
        $sql = "
            INSERT INTO invitation_referral_user (
                `customer_id`,
                `reference_number`,
                `id_type`,
                `id_number`,
                `bank_code`,
                `bank_account_number`,
                `bank_account_name`,
                `register_date`,
                `status`
            )
            VALUES (
                '$customer_id',
                '$reference_number',
                $id_type,
                '$id_number',
                '$bank_code',
                '$bank_account_number',
                '$bank_account_name',
                NOW(),
                1
            );";

        $this->db->query($sql);
    }

    public function updateReferralUser($customer_id, $id_type, $id_number, $bank_code, $bank_account_number, $bank_account_name) {
        $sql = "
            UPDATE 
                `invitation_referral_user`
            SET 
                  `id_type` = $id_type,
                  `id_number` = '$id_number',
                  `bank_code` = '$bank_code',
                  `bank_account_number` = '$bank_account_number',
                  `bank_account_name` = '$bank_account_name'
            WHERE 
                `customer_id` = '$customer_id'
            ";

        $this->db->query($sql);
    }

    public function sum_total_reward($apps_id, $referral_number) {
        $sql = "
            SELECT
                SUM(b.reward_value) AS reward_total
            FROM 
                `invitation_logs` a LEFT JOIN `invitation` b ON a.invitation_id = b.invitation_id
            WHERE
                `apps_id` = '$apps_id'
                AND `referral_number` = '$referral_number'
        ";

        $result = $this->db->query($sql)->row()->reward_total;
        return $result;
    }

    public function getBankList() {
        $sql = "
            SELECT
                `bank_code`,
                `bank_name`
            FROM `bank`;
            ";
        $row = $this->db->query($sql)->result();
        return $row;
    }

    public function getGrandPrizeList() {
        $sql = "
            SELECT
                `grandprize_id`,
                `invitation_id`,
                `title`,
                `description`,
                `thumbnail`,
                `order_no`,
                `required_download`
            FROM `invitation_grandprize`;
            ";
        $rows = $this->db->query($sql)->result();
        foreach ($rows as $row) {
            $row->thumbnail = $this->get_thumbnail_url($row->thumbnail) . "&w=200";
        }

        return $rows;
    }

    public function getIntroduction($lang) {

        $introuction_field = $lang == "indonesian" ? "introduction" : "introduction_en";
        $legal_field = $lang == "indonesian" ? "legal" : "legal_en";

        $sql = "
            SELECT
                `$introuction_field` AS introduction,
                `$legal_field`  AS legal
            FROM `invitation_setup`;
            ";
        $row = $this->db->query($sql)->row();
        return $row;
    }

}

?>