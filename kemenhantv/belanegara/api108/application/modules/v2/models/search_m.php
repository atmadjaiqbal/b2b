<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  Zola Octanoviar
  Date: oct 2015
 */

class Search_m extends MY_Model {


    public function __construct() {
        parent::__construct();
    }

	function xsearch_list($page, $limit, $apps_id, $keyword){
		$sql = "SELECT crew_id, crew_name
		        FROM crew
        		WHERE crew_name = '{$keyword}' OR crew_name = '{$keyword}'
        		ORDER BY crew_name
        ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    public function search_list($page=0, $limit=28, $apps_id, $keyword) {
    	
        $keyword_weight  = 2;
        $keyword_text  = '';
        $keyword_exact = '';
        $keyword_like  = '';
        $full_keyword  = $keyword;
        $keywords      = explode(' ',$keyword);
		$weight		   = '';
        //	$excludeKeyword = array('and','of');
		$excludeKeyword = array();
        foreach($keywords as $k){
            if (!in_array(strtolower($k), $excludeKeyword)){
                $keyword_text  .= $k.' ';
                $keyword_exact .= '+'.$k.' ';

                // find keyword in crew, genre, category, if found exlude keyword from keyword like
                $inCrew = $this->searchCrew($k);
                if (!$inCrew) $keyword_like  .= $k.'* ';

                $inGenre= $this->searchGenre($k);
                if (!$inGenre) $keyword_like  .= $k.'* ';

            }
        }
        if (strlen($keyword_text)> 0) $keyword_text = substr($keyword_text,0,strlen($keyword_text)-1 );
        if (strlen($keyword_exact)> 0) $keyword_exact = substr($keyword_exact,0,strlen($keyword_exact)-1 );
        if (strlen($keyword_like)> 0) $keyword_like = substr($keyword_like,0,strlen($keyword_like)-1 );

        $weight .= "(IF(cs.title = '{$full_keyword}', 30, 0))\n";
        $weight .= " + (IF(MATCH(cs.title) AGAINST ('\"".$keyword_text."\"' IN BOOLEAN MODE), 28, 0) ) \n";
        $weight .= " + (IF(MATCH(cs.title) AGAINST ('".$keyword_exact."' IN BOOLEAN MODE), 26, 0) ) \n";
        $weight .= " + (IF(MATCH(cs.title) AGAINST ('".$keyword_like."' IN BOOLEAN MODE), 24, 0) ) \n";

        $weight .= " + (IF(cr.crew_name = '{$full_keyword}', 20, 0) ) \n";
        $weight .= " + (IF(MATCH(cr.crew_name) AGAINST ('\"".$keyword_text."\"' IN BOOLEAN MODE), 18, 0) ) \n";
        $weight .= " + (IF(MATCH(cr.crew_name) AGAINST ('".$keyword_exact."' IN BOOLEAN MODE), 16, 0) ) \n";
        $weight .= " + (IF(MATCH(cr.crew_name) AGAINST ('".$keyword_like."' IN BOOLEAN MODE), 14, 0) ) \n";
        $weight .= " + (IF(g.genre_name = '{$full_keyword}', 10, 0) ) \n";
        $weight .= " + (IF(MATCH(g.genre_name) AGAINST ('\"".$keyword_text."\"' IN BOOLEAN MODE), 8, 0) ) \n";
        $weight .= " + (IF(MATCH(g.genre_name) AGAINST ('".$keyword_exact."' IN BOOLEAN MODE), 6, 0) ) \n";
        $weight .= " + (IF(MATCH(g.genre_name) AGAINST ('".$keyword_like."' IN BOOLEAN MODE), 4, 0) ) ";
        $weight .= "as weight_value,  \n";
        $order_by = "        	ORDER BY weight_value DESC LIMIT $page, $limit ";
        $sql = "SELECT 
        		$weight 
                m.menu_id, p.apps_id, p.product_id,'' channel_category_id, '' channel_id, '' channel_type_id, c.content_id, 
                IF(c.status = '1','channel','content') content_type,
                c.title, '' category_name, concat('".$this->config->item('thumbnail_url')."',video_thumbnail,'&w=350') thumbnail,  
                description, prod_year, SEC_TO_TIME(video_length*60) video_duration,video_length,
                (SELECT count(content_id) FROM content_like WHERE content_id=c.content_id AND ulike=1) as countlike,	
                (SELECT count(content_id) FROM content_comment WHERE content_id=c.content_id AND status=1) as countcomment,					
                countviewer,0 icon_size, 0 poster_size, 0 price
            FROM 
                menu_product p
	            INNER JOIN menu m on p.menu_id = m.menu_id
	            INNER JOIN content_search cs on p.product_id = cs.content_id 
	            INNER JOIN content c on p.product_id = c.content_id and c.active='1'
	            LEFT JOIN content_crew ccr on p.product_id = ccr.content_id
	            LEFT JOIN crew_search cr on ccr.crew_id = cr.crew_id
	            LEFT JOIN content_genre cg on p.product_id = cg.content_id
	            LEFT JOIN genre_search g on g.genre_id = cg.genre_id
            WHERE c.active='1' AND p.apps_id = '$apps_id' AND m.apps_id = '$apps_id'
			GROUP BY c.title
			having weight_value > 0

		";	
        $total_item = 0;
        if($page == 0){
        	$sql_count = "select count(content_id) as total_item from (" . $sql . ") tmp";
        	$total_item = $this->db->query($sql_count)->row()->total_item;
        }
		$sql .= $order_by;
		
		$query = $this->db->query($sql);
		$rows = $query->result();

        foreach ($rows as $row) {
            $row->genre_list = $this->content_genre($row->product_id);
        }

		$result = array(
			"total" => $total_item,
			"list" => $rows,
		);

        return $result;
    }


	function searchCrew($name){
		$sql = "SELECT crew_id, crew_name
		        FROM crew
        		WHERE crew_name = '{$name}' OR crew_name = '{$name}'
        		ORDER BY crew_name
        ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function searchGenre($name){
		$sql = "SELECT genre_id, genre_name
		        FROM genre
        		WHERE genre_name = '{$name}' OR genre_name = '{$name}'
        		ORDER BY genre_name
        ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function content_genre($content_id) {
        $textquery = "
            SELECT a.genre_id, b.genre_name
            FROM content_genre a, genre b
            WHERE a.content_id = '$content_id'
            AND a.genre_id = b.genre_id
        ";

        $query = $this->db->query($textquery);
        $rows = $query->result();
        return $rows;
    }



}
