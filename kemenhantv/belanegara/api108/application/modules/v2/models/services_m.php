<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services_m extends MY_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function setweather($latitude, $longitude, $cuaca, $suhu){
		$textquery = "
			REPLACE INTO weather (latitude, longitude, cuaca, suhu, last_update)
			VALUES ('$latitude', '$longitude', '$cuaca', '$suhu', now())
		";
		
		$query = $this->db->query($textquery);

		$weather = $this->getweather($latitude, $longitude);

		return $weather;
	}

	public function getweather($latitude, $longitude){
		$textquery = "
			SELECT 
				latitude, longitude, cuaca, floor(suhu) suhu, last_update,
				timestampdiff(minute, last_update, now()) minutediff
  			FROM weather
  			WHERE latitude = '$latitude' and longitude = '$longitude'
			";
  		$query = $this->db->query($textquery);

		return $query->row();
	}

	public function getdictionary($wordtotranslate, $localeID){
		$textquery = "
			SELECT 
				words
  			FROM dictionary
  			WHERE lower(keyword) = lower('$wordtotranslate') and lang = '$localeID'
			";

  		$query = $this->db->query($textquery);
  		if($query->num_rows() > 0){
  			$result = $query->row();
  			$result = $result->words;
  		}
  		else{
  			$result = $wordtotranslate;
  		}

		return $result;
	}

	public function getfids_lastupdatetime($airportcode){
		$textquery = "
			SELECT max(updatetime) as updatetime,
				TIMESTAMPDIFF(MINUTE, MAX(updatetime), NOW()) minutediff
  			FROM fids
  			WHERE airportcode = '$airportcode'
  			order by updatetime DESC
			";
  		$query = $this->db->query($textquery);

		return $query->row();
	}

	public function getfidstoday($airportcode){
		error_reporting(0);
		$arr_result = array();

		$airlines = array(
					        'JT' => array('airlinename' => 'Lion Air', //'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/lni.png'
					                ),
					        'ID' => array('airlinename' => 'Batik Air', //'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/btk.png'
					                ),
					        'SJ' => array('airlinename' => 'Sriwijaya Air',//'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/sjy.png'
					                ),
					        'KD' => array('airlinename' => 'Kal Star Aviation',//'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/kls.png'
					                ),
					        'IW' => array('airlinename' => 'Wings Air',//'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/won.png'
					                ),
					        'GA' => array('airlinename' => 'Garuda Indonesia',//'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/gia.png'
					                ),
					        'QG' => array('airlinename' => 'Citilink',//'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/ctv.png'
					                ),
					        'PK' => array('airlinename' => 'Charter',//'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/pk.png'
					                ),
					        '6D' => array('airlinename' => 'Pelita Air',//'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/pas.png'
					                ),
					       	'GM' => array('airlinename' => 'Tri-M.G. Airlines',//'thumbnail' => 'http://sepinggan-airport.com/the-fids/partials/images/tmg.png'
					                ),
					       	'IN' => array('airlinename' => 'NAM Air',//'thumbnail' => ''
					                ),
					       	'SI' => array('airlinename' => 'Susi Air',//'thumbnail' => ''
					                ),
					       	'IL' => array('airlinename' => 'Trigana Air'),
					       	'3Y' => array('airlinename' => 'Kartika Airlines'),
					       	'KI' => array('airlinename' => 'Adam Air'),
					       	'MV' => array('airlinename' => 'Aviastar Indonesia'),
					       	'MZ' => array('airlinename' => 'Merpati Nusantara Airlines'),
					       	'QZ' => array('airlinename' => 'Indonesia AirAsia'),
					       	'RI' => array('airlinename' => 'Mandala Airlines'),
					       	'RY' => array('airlinename' => 'Pacific Royale Airways'),
					       	'XN' => array('airlinename' => 'Xpressair'),
					       	'Y6' => array('airlinename' => 'Batavia Air'),
					       	'LN' => array('airlinename' => 'Lion Air'),
					       	'WO' => array('airlinename' => 'Wings Air'),
					       	'GI' => array('airlinename' => 'Garuda Indonesia'),
					       	'BT' => array('airlinename' => 'Batik Air'),
					       	'CT' => array('airlinename' => 'Citilink'),
					       	'AF' => array('airlinename' => 'AIRFAST'),
					       	'SL' => array('airlinename' => 'Silk Air'),
					);

		$textdeparture = "
			SELECT airportcode, SUBSTRING(flightnumber, 1, 2) airlinecode, flightnumber, origin, destination, 
				DATE_FORMAT(schedule_time, '%H:%i') as schedule_times, status, updatetime
  			FROM fids
  			WHERE airportcode = '$airportcode' and (origin is null or origin = '')
  				AND STR_TO_DATE(CONCAT(DATE_FORMAT(schedule_date, '%Y-%m-%d'), ' ', DATE_FORMAT(schedule_time, '%H:%i')), '%Y-%m-%d %H:%i') 
					>= NOW()
  			order by schedule_date, schedule_time, STATUS
  			limit 0,100
			";

  		$querydep = $this->db->query($textdeparture);

  		if($querydep->num_rows() > 0){
  			foreach ($querydep->result_array() as $key => $value) {
  				if(!empty(trim($value['airlinecode']))){
  					$arr_result['departure'][] = array(
  												"airline" 			=> $airlines[$value['airlinecode']]['airlinename'],
  												// "airlinethumbnail"	=> $airlines[$value['airlinecode']]['thumbnail'],
  												"airportcode" 	 	=> $value['airportcode'],
									            "airlinecode" 	 	=> $value['airlinecode'],
									            "flightnumber" 	 	=> $value['flightnumber'],
									            "origin" 		 	=> $value['origin'],
									            "destination" 	 	=> $value['destination'],
									            "schedule_time"  	=> $value['schedule_times'],
									            "status" 		 	=> $value['status'],
									            "updatetime"	 	=> $value['updatetime']
  											);
  				}
  			}
  			// $arr_result['departure'] = $querydep->result();
  		}else{
  			$arr_result['departure'] = array();
  		}

  		$textarrival = "
			SELECT airportcode, SUBSTRING(flightnumber, 1, 2) airlinecode, flightnumber, origin, destination, 
				DATE_FORMAT(schedule_time, '%H:%i') as schedule_times, status, updatetime
  			FROM fids
  			WHERE airportcode = '$airportcode' and (destination is null or destination = '')
  				AND STR_TO_DATE(CONCAT(DATE_FORMAT(schedule_date, '%Y-%m-%d'), ' ', DATE_FORMAT(schedule_time, '%H:%i')), '%Y-%m-%d %H:%i') 
					>= NOW()
  			order by schedule_date, schedule_time, STATUS
			";

  		$queryarri = $this->db->query($textarrival);

  		if($queryarri->num_rows() > 0){
  			foreach ($queryarri->result_array() as $key => $value) {
  				if(!empty(trim($value['airlinecode']))){
  					$arr_result['arrival'][] = array(
  												"airline" 			=> $airlines[$value['airlinecode']]['airlinename'],
  												// "airlinethumbnail"	=> $airlines[$value['airlinecode']]['thumbnail'],
  												"airportcode" 	 	=> $value['airportcode'],
									            "airlinecode" 	 	=> $value['airlinecode'],
									            "flightnumber" 	 	=> $value['flightnumber'],
									            "origin" 		 	=> $value['origin'],
									            "destination" 	 	=> $value['destination'],
									            "schedule_time"  	=> $value['schedule_times'],
									            "status" 		 	=> $value['status'],
									            "updatetime"	 	=> $value['updatetime']
  											);	
  				}
  			}
  			// $arr_result['arrival'] = $queryarri->result();
  		}else{
  			$arr_result['arrival'] = array();
  		}

		return $arr_result;
	}

	public function saveReferenceCode($user_id, $mobile_id, $reference_code, $invitation_id ){
		$sql = "
			SELECT log_id, mobile_id
			FROM invitation_logs
			WHERE reference_code='$reference_code' AND mobile_id = '$mobile_id' AND user_id = '$user_id'
			";
		$rows = $this->db->query ( $sql )->row ();
	
		if(!$rows){
			$sql = "
			INSERT INTO invitation_logs (invitation_id, user_id, mobile_id, reference_code, status, log_date)
			VALUES ('$invitation_id', '$user_id', '$mobile_id', '$reference_code', 0, now())
			";
			$query = $this->db->query($sql);
			$response = 1;
		} else {
			$response =  0;
		}
			
		return $response;
	}
	
	public function registerRefererUser($real_name, $gender, $birthdate, $id_type, $id_number, $bank_number, $phone,$address, $email, $user_id, $mobile_id){
		$sql = "
			SELECT user_id from invitation_referral_user
			WHERE user_id='$user_id' and mobile_id = '$mobile_id'
			";
		$rows = $this->db->query ( $sql )->row ();
		
		if (! $rows) {
			$sql = "
			INSERT INTO invitation_referral_user (real_name, gender, birthdate, id_type, id_number, bank_account_type, 
			bank_account_number, phone, address, email, user_id, mobile_id)
			VALUES ('$real_name', '$gender', STR_TO_DATE('$birthdate', '%d-%m-%Y'), $id_type, '$id_number', 0, '$bank_number', '$phone', 
			'$address', '$email', '$user_id', '$mobile_id')
			";
			$query = $this->db->query ( $sql );
			$response = 1;
		} else {
			$response = 0;
		}
		return $response;
	}

	public function getnomorpenting($channel_id){
		$textquery = "
			SELECT 
				IFNULL(title, '') as nama_penting, tipe, nophone, enable_call, enable_sms
  			FROM nomor_penting
  			WHERE channel_id = '$channel_id'
  			order by index_count asc
			";
  		$query = $this->db->query($textquery);
  		$result = array();

  		if($query->num_rows() > 0){

  			foreach ($query->result_array() as $key => $value) {

  				$result[] = array(
  						'nama_penting' => $value['nama_penting'],
  						'tipe' 		   => $value['tipe'],
  						'telp_penting' => array(array(
  								'no' 			=> $value['nophone'],
  								'enable_call' 	=> $value['enable_call'],
  								'enable_sms' 	=> $value['enable_sms'],
  							))
  					);
  			}
  		}
  		
		return $result;
	}
}
?>