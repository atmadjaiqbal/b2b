<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Zola Octanoviar
	Date: nov, 17 2014 22:20 
*/
class Menu_m extends MY_Model {
	
	protected $table 	= 'menu';
	protected $key		= 'menu_id';

	public function __construct()
	{
		parent::__construct();
	}
	
	public function menu_list($apps_id, $device_id, $menu_id='0',$menu_type='menu'){
		if ($menu_type =='menu') {
                    $textquery = "
                        SELECT 
                                        b.product_id, a.apps_id, a.menu_id, a.parent_menu_id, 'menu' menu_type,  a.title, a.thumbnail, a.link , a.tab_type, a.show_tab_menu, a.show_dd_menu, 
                                        a.active, count(b.menu_id) count_product, c.icon_size, c.poster_size, a.created
                        FROM menu a
                        LEFT  JOIN menu_product b on a.menu_id = b.menu_id 
                        INNER JOIN apps c on a.apps_id = c.apps_id
                        WHERE  a.apps_id ='$apps_id' 
                                AND parent_menu_id = '$menu_id' 
                        GROUP BY a.menu_id
                        ORDER BY a.order_number, b.order_number
                     ";
		}  else {
                    $order_by = ($device_id == '1') ?  " b.order_number " : " c.alias" ;
                    $textquery = "
                            SELECT 
                    b.product_id, a.apps_id, a.menu_id, a.parent_menu_id,  c.product_type menu_type,  c.title title, c.thumbnail, '' as link , 
                    '0' tab_type, '0' show_tab_menu, '0' show_dd_menu, a.active, '1' count_product, d.icon_size, d.poster_size, a.created
                    FROM menu a
                    INNER JOIN menu_product b on a.menu_id  = b.menu_id 
                    INNER JOIN vproduct c on b.product_id = c.product_id 
                    INNER JOIN apps d on a.apps_id = d.apps_id
                    WHERE  a.apps_id ='$apps_id' 
                            AND a.menu_id = '$menu_id' 
                    ORDER BY a.order_number, b.order_number
                     ";
		}
		$rows = $this->db->query($textquery)->result();
                $results    = array();
                foreach($rows as $row) {
                    $sub_menu = array();
                    if ($menu_type == 'menu') {
                                            $sub_menu = $this->menu_list($apps_id, $device_id, $row->menu_id, 'menu');	
                                            $sub_menu_channel = $this->menu_list($apps_id, $device_id, $row->menu_id, 'channel');
                                            $sub_menu = array_merge($sub_menu, $sub_menu_channel);
                    }

                    $row->sub_menu_list  = $sub_menu;

                                    $width = $row->icon_size;
                                    if ($device_id =='4') {
                                            $width = '50';
                                    }

                                    $row->thumbnail = $this->get_thumbnail_url($row->thumbnail) ."&w=".$width;
                    $results[] = $row;
                }
                return $results;
	}
        
        public function menu_list_all($apps_id, $device_id, $menu_id='0',$menu_type='menu', $limit=10, $offset=0, $sublimit=10) {
            $textquery = "";
            if ($menu_type =='menu') {
                    $textquery = "
                        SELECT
                            b.product_id, a.apps_id, a.menu_id, a.parent_menu_id, 'menu' AS  menu_type,  a.title, a.thumbnail, a.link, a.tab_type, a.show_tab_menu, a.show_dd_menu,
                            a.active, a.menu_direction, COUNT(b.menu_id) count_product, c.icon_size, c.poster_size, a.created
                        FROM menu a
                        LEFT  JOIN menu_product b ON a.menu_id = b.menu_id
                            INNER JOIN apps c ON a.apps_id = c.apps_id
                        WHERE  a.apps_id = '$apps_id'
                            AND parent_menu_id = '$menu_id'
                        GROUP BY a.menu_id
                        ORDER BY a.order_number, b.order_number
                     ";
                    $textquery .= ($menu_id != 0)?" LIMIT $limit OFFSET $offset":  "";
		}  else {
                    $textquery = "
                        SELECT
                            b.product_id, a.apps_id, a.menu_id, 'product' as menu_type,  a.parent_menu_id,  c.product_type product_type,  c.title title, c.thumbnail, c.link , a.active, d.icon_size, d.poster_size, a.created,
                            c.channel_type_id, c.status, c.additional_parameter as addparam
                        FROM menu a
                            INNER JOIN menu_product b ON a.menu_id  = b.menu_id
                            INNER JOIN vproduct_all c ON b.product_id = c.product_id
                            INNER JOIN apps d ON a.apps_id = d.apps_id
                        WHERE  a.apps_id = '$apps_id'
                            AND a.menu_id = '$menu_id'
                        ORDER BY a.order_number, b.order_number
                        LIMIT $limit
                        OFFSET $offset
                     ";
		}
		$rows = $this->db->query($textquery)->result();
        // echo $this->db->last_query();
                $results    = array();
                foreach($rows as $row) {
                    $sub_menu = array();
                    $sub_product_list = array();
                    $sub_menu_total = 0;
                    $sub_product_total = 0;
                    if ($row->menu_type == 'menu') {
                        $sub_menu = $this->menu_list_all($apps_id, $device_id, $row->menu_id, 'menu', $sublimit, 0, $sublimit);
                        $sub_menu_total = $this->menu_count($apps_id, $device_id, $row->menu_id, 'menu');
                        $sub_product_list = $this->menu_list_all($apps_id, $device_id, $row->menu_id, 'program', $sublimit, 0, $sublimit);
                        $sub_product_total = $this->menu_count($apps_id, $device_id, $row->menu_id, 'program');
                        
                        $row->menu_total = $sub_menu_total;
                        $row->menu_list  = $sub_menu;
                        $row->product_total = $sub_product_total;
                        $row->product_list = $sub_product_list;
                    }

                    $width = $row->icon_size;
                    if ($device_id =='4') {
                            $width = '50';
                    }

                    $row->thumbnail = $this->get_thumbnail_url($row->thumbnail) ."&w=".$width;
                    $results[] = $row;
                }
                return $results;
        }
        
        public function menu_count($apps_id, $device_id, $menu_id='0',$menu_type='menu') {
            if ($menu_type =='menu') {
                //$order_by = ($device_id == '1') ?  " a.order_number " : " a.title " ;
                $textquery = "
                    SELECT COUNT(menu_id) AS datacount
                    FROM (
                        SELECT a.menu_id FROM menu a
                            LEFT  JOIN menu_product b ON a.menu_id = b.menu_id 
                            INNER JOIN apps c ON a.apps_id = c.apps_id
                        WHERE  a.apps_id = '$apps_id'
                                AND parent_menu_id = '$menu_id'
                        GROUP BY a.menu_id
                    )product
                 ";
            }  else {
                $order_by = ($device_id == '1') ?  " b.order_number " : " c.alias" ;
                $textquery = "
                        SELECT COUNT(b.product_id) AS datacount
                        FROM menu a
                            INNER JOIN menu_product b on a.menu_id  = b.menu_id 
                            INNER JOIN vproduct_all c on b.product_id = c.product_id 
                            INNER JOIN apps d on a.apps_id = d.apps_id
                        WHERE  a.apps_id = '$apps_id'
                            AND a.menu_id = '$menu_id'
                 ";
            }
            $query = $this->db->query($textquery);
            return $query->row()->datacount;
        }


        public function navigation_count($apps_id, $device_id, $menu_id='0',$menu_type='menu') {
            if ($menu_type =='menu') {
                //$order_by = ($device_id == '1') ?  " a.order_number " : " a.title " ;
                $textquery = "
                    SELECT COUNT(menu_id) AS datacount
                    FROM (
                        SELECT a.menu_id FROM menu a
                            LEFT  JOIN menu_product b ON a.menu_id = b.menu_id 
                            INNER JOIN apps c ON a.apps_id = c.apps_id
                        WHERE  a.apps_id = '$apps_id'
                                AND parent_menu_id = '$menu_id'
                        GROUP BY a.menu_id
                    )product
                 ";
            }  else {
                $order_by = ($device_id == '1') ?  " b.order_number " : " c.alias" ;
                $textquery = "
                        SELECT COUNT(b.product_id) AS datacount
                        FROM menu a
                            INNER JOIN menu_product b on a.menu_id  = b.menu_id 
                            INNER JOIN vproduct_all c on b.product_id = c.product_id 
                            INNER JOIN apps d on a.apps_id = d.apps_id
                        WHERE  a.apps_id = '$apps_id'
                            AND a.menu_id = '$menu_id'
                 ";
            }
            $query = $this->db->query($textquery);
            return $query->row()->datacount;
        }


        public function navigation_list_all($apps_id, $device_id, $menu_id='0',$menu_type='menu', $limit=10, $offset=0, $sublimit=10) {
            $textquery = "";
            if ($menu_type =='menu') {
                    $textquery = "
                        SELECT
                            b.product_id, a.apps_id, a.menu_id, a.parent_menu_id, 'menu' AS  menu_type,  a.title, 
                            CONCAT('".$this->config->item('thumbnail_url')."',a.thumbnail, '&w=', c.icon_size) thumbnail, 
                            a.link, a.tab_type, a.show_tab_menu, a.show_dd_menu,
                            a.active, c.icon_size, c.poster_size, a.created, 
                            a.menu_direction, COUNT(b.menu_id) count_product, 'menu' type, a.order_number,
                            '' channel_type_id, '' status, '' addparam, '' product_type
                        FROM menu a
                        LEFT  JOIN menu_product b ON a.menu_id = b.menu_id
                            INNER JOIN apps c ON a.apps_id = c.apps_id
                        WHERE  a.apps_id = '$apps_id'
                            AND parent_menu_id = '$menu_id'
                        GROUP BY a.menu_id
                        ORDER BY a.order_number, b.order_number
                     ";
                    $textquery .= ($menu_id != 0)?" LIMIT $limit OFFSET $offset":  "";
		}  else {
                    $textquery = "
                        SELECT
                            b.product_id, a.apps_id, a.menu_id, a.parent_menu_id, 'product' as menu_type,    c.title title, 
                            CONCAT('".$this->config->item('thumbnail_url')."', c.thumbnail, '&w=', d.icon_size) thumbnail, 
                            c.link , '' tab_type, '' show_tab_menu, '' show_dd_menu,
                            a.active, d.icon_size, d.poster_size, a.created,
                            '' menu_direction, '' count_product, 'product' type, b.order_number,
                            c.channel_type_id, c.status, c.additional_parameter as addparam, c.product_type product_type  
                        FROM menu a
                            INNER JOIN menu_product b ON a.menu_id  = b.menu_id
                            INNER JOIN vproduct_all c ON b.product_id = c.product_id
                            INNER JOIN apps d ON a.apps_id = d.apps_id
                        WHERE  a.apps_id = '$apps_id'
                            AND a.menu_id = '$menu_id'
                        ORDER BY a.order_number, b.order_number
                        LIMIT $limit
                        OFFSET $offset
                     ";
		}
		$rows = $this->db->query($textquery)->result();
                $results    = array();
                foreach($rows as $row) {
                    $sub_menu = array();
                    $sub_product_list = array();
                    $sub_menu_total = 0;
                    $sub_product_total = 0;
                    if ($row->menu_type == 'menu') {
                        $sub_menu = $this->navigation_list_all($apps_id, $device_id, $row->menu_id, 'menu', $sublimit, 0, $sublimit);
                        $sub_menu_total = $this->menu_count($apps_id, $device_id, $row->menu_id, 'menu');
                        $sub_product_list = $this->navigation_list_all($apps_id, $device_id, $row->menu_id, 'program', $sublimit, 0, $sublimit);
                        $sub_product_total = $this->menu_count($apps_id, $device_id, $row->menu_id, 'program');
                        $menu_all = array_merge($sub_menu, $sub_product_list);
                        $menu_all_count =  $sub_menu_total + $sub_product_total ;
                        
                        $sortArray = array();

						foreach($menu_all as $person){
						    foreach($person as $key=>$value){
						        if(!isset($sortArray[$key])){
						            $sortArray[$key] = array();
						        }
						        $sortArray[$key][] = $value;
						    }
						}
						
						$orderby = "order_number"; //change this to whatever key you want from the array
						
						array_multisort($sortArray[$orderby],SORT_ASC,$menu_all); 

                        $row->menu_total = $menu_all_count;
                        $row->menu_list  = $menu_all;
                        
                    }
                    $results[] = $row;
                }
                return $results;
        }


        public function navigation_allow($apps_id, $geo_code) {
            $textquery = "
                    SELECT menu_id 
                    FROM  menu
                    WHERE  apps_id = '$apps_id'
                        AND allow_access not like  '%$geo_code%'
						AND parent_menu_id='0'
             ";
			$rows = $this->db->query($textquery)->result();
            return $rows;
        }

}