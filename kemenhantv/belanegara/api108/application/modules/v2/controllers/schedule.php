<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Schedule extends CI_Controller{
	
	function Schedule(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('schedule_model');
		$this->load->model('channel_model');
	}	
	
	
	
	
	function today(){
		/*
        $channel_id = $this->input->post("channel_id");
        $schedule_type_id = $this->input->post("schedule_type_id");
		
		$vid_server = $this->channel_model->channel_server_cluster($channel_id);
		$rtmp_addr = $vid_server->public_ip;

		$payload = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/RtmpClient/channel_schedule.php?rtmp_addr=" . $rtmp_addr . "&channel_id=" . $channel_id . "&schedule_type_id=" . $schedule_type_id);

        $this->output->set_content_type('application/json');
		$this->output->set_output($payload);
		*/
		
		
		
		date_default_timezone_set('America/New_York');
		
		$serverAddr = $_SERVER["HTTP_HOST"];
		$start_row = ($this->input->post("page") ? $this->input->post("page") : 1) - 1 ;
        $limit = $this->input->post("limit") ? $this->input->post("limit") : 10 ;

        $channel_id = $this->input->post("channel_id");
		$channel_type_id = $this->input->post("channel_type_id");
        $schedule_type_id = $this->input->post("schedule_type_id");
        $customer_id = $this->input->post("customer_id");
		
        $start_date = $this->input->post("start_date") ? $this->input->post("start_date") : date("Y-m-d H:i:s") ;
        $end_date = $this->input->post("end_date") ? $this->input->post("end_date") : date("Y-m-d H:i:s", strtotime ( '1 day', strtotime (date("Y-m-d H:i:s")))) ;

    	$rows = $this->schedule_model->schedule_today($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id, $channel_type_id);
		$response = array(
        	"schedule_list" => $rows
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
 	}
	
	
	function record(){
        $apps_id = $this->input->post("apps_id");
        $channel_id = $this->input->post("channel_id");
        $schedule_id = $this->input->post("schedule_id");
        $customer_id = $this->input->post("customer_id");
		
		$this->schedule_model->record($apps_id, $channel_id, $schedule_id, $customer_id);
		$response = array(
			"rcode"=>"1",
			"message"=>"Successfully"
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
	}
	
	
	function tvod_list(){
        $apps_id = $this->input->post("apps_id");
        $channel_id = $this->input->post("channel_id");
        $channel_type_id = $this->input->post("channel_type_id");
		$tvod_list = $this->schedule_model->tvod_list($apps_id, $channel_id, $channel_type_id);
		$response = array(
        	"tvod_list" => $tvod_list
        );

        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
		
	}
	
	
	function myschedule_list(){
		date_default_timezone_set('America/New_York');
		
		$serverAddr = $_SERVER["HTTP_HOST"];
		$start_row = ($this->input->post("page") ? $this->input->post("page") : 1) - 1 ;
        $limit = $this->input->post("limit") ? $this->input->post("limit") : 10 ;

        $channel_id = $this->input->post("channel_id");
        $schedule_type_id = $this->input->post("schedule_type_id");
        $customer_id = $this->input->post("customer_id");
		
        $start_date = $this->input->post("start_date") ? $this->input->post("start_date") : date("Y-m-d H:i:s") ;
        $end_date = $this->input->post("end_date") ? $this->input->post("end_date") : date("Y-m-d H:i:s", strtotime ( '1 day', strtotime (date("Y-m-d H:i:s")))) ;

    	$rows = $this->schedule_model->myschedule_list($channel_id, $schedule_type_id, $start_date, $end_date, $customer_id);
		$response = array(
        	"schedule_list" => $rows
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
		
 	}
	
	
/*
 	function schedule_detail(){
 		$schedule_id = $this->input->post("schedule_id");
 		$program_id = $this->input->post("program_id");
 		$channel_id = $this->input->post("channel_id");
 		$channel_type_id = $this->input->post("channel_type_id");
 		if($channel_type_id=="")
 			$channel_type_id = 1;
 	
 		$detail = $this->schedule_model->schedule_detail($schedule_id, $program_id, $channel_id, $channel_type_id);
 		$playlist = $this->schedule_model->program_content($detail->program_id);
 		$response = array(
 				"schedule_id" => $detail->schedule_id,
 				"start_date_time" => $detail->start_date_time,
 				"end_date_time" => $detail->end_date_time,
 				"schedule_title" => $detail->schedule_title,
 				"program_name" => $detail->program_name,
 				"description" => $detail->description,
 				"playlist" => $playlist
 		);
 	
 		$this->output->set_content_type('application/json');
 		$this->output->set_output(json_encode($response));
 	}
 	*/
 		
 		function schedule_detail(){
 			$schedule_id = $this->input->post("schedule_id");
 			$program_id = $this->input->post("program_id");
 			$content_id = $this->input->post("content_id");
 			$channel_id = $this->input->post("channel_id");
 			$channel_type_id = $this->input->post("channel_type_id");
 			if($channel_type_id=="")
 				$channel_type_id = 1;
 		
 			$detail = $this->schedule_model->schedule_detail($schedule_id, $channel_type_id);
 			$now_playing = $this->schedule_model->get_now_playing($schedule_id, $content_id);
 			$playlist = $this->schedule_model->program_contents($schedule_id, $content_id);
 			$response = array(
 					"schedule_id" => $detail->schedule_id,
 					"start_date_time" => $detail->start_date_time,
 					"end_date_time" => $detail->end_date_time,
 					"schedule_title" => $detail->schedule_title,
 					"program_name" => $detail->program_name,
 					"description" => $detail->description,
 					"now_playing" => $now_playing,
 					"playlist" => $playlist
 			);
 		
 			$this->output->set_content_type('application/json');
 			$this->output->set_output(json_encode($response));
 		
 		}


}
