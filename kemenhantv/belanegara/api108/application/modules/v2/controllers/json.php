<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Json extends CI_Controller {

    function Json() {
        parent::__construct();
        $this->load->model(array('menu_m'));
        $this->load->helper('url');
        $this->lang->load('default');
        $this->device_id = '2';
    }

    public function generate_channel_list() {
        $apps_id = $this->input->get("apps_id");

        //if (empty($apps_id)) $this->response_failed('missing_app_id');

        $menu_type = 'menu';
        $offset = 0;
        $limit = 10;
        $menu_id = '0';
        $sublimit = $limit;

        $menu_total = $this->menu_m->menu_count($apps_id, $this->device_id, $menu_id, 'menu');
        $menu_list = $this->menu_m->menu_list_all($apps_id, $this->device_id, $menu_id, 'menu', $limit, $offset, $sublimit);

        $product_total = 0;
        $product_list = array();
        if (!($menu_id == '0' && $menu_type == 'menu')) {
            $product_total = $this->menu_m->menu_count($apps_id, $this->device_id, $menu_id, 'program');
            $product_list = $this->menu_m->menu_list_all($apps_id, $this->device_id, $menu_id, 'program', $limit, $offset, $sublimit);
        }
        $message = 'json saved';
        $response_data = array(
            'rcode' => 1,
            'message' => $message,
            'limit' => $limit,
            'offset' => $offset,
            'menu_total' => $menu_total,
            'menu_list' => $menu_list,
            'product_total' => $product_total,
            'product_list' => $product_list
        );
        
        //$response = $this->response_success($response_data);
        $response = json_encode($response_data);
        $json_file = $this->config->item('generate_json_path') . $apps_id . '.json';
        $fp = fopen($json_file, 'w');
        fwrite($fp, $response);
        fclose($fp);

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode(array('rcode' => 1, 'message' => $message)));
    }


    public function execute_generate_json_navigation($apps_id) {
        if (empty($apps_id)) $apps_id = $this->input->get("apps_id");

        $menu_type = 'menu';
        $offset = 0;
        $limit = 10;
        $menu_id = '0';
        $sublimit = $limit;
        
        

        $menu_total = $this->menu_m->navigation_count($apps_id, $this->device_id, $menu_id, 'menu');
        $menu_list = $this->menu_m->navigation_list_all($apps_id, $this->device_id, $menu_id, 'menu', $limit, $offset, $sublimit);

        $message = 'Navigation JSON Saved';
        $response_data = array(
            'rcode' => 1,
            'message' => $message,
            'limit' => $limit,
            'offset' => $offset,
            'generated_date' => date('Y-m-d H:i:s'),
            'menu_total' => $menu_total,
            'menu_list' => $menu_list
        );

        $response = json_encode($response_data);
        $json_directory = $this->config->item('generate_json_path');
        $json_file = $json_directory . $apps_id . '-navigation.json';

        if (!file_exists($json_directory)) {
            mkdir($json_directory);
        }
        $fp = fopen($json_file, 'w');
        if ($fp) {
            fwrite($fp, $response);
            fclose($fp);
        }
        
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode(array('rcode' => 1, 'message' => $message)));
    }

}
