<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: nov, 17 2014
	Authentication Application
	Original copy from signup functions.
*/

class Ads extends REST_Controller{

	public function __construct(){
		parent::__construct();
    	$this->load->model(array('ads_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
	}	


	public function ads_list_post(){
		$apps_id = $this->input->post("apps_id");
		$channel_id = $this->input->post("channel_id");


		$response = array(
			'rcode' => 1,
			'ads_list' => $this->ads_m->ads_list($apps_id, $channel_id)
		);
		
		$this->response_success($response);

	}


}
