<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  Author: zola.octanoviar
  Date: nov, 17 2014
  Authentication Application
  Original copy from signup functions.
 */

class Menu extends REST_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('menu_m'));
        $this->load->helper('url');
        $this->lang->load('default');

        $this->apps_id = $this->input->post("apps_id");
        $this->lang = $this->input->post("lang");
        $this->customer_id = $this->input->post("customer_id");
        $this->device_id = $this->input->post("device_id");

        // checking params
        if (empty($this->apps_id))
            $this->response_failed('missing_app_id');
        if (empty($this->lang))
            $this->lang = $this->config->item('lang');
        if (empty($this->customer_id))
            $this->response_failed('missing_customer_id');
        if (empty($this->device_id))
            $this->response_failed('missing_device_id');
    }

    /**
     * List of Menu
     * method: POST
     * url: /menu_list
     * required parameters: apps_id, customer_id, version
     */
    public function menu_list_post() {
        $menu_id = $this->input->post("menu_id");
        $offset = $this->input->post("offset");
        $limit = $this->input->post("limit");

        if (!isset($offset)) {
            $offset = 0;
        }

        if (!isset($limit)) {
            $limit = 10;
        }

        if (empty($menu_id))
            $menu_id = '0';


        $menu_total = $this->menu_m->menu_count($this->apps_id, $this->device_id, $menu_id, 'menu');
        $total_included_items = $menu_total;
        $menu_list = $this->menu_m->menu_list_all($this->apps_id, $this->device_id, $menu_id, 'menu', $limit, $offset);

        $response = array(
            "menu_total" => $menu_total,
            "menu_list" => $menu_list
        );
        $this->response_success($response);
    }

    public function menu_list_new_post() {
        $menu_id = $this->input->post("menu_id");
        $menu_type = $this->input->post("menu_type");
        $offset = $this->input->post("offset");
        $limit = $this->input->post("limit");

        if (empty($offset)) {
            $offset = 0;
        }

        if (empty($limit)) {
            $limit = 10;
        }

        if (empty($menu_id)) {
            $menu_id = '0';
        }

        $menu_total = $this->menu_m->menu_count($this->apps_id, $this->device_id, $menu_id, $menu_type);
        $menu_list = $this->menu_m->menu_list_all($this->apps_id, $this->device_id, $menu_id, $menu_type, $limit, $offset);

        $response = array(
            "menu_total" => $menu_total,
            "menu_list" => $menu_list
        );
        $this->response_success($response);
    }

    public function category_list_post() {
        $menu_id = $this->input->post("menu_id");
        $menu_type = $this->input->post("menu_type");
        $offset = $this->input->post("offset");
        $limit = $this->input->post("limit");

        if (empty($offset)) {
            $offset = 0;
        }

        if (empty($limit)) {
            $limit = 10;
        }

        if (empty($menu_id)) {
            $menu_id = '0';
        }

        $menu_total = $this->menu_m->menu_count($this->apps_id, $this->device_id, $menu_id, $menu_type);
        $menu_list = $this->menu_m->menu_list_all($this->apps_id, $this->device_id, $menu_id, $menu_type, $limit, $offset);

        $response = array(
            "menu_total" => $menu_total,
            "menu_list" => $menu_list
        );
        $this->response_success($response);
    }

    public function channel_list_post() {
        $menu_id = $this->input->post("menu_id");
        $menu_type = $this->input->post("menu_type");
        $offset = $this->input->post("offset");
        $limit = $this->input->post("limit");
        $sublimit = $this->input->post("sublimit");

        if (empty($offset)) {
            $offset = 0;
        }

        if (empty($limit)) {
            $limit = 10;
        }

        if (empty($menu_id)) {
            $menu_id = '0';
        }

        if (empty($sublimit)) {
            $sublimit = $limit;
        }


        if ($offset == '0') {
            $jsonUrl = $this->config->item('generate_json_path') .$this->apps_id . '.json';
            if (file_exists($jsonUrl)) {
                $json = file_get_contents($jsonUrl);
                if ($json) {
                    $response = json_decode($json, TRUE);
                } else {
                    $response = $this->execute_generate_json($this->apps_id);
                }
            } else {
                $response = $this->execute_generate_json($this->apps_id);
            }
        } else {
            $menu_total = $this->menu_m->menu_count($this->apps_id, $this->device_id, $menu_id, 'menu');
            $menu_list = $this->menu_m->menu_list_all($this->apps_id, $this->device_id, $menu_id, 'menu', $limit, $offset, $sublimit);

            $product_total = 0;
            $product_list = array();
            if (!($menu_id == '0' && $menu_type == 'menu')) {
                $product_total = $this->menu_m->menu_count($this->apps_id, $this->device_id, $menu_id, 'program');
                $product_list = $this->menu_m->menu_list_all($this->apps_id, $this->device_id, $menu_id, 'program', $limit, $offset, $sublimit);
            }

            $response = array(
                "limit" => $limit,
                "offset" => $offset,
                "menu_total" => $menu_total,
                "menu_list" => $menu_list,
                "product_total" => $product_total,
                "product_list" => $product_list
            );
        }
        $this->response_success($response);
    }
    
    
    private function execute_generate_json($apps_id) {
        $menu_type = 'menu';
        $offset = 0;
        $limit = 10;
        $menu_id = '0';
        $sublimit = $limit;

        $menu_total = $this->menu_m->menu_count($apps_id, $this->device_id, $menu_id, 'menu');
        $menu_list = $this->menu_m->menu_list_all($apps_id, $this->device_id, $menu_id, 'menu', $limit, $offset, $sublimit);

        $product_total = 0;
        $product_list = array();
        if (!($menu_id == '0' && $menu_type == 'menu')) {
            $product_total = $this->menu_m->menu_count($apps_id, $this->device_id, $menu_id, 'program');
            $product_list = $this->menu_m->menu_list_all($apps_id, $this->device_id, $menu_id, 'program', $limit, $offset, $sublimit);
        }
        $message = 'json saved';
        $response_data = array(
            'rcode' => 1,
            'message' => $message,
            'limit' => $limit,
            'offset' => $offset,
            'menu_total' => $menu_total,
            'menu_list' => $menu_list,
            'product_total' => $product_total,
            'product_list' => $product_list
        );
        
        $response = json_encode($response_data);
        $json_directory = $this->config->item('generate_json_path');
        $json_file = $json_directory . $apps_id . '.json';
        if (!file_exists($json_directory)) {
            mkdir($json_directory);
        }
        $fp = fopen($json_file, 'w');
        if ($fp) {
            fwrite($fp, $response);
            fclose($fp);
        }
        
        return $response_data;
    }
    
    public function execute_generate_json_navigation($apps_id) {
        $menu_type = 'menu';
        $offset = 0;
        $limit = 10;
        $menu_id = '0';
        $sublimit = $limit;
        
        

        $menu_total = $this->menu_m->navigation_count($apps_id, $this->device_id, $menu_id, 'menu');
        $menu_list = $this->menu_m->navigation_list_all($apps_id, $this->device_id, $menu_id, 'menu', $limit, $offset, $sublimit);

        $message = 'json saved';
        $response_data = array(
            'rcode' => 1,
            'message' => $message,
            'limit' => $limit,
            'offset' => $offset,
            'generated_date' => date('Y-m-d H:i:s'),
            'menu_total' => $menu_total,
            'menu_list' => $menu_list
        );

        $response = json_encode($response_data);
        $json_directory = $this->config->item('generate_json_path');
        $json_file = $json_directory . $apps_id . '-navigation.json';

        if (!file_exists($json_directory)) {
            mkdir($json_directory);
        }
        $fp = fopen($json_file, 'w');
        if ($fp) {
            fwrite($fp, $response);
            fclose($fp);
        }
        
        return $response_data;
    }


    public function generate_json_menu_post()
    {
    	$response = $this->execute_generate_json($this->apps_id);
    	$this->response_success($response);
    }
 
 
     public function navigation_list_post() {
        $menu_id = $this->input->post("menu_id");
        $menu_type = $this->input->post("menu_type");
        $offset = $this->input->post("offset");
        $limit = $this->input->post("limit");
        $sublimit = $this->input->post("sublimit");

        if (empty($offset)) {
            $offset = 0;
        }

        if (empty($limit)) {
            $limit = 10;
        }

        if (empty($menu_id)) {
            $menu_id = '0';
        }

        if (empty($sublimit)) {
            $sublimit = $limit;
        }

        if ($offset == '0') {
            $jsonUrl = $this->config->item('generate_json_path') . $this->apps_id .'-navigation.json';

            if (file_exists($jsonUrl)) {
				//echo 'file exist';
                $json = file_get_contents($jsonUrl);
                if ($json) {
                    $response = json_decode($json, TRUE);
                } else {
                    $response = $this->execute_generate_json_navigation($this->apps_id);
                }
            } else {
                $response = $this->execute_generate_json_navigation($this->apps_id);
            }
        } else {
			$type='menu';
            if (!($menu_id == '0' && $menu_type == 'menu')) { //BUG ??? Saat Load More Menu menu_id!=0 && menu_type=='menu'. Dalam sub menu isinya menu
				$type='program';
            }
            
            //if ($menu_id != '0' && $menu_type != 'menu') { //BUG ??? Saat Load More Menu menu_id!=0 && menu_type=='menu'. Dalam sub menu isi nya product
            //	$type='program';
            //}

            //echo 'menu type ' . $menu_type . ' type '. $type . 'menu_id ' . $menu_id;
            
            $menu_total = $this->menu_m->navigation_count($this->apps_id, $this->device_id, $menu_id, $type);
            $menu_list = $this->menu_m->navigation_list_all($this->apps_id, $this->device_id, $menu_id, $type, $limit, $offset, $sublimit);

			/*
            $menu_total = $this->menu_m->navigation_count($this->apps_id, $this->device_id, $menu_id, 'menu');
            $menu_list = $this->menu_m->navigation_list_all($this->apps_id, $this->device_id, $menu_id, 'menu', $limit, $offset, $sublimit);

            $product_total = 0;
            $product_list = array();
            if (!($menu_id == '0' && $menu_type == 'menu')) {
                $menu_total = $this->menu_m->menu_count($this->apps_id, $this->device_id, $menu_id, 'program');
                $menu_list = $this->menu_m->menu_list_all($this->apps_id, $this->device_id, $menu_id, 'program', $limit, $offset, $sublimit);
            }
            $response = array(
                "limit" => $limit,
                "offset" => $offset,
                "menu_total" => $menu_total,
                "menu_list" => $menu_list,
                "product_total" => $product_total,
                "product_list" => $product_list
            );
            */

            $response = array(
                "limit" => $limit,
                "offset" => $offset,
                "menu_total" => $menu_total,
                "menu_list" => $menu_list
            );
        }
        $this->response_success($response);
    }
     
     public function navigation_allow_post() {
        $this->load->model(array('apps_m'));

        $apps_id = $this->input->post("apps_id");
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)){  //$_SERVER['HTTP_X_FORWARDED_FOR']){
			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		}else{
			$ip = $_SERVER["REMOTE_ADDR"];
		}
	        
		//CONTENT GEO TAG VALIDATION
		$geo = $this->apps_m->geoip($ip);
		$geo_code = 'XXX';
		if($geo){
			$geo_code = $geo->country_code;
		}
	    
	    $menu = $this->menu_m->navigation_allow($apps_id, $geo_code);
        $response_data = array('rcode' => 1,'menu_hide' => $menu);
    	$this->response_success($response_data);
	 }
}