<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: nov, 17 2014
	Authentication Application
	Original copy from signup functions.
*/

class Survey extends REST_Controller{

	public function __construct(){
		parent::__construct();
    	$this->load->model(array('survey_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
	}	


	public function survey_list_post(){
		$apps_id = $this->input->post("apps_id");
		$channel_id = $this->input->post("channel_id");
		$mobile_id = $this->input->post("mobile_id");


		$response = array(
			'rcode' => 1,
			'survey_list' => $this->survey_m->survey_list($apps_id, $channel_id, $mobile_id)
		);
		
		$this->response_success($response);

	}


	public function survey_save_post(){
		$survey_id = $this->input->post("survey_id");
		$mobile_id = $this->input->post("mobile_id");
		$quest_number = $this->input->post("quest_number");
		
    	$response['rcode'] = $this->survey_m->survey_save($survey_id, $mobile_id, $quest_number);
		if ($response['rcode']  == '0') {
			$response['message']  = 'Already participate';
		} else {
			$response['message']  = 'Thank you';
		}
		$this->response_success($response);

	}


}
