<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

    function Customer() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model(array('customer_m', 'apps_model', 'referral_m'));
        //$this->load->model('statistic_model');
        //$this->output->set_content_type('application/json');
    }

    function login() {
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $longitude = $this->input->post("longitude");
        $latitude = $this->input->post("latitude");

        $apps_id = $this->input->post("apps_id");
        $version = $this->input->post("version");
        $customer_id = $this->input->post("customer_id");
        $device_id = $this->input->post("device_id");
        $screen_size = $this->input->post("screen_size");
        $remote_host = $_SERVER['REMOTE_ADDR'];
        $referral = filter_input(INPUT_SERVER, 'HTTP_REFERER', FILTER_SANITIZE_STRING);
        $user_agent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_STRING);
//        $referral = (isset($_SERVER["HTTP_REFERER"])) ? $_SERVER["HTTP_REFERER"] : '';
//        $user_agent = (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $mobile_id = $this->input->post("mobile_id");

        //bawel auto login
        $bawel_login = $this->input->post("bawel_login");
        //army login
//        $army_login = $this->input->post("army_login");
        $army_login = $this->input->post("army_login");

        $status = 1;

        if (empty($longitude))
            $longitude = '0';
        if (empty($latitude))
            $latitude = '0';

        $customer = $this->customer_m->login($email, $password, $latitude, $longitude);

        if (!$customer) {
            $customer = array(
                "rcode" => 0,
                "message" => "Invalid combination usr/password"
            );
        } else {
            $customer->rcode = 1;
            $customer->message = "Successfully Login";

            if (($apps_id == 'com.balepoint.ibolz.diamond') || ($apps_id == 'com.balepoint.ibolz.mtn')) {
                if ($customer->verify_status == 0) {
                    $customer = array(
                        "rcode" => 0,
                        "message" => "Please verify your email"
                    );
                }
            }

            //login bawel user
            if ($bawel_login) {
                $customer->bawel_user = json_decode($this->_bawel_login($customer, $password));
            }

            //ARMY
            if ($army_login) {
                $params = array(
                    "customer_id" => $customer->customer_id,
                    "apps_id" => $apps_id,
                    //customer fields
                    'email' => $customer->email,
                    'user_id' => $customer->user_id,
                    'first_name' => (empty($customer->first_name) ? "" : $customer->first_name),
                    'last_name' => (empty($customer->last_name) ? "" : $customer->last_name),
                    'display_name' => (empty($customer->display_name) ? "" : $customer->display_name),
                    'mobile_no' => (empty($customer->mobile_no) ? "" : $customer->mobile_no),
                    'bod' => (empty($customer->bod) ? "" : $customer->bod),
                    'gender' => (empty($customer->gender) ? "" : $customer->gender),
                );

                $reference_user = null;

                $curl = new Curl();
                $ws_result = $curl->_simple_call('post', $this->config->item('army_url') . 'army/register_army', $params);
//                
//                $ws_result = $this->_exec_curl('post', $this->config->item('army_url') . 'army/register_army', $params);
                $ws_result_object = json_decode($ws_result);

                if ($ws_result_object != null && $ws_result_object->rcode != 0 && $ws_result_object->customer != null) {
                    $reference_user = $ws_result_object->customer->reference_user;
                }


                $customer->reference_user = $reference_user;
//                $customer->params = $params;
//                $customer->army_resp = $curl->debug();
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($customer));
        //$this->statistic_model->apps_activity('login', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
    }

    function signup() {
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $display_name = $this->input->post("name");
        $mobile_no = $this->input->post("mobile_no");

        $gender = $this->input->post("gender");
        $birthdate_s = $this->input->post("birthdate"); //dd-mm-yyyy

        $apps_id = $this->input->post("apps_id");
        $version = $this->input->post("version");
        $device_id = $this->input->post("device_id");
        $longitude = $this->input->post("longitude");
        $latitude = $this->input->post("latitude");

        $user_id = $email;

        $first_name = $this->input->post("first_name");
        $last_name = $this->input->post("last_name");
        $customer_id = $this->input->post("customer_id");
        $screen_size = $this->input->post("screen_size");
        $mobile_id = $this->input->post("mobile_id");

        $bawel_login = $this->input->post("bawel_login");
        $army_login = $this->input->post("army_login");
//        $army_login = 1;

        $remote_host = $_SERVER['REMOTE_ADDR'];


        if (empty($mobile_id))
            $mobile_id = 'webversion';
        if (empty($device_id))
            $device_id = '1';
        if (empty($first_name))
            $first_name = $display_name;
        if (empty($last_name))
            $last_name = $display_name;
        if (empty($longitude))
            $longitude = '0';
        if (empty($latitude))
            $latitude = '0';

        $status = 1;

        //validate
        if (empty($email) || empty($password)) {
            $customer = array(
                "rcode" => 0,
                "message" => "Failed. Arguments are mandatory : email, password"
            );
        } else {

            $customer = $this->customer_m->get_account_by_email($email);
            if ($customer) {
                $customer = array(
                    "rcode" => 0,
                    "message" => "Email already exists"
                );
            } else {
                $customer = $this->customer_m->signup($email, $mobile_id, $user_id, $password, $first_name, $last_name, $display_name, $status, $apps_id, $device_id, $longitude, $latitude, $mobile_no, $gender, $birthdate_s);
                $customer->rcode = 1;
                $customer->message = "Successfully Registered.";
                $verify = $this->customer_m->signup_create_verify($customer->customer_id);


                $apps = $this->apps_model->apps_detail($apps_id);
                $apps_name = ($apps) ? $apps->app_name : 'iBolz';

                //login bawel user
                if ($bawel_login) {
                    $customer->bawel_user = json_decode($this->_bawel_login($customer, $password));
                }

                //ARMY
                if ($army_login) {
                    $params = array(
                        "customer_id" => $customer->customer_id,
                        "apps_id" => $apps_id,
                        //customer fields
                        'email' => $customer->email,
                        'user_id' => $customer->user_id,
                        'first_name' => (empty($customer->first_name) ? "" : $customer->first_name),
                        'last_name' => (empty($customer->last_name) ? "" : $customer->last_name),
                        'display_name' => (empty($customer->display_name) ? "" : $customer->display_name),
                        'mobile_no' => (empty($customer->mobile_no) ? "" : $customer->mobile_no),
                        'bod' => (empty($customer->bod) ? "" : $customer->bod),
                        'gender' => (empty($customer->gender) ? "" : $customer->gender),
                    );

                    $curl = new Curl();
                    $ws_result = $curl->_simple_call('post', $this->config->item('army_url') . 'army/register_army', $params);

                    $ws_result_object = json_decode($ws_result);
                    $reference_user = null;
                    if ($ws_result_object != null && $ws_result_object->rcode != 0 && $ws_result_object->customer != null) {
                        $reference_user = $ws_result_object->customer->reference_user;
                    }

                    $customer->reference_user = $reference_user;
                }

                $mailer_url = $this->config->item('mailer_url') . 'send_verification';
                $mailer_data = array(
                    'apps_id' => $apps_id,
                    'apps_name' => $apps_name,
                    'name' => $customer->display_name,
                    'email' => $customer->email,
                    'url' => $this->config->item('apps_url') . 'customer/signup_verify?id=' . $customer->customer_id . '&vc=' . $verify->verify_code
                );
                $new_mailer_data = http_build_query($mailer_data, NULL, "&");

                $this->load->helper('ibolzmail_helper');
                exec_post_async(base_url('/mailer/send_verification_async'), $new_mailer_data);

//                $customer->mailer = modules::run('mailer/send_verification', $mailer_data);
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($customer));
        //$this->statistic_model->apps_activity('signup', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
    }

    function resend_email_verification() {
        $apps_id = $this->input->post("apps_id");
        $email = $this->input->post("email");

        $customer = $this->customer_m->get_account_by_email($email);
        if ($customer) {
            $verify = $this->customer_m->signup_create_verify($customer->customer_id);
            $apps = $this->apps_model->apps_detail($apps_id);
            $apps_name = ($apps) ? $apps->app_name : 'iBolz';


            $mailer_url = $this->config->item('mailer_url') . 'send_verification';
            $mailer_data = array(
                'apps_id' => $apps_id,
                'apps_name' => $apps_name,
                'name' => $customer->display_name,
                'email' => $customer->email,
                'url' => $this->config->item('apps_url') . 'customer/signup_verify?id=' . $customer->customer_id . '&vc=' . $verify->verify_code
            );

//            $customer->mailer = modules::run('mailer/send_verification_code', $mailer_data);

            $new_mailer_data = http_build_query($mailer_data, NULL, "&");

            $this->load->helper('ibolzmail_helper');
            exec_post_async(base_url('/mailer/send_verification_code_async'), $new_mailer_data);

            $customer = array(
                "rcode" => 1,
                "message" => "Please check your email, verification code send."
            );
        } else {
            $customer = array(
                "rcode" => 0,
                "message" => "Email not found"
            );
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($customer));
    }

    function change_password() {
        $customer_id = $this->input->post("customer_id");
        $password = $this->input->post("password");
        $new_password = $this->input->post("new_password");

        $customer = $this->customer_m->change_password($customer_id, $password, $new_password);
        if ($customer) {
            $customer = array(
                "rcode" => 1,
                "message" => "Updated"
            );
        } else {
            $customer = array(
                "rcode" => 0,
                "message" => "Failed"
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($customer));
        //$this->statistic_model->apps_activity('signup', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
    }

    function forget_password() {
        $apps_id = $this->input->post("apps_id");
        $email = $this->input->post("email");
        $customer = $this->customer_m->get_account_by_email($email);

        if ($customer) {
            //forgot password link email
            $respon = array(
                "rcode" => 1,
                "message" => "Check your email to reset your password"
            );
            //$customer->rcode = 1;
            //$customer->message = "Check your email to reset your password";

            $verify = $this->customer_m->forget_password_create_verify($email);

            $apps = $this->apps_model->apps_detail($apps_id);
            $apps_name = ($apps) ? $apps->app_name : 'iBolz';

            $mailer_url = $this->config->item('mailer_url') . 'send_forgot_password';
            $mailer_data = array(
                'apps_id' => $apps_id,
                'apps_name' => $apps_name,
                'name' => $customer->display_name,
                'email' => $customer->email,
                'url' => $this->config->item('apps_url') . 'customer/reset_password?id=' . $customer->customer_id . '&vc=' . $verify->verify_code
            );

//            $customer->mailer = modules::run('mailer/send_forgot_password', $mailer_data);
            
            $new_mailer_data = http_build_query($mailer_data, NULL, "&");

            $this->load->helper('ibolzmail_helper');
            exec_post_async(base_url('/mailer/send_forgot_password_async'), $new_mailer_data);

        } else {
            $respon = array(
                "rcode" => 0,
                "message" => "Can't find email account"
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($respon));
    }

    function profile() {
        $customer_id = $this->input->post("customer_id");
        $customer = $this->customer_m->profile($customer_id);

        if (!$customer) {
            $customer = array(
                "rcode" => 0,
                "message" => "Can not find customer"
            );
        } else {
            $customer->rcode = 1;
            $customer->message = "Successfully get profile";
            //set thumbnail
            if ($customer->filename) {
                $customer->thumbnail = "http://" . $_SERVER['SERVER_NAME'] . "/media/thumbnail_customer?id=" . $customer->filename;
            } else {
                $customer->thumbnail = "http://" . $_SERVER['SERVER_NAME'] . "/media/thumbnail_customer?id=" . $this->config->item('img_not_found');
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($customer));
    }

    function update_profile() {
        $customer_id = $this->input->post("customer_id");
        $display_name = $this->input->post("name");
        $email = $this->input->post("email");
        $mobile_no = $this->input->post("mobile_no");
        $gender = $this->input->post("gender");
        $bod = $this->input->post("birthdate");
        $first_name = $this->input->post("first_name");
        $last_name = $this->input->post("last_name");


        $customer = $this->customer_m->update_profile($customer_id, $display_name, $first_name, $last_name, $email, $mobile_no, $gender, $bod);
        if ($customer) {
            $customer = array(
                "rcode" => 1,
                "message" => "Updated"
            );
        } else {
            $customer = array(
                "rcode" => 0,
                "message" => "Failed"
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($customer));
        //$this->statistic_model->apps_activity('signup', $mobile_id, $apps_id, $version, $latitude, $longitude, $device_id, $customer_id, $screen_size, $remote_host, $status, json_encode($customer));
    }

    public function get_video_duration($filename) {
        $xyz = shell_exec($this->config->item('ffmpeg') . " -i \"{$filename}\"  2>&1");
        $search = '/Duration: (.*?),/';
        preg_match($search, $xyz, $matches);
        $explode = explode(':', $matches[1]);

        $hour = intval($explode[0]) * 3600;
        $minute = intval($explode[1]) * 60;
        $second = intval($explode[2]);
        $duration = $hour + $minute + $second;
        return $duration;
    }

    function upload_photo() {
        $customer_id = $this->input->post("customer_id");
        $target_path = $this->config->item('customer_media_path') . '/customer/thumbnails/';


        $response = array(
            "rcode" => 0,
            "message" => "Error on upload",
            "thumbnail" => "http://" . $_SERVER['SERVER_NAME'] . "/media/thumbnail_customer?id=" . $this->config->item('img_not_found')
        );
        $is_file = 0;

        if (isset($_FILES['file']['tmp_name']) && $_FILES['file']['tmp_name']) {
            $is_file = 1;
            if (!empty($_FILES['file']['error'])) {
                $file_error = $_FILES['file']['error'];
                switch ($file_error) {
                    case '1':
                    case '2':
                        $response['message'] = 'File too big to upload';
                        break;
                    case '3':
                        $response['message'] = 'File partially uploaded';
                        break;
                    case '4':
                        $response['message'] = 'No file was uploaded.';
                        break;
                    case '6':
                        $response['message'] = 'Missing a temporary folder';
                        break;
                    case '7':
                        $response['message'] = 'Failed to save file';
                        break;
                    default:
                        $response['message'] = 'Failed to upload, unknown error';
                        break;
                }
            }
        }


        if ($is_file) {

            $filename = basename($_FILES['file']['name']);
            $newFileName = uniqid(rand(), false) . substr($filename, strrpos($filename, ".", -2));
            $target_path = $target_path . $newFileName;
            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
                $this->customer_m->upload_photo($customer_id, $newFileName);
                $response = array(
                    "rcode" => 1,
                    "message" => "Successfully Uploaded",
                    "thumbnail" => "http://" . $_SERVER["SERVER_NAME"] . "/media/thumbnail_customer?id=" . $newFileName
                );
            } else {
                $response['rcode'] = "0";
                $response['message'] = "unable to copy file to server.";
            }
        } else {
            $response['rcode'] = "0";
            $response['message'] = "No file to upload.";
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function update_photo() {
        $customer_id = $this->input->post("customer_id");
        $filename = $this->input->post("filename");
        $response = array(
            "rcode" => 1,
            "message" => "Success"
        );


        $this->customer_m->update_photo($customer_id, $filename);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function upload_video() {
        $customer_id = $this->input->post("customer_id");
        $customer_category_id = $this->input->post("customer_category_id");
        $apps_id = $this->input->post("apps_id");
        $amount = $this->input->post("amount");


        $customer_path = $this->config->item('customer_media_path');
        $streaming_path = $customer_path . 'streaming/';
        $transcode_path = $this->config->item('customer_transcode_path');

        $response = array(
            "rcode" => 0,
            "message" => "Error on upload"
        );
        $is_file = 0;

        if (isset($_FILES['video']['tmp_name']) && $_FILES['video']['tmp_name']) {
            $is_file = 1;
            if (!empty($_FILES['video']['error'])) {
                $file_error = $_FILES['video']['error'];
                switch ($file_error) {
                    case '1':
                    case '2':
                        $response['message'] = 'File too big to upload';
                        break;
                    case '3':
                        $response['message'] = 'File partially uploaded';
                        break;
                    case '4':
                        $response['message'] = 'No file was uploaded.';
                        break;
                    case '6':
                        $response['message'] = 'Missing a temporary folder';
                        break;
                    case '7':
                        $response['message'] = 'Failed to save file';
                        break;
                    default:
                        $response['message'] = 'Failed to upload, unknown error';
                        break;
                }
            }
        }


        if ($is_file) {

            $filename = basename($_FILES['video']['name']);
            $file_info = (object) pathinfo($_FILES['video']['name']);
            $uniq_filename = uniqid(rand(), false) . '-' . $_FILES['video']['name'];
            $new_filename = (preg_replace('/[^a-zA-Z0-9-]/', '', strtoupper($customer_path == "" ? "" : $customer_path . "-") . $uniq_filename));
            $new_file = $transcode_path . $new_filename . '.' . $file_info->extension;

            if (move_uploaded_file($_FILES['video']['tmp_name'], $new_file)) {

                //$this->customer_m->upload_video($customer_id, $newFileName);
                $response = array(
                    "rcode" => 1,
                    "message" => "Successfully Uploaded"
                );


                //generate thumbnail
                $input_filename = $new_file;
                $thumbnail_script = $this->config->item('ffmpeg') . ' ' . $this->config->item('thumbnail_script');
                $thumbnail_script = str_replace('__input__', $input_filename, $thumbnail_script);
                $thumbnail_script = str_replace('__output__', ($this->config->item('thumbnail_path') . $new_filename . '.jpg'), $thumbnail_script);

                $xyz = shell_exec($thumbnail_script . '  2>&1');

                //get duration
                $duration = ($this->get_video_duration($new_file));

                $status = 10;
                $thumbnail = $new_filename . '.jpg';
                $filesize = filesize($input_filename);

                $this->load->model('content_model');
                $script_list = $this->content_model->script_file_list();
                $content_status = '0';
                $content = $this->content_model->content_save($customer_id, $filename, $filename, $new_filename, '9', $content_status, $duration, $thumbnail, '', '9');


                foreach ($script_list as $script) {
                    $device_id = $script->device_id;
                    $res_id = $script->res_id;
                    $script_file = $script->script_file;

                    $this->content_model->content_res_save($content->content_id, $res_id, $device_id, $input_filename, $content_status, $script_file, $filesize, 'file', $streaming_path);
                }
                // update content status = 10, waiting for transcode
                $this->content_model->content_status_update($content->content_id, '10');
                $this->content_model->content_vod_save($apps_id, '-2', $content->content_id, '', $amount, '');
                $allow_share = $this->customer_m->allow_share($customer_id, $customer_category_id);
                if ($allow_share) {
                    $this->content_model->content_customer_category_add($content->content_id, $customer_category_id);
                }
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "unable to copy file to server."
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => "No file to upload."
            );
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function my_video_list() {
        $this->load->model('content_model');

        $customer_id = $this->input->post("customer_id");
        $video_list = $this->content_model->content_my_list('', '', '', $customer_id, $list_type = 'my');
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($video_list));
    }

    function customer_history() {
        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");

        $history_list = $this->customer_m->customer_history($apps_id, $customer_id);
        $response = array(
            "history_list" => $history_list
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function transaction_history() {
        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");

        $transaction_list = $this->customer_m->transaction_history($apps_id, $customer_id);
        $response = array(
            "transaction_list" => $transaction_list
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function summary_transaction() {
        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");

        $transaction_list = $this->customer_m->summary_transaction($apps_id, $customer_id);
        $response = array(
            "transaction_list" => $transaction_list
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function mygroup_list() {
        $customer_id = $this->input->post("customer_id");

        $group_list = $this->customer_m->group_list($customer_id);
        $response = array(
            "group_list" => $group_list
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function remove_mygroups() {
        $customer_category_id = $this->input->post("customer_category_id");
        $customer_id = $this->input->post("customer_id");

        $group_list = $this->customer_m->remove_mygroups($customer_id, $customer_category_id);
        $response = array(
            "rcode" => $group_list,
            "rmessage" => "OK Lah"
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function add_mygroups() {
        $customer_category_id = $this->input->post("customer_category_id");
        $customer_id = $this->input->post("customer_id");

        $group_list = $this->customer_m->add_mygroups($customer_id, $customer_category_id);
        $response = array(
            "rcode" => $group_list,
            "rmessage" => "OK Lah"
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    function topup() {
        $customer_id = $this->input->post("customer_id");
        $amount = $this->input->post("amount");

        $amount = $this->customer_m->topup($customer_id, $amount);
        $response = array(
            "rcode" => 1,
            "rmessage" => $amount
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    private function _bawel_login($customer, $password) {

        $bawel_url = $this->config->item('bawel_url');
        $bawel_url_login = $this->config->item('bawel_url_login');

        if (empty($bawel_url) || empty($bawel_url_login)) {
            return null;
        }
        $bawel_login_url = 'http://' . $this->config->item('bawel_url') . $this->config->item('bawel_url_login');
        $login_data = array(
            'user_email' => $customer->email,
            'user_pass' => $password,
            'first_name' => $customer->first_name,
            'last_name' => $customer->last_name,
            'display_name' => $customer->display_name,
            'device_id' => time()
        );

        $this->load->library('curl');
        $this->curl->option('TIMEOUT', 180, 'OPT');
        $this->curl->option('FOLLOWLOCATION', FALSE, 'OPT');
        $this->curl->option('RETURNTRANSFER', TRUE, 'OPT');
        return $this->curl->simple_post($bawel_login_url, $login_data);
    }

}
