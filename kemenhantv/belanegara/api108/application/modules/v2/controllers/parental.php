<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: nov, 17 2014
	Authentication Application
	Original copy from signup functions.
*/

class Parental extends REST_Controller{

	public function __construct(){
		parent::__construct();
    	$this->load->model(array('parental_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
	}	


	public function parental_list_post(){
		$apps_id = $this->input->post("apps_id");
		$customer_id = $this->input->post("customer_id");

		$response = array(
			'rcode' => 1,
			'parental_list' => $this->parental_m->parental_list(),
			'parental_id' => $this->parental_m->parental_detail($apps_id, $customer_id)			
		);
		
		$this->response_success($response);

	}

	public function parental_save_post(){
		$apps_id = $this->input->post("apps_id");
		$customer_id = $this->input->post("customer_id");
		$parental_id = $this->input->post("parental_id");
		$message = $this->parental_m->parental_save($apps_id, $customer_id, $parental_id);
		$response = array(
			'rcode' => 1,
			'parental_list' => 'Updated'
		);
		
		$this->response_success($response);

	}


}
