<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Referral extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('referral_m', 'customer_m'));
        $this->load->helper('url');

        $this->language = "english";
        if ($this->input->post("lang") == "zh_CN") {
            $this->language = "chinese";
        } else if ($this->input->post("lang") == "in_ID") {
            $this->language = "indonesian";
        }
        $this->lang->load('default', $this->language);
    }

    function save_reference_code_post() {

        $apps_id = $this->input->post("apps_id");
        $mobile_id = $this->input->post("mobile_id");
        $referral_number = $this->input->post("referral_number");
        $encryption_key = $this->input->post("encryption_key");

        $customer_id = $this->input->post("customer_id");

        if (empty($apps_id) || empty($mobile_id) || empty($referral_number)) {
            $response = array(
                "rcode" => 0,
                "message" => "Failed. Following arguments are required : apps_id, mobile_id, referral_number"
            );
        } else {
            //check encryption key
            $server_encryption_key = MD5("kramat@123#" . $apps_id . "balepoint" . $mobile_id . "ibolz" . $referral_number . "amrizalsholding");
            
            if ($encryption_key == $server_encryption_key) {
                
                $existingReference = $this->referral_m->getReferenceLog($apps_id, $mobile_id, $referral_number);
                if ($existingReference) {
                    // log extist. do nothing
                    $response = array(
                        "rcode" => 0,
                        "message" => "Data already exist"
                    );
                } else {
                    //log doesn't exist. check active invitation
                    //validate active installed mobile id
                    $installedDevice = $this->referral_m->getInstalledMobileID($apps_id, $mobile_id);
                    if ($installedDevice) {
                        //check referral user
                        $referralUser = $this->referral_m->getReferralUser($referral_number);
                        if ($referralUser) {

                            //check if you insert the same army code as your code
                            //insert
                            $invitation = $this->referral_m->getReferralInvitation();
                            $invitation_id = "";
                            if ($invitation) {
                                $invitation_id = $invitation->invitation_id;
                            }

                            $log_id = substr(str_shuffle(MD5(microtime())), 0, 10);
                            $this->referral_m->saveReferenceLog($log_id, $apps_id, $mobile_id, $referral_number, $invitation_id, $customer_id);
                            $totalDownload = (int) $this->referral_m->countReferenceHistory($apps_id, $referral_number);
                            $totalPoint = 0;
                            if ($invitation) {
                                $totalPoint = (int) $totalDownload * (int) $invitation->reward_value;
                            }

                            $response = array(
                                "rcode" => 1,
                                "message" => "Saved successfuly"
                            );


                            //get the customer detail
                            $reference_customer = $this->customer_m->profile($referralUser->customer_id);

                            //send email notification
                            $mailer_url = $this->config->item('mailer_url') . 'send_referral_submition';
                            $mailer_data = array(
                                'apps_id' => $apps_id,
                                'customer_name' => $reference_customer->display_name,
                                'email' => $reference_customer->email,
                                'reference_code' => $referralUser->reference_number,
                                'total_download' => $totalDownload,
                                'total_point' => $totalPoint
                            );

                            modules::run('mailer/send_referral_submition', $mailer_data);
                        } else {
                            $response = array(
                                "rcode" => 0,
                                "message" => "Failed. Not a valid Army Code"
                            );
                        }
                    } else {
                        $response = array(
                            "rcode" => 0,
                            "message" => "Failed. Not a valid mobile ID, please install application first"
                        );
                    }
                }
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Failed. Invalid encryption key"
                );
            }
        }

        $this->response_success($response);
    }

    function register_referer_user_post() {

        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");

        //referral fields
        $id_type = $this->input->post("id_type");
        $id_number = $this->input->post("id_number");

        $bank_code = $this->input->post("bank_code");
        $bank_account_name = $this->input->post("bank_account_name");
        $bank_account_number = $this->input->post("bank_account_number");

        //customer fields
        $gender = $this->input->post("gender");
        $mobile_no = $this->input->post("mobile_no");
        $birthdate_s = $this->input->post("birthdate");   //dd-mm-yyyy
        $birthdate = null;
        if (!empty($birthdate_s)) {
            $birthdate = DateTime::createFromFormat('d-m-Y', $birthdate_s);
        }

        $response = array(
            "rcode" => 0,
            "message" => "Failed. Missing arguments"
        );

        //echo $real_name . $gender . $birthdate . $id_type . $id_number . $bank_number . $phone.$address. $email.$user_id.$mobile_id;

        if (empty($apps_id)) { // || empty($mobile_id) || empty($user_id) || empty($real_name)
            //|| empty($id_number) || empty($bank_number) || empty($phone))
            $response = array(
                "rcode" => 0,
                "message" => "Failed. Missing arguments"
            );
        } else if (empty($customer_id)) {
            $response = array(
                "rcode" => 0,
                "message" => "Failed. Customer ID not found"
            );
        } else if (empty($bank_code) || empty($bank_account_name) || empty($bank_account_number)) {

            $response = array(
                "rcode" => 0,
                "message" => "Failed. Please fill all the required fields : bank_code, bank_account_name, bank_account_number"
            );
        } else {
            //fetch customer
            $customer = $this->customer_m->profile($customer_id);
            if (!$customer) {
                $response = array(
                    "rcode" => 0,
                    "message" => "Failed. Customer not found"
                );
            } else {
                //update customer fields
                $customer->mobile_no = empty($mobile_no) ? $customer->mobile_no : $mobile_no;
                $customer->gender = empty($gender) ? $customer->gender : $gender;
                $customer->bod = empty($birthdate_s) ? $customer->bod : $birthdate->format("Y-m-d");
                $updatedCustomer = $this->customer_m->update_customer_profile($customer);

                $reference_number_suffix = $customer->mobile_no;
                $reference_number = $this->referral_m->generateReference(5);
                //check reference number
//                $referalUser = $this->referral_m->getReferralUser($reference_number);

                $customer_referral = $this->referral_m->getReferalUserByCustomerId($customer->customer_id);
                if ($customer_referral) {

                    $customer_referral->id_type = (empty($id_type) ? $customer_referral->id_type : $id_type);
                    $customer_referral->id_number = (empty($id_number) ? $customer_referral->id_number : $id_number);
                    $customer_referral->bank_code = (empty($bank_code) ? $customer_referral->bank_code : $bank_code);
                    $customer_referral->bank_account_number = (empty($bank_account_number) ? $customer_referral->bank_account_number : $bank_account_number);
                    $customer_referral->bank_account_name = (empty($bank_account_name) ? $customer_referral->bank_account_name : $bank_account_name);

                    $this->referral_m->updateReferralUser($customer_referral->customer_id, $customer_referral->id_type, $customer_referral->id_number, $customer_referral->bank_code, $customer_referral->bank_account_number, $customer_referral->bank_account_name);

                    $updatedCustomer->reference_user = $customer_referral;

                    $response = array(
                        "rcode" => 2,
                        "message" => "Success. Data has been updated",
                        "customer" => $updatedCustomer
                    );
                } else {

                    $this->referral_m->registerReferrerUser($customer->customer_id, $reference_number, $id_type, $id_number, $bank_code, $bank_account_number, $bank_account_name);

                    $customer_referral = $this->referral_m->getReferalUserByCustomerId($customer->customer_id);
                    $updatedCustomer->reference_user = $customer_referral;
                    $response = array(
                        "rcode" => 1,
                        "message" => "Saved successfuly",
                        "customer" => $updatedCustomer
                    );

                    //send email notification
                    $mailer_url = $this->config->item('mailer_url') . 'send_referral_register';
                    $mailer_data = array(
                        'apps_id' => $apps_id,
                        'customer_name' => $customer->display_name,
                        'email' => $customer->email,
                        'reference_code' => $reference_number
                    );

                    $customer->mailer = modules::run('mailer/send_referral_register', $mailer_data);
                }
            }
        }

        $this->response_success($response);
    }

    public function reference_history_post() {

        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");
        $reference_number = $this->input->post("reference_number");

        $limit = $this->input->post("limit");
        $offset = $this->input->post("offset");

        if (!$limit) {
            $limit = 0;
        }
        if (!$offset) {
            $offset = 0;
        }

        $response = array();


        $total_reward = 0;
        $totalTransfer = 0;
        $totalOutstanding = 0;

        if ($offset == 0) {
            $total_reward = $this->referral_m->sum_total_reward($apps_id, $reference_number);
            $totalTransfer = 0;
            $totalOutstanding = $this->referral_m->sum_total_reward($apps_id, $reference_number);
        }

        if (empty($apps_id) || empty($reference_number)) {
            $response = array(
                'rcode' => 0,
                'message' => 'Incomplete parameters. apps_id, reference_number'
            );
        } else {
            $reference_log = $this->referral_m->getReferenceHistory($apps_id, $reference_number, $limit, $offset);
            $reference_log_total = $this->referral_m->countReferenceHistory($apps_id, $reference_number);
            $response = array(
                'rcode' => 1,
                'message' => 'Succeed',
                'total_reward' => $total_reward,
                'total_transfer' => $totalTransfer,
                'total_outstanding' => $totalOutstanding,
                'reference_log_total' => $reference_log_total,
                'limit' => $limit,
                'offset' => $offset,
                'reference_log' => $reference_log
            );
        }

        $this->response_success($response);
    }

    public function introduction_post() {

        $introduction = $this->referral_m->getIntroduction($this->language);
        $bank_list = $this->referral_m->getBankList();
        $grandprize_list = $this->referral_m->getGrandPrizeList();

        $response = array(
            "rcode" => 1,
            "message" => "Succeed",
            "introduction" => $introduction,
            "bank_list" => $bank_list,
            "grandprize_list" => $grandprize_list
        );

        $this->response_success($response);
    }

}
