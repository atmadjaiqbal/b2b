<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: nov, 17 2014
	Authentication Application
	Original copy from signup functions.
*/

class Product extends REST_Controller{

	public function __construct(){
		parent::__construct();
    $this->load->model(array('product_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
		

		$this->apps_id = $this->input->post("apps_id");
		$this->lang = $this->input->post("lang");
		$this->customer_id = $this->input->post("customer_id");		
		$this->device_id = $this->input->post("device_id");		
				
		// checking params
    if(empty($this->apps_id)) $this->response_failed('missing_app_id');        
    if(empty($this->customer_id)) $this->response_failed('missing_customer_id');        
		if(empty($this->device_id)) $this->response_failed('missing_device_id');        
    if(empty($this->lang)) $this->lang =  'id_ID';        
		
	}	

	/**
	* List of Product
	* method: POST
	* url: /product_list
	* required parameters: apps_id, customer_id, menu_id
	*/
	public function product_list_post(){
		$menu_id = $this->input->post("menu_id");
		$device_id = $this->input->post("device_id");
				
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
		$genre_id = $this->input->post("genre_id") == "" ? "" : $this->input->post("genre_id");
		$crew_id = $this->input->post("crew_id") == "" ? "" : $this->input->post("crew_id");
		$search_key = $this->input->post("search_key") == "" ? "" : $this->input->post("search_key");
        $category = $this->input->post("categories_id") == "" ? "" : $this->input->post("categories_id");
		$totalitem = $this->input->post("totalitem") == "" ? "" : $this->input->post("totalitem");
		$cust_id = $this->input->post("customer_id") == "" ? null : $this->input->post("customer_id");

		$product_list = $this->product_m->product_list($page, $limit, $this->apps_id, $this->device_id, $menu_id, $genre_id, $crew_id, $search_key, $category, $cust_id);
		if($page == 0){
			//$totalitem = $this->product_m->count_product_list($page, $limit, $this->apps_id, $this->device_id, $menu_id, $genre_id, $crew_id, $search_key, $category);
			if($product_list && count($product_list)>0)
				$totalitem = $product_list[0]->totalitem;
		}
		$response = array(
			"product_list" => $product_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang,
				"menu_id" => $menu_id,
				"genre_id" => $genre_id,
				"crew_id" => $crew_id,
				"totalitem" => $totalitem,
				"search_key" => $search_key	
			)			
    	);
		$this->response_success($response);
	}


	public function content_list_post(){
		$menu_id = $this->input->post("menu_id");
		$device_id = $this->input->post("device_id");
				
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
		$genre_id = $this->input->post("genre_id") == "" ? "" : $this->input->post("genre_id");
		$crew_id = $this->input->post("crew_id") == "" ? "" : $this->input->post("crew_id");
		$search_key = $this->input->post("search_key") == "" ? "" : $this->input->post("search_key");
        $category = $this->input->post("categories_id") == "" ? "" : $this->input->post("categories_id");
		$totalitem = $this->input->post("totalitem") == "" ? "" : $this->input->post("totalitem");
		$cust_id = $this->input->post("customer_id") == "" ? null : $this->input->post("customer_id");

		$product_list = $this->product_m->content_list($page, $limit, $this->apps_id, $this->device_id, $menu_id, $genre_id, $crew_id, $search_key, $category, $cust_id);
		if($page == 0){
			$totalitem = $this->product_m->count_product_list($page, $limit, $this->apps_id, $this->device_id, $menu_id, $genre_id, $crew_id, $search_key, $category);
		}
		$response = array(
			"product_list" => $product_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang,
				"menu_id" => $menu_id,
				"genre_id" => $genre_id,
				"crew_id" => $crew_id,
				"totalitem" => $totalitem,
				"search_key" => $search_key	
			)			
    	);
		$this->response_success($response);
	}


	public function home_list_post(){
		$menu_id = $this->input->post("menu_id");
		$device_id = $this->input->post("device_id");
				
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
		$genre_id = $this->input->post("genre_id") == "" ? "" : $this->input->post("genre_id");
		$crew_id = $this->input->post("crew_id") == "" ? "" : $this->input->post("crew_id");
		$search_key = $this->input->post("search_key") == "" ? "" : $this->input->post("search_key");

		$product_list = $this->product_m->home_list($page, $limit, $this->apps_id, $this->device_id, $menu_id, $genre_id, $crew_id, $search_key);
		$response = array(
			"product_list" => $product_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang,
				"menu_id" => $menu_id,
				"genre_id" => $genre_id,
				"crew_id" => $crew_id,
				"search_key" => $search_key	
			)			
    );
		$this->response_success($response);
	}
	
	
	public function product_detail_post(){
    $menu_id = $this->input->post("menu_id");
    $product_id = $this->input->post("product_id");

		$product = $this->product_m->product_detail($this->apps_id, $menu_id, $product_id);

		if($product && $product->trailer_id){
			$product->trailer = $this->endpoint_m->product_detail('', '', $product->trailer_id);
		}
		$this->response_success($product);
	}	
	
	public function comment_list_post(){
		$product_id = $this->input->post("product_id");
		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$comment_list = $this->product_m->comment_list($page, $limit, $product_id);
		$response = array(
			"comment_list" => $comment_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"product_id" => $product_id
			)			
    );
		$this->response_success($response);
	}	
	
  public function comment_save_post ()  {
    $product_id = $this->input->post('product_id');
    $customer_id = $this->input->post('customer_id');
    $comment = $this->input->post('comment');
	if(empty($comment)){
		$comment = $this->input->post('comment_text');
	}
		$comment_text = addslashes($comment);

    $response['comment_list'] = $this->product_m->comment_save($product_id, $customer_id, $comment_text);
		$this->response_success($response);
  }

    public function comment_total_post() {
        $apps_id = $this->input->post('apps_id');
        $product_id = $this->input->post('product_id');
        $totalcomm = $this->product_m->comment_total($product_id);
        $response = array(
            "comment_total" => $totalcomm
        );
        $this->response_success($response);
    }

    public function like_list_post(){
		$product_id = $this->input->post("product_id");
		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$comment_list = $this->product_m->like_list($page, $limit, $product_id);
		$response = array(
			"like_list" => $comment_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"product_id" => $product_id
			)			
    );
		$this->response_success($response);
	}	
	
  public function like_save_post ()  {
    $apps_id = $this->input->post('apps_id');
    $product_id = $this->input->post('product_id');
    $customer_id = $this->input->post('customer_id');

    $response = $this->product_m->like_save($product_id, $customer_id);
		if ($response['rcode']  == '0') {
			$response['message']  = 'Already like product';
		} else {
			$response['message']  = 'Thank you';
		}
		$this->response_success($response);
  }

  public function like_total_post() {
      $apps_id = $this->input->post('apps_id');
      $product_id = $this->input->post('product_id');
      $totalllike = $this->product_m->total_like($product_id);
      $response = array(
          "like_total" => $totalllike
          );
      $this->response_success($response);
  }

	public function unlike_list_post(){
		$product_id = $this->input->post("product_id");
		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$comment_list = $this->product_m->unlike_list($page, $limit, $product_id);
		$response = array(
			"unlike_list" => $comment_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"product_id" => $product_id
			)			
    );
		$this->response_success($response);
	}	
	
  public function unlike_save_post ()  {
    $apps_id = $this->input->post('apps_id');
    $product_id = $this->input->post('product_id');
    $customer_id = $this->input->post('customer_id');

    $response = $this->product_m->unlike_save($product_id, $customer_id);
		if ($response['rcode']  == '0') {
			$response['message']  = 'Already unlike product';
		} else {
			$response['message']  = 'Thank you';
		}
		$this->response_success($response);
  }	
	
	public function channel_list_post(){
		$channel_category_id = $this->input->post("channel_category_id");
				
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$product_list = $this->product_m->channel_list($page, $limit, $this->apps_id, $channel_category_id);
		$channel_category_list = $this->product_m->channel_category_list($this->apps_id);
        $channel_total = $this->product_m->channel_total($this->apps_id);
		$response = array(
			"product_list" => $product_list,
			"channel_category_list" => $channel_category_list,
            "channel_total" => $channel_total,
            "filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang,
				"channel_category_id" => $channel_category_id
			)			
    );
		$this->response_success($response);
	}


	public function genre_list_post(){
		
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$genre_list = $this->product_m->genre_list($page, $limit);
		$response = array(
			"product_list" => $genre_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang
			)			
    );
		$this->response_success($response);
	}

	public function boxoffice_list_post(){
		$menu_id = $this->input->post("menu_id");
		$device_id = $this->input->post("device_id");
				
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$product_list = $this->product_m->boxoffice_list($page, $limit, $this->apps_id, $this->device_id);
		$response = array(
			"product_list" => $product_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang
			)			
    );
		$this->response_success($response);
	}

    public function product_popular_post(){
        $menu_id = $this->input->post("menu_id");
        $device_id = $this->input->post("device_id");
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
        $limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));
        $popular = $this->input->post("sort_by");
        $product_list = $this->product_m->product_popular($page, $limit, $this->apps_id, $this->device_id, $popular);
        $response = array(
            "product_list" => $product_list,
            "filter"=> array(
                "page" => $page,
                "limit" => $limit,
                "lang" => $this->lang
            )
        );
        $this->response_success($response);
    }

    public function product_categories_post() {
        $categories_list = $this->product_m->product_categories($this->apps_id);
        $response = array("categories_list" => $categories_list);
        $this->response_success($response);
    }

    public function product_bycontentid_post(){
        $menu_id = $this->input->post("menu_id");
        $device_id = $this->input->post("device_id");
        $content_id = $this->input->post("content_id") == "" ? "" : $this->input->post("content_id");

        $product_list = $this->product_m->product_bycontentid($this->apps_id, $this->device_id, $content_id);
        $response = array(
            "product_list" => $product_list,
        );
        $this->response_success($response);
    }
	
	
  	public function fav_save_post ()  {
    	$apps_id = $this->input->post('apps_id');
    	$product_id = $this->input->post('product_id');
    	$customer_id = $this->input->post('customer_id');

    	$response['rcode'] = $this->product_m->fav_save($apps_id, $product_id, $customer_id);
		
		if ($response['rcode']  == '0') {
			$response['message']  = 'Already save favorite';
		} else {
			$response['message']  = 'Thank you';
		}
		
		$this->response_success($response);
  	}
	
	
  	public function fav_remove_post ()  {
    	$apps_id = $this->input->post('apps_id');
    	$product_id = $this->input->post('product_id');
    	$customer_id = $this->input->post('customer_id');

    	$response['rcode'] = $this->product_m->fav_remove($apps_id, $product_id, $customer_id);
		
		if ($response['rcode']  == '0') {
			$response['message']  = 'Already remove favorite';
		} else {
			$response['message']  = 'Thank you';
		}
		
		$this->response_success($response);
  	}
	
  	public function fav_list_post ()  {
    	$apps_id = $this->input->post('apps_id');
    	$customer_id = $this->input->post('customer_id');

		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$fav_list = $this->product_m->fav_list($page, $limit, $apps_id, $customer_id);
		$response = array(
			"fav_list" => $fav_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit
			)			
    	);
		$this->response_success($response);
  	}
	
	
	public function layang_list_post(){
        $apps_id = $this->input->post("apps_id") == "" ? null : $this->input->post("apps_id");
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 12 : $this->input->post("limit"));

		$product_list = $this->product_m->layang_list($page, $limit, $apps_id);

		$response = array(
			"product_list" => $product_list,
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang	
			)			
    	);
		$this->response_success($response);
	}

    public function check_country_permit_post()
    {
        $apps_id = $this->input->post('apps_id');
        $product_id = $this->input->post('product_id');
        $ipfrom = $this->input->post('ip_from');

        $geoipRes = $this->product_m->get_geoip($ipfrom);

        if($geoipRes)
        {
            $ipfrom = ''; $ipto = ''; $iplongfrom = '';
            $iplongto = ''; $country_code = ''; $country_name = '';
            foreach($geoipRes as $val)
            {
                $ipfrom = $val->ipfrom;
                $ipto = $val->ipto;
                $iplongfrom = $val->iplongfrom;
                $iplongto = $val->iplongto;
                $country_code = $val->country_code;
                $country_name = $val->country_name;
            }

            $getContent = $this->product_m->get_content_bycountry($product_id, $country_name);
            if($getContent) {
                $response = array(
                    "check_country" => 1
                );
            } else {
                $response = array(
                    "check_country" => 0
                );
            }

        } else {
            $response = array(
                "check_country" => 0
            );
        }

        $this->response_success($response);
    }


	
}


