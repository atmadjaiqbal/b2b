<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Services extends CI_Controller{
	
	function Services(){
		// error_reporting(0);
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('apps_model');
		$this->load->model('services_m');
		//$this->output->set_content_type('application/json');
	}


  	function weather(){

		$latitude = $this->input->post("content_latitude");
		$longitude = $this->input->post("content_longitude");
	    
	    $apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";
	    $status = 1;

		if (empty($latitude))  $latitude = '0'; else $latitude = number_format($latitude, 12); //substr($latitude, 0, -2);
		if (empty($longitude))  $longitude = '0'; else $longitude = number_format($longitude, 12); //substr($longitude, 0, -2);

		$apikeyparam	= "&APPID=facc677cb16517d935298d04557560b3";
		$urlWeatherMap  = "http://api.openweathermap.org/data/2.5/weather?lat=".$latitude."&lon=".$longitude;
		
	  	$weather = $this->services_m->getweather($latitude, $longitude);
	  	// echo 'echo 1';//var_export($weather);
		if(!$weather || $weather->minutediff >= 11){
			// echo 'echo 2';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $urlWeatherMap);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 3600
			curl_setopt($ch, CURLOPT_POST, true);
			$response = curl_exec($ch);
			$res_code = curl_errno($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);
			
			if ($info['http_code'] == 200) {
				$data = json_decode($response, true);
				// echo $response;
			// $result = file_get_contents($urlWeatherMap);

			// if($result != ''){ //Sukses
			// 	$data = json_decode($result, true);
				// var_export($data);
				$suhu = ( isset($data['main']['temp']) ? $data['main']['temp'] : 0 );
				$suhu = (empty($suhu) ? 0 : $suhu - 273.15); //Kurangi 273.15 untuk merubah ke Celcius
				// $cuaca = $data['weather'][0]['main'].' - '.$data['weather'][0]['description']; //Cuaca
				// $cuaca = $data['weather'][0]['main']; //Cuaca
				$cuaca = ( isset($data['weather'][0]['main']) ? $data['weather'][0]['main'] : '' ); //Cuaca
				//$city = $data['name'];

				$weather = $this->services_m->setweather($latitude, $longitude, $cuaca, $suhu);
			}
		}
		// var_export($weather);//echo $weather->cuaca;die;

		$weather->cuaca = $this->services_m->getdictionary($weather->cuaca, $lang);
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($weather));
  	}

  	function fids(){

		// $latitude = $this->input->post("content_latitude");
		// $longitude = $this->input->post("content_longitude");
		$apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";
	    $status = 1;

		$airportcode = $this->input->post("airportcode");

		$airportcode = (empty($airportcode) ? 'BPN' : $airportcode );

		$ap2airport = array('CGK', 'PDG', 'KNO');
		// $rooturl	= 'http://localhost/ibolzid/poi/jadwalbandara/';
		$rooturl	= 'http://172.16.9.113/poi-testing/jadwalbandara/';

		$fidsscrap = $rooturl.'autoscrap.php'; // Website Bandara Angkasa Pura 1

		if(in_array($airportcode, $ap2airport)){
		    $fidsscrap = $rooturl.'autoscrapap2.php'; // Website Bandara Angkasa Pura 2
		}

		$lastupdatetime = $this->services_m->getfids_lastupdatetime($airportcode);
	  	// var_export($lastupdatetime);
		// if(!$lastupdatetime || $lastupdatetime->minutediff >= 5){
		if(!$lastupdatetime || $lastupdatetime->minutediff == NULL || $lastupdatetime->minutediff >= 5){

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $fidsscrap);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 3600
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "airportcode=".$airportcode);
			$response = curl_exec($ch);
			$res_code = curl_errno($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);
			
			// if ($info['http_code'] == 200) {
			// 	$data = json_decode($response, true);
			// 	var_export($data);
			// 	echo $data['status'].' -=- '.$data['message'];
			// }
	    }
	    
		$fidstoday = $this->services_m->getfidstoday($airportcode);
	  	
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($fidstoday));
  	}

  	function infopenting_ntmc(){
  		$apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";

		$add_param = $this->input->post("addParam");

	    if(empty($add_param)){

	    	// $rooturl	= 'http://localhost/ibolzid/poi/';
			$rooturl	= 'http://172.16.9.113/poi-testing/';

			$services_url = $rooturl . "channelnomorpenting.php";

	    	$postfield = "apps_id=".$apps_id;

	    	// $result = file_get_contents($services_url."?".$postfield);
	    	// $infopenting = $result;

	  		$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $services_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 3600
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postfield);
			$response = curl_exec($ch);
			$res_code = curl_errno($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);
			
			if ($info['http_code'] == 200) {
				$channel_id = $response;
			}

	    }else{
	    	$channel_id = $add_param;
	    }

	    $nopenting = $this->services_m->getnomorpenting($channel_id);

	    if(count($nopenting) == 0){ // Kalau hasil kosong, pakai nomor penting dr NTMC
	    	$default_channel = '201714251655a3c93aca90e';
	    	$nopenting = $this->services_m->getnomorpenting($default_channel);
	    }

	   	$infopenting = json_encode($nopenting);

	    $this->output->set_content_type('application/json');
	    $this->output->set_output($infopenting);
  	}

  	function get_listlaporan(){
  		$apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";

  		$laporan =
		'[	
			{"id" : 1, "mode" : "umum", "date" : "28/07/15", "title" : "Jalan Berlubang di Jatiwaringin, Bekasi", "nama" : "Ardi", "lokasi" : "BEKASI", "email": "", "phone": "088881234567", "content":"Jl. Raya Jatiwaringin masih banyak yang berlubang dan membahayakan pengguna jalan", "pro" : 789, "cons" : 2, "status" : "aktif"},
			{"id" : 2, "mode" : "umum", "date" : "28/07/15", "title" : "Sampah 2 Bulan tidak diangkut", "nama" : "Sofyan", "lokasi" : "JAKARTA", "email": "sofyan@email.id", "phone": "", "content":"Sampah di Jl. Garuda 5 sudah 2 bulan tidak diangkut, sehingga menimbulkan bau, juga jadi sarang penyakit", "pro" : 92, "cons" : 0, "status" : "aktif"},
			{"id" : 3, "mode" : "umum", "date" : "30/07/15", "title" : "Lampu Jalanan di Jl. Antasari sangat minim", "nama" : "Jeje", "lokasi" : "JAKARTA", "email": "", "phone": "088881234567", "content":"Lampu penerangan di Jl. Antasari sangat sedikit, sehingga berbahaya jika sudah malam", "pro" : 989, "cons" : 21, "status" : "aktif"},
			{"id" : 4, "mode" : "umum", "date" : "29/07/15", "title" : "BPJS tidak bisa digunakan di RSUD Selalu Sehat", "nama" : "Jojo", "lokasi" : "JAKARTA", "email": "", "phone": "0881231212", "content":"Saya adalah peserta BPJS Kesehatan kelas 3, yang memerlukan rawat inap, dan di Rujuk ke RSUD Selalu Sehat, tapi yang terjadi ketika sampai disana, saya di tolak dengan alasan BPJS tidak berlaku disini. Mohon ditindak lanjuti", "pro" : 12, "cons" : 23, "status" : "aktif"},
			{"id" : 5, "mode" : "anonymous", "date" : "30/07/15", "title" : "Iuran Sekolah Memberatkan", "nama" : "Jono", "lokasi" : "BOGOR", "email": "", "phone": "0888123912", "content":"Iuran Sekolah di SMUN 9877 sangat memberatkan, terdapat biaya-biaya yang menurut saya tidak diperlukan, seperti perawatan taman sekolah yang mencapai 50rb per siswa setiap bulan nya", "pro" : 345, "cons" : 4, "status" : "aktif"},
			{"id" : 6, "mode" : "anonymous", "date" : "29/07/15", "title" : "Biaya Siluman Pembuatan Akta Tanah", "nama" : "Jeni", "lokasi" : "JAKARTA", "email": "", "phone": "0888123123", "content":"Saya berniat membuat akta Tanah tapi selalu ditolak dengan alasan kurang lengkap, tapi selalu di arahkan untuk melalui jalur singkat dengan membayar sejumlah biaya siluman kalau Akta Tanah saya mau dibuat", "pro" : 314, "cons" : 23, "status" : "aktif"},
			{"id" : 7, "mode" : "anonymous", "date" : "30/07/15", "title" : "Pelayanan Kelurahan Antah Berantah buruk", "nama" : "Juju", "lokasi" : "DEPOK", "email": "", "phone": "0888812312312", "content":"Pelayanan di Kelurahan Antah Berantah sangat buruk, pembuatan KTP berlarut-larut, sampai 2 minggu belum selesai, Bagaimana ini ?", "pro" : 13, "cons" : 23, "status" : "aktif"},
			{"id" : 8, "mode" : "anonymous", "date" : "28/07/15", "title" : "Pungli di SMA Favorit", "nama" : "Badri", "lokasi" : "JAKARTA", "email": "joyan@email.id", "phone": "088812881", "content":"Pungli terjadi di SMA FAVORIT, siswa di bebankan dana OSIS dan lain-lain yang mencapai 980rb", "pro" : 543, "cons" : 12, "status" : "aktif"},
			{"id" : 9, "mode" : "umum", "date" : "30/07/15", "title" : "Lampu Jalanan di Jl. Antasari sangat minim", "nama" : "Jeje", "lokasi" : "JAKARTA", "email": "", "phone": "088881234567", "content":"Lampu penerangan di Jl. Antasari sangat sedikit, sehingga berbahaya jika sudah malam", "pro" : 989, "cons" : 21, "status" : "aktif"},
			{"id" : 10, "mode" : "anonymous", "date" : "29/07/15", "title" : "Biaya Siluman Pembuatan Akta Tanah", "nama" : "Jeni", "lokasi" : "JAKARTA", "email": "", "phone": "0888123123", "content":"Saya berniat membuat akta Tanah tapi selalu ditolak dengan alasan kurang lengkap, tapi selalu di arahkan untuk melalui jalur singkat dengan membayar sejumlah biaya siluman kalau Akta Tanah saya mau dibuat", "pro" : 314, "cons" : 23, "status" : "aktif"},
			{"id" : 11, "mode" : "anonymous", "date" : "30/07/15", "title" : "Iuran Sekolah Memberatkan", "nama" : "Jono", "lokasi" : "BOGOR", "email": "", "phone": "0888123912", "content":"Iuran Sekolah di SMUN 9877 sangat memberatkan, terdapat biaya-biaya yang menurut saya tidak diperlukan, seperti perawatan taman sekolah yang mencapai 50rb per siswa setiap bulan nya", "pro" : 345, "cons" : 4, "status" : "aktif"}
		]';

		$this->output->set_content_type('application/json');
		$this->output->set_output(trim($laporan));
  	}

  	function get_listdata(){
  		$apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";
	    
	    $add_param = $this->input->post("addParam");
	    // $detail = explode("&", $add_param);
	    
	    // $param1 = count($detail) >0 ? $detail[0]: "";
	    // $param2 = count($detail) >1 ? $detail[1]: "";
	    
	    // | no | tgl | title | Lembaga | status | pro | cons | detail |
  		/*
  		$data =
		'{	
			"header" : [
						{"key" : "id", "title" : "No", "width" : 0.05, "type" : "text"},
						{"key" : "date", "title" : "Tanggal", "width" : 0.05, "type" : "text"},
						{"key" : "title", "title" : "Nama", "width" : 0.3, "type" : "text"},
						{"key" : "lembaga", "title" : "Lembaga", "width" : 0.2, "type" : "text"},
						{"key" : "keterangan", "title" : "Keterangan", "width" : 0.2, "type" : "text"},
						{"key" : "pro", "title" : "Pro", "width" : 0.05, "type" : "button"},
						{"key" : "cons", "title" : "Cons", "width" : 0.05, "type" : "button"},
						{"key" : "urllink", "title" : "Detail", "width" : 0.1, "type" : "webview"}
					  ],
			"data" : [
				{"id" : 1, "date" : "09/02/15", "title" : "RUU tentang Perubahan atas UU No. 32 Tahun 2002 tentang Penyiaran", "lembaga" : "Komisi I DPR", "keterangan" : "Pembicaraan Tk. I, Ada NA+RUU", "status" : "Proses", "pro" : 234, "cons" : 2, "urllink" : "http://www.dpr.go.id/prolegnas/index/id/1"},
				{"id" : 2, "date" : "01/06/15", "title" : "RUU tentang Radio Televisi Republik Indonesia", "lembaga" : "Komisi I DPR", "keterangan" : "Periode 2009-2014, Usul DPR, Ada NA+RUU", "status" : "Proses", "pro" : 874, "cons" : 12, "urllink" : "http://www.dpr.go.id/prolegnas/index/id/2"},
				{"id" : 3, "date" : "06/12/14", "title" : "RUU tentang Perubahan atas Undang-Undang No.11 Tahun 2008 tentang Informasi dan Transaksi Elektronik (ITE)", "lembaga" : "Kementerian Komunikasi dan Informatika", "keterangan" : "Prolegnas 2010-2014, Ada NA+RUU", "status" : "Tinjauan", "pro" : 724, "cons" : 15, "urllink" : "http://www.dpr.go.id/prolegnas/index/id/3"},
				{"id" : 4, "date" : "17/02/15", "title" : "RUU tentang Wawasan Nusantara", "lembaga" : "PPUU DPD", "keterangan" : "Sudah ada NA, RUU sedang proses", "status" : "Selesai", "pro" : 970, "cons" : 7, "urllink" : "http://www.dpr.go.id/prolegnas/index/id/4"},
				{"id" : 5, "date" : "19/05/15", "title" : "RUU tentang Pertanahan", "lembaga" : "Komisi II DPR", "keterangan" : "Pembicaraan Tk. I, Ada NA+RUU", "status" : "Selesai", "pro" : 789, "cons" : 3, "urllink" : "http://www.dpr.go.id/prolegnas/index/id/5"}
			]
		}
		';
		*/

	    // $rooturl	= 'http://localhost/ibolzid/poi/';
		$rooturl	= 'http://172.16.9.113/poi-testing/';

		$services_url = $rooturl . "channeltabeldata.php";

    	$postfield = "addParam=".urlencode($add_param)."&apps_id=$apps_id&customer_id=$customer_id";

    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $services_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 3600
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfield);
		$response = curl_exec($ch);
		$res_code = curl_errno($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		
		if ($info['http_code'] == 200) {
			$data = $response;
		}
		
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(trim($data));
  	}

  	function get_datakartu(){
  		$apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";
	    // | no | tgl | title | Lembaga | status | pro | cons | detail |

	    $kartu = $this->input->post("addParam");
	    $return = "";
		$kartu = (empty($kartu) ? 'kip' : $kartu );
  		
		$datas =
		'{	
			"kis" : {"image_depan" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/kis_depan.png", "image_belakang" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/kis_belakang.png", "qrcode" : "", "isregister" : 1, "image_qrcode" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/qrcode_kartu.png",
						"data" : {"no_kartu" : "0398602.18749", "nama" : "Yusuf Indradjaya", "tgl_lahir" : "18/09/79", "alamat" : "Sawo II/17 V, RT 003/02"} },
			"kip" : {"image_depan" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/kip_depan.png", "image_belakang" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/kip_belakang.png", "qrcode" : "", "isregister" : 1, "image_qrcode" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/qrcode_kartu.png",
						"data" : {"no_kartu" : "24536.1536", "nama" : "Budi Indradjaya", "tgl_lahir" : "20/09/03", "alamat" : "Sawo II/17 V, RT 003/02"} },
			"kks" : {"image_depan" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/kks_depan.png", "image_belakang" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/kks_belakang.png", "qrcode" : "", "isregister" : 0, "image_qrcode" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/qrcode_kartu.png",
						"data" : {"no_kartu" : "4605.058883", "nama" : "Yusuf Indradjaya", "tgl_lahir" : "18/09/79", "alamat" : "Sawo II/17 V, RT 003/02"} },
			"ksks" : {"image_depan" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/ksks_depan.png", "image_belakang" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/ksks_belakang.png", "qrcode" : "", "isregister" : 0, "image_qrcode" : "http://tmb001.3d.ibolztv.net/assets/images/jtv/qrcode_kartu.png",
						"data" : {"no_kartu" : "4605.058883", "nama" : "Yusuf Indradjaya", "tgl_lahir" : "18/09/79", "alamat" : "Sawo II/17 V, RT 003/02"} }
		}';

		$data = json_decode($datas, TRUE);

		$return = json_encode($data[$kartu]);

		$this->output->set_content_type('application/json');
		$this->output->set_output(trim($return));
  	}

  	function tabel_cobahtml(){
  		// $this->output->set_content_type('text/html');
  		// echo '<pre>';
    //     echo $_SERVER['HTTP_USER_AGENT'] . "<br />==========<br />";

    //     $browser = get_browser(null, true);
        
    //     print_r($browser);
    //     echo '</pre>';
  		echo '<html>
			<head>
			<title>test table</title>
			</head>
			<Body>
			<a href="com.balepoint.ibolz.ntmc.prod://path?546326089555aba40de30a;3;Rambo"> Rambo </a>
			<br />
			<table border="1" cellpadding="5" cellspacing="0">
			  <tr>
			    <th>No</th>
			    <th>Nama RUU</th>
			    <th>Pengusul Prioritas</th>
			    <th>Keterangan</th>
			    <th>Link</th>
			    <th>Call</th>
			    <th>SMS</th>
			  </tr>
			  <tr>
			    <td>1</td>
			    <td>RUU tentang Perubahan atas UU No. 32 Tahun 2002 tentang Penyiaran</td>
			    <td>Komisi I DPR</td>
			    <td>Pembicaraan Tk. I, Ada NA+RUU</td>
			    <td><a href="http://id.ibolz.tv">id.ibolz.tv</a></td>
			    <td>call <a href="tel:+6285782221361">+62857-8222-1361</a></td>
			    <td>sms 6285782221361 <a href="sms:+6285782221361?body=hello">Tap to say hello!</a></td>
			  </tr>
			  <tr>
			    <td>2</td>
			    <td>RUU tentang Radio Televisi Republik Indonesia</td>
			    <td>Komisi I DPR</td>
			    <td>Periode 2009-2014, Usul DPR, Ada NA+RUU</td>
			    <td><a href="http://id.ibolz.tv">id.ibolz.tv</a></td>
			    <td>call <a href="tel:+6285782221361">+62857-8222-1361</a></td>
			    <td>sms 6285782221361 <a href="sms:+6285782221361?body=hello">Tap to say hello!</a></td>
			  </tr>
			  <tr>
			    <td>3</td>
			    <td>RUU tentang Perubahan atas Undang-Undang No.11 Tahun 2008 tentang Informasi dan Transaksi Elektronik (ITE)</td>
			    <td>Kementerian Komunikasi dan Informatika</td>
			    <td>Prolegnas 2010-2014, Ada NA+RUU</td>
			    <td><a href="http://id.ibolz.tv">id.ibolz.tv</a></td>
			    <td>call <a href="tel:+6285782221361">+62857-8222-1361</a></td>
			    <td>sms 6285782221361 <a href="sms:+6285782221361?body=hello">Tap to say hello!</a></td>
			  </tr>
			  <tr>
			    <td>4</td>
			    <td>RUU tentang Wawasan Nusantara</td>
			    <td>PPUU DPD</td>
			    <td>Sudah ada NA, RUU sedang proses</td>
			    <td><a href="http://id.ibolz.tv">id.ibolz.tv</a></td>
			    <td>call <a href="tel:+6285782221361">+62857-8222-1361</a></td>
			    <td>sms 6285782221361 <a href="sms:+6285782221361?body=hello">Tap to say hello!</a></td>
			  </tr>
			  <tr>
			    <td>5</td>
			    <td>RUU tentang Pertanahan</td>
			    <td>Komisi II DPR</td>
			    <td>Pembicaraan Tk. I, Ada NA+RUU</td>
			    <td><a href="http://id.ibolz.tv">id.ibolz.tv</a></td>
			    <td>call <a href="tel:+6285782221361">+62857-8222-1361</a></td>
			    <td>sms 6285782221361 <a href="sms:+6285782221361?body=hello">Tap to say hello!</a></td>
			  </tr>
			  <tr>
			    <td>6</td>
			    <td>RUU tentang Perubahan atas UU No. 33 Tahun 2004 tentang Perimbangan Keuangan antara Pusat dan Daerah</td>
			    <td>Komisi II DPR, Kementerian Keuangan, PPUU DPD</td>
			    <td>Pembicaraan Tk. I, Ada NA+RUU</td>
			    <td><a href="http://id.ibolz.tv">id.ibolz.tv</a></td>
			    <td>call <a href="tel:+6285782221361">+62857-8222-1361</a></td>
			    <td>sms 6285782221361 <a href="sms:+6285782221361?body=hello">Tap to say hello!</a></td>
			  </tr>
			  <tr>
			    <td>7</td>
			    <td>RUU tentang Perubahan atas Undang-Undang Nomor 1 Tahun 2015 tentang Penetapan Peraturan Pemerintah Pengganti UU No. 1 Tahun 2014 tentang Pemilihan Gubernur, Bupati, dan Walikota Menjadi UU</td>
			    <td>Komisi II DPR</td>
			    <td>Ada NA+RUU</td>
			    <td><a href="http://id.ibolz.tv">id.ibolz.tv</a></td>
			    <td>call <a href="tel:+6285782221361">+62857-8222-1361</a></td>
			    <td>sms 6285782221361 <a href="sms:+6285782221361?body=hello">Tap to say hello!</a></td>
			  </tr>
			</table>
			</Body>
			</html>';
  	}

  	function upload_screenshot() {
  		$this->load->model('product_m'); //product_bycontentid

  		$apps_id 		= $this->input->post("apps_id");
	    $version 		= $this->input->post("version");
	    $customer_id 	= $this->input->post("customer_id");
	    $device_id 		= $this->input->post("device_id");
	    $screen_size 	= $this->input->post("screen_size");
	    $remote_host 	= $_SERVER['REMOTE_ADDR'];
		$referral 		= ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent 	= $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id 		= $this->input->post("mobile_id");
	    $lang 			= ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";

        $share_type  	= $this->input->post("share_type");
        $product_id   	= $this->input->post("product_id");
        $channel_type_id= $this->input->post("channel_type_id");
        // $menu_id		= $this->input->post("menu_id");

        $target_path 	= $this->config->item('thumbnail_path');
        // $target_path = 'C:/xampp/htdocs/iBOLZID/api106/uploadimages/';
        $url_landingpage = 'http://id.ibolz.tv/';

        $url_sharetype	 = 'https://www.facebook.com/sharer/sharer.php?u=';
		if ($share_type == 'Facebook') {
        	$url_sharetype	 = 'https://www.facebook.com/sharer/sharer.php?u=';
        } elseif ($share_type == 'Twitter') {
        	$url_sharetype	 = 'https://twitter.com/intent/tweet?url=';
        }
        
        

        $response = array(
            "rcode" => 0,
            "message" => "Error on upload",
            "thumbnail" => "http://tmb001.3d.ibolztv.net/media/thumbnail.php?id=" . $this->config->item('img_not_found'),
            "thumbnail_id" => $this->config->item('img_not_found'),
            "share_url" => $url_sharetype.urlencode($url_landingpage)
        );
        $is_file = 0;

        if (isset($_FILES['image_file']['tmp_name']) && $_FILES['image_file']['tmp_name']) {
            $is_file = 1;
            if (!empty($_FILES['image_file']['error'])) {
                $file_error = $_FILES['image_file']['error'];
                switch ($file_error) {
                    case '1':
                    case '2':
                        $response['message'] = 'File too big to upload';
                        break;
                    case '3':
                        $response['message'] = 'File partially uploaded';
                        break;
                    case '4':
                        $response['message'] = 'No file was uploaded.';
                        break;
                    case '6':
                        $response['message'] = 'Missing a temporary folder';
                        break;
                    case '7':
                        $response['message'] = 'Failed to save file';
                        break;
                    default:
                        $response['message'] = 'Failed to upload, unknown error';
                        break;
                }
            }
        }


        if ($is_file) {

            $filename = basename($_FILES['image_file']['name']);
            $newFileName = uniqid(rand(), false) . substr($filename, strrpos($filename, ".", -2));
            $target_path = $target_path . $newFileName;
            if (move_uploaded_file($_FILES['image_file']['tmp_name'], $target_path)) {
                // $this->customer_m->upload_photo($customer_id, $newFileName);

     //            if ($product_id) {
					// $product_list = $this->product_m->product_bycontentid($apps_id, $device_id, $product_id);
					// $menu_id = $product_list[0]->menu_id;

					$url_landingpage = 'http://id.ibolz.tv/home/content_screenshot/'.$product_id.'/0/'.$newFileName.'/content';
				// }

                $response = array(
                    "rcode" => 1,
                    "message" => "Successfully Uploaded",
                    "thumbnail" => "http://tmb001.3d.ibolztv.net/media/thumbnail.php?id=" . $newFileName,
                    "thumbnail_id" => $newFileName,
                    "share_url" => $url_sharetype.urlencode($url_landingpage)
                );
            } else {
                $response['rcode'] = "0";
                $response['message'] = "unable to copy file to server.";
                $response['thumbnail'] = "http://tmb001.3d.ibolztv.net/media/thumbnail.php?id=" . $this->config->item('img_not_found');
                $response['thumbnail_id'] = $this->config->item('img_not_found');
                $response['share_url'] = $url_sharetype.urlencode($url_landingpage);
            }
        } else {
            $response['rcode'] = "0";
            $response['message'] = "No file to upload.";
            $response['thumbnail'] = "http://tmb001.3d.ibolztv.net/media/thumbnail.php?id=" . $this->config->item('img_not_found');
            $response['thumbnail_id'] = $this->config->item('img_not_found');
            $response['share_url'] = $url_sharetype.urlencode($url_landingpage);
        }

        

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }
    
    function save_reference_code()
    {
    	$apps_id = $this->input->post("apps_id");
    	$version = $this->input->post("version");
    	$device_id = $this->input->post("device_id");
    	$mobile_id = $this->input->post("mobile_id");
    	$user_id = $this->input->post("user_id");
    	$reference_code = $this->input->post("reference_code");
    	
    	if(empty($apps_id) || empty($mobile_id) || empty($user_id) || empty($reference_code))
    	{
    		$response = array(
    				"rcode" => 0,
    				"message" => "Failed. Missing arguments"
    		);
    	}
    	else {
    		$result = $this->services_m->saveReferenceCode ( $user_id, $mobile_id, $reference_code, '0' );
			if ($result == 1) {
				$response = array (
						"rcode" => 1,
						"message" => "Saved successfuly" 
				);
			} else {
				$response = array (
						"rcode" => 0,
						"message" => "Data already exist" 
				);
			}
		}
    	
    	
    	$this->output->set_content_type('application/json');
    	$this->output->set_output(json_encode($response));
    	
    	
    }
    
    function register_referer_user()
    {
    	$apps_id = $this->input->post("apps_id");
    	$version = $this->input->post("version");
    	$device_id = $this->input->post("device_id");
    	$mobile_id = $this->input->post("mobile_id");
    	$user_id = $this->input->post("user_id");
    	$real_name = $this->input->post("real_name");
    	$gender = $this->input->post("gender");
    	$birthdate = $this->input->post("birthdate");
    	$id_type = $this->input->post("id_type");
    	$id_number = $this->input->post("id_number");
    	$bank_number = $this->input->post("bank_number");
    	$phone = $this->input->post("phone");
    	$address = $this->input->post("address");
    	$email = $this->input->post("email");

    	//echo $real_name . $gender . $birthdate . $id_type . $id_number . $bank_number . $phone.$address. $email.$user_id.$mobile_id;
    	
    	if(empty($apps_id)) // || empty($mobile_id) || empty($user_id) || empty($real_name)
    			//|| empty($id_number) || empty($bank_number) || empty($phone))
    	{
    		$response = array(
    				"rcode" => 0,
    				"message" => "Failed. Missing arguments"
    		);
    	}
    	else 
    	{
    		$result = $this->services_m->registerRefererUser ($real_name, $gender, $birthdate, $id_type, $id_number, $bank_number, $phone,$address, $email, $user_id, $mobile_id);
    		if ($result == 1) {
    			$response = array (
    					"rcode" => 1,
    					"message" => "Saved successfuly"
    			);
    		} else {
    			$response = array (
    					"rcode" => 0,
    					"message" => "Failed. Data already exist"
    			);
    		}
    	}
    	
    	
    	$this->output->set_content_type('application/json');
    	$this->output->set_output(json_encode($response));
    }

    function form_submit(){
  		$apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";
	    
	    $form_param = $this->input->post("form_param");
	    $add_param = $this->input->post("addParam");

	    // $rooturl	= 'http://localhost/ibolzid/poi/';
		$rooturl	= 'http://172.16.9.113/poi-testing/';

		$services_url = $rooturl . "channelformsubmit.php";

    	$postfield = json_encode( array( "form_param"=> $form_param, "form_name"=> $add_param, "customer_id"=> $customer_id, "apps_id"=> $apps_id ) );

    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $services_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 3600
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfield);
		curl_setopt($ch, CURLOPT_HTTPHEADER,
						array('Content-Type:application/json',
								'Content-Length: ' . strlen($postfield))
					);
		$response = curl_exec($ch);
		$res_code = curl_errno($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		
		if ($info['http_code'] == 200) {
			$data = $response;
		}else{
			$data = array();
		}

	    $this->output->set_content_type('application/json');
    	$this->output->set_output($data);

	}

	function custom_tabnavigation(){
  		$apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";
	    
	    // $form_param = $this->input->post("form_param");
	    $feature_name = $this->input->post("feature_name");
	    $add_param = $this->input->post("addParam");
	    if(empty($add_param)){
	    	$add_param = "";
	    }

	    // $rooturl	= 'http://localhost/ibolzid/poi/';
		$rooturl	= 'http://172.16.9.113/poi-testing/';

		$services_url = $rooturl . "customnavigation.php";

    	$postfield = json_encode( array( "apps_id"=> $apps_id, "feature_name"=> $feature_name ) );

    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $services_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 3600
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfield);
		curl_setopt($ch, CURLOPT_HTTPHEADER,
						array('Content-Type:application/json',
								'Content-Length: ' . strlen($postfield))
					);
		$response = curl_exec($ch);
		$res_code = curl_errno($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		
		if ($info['http_code'] == 200) {
			$data = $response;
		}else{
			$data = array();
		}

	    $this->output->set_content_type('application/json');
    	$this->output->set_output($data);

	}

	function gallery_list(){
  		$apps_id = $this->input->post("apps_id");
	    $version = $this->input->post("version");
	    $customer_id = $this->input->post("customer_id");
	    $device_id = $this->input->post("device_id");
	    $screen_size = $this->input->post("screen_size");
	    $remote_host = $_SERVER['REMOTE_ADDR'];
		$referral = ""; //$_SERVER["HTTP_REFERER"];
	    $user_agent = $_SERVER['HTTP_USER_AGENT'];            
	    $mobile_id = $this->input->post("mobile_id");
	    $lang = ($this->input->post("lang") != "") ? $this->input->post("lang") : "en_US";
	    
	    $add_param = $this->input->post("addParam");
	    if(empty($add_param)){
	    	$add_param = "";
	    }

	    // $rooturl	= 'http://localhost/ibolzid/poi/';
		$rooturl	= 'http://172.16.9.113/poi-testing/';

		$services_url = $rooturl . "gallery_list.php";

    	$postfield = json_encode( array( "apps_id"=> $apps_id, "add_param"=> $add_param, "customer_id"=> $customer_id ) );

    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $services_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // 3600
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfield);
		curl_setopt($ch, CURLOPT_HTTPHEADER,
						array('Content-Type:application/json',
								'Content-Length: ' . strlen($postfield))
					);
		$response = curl_exec($ch);
		$res_code = curl_errno($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		
		if ($info['http_code'] == 200) {
			$data = $response;
		}else{
			$data = array();
		}

	    $this->output->set_content_type('application/json');
    	$this->output->set_output($data);

	}

}