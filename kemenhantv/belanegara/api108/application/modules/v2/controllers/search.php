<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: zola.octanoviar
	Date: oct 2015
	Searching Application
*/

class Search extends REST_Controller{

	public function __construct(){
		parent::__construct();
    	$this->load->model(array('search_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
		

		$this->apps_id = $this->input->post("apps_id");
		$this->lang = $this->input->post("lang");
		$this->customer_id = $this->input->post("customer_id");		
		$this->device_id = $this->input->post("device_id");		
				
		// checking params
	    if(empty($this->apps_id)) $this->response_failed('missing_app_id');        
    	if(empty($this->customer_id)) $this->response_failed('missing_customer_id');        
		if(empty($this->device_id)) $this->response_failed('missing_device_id');        
    	//if(empty($this->lang)) $this->lang =  'id_ID';        
		
	}	

	public function search_list_post(){
		//ALTER TABLE content_search ADD FULLTEXT fulltext_title(title);
		//ALTER TABLE crew_search ADD FULLTEXT fulltext_crew(crew_name);
		//ALTER TABLE genre_search ADD FULLTEXT fulltext_genre(genre_name);
		$menu_id = $this->input->post("menu_id");
		$device_id = $this->input->post("device_id");
				
		$page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$limit = (int)($this->input->post("limit") == "" ? 28 : $this->input->post("limit"));
		$keyword = $this->input->post("keyword") == "" ? "" : $this->input->post("keyword");
		$totalitem = $this->input->post("totalitem") == "" ? "" : $this->input->post("totalitem");
		$customer_id = $this->input->post("customer_id") == "" ? null : $this->input->post("customer_id");

		$product_list = $this->search_m->search_list($page, $limit, $this->apps_id, $keyword);
		if($page == 0){
			if($product_list && count($product_list)>0)
				$totalitem = $product_list['total'];
		}
		$response = array(
			"filter"=> array(
				"page" => $page,
				"limit" => $limit,
				"lang" => $this->lang,
				"totalitem" => $totalitem,
				"keyword" => $keyword
			),
			"product_list" => $product_list['list']
    	);
		$this->response_success($response);
	}

	
}


