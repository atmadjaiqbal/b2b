<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Army extends REST_Controller {

    public function __construct() {
        parent::__construct();
    }

    function save_reference_code_post() {

        //ws direct
        $params = array(
            "apps_id" => $this->input->post("apps_id"),
            "mobile_id" => $this->input->post("mobile_id"),
            "referral_number" => $this->input->post("referral_number"),
            "encryption_key" => $this->input->post("encryption_key")
        );
        
        $curl = new Curl();
        $ws_result = $curl->_simple_call('post', $this->config->item('army_url') . 'army/save_army_code', $params);
        $this->response(json_decode($ws_result), 200);
    }

    function register_referer_user_post() {
        
        $this->load->model('customer_m');

        $customer_id = $this->input->post("customer_id");

        $params = array(
            "customer_id" => $customer_id,
            "apps_id" => $this->input->post("apps_id"),
            //referral fields
            "id_type" => $this->input->post("id_type"),
            "id_number" => $this->input->post("id_number"),
            "bank_code" => $this->input->post("bank_code"),
            "bank_account_name" => $this->input->post("bank_account_name"),
            "bank_account_number" => $this->input->post("bank_account_number")
        );

        if (!empty($customer_id)) {
            $customer = $this->customer_m->profile($customer_id);
            if ($customer) {
                $params['email'] = $customer->email;
                $params['user_id'] = $customer->user_id;
                $params['first_name'] = (empty($customer->first_name) ? "" : $customer->first_name);
                $params['last_name'] = (empty($customer->last_name) ? "" : $customer->last_name);
                $params['display_name'] = (empty($customer->display_name) ? "" : $customer->display_name);
                $params['mobile_no'] = (empty($customer->mobile_no) ? "" : $customer->mobile_no);
                $params['bod'] = (empty($customer->bod) ? "" : $customer->bod);
                $params['gender'] = (empty($customer->gender) ? "" : $customer->gender);
            }
        }

        $curl = new Curl();
        $ws_result = $curl->_simple_call('post', $this->config->item('army_url') . 'army/register_army', $params);
        $this->response(json_decode($ws_result), 200);
    }

    public function reference_history_post() {

        $customer_id = $this->input->post("customer_id");
        $apps_id = $this->input->post("apps_id");
        $reference_number = $this->input->post("reference_number");

        $limit = $this->input->post("limit");
        $offset = $this->input->post("offset");

        if (!$limit) {
            $limit = 0;
        }
        if (!$offset) {
            $offset = 0;
        }
        
        $params = array(
            "customer_id" => $customer_id,
            "apps_id" => $apps_id,
            "reference_number" => $reference_number,
            "limit" => $limit,
            "offset" => $offset
        );
        
        $curl = new Curl();
        $ws_result = $curl->_simple_call('post', $this->config->item('army_url') . 'army/army_history', $params);
        $this->response(json_decode($ws_result), 200);
    }

    public function introduction_post() {

        $curl = new Curl();
        $ws_result = $curl->_simple_call('post', $this->config->item('army_url') . 'army/introduction', null);
        $this->response(json_decode($ws_result), 200);
    }

}
