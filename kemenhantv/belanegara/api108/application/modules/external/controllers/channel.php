<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 17 2014
	Channel controllers
	package: /external
*/

class Channel extends REST_Controller{
	
	private $thumbnail_path;
	public function __construct(){
		parent::__construct();
        $this->load->model(array('apps_m', 'channel_m'));
		$this->load->helper('url');		
		$this->lang->load('default');
		$this->config->load('config');

		$this->thumbnail_path = config_item('thumbnail_url');
	}	
	
	/*
	* Get channel categories 
	* method: GET
	* url: /channel/categories/$apps_id
	* argument: apps_id
	*/
    public function categories_get($app_id = FALSE){
    	if(empty($app_id)) $this->response_failed(lang('missing_app_id'));
        $app = $this->apps_m->find($app_id);
        if(!$app) $this->response_failed(lang('error_app_id'));        
    	$categories = $this->channel_m->get_channel_categories($app_id);
    	$items = array();
    	if($categories){
    		foreach ($categories as $row) {
    			$items[] = array(
					'apps_id' => $row->apps_id,
		            "channel_category_id" => $row->channel_category_id,
		            "category_name" => $row->category_name,
		            "category_name_chn" => $row->category_name_chn,
		            "thumbnail" => $this->thumbnail_path . '?id='. ($row->thumbnail ? $row->thumbnail : config_item('img_not_found'))
    			);
    		}
    	}
    	$data = array(
    		'items' => $items,
    		'count_items' => count($items)
    	);
    	$this->response_success($data);
    }

	/*
	* Get channels list
	* method: GET
	* url: /channel/list/$apps_id
	* argument: apps_id
	* parameters: customer_id, channel_category_id
	*/
    public function lists_get($app_id = FALSE){
    	if(empty($app_id)) $this->response_failed(lang('missing_app_id'));
        $app = $this->apps_m->find($app_id);
        if(!$app) $this->response_failed(lang('error_app_id'));

        $customer_id = $this->input->get('customer_id');

        $channel_categories = array();
        $param_channel_category_id = $this->input->get('channel_category_id');
        if(!$param_channel_category_id){
        	$categories = $this->channel_m->get_channel_categories($app_id);
	    	if($categories){
	    		foreach ($categories as $row) {
	    			$channel_categories[] = $row->channel_category_id;
	    		}
	    	}        	
        }else{
        	$channel_categories[] = $param_channel_category_id;
        }

        $items = array();
        foreach ($channel_categories as $channel_category_id) {
        	if($channel_category_id != '-2'){
				$channels = $this->channel_m->get_channels($app_id, $channel_category_id);
			}else{
				// $channels = $this->channel_m->get_groups_as_channels($app_id, $channel_category_id);
				$channels = $this->channel_m->get_category_groups_as_channels($app_id, $channel_category_id, $customer_id);
			}
	    	if(isset($channels) && $channels){
	    		foreach ($channels as $row) {
	    			$item = array(
						'apps_id' => $row->apps_id,
			            // "channel_category_id" => $row->channel_category_id,
			            // "category_name" => $row->category_name,
			            "channel_id" => $row->channel_id,
			            "channel_name" => $row->channel_name,
			            "display_name" => $row->display_name,
			            "thumbnail" => $this->thumbnail_path . '?id='. ($row->thumbnail ? $row->thumbnail : config_item('img_not_found'))
	    			);
	    			if(isset($row->group_id)){
	    				$item['group_id'] = $row->group_id;
		    			$item['workgroup_id'] = $row->workgroup_id;
		    			$item['workgroup_name'] = $row->workgroup_name;
		    			$item['display_name'] = $row->display_name . ' - ' . $row->workgroup_name;
	    			}
	    			$items[] = $item;
	    		}
	    	}			
		}
    	$data = array(
    		'items' => $items,
    		'count_items' => count($items)
    	);
    	$this->response_success($data);
    }


    public function ntmc_get(){

        $items = array();
		$channels = $this->channel_m->get_channels_ntmc();
		if(isset($channels) && $channels){
    		foreach ($channels as $row) {
    			$item = array(
		            "channel_id" => $row->channel_id,
		            "channel_name" => $row->channel_name,
		            "alias" => $row->alias,
		            "link" => $row->link,
		            "status" => $row->status,
		            "cctv_ip" => $row->cctv_ip
    			);
    			$items[] = $item;
    		}
		}

    	$data = array(
    		'items' => $items,
    		'count_items' => count($items)
    	);
    	$this->response_success($data);
    }

}