<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 13 2014
	external/models/customer_m extend to Customer_Model
*/

require APPPATH."models/customer_model.php";

class Customer_m extends Customer_model {
	
	protected $date_format = 'datetime';
	protected $table 	= 'customer';
	protected $key		= 'customer_id';

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('email');
	}

	public function login($user_id, $password, $latitude, $longitude){
		$password = md5('mat123' . $password . 'kra');
		if(valid_email($user_id)){
			$customer = $this->find_by(array(
				'LOWER(email)' => strtolower($user_id),
				'password' => $password
			));
		}else{
			$customer = $this->find_by(array(
				'mobile_no' => $user_id,
				'password' => $password
			));
		}
		if($customer){
			$this->update($customer->customer_id, array(
				'last_login' => $this->set_date(), 
				'latitude' => $latitude, 
				'longitude' => $longitude
			));
		}
		return $customer;
	}

	public function get_account_by_mobile_no($mobile_no){
		$sql = "
			SELECT 
				customer_id, email, mobile_id, user_id, first_name, last_name, 
				display_name, status
  			FROM customer
  			WHERE lower(mobile_no) = lower('$mobile_no')
		";
  		$query = $this->db->query($sql);	
		return $query->row();
	}	

}