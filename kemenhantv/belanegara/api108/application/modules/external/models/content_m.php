<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 18 2014 20:46 
*/
class Content_m extends MY_Model {
	
	protected $table 	= 'content';
	protected $key		= 'content_id';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer_group_m');
	}

	// get contents
	public function get_contents($apps_id, $channel_id = false, $customer_id = false){
		$sql = "
			SELECT  *
			FROM
			(
				SELECT 
					a.apps_id, a.channel_category_id, a.channel_id, a.channel_type_id, 
					b.channel_name, b.alias AS channel_display_name,
					a.content_id, a.content_type, 
					a.title, a.thumbnail, a.description, 
					a.prod_year, a.video_duration, a.order_number
				FROM 
					vcontent a
					JOIN channel b ON (a.channel_id = b.channel_id)
				WHERE 
					a.apps_id = '{$apps_id}' AND a.channel_category_id != '-2'					
				UNION				
				SELECT
					g.apps_id, '-2' AS channel_category_id, c.customer_category_id AS channel_id, '3' AS channel_type_id,
					d.category_name AS channel_name, CONCAT(d.category_name, ' - ', f.groups_name) AS channel_display_name,
					b.content_id, 'content' AS content_type,
					b.title, b.video_thumbnail AS thumbnail, b.description,
					YEAR(b.created) AS prod_year, b.video_length AS video_duration, 1000 AS order_number
				FROM content b
				JOIN content_customer_category c ON (b.content_id = c.content_id)	
				JOIN customer_category d ON (c.customer_category_id = d.customer_category_id)
				JOIN groups_customer_category e ON (c.customer_category_id = e.customer_category_id)
				JOIN groups f ON (e.groups_id = f.groups_id)
				JOIN groups_apps g  ON (f.groups_id = g.groups_id)
				WHERE 
					g.apps_id = '{$apps_id}' AND b.active = '1' AND b.status = '0'
			) rows_content	
		";
		$sql .= " WHERE apps_id = '{$apps_id}' ";
		if($channel_id){
			$sql .= " AND channel_id = '{$channel_id}' ";
		}
		$sql .= "
			GROUP BY content_id 
			ORDER BY order_number, title
		";
  		$query = $this->db->query($sql);	
		return $query->result();
	}

}