<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Author: htridianto
	guangzhou nov, 17 2014 22:20 
*/
class Channel_m extends MY_Model {
	
	protected $table 	= 'channel';
	protected $key		= 'channel_id';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer_group_m');
	}

	// get channel categories
	public function get_channel_categories($apps_id){
		$sql = "
			SELECT 
				a.apps_id, a.app_name,
				b.channel_category_id, 	
				c.category_name, c.category_name_chn, 
				c.thumbnail, c.tab_type, c.link,
				b.show_tab_menu, b.show_dd_menu
			FROM 
				apps a 
				JOIN apps_category b ON (a.apps_id = b.apps_id)
				JOIN channel_category c ON (b.channel_category_id = c.channel_category_id)
			WHERE a.apps_id = '{$apps_id}'
			ORDER BY b.order_number, c.category_name
		";
  		$query = $this->db->query($sql);	
		return $query->result();
	}

	// get channels
	public function get_channels($apps_id, $channel_category_id = false){
		$sql = "
			SELECT 
				a.apps_id, b.channel_category_id, cc.category_name,
				a.channel_id, d.channel_name, 
				d.alias AS display_name, d.channel_type_id,
				d.thumbnail, d.status, d.link
			FROM 
				apps_channel_category a
				JOIN apps_category b ON (a.apps_id = b.apps_id AND a.channel_category_id = b.channel_category_id)	
				JOIN apps_channel c ON (a.apps_id = c.apps_id AND a.channel_id = c.channel_id)
				JOIN channel_category cc ON (a.channel_category_id = cc.channel_category_id)				
				JOIN channel d ON (c.channel_id = d.channel_id)
			WHERE 
				a.apps_id = '{$apps_id}' AND d.status = 1
				-- AND b.channel_category_id != '-2'
		";
		if($channel_category_id){
			$sql .= "
				AND b.channel_category_id = '{$channel_category_id}'
			";
		}
		$sql .= "ORDER BY ";
		if(!$channel_category_id){
			$sql .= "cc.category_name, ";
		}
		$sql .= "a.order_number, d.channel_name";
  		$query = $this->db->query($sql);
		return $query->result();
	}

	// this function will return channel for customer content (channel_category_id = -2)
	public function get_channel_customercontent($apps_id){
		$channel_id = -2;
		$sql = "
			SELECT
				a.channel_id
			FROM
				apps_channel_category a
				JOIN channel b ON (a.channel_id = b.channel_id)
			WHERE 
				a.apps_id = '{$apps_id}'
				AND a.channel_category_id = -2 AND b.status = 1
			ORDER BY RAND()		
		";
		$query = $this->db->query($sql);	
		$row = $query->row();
		if($row){
			$channel_id = $row->channel_id;
		}
		return $channel_id;
	}

	// this function for mapping customer group as channels
	// public function get_groups_as_channels($apps_id, $channelcategory_id, $customer_id = false){
	// 	$channel_category_id = '3';
	// 	$category_name = 'VOD';
	// 	$thumbnail = config_item('img_not_found');
	// 	// get thumbnail ... original copy from query of 'apps_model->apps_groups_list'
	// 	$sql = "
	// 		SELECT * 
	// 		FROM channel_category  
	// 		WHERE channel_category_id = '{$channelcategory_id}'
	// 	";
	// 	$query = $this->db->query($sql);
	// 	$channel_category = $query->row();		
	// 	if($channel_category){
	// 		$channel_category_id = $channel_category->channel_category_id;
	// 		$category_name = $channel_category->category_name;
	// 		$thumbnail = $channel_category->thumbnail;
	// 	}
	// 	$sql = "
	// 		SELECT 
	// 			a.apps_id, '{$channel_category_id}' AS channel_category_id, '{$category_name}' AS category_name, 
	// 			b.groups_id AS channel_id, b.groups_name AS channel_name, 
	// 			b.groups_name AS display_name, '3' AS channel_type_id, 
	// 			'{$thumbnail}' AS thumbnail, '1' AS `status`, '' AS link
	// 		FROM 
	// 			groups_apps a 
	// 			JOIN groups b ON (a.groups_id = b.groups_id)
	// 		WHERE a.apps_id = '{$apps_id}'
	// 		ORDER BY b.groups_name
	// 	";
 //  		$query = $this->db->query($sql);	
	// 	return $query->result();
	// }

	// this function for mapping customer category group as channels
	public function get_category_groups_as_channels($apps_id, $channelcategory_id, $customer_id = false){
		$groups = array();
		if($customer_id){
			$workgroups = $this->customer_group_m->get_member_of_group($apps_id, $customer_id);
			if($workgroups){
				foreach ($workgroups as $row) {
					if($row->allow_access == 'true'){
						$groups[] = $row->group_id; // is mapping of customer_category_id 
					}
				}
			}
		}
		
		$channel_category_id = '3';
		$category_name = 'VOD';
		$thumbnail = config_item('img_not_found');
		// get thumbnail ... original copy from query of 'apps_model->apps_groups_list'
		$sql = "
			SELECT * 
			FROM channel_category  
			WHERE channel_category_id = '{$channelcategory_id}'
		";
		$query = $this->db->query($sql);
		$channel_category = $query->row();		
		if($channel_category){
			$channel_category_id = $channel_category->channel_category_id;
			$category_name = $channel_category->category_name;
			$thumbnail = $channel_category->thumbnail;
		}

		// $channel_id = $this->get_channel_customercontent($apps_id);
		$sql = "
			SELECT
			a.apps_id, {$channel_category_id} AS channel_category_id, '{$category_name}' AS category_name, 
			b.groups_id AS workgroup_id, b.groups_name AS workgroup_name, 
			d.customer_category_id AS group_id,
			d.customer_category_id AS channel_id, d.category_name AS channel_name, 
			d.category_name AS display_name, '3' AS channel_type_id, 
			'{$thumbnail}' AS thumbnail, '1' AS `status`, '' AS link
			FROM
				groups_apps a 
				JOIN groups b ON (a.groups_id = b.groups_id)
				JOIN groups_customer_category c ON (b.groups_id = c.groups_id)
				JOIN customer_category d ON (c.customer_category_id = d.customer_category_id)
			WHERE a.apps_id = '{$apps_id}'			
		";
		if(!empty($groups)){
			$sql .= " AND d.customer_category_id IN ('" . implode("','", $groups) . "') ";
		}
		$sql .= "ORDER BY b.groups_name";
  		$query = $this->db->query($sql);	
		return $query->result();
	}


	// get channels
	public function get_channels_ntmc(){
		$sql = "
			SELECT 
				channel_id, channel_name, alias, channel_type_id, thumbnail, status, link, cctv_ip
			FROM channel 
			WHERE  channel_type_id = '8'
		";
		$sql .= "ORDER BY alias";
  		$query = $this->db->query($sql);
		return $query->result();
	}
}