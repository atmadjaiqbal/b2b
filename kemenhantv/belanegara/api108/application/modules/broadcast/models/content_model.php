<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends MY_Model {
	


	public function content_category_list($groups_id) {		
		$textquery = "
			SELECT content_category_id, category_name
			FROM content_category 
			WHERE content_category_id in (select content_category_id from groups_content_category where groups_id='$groups_id')
			ORDER BY category_name
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;
	}
	
	
	public function content_update($content_id, $title, $content_category_id) {		
		$textquery = "
			UPDATE content SET title = '$title'
			WHERE content_id = '$content_id'
		 	";

		$query = $this->db->query($textquery);	
	}
	

	public function groups_content_insert($content_id, $groups_id, $createdby) {		
		$textquery = "
			INSERT INTO groups_content(content_id, groups_id, createdby)
			VALUES ('$content_id', '$groups_id', '$createdby')
		 	";

		$query = $this->db->query($textquery);	
	}
	

	public function content_apps_insert($content_id, $apps_id, $createdby) {		
		$textquery = "
			INSERT INTO content_apps(content_id, apps_id, createdby, created)
			VALUES ('$content_id', '$apps_id', '$createdby', now())
		 	";

		$query = $this->db->query($textquery);	
	}
	

	public function content_list($page, $limit, $keyword, $content_category_id, $prod_house_id, $groups_id) {		
		$textquery = "
			SELECT  
				a.content_id, title, description, video_thumbnail as thumbnail,
				(select category_name from content_category where content_category_id=a.content_category_id) as category_name,
				(select prod_house_name from prod_house where prod_house_id=a.prod_house_id) as prod_house_name,
				video_length as duration, (select status_name from content_status where status_id=a.status) as status_name, status,
				TIME_TO_SEC(TIMEDIFF(now(), created)) as created_time, created
			FROM content a, groups_content b
			WHERE lower(title) like lower('%$keyword%')
            AND b.groups_id = '$groups_id'
            AND a.content_id = b.content_id
			ORDER BY created DESC, title
		";

			
		if(!empty($page) || !empty($limit)){
			$textquery .= "LIMIT $page, $limit";
		}

		$query = $this->db->query($textquery);	
		$rows = $query->result();
		
		foreach($rows as $row){
			//convert duration
			$duration = $row->duration;
			$seconds = $duration %60;
			$minutes = (floor($duration/60)) % 60;
			$hours = floor($duration/3600);
			
			$duration_text = '';
			if($hours > 0){
				$duration_text = $duration_text . $hours . ' Hour ';
			}
			
			if($minutes > 0){
				$duration_text = $duration_text . $minutes . ' Min ';
			}

			if($seconds > 0){
				$duration_text = $duration_text . $seconds . ' Sec ';
			}
			$row->duration_text = $duration_text;


			$created_time = ($row->created_time);
			$seconds = $created_time %60;
			$minutes = (floor($created_time/60)) % 60;
			$hours = floor($created_time/3600);
			$created_time_text = '';
			
			if($created_time < 86400){
				if($hours > 0){
					$created_time_text = $created_time_text . $hours . ' Hour ';
				}
			
				if($minutes > 0){
					$created_time_text = $created_time_text . $minutes . ' Min ';
				}
			
				$row->created_time_text = $created_time_text . 'ago';
			}else{
				$row->created_time_text = $row->created;
			}
			
			
			//set thumbnail
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
			
			//available resolution
			//$content_res_list = $this->content_res_list($row->content_id);
			//$row->content_res_list = $content_res_list;
		}
		return $rows;
	}

	public function content_my_list($page, $limit, $keyword, $customer_id, $list_type='my') {		
		$where = '';
		if ($list_type == 'my')  $where  = " AND customer_id = '$customer_id' ";
		if ($list_type == 'share')  $where  = " AND active = '1' AND customer_id = '$customer_id' ";

		$textquery = "
			SELECT  
				content_id, title, description, video_thumbnail as thumbnail,
				(select category_name from content_category where content_category_id=a.content_category_id) as category_name,
				(select prod_house_name from prod_house where prod_house_id=a.prod_house_id) as prod_house_name,
				video_length as duration, (select status_name from content_status where status_id=a.status) as status_name, status,
				TIME_TO_SEC(TIMEDIFF(now(), created)) as created_time, created
			FROM content a
			WHERE lower(title) like lower('%$keyword%')
			AND content_category_id = '9'
			AND privacy = '1' $where 
			ORDER BY created DESC, title ";

			
		if(!empty($page) || !empty($limit)){
			$textquery .= "LIMIT $page, $limit";
		}

		$query = $this->db->query($textquery);	
		$rows = $query->result();
		
		foreach($rows as $row){
			//convert duration
			$duration = $row->duration;
			$seconds = $duration %60;
			$minutes = (floor($duration/60)) % 60;
			$hours = floor($duration/3600);
			
			$duration_text = '';
			if($hours > 0){
				$duration_text = $duration_text . $hours . ' Hour ';
			}
			
			if($minutes > 0){
				$duration_text = $duration_text . $minutes . ' Min ';
			}

			if($seconds > 0){
				$duration_text = $duration_text . $seconds . ' Sec ';
			}
			$row->duration_text = $duration_text;


			$created_time = ($row->created_time);
			$seconds = $created_time %60;
			$minutes = (floor($created_time/60)) % 60;
			$hours = floor($created_time/3600);
			$created_time_text = '';
			
			if($created_time < 86400){
				if($hours > 0){
					$created_time_text = $created_time_text . $hours . ' Hour ';
				}
			
				if($minutes > 0){
					$created_time_text = $created_time_text . $minutes . ' Min ';
				}
			
				$row->created_time_text = $created_time_text . 'ago';
			}else{
				$row->created_time_text = $row->created;
			}
			
			
			//set thumbnail
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/api/media/thumbnail_customer/". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail/" . $this->config->item('img_not_found') ;
			}
			
			//available resolution
			//$content_res_list = $this->content_res_list($row->content_id);
			//$row->content_res_list = $content_res_list;
		}
		return $rows;
	}
	

	public function content_detail($content_id, $device_id) {		
		$textquery = "
			SELECT  
				content_id, title, description, video_thumbnail as thumbnail, live_url, filename,
				(select category_name from content_category where content_category_id=a.content_category_id) as category_name,
				(select prod_house_name from prod_house where prod_house_id=a.prod_house_id) as prod_house_name,
				video_length as duration, (select status_name from content_status where status_id=a.status) as status_name, status,
				TIME_TO_SEC(TIMEDIFF(now(), created)) as created_time, created
			FROM content a
			WHERE content_id = '$content_id'";

		$query = $this->db->query($textquery);	
		$row = $query->row();
		
		//convert duration
		$duration = $row->duration;
		$seconds = $duration %60;
		$minutes = (floor($duration/60)) % 60;
		$hours = floor($duration/3600);
		
		$duration_text = '';
		if($hours > 0){
			$duration_text = $duration_text . $hours . ' Hour ';
		}
		
		if($minutes > 0){
			$duration_text = $duration_text . $minutes . ' Min ';
		}

		if($seconds > 0){
			$duration_text = $duration_text . $seconds . ' Sec ';
		}
		$row->duration_text = $duration_text;


		$created_time = ($row->created_time);
		$seconds = $created_time %60;
		$minutes = (floor($created_time/60)) % 60;
		$hours = floor($created_time/3600);
		$created_time_text = '';
		
		if($created_time < 86400){
			if($hours > 0){
				$created_time_text = $created_time_text . $hours . ' Hour ';
			}
		
			if($minutes > 0){
				$created_time_text = $created_time_text . $minutes . ' Min ';
			}
		
			$row->created_time_text = $created_time_text . 'ago';
		}else{
			$row->created_time_text = $row->created;
		}
		
		
		//set thumbnail
		if($row->thumbnail){
			$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
		}else{
			$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
		}
		
		//available resolution
		$content_res_list = $this->content_res_list($row->content_id, $device_id,  $row->filename);
		$row->res_list = $content_res_list;
		return $row;
	}

	
	public function content_res_list($content_id, $device_id, $filename){
		$textquery = "
			SELECT 
				a.res_id, c.res_name, bw_min, 1 as available, '' as connection_url,  
				CONCAT(a.res_id, '/', '$filename', '.', video_format) as stream_name
			FROM content_res a, device b, res c
			WHERE content_id = '$content_id'
			AND b.device_id = '$device_id'
			AND b.device_id = a.device_id
			AND c.res_id = a.res_id
			ORDER BY b.device_id, c.order_number
		 	";
			
		if($device_id == "2"){
			$textquery = "
				SELECT 
					a.res_id, c.res_name, bw_min, 1 as available, '' as connection_url,  
					CONCAT(a.res_id, '/iphone/', '$filename', '.', video_format) as stream_name
				FROM content_res a, device b, res c
				WHERE content_id = '$content_id'
				AND b.device_id = '$device_id'
				AND b.device_id = a.device_id
				AND c.res_id = a.res_id
				ORDER BY b.device_id, c.order_number
			 	";
		}	
		$query = $this->db->query($textquery);	
		return $query->result();
	}
	
	
	function content_res_save($content_id, $res_id, $device_id, $input, $status, $script_file, $filesize, $source_type, $streaming_folder){
		$input = mysql_real_escape_string($input);
		$script_file = mysql_real_escape_string($script_file);
		$content_res_id = uniqid(rand(), false);
		
		$script = $this->script_detail($device_id, $res_id, $source_type);
		
		if(!empty($streaming_folder)){
			$script->dest_dir = $streaming_folder . $script->res_path . '/';

			if(!is_dir($script->dest_dir)){
				mkdir($script->dest_dir, 0777, true);
				if(!is_dir($script->dest_dir . 'temp')){
					mkdir($script->dest_dir . 'temp' , 0777, true);
				}
			}
		}
		
		$textquery = "
			INSERT INTO content_res(
				content_res_id, content_id, res_id, device_id, input, status, created, script_file, filesize, video_format, dest_dir
			)VALUES(
				'$content_res_id', '$content_id', '$res_id', '$device_id', '$input', $status, now(), '$script_file', $filesize, '$script->video_format', '$script->dest_dir'
			)
		 	";
		$query = $this->db->query($textquery);	
		//return $this->content_detail($content_id);
	}


	function content_save($customer_id, $title, $description, $filename, $content_category_id, $status, $duration, $video_thumbnail, $live_url, $prod_house_id){
		$title = mysql_real_escape_string($title);
		$description = mysql_real_escape_string($description);
		$filename = mysql_real_escape_string($filename);
		$video_thumbnail = mysql_real_escape_string($video_thumbnail);
		
		$content_id = uniqid(rand(), false);
		$textquery = "
			INSERT INTO content(
				content_id, customer_id, title, description, filename, content_category_id, status, created, video_length, video_thumbnail, live_url, prod_house_id, privacy
			)VALUES(
				'$content_id', '$customer_id', '$title', '$description', '$filename', '$content_category_id', $status, now(), $duration, '$video_thumbnail', '$live_url', '$prod_house_id', '1'
			)
		 	";
		$query = $this->db->query($textquery);	
		return $this->content_detail($content_id, '1');
	}
	
	function content_vod_save($apps_id, $channel_id, $content_id, $product_code, $price, $validity){
		$textquery = "
			SELECT apps_id, channel_id, content_id, product_code, price, validity FROM content_vod
			WHERE apps_id = '$apps_id'
			AND channel_id = '$channel_id'
			AND content_id = '$content_id'
		 	";
		$query = $this->db->query($textquery);	
		if($query->row()){
			return '';
		}else{
			$textquery = "
				INSERT INTO content_vod (apps_id, channel_id, content_id, product_code, price, validity, created)
				VALUES ('$apps_id', '$channel_id', '$content_id', '$product_code', '$price', '$validity', now())
			 	";

			$query = $this->db->query($textquery);	
		}
	}
	
	function content_customer_category_save($content_id, $customer_id){
		$textquery = "
			SELECT  
				customer_category_id 
			FROM customer_member
			WHERE customer_id = '$customer_id' 
		 	";
		$query = $this->db->query($textquery);	
		$category = $query->result();
		if (!$category) {
				$default_category ='standard';
				$textquery = "
					INSERT INTO customer_member (customer_id, customer_category_id)
					VALUES ('$customer_id', '$default_category')
				 	";
				$query = $this->db->query($textquery);	
				$category[0]->customer_category_id = $default_category;
		}	
		foreach($category as $row){
			$textquery = "
				SELECT content_id, customer_category_id
				FROM content_customer_category 
				WHERE content_id = '$content_id'
				AND customer_category_id = '$customer_category_id'
			 	";
			$result = $this->db->query($textquery)->row();	
			if(!$result){
				$textquery = "
					INSERT INTO content_customer_category (content_id, customer_category_id)
					VALUES ('$content_id', '$customer_category_id')
				 	";
	
				$query = $this->db->query($textquery);	
			}
		}
	}
	

	
	function content_status_update($content_id, $status) {		
		$textquery = "
			UPDATE content_res SET status = '$status'
			WHERE content_id = '$content_id'
		 	";
		$query = $this->db->query($textquery);	


		$textquery = "
			UPDATE content SET status = '$status'
			WHERE content_id = '$content_id'
		 	";
		$query = $this->db->query($textquery);	
	}
	
	public function script_detail($device_id, $res_id, $source_type){
		$textquery = "
			SELECT  
				script_id, a.device_id, a.res_id, device_name, res_name, source_type, script_name, script_file, dest_dir, video_format, a.res_path
			FROM script a, device b, res c 
			WHERE a.device_id = '$device_id'
			AND a.res_id = '$res_id' 
			AND lower(source_type) = lower('$source_type')
			AND a.device_id = b.device_id
			AND a.res_id = c.res_id
		 	";

		$query = $this->db->query($textquery);	
		return $query->row();
	}
		
	public function script_list(){
		$textquery = "
			SELECT  
				* 
			FROM script
			ORDER BY device_id
		 	";

		$query = $this->db->query($textquery);	
		return $query->result();
	}		
}

?>
