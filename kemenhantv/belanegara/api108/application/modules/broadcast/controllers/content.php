<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Content extends CI_Controller{
	
	function Content(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('content_model');
        $this->output->set_content_type('application/json');
	}	
	
	
	function content_category_list(){
        $groups_id = $this->input->post("groups_id");
		$content_category_list = $this->content_model->content_category_list($groups_id);
		$response = array(
        	"content_category_list" => $content_category_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	
	function content_list(){
        $page = (int)($this->input->post("page") == "" ? 0 : $this->input->post("page"));
		$page = 0;
		$limit = 12 ;

        $groups_id = $this->input->post("groups_id");
        $prod_house_id = "";
        $keyword = "";
		$content_category_id = "";
		
		$content_list = $this->content_model->content_list($page, $limit, $keyword, $content_category_id, $prod_house_id, $groups_id);
		$response = array(
        	"content_list" => $content_list
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	
	function content_update(){
        $apps_id = $this->input->post("apps_id");
        $content_id = $this->input->post("content_id");
        $title = $this->input->post("title");
        $content_category_id = $this->input->post("content_category_id");
        $groups_id = $this->input->post("groups_id");
        $account_id = $this->input->post("account_id");

		$this->content_model->content_update($content_id, $title, $content_category_id);
		$this->content_model->groups_content_insert($content_id, $groups_id, $account_id);
		$this->content_model->content_apps_insert($content_id, $apps_id, $account_id);
		$response = array(
        	"rcode" => 1
        );
        
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	
	function content_detail(){
        $content_id = $this->input->post("content_id");
        $device_id = $this->input->post("device_id");

		$content = $this->content_model->content_detail($content_id, $device_id);
		
        $this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($content));
	}
	
	


    
}
