<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Account extends CI_Controller{
	
	function Account(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->model('account_m');
		//$this->load->model('statistic_model');
		//$this->output->set_content_type('application/json');
	}	
	
	
  	function login(){
    	$email = $this->input->post("email");
    	$password = $this->input->post("password");
		$longitude = $this->input->post("longitude");
		$latitude = $this->input->post("latitude");
    
    	$apps_id = $this->input->post("apps_id");
    	$version = $this->input->post("version");
    	$customer_id = $this->input->post("customer_id");
    	$device_id = $this->input->post("device_id");
    	$screen_size = $this->input->post("screen_size");
    	$remote_host = $_SERVER['REMOTE_ADDR'];
    	$mobile_id = $this->input->post("mobile_id");
    	$status = 1;

		if (empty($longitude))  $longitude= '0';		
		if (empty($latitude))  $latitude= '0';									
	
  		$account = $this->account_m->login($email, $password, $latitude, $longitude);
	
		if(!$account){
			$account = array(
				"rcode" => 0,
				"message" => "Invalid combination usr/password"
			);
		}else{
			$account->rcode = 1;
			$account->message = "Successfully Login";
		}
	
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($account));
  	}



}
