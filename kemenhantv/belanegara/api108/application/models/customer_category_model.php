<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
	Author: htridianto
	Date: nov, 11 2014 14:20 
	Customer category related tables: customer_category, groups_apps, groups_customer_category
*/

class Customer_category_model extends MY_Model {
	
	protected $table 	= 'customer_category';
	protected $key		= 'customer_category_id';

	public function __construct()
	{
		parent::__construct();
	}

	// add customer as member of customer category
	public function add_customer_member($group_id, $customer_id, $allow_access, $allow_upload, $allow_share){
		$data = array(
			'customer_category_id' => $group_id,
		  	'customer_id' => $customer_id,
		  	'allow_access' => $allow_access,
		  	'allow_upload' => $allow_upload,
		  	'allow_share' => $allow_share,
		);
		$this->db->insert('customer_member', $data);
	}	

	// get customer members of group by application 
	public function get_member_of_group($app_id, $customer_id){
		$sql = "
			SELECT 
				-- a.apps_id, b.groups_id, b.groups_name, 
				c.customer_category_id AS group_id, d.category_name AS group_name,
				e.allow_access, e.allow_upload, e.allow_share				
			FROM 
				groups_apps a 
				JOIN groups b ON (a.groups_id = b.groups_id) 
				JOIN groups_customer_category c on (b.groups_id = c.groups_id)
				JOIN customer_category d on (c.customer_category_id = d.customer_category_id)
				JOIN customer_member e on (d.customer_category_id = e.customer_category_id)
			WHERE a.apps_id = '{$app_id}' AND e.customer_id = '{$customer_id}'	
			GROUP BY d.category_name
		";
  		$query = $this->db->query($sql);
  		$rows = $query->result();
  		if($rows){
  			foreach ($rows as $row) {
  				$row->allow_access = ($row->allow_access == 1) ? 'true' : 'false';
  				$row->allow_upload = ($row->allow_upload == 1) ? 'true' : 'false';
  				$row->allow_share = ($row->allow_share == 1) ? 'true' : 'false';
  			}
  		}
		return $query->result();
	}

	// get customer groups by application 
	public function get_groups($app_id){
		$sql = "
			SELECT a.apps_id, a.groups_id, b.groups_name
			FROM groups_apps a JOIN groups b ON (a.groups_id = b.groups_id) 
			WHERE a.apps_id = '{$app_id}'	
			GROUP BY a.groups_id, b.groups_name	
			ORDER BY b.groups_name;
		";
  		$query = $this->db->query($sql);	
		return $query->result();
	}

	// get customer categories groups by application 
	public function get_categories_groups($app_id, $group_id = false){
		$sql = "
			SELECT 
				a.apps_id, b.groups_id, b.groups_name, 
				c.customer_category_id, d.category_name
			FROM 
			groups_apps a 
			JOIN groups b ON (a.groups_id = b.groups_id) 
			JOIN groups_customer_category c on (b.groups_id = c.groups_id)
			JOIN customer_category d on (c.customer_category_id = d.customer_category_id)
			WHERE a.apps_id = '{$app_id}'
		";
		if($group_id){
			$sql .= " AND a.groups_id = '{$group_id}' ";
		}
		$sql .= "
			GROUP BY b.groups_id, c.customer_category_id
			ORDER BY b.groups_name, d.category_name
		";
  		$query = $this->db->query($sql);	
		return $query->result();
	}
}

?>
