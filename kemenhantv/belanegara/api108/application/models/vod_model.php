<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vod_model extends MY_Model {
	
	function Vod_model(){
		parent::__construct();
		$this->load->model('apps_model');
	}	
	

	public function channel_category_list($apps_id, $customer_id, $lang){
		return $this->apps_model->apps_category_list($apps_id, $customer_id, $lang);
	}



	public function sub_category_list($apps_id, $parent_category_id, $channel_category_id, $lang){
		$textquery = "
			SELECT apps_id, parent_category_id, channel_category_id, category_name, thumbnail, order_number FROM
			(	
				SELECT 
					a.apps_id, 'live_channel' as parent_category_id, b.channel_category_id, b.category_name, b.thumbnail, b.order_number					
				FROM apps_channel_category a, channel_category b, channel c, apps_category d
				WHERE a.apps_id = '$apps_id'
				AND a.channel_category_id = b.channel_category_id
				AND a.channel_id = c.channel_id
				AND c.channel_type_id in (1,2)
				AND a.apps_id = d.apps_id
				AND a.channel_category_id = d.channel_category_id
				GROUP BY b.channel_category_id, b.category_name
				UNION
				SELECT 
					a.apps_id, a.channel_category_id as parent_category_id, a.channel_id as channel_category_id, alias as category_name, thumbnail, d.order_number
				FROM apps_channel_category a, apps_channel b, apps_category c, channel d
				WHERE a.apps_id = '$apps_id'
				AND d.status = 1
				AND a.apps_id = b.apps_id
				AND a.apps_id = c.apps_id
				AND a.channel_id = b.channel_id
				AND a.channel_category_id = c.channel_category_id
				AND a.channel_id = d.channel_id
			) sub_menu
			WHERE apps_id = '$apps_id'
			";
		
		if(!empty($parent_category_id)){
			$textquery .= " AND parent_category_id = '$parent_category_id' ";
		}	
		
		if(!empty($channel_category_id)){
			$textquery .= " AND channel_category_id = '$channel_category_id' ";
		}
		
			
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;
	}

	/*
	public function content_channel_list($page, $limit, $apps_id, $parent_category_id, $channel_category_id, $genre_id, $crew_id, $search_key, $lang){
		$textquery = "
			SELECT 
				a.apps_id, a.channel_id, 'live_channel' as parent_category_id, a.channel_category_id as channel_category_id, a.channel_id as content_id, 'live_channel' as content_type,
				b.alias as title, b.channel_descr as description, b.thumbnail
			FROM apps_channel_category a, channel b, channel_category c, apps_category d
			WHERE a.apps_id = '$apps_id'
			AND b.channel_type_id in (1,2)
			AND a.channel_id = b.channel_id
			AND a.channel_category_id = c.channel_category_id
			AND a.apps_id = d.apps_id
			AND a.channel_category_id = d.channel_category_id
		";
		
		if(!empty($channel_category_id)){
			$textquery .= " AND a.channel_category_id = '$channel_category_id' ";
		}
		
		if(!empty($genre_id)){
		}
		
		if(!empty($crew_id)){
		}
		
		if(!empty($search_key)){
			$textquery .= " AND LOWER(b.alias) like LOWER('%$search_key%') ";
		}
		
		$textquery .= " ORDER BY b.alias LIMIT $page, $limit ";
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;
		
	}
	*/

	public function content_vod_list($page, $limit, $apps_id, $channel_category_id, $channel_id, $genre_id, $crew_id, $search_key, $lang, $customer_id){
			$textquery = "
				SELECT 
					apps_id, channel_category_id, channel_id, channel_type_id, content_id, content_type,
					title, thumbnail, description, prod_year, video_duration			
				FROM vcontent
				WHERE apps_id = '$apps_id'
			";
					
			if(empty($customer_id)){
				$textquery .= " AND privacy = 0 ";
			}else{
				$textquery .= " AND (privacy = 0 OR (privacy = 1 AND customer_id = '$customer_id')) ";
			}
		
			if(empty($channel_category_id) && empty($channel_id)){
				//hide trailer gallery on landing video list
				$textquery .= " AND channel_id != '45063004540979416d068' ";
			}
		
			if(!empty($channel_id)){
				$textquery .= " AND channel_id = '$channel_id' ";
			}
		
			if(!empty($genre_id)){
				$textquery .= " 
					AND content_id in (
						SELECT content_id FROM content_genre y
						WHERE y.genre_id = '$genre_id'
					) 
				";
			}
		
			if(!empty($crew_id)){
				$textquery .= " 
					AND content_id in (
						SELECT content_id FROM content_crew y
						WHERE y.crew_id = '$crew_id'
					) 
				";
			}
		
			if(!empty($search_key)){
				$textquery .= " 
					AND (
						LOWER(title) like LOWER('%$search_key%')
						OR content_id in (
							SELECT content_id FROM content_crew x, crew y
							WHERE LOWER(y.crew_name) like LOWER('%$search_key%') 
							AND x.crew_id = y.crew_id
						) 
					)
				";
			}
		
			
			$textquery .= " GROUP BY content_id ORDER BY order_number, created DESC, title LIMIT $page, $limit ";
		
			$query = $this->db->query($textquery);	
			$rows =  $query->result();
			foreach($rows as $row){
				if($row->thumbnail){
					$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
				}else{
					$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
				}
				
				if(!empty($search_key)){
					$row->crew_category_list = $this->content_actor_list($row->content_id);
					$row->genre_list = $this->content_genre($row->content_id);
				}
			}
			
			//if(!empty($customer_id)){
			//	$private_vod = $this->content_private_vod_list($page, $limit, $apps_id, $parent_category_id, $channel_category_id, $genre_id, $crew_id, $search_key, $lang, $customer_id);
			//	return array_merge($rows, $this->content_private_vod_list($page, $limit, $apps_id, $parent_category_id, $channel_category_id, $genre_id, $crew_id, $search_key, $lang, $customer_id));
			//}else{
			return $rows;
			//}
			
			
		//}

		
	}
	
	/*
	public function content_private_vod_list($page, $limit, $apps_id, $parent_category_id, $channel_category_id, $genre_id, $crew_id, $search_key, $lang, $customer_id){
		$textquery = "
			SELECT 
				a.apps_id, a.channel_id, c.channel_category_id as parent_category_id, a.channel_id as channel_category_id, a.content_id, 'content' as content_type,
				b.title, b.description, b.video_thumbnail as thumbnail, b.prod_year, b.video_length as video_duration				
			FROM content_vod a, content b, apps_channel_category c
			WHERE a.apps_id = '$apps_id'
			AND a.content_id = b.content_id
			AND a.apps_id = c.apps_id
			AND a.channel_id = c.channel_id
			AND b.privacy = 1
			AND a.content_id in (
					select content_id 
					from content_customer_category x, customer_member y 
					where y.customer_id='$customer_id' 
					and x.customer_category_id = y.customer_category_id
					)
		";
		
		if(!empty($parent_category_id)){
			$textquery .= " AND c.channel_category_id = '$parent_category_id' ";
		}
	
		if(!empty($channel_category_id)){
			$textquery .= " AND a.channel_id = '$channel_category_id' ";
		}
	
		if(!empty($genre_id)){
			$textquery .= " 
				AND a.content_id in (
					SELECT content_id FROM content_genre y
					WHERE y.genre_id = '$genre_id'
				) 
			";
		}
	
		if(!empty($crew_id)){
			$textquery .= " 
				AND a.content_id in (
					SELECT content_id FROM content_crew y
					WHERE y.crew_id = '$crew_id'
				) 
			";
		}
	
		if(!empty($search_key)){
			$textquery .= " 
				AND (
					LOWER(b.title) like LOWER('%$search_key%')
					OR a.content_id in (
						SELECT content_id FROM content_crew x, crew y
						WHERE LOWER(y.crew_name) like LOWER('%$search_key%') 
						AND x.crew_id = y.crew_id
					) 
				)
			";
		}
		
	
		$textquery .= " GROUP BY content_id ORDER BY b.created DESC, b.title LIMIT $page, $limit ";
	
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
			
			if(!empty($search_key)){
				$row->crew_category_list = $this->content_actor_list($row->content_id);
				$row->genre_list = $this->content_genre($row->content_id);
			}
		}
		return $rows;
		
	}
	*/
	
	//All channel and content
	/*
	public function content_list($page, $limit, $apps_id, $parent_category_id, $channel_category_id, $genre_id, $crew_id, $search_key, $lang){
		$sub_category_list = $this->sub_category_list($apps_id, $parent_category_id, $channel_category_id, $lang);
		foreach($sub_category_list as $channel_category){
			$textquery = "
				SELECT 
					apps_id, channel_id, parent_category_id, parent_category_name, channel_category_id, category_name, content_id, content_type,
					title, description, thumbnail
				FROM (
					SELECT 
						a.apps_id, a.channel_id, c.channel_category_id as parent_category_id, d.category_name as parent_category_name, a.channel_id as channel_category_id, e.alias as category_name, a.content_id, 'content' as content_type,
						b.title, b.description, b.video_thumbnail as thumbnail
					FROM content_vod a, content b, apps_channel_category c, channel_category d, channel e
					WHERE a.apps_id = '$apps_id'
					AND a.content_id = b.content_id
					AND a.apps_id = c.apps_id
					AND a.channel_id = c.channel_id
					AND c.channel_category_id = d.channel_category_id
					AND a.channel_id = e.channel_id
					UNION
					SELECT 
						a.apps_id, a.channel_id, 'live_channel' as parent_category_id, 'Live Channel' as parent_category_name, a.channel_category_id as channel_category_id, c.category_name, a.channel_id as content_id, 'live_channel' as content_type,
						b.alias as title, b.channel_descr as description, b.thumbnail
					FROM apps_channel_category a, channel b, channel_category c
					WHERE a.apps_id = '$apps_id'
					AND b.channel_type_id in (1,2)
					AND a.channel_id = b.channel_id
					AND a.channel_category_id = c.channel_category_id
				) content
				WHERE apps_id = '$apps_id'
				AND parent_category_id = '$channel_category->parent_category_id'
				AND channel_category_id = '$channel_category->channel_category_id'
			 	";
			
			if(!empty($search_key)){
				$textquery .= " 
					AND LOWER(title) like LOWER('%$search_key%') 
				";
			}
			
			$textquery .= " ORDER BY parent_category_id, channel_category_id LIMIT $page, $limit ";
			
			$query = $this->db->query($textquery);	
			$rows =  $query->result();
			foreach($rows as $row){
				if($row->thumbnail){
					$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
				}else{
					$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
				}
				
				if(!empty($search_key)){
					$row->crew_category_list = $this->content_actor_list($row->content_id);
					$row->genre_list = $this->content_genre($row->content_id);
				}
				
			}
			$channel_category->content_list = $rows;
		}
		
		
		return $sub_category_list;
	}
	*/
	
	
	public function play_list($apps_id, $channel_id, $content_id, $customer_id){
		$textquery = "
				SELECT 
					content_id, product_code, price 
				FROM content_vod 
				WHERE apps_id = '$apps_id'
				AND content_id = '$content_id'
				LIMIT 1
		 	";
		$query = $this->db->query($textquery);	
		$content_vod =  $query->row();
		
		$auth = 1;
		if($content_vod && $content_vod->price > 0){
			$textquery = "
					SELECT 
						apps_id, content_id, customer_id
					FROM d_customer_transaction 
					WHERE apps_id = '$apps_id'
					AND content_id = '$content_id'
					AND customer_id = '$customer_id'
			 	";
			$query = $this->db->query($textquery);	
			$transaction = $query->row();
			if($transaction){
				$auth = 1;
			}else{
				$auth = 0;
			}
		}

		$textquery = "
			select content_id, '$auth' as auth, '$content_vod->product_code' as product_code, '$content_vod->price' as price, title, video_duration, filename, thumbnail, message, message_url, message_status, channel_id from (
				select  
					b.content_id, title, video_length as video_duration, b.filename, b.video_thumbnail as thumbnail, -1 as order_number,
					b.message, b.message_url, b.message_status, '$channel_id' as channel_id
				from content b
				WHERE content_id = '$content_id'
				UNION ALL
				select 
					b.content_id, title, video_length as video_duration, b.filename, b.video_thumbnail as thumbnail, order_number, 
					b.message, b.message_url, b.message_status, '$channel_id' as channel_id
				from content_related a, content b  
				where a.content_id='$content_id' 
				AND a.content_related_id = b.content_id
			) x
			group by content_id, title, video_duration, filename, thumbnail
			order by order_number
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		
		return $rows;
	}




	public function channel_detail($apps_id, $channel_id, $timezone) {		
		$textquery = "
			SELECT 
				a.channel_id, channel_name, alias, thumbnail, channel_type_id, status, channel_descr
		 	FROM apps_channel a, channel b WHERE a.channel_id = '$channel_id'
			AND apps_id = '$apps_id'
			AND a.channel_id = b.channel_id
			";
		$query = $this->db->query($textquery);	
		$row = $query->row();
		if($row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
			
			
	        $start_date = date("Y-m-d", strtotime ( '-7 day', strtotime (date("Y-m-d")))) ;
	        $end_date = date("Y-m-d") ;

	    	$row->schedule_list = $this->channel_schedule($channel_id, 1, $start_date, $end_date, $timezone);
			
		}
		return $row;
	}
	
	
	public function channel_schedule($channel_id, $schedule_type_id, $start_date, $end_date, $timezone) {		
		$textquery = "
			SELECT 
				distinct DATE_FORMAT(start_date_time, '%Y-%m-%d') as sdate, DATE_FORMAT(start_date_time, '%d %M %Y') as schedule_date 
			FROM schedule a
			WHERE a.channel_id = '$channel_id'
			AND (DATE_FORMAT(CONVERT_TZ(start_date_time,'+07:00','$timezone'), '%Y-%m-%d') between '$start_date' and '$end_date')
			AND	schedule_type_id = $schedule_type_id
		 	";
		$textquery = $textquery . " ORDER BY start_date_time DESC";

		$query = $this->db->query($textquery);	
		$schedule_list =  $query->result();
		foreach($schedule_list as $schedule){
			$textquery = "
				SELECT 
					a.schedule_id, a.channel_id, a.program_id, a.duration, b.program_name,
					b.thumbnail, CONVERT_TZ(a.start_date_time,'+07:00','$timezone') start_date_time,
					CONCAT(SUBSTRING(DATE_FORMAT(a.start_date_time, '%W'), 1, 3), ' ', DATE_FORMAT(a.start_date_time, '%h:%i %p')) as start_time_text,
					DATE_FORMAT(CONVERT_TZ(a.start_date_time,'+07:00','$timezone'), '%h:%i %p') as end_time_text,
					schedule_type_id, DATE_FORMAT(CONVERT_TZ(now(),'+07:00','$timezone'), '%W %h:%i %p') as server_time, 1 as enable_record, 0 as enable_remove, 0 enable_play
				FROM schedule a, program b
				WHERE a.program_id = b.program_id 
				AND a.channel_id = '$channel_id'
				AND DATE_FORMAT(CONVERT_TZ(a.start_date_time,'+07:00','$timezone'), '%Y-%m-%d') = '$schedule->sdate'
				AND	schedule_type_id = $schedule_type_id
			 	";

			$textquery = $textquery . " ORDER BY start_date_time ";
			$query = $this->db->query($textquery);	
			$rows =  $query->result();
			$schedule->schedule_list = $rows;

		}
		return $schedule_list;
	}
	

	public function content_detail($apps_id, $channel_id, $content_id) {		
		$textquery = "
			SELECT 
				a.channel_id, c.content_id, c.title, c.video_length as video_duration, c.filename, c.video_thumbnail as thumbnail,
				c.description, c.prod_year, c.trailer_id, a.price,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_id' AND ulike=1) as ulike,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_id' AND ulike=-1) as unlike,
				(SELECT count(content_id) FROM content_comment WHERE content_id='$content_id') as comment				
			from content_vod a, content c 
			WHERE a.apps_id = '$apps_id'
			AND a.channel_id = '$channel_id'
			AND c.content_id = '$content_id'
			AND a.content_id = c.content_id
		 	";

		$query = $this->db->query($textquery);	
		$row =  $query->row();
		if($row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
			$row->crew_category_list = $this->content_actor_list($row->content_id);
			$row->genre_list = $this->content_genre($row->content_id);
			$row->photo_list = $this->content_photo($row->content_id);
			$row->related_list = $this->content_related_list($row->content_id);
		}
		return $row;
	}
	

	public function trailer_detail($content_id) {		
		$textquery = "
			SELECT 
				c.content_id, c.title, c.video_length as video_duration, c.video_thumbnail as thumbnail
			from content c 
			WHERE c.content_id = '$content_id'
		 	";

		$query = $this->db->query($textquery);	
		$row =  $query->row();
		if($row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $row;
	}
	

	public function content_actor_list($content_id) {
		$textquery = "
			SELECT 
				a.crew_category_id, b.category_name FROM content_crew a, crew_category b
			WHERE a.content_id='$content_id'
			AND a.crew_category_id = b.crew_category_id
			GROUP BY a.crew_category_id, b.category_name
			ORDER BY b.order_number;
		 	";
		
		$query = $this->db->query($textquery);	
		$crew_category_rows =  $query->result();

		foreach($crew_category_rows as $crew_category){
			$textquery = "
				SELECT 
					b.crew_id, b.crew_name 
				FROM content_crew a, crew b
				WHERE a.content_id='$content_id'
				AND a.crew_category_id = '$crew_category->crew_category_id'
				AND a.crew_id = b.crew_id
			 	";
		
			$query = $this->db->query($textquery);	
			$crew_rows =  $query->result();
			$crew_category->crew_list = $crew_rows;
		}
		

		return $crew_category_rows;				
	}
	
	
	public function content_related_list($content_id) {
		$textquery = "
			SELECT 
				a.content_id, a.content_related_id, b.title, b.video_length as video_duration, b.video_thumbnail as thumbnail
			FROM content_related a, content b
			WHERE a.content_id='$content_id'
			AND a.content_related_id = b.content_id
			ORDER BY a.order_number, b.title;
		 	";
		
		$query = $this->db->query($textquery);	
		$rows =  $query->result();

		foreach($rows as $row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;				
	}
	
	
	public function content_related_detail($content_id, $content_related_id) {		
		$textquery = "
			SELECT 
				a.content_id, a.content_related_id, c.title, c.video_length as video_duration, c.video_thumbnail as thumbnail,
				c.description, c.prod_year, c.trailer_id,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_related_id' AND ulike=1) as ulike,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_related_id' AND ulike=-1) as unlike,
				(SELECT count(content_id) FROM content_comment WHERE content_id='$content_related_id') as comment				
			from content_related a, content c 
			WHERE a.content_id = '$content_id'
			AND a.content_related_id = '$content_related_id'
			AND a.content_related_id = c.content_id
		 	";

		$query = $this->db->query($textquery);	
		$row =  $query->row();
		if($row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
			$row->crew_category_list = $this->content_actor_list($row->content_related_id);
			$row->genre_list = $this->content_genre($row->content_related_id);
			$row->photo_list = $this->content_photo($row->content_related_id);
		}
		return $row;
	}
	

	
	
	
	public function content_genre($content_id) {
		$textquery = "
			SELECT a.genre_id, b.genre_name
			FROM content_genre a, genre b
			WHERE a.content_id = '$content_id'
			AND a.genre_id = b.genre_id
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		return $rows;				
	}
	
	
	public function content_photo($content_id) {
		$textquery = "
			SELECT a.content_id, a.filename
			FROM content_photo a
			WHERE a.content_id = '$content_id'
		 	";

		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		foreach($rows as $row){
			if($row->filename){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->filename;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $rows;				
	}
	
	
	public function content_like($content_id, $customer_id, $like) {
		$textquery = "
			SELECT content_id, customer_id
			FROM content_like
			WHERE content_id = '$content_id'
			AND customer_id = '$customer_id'
		 	";

		$query = $this->db->query($textquery);	
		$row =  $query->row();
		
		if($row){
			$textquery = "
				UPDATE content_like
				SET ulike = $like,
				created = now()
				WHERE content_id = '$content_id'
				AND customer_id = '$customer_id'
			 	";
			$query = $this->db->query($textquery);	
		}else{
			$textquery = "
				INSERT INTO content_like (content_id, customer_id, ulike, created)
				VALUES ('$content_id', '$customer_id', '$like', now())
			 	";
			$query = $this->db->query($textquery);	
		}
		
		$textquery = "
			SELECT 
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_id' AND ulike=1) as ulike,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_id' AND ulike=-1) as unlike
		 	";

		$query = $this->db->query($textquery);	
		$row =  $query->row();

		return $row;				
	}
	
	
	public function content_comment($content_id, $customer_id, $comment) {
		$comment_id = uniqid(rand(), false);
		$textquery = "
			INSERT INTO content_comment (comment_id, content_id, customer_id, comment_text, created)
			VALUES ('$comment_id', '$content_id', '$customer_id', '$comment', now())
		 	";
		$query = $this->db->query($textquery);	
		
		$textquery = "
				SELECT count(content_id) as comment FROM content_comment WHERE content_id='$content_id'
		 	";

		$query = $this->db->query($textquery);	
		$row =  $query->row();
		
		return $row;
	}


	public function content_comment_list($page, $limit, $content_id) {
		$textquery = "
			SELECT 
				comment_id, a.content_id, a.customer_id, a.comment_text, a.created,
				b.display_name
			FROM content_comment a, customer b
			WHERE content_id='$content_id'
			AND a.customer_id = b.customer_id
			ORDER BY a.created DESC
			LIMIT $limit
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		return $rows;
	}


	public function customer_balance($customer_id, $password) {	
		$password = md5('mat123' . $password . 'kra');	
		$textquery = "
			SELECT 
				mobile_id, customer_id, email, user_id, first_name, last_name, 
				display_name, status, verify_status, credit_balance
  			FROM customer
  			WHERE customer_id = '$customer_id'
  			AND password = '$password'
  			AND status = 1
			";
		$query = $this->db->query($textquery);	
		$row = $query->row();
		return $row;
	}
	
	
	public function vod_price($apps_id, $channel_id, $content_id) {		
		$textquery = "
			SELECT 
				a.channel_id, c.content_id, c.title, c.video_length as video_duration, c.filename, c.video_thumbnail as thumbnail,
				c.description, c.prod_year, c.trailer_id, a.price				
			from content_vod a, content c 
			WHERE a.apps_id = '$apps_id'
			AND a.channel_id = '$channel_id'
			AND c.content_id = '$content_id'
			AND a.content_id = c.content_id
		 	";

		$query = $this->db->query($textquery);	
		$row =  $query->row();
		return $row;
	}
	


	public function vod_buy($apps_id, $content_id, $customer_id, $mobile_id) {
		$transaction_code = substr(str_shuffle(MD5(microtime())), 0, 6);
		
		
		$textquery = "
			SELECT 
				apps_id, a.channel_id, b.alias, b.channel_type_id, c.content_id, c.title, product_code, a.price
			FROM content_vod a, channel b, content c
			WHERE a.content_id='$content_id'
			AND apps_id = '$apps_id'
			AND a.channel_id = b.channel_id
			AND a.content_id = c.content_id
			LIMIT 1
		 	";
		$query = $this->db->query($textquery);	
		$content =  $query->row();
		
		
		$textquery = "
			INSERT INTO d_customer_transaction 
				(apps_id, customer_id, content_id, transaction_code, product_code, transaction_date, price, mobile_id, channel_id, content_title, channel_alias, channel_type_id) 
			VALUES 
				('$apps_id', '$customer_id', '$content_id', '$transaction_code', '$content->product_code', now(), $content->price, '$mobile_id', '$content->channel_id', '$content->title', '$content->alias', $content->channel_type_id)
		 	";
		$query = $this->db->query($textquery);	
		
		return $transaction_code;
	}


}

?>
