<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Channel_model extends MY_Model {
	


	public function channel_detail($apps_id, $channel_id) {
		$textquery = "
			SELECT 
				a.channel_id, channel_name, alias, thumbnail, channel_type_id, status, 
				vid_subs_appl, msg_server_id, msg_subs_appl
		 	FROM apps_channel a, channel b WHERE a.channel_id = '$channel_id'
			AND apps_id = '$apps_id'
			AND a.channel_id = b.channel_id
			";
		$query = $this->db->query($textquery);	
		$row = $query->row();
		if($row){
			if($row->thumbnail){
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=". $row->thumbnail;
			}else{
				$row->thumbnail = "http://" . $_SERVER["HTTP_HOST"] . "/media/thumbnail?id=" . $this->config->item('img_not_found') ;
			}
		}
		return $row;
	}
	
	
	public function channel_server_cluster($channel_id) {
		$textquery = "
			SELECT 
				b.public_ip, polling
			FROM server_cluster a, server b
			WHERE a.channel_id = '$channel_id'
			AND a.server_id = b.server_id
			LIMIT 1 
			";
		$query = $this->db->query($textquery);	
		$row = $query->row();
		return $row;
	}
	
	
	
	public function channel_res_detail($channel_id, $device_id, $res_id){
		$textquery = "
			SELECT channel_id, device_id, res_id 
			FROM channel_res 
			WHERE channel_id='$channel_id' 
			AND device_id='$device_id' 
			AND res_id='$res_id'
			";
		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	
	public function channel_schedule($channel_id, $schedule_type_id){
		$textquery = "
			SELECT channel_id, device_id, res_id 
			FROM channel_res 
			WHERE channel_id='$channel_id' 
			AND device_id='$device_id' 
			AND res_id='$res_id'
			";
		$query = $this->db->query($textquery);	
		return $query->row();
	}
	
	
	public function add_favorite($apps_id, $channel_id, $customer_id){
		$textquery = "
			INSERT INTO channel_favorite (apps_id, channel_id, customer_id) 
			VALUES ('$apps_id', '$channel_id', '$customer_id')
			";
		$query = $this->db->query($textquery);	
	}
	
	
	public function remove_favorite($apps_id, $channel_id, $customer_id){
		$textquery = "
			DELETE FROM channel_favorite 
			WHERE apps_id = '$apps_id'
			AND channel_id = '$channel_id'
			AND customer_id = '$customer_id'
			";
		$query = $this->db->query($textquery);	
	}
	
	
	
	
	
	
	
}

?>
