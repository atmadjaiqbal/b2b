﻿<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Metadata_model extends MY_Model {
	
	
	
	public function get_content($content_id){

		$textquery = "
			SELECT content_id, title, description, video_thumbnail, a.content_category_id, b.category_name, b.option content_type,  prod_house_id, prod_year, created, trailer_id,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_id' AND ulike=1) as ulike,
				(SELECT count(content_id) FROM content_like WHERE content_id='$content_id' AND ulike=-1) as unlike,
				(SELECT count(content_id) FROM content_comment WHERE content_id='$content_id') as comment							
			
			FROM content  a
			LEFT JOIN content_category b on a.content_category_id = b.content_category_id
			WHERE content_id = '$content_id'
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->row();
		$rows->crew = array();
		$rows->genre = array();
		$rows->related = array();
		$rows->photo = array();
		$rows->trailer = array();
		$rows->comments = array();
		
		$rows->video_thumbnail = 'http://moviebay2.ibolz.tv/admin/media/thumbnail/'.$rows->video_thumbnail.'?w=200' ;
		if ($rows) {
			$textquery = "
				SELECT 
						a.crew_id, b.crew_name, a.crew_category_id, c.category_name
				FROM content_crew a 
					INNER JOIN crew b on a.crew_id = b.crew_id 
					INNER JOIN crew_category c on a.crew_category_id = c.crew_category_id
				WHERE content_id = '$content_id'
				ORDER BY c.order_number
			 	";
			$query = $this->db->query($textquery);	
			$crews =  $query->result();
			$director =  array();
			$producer =  array();
			$cast =  array();
			$writer	 =  array();

			if ($crews) {
				
				foreach($crews as $crew){				
					if ($crew->crew_category_id =='director') {
						$director[] = array('crew_id' =>  $crew->crew_id, 'crew_name' => $crew->crew_name, 'crew_category_id' => $crew->crew_category_id, 'category_name' =>$crew->category_name);

					}	else if ($crew->crew_category_id =='producer') {
						$producer[] = array('crew_id' =>  $crew->crew_id, 'crew_name' => $crew->crew_name, 'crew_category_id' => $crew->crew_category_id, 'category_name' =>$crew->category_name);

					}	else if ($crew->crew_category_id =='cast') {
						$cast[] = array('crew_id' =>  $crew->crew_id, 'crew_name' => $crew->crew_name, 'crew_category_id' => $crew->crew_category_id, 'category_name' =>$crew->category_name);						

					}	else if ($crew->crew_category_id =='writer') {
						$writer[] = array('crew_id' =>  $crew->crew_id, 'crew_name' => $crew->crew_name, 'crew_category_id' => $crew->crew_category_id, 'category_name' =>$crew->category_name);
					}	
				}
			}

			$rows->crew = array('director' => $director, 'producer' => $producer, 'cast' => $cast, 'writer' => $writer);

			$textquery = "
				SELECT 
					a.genre_id, b.genre_name
				FROM content_genre a 
					INNER JOIN genre b on a.genre_id = b.genre_id 
				WHERE content_id = '$content_id'
				ORDER BY b.genre_name

			 	";
			$query = $this->db->query($textquery);	
			$genre =  $query->result();
			$rows->genre = $genre;

			$textquery = "
				SELECT 
					a.content_related_id content_id, title, video_thumbnail, prod_year, created 
				FROM content_related a 
					INNER JOIN content b on a.content_related_id = b.content_id 
				WHERE a.content_id = '$content_id'
				ORDER BY a.order_number
			 	";
			$query = $this->db->query($textquery);	
			$related =  $query->result();
			$rows->related = $related;
			
			if ($rows->content_type =='channel') {
				$today = date("Ymd"); 
				$textquery = "
					SELECT 
						`program-name` title, concat(concat(substr(start_time, -6,2), ':') , substr(start_time, -4,2)) start_time
					FROM epg 
					WHERE date ='20140905'
					ORDER BY start_time
				 	";
				$query = $this->db->query($textquery);	
				$related =  $query->result();
				$rows->related = $related;
			}
			
			$textquery = "
				SELECT 
					filename
				FROM content_photo a 
				WHERE a.content_id = '$content_id'
				ORDER BY filename
			 	";
			$query = $this->db->query($textquery);	
			$photos =  $query->result();
			$content_photo = array();
			
			if ($photos) {
				foreach($photos as $photo){				
					$content_photo[] = 'http://moviebay2.ibolz.tv/admin/media/thumbnail/'.$photo->filename.'?w=200' ;
				}
			}
			$rows->photo = $content_photo;
			
			if (!empty($rows->trailer_id)) {
				$textquery = "
					SELECT content_id, title, video_thumbnail, a.content_category_id, b.category_name, b.option content_type,  'slug' as slug
					
					FROM content  a
					LEFT JOIN content_category b on a.content_category_id = b.content_category_id
					WHERE content_id = '". $rows->trailer_id ."'
				 	";

				$query = $this->db->query($textquery);	
				$trailer =  $query->row();
				$rows->trailer = $trailer;
				if (!empty($trailer)) {
					$slug = strtolower($trailer->title);
					$slug  = str_replace(" ","-",$slug );
					$trailer->slug = $slug ;
				}
			}
			
			$textquery = "
				SELECT 
					comment_id, content_id, a.customer_id, display_name, comment_text, created
				FROM content_comment a 
					LEFT JOIN customer b on a.customer_id = b.customer_id
				WHERE a.content_id = '$content_id'
				ORDER BY a.created desc
			 	";
			$query = $this->db->query($textquery);	
			$comments =  $query->result();
			$rows->comments = $comments;			
		}


		return $rows;
	}
	
	function content_list($search_key, $sort_key, $sort_type, $offset, $limit){
		$search = ( $search_key ) ? 	" WHERE title like '%". $search_key."%'" : "";
		$order_key =  ( $sort_key ) ? 	$sort_key : " title " ;
		$order_type =  ( $sort_type ) ? 	$sort_type : " ASC " ;
		
		
		
		$textquery = "
			SELECT 
					content_id, title, description, video_thumbnail, a.content_category_id, b.category_name, b.option content_type, prod_year, created 
			FROM content a
			LEFT JOIN content_category b on a.content_category_id = b.content_category_id
			$search 
			ORDER BY $order_key  $order_type
			LIMIT $offset, $limit

		 	";
		//echo $textquery;
		$query = $this->db->query($textquery);	
		$rows =  $query->result();		
		if ($rows) {
			foreach($rows as $row ){
				$row->video_thumbnail = 'http://moviebay2.ibolz.tv/admin/media/thumbnail/'.$row->video_thumbnail.'?w=200' ;

				$textquery = "
					SELECT 
							a.crew_id, b.crew_name, a.crew_category_id, c.category_name
					FROM content_crew a 
						INNER JOIN crew b on a.crew_id = b.crew_id 
						INNER JOIN crew_category c on a.crew_category_id = c.crew_category_id
					WHERE content_id = '".$row->content_id."'
					ORDER BY c.order_number
				 	";
				$query = $this->db->query($textquery);	
				$crews =  $query->result();
				$director =  array();
				$producer =  array();
				$cast =  array();
				$writer	 =  array();
	
				if ($crews) {
					
					foreach($crews as $crew){				
						if ($crew->crew_category_id =='director') {
							$director[] = array('crew_id' =>  $crew->crew_id, 'crew_name' => $crew->crew_name, 'crew_category_id' => $crew->crew_category_id, 'category_name' =>$crew->category_name);
	
						}	else if ($crew->crew_category_id =='producer') {
							$producer[] = array('crew_id' =>  $crew->crew_id, 'crew_name' => $crew->crew_name, 'crew_category_id' => $crew->crew_category_id, 'category_name' =>$crew->category_name);
	
						}	else if ($crew->crew_category_id =='cast') {
							$cast[] = array('crew_id' =>  $crew->crew_id, 'crew_name' => $crew->crew_name, 'crew_category_id' => $crew->crew_category_id, 'category_name' =>$crew->category_name);						
	
						}	else if ($crew->crew_category_id =='writer') {
							$writer[] = array('crew_id' =>  $crew->crew_id, 'crew_name' => $crew->crew_name, 'crew_category_id' => $crew->crew_category_id, 'category_name' =>$crew->category_name);
						}	
					}
				}
	
				$row->crew = array('director' => $director, 'producer' => $producer, 'cast' => $cast, 'writer' => $writer);

				$textquery = "
					SELECT 
						a.genre_id, b.genre_name
					FROM content_genre a 
						INNER JOIN genre b on a.genre_id = b.genre_id 
					WHERE content_id = '".$row->content_id."'
					ORDER BY genre_name asc
				 	";
				$query = $this->db->query($textquery);	
				$genre =  $query->result();
				$row->genre = $genre;
			}			
		}
		
		return $rows;		
	}


	function channel_list() {
		$textquery = "
				SELECT a.channel_category_id, a.category_name 
				FROM channel_category a 
				WHERE channel_category_id in ('125929935654092d653330b', '80386255054092d8aa5545', '210757968654092db1b99c7', '104830259654092dc8ddd65', '90678217954092dda401bc','213584991254254cde21f01','171118372054254c1787d00','-2','-3')
				Order by a.order_number 		
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		if ($rows){
			foreach($rows as $row){				
				$textquery = "
					SELECT a.channel_id,  b.alias channel_name 
					FROM apps_channel_category a
					LEFT JOIN channel b on a.channel_id = b.channel_id
					WHERE apps_id='com.balepoint.ibolz.moviebay'
								AND  channel_category_id='". $row->channel_category_id ."'
					ORDER BY a.order_number
				 	";
				$query = $this->db->query($textquery);	
				$channels =  $query->result();
				$row->channels = $channels;
				
				if ($row->channel_category_id =='-2')  $row->channel_category_id = '171118372054254c1787d00';
				if ($row->channel_category_id =='-3')  $row->channel_category_id = '213584991254254cde21f01';

			}	
		}


		
		return $rows;		
	}

	function crew_list($offset, $limit){
		$textquery = "
			SELECT 
					a.crew_id, b.crew_name, a.crew_category_id, c.category_name
			FROM content_crew a 
				INNER JOIN crew b on a.crew_id = b.crew_id 
				INNER JOIN crew_category c on a.crew_category_id = c.crew_category_id
			ORDER BY b.crew_name 
			LIMIT $offset, $limit
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		return $rows;		
	}
		
	function content_list_crew($crew_id, $offset, $limit){
		$textquery = "
			SELECT 
					a.crew_id, b.content_id, b.title, left(b.description,125) description, b.video_thumbnail 
		FROM content_crew a 
				INNER JOIN content b on a.content_id = b.content_id 
				INNER JOIN crew c on a.crew_id = c.crew_id 
			WHERE a.crew_id = '$crew_id'
			LIMIT $offset, $limit
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();		
		if ($rows) {
			foreach($rows as $row ){
				$row->video_thumbnail = 'http://moviebay2.ibolz.tv/admin/media/thumbnail/'.$row->video_thumbnail.'?w=200' ;
			}			
		}
		return $rows;		
	}


	function genre_list($offset, $limit){
		$textquery = "
			SELECT genre_id, genre_name 
			FROM genre  
			ORDER BY genre_name 
			LIMIT $offset, $limit
		";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();
		
		return $rows;		
	}
		
		
	function content_list_genre($genre_id, $offset, $limit){
		$textquery = "
			SELECT 
					a.genre_id, b.content_id, b.title, left(b.description,125) description , b.video_thumbnail
		FROM content_genre a 
				INNER JOIN content b on a.content_id = b.content_id 
				INNER JOIN genre c on a.genre_id = c.genre_id 
			WHERE a.genre_id = '$genre_id'
			LIMIT $offset, $limit
		 	";
		$query = $this->db->query($textquery);	
		$rows =  $query->result();		
		if ($rows) {
			foreach($rows as $row ){
				$row->video_thumbnail = 'http://moviebay2.ibolz.tv/admin/media/thumbnail/'.$row->video_thumbnail.'?w=200' ;
			}			
		}

		return $rows;		
	}


}

?>
