<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_model extends MY_Model {
	
	public function provider_list($apps_id){
		$textquery = "
			SELECT payment_id, payment_name, payment_code, payment_provider, post_url, post_field, post_option, post_url_unsubscribe, post_field_unsubscribe, 
						post_purchase_url , post_purchase_field
  		FROM payment_provider
  		WHERE apps_id = '$apps_id' and status='1'
  		ORDER BY order_number	asc
			";
  		$query = $this->db->query($textquery);	
			$rows = $query->result();
			if ($rows) {
				$this->load->helper('url');
				foreach($rows as $row){				
					if(strpos($row->post_url, '{BASE_URL}') !== false) {
						$base_url = base_url();
						$row->post_url = str_replace("{BASE_URL}",$base_url,$row->post_url);
						$row->post_purchase_url = str_replace("{BASE_URL}",$base_url,$row->post_purchase_url);
						$row->post_url_unsubscribe = str_replace("{BASE_URL}",$base_url,$row->post_url_unsubscribe);
					}
				}
			}
			
		return $rows;
	}
	
	public function update_customer_product($customer_id, $apps_id, $product_id, $amount){
		$textquery = "
			SELECT 
				purchase_id, customer_id, apps_id, product_id, created, expired
  			FROM customer_product
  			WHERE lower(customer_id) = lower('$customer_id') 
  				AND lower(apps_id) = lower('$apps_id')
  				AND product_id = '$product_id'
			";
  	$purchased = $this->db->query($textquery)->row();	
		$active_days = "2";
		$expired  = 	" date_add(now(), INTERVAL ". $active_days ."  DAY) ";
		
		if($purchased){
			// INSERT HISTORY
			$textquery = "
				INSERT INTO customer_product_history (purchase_id, customer_id, apps_id, product_id, amount, created, expired) 
				SELECT purchase_id, customer_id, apps_id, product_id, amount, created, expired
  			FROM customer_product
  			WHERE purchase_id = '". $purchased->purchase_id ."' 
			";
			$this->db->query($textquery);	
			

			$textquery = "
				UPDATE customer_product
				SET amount = '$amount', expired = $expired, created = now()
				WHERE purchase_id = '". $purchased->purchase_id ."' 
			 	";
			$this->db->query($textquery);	
		} else {
			$textquery = "
				INSERT INTO customer_product (apps_id, customer_id, product_id, amount, created, expired)
				VALUES ('$apps_id', '$customer_id', '$product_id', '$amount',  now(), $expired )
			";
			$this->db->query($textquery);	
		}
		
		return $purchased;
	}
	
}

?>
