/* 
    Author: htridianto
    Email: htridianto@gmail.com
*/
var currentTemplateMenusPath;
define([
	'backbone', 'underscore',
	'views/index',
],function(Backbone, _, indexView){
    var Router = Backbone.Router.extend({		
		historyState: 1,
    	initialize: function(options){
    		console.log('[AppRouter ... initialized]');
    	},
		routes:{		
			"*actions" : "defaultAction",			
		},
		defaultAction:function(action){
			var _this = this;
            if( action === '' ){
                action = 'index';                
            }else if( action.charAt(action.length-1) === '/' ){
                action += 'index';                
            }    
            var templateMenusPath = 'templates/'+ action.substring(0, action.lastIndexOf('/')+1) + 'menus.html';
            var templatePath = 'templates/'+action+'.html';	            	            
            require([ 'text!'+templateMenusPath, 'text!'+templatePath],
                function(textTemplateMenus, textTemplatePage){                       
                    if( currentTemplateMenusPath !== textTemplateMenus){                            
                        if(textTemplateMenus){        
                            if( currentTemplateMenusPath == null ){
                                $('#nav').html(textTemplateMenus);
                            }else{
                                if($('#nav').hasClass('fadeInLeft')){
                                    $('#nav').removeClass('fadeInLeft').addClass('fadeOutLeft');   
                                }
                                setTimeout(function() {
                                    if($('#nav').hasClass('fadeOutLeft')){
                                        $('#nav').removeClass('fadeOutLeft').addClass('fadeInLeft');   
                                    }
                                    $('#nav').html(textTemplateMenus);
                                }, 300);
                            }      
                            currentTemplateMenusPath = textTemplateMenus;                            
                        }               
                    }
                    var action_id = action.replace(/[\/-]/g, '_');                    
                    if(textTemplatePage){  
                        indexView.setTemplate( _.template(textTemplatePage, {action_id: action_id}) );
                    }
                    indexView.render(action_id);
                    
                    $.each($('#nav').find('li'), function(index, item){
                      $(item).removeClass('current');
                      if( $(item).find('a').attr('href') == '#'+action ){
                        $(item).addClass('current');
                      }
                    })                                        
                },function (err) {
    				console.log('an error occured while displaying a view '+ action + '[' + err + ']');
    				alert('an error occured while displaying a view '+ action + '[' + err + ']');
    			}
            );
		}	
	});
	return Router;
});
