define([
	'backbone', 'underscore'
	], 
function(Backbone, _){	
	var theView = Backbone.View.extend({								
		el: $('#content'),	
		initialize: function(){
			console.log('view index init');			
        },
		render:function(action_id){			
            if( this.template ){
                var self = this;                                    
                $(self.el).removeClass('fadeIn').addClass('fadeOut');                                    
				setTimeout(function() {                    
					$(self.el).html(self.template);					
                    $(self.el).removeClass('fadeOut').addClass('fadeIn');                       
                }, 50);
			}            
			return this;
		},
		
		setTemplate: function(textTemplate){
            this.template = textTemplate; 			
		},


	});
	return new theView;
});
