/* 
	Author: htridianto
	Email: htridianto@gmail.com
*/
var mainContentScroll, navContentScroll;
var SERVER_URL = 'http://localhost:8082/ibolz-api-documentation/';
//var REST_API = 'http://china2.ibolz.tv/api106'; //'http://api.ibolz.vhost'; 
//var REST_API = 'http://localhost/ibolzidapi106'; //'http://api.ibolz.vhost';
var REST_API = 'http://172.16.2.28/api106/v2'; //'http://api.ibolz.vhost';
var PHP_REST_API = 'index.php';
var def_apps_id = 'com.balepoint.ibolz.id';
var def_version = '1.1.15';
var def_customer_id = '-2';
var def_lang = 'en_US';
var def_device_id = '1';
var def_mobile_id = '74be2b9af1';
var def_longitude = '0';
var def_latitude = '0';
var def_screen_size = '1018x631';
var default_lang = 'en';


/*
var api_params = {device_id:'1', mobile_id:'74be2b9af1', longitude:'0', latitude:'0', 
lang:'en_US', screen_size:'1018x631', version:'1.0.6',
apps_id:'com.balepoint.ibolz.demo.cn', customer_id:'b7858d2dbb5346d89d3f977e7e62de5a'};
*/
var api_params = [];
//var api_params = ['device_id':'1', 'mobile_id':'74be2b9af1', 'longitude':'0', 'latitude':'0', 
//'lang':'en_US', 'screen_size':'1018x631', 'version':'1.0.6',
//'apps_id':'com.balepoint.ibolz.demo.cn', 'customer_id':'b7858d2dbb5346d89d3f977e7e62de5a'];

setupParams();

function setupApps(_api_url, _apps_id, _version, _lang, _device_id, _mobile_id, _screen_size, _longitude, _latitude) {
	this.REST_API = _api_url;
	this.def_apps_id = _apps_id;
	this.def_version = _version;
	this.def_lang = _lang;
        this.def_device_id = _device_id;
        this.def_mobile_id = _mobile_id;
        this.def_screen_size = _screen_size;
        this.def_longitude = _longitude;
        this.def_latitude = _latitude;
	
	setupParams();
}

/*
function setupParams() {
	api_params = [
            {apps_id:def_apps_id},
            {version:def_version},
            {lang:def_lang},
            {device_id:def_device_id}, 
            {mobile_id:def_mobile_id}, 
            {screen_size:def_screen_size},
            {longitude:def_longitude}, 
            {latitude:def_latitude}
        ];
}
*/

function setupParams() {
    api_params = {
        apps_id:def_apps_id,
        version:def_version,
        lang:def_lang,
        device_id:def_device_id,
        mobile_id:def_mobile_id,
        screen_size:def_screen_size,
        longitude:def_longitude,
        latitude:def_latitude
        };
}


function setCustomerID(_customerID) {
    this.def_customer_id = _customerID;
}

function getParams() {
    $result = api_params;
    if (def_customer_id !== '') {
        $result['customer_id'] = def_customer_id;
    }
    return $result;
}

var config_path;
// init main application module
var MainApp = {
    app_name: 'iBolz',
    app_version: '1.6',
    routes: {},
    initialize: function () { // Called once, at app startup
            require.config({ //	------- requirejs config
                    paths:{
                            'underscore': 'libs/underscore/underscore',
                            'backbone':'libs/backbone/backbone',				
                            'i18n':'libs/i18n',
                            'text':'libs/require/text',				
                            'templates':'../html',	
                    },
                    shim:{
                            'backbone' :{
                                    deps:['underscore'], export: 'Backbone'
                            }
                    }
            });			
            require(['underscore', 'backbone', 'router'],
                function(_, Backbone, Router){
                    navContentScroll = new iScroll('nav_content', {
                        checkDOMChanges: true
                    });
                    // mainContentScroll = new iScroll('main_content');  
//                    var strPath = window.location.href;
//                    if(strPath.indexOf("/documentation") > 0){
//                        REST_API = strPath.substr(0,strPath.indexOf('/documentation'));
//                    }
                    
                    MainApp.routes = new Router();
                    Backbone.history = Backbone.history || new Backbone.History({});
                    Backbone.history.start();
                    
                    }
            ); 
    }// endo of initialize
};
MainApp.initialize();

createParams = function($url, $form) {
    $params = new Object();
    $newForm = getParams();
    if ($form !== null) {
        addAll($newForm, $form);
    }
    $params['server_url'] = $url;
    $params['params'] = $newForm;
    
    return $params;
};

$.fn.addAll = function($newArray) {
    $.each($newArray, function(key, value) {
        this[key] = value;
    });
};

addAll = function(array1, array2) {
    $.each(array2, function(key, value) {
        array1[key] = value;
    });
};

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Accept-Language', default_lang);
        xhr.setRequestHeader("Authorization", "Basic "+ btoa('ibolz:kramat@123#'));
    }
});
