<?php

//namespace abeautifulsite;

//use Exception;

//require_once '../libraries/SimpleImage';

class Thumbnail extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->library('SimpleImage');
    }

    public function index() {
        $filename = $_GET["id"];

        $w = 0;
        if (isset($_GET["w"])) {
            $w = intval($_GET["w"]);
        }

        $h = 0;
        if (isset($_GET["h"])) {
            $h = intval($_GET["h"]);
        }


//        $image_file = '/mnt/media/thumbnails/' . $filename;
        $image_file = $this->config->item('thumbnail_path') . $filename;
        $img = new SimpleImage($image_file);

        if ($w > 0 && $h == 0) {
            $img->fit_to_width($w)->output('');
        } else if ($h > 0 && $w == 0) {
            $img->fit_to_height($h)->output('');
        } else if ($h > 0 && $w > 0) {
            $img->best_fit($w, $h)->output('');
        }
    }

}
