<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama, amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Login extends UX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('account_model', 'account_group_model'));
    }
    
    public function index() {
        $this->data['ret'] = $this->input->get('ret');

        $this->parser->parse('login.tpl', $this->data);
    }

    public function doLogin() {


        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'username',
                        'label' => 'Username',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            // $useraccount = array('username' => $username, 'password' => $password);
            $useraccount = $this->account_model->login($username, $password);

            if ($useraccount) {

                $role_list = $this->account_model->getAccountRoles($useraccount->account_id);
                $group_list = $this->account_group_model->getGroupListByAccount($useraccount->account_id);
                $privilege_list = $this->account_model->getAccountPrivileges($useraccount->account_id);
                $useraccount->role_list = $role_list;
                $useraccount->group_list = $group_list;
                // $useraccount->privilege_list = $privilege_list;

                $new_privilege_list = array();
                foreach ($privilege_list as $privilege) {
                    $new_privilege_list[$privilege->privilege_id] = $privilege->privilege_checked;
                }
                $useraccount->privilege_list = $new_privilege_list;

                // print_r($privilege_list);

                $this->session->set_userdata('user_account', $useraccount);
                redirect('/' . base64_decode($this->input->post('ret')));
                return;
            } else {
                $this->session->set_flashdata('error_login', 'Invalid username or password');
                $this->data['error_login'] = 'Invalid username or password';
            }
            
        } else {

            $this->session->set_flashdata('error_login', "Please enter your username and password");
            $this->data["username"] = $this->input->post('username');
            $this->data["error_login"] = validation_errors();
        }

        $this->parser->parse('login.tpl', $this->data);
    }

    public function doLogout() {

        // clear session data        
        $this->session->unset_userdata('user_account');

        $this->session->sess_destroy();

        redirect(base_url('login'));

        return false;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */