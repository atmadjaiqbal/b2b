<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Certificate_preview extends UX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('certificate_model', 'customer_model'));
    }

    public function index() {

        $certificate_id = $this->input->get("certificate_id");
        $is_print = $this->input->get("print");

        $certificate = NULL;
        $customer = NULL;
        $authors = array();
        $template = NULL;

        if (!empty($certificate_id)) {
            $certificate = $this->certificate_model->getCertificateByID($certificate_id);
            $customer = $this->customer_model->getCustomerDetail($certificate->customer_id);
            $author_list = $this->certificate_model->getTemplateAuthors($certificate->template_id);
            $template = $this->certificate_model->getCertificateTemplateList($certificate->template_id);

            foreach ($author_list as $author) {
                $authors[$author->position] = $author;
            }
        }

        $this->data["template_background"] = 'http://belanegara.local/thumbnail?id=105681549156563d8e583de.jpg';

        $this->data["certificate"] = $certificate;
        $this->data["customer"] = $customer;
        $this->data["authors"] = $authors;
        $this->data["template"] = $template;
        $this->data["is_print"] = $is_print;

        $this->parser->parse('certificate_preview.tpl', $this->data);
        
    }

    public function default_template() {
    	$this->parser->parse('certificate_template_default.tpl', $this->data);
    }

}
