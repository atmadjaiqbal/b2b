<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @license 		http://www.apache.org/licenses/LICENSE-2.0.html
 * @version 		2.0
 * 
 * */

/**
 * 
 */
class UI_Controller extends CI_Controller {
    
    public $data = array();

    public function __construct() {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');
        
        $this->load->library('layout');

        if (!$this->session->userdata('user_account')) {
            redirect(base_url('login') . '?ret=' . base64_encode($this->uri->uri_string()));
        }
    }
    
    public function setActiveNavigation($navigation_id, $navigation_title="", $navigation_menu="") {
        $this->data["navigation_id"] = $navigation_id;
        $this->data["navigation_title"] = $navigation_title;
        $this->data["navigation_menu"] = $navigation_menu;
    }

}

/**
 * Login
 */
class UX_Controller extends CI_Controller {
    
    public $data = array();

    public function __construct() {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->library('parser');
    }

}
