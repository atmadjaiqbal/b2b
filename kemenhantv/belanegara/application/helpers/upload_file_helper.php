<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */


if (!function_exists('upload_file')) {

    /**
     * 
     * Just a bit helper for us.
     * 
     * @param type $files The files. Send the '$files' instead
     * @param type $destinationDirectory <b>IMPORTANT. DIRECTORY ONLY. NOT FULL PATH</b>
     * @param type $newfilename New filename of the file to be uploaded
     * @param type $sizeLimit Integer Limit the size. Default is 10Mb
     * @param type $extensionFilter Array filter the extension
     * @return boolean return true if the uploading process is succeed.
     * @throws Exception Thows Exception when error occured.
     */
    function upload_file($files, $destinationDirectory, $sizeLimit = 10000000, $extensionFilter = null) {
        // Undefined | Multiple Files | $files Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (!isset($files['file']['error']) || is_array($files['file']['error'])) {
            throw new Exception('Invalid parameters.');
        }

        // Check $files['upfile']['error'] value.
        switch ($files['file']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new Exception('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new Exception('Exceeded filesize limit. Ask your network engineer about this');
            default:
                throw new Exception('Unknown errors.');
        }

        // You should also check filesize here. 
        if ($files['file']['size'] > $sizeLimit) {
            throw new Exception('Exceeded filesize limit. maximum is 1Mb');
        }

        // DO NOT TRUST $files['upfile']['mime'] VALUE !!
        // Check MIME Type by yourself.
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search($finfo->file($files['file']['tmp_name']), array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif',
                ), true
                )) {
            throw new Exception('Invalid file format.');
        }

        // You should name it uniquely.
        // DO NOT USE $files['upfile']['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.
        $newFileName = (uniqid(rand(), false)) . '.' . $ext;
        if (!move_uploaded_file($files['file']['tmp_name'], $destinationDirectory . $newFileName)) {
            throw new Exception('Failed to move uploaded file.');
        }

        return $newFileName;
    }

}


if (!function_exists("file_extension")) {

    function file_extension($filename) {
        $ext = $finfo->file($files['file']['tmp_name']);

        return $ext;
    }

}
