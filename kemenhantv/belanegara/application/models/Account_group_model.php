<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Role_model
 *
 * @author Ichalz Break
 */
class Account_group_model extends MY_Model {
    
    public function getGroupList($group_id = NULL) {
        $sql = "
            SELECT
                `group_id`,
                `group_name`
            FROM `tm_group`
            ";
        if (empty($group_id)) {
            $sql .= " ORDER BY group_name ASC ";
            return $this->db->query($sql)->result();
        } else {
            $sql .= " WHERE group_id = ? ";
            return $this->db->query($sql, array($group_id))->row();
        }
        
    }

    public function insertGroup($group_id, $group_name) {
        $sql = "
            INSERT INTO `tm_group`
                (`group_id`, `group_name`)
            VALUES 
                (?, ?)
            ;";
        
        return $this->db->query($sql, array($group_id, $group_name));
    }

    public function updateGroup($group_id, $group_name) {
        $sql = "
            UPDATE `tm_group`
            SET
              `group_name` = ?
            WHERE `group_id` = ?;";

        return $this->db->query($sql, array($group_name, $group_id));
    }

    public function removeGroup($group_id) {
        $sql = "
            DELETE FROM
                tm_group
            WHERE
                group_id = ?
        ";
        return $this->db->query($sql, array($group_id));

    }

    public function getGroupListByAccount($account_id) {
        $params = NULL;
        $sql = "
            SELECT
                a.`group_id`,
                b.`group_name`
            FROM `t_account_group` a JOIN tm_group b ON a.group_id = b.group_id  
        ";

        if (!empty($account_id)) {
            $sql .= "
                WHERE a.account_id= ?
            ";
            $params[] = $account_id;
        }

        $rows = $this->db->query($sql, $params)->result();
        
        return $rows;
    }

    public function getAccountGroupByGroup($group_id) {
        $params = NULL;
        $sql = "
            SELECT
                `group_id`,
                b.`account_id`,
                b.`full_name`,
                b.`user_id`,
                b.`email`,
                b.`phone`
            FROM `t_account_group` a JOIN t_account b ON a.account_id = b.account_id  
        ";

        if (!empty($group_id)) {
            $sql .= "
                WHERE group_id= ?
            ";
            $params[] = $group_id;
        }

        $rows = $this->db->query($sql, $params)->result();
        
        return $rows;
    }

    public function getGroupAccountByAccount($group_id) {
        $params = NULL;
        $sql = "
            SELECT
                a.`account_id`,
                a.`user_name`,
                a.`user_id`,
                a.`user_email`,
                a.`user_phone`,
                b.`group_id`,
                IF (b.group_id = ?, 1, 0) as checked_group_id
            FROM 
                t_account a LEFT JOIN `t_account_group` b ON a.account_id = b.account_id 
                AND b.group_id = ?
            GROUP BY a.account_id
        ";

        $rows = $this->db->query($sql, array($group_id, $group_id))->result();
        
        return $rows;
    }


    public function updateAccountGroup($group_id, $account_id) {
        $sql = "
            DELETE
            FROM 
                `t_account_group`
            WHERE 
                `group_id` = ?;";
        
        if ($this->db->query($sql, array($group_id))) {
            //begin transaction
            if (empty($account_id)) return TRUE;
            
            $this->db->trans_start();
            foreach ($account_id as $item) {
                $sql = "
                    INSERT INTO 
                        `t_account_group`
                        (`group_id`, `account_id`)
                    VALUES
                        (?, ?);";
                $this->db->query($sql, array($group_id, $item));
            }
            $this->db->trans_complete();
            
            return $this->db->trans_status();
            
        }
        
        return FALSE;
        
    }

}
