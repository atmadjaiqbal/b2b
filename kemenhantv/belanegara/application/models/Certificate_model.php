<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Role_model
 *
 * @author Ichalz Break
 */
class Certificate_model extends MY_Model {

    /**
    * ========= CERTIFICATE TYPE ==========
    */
    public function getCertificateTypeList($certificate_type_id = NULL) {
        $sql = "
            SELECT 
                certificate_type_id, 
                certificate_name, 
                certificate_acronym
            FROM 
                tm_certificate_type
            ";
        $rows = $this->db->query($sql)->result();
        
        return $rows;
    }

    public function insertCertificateType($certificate_name, $certificate_acronym) {
        $sql = "
            INSERT INTO 
            `tm_certificate_type`
                (`certificate_name`,
                `certificate_acronym`)
            VALUES 
                (?, ?)
            ;";
        
        return $this->db->query($sql, array($certificate_name, $certificate_acronym));
    }

    public function updateCertificateType($certificate_type_id, $certificate_name, $certificate_acronym) {
        $sql = "
            UPDATE 
            `tm_certificate_type`
            SET
                `certificate_name` = ?,
                `certificate_acronym` = ?
            WHERE
                `certificate_type_id` = ?
            ;";
        
        return $this->db->query($sql, array($certificate_name, $certificate_acronym, $certificate_type_id));
    }


    /**
    * ======== TEMPLATES =========
    */
    public function getCertificateTemplateList($template_id = NULL) {
        $rows = NULL;
        $sql = "
            SELECT 
                `template_id`,
                `template_title`,
                `created_date`,
                `status`,
                `thumbnail`,
                `content_text`,
                `background_image`,
                a.`pusdiklat_id`,
                a.`institution_id`,
                `is_default`,
                b.pusdiklat_name,
                c.institution_name,
                d.province_name
            FROM 
                `t_certificate_template` a LEFT JOIN t_pusdiklat b ON a.pusdiklat_id = b.pusdiklat_id
                LEFT JOIN t_institution c ON a.institution_id = c.institution_id
                LEFT JOIN tm_province d ON b.province_id = d.province_id
            ";

        if (!empty($template_id)) {
            $sql .= " WHERE template_id = ?";
            $rows = $this->db->query($sql, array($template_id))->row();

            if ($rows) {
                $rows->background_image = $this->get_thumbnail_url($rows->background_image);
                $rows->thumbnail = $this->get_thumbnail_url($rows->thumbnail);
            }
        } else {
            $rows = $this->db->query($sql)->result();

            foreach ($rows as $item) {
                $item->background_image = $this->get_thumbnail_url($item->background_image);
                $item->thumbnail = $this->get_thumbnail_url($item->thumbnail);
            }
        }
        
        
        return $rows;
    }

    public function insertCertificateTemplate($template_id, $template_title, $status, $content_text="", $pusdiklat_id, $institution_id, $is_default) {

        $sql = "
            INSERT INTO 
                `t_certificate_template`
                (`template_id`,
                `template_title`,
                `created_date`,
                `status`,
                `content_text`,
                `pusdiklat_id`,
                `institution_id`,
                `is_default`
                )
            VALUES
                (?, ?, NOW(), ?, ?);
        ";

        return $this->db->query($sql, array($template_id, $template_title, $status, $content_text, $pusdiklat_id, $institution_id, $is_default));

    }

    public function updateCertificateTemplate($template_id, $template_title, $status, $content_text, $pusdiklat_id, $institution_id, $is_default) {

        $sql = "
            UPDATE
                t_certificate_template
            SET
                template_title = ?,
                status = ?,
                content_text = ?,
                pusdiklat_id = ?,
                institution_id = ?,
                is_default = ?
            WHERE
                template_id = ?;
        ";

        return $this->db->query($sql, array($template_title, $status, $content_text, $pusdiklat_id, $institution_id, $is_default, $template_id));

    }

    public function updateTemplateThumbnail($template_id, $thumbnail) {

        $sql = "
            UPDATE
                t_certificate_template
            SET
                background_image = ?
            WHERE
                template_id = ?;
        ";

        return $this->db->query($sql, array($thumbnail, $template_id));

    }

    public function deleteTemplate($template_id) {
        $sql = "
            DELETE FROM
                t_certificate_template
            WHERE
                template_id = ?;
        ";

        return $this->db->query($sql, array($template_id));
    }

    public function getDefaultTemplate($pusdiklat_id, $institution_id) {

        $rows = NULL;
        $params = array();
        $sqlDefaultAlgo = "";

        //institution id first
        if (empty($institution_id)) {
            $sqlDefaultAlgo .= " IF (institution_id IS NULL, 1, 0) AS institution_match, ";
        } else {
            $sqlDefaultAlgo .= " IF (institution_id = ? , 1, 0) AS institution_match, ";
            $params[] = $institution_id;
        }

        //pusdiklat_id then
        if (empty($pusdiklat_id)) {
            $sqlDefaultAlgo .= " IF (pusdiklat_id IS NULL, 1, 0) AS pusdiklat_match, ";
        } else {
            $sqlDefaultAlgo .= " IF (pusdiklat_id = ? , 1, 0) AS pusdiklat_match, ";
            $params[] = $pusdiklat_id;
        }


        $sql = "
            SELECT
                `template_id`,
                `template_title`,
                `created_date`,
                `status`,
                `thumbnail`,
                `content_text`,
                `background_image`,
                `pusdiklat_id`,
                `institution_id`,
                `is_default`,
                ";
        $sql .= $sqlDefaultAlgo;
        $sql .= "
                IF (is_default = 1, 1, 0) AS default_match
            FROM 
                `t_certificate_template`
            ORDER BY 
                institution_match DESC, pusdiklat_match DESC, default_match DESC, created_date DESC
            LIMIT 1
            ";
        $rows = $this->db->query($sql, $params)->row();

        if ($rows) {
            $rows->background_image = $this->get_thumbnail_url($rows->background_image);
            $rows->thumbnail = $this->get_thumbnail_url($rows->thumbnail);
        }        
        
        return $rows;

    }


    public function getTemplateAuthors($template_id) {
        $sql = "
            SELECT
                `template_id`,
                a.`author_id`,
                `position`,
                b.`author_id`,
                b.`author_title`,
                b.`author_name`,
                b.`author_position`,
                b.`thumbnail`
            FROM
                t_certificate_authorization a JOIN t_certificate_authority b ON a.author_id = b.author_id
            WHERE
                template_id = ?;
        ";

        $rows = $this->db->query($sql, array($template_id))->result();

        foreach ($rows as $row) {
            if ($row->thumbnail) {
                $row->thumbnail = $this->get_thumbnail_url($row->thumbnail);
            }
        } 

        return $rows;

    }


    public function updateTemplateAuthors($template_id, $authors) {
        $sql = "
            DELETE FROM
                t_certificate_authorization
            WHERE
                template_id = ?";
        
        if ($this->db->query($sql, array($template_id))) {
            //begin transaction
            if (empty($authors)) return TRUE;
            
            $this->db->trans_start();
            foreach ($authors as $item) {
                $sql = "
                    INSERT INTO 
                        `t_certificate_authorization`
                        (`template_id`,
                        `author_id`,
                        `position`)
                    VALUES
                        (?, ?, ?);
                ";
                $this->db->query($sql, array($template_id, $item['author_id'], $item['position']));
            }
            $this->db->trans_complete();
            
            return $this->db->trans_status();
            
        }
        
        return FALSE;
        
    }


    /**
    * ======= CERTIFICATE AUTHORITY =======
    */
    public function getCertificateAuthorityList($author_id=NULL) {
        $sql = "
            SELECT 
                author_id, 
                author_title, 
                author_name, 
                author_position, 
                thumbnail
            FROM t_certificate_authority
        ";

        if (empty($author_id)) {
            $rows = $this->db->query($sql)->result();
            foreach ($rows as $item) {
                $item->thumbnail = $this->get_thumbnail_url($item->thumbnail);
            }
        } else {
            $sql .= " WHERE author_id=? ";
            $rows = $this->db->query($sql, array($author_id))->row();

            if ($rows) {
                $rows->thumbnail = $this->get_thumbnail_url($rows->thumbnail);
            }
        }
        
        return $rows;

    }

    public function insertAuthor($author_id, $author_title, $author_name, $author_position) {
        $sql = "
            INSERT INTO
                t_certificate_authority
                (author_id, author_title, author_name, author_position)
            VALUES
                (?, ?, ?, ?);
        ";

        return $this->db->query($sql, array($author_id, $author_title, $author_name, $author_position));
    }

    public function updateAuthor($author_id, $author_title, $author_name, $author_position) {
        $sql = "
            UPDATE
                t_certificate_authority
            SET
                author_title = ?,
                author_name = ?,
                author_position = ?
            WHERE
                author_id = ?;
        ";

        return $this->db->query($sql, array($author_title, $author_name, $author_position, $author_id));
    }

    public function updateAuthorThumbnail($author_id, $thumbnail) {
        $sql = "
            UPDATE
                t_certificate_authority
            SET
                thumbnail = ?
            WHERE
                author_id = ?;
        ";

        return $this->db->query($sql, array($thumbnail, $author_id));
    }

    public function deleteAuthor($author_id) {
        $sql = "
            DELETE FROM
                t_certificate_authority
            WHERE
                author_id = ?;
        ";

        return $this->db->query($sql, array($author_id));
    }


    /**
    * ========== CERTIFICATE LIST ========
    */
    public function getCertificateByID($certificate_id) {
        $sql = "
            SELECT 
                `certificate_id`,
                `certificate_no`,
                `certificate_type_id`,
                `customer_id`,
                `status_id`,
                `template_id`,
                `last_modified_date`
            FROM 
                `t_certificate`
            WHERE
                `certificate_id` = ?;
        ";
        $rows = $this->db->query($sql, array($certificate_id))->row();
        
        return $rows;
    }

    public function getCertificateListByStatus($status_id) {
        $sql = "
            SELECT 
                certificate_id, 
                certificate_no, 
                a.certificate_type_id, 
                b.certificate_name,
                a.customer_id, 
                c.full_name,
                a.status_id, 
                last_modified_date,
                template_id,
                d.pusdiklat_name,
                e.province_name
            FROM 
                t_certificate a JOIN tm_certificate_type b ON a.certificate_type_id = b.certificate_type_id
                JOIN t_customer c ON a.customer_id = c.customer_id
                JOIN t_pusdiklat d ON c.pusdiklat_id = d.pusdiklat_id
                LEFT JOIN tm_province e ON d.province_id = e.province_id
            WHERE
                a.status_id = ?
            ORDER BY last_modified_date DESC;
            ";
        $rows = $this->db->query($sql, array($status_id))->result();
        
        return $rows;
    }

    public function getCertificateByCustomer($customer_id) {
        $sql = "
            SELECT 
                certificate_id, 
                certificate_no, 
                certificate_type_id, 
                customer_id, 
                status_id, 
                template_id
            FROM 
                t_certificate
            WHERE
                customer_id = ?;
            ";
        $rows = $this->db->query($sql, array($customer_id))->result();
        
        return $rows;
    }

    public function insertCertificates($certificate_to_insert, $status_id, $account_id = NULL, $remark = "") {
        if (empty($certificate_to_insert)) {
            return TRUE;
        }

        $this->db->trans_start();
        foreach ($certificate_to_insert as $customer_id=>$certificates) {

            foreach ($certificates as $certificate) {

                $certificate_type_id = $certificate["certificate_type_id"];
                $template_id = $certificate["template_id"];
                $certificate_id = uniqid(rand(), FALSE);
                $sql = "
                    INSERT INTO 
                        t_certificate
                        (certificate_id, certificate_type_id, customer_id, status_id, last_modified_date, template_id)
                    VALUES
                        (?, ?, ?, ?, NOW(), ?);
                ";
                $sqlHistory = "
                    INSERT INTO
                        t_certificate_history
                        (
                        account_id,
                        certificate_id,
                        old_status_id,
                        new_status_id,
                        issued_date,
                        remark)
                    VALUES
                        (?, ?, NULL, ?, NOW(), ?);
                ";
                $this->db->query($sql, array($certificate_id, $certificate_type_id, $customer_id, $status_id, $template_id));
                $this->db->query($sqlHistory, array($account_id, $certificate_id, $status_id, $remark));

            }

        }
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function updateCertificatesStatus($certificates, $account_id, $remark="") {
        $this->db->trans_start();
        foreach ($certificates as $certificate) {
            $sqlUpdate = "
                UPDATE 
                    `t_certificate`
                SET
                    `certificate_no` = ? ,
                    `status_id` = ? ,
                    `last_modified_date` = NOW()
                WHERE 
                    `certificate_id` = ? ;
            ";

            $sqlHistory = "
                INSERT INTO
                    t_certificate_history
                    (
                    account_id,
                    certificate_id,
                    old_status_id,
                    new_status_id,
                    issued_date,
                    remark)
                VALUES
                    (?, ?, ?, ?, NOW(), ?);
            ";

            $this->db->query($sqlUpdate, array($certificate->certificate_no, $certificate->new_status_id, $certificate->certificate_id));
            $this->db->query($sqlHistory, array($account_id, $certificate->certificate_id, $certificate->status_id, $certificate->new_status_id, $remark));

        }

        $this->db->trans_complete();

        return $this->db->trans_status();

    }

    public function detail($certificate_id) {
        $sql = "
            SELECT 
                certificate_id, 
                certificate_no, 
                a.certificate_type_id, 
                b.certificate_name,
                a.customer_id, 
                c.id_no,
                c.address,
                c.phone_no,
                c.birth_date,
                c.birth_place,
                c.occupation,
                c.full_name,
                c.gender,
                a.status_id, 
                last_modified_date,
                template_id,
                d.pusdiklat_name,
                e.province_name,
                f.religion_name
            FROM 
                t_certificate a JOIN tm_certificate_type b ON a.certificate_type_id = b.certificate_type_id
                JOIN t_customer c ON a.customer_id = c.customer_id
                JOIN t_pusdiklat d ON c.pusdiklat_id = d.pusdiklat_id
                LEFT JOIN tm_province e ON d.province_id = e.province_id
                JOIN tm_religion f ON c.religion_id = f.religion_id
            WHERE
                certificate_id = ?;
            ";
        $rows = $this->db->query($sql, array($certificate_id))->row();
        
        return $rows;
    }

}
