<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Role_model
 *
 * @author Ichalz Break
 */
class Master_model extends MY_Model {

    //put your code here


    /**
    *
    * ========= PROVINCE ==============
    */
    public function getProvinceList($province_id = NULL) {
        $sql = "
            SELECT 
                province_id, 
                province_code, 
                province_name, 
                longitude, 
                latitude
            FROM 
                tm_province
            ";
        $rows = $this->db->query($sql)->result();
        
        return $rows;
    }

    public function getProvinceListDetail() {
        $sql = "
            SELECT 
                a.province_id,
                province_code, 
                province_name, 
                a.longitude, 
                a.latitude,
                COUNT(b.pusdiklat_id) AS count_pusdiklat,
                COUNT(d.certificate_id) AS count_certificate
            FROM bela_negara.tm_province a LEFT JOIN t_pusdiklat b ON a.province_id = b.province_id 
                LEFT JOIN t_customer c ON b.pusdiklat_id = c.pusdiklat_id 
                LEFT JOIN t_certificate d ON c.customer_id = d.customer_id
            GROUP BY a.province_id
            ";
        $rows = $this->db->query($sql)->result();
        
        return $rows;
    }

    public function insertProvince($province_code, $province_name, $longitude, $latitude) {
        $sql = "
            INSERT INTO 
            `tm_province`
                (`province_code`,
                `province_name`,
                `longitude`,
                `latitude`
                )
            VALUES 
                (?, ?, ?, ?)
            ;";
        
        return $this->db->query($sql, array($province_code, $province_name, $longitude, $latitude));
    }

    public function updateProvince($province_id, $province_code, $province_name, $longitude, $latitude) {
        $sql = "
            UPDATE `tm_province`
            SET
              `province_code` = ?,
              `province_name` = ?,
              `longitude` = ?,
              `latitude` = ?
            WHERE `province_id` = ?;";

        return $this->db->query($sql, array($province_code, $province_name, $longitude, $latitude, $province_id));
    }

    
    
    /**
    * ============ CITY ===========
    */
    public function getCityList($city_id = NULL) {
        $sql = "
            SELECT 
                city_id, 
                city_code, 
                city_name, 
                a.longitude, 
                a.latitude,
                a.province_id,
                b.province_name
            FROM 
                tm_city a LEFT JOIN tm_province b ON a.province_id = b.province_id
            ";
        $rows = $this->db->query($sql)->result();
        
        return $rows;
    }

    public function insertCity($city_code, $city_name, $longitude, $latitude, $province_id) {
        $sql = "
            INSERT INTO 
            `tm_city`
                (`city_code`,
                `city_name`,
                `longitude`,
                `latitude`,
                `province_id`
                )
            VALUES 
                (?, ?, ?, ?, ?)
            ;";
        
        return $this->db->query($sql, array($city_code, $city_name, $longitude, $latitude, $province_id));
    }

    public function updateCity($city_id, $city_code, $city_name, $longitude, $latitude, $province_id) {
        $sql = "
            UPDATE `tm_city`
            SET
              `city_code` = ?,
              `city_name` = ?,
              `longitude` = ?,
              `latitude` = ?,
              `province_id` = ?
            WHERE `city_id` = ?;";

        return $this->db->query($sql, array($city_code, $city_name, $longitude, $latitude, $province_id, $city_id));
    }


    /**
    * ======== CUSTOMER ID TYPE ========
    */
    public function getCustomerIDTypeList() {
        $sql = "
            SELECT
                id_type,
                id_name
            FROM
                tm_identification_type;
        ";

        $rows = $this->db->query($sql)->result();
        return $rows;
    }

    /**
    * ======= RELIGION =======
    */
    public function getReligionList() {
        $sql = "
            SELECT
                religion_id,
                religion_name
            FROM
                tm_religion;
        ";

        $rows = $this->db->query($sql)->result();
        return $rows;
    }

}
