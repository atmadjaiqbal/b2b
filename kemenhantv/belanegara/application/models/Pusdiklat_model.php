<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Role_model
 *
 * @author Ichalz Break
 */
class Pusdiklat_model extends MY_Model {

    //put your code here
    public function getList($pusdiklat_id = NULL) {
        $sql = "
            SELECT 
                pusdiklat_id, 
                pusdiklat_name, 
                address, 
                contact_person_name, 
                contact_person_email, 
                contact_person_phone, 
                a.longitude, 
                a.latitude, 
                a.province_id, 
                b.province_name, 
                city_id
            FROM t_pusdiklat a LEFT JOIN tm_province b ON a.province_id = b.province_id
            ";
        $rows = $this->db->query($sql)->result();
        
        return $rows;
    }

    public function getListByProvince($province_id = NULL) {
        $sql = "
            SELECT 
                a.pusdiklat_id, 
                pusdiklat_name, 
                address, 
                contact_person_name, 
                contact_person_email, 
                contact_person_phone, 
                a.longitude, 
                a.latitude, 
                a.province_id, 
                b.province_name, 
                city_id,
                COUNT(c.certificate_id) AS count_certificate
            FROM 
                t_pusdiklat a LEFT JOIN tm_province b ON a.province_id = b.province_id
                LEFT JOIN (
                    SELECT 
                        pusdiklat_id, certificate_id
                    FROM
                        t_certificate a LEFT JOIN t_customer b ON a.customer_id = b.customer_id) c
                ON a.pusdiklat_id = c.pusdiklat_id
            WHERE
                a.province_id = ?
            GROUP BY a.pusdiklat_id 
            ";
            
        $rows = $this->db->query($sql, array($province_id))->result();
        
        return $rows;
    }

    public function insertPusdiklat($pusdiklat_id, $pusdiklat_name, $address, $contact_person_name, $contact_person_email, $contact_person_phone, $longitude, $latitude, $province_id, $city_id) {
        $sql = "
            INSERT INTO t_pusdiklat
                (pusdiklat_id,
                pusdiklat_name,
                address,
                contact_person_name,
                contact_person_email,
                contact_person_phone,
                longitude,
                latitude,
                province_id, 
                city_id)
            VALUES 
                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            ;";
        
        return $this->db->query($sql, array($pusdiklat_id, $pusdiklat_name, $address, $contact_person_name, $contact_person_email, $contact_person_phone, $longitude, $latitude, $province_id, $city_id));
    }

    public function updatePusdiklat($pusdiklat_id, $pusdiklat_name, $address, $contact_person_name, $contact_person_email, $contact_person_phone, $longitude, $latitude, $province_id, $city_id) {
        $sql = "
            UPDATE t_pusdiklat
                SET 
                pusdiklat_name = ?,
                address = ?,
                contact_person_name = ?,
                contact_person_email = ?,
                contact_person_phone = ?,
                longitude = ?,
                latitude = ?,
                province_id = ?, 
                city_id = ?
            WHERE
                pusdiklat_id = ?
            ;";
        
        return $this->db->query($sql, array($pusdiklat_name, $address, $contact_person_name, $contact_person_email, $contact_person_phone, $longitude, $latitude, $province_id, $city_id, $pusdiklat_id));
    }

    public function pusdiklatDetail($pusdiklat_id) {
        $sql = "
            SELECT pusdiklat_id, 
                pusdiklat_name, 
                address, 
                contact_person_name, 
                contact_person_email, 
                contact_person_phone, 
                longitude, 
                latitude, 
                province_id, 
                city_id
            FROM t_pusdiklat
            WHERE
                pusdiklat_id = ?
            ";
        $rows = $this->db->query($sql, array($pusdiklat_id))->row();
        
        return $rows;
    }
    

}
