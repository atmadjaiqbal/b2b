<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama, amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Customer_model extends MY_Model {

    public function getCustomerList() {
        $sql = "
            SELECT 
                a.customer_id, 
                full_name, 
                phone_no, 
                a.address, 
                id_no, 
                birth_date, 
                occupation, 
                id_type, 
                religion_id, 
                a.status_id, 
                a.pusdiklat_id, 
                b.pusdiklat_name, 
                c.province_name,
                birth_place, 
                institution_id,
                register_date
            FROM 
                t_customer a LEFT JOIN t_pusdiklat b ON a.pusdiklat_id = b.pusdiklat_id
                LEFT JOIN tm_province c ON b.province_id = c.province_id
            ORDER BY register_date DESC;
            ";
        $rows = $this->db->query($sql)->result();
        
        return $rows;
    }

    public function getCustomerDetail($customer_id) {
        $sql = "
            SELECT 
                customer_id, 
                full_name, 
                phone_no, 
                a.address, 
                id_no, 
                birth_date, 
                occupation, 
                id_type, 
                religion_id, 
                status_id, 
                a.pusdiklat_id, 
                b.pusdiklat_name, 
                c.province_name,
                birth_place, 
                institution_id,
                register_date,
                d.city_id,
                d.city_name,
                jabatan
            FROM 
                t_customer a LEFT JOIN t_pusdiklat b ON a.pusdiklat_id = b.pusdiklat_id
                LEFT JOIN tm_province c ON b.province_id = c.province_id
                LEFT JOIN tm_city d ON b.city_id = d.city_id
            WHERE
                customer_id = ?;
            ";
        $rows = $this->db->query($sql, array($customer_id))->row();
        
        return $rows;
    }
    
    public function insertCustomer($customer_id, $full_name, $phone_no, $address, $id_no, $birth_date, $occupation, $id_type, $religion_id, $status_id, $pusdiklat_id, $birth_place, $institution_id, $jabatan) {
        $sql = "
            INSERT INTO 
                t_customer
                    (customer_id, 
                    full_name, 
                    phone_no, 
                    address, 
                    id_no, 
                    birth_date, 
                    occupation, 
                    id_type, 
                    religion_id, 
                    status_id, 
                    pusdiklat_id, 
                    birth_place, 
                    institution_id,
                    register_date,
                    jabatan)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?);
            ";
        return $this->db->query($sql, array($customer_id, $full_name, $phone_no, $address, $id_no, $birth_date, $occupation, $id_type, $religion_id, $status_id, $pusdiklat_id, $birth_place, $institution_id, $jabatan));
    }
    
    public function updateCustomer($customer_id, $full_name, $phone_no, $address, $id_no, $birth_date, $occupation, $id_type, $religion_id, $status_id, $pusdiklat_id, $birth_place, $institution_id, $jabatan) {
        $sql = "
            UPDATE t_customer
            SET 
                full_name = ?,
                phone_no = ?,
                address = ?,
                id_no = ?,
                birth_date = ?,
                occupation = ?,
                id_type = ?,
                religion_id = ?,
                status_id = ?,
                pusdiklat_id = ?,
                birth_place = ?,
                institution_id =?,
                jabatan =?
            WHERE 
                customer_id = ?
            ";
        return $this->db->query($sql, array($full_name, $phone_no, $address, $id_no, $birth_date, $occupation, $id_type, $religion_id, $status_id, $pusdiklat_id, $birth_place, $institution_id, $jabatan, $customer_id));
    }
    
    


}
