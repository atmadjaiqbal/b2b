<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama, amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Account_model extends MY_Model {

    public function getAccountList() {
        $sql = "
            SELECT
                `account_id`,
                `user_id`,
                `user_password`,
                `user_name`,
                `user_email`,
                `user_phone`,
                `status_id`,
                `pusdiklat_id`,
                `institution_id`
            FROM `t_account`;
            ";
        $rows = $this->db->query($sql)->result();
        
        return $rows;
    }
    
    public function insertAccount($account_id, $user_id, $user_password, $user_name, $user_email, $user_phone, $status_id, $pusdiklat_id, $institution_id) {
        $sql = "
            INSERT INTO `t_account`
                (`account_id`,
                 `user_id`,
                 `user_password`,
                 `user_name`,
                 `user_email`,
                 `user_phone`,
                 `status_id`,
                 `pusdiklat_id`,
                 `institution_id`
                 )
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);
            ";
        return $this->db->query($sql, array($account_id, $user_id, MD5("bela" . $user_password . "negara"), $user_name, $user_email, $user_phone, $status_id, $pusdiklat_id, $institution_id));
    }
    
    public function updateAccount($account_id, $user_id, $user_name, $user_email, $user_phone, $status_id, $pusdiklat_id, $institution_id) {
        $sql = "
            UPDATE `t_account`
            SET 
              `user_id` = ?,
              `user_name` = ?,
              `user_email` = ?,
              `user_phone` = ?,
              `status_id` = ?,
              `pusdiklat_id` = ?,
              `institution_id` = ?
            WHERE `account_id` = ?;
            ";
        return $this->db->query($sql, array($user_id, $user_name, $user_email, $user_phone, $status_id, $pusdiklat_id, $institution_id, $account_id));
    }
    
    public function login($username, $password) {
        $sql = "
            SELECT
                `account_id`,
                `user_id`,
                `user_password`,
                `user_name`,
                `user_email`,
                `user_phone`,
                `status_id`,
                `pusdiklat_id`,
                `institution_id`
            FROM `t_account`
            WHERE
                user_id = ?
                AND user_password = ?;
            ";
        $rows = $this->db->query($sql, array($username, MD5("bela" . $password . "negara")))->row();
        
        return $rows;
    }
    
    public function getAccountByUserIDorEmail($user_id, $user_email = NULL) {
        $params = array($user_id);
        $sql = "
            SELECT
                `account_id`,
                `user_id`,
                `user_password`,
                `user_name`,
                `user_email`,
                `user_phone`,
                `status_id`,
                `pusdiklat_id`,
                `institution_id`
            FROM 
                `t_account`
            WHERE
                `user_id` = ?
            ";
        if (!empty($user_email)) {
            $sql .= " AND user_email = ?";
            $params[] = $user_email;
        }
        $row = $this->db->query($sql, $params)->result();
        
        return $row;
    }
    
    public function getAccountDetail($account_id) {
        $sql = "
            SELECT
                `account_id`,
                `user_id`,
                `user_password`,
                `user_name`,
                `user_email`,
                `user_phone`,
                `status_id`,
                `pusdiklat_id`,
                `institution_id`
            FROM 
                `t_account`
            WHERE
                `account_id` = ?;
            ";
        $row = $this->db->query($sql, array($account_id))->row();
        
        return $row;
    }
    
    public function getAccountRoles($account_id) {
        $sql = "
            SELECT
                `account_id`,
                a.`role_id`,
                b.`role_name`
            FROM 
                `t_account_role` a JOIN tm_role b ON a.role_id = b.role_id
            WHERE
                `account_id` = ?;
            ";
        $row = $this->db->query($sql, array($account_id))->result();
        
        return $row;
    }

    public function getAccountPrivileges($account_id) {
        $sql = "
            SELECT
                a.privilege_id,
                if (d.account_id is null, 0, 1) as privilege_checked
            FROM tm_privilege a LEFT JOIN 
            (
                SELECT b.privilege_id, c.account_id FROM t_role_privilege b JOIN t_account_role c ON b.role_id = c.role_id
                WHERE c.account_id = ?
                GROUP BY b.privilege_id, c.account_id)
            d ON a.privilege_id = d.privilege_id
        ";

        $rows = $this->db->query($sql, array($account_id))->result();
        
        return $rows;
    }
    
    
    public function updateAccountRoles($account_id, $account_roles) {
        $sql = "
            DELETE
            FROM 
                `t_account_role`
            WHERE 
                `account_id` = ?;";
        
        if ($this->db->query($sql, array($account_id))) {
            //begin transaction
            if (empty($account_roles)) return TRUE;
            
            $this->db->trans_start();
            foreach ($account_roles as $item) {
                $sql = "
                    INSERT INTO 
                        `t_account_role`
                        (`account_id`, `role_id`)
                    VALUES 
                        (?, ?);";
                $this->db->query($sql, array($account_id, $item));
            }
            $this->db->trans_complete();
            
            return $this->db->trans_status();
            
        }
        
        return FALSE;
        
    }

    public function deleteAccount($account_id) {
        $sql = "
            DELETE
            FROM 
                `t_account`
            WHERE 
                `account_id` = ?;";

        return $this->db->query($sql, array($account_id));

    }


}
