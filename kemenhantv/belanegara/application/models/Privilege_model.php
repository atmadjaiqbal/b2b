<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Role_model
 *
 * @author Ichalz Break
 */
class Privilege_model extends MY_Model {
    
    public function getPrivilegeList($privilege_id = NULL) {
        $sql = "
            SELECT
                `privilege_id`,
                `privilege_name`,
                `privilege_group`
            FROM `tm_privilege`
            ";
        if (empty($privilege_id)) {
            $sql .= " ORDER BY privilege_group ASC ";
            return $this->db->query($sql)->result();
        } else {
            $sql .= " WHERE privilege_id = ? ";
            return $this->db->query($sql, array($privilege_id))->row();
        }
        
    }

    public function insertPrivilege($privilege_id, $privilege_name, $privilege_group="") {
        $sql = "
            INSERT INTO `tm_privilege`
                (`privilege_id`, `privilege_name`, `privilege_group`)
            VALUES 
                (?, ?, ?)
            ;";
        
        return $this->db->query($sql, array($privilege_id, $privilege_name, $privilege_group));
    }

    public function updatePrivilege($new_privilege_id, $privilege_id, $privilege_name, $privilege_group) {
        $sql = "
            UPDATE `tm_privilege`
            SET
              `privilege_id` = ?,
              `privilege_name` = ?,
              `privilege_group` = ?
            WHERE `privilege_id` = ?;";

        return $this->db->query($sql, array($new_privilege_id, $privilege_name, $privilege_group, $privilege_id));
    }

}
