<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Role_model
 *
 * @author Ichalz Break
 */
class Role_model extends MY_Model {

    //put your code here


    public function getRoleList($role_id = NULL) {
        $sql = "
            SELECT
                `role_id`,
                `role_name`
            FROM `tm_role`
            ";
        if (empty($role_id)) {
            return $this->db->query($sql)->result();
        } else {
            $sql .= " WHERE role_id = ?";
            return $this->db->query($sql, array($role_id))->row();
        }
        
        
        return $rows;
    }

    public function insertRole($role_id, $role_name) {
        $sql = "
            INSERT INTO `tm_role`
                (`role_id`,
                `role_name`)
            VALUES 
                (?, ?)
            ;";
        
        return $this->db->query($sql, array($role_id, $role_name));
    }

    public function updateRole($role_id, $new_role_id, $role_name) {
        $sql = "
            UPDATE `tm_role`
            SET
              `role_id` = ?,
              `role_name` = ?
            WHERE `role_id` = ?;";

        return $this->db->query($sql, array($new_role_id, $role_name, $role_id));
    }

    public function deleteRole($role_id) {
        $sql = "
            DELETE FROM 
                `tm_role`
            WHERE `role_id` = ?;";

        return $this->db->query($sql, array($role_id));
    }

    public function getRoleDetail($role_id) {
        $sql = "
            SELECT
        ";
    }

    public function getRolePrivilege($role_id) {
        $params = NULL;
        $sql = "
            SELECT
                `role_id`,
                `privilege_id`
            FROM `t_role_privilege`
        ";

        if (!empty($role_id)) {
            $sql .= "
                WHERE role_id= ?
            ";
            $params[] = $role_id;
        }

        $rows = $this->db->query($sql, $params)->result();
        
        return $rows;
    }

    public function updateRolePrivilege($role_id, $role_privileges) {
        $sql = "
            DELETE
            FROM 
                `t_role_privilege`
            WHERE 
                `role_id` = ?;";
        
        if ($this->db->query($sql, array($role_id))) {
            //begin transaction
            if (empty($role_privileges)) return TRUE;
            
            $this->db->trans_start();
            foreach ($role_privileges as $item) {
                $sql = "
                    INSERT INTO 
                        `t_role_privilege`
                        (`role_id`, `privilege_id`)
                    VALUES
                        (?, ?);";
                $this->db->query($sql, array($role_id, $item));
            }
            $this->db->trans_complete();
            
            return $this->db->trans_status();
            
        }
        
        return FALSE;
        
    }
    

}
