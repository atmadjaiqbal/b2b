<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama, amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Institution_model extends MY_Model {

    public function getInstitutionList($institution_id = NULL) {
        $sql = "
            SELECT 
                `institution_id`,
                `institution_name`,
                `institution_address`,
                `institution_alias`,
                `contact_person_name`,
                `contact_person_phone`,
                `contact_person_email`,
                `contact_person_fax`,
                `website`
            FROM 
                `t_institution`
            ";
        if (empty($institution_id)) {
            $rows = $this->db->query($sql)->result();
        } else {
            $sql .= " WHERE `institution_id` = ? ";
            $rows = $this->db->query($sql, array($institution_id))->row();
        }
        
        
        return $rows;
    }
    
    public function insertInstitution($institution_id, $institution_name, $institution_address, $institution_alias, $contact_person_name, $contact_person_phone, $contact_person_email, $contact_person_fax, $website) {
        $sql = "
            INSERT INTO 
            `t_institution`
                (`institution_id`,
                `institution_name`,
                `institution_address`,
                `institution_alias`,
                `contact_person_name`,
                `contact_person_phone`,
                `contact_person_email`,
                `contact_person_fax`,
                `website`)
            VALUES
                (?, ?, ?, ?, ?, ?, ?, ?, ?);

            ";
        return $this->db->query($sql, array($institution_id, $institution_name, $institution_address, $institution_alias, $contact_person_name, $contact_person_phone, $contact_person_email, $contact_person_fax, $website));
    }
    
    public function updateInstitution($institution_id, $institution_name, $institution_address, $institution_alias, $contact_person_name, $contact_person_phone, $contact_person_email, $contact_person_fax, $website) {
        $sql = "
            UPDATE 
                `t_institution`
            SET
                `institution_name` = ?,
                `institution_address` = ?,
                `institution_alias` = ?,
                `contact_person_name` = ?,
                `contact_person_phone` = ?,
                `contact_person_email` = ?,
                `contact_person_fax` = ?,
                `website` = ?
            WHERE `institution_id` = ?;
            ";
        return $this->db->query($sql, array($institution_name, $institution_address, $institution_alias, $contact_person_name, $contact_person_phone, $contact_person_email, $contact_person_fax, $website, $institution_id));
    }

    public function deleteInstitution($institution_id) {
        $sql = "
            DELETE FROM
                `t_institution`
            WHERE 
                `institution_id` = ?;
            ";
        return $this->db->query($sql, array($institution_id));
    }

}
