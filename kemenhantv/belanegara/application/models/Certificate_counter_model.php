<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Role_model
 *
 * @author Ichalz Break
 */
class Certificate_counter_model extends MY_Model {


    public function lastCertificateNumber($counter_id = 1) {
        //A99999A
        $sql = "
            SELECT 
                `counter_id`,
                `counter_type_id`,
                `last_count`,
                `incremental_counter`,
                `last_prefix`,
                `last_suffix`,
                `counter_number_length`
            FROM 
                `t_certificate_counter`
            WHERE
                `counter_id` = ?
            ";
        $rows = $this->db->query($sql, array($counter_id))->row();

        if ($rows) {
            $last_count_number = $rows->last_count + 1;
            $last_count_prefix = $rows->last_prefix;
            $last_count_suffix = $rows->last_suffix;

            if (strlen((string)$last_count_number) > $rows->counter_number_length) {
                $last_count_number = 1;
                $last_count_suffix++;
                if ($last_count_suffix == 'Z') {
                    $last_count_suffix = 'A';

                    $last_count_prefix++;
                }
            }

            $sqlUpdate = "
                UPDATE
                     `t_certificate_counter`
                SET
                    `last_count` = ?,
                    `last_prefix` = ?,
                    `last_suffix` = ?
                WHERE
                    `counter_id` = ?;
                ";
            $this->db->query($sqlUpdate, array($last_count_number, $last_count_prefix, $last_count_suffix, $counter_id));

            return $rows->last_prefix . sprintf('%0'.$rows->counter_number_length.'d', $rows->last_count) . $rows->last_suffix;

        }
        
        return NULL;
    }

}
