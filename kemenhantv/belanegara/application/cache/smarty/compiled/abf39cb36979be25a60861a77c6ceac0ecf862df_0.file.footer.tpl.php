<?php /* Smarty version 3.1.27, created on 2015-12-02 15:39:34
         compiled from "themes/default/views/layouts/partials/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1405807216565eae464cb710_35047438%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'abf39cb36979be25a60861a77c6ceac0ecf862df' => 
    array (
      0 => 'themes/default/views/layouts/partials/footer.tpl',
      1 => 1449045288,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1405807216565eae464cb710_35047438',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eae464cc2b9_87748067',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eae464cc2b9_87748067')) {
function content_565eae464cc2b9_87748067 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1405807216565eae464cb710_35047438';
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2015 <a href="http://ibolz.tv">iBOLZ</a>.</strong> All rights reserved.
</footer><?php }
}
?>