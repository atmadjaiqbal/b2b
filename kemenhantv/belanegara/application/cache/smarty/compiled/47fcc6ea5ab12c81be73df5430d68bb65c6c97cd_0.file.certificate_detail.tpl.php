<?php /* Smarty version 3.1.27, created on 2015-12-03 12:53:50
         compiled from "/var/www/html/kemenhantv/belanegara/application/modules/administration/views/certificate_detail.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:387376952565fd8ee3a8df0_98753576%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47fcc6ea5ab12c81be73df5430d68bb65c6c97cd' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/modules/administration/views/certificate_detail.tpl',
      1 => 1449118179,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '387376952565fd8ee3a8df0_98753576',
  'variables' => 
  array (
    'certificate' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565fd8ee3fca63_79378631',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565fd8ee3fca63_79378631')) {
function content_565fd8ee3fca63_79378631 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '387376952565fd8ee3a8df0_98753576';
?>
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>

<?php echo js("iCheck/iCheck.min.js");?>


<?php echo '<script'; ?>
 type="text/javascript">

    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }

    $(document).ready(function () {

        setupViewValues();

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#btnPreview').click(function() {
            <?php if ($_smarty_tpl->tpl_vars['certificate']->value) {?>
            window.open("<?php echo base_url('certificate_preview?certificate_id=');
echo $_smarty_tpl->tpl_vars['certificate']->value->certificate_id;?>
", '<?php echo $_smarty_tpl->tpl_vars['certificate']->value->certificate_id;?>
', "width=1024, height=640");
            // window.open("<?php echo base_url('certificate_preview?');?>
");
            <?php }?>
        });

        $('#theFormSubmit').click(function () {
            $(this).attr('disabled', true);
            clearAlert();

            var url = "<?php echo base_url('administration/customer/customer_save');?>
";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#theForm').serialize(),
                success: function (resp) {
                    $('#theFormSubmit').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#customer_id').val(resp.customer_id);

                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#theFormSubmit').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(jqXHR.responseText);
                }
            });
        });

        function setupViewValues() {
            <?php if ($_smarty_tpl->tpl_vars['certificate']->value) {?>
            $('#certificate_id').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->certificate_id;?>
');
            $('#certificate_name').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->certificate_name;?>
');
            $('#last_modified_date').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->last_modified_date;?>
');
            $('#id_no').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->id_no;?>
');
            $('#full_name').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->full_name;?>
');
            $('#address').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->address;?>
');
            $('#phone_no').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->phone_no;?>
');
            $('#occupation').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->occupation;?>
');
            $('#birth_date').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->birth_date;?>
');
            $('#birth_place').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->birth_place;?>
');
            $('#religion_name').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->religion_name;?>
');
            $('#pusdiklat_name').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->pusdiklat_name;?>
');
            $('#province_name').val('<?php echo $_smarty_tpl->tpl_vars['certificate']->value->province_name;?>
');
            <?php }?>
        }
    });
<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url('administration/customer/');?>
"><i class="fa fa-dashboard"></i> Customer</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Customer Form</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="certificate_id" name="certificate_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="last_modified_date" class="col-sm-3 control-label">Tanggal</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="last_modified_date" name="last_modified_date" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="certificate_name" class="col-sm-3 control-label">Tipe</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="certificate_name" name="certificate_name" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_no" class="col-sm-3 control-label">No Identitas</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="id_no" name="id_no" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="full_name" class="col-sm-3 control-label">Nama Lengkap</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="full_name" name="full_name" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="address" class="col-sm-3 control-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="address" name="address" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone_no" class="col-sm-3 control-label">No Telp</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="occupation" class="col-sm-3 control-label">Pekerjaan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="occupation" name="occupation" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="birth_date" class="col-sm-3 control-label">Tgl Lahir</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="birth_date" name="birth_date" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="birth_place" class="col-sm-3 control-label">Tempat Lahir</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="birth_place" name="birth_place" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion_name" class="col-sm-3 control-label">Agama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="religion_name" name="religion_name" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="pusdiklat_name" class="col-sm-3 control-label">Pusdiklat</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="pusdiklat_name" name="pusdiklat_name" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="province_name" class="col-sm-3 control-label">Provinsi</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="province_name" name="province_name" placeholder=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="<?php echo base_url('administration/certificate/request_list');?>
"><button type="button" class="btn btn-default">Go Back</button></a>
                        <button type="button" id="btnPreview" class="btn btn-info pull-right">Preview Certificate</button>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper --><?php }
}
?>