<?php /* Smarty version 3.1.27, created on 2015-11-30 15:53:38
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/modules/administration/views/certificate_list.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:201664223565c0e92e98d18_23465339%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6f9f936030a6154d8325ca11978cb8a3e46f3ecb' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/modules/administration/views/certificate_list.tpl',
      1 => 1448851116,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201664223565c0e92e98d18_23465339',
  'variables' => 
  array (
    'certificate_list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565c0e92efcbd2_88655825',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565c0e92efcbd2_88655825')) {
function content_565c0e92efcbd2_88655825 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '201664223565c0e92e98d18_23465339';
?>
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>

<?php echo js("iCheck/iCheck.min.js");?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Certificate List
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Certificate List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Certificate List</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Certificate No</th>
                                    <th>Type</th>
                                    <th>Name</th>
                                    <th>Province</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['certificate_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                <tr role="row">
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->last_modified_date;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->certificate_no;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->certificate_name;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->full_name;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default" name="btnDetail" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->certificate_id;?>
">
                                                <i class="fa fa-th"> Preview</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Validate Certificates</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="modal_label" id="modal_label" class="control-label">Are you sure want to validate certificate for selected items?</label>
                        </div>
                        <div class="box-footer"></div>
                    </div>
                    <div class="callout callout-danger hidden" id="response_error"></div>
                        <div class="callout callout-success hidden" id="response_succeed"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Confirm</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div><!-- /.content-wrapper -->

<?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

        $('input.check_all').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('input.check_single').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('input.check_all').on('ifChecked ifUnchecked', function(event) {    
            if (event.type == 'ifChecked') {
                $('input.check_single').iCheck('check');
            } else {
                $('input.check_single').iCheck('uncheck');
            }
        });

        $('#btnCreate').click(function () {
            clearModalForm();
            $('#layoutModal').modal('show');
        });

        $('button[name="btnDetail"]').on('click', function () {
            var data_id = $(this).attr('data-id');

            if (data_id === '')
                return;

            window.open("<?php echo base_url('certificate_preview?certificate_id=');?>
" + data_id);
        });

        $('button[name="btnEdit"]').on('click', function () {
            var data_id = $(this).attr('data-id');
            var data_name = $(this).attr('data-name');
            var data_acronym = $(this).attr('data-acronym');


            if (data_id === '')
                return;

            clearModalForm();

            $('#layoutModal').modal('show');
            
            $('#certificate_type_id').val(data_id);
            $('#certificate_name').val(data_name);
            $('#certificate_acronym').val(data_acronym);

        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var data = serializeCheckbox();

            var url = "validate_certificate";


            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: data,
                success: function (resp) {
                    if (resp.rcode === 1) {
                        $('#response_succeed').html(resp.message);
                        $('#response_succeed').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');

                        // location.reload();
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
    });

    $('#btnRequest').click(function() {
        clearModalForm();

        var data = serializeCheckbox();

        if (data) {
            $('#modal_label').text("Are you sure want to request certificate for selected items?");
        } else {
            $('#modal_label').text("Please select customer!");
        }
        
        
        $('#layoutModal').modal('show');

    });

    function serializeCheckbox() {
        var result = "";
        $('input[name="certificate_id[]"]:checked').each(function(k, v) {
            result += "&certificate_id[]=" + $(this).val();
        });

        return result;
    }

    function serializeCertificateType() {
        var result = "";
        $('input[name="certificate_type_id[]"]:checked').each(function(k, v) {
            result += "&certificate_type_id[]=" + $(this).val();
        });

        return result;
    }

    function clearModalForm() {
        $('#btnSubmitForm').removeAttr("disabled");

        $('#certificate_type_id').val('');
        $('#certificate_name').val('');
        $('#certificate_acronym').val('');

        hideErrorMessage();
        hideProgressBar();
    }

    function hideErrorMessage() {
        $('#response_error').html('');
        $('#response_error').addClass('hidden');
    }

    function hideProgressBar() {
        $('#progressModal').addClass('hidden');
        $('#progressBar').width(0);
        $('#progressBar').text(0);
    }

    function prepareSubmit() {
        $('#btnSubmitForm').attr("disabled", "true");
        hideErrorMessage();
    }

    });
<?php echo '</script'; ?>
><?php }
}
?>