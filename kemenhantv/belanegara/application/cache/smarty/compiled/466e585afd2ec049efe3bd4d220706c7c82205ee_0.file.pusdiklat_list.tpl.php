<?php /* Smarty version 3.1.27, created on 2015-12-01 01:57:11
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/modules/master/views/pusdiklat_list.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1664594852565c9c07dbfd41_90126822%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '466e585afd2ec049efe3bd4d220706c7c82205ee' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/modules/master/views/pusdiklat_list.tpl',
      1 => 1448909430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1664594852565c9c07dbfd41_90126822',
  'variables' => 
  array (
    'pusdiklat_list' => 0,
    'item' => 0,
    'province_list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565c9c07de4ea6_59653203',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565c9c07de4ea6_59653203')) {
function content_565c9c07de4ea6_59653203 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1664594852565c9c07dbfd41_90126822';
?>
<?php echo '<script'; ?>
 type="text/javascript">
$(document).ready(function () {

    $('#example2').DataTable();

});

<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pusdiklat
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pusdiklat</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pusdiklat</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Province</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['pusdiklat_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                <tr role="row">
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_name;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->address;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default" name="btnEdit" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_id;?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_name;?>
">
                                                <i class="fa fa-edit"> Edit</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Pusdiklat Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="pusdiklat_id" name="pusdiklat_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="pusdiklat_name" class="col-sm-2 control-label">Pusdiklat Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="pusdiklat_name" name="pusdiklat_name" placeholder="pusdiklat_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="address" name="address" placeholder="address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="longitude" class="col-sm-2 control-label">Longitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="longitude" name="longitude" placeholder="longitude"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="latitude" class="col-sm-2 control-label">Latitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="latitude" name="latitude" placeholder="latitude"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="province_id" class="col-sm-2 control-label">Province</label>
                                <div class="col-sm-10">
                                    <select id="province_id" name="province_id" class="form-control">
                                        <option value="0">-- Choose Province --</option>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['province_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->province_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
</option>
                                        <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                    </select>
                                </div>
                            </div>
                            <h5 class="modal-title">Contact Person</h5>
                            <div class="form-group">
                                <label for="contact_person_name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_name" name="contact_person_name" placeholder="name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_person_email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_email" name="contact_person_email" placeholder="email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_person_phone" class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_phone" name="contact_person_phone" placeholder="phone"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<?php echo '<script'; ?>
 type="text/javascript">
$(document).ready(function () {

    var original_stock = 0;

    $('#btnCreate').click(function () {
        clearModalForm();
        $('#layoutModal').modal('show');
    });

    $('button[name="btnEdit"]').on('click', function () {
        var data_id = $(this).attr('data-id');

        if (data_id === '')
            return;

        clearModalForm();

        $('#layoutModal').modal('show');
        $('#response_error').html('Please wait...');
        $('#response_error').removeClass('hidden');
        $('#btnSubmitForm').attr("disabled", "true");

        $.ajax({
                url: 'pusdiklat/detail?pusdiklat_id=' + data_id,
                success: function (resp) {
                    hideErrorMessage();
                    $('#btnSubmitForm').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        var item = resp.item;

                        $('#pusdiklat_id').val(item.pusdiklat_id);
                        $('#pusdiklat_name').val(item.pusdiklat_name);
                        $('#address').val(item.address);
                        $('#longitude').val(item.longitude);
                        $('#latitude').val(item.latitude);
                        $('#province_id').val(item.province_id);
                        $('#contact_person_name').val(item.contact_person_name);
                        $('#contact_person_email').val(item.contact_person_email);
                        $('#contact_person_phone').val(item.contact_person_phone);
                        
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            })

    });

    $('#btnSubmitForm').click(function () {

        prepareSubmit();

        var url = "pusdiklat/save";

        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: $('#main_form').serialize(),
            success: function (resp) {
                if (resp.rcode === 1) {
                    $('#pusdiklat_id').val(resp.pusdiklat_id);

                    $('#response_succeed').html(resp.message);
                    $('#response_succeed').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');

                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#response_error').html(jqXHR.responseText);
                $('#response_error').removeClass('hidden');
                $('#btnSubmitForm').removeAttr('disabled');
            }
        });
});

function clearModalForm() {
    $('#btnSubmitForm').removeAttr("disabled");

    $('#role_id').val('');
    $('#new_role_id').val('');
    $('#role_name').val('');

    hideErrorMessage();
    hideProgressBar();
}

function hideErrorMessage() {
    $('#response_error').html('');
    $('#response_error').addClass('hidden');
}

function hideProgressBar() {
    $('#progressModal').addClass('hidden');
    $('#progressBar').width(0);
    $('#progressBar').text(0);
}

function prepareSubmit() {
    $('#btnSubmitForm').attr("disabled", "true");
    hideErrorMessage();
}

});
<?php echo '</script'; ?>
><?php }
}
?>