<?php /* Smarty version 3.1.27, created on 2015-11-30 20:36:44
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/modules/master/views/city_list.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:760523231565c50ecbf7b68_95456924%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b8a78dd54a04a590c6b7ec3dfd981eb44849177' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/modules/master/views/city_list.tpl',
      1 => 1448207823,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '760523231565c50ecbf7b68_95456924',
  'variables' => 
  array (
    'city_list' => 0,
    'item' => 0,
    'province_list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565c50ecc47903_52183893',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565c50ecc47903_52183893')) {
function content_565c50ecc47903_52183893 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '760523231565c50ecbf7b68_95456924';
?>
<?php echo '<script'; ?>
 type="text/javascript">
$(document).ready(function () {

    $('#example2').DataTable();

});

<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            City
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">City</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">City</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Longitude</th>
                                    <th>Latitude</th>
                                    <th>Province</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['city_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                <tr role="row">
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->city_code;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->city_name;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->longitude;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->latitude;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default" name="btnEdit" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->city_id;?>
" data-code="<?php echo $_smarty_tpl->tpl_vars['item']->value->city_code;?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value->city_name;?>
" data-longitude="<?php echo $_smarty_tpl->tpl_vars['item']->value->longitude;?>
" data-latitude="<?php echo $_smarty_tpl->tpl_vars['item']->value->latitude;?>
" data-province-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->province_id;?>
">
                                                <i class="fa fa-edit"> Edit</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">City Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="city_id" name="city_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="city_code" class="col-sm-2 control-label">City Code</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="city_code" name="city_code" placeholder="city_code"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="city_name" class="col-sm-2 control-label">City Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="city_name" name="city_name" placeholder="city_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="longitude" class="col-sm-2 control-label">Longitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="longitude" name="longitude" placeholder="longitude"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="latitude" class="col-sm-2 control-label">Latitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="latitude" name="latitude" placeholder="latitude"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="province_id" class="col-sm-2 control-label">Province</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="province_id" name="province_id">
                                        <option value="">-- Choose Province --</option>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['province_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->province_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
</option>
                                        <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                    </select>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<?php echo '<script'; ?>
 type="text/javascript">
$(document).ready(function () {

    $('#btnCreate').click(function () {
        clearModalForm();
        $('#layoutModal').modal('show');
    });

    $('button[name="btnEdit"]').on('click', function () {
        var data_id = $(this).attr('data-id');
        var data_code = $(this).attr('data-code');
        var data_name = $(this).attr('data-name');
        var data_longitude = $(this).attr('data-longitude');
        var data_latitude = $(this).attr('data-latitude');
        var data_province_id = $(this).attr('data-province-id');


        if (data_id === '')
            return;

        clearModalForm();

        $('#layoutModal').modal('show');
        
        $('#city_id').val(data_id);
        $('#city_code').val(data_code);
        $('#city_name').val(data_name);
        $('#longitude').val(data_longitude);
        $('#latitude').val(data_latitude);
        $('#province_id').val(data_province_id);

    });

    $('#btnSubmitForm').click(function () {

        prepareSubmit();

        var url = "city/save";

        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: $('#main_form').serialize(),
            success: function (resp) {
                if (resp.rcode === 1) {

                    $('#response_succeed').html(resp.message);
                    $('#response_succeed').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');

                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#response_error').html(jqXHR.responseText);
                $('#response_error').removeClass('hidden');
                $('#btnSubmitForm').removeAttr('disabled');
            }
        });
});

function clearModalForm() {
    $('#btnSubmitForm').removeAttr("disabled");

    $('#city_id').val('');
    $('#city_code').val('');
    $('#city_name').val('');
    $('#longitude').val('');
    $('#latitude').val('');
    $('#province_id').val('');

    hideErrorMessage();
    hideProgressBar();
}

function hideErrorMessage() {
    $('#response_error').html('');
    $('#response_error').addClass('hidden');
}

function hideProgressBar() {
    $('#progressModal').addClass('hidden');
    $('#progressBar').width(0);
    $('#progressBar').text(0);
}

function prepareSubmit() {
    $('#btnSubmitForm').attr("disabled", "true");
    hideErrorMessage();
}

});
<?php echo '</script'; ?>
><?php }
}
?>