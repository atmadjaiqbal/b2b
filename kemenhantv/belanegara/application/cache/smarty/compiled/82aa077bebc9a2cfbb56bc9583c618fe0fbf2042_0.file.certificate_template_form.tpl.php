<?php /* Smarty version 3.1.27, created on 2015-11-30 19:20:47
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/modules/master/views/certificate_template_form.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:643498169565c3f1f42ac58_04708663%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '82aa077bebc9a2cfbb56bc9583c618fe0fbf2042' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/modules/master/views/certificate_template_form.tpl',
      1 => 1448492373,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '643498169565c3f1f42ac58_04708663',
  'variables' => 
  array (
    'template' => 0,
    'default_content_text' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565c3f1f47bff4_10765061',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565c3f1f47bff4_10765061')) {
function content_565c3f1f47bff4_10765061 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '643498169565c3f1f42ac58_04708663';
?>
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>
<link rel="stylesheet" href="<?php echo assets_path('js/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>
">

<?php echo js("iCheck/iCheck.min.js");?>

<?php echo js("bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js");?>


<?php echo '<script'; ?>
 type="text/javascript">

    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }

    $(document).ready(function () {

        $('#content_text').wysihtml5({
            toolbar : {
                'html' : true
            }
        });

        setupViewValues();

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "template_save";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#mainForm').serialize(),
                success: function (resp) {
                    $('#btnSubmitForm').removeAttr('disabled');

                    if (resp.rcode === 1) {
                        $('#template_id').val(resp.template_id);
                        console.log("template_id" + resp.template_id);
                        if (!$('#mainForm input[name=file_name]')[0].files[0]) {
                            // location.reload();
                            $('#response_success').html(resp.message);
                            $('#response_success').removeClass('hidden');
                        } else {
                            uploadThumbnail(resp.template_id);
                        }
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btnSubmitForm').removeAttr('disabled');

                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function uploadThumbnail(template_id) {
            $('#progressModal').removeClass('hidden');

            var url = "<?php echo base_url('/certificate/certificate/template_upload');?>
";

            var xhr = new XMLHttpRequest();
            (xhr.upload || xhr).addEventListener('progress', function (e) {
                var done = e.loaded;
                var total = e.total;
                var percent = Math.round(done / total * 100) + '%';
                $('#progressBar').width(percent);
                $('#progressBar').text(percent);
            });

            xhr.addEventListener('load', function (e) {
                $('#btnSubmitForm').removeAttr('disabled');
                console.log(this.responseText);
                try {
                    var resp = JSON.parse(this.responseText);
                    if (resp.rcode === 1) {
                        // location.reload();

                        $('#response_success').html(resp.message);
                        $('#response_success').removeClass('hidden');
                        
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');

                        hideProgressBar();
                    } 
                } catch (e) {
                    $('#response_error').html(this.responseText);
                    $('#response_error').removeClass('hidden');

                    hideProgressBar();
                }

            });
            xhr.open('post', url, true);

            var data;
            data = new FormData();
            data.append('file', $('#mainForm input[name=file_name]')[0].files[0]);
            data.append('template_id', template_id);

            xhr.send(data);
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');

            $('#response_success').html('');
            $('#response_success').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

        function setupViewValues() {
            <?php if ($_smarty_tpl->tpl_vars['template']->value) {?>
            $('#template_id').val('<?php echo $_smarty_tpl->tpl_vars['template']->value->template_id;?>
');
            $('#template_title').val('<?php echo $_smarty_tpl->tpl_vars['template']->value->template_title;?>
');
            <?php }?>
        }
    });
<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Certificate Template Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url('administration/customer/');?>
"><i class="fa fa-dashboard"></i> Certificate Template</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Certificate Template Form
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="mainForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="template_id" name="template_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="template_title" class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="template_title" name="template_title" placeholder="template_title"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="file_name" class="col-sm-2 control-label">Background</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" id="file_name" name="file_name"/>
                                            </div>
                                        </div>
                                    </div><!-- /.box-body -->
                                </div>
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <textarea id="content_text" name="content_text" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                            <?php if ($_smarty_tpl->tpl_vars['template']->value) {?>
                                            <?php echo $_smarty_tpl->tpl_vars['template']->value->content_text;?>

                                            <?php } else { ?>
                                            <?php echo $_smarty_tpl->tpl_vars['default_content_text']->value;?>

                                            <?php }?>
                                        </textarea>
                                    </div>
                                    <div class="callout callout-danger hidden" id="response_error"></div>
                                    <div class="callout callout-success hidden" id="response_success"></div>
                                    <div class="box-footer">
                                        <a href="<?php echo base_url('master/certificate/template_list');?>
"><button type="button" class="btn btn-default">Go Back</button></a>
                                        <button type="button" id="btnSubmitForm" class="btn btn-info pull-right">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper --><?php }
}
?>