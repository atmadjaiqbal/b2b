<?php /* Smarty version 3.1.27, created on 2015-12-01 02:44:20
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/modules/master/views/institution_list.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:848568675565ca7142b8482_10094304%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '00b3cde806de85b762f982e2f204bd30259b3461' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/modules/master/views/institution_list.tpl',
      1 => 1448912658,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '848568675565ca7142b8482_10094304',
  'variables' => 
  array (
    'institution_list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565ca714304fd1_88527269',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565ca714304fd1_88527269')) {
function content_565ca714304fd1_88527269 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '848568675565ca7142b8482_10094304';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Institution
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Institution</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Institution</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Alias</th>
                                    <th>Address</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['institution_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                <tr role="row">
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->institution_name;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->institution_alias;?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->institution_address;?>
</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default" name="btnEdit" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->institution_id;?>
">
                                                <i class="fa fa-edit"> Edit</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Institution Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="institution_id" name="institution_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="institution_name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="institution_name" name="institution_name" placeholder="institution_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="institution_alias" class="col-sm-2 control-label">Alias</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="institution_alias" name="institution_alias" placeholder="institution_alias"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="institution_address" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="institution_address" name="institution_address" placeholder="institution_address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="website" class="col-sm-2 control-label">Website</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="website" name="website" placeholder="website"/>
                                </div>
                            </div>
                            <h5 class="modal-title">Contact Person</h5>
                            <div class="form-group">
                                <label for="contact_person_name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_name" name="contact_person_name" placeholder="name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_person_email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_email" name="contact_person_email" placeholder="email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_person_phone" class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_phone" name="contact_person_phone" placeholder="contact_person_phone"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_person_fax" class="col-sm-2 control-label">Fax</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_fax" name="contact_person_fax" placeholder="contact_person_fax"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error"></div>
                            <div class="callout callout-success hidden" id="response_succeed"></div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="btnRemoveItem">Remove</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<?php echo '<script'; ?>
 type="text/javascript">
$(document).ready(function () {

    $('#example2').DataTable();

    $('#btnCreate').click(function () {
        clearModalForm();
        $('#layoutModal').modal('show');
    });

    $('button[name="btnEdit"]').on('click', function () {
        var data_id = $(this).attr('data-id');

        if (data_id === '')
            return;

        clearModalForm();

        $('#layoutModal').modal('show');
        $('#response_error').html('Please wait...');
        $('#response_error').removeClass('hidden');
        $('#btnSubmitForm').attr("disabled", "true");

        $.ajax({
                url: 'institution/detail?institution_id=' + data_id,
                success: function (resp) {
                    hideErrorMessage();
                    $('#btnSubmitForm').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        var item = resp.item;

                        $('#institution_id').val(item.institution_id);
                        $('#institution_name').val(item.institution_name);
                        $('#institution_address').val(item.institution_address);
                        $('#institution_alias').val(item.institution_alias);
                        $('#website').val(item.website);
                        $('#contact_person_name').val(item.contact_person_name);
                        $('#contact_person_email').val(item.contact_person_email);
                        $('#contact_person_phone').val(item.contact_person_phone);
                        $('#contact_person_fax').val(item.contact_person_fax);

                        $('#btnRemoveItem').removeClass("hidden");
                        
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            })

    });

    $('#btnSubmitForm').click(function () {

        prepareSubmit();

        var url = "institution/save";

        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: $('#main_form').serialize(),
            success: function (resp) {
                if (resp.rcode === 1) {
                    $('#institution_id').val(resp.institution_id);

                    $('#response_succeed').html(resp.message);
                    $('#response_succeed').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');

                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#response_error').html(jqXHR.responseText);
                $('#response_error').removeClass('hidden');
                $('#btnSubmitForm').removeAttr('disabled');
            }
        });
    });

    $('#btnRemoveItem').click(function () {

        var res = confirm('Are you sure want to delete item?');
        if (!res) return;

        prepareSubmit();

        var url = "institution/delete";

        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: $('#main_form').serialize(),
            success: function (resp) {
                if (resp.rcode === 1) {

                    $('#response_succeed').html(resp.message);
                    $('#response_succeed').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');

                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#response_error').html(jqXHR.responseText);
                $('#response_error').removeClass('hidden');
                $('#btnSubmitForm').removeAttr('disabled');
            }
        });
    });

    function clearModalForm() {
        $('#btnSubmitForm').removeAttr("disabled");
        $('#btnRemoveItem').addClass("hidden");


        $('#institution_id').val('');
        $('#institution_name').val('');
        $('#institution_address').val('');
        $('#institution_alias').val('');
        $('#website').val('');
        $('#contact_person_name').val('');
        $('#contact_person_email').val('');
        $('#contact_person_phone').val('');
        $('#contact_person_fax').val('');

        hideErrorMessage();
        hideProgressBar();
    }

    function hideErrorMessage() {
        $('#response_error').html('');
        $('#response_error').addClass('hidden');
    }

    function hideProgressBar() {
        $('#progressModal').addClass('hidden');
        $('#progressBar').width(0);
        $('#progressBar').text(0);
    }

    function prepareSubmit() {
        $('#btnSubmitForm').attr("disabled", "true");
        hideErrorMessage();
    }

    });
<?php echo '</script'; ?>
><?php }
}
?>