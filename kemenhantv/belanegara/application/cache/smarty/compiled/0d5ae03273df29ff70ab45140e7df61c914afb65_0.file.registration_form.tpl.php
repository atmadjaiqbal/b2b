<?php /* Smarty version 3.1.27, created on 2015-12-02 15:52:19
         compiled from "/var/www/html/kemenhantv/belanegara/application/modules/administration/views/registration_form.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1594453015565eb143639412_82105851%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d5ae03273df29ff70ab45140e7df61c914afb65' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/modules/administration/views/registration_form.tpl',
      1 => 1449045492,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1594453015565eb143639412_82105851',
  'variables' => 
  array (
    'customer' => 0,
    'pusdiklat_list' => 0,
    'item' => 0,
    'institution_list' => 0,
    'id_type_list' => 0,
    'religion_list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eb14366f3e6_41085628',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eb14366f3e6_41085628')) {
function content_565eb14366f3e6_41085628 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1594453015565eb143639412_82105851';
?>
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/daterangepicker/daterangepicker-bs3.css');?>
" media="screen"/>

<?php echo js("moment/moment.min.js");?>

<?php echo js("daterangepicker/daterangepicker.js");?>


<?php echo '<script'; ?>
 type="text/javascript">



    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }

    $(document).ready(function () {

        setupViewValues();

        $('#birth_date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
              format: 'YYYY-MM-DD'
            }
        });

        $('#pusdiklat_id').select2();
        $('#institution_id').select2();
        $('#id_type').select2();
        $('#religion_id').select2();

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#theFormSubmit').click(function () {
            $(this).attr('disabled', true);
            clearAlert();

            var url = "<?php echo base_url('administration/customer/customer_save');?>
";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#theForm').serialize(),
                success: function (resp) {
                    $('#theFormSubmit').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#customer_id').val(resp.customer_id);

                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#theFormSubmit').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(jqXHR.responseText);
                }
            });
        });

        function setupViewValues() {
            <?php if ($_smarty_tpl->tpl_vars['customer']->value) {?>
            $('#customer_id').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->customer_id;?>
');
            $('#pusdiklat_id').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->pusdiklat_id;?>
');
            $('#jabatan').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->jabatan;?>
');
            $('#id_no').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->id_no;?>
');
            $('#id_type').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->id_type;?>
');
            $('#full_name').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->full_name;?>
');
            $('#address').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->address;?>
');
            $('#phone_no').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->phone_no;?>
');
            $('#occupation').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->occupation;?>
');
            $('#birth_date').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->birth_date;?>
');
            $('#birth_place').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->birth_place;?>
');
            $('#religion_id').val('<?php echo $_smarty_tpl->tpl_vars['customer']->value->religion_id;?>
');
            <?php }?>
        }
    });
<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url('administration/customer/');?>
"><i class="fa fa-dashboard"></i> Customer</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Customer Form</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="customer_id" name="customer_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="pusdiklat_id" class="col-sm-3 control-label">Pusdiklat</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="pusdiklat_id" name="pusdiklat_id">
                                                    <option value="">-- Choose Pusdiklat --</option>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['pusdiklat_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_name;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="institution_id" class="col-sm-3 control-label">Institution</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="institution_id" name="institution_id">
                                                    <option value="">-- Choose Institution --</option>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['institution_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->institution_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->institution_name;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value->institution_alias;?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="jabatan" class="col-sm-3 control-label">Jabatan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="jabatan"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="id_no" class="col-sm-3 control-label">No Identitas</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="id_no" name="id_no" placeholder="id_no"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_type" class="col-sm-3 control-label">Tipe Identitas</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="id_type" name="id_type">
                                                    <option value="">-- Choose your ID Type --</option>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['id_type_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->id_type;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->id_name;?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="full_name" class="col-sm-3 control-label">Nama Lengkap</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="full_name" name="full_name" placeholder="full_name"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="address" class="col-sm-3 control-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="address" name="address" placeholder="address"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone_no" class="col-sm-3 control-label">No Telp</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="phone_no"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="occupation" class="col-sm-3 control-label">Pekerjaan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="occupation" name="occupation" placeholder="occupation"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="birth_date" class="col-sm-3 control-label">Tgl Lahir</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="birth_date" name="birth_date" placeholder="birth_date" value=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="birth_place" class="col-sm-3 control-label">Tempat Lahir</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="birth_place" name="birth_place" placeholder="birth_place"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion_id" class="col-sm-3 control-label">Agama</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="religion_id" name="religion_id">
                                                    <option value="">-- Choose your religion --</option>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['religion_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->religion_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->religion_name;?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="<?php echo base_url('administration/customer');?>
"><button type="button" class="btn btn-default">Go Back</button></a>
                        <button type="button" id="theFormSubmit" class="btn btn-info pull-right">Submit</button>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper --><?php }
}
?>