<?php /* Smarty version 3.1.27, created on 2015-12-02 15:40:20
         compiled from "/var/www/html/kemenhantv/belanegara/application/modules/index/views/index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1052169624565eae741c9fe2_71843203%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a9d5e036824f29b075ee5ad6df382c630eac3fd1' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/modules/index/views/index.tpl',
      1 => 1449045506,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1052169624565eae741c9fe2_71843203',
  'variables' => 
  array (
    'province_list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eae741e2546_99330820',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eae741e2546_99330820')) {
function content_565eae741e2546_99330820 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1052169624565eae741c9fe2_71843203';
?>
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/jvectormap/jquery-jvectormap-1.2.2.css');?>
" media="screen"/>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>150</h3>
                        <p>Total Peserta</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>53</h3>
                        <p>Total Institusi</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>120</h3>
                        <p>Total Sertifikat</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            
        </div><!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-12 connectedSortable">
                <!-- solid sales graph -->
                <div class="box box-success">
                    <div class="box-header with-border">
                      <h3 class="box-title">Visitors Report</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                      <div class="row">
                        <div class="col-md-9 col-sm-8">
                          <div class="pad">
                            <!-- Map will be created here -->
                            <div id="world-map-markers" style="height: 325px;"></div>
                          </div>
                        </div><!-- /.col -->
                        <div class="col-md-3 col-sm-4">
                          <div class="pad box-pane-right bg-green" style="min-height: 280px">
                            <div class="description-block margin-bottom">
                              <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div>
                              <h5 class="description-header">8390</h5>
                              <span class="description-text">Visits</span>
                            </div><!-- /.description-block -->
                            <div class="description-block margin-bottom">
                              <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                              <h5 class="description-header">30%</h5>
                              <span class="description-text">Referrals</span>
                            </div><!-- /.description-block -->
                            <div class="description-block">
                              <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                              <h5 class="description-header">70%</h5>
                              <span class="description-text">Organic</span>
                            </div><!-- /.description-block -->
                          </div>
                        </div><!-- /.col -->
                      </div><!-- /.row -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </section><!-- right col -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php echo js("jvectormap/jquery-jvectormap-1.2.2.min.js");?>

<?php echo js("jvectormap/jquery-jvectormap-world-mill-en.js");?>


<?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function() {


        $('#world-map-markers').vectorMap({
            map: 'world_mill_en',
            normalizeFunction: 'polynomial',
            hoverOpacity: 0.7,
            hoverColor: false,
            backgroundColor: 'transparent',
            regionStyle: {
              initial: {
                fill: 'rgba(210, 214, 222, 1)',
                "fill-opacity": 1,
                stroke: 'none',
                "stroke-width": 0,
                "stroke-opacity": 1
              },
              hover: {
                "fill-opacity": 0.7,
                cursor: 'pointer'
              },
              selected: {
                fill: 'yellow'
              },
              selectedHover: {
              }
            },
            markerStyle: {
              initial: {
                fill: '#00a65a',
                stroke: '#111'
              }
            },
            markers: [
            <?php
$_from = $_smarty_tpl->tpl_vars['province_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                {latLng: [<?php echo $_smarty_tpl->tpl_vars['item']->value->latitude;?>
, <?php echo $_smarty_tpl->tpl_vars['item']->value->longitude;?>
], name: '<?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
'},
            <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
            // 
            //   {latLng: [41.90, 12.45], name: 'Vatican City'},
            //   {latLng: [43.73, 7.41], name: 'Monaco'},
            //   {latLng: [-0.52, 166.93], name: 'Nauru'},
            //   {latLng: [-8.51, 179.21], name: 'Tuvalu'},
            //   {latLng: [43.93, 12.46], name: 'San Marino'},
            //   {latLng: [47.14, 9.52], name: 'Liechtenstein'},
            //   {latLng: [7.11, 171.06], name: 'Marshall Islands'},
            //   {latLng: [17.3, -62.73], name: 'Saint Kitts and Nevis'},
            //   {latLng: [3.2, 73.22], name: 'Maldives'},
            //   {latLng: [35.88, 14.5], name: 'Malta'},
            //   {latLng: [12.05, -61.75], name: 'Grenada'},
            //   {latLng: [13.16, -61.23], name: 'Saint Vincent and the Grenadines'},
            //   {latLng: [13.16, -59.55], name: 'Barbados'},
            //   {latLng: [17.11, -61.85], name: 'Antigua and Barbuda'},
            //   {latLng: [-4.61, 55.45], name: 'Seychelles'},
            //   {latLng: [7.35, 134.46], name: 'Palau'},
            //   {latLng: [42.5, 1.51], name: 'Andorra'},
            //   {latLng: [14.01, -60.98], name: 'Saint Lucia'},
            //   {latLng: [6.91, 158.18], name: 'Federated States of Micronesia'},
            //   {latLng: [1.3, 103.8], name: 'Singapore'},
            //   {latLng: [1.46, 173.03], name: 'Kiribati'},
            //   {latLng: [-21.13, -175.2], name: 'Tonga'},
            //   {latLng: [15.3, -61.38], name: 'Dominica'},
            //   {latLng: [-20.2, 57.5], name: 'Mauritius'},
            //   {latLng: [26.02, 50.55], name: 'Bahrain'},
            //   {latLng: [0.33, 6.73], name: 'São Tomé and Príncipe'}
            // 
            ]
          });
    });
<?php echo '</script'; ?>
><?php }
}
?>