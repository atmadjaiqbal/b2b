<?php /* Smarty version 3.1.27, created on 2015-12-01 02:46:07
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/modules/account/views/account_form.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2088136681565ca77fcdc366_36174783%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '16a0acc4e36f30896d53d0f5017ad1fd83d7492f' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/modules/account/views/account_form.tpl',
      1 => 1448908476,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2088136681565ca77fcdc366_36174783',
  'variables' => 
  array (
    'account' => 0,
    'role_list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565ca77fd29db6_51486366',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565ca77fd29db6_51486366')) {
function content_565ca77fd29db6_51486366 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2088136681565ca77fcdc366_36174783';
?>
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>

<?php echo js("iCheck/iCheck.min.js");?>


<?php echo '<script'; ?>
 type="text/javascript">

    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }
    
    $(document).ready(function () {

        setupViewValues();

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#theFormSubmit').click(function () {
            $(this).attr('disabled', true);
            clearAlert();

            var url = "<?php echo base_url('account/account_list/save');?>
";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#theForm').serialize(),
                success: function (resp) {
                    $('#theFormSubmit').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#theFormSubmit').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(jqXHR.responseText);
                }
            });
        });

        function setupViewValues() {
            $('#account_id').val('<?php echo $_smarty_tpl->tpl_vars['account']->value->account_id;?>
');
            $('#user_id').val('<?php echo $_smarty_tpl->tpl_vars['account']->value->user_id;?>
');
            $('#user_email').val('<?php echo $_smarty_tpl->tpl_vars['account']->value->user_email;?>
');
            $('#user_phone').val('<?php echo $_smarty_tpl->tpl_vars['account']->value->user_phone;?>
');
            $('#user_name').val('<?php echo $_smarty_tpl->tpl_vars['account']->value->user_name;?>
');
        }
    });
<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Account Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url('account/account_list');?>
"><i class="fa fa-dashboard"></i> Account</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Account Form</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="account_id" name="account_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="user_id" class="col-sm-3 control-label">User ID</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="user_id" name="user_id" placeholder="User ID" readonly="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_name" class="col-sm-3 control-label">Full Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Full Name"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_phone" class="col-sm-3 control-label">Phone No</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="user_phone" name="user_phone" placeholder="Phone No"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="account_role" class="col-sm-3 control-label">Role</label>
                                            <div class="col-sm-9">
                                                <?php
$_from = $_smarty_tpl->tpl_vars['role_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                <div class="checkbox icheck">
                                                    <label><input type="checkbox" id="account_role_<?php echo $_smarty_tpl->tpl_vars['item']->value->role_id;?>
" name="account_role[]" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->role_id;?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value->checked) {?>checked<?php }?>> <?php echo $_smarty_tpl->tpl_vars['item']->value->role_name;?>
</label>
                                                </div>
                                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="<?php echo base_url('account/account_list');?>
"><button type="button" class="btn btn-default">Go Back</button></a>
                        <button type="button" id="theFormSubmit" class="btn btn-info pull-right">Submit</button>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php }
}
?>