<?php /* Smarty version 3.1.27, created on 2015-11-30 11:46:34
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/modules/account/views/role_list.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:433223861565bd4aa22ddb7_18340196%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d9ba089530ceaa7fd212ea88dbe8b32be7dba5c' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/modules/account/views/role_list.tpl',
      1 => 1448187087,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '433223861565bd4aa22ddb7_18340196',
  'variables' => 
  array (
    'role_list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565bd4aa271e34_65755044',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565bd4aa271e34_65755044')) {
function content_565bd4aa271e34_65755044 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '433223861565bd4aa22ddb7_18340196';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Role
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Role</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Role</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Role ID</th>
                                    <th>Role Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['role_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                    <tr role="row">
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->role_id;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->role_name;?>
</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-default" name="btnEdit" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->role_id;?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value->role_name;?>
">
                                                    <i class="fa fa-edit"> Edit</i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Role Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="role_id" name="role_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="role_id" class="col-sm-2 control-label">Role ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="new_role_id" name="new_role_id" placeholder="role_id"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="role_name" class="col-sm-2 control-label">Role Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="role_name" name="role_name" placeholder="role_name"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div><!-- /.content-wrapper -->

<?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

        $('#btnCreate').click(function () {
            location.href = '<?php echo base_url("account/role/role_privilege");?>
';
        });

        $('button[name="btnPriviledge"').on('click', function() {
            var role_id = $(this).attr('data-id');

            location.href = '<?php echo base_url("account/role/role_privilege");?>
?role_id=' + role_id;

        });

        $('button[name="btnEdit"]').on('click', function () {
            var role_id = $(this).attr('data-id');
            var role_name = $(this).attr('data-name');

            if (role_id === '')
                return;

            location.href = '<?php echo base_url("account/role/role_privilege");?>
?role_id=' + role_id;
            return;

            clearModalForm();

            $('#layoutModal').modal('show');

            $('#role_id').val(role_id);
            $('#role_name').val(role_name);
            $('#new_role_id').val(role_id);
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "role/save_role";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#main_form').serialize(),
                success: function (resp) {
                    console.log('resp:' + resp.rcode);
                    if (resp.rcode === 1) {
                        $('#role_id').val(resp.role_id);

                        $('#response_succeed').html(resp.message);
                        $('#response_succeed').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');

                        location.reload();
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(errorThrown);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function clearModalForm() {
            $('#btnSubmitForm').removeAttr("disabled");

            $('#role_id').val('');
            $('#new_role_id').val('');
            $('#role_name').val('');

            hideErrorMessage();
            hideProgressBar();
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

    });
<?php echo '</script'; ?>
><?php }
}
?>