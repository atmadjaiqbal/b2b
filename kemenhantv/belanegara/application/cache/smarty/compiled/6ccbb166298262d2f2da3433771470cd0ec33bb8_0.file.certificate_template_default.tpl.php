<?php /* Smarty version 3.1.27, created on 2015-11-30 19:20:47
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/views/certificate_template_default.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:49321741565c3f1f3bd824_32263833%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6ccbb166298262d2f2da3433771470cd0ec33bb8' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/views/certificate_template_default.tpl',
      1 => 1448558979,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '49321741565c3f1f3bd824_32263833',
  'variables' => 
  array (
    'template_background' => 0,
    'certificate_no' => 0,
    'customer' => 0,
    'certificate' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565c3f1f4029a2_87113632',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565c3f1f4029a2_87113632')) {
function content_565c3f1f4029a2_87113632 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '49321741565c3f1f3bd824_32263833';
?>
<!doctype html>
<html>
  <head>
    <style type="text/css" media="print">
      @page 
      {
        size: auto;   /* auto is the current printer page size */
        margin: 0mm;  /* this affects the margin in the printer settings */
      }

      .page
      {
        -webkit-transform: rotate(90deg); 
        -moz-transform:rotate(90deg);
        filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
      }

      html { 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      }
      br {
        display: block;
        margin: 10px 0;
        line-height:10px;
        content: " ";
      }
    </style>

    <?php echo '<script'; ?>
 type="text/javascript">

      function onReady() {
      // window.print();
      }

      document.addEventListener('DOMContentLoaded', onReady, false);

    <?php echo '</script'; ?>
>
  </head>
  <body class="page" style="margin-top:30px;">
    <div style="width:1024px; height:640px; text-align:center; background: url(<?php echo $_smarty_tpl->tpl_vars['template_background']->value;?>
&w=1000) no-repeat center; padding-top:50px;">
      <div style="text-align:center;">
        <div style="padding:100px;">
          <span style="font-size:15px"><b>KEMENTRIAN PERTAHANAN RI</b></span>
          <br/>
          <span style="font-size:15px"><b>BADAN PENDIDIKAN DAN PELATIHAN</b></span>
          <br/>
          <br/>
          <span style="font-size:18px">NOMOR : <?php echo $_smarty_tpl->tpl_vars['certificate_no']->value;?>
</span>
          <br/>
          <br/>
          <span style="font-size:28px;">PIAGAM PENGHARGAAN</span>
          <br>
          <br>
          <span style="font-size:20px">Diberikan kepada : </span>
          <br>
          <br>
          <div style="text-align:left; margin-left:50px; margin-right:50px; font-size:18px;">
            <table style="width:100%;">
              <tr>
                <td style="width:40%">
                  <span>NAMA</span>
                </td>
                <td style="width:2%">
                  :
                </td>
                <td width="50%">
                  <span><?php echo $_smarty_tpl->tpl_vars['customer']->value->full_name;?>
</span>
                </td>
              </tr>
              <tr>
                <td style="width:40%;">
                  <span>TEMPAT & TANGGAL LAHIR </span>
                </td>
                <td style="width:2%">
                  :
                </td>
                <td width="50%">
                  <span><?php echo $_smarty_tpl->tpl_vars['customer']->value->birth_place;?>
, <?php echo $_smarty_tpl->tpl_vars['customer']->value->birth_date;?>
</span>
                </td>
              </tr>
              <tr>
                <td style="width:40%;">
                  <span>JABATAN</span>
                </td>
                <td style="width:2%">
                  :
                </td>
                <td width="50%">
                  <span><?php echo $_smarty_tpl->tpl_vars['certificate']->value->rank;?>
</span>
                </td>
              </tr>
            </table>
          </div>
          <br/>
          <span style="font-size:18px">PANITIA FORUM DIALOG WAWASAN KEBANGSAAN DAN BELA NEGARA</span>
          <br/>
          <br/>
          <div style="padding-left:30px; text-align:left;">
            <span style="font-size:17px">Yang diselenggarakan di <?php echo $_smarty_tpl->tpl_vars['certificate']->value->address;?>
 Kota <?php echo $_smarty_tpl->tpl_vars['certificate']->value->city_name;?>
, <?php echo $_smarty_tpl->tpl_vars['certificate']->value->province_name;?>
, pada tanggal <?php echo $_smarty_tpl->tpl_vars['certificate']->value->event_date;?>

            </span>
          </div>
          <br/>
          <br/>
          <div align="right">
            <span style="font-size:17px">
                <?php echo $_smarty_tpl->tpl_vars['certificate']->value->province_name;?>
, <?php echo $_smarty_tpl->tpl_vars['certificate']->value->signed_date;?>

            </span>
          </div>
        </div>
      </div>
    </div>
  </body>
</html><?php }
}
?>