<?php /* Smarty version 3.1.27, created on 2015-12-03 11:41:15
         compiled from "/var/www/html/kemenhantv/belanegara/application/views/login.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1199130479565fc7eb887628_34867479%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9aabb4a46324d8189b3aa7b2e1d6eadd1c3f5ef2' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/views/login.tpl',
      1 => 1449117368,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1199130479565fc7eb887628_34867479',
  'variables' => 
  array (
    'error_login' => 0,
    'username' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565fc7eb8a02a4_42457214',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565fc7eb8a02a4_42457214')) {
function content_565fc7eb8a02a4_42457214 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1199130479565fc7eb887628_34867479';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bela Negara | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- FAVICON -->
        <link rel="shortcut icon" href="<?php echo theme_url("assets/img/favicon.ico");?>
" />
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" type="text/css" href="<?php echo theme_url('assets/js/bootstrap/css/bootstrap.min.css');?>
" media="screen"/>
        <!-- Theme style -->
        <?php echo css("AdminLTE.min.css");?>

        <!-- Custom -->
        <?php echo css("login.css");?>


        <!-- jQuery 2.1.4 -->
        <?php echo js("jQuery/jQuery-2.1.4.min.js");?>

        <!-- Bootstrap 3.3.5 -->
        <?php echo js("bootstrap/js/bootstrap.js");?>


        <?php echo '<script'; ?>
 type="text/javascript">
            $(document).ready(function () {

            <?php if ($_smarty_tpl->tpl_vars['error_login']->value) {?>
                $('#login-alert').removeClass('hidden');
            <?php }?>
            });
        <?php echo '</script'; ?>
>

    </head>
    <body class="hold-transition login-page">
        <div class="login-box">

            <div class="login-box-body">

                <div class="login-logo">
                    <a href="<?php echo base_url();?>
"><b>KEMHAN TV</b></a>
                </div><!-- /.login-logo -->
                <p class="login-box-msg">BELA NEGARA ADMIN</p>

                <form action="<?php echo base_url('login/doLogin');?>
" method="POST">
                    <input type="hidden" name="ret" value="<?php echo $_GET['ret'];?>
"/>
                    <div class="form-group has-feedback">
                        <input type="text" name="username" class="form-control" placeholder="User ID" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <!--
                        <input type="lang" class="form-control" placeholder="Language">
                        -->
                        <select id="role_id" name="role_id" class="form-control">
                            <option value="">Super User</option>
                            <option value="">Pusdiklat</option>
                            <option value="">Institution</option>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <!--
                        <input type="lang" class="form-control" placeholder="Language">
                        -->
                        <select id="lang" name="lang" class="form-control">
                            <option value="en_US">English</option>
                            <option value="zh_CN">Bahasa Indonesia</option>
                        </select>

                    </div>
                    <div class="callout callout-danger hidden" id="login-alert">
                        <?php echo $_smarty_tpl->tpl_vars['error_login']->value;?>

                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            
                        </div><!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-warning btn-block btn-flat">Sign In</button>
                        </div><!-- /.col -->
                    </div>
                </form>

                <!--
            <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
            </div>
                -->
                <!-- /.social-auth-links -->
                <!--
            <a href="#">I forgot my password</a><br>
            <a href="register.html" class="text-center">Register a new membership</a>
                -->

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->
    </body>
</html>

<?php }
}
?>