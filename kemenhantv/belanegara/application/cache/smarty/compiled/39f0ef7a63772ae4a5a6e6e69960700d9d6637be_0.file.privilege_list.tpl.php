<?php /* Smarty version 3.1.27, created on 2015-12-02 19:16:15
         compiled from "/var/www/html/kemenhantv/belanegara/application/modules/account/views/privilege_list.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1080673695565ee10f844955_72919415%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39f0ef7a63772ae4a5a6e6e69960700d9d6637be' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/modules/account/views/privilege_list.tpl',
      1 => 1449045488,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1080673695565ee10f844955_72919415',
  'variables' => 
  array (
    'privilege_list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565ee10f864572_41814843',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565ee10f864572_41814843')) {
function content_565ee10f864572_41814843 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1080673695565ee10f844955_72919415';
?>
<?php echo '<script'; ?>
 type="text/javascript">
    
    function onBtnEditClickFunction(data_id, data_name, data_group) {
            // var data_id = $(this).attr('data-id');
            // var data_name = $(this).attr('data-name');
            // var data_group = $(this).attr('data-group');

            if (data_id === '')
                return;

            clearModalForm();

            $('#layoutModal').modal('show');

            $('#privilege_id').val(data_id);
            $('#new_privilege_id').val(data_id);
            $('#privilege_name').val(data_name);
            $('#privilege_group').val(data_group);
        }

        function clearModalForm() {
            $('#btnSubmitForm').removeAttr("disabled");

            $('#privilege_id').val('');
            $('#privilege_name').val('');

            hideErrorMessage();
            hideProgressBar();
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }
    

<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Privilege
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Privilege</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Privilege</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Privilege ID</th>
                                    <th>Privilege Name</th>
                                    <th>Privilege Group</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['privilege_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                    <tr role="row">
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_id;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_name;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_group;?>
</td>
                                        <td>
                                            <div class="btn-group-vertical">
                                                <button class="btn btn-default" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_id;?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_name;?>
" data-group="<?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_group;?>
" onclick="onBtnEditClickFunction('<?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_id;?>
', '<?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_name;?>
', '<?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_group;?>
')">
                                                    <i class="fa fa-edit"> Edit</i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Privilege Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="privilege_id" name="privilege_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="new_privilege_id" class="col-sm-2 control-label">Privilege ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="new_privilege_id" name="new_privilege_id" placeholder="privilege_id"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="privilege_name" class="col-sm-2 control-label">Privilege Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="privilege_name" name="privilege_name" placeholder="privilege_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="privilege_group" class="col-sm-2 control-label">Privilege Group</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="privilege_group" name="privilege_group" placeholder="privilege_group"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div><!-- /.content-wrapper -->

<?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function () {

        $(document).ready(function () {

            $('#example2').DataTable();

        });

        $('#btnCreate').click(function () {
            clearModalForm();
            $('#layoutModal').modal('show');
        });

        $('.btnEdit').bind('click', function () {
            var data_id = $(this).attr('data-id');
            var data_name = $(this).attr('data-name');
            var data_group = $(this).attr('data-group');

            if (data_id === '')
                return;

            clearModalForm();

            $('#layoutModal').modal('show');

            $('#privilege_id').val(data_id);
            $('#new_privilege_id').val(data_id);
            $('#privilege_name').val(data_name);
            $('#privilege_group').val(data_group);
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "privilege/save_privilege";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#main_form').serialize(),
                success: function (resp) {
                    if (resp.rcode === 1) {
                        $('#privilege_id').val(resp.privilege_id);

                        $('#response_succeed').html(resp.message);
                        $('#response_succeed').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');

                        location.reload();
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        

    });
<?php echo '</script'; ?>
><?php }
}
?>