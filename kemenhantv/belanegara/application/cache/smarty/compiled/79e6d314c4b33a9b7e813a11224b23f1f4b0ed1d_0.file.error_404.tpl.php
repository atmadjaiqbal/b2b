<?php /* Smarty version 3.1.27, created on 2015-12-02 15:39:34
         compiled from "/var/www/html/kemenhantv/belanegara/application/views/errors/html/error_404.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:680669672565eae464c79a2_49323132%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '79e6d314c4b33a9b7e813a11224b23f1f4b0ed1d' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/views/errors/html/error_404.tpl',
      1 => 1449045561,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '680669672565eae464c79a2_49323132',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eae464c9ff0_27630355',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eae464c9ff0_27630355')) {
function content_565eae464c9ff0_27630355 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '680669672565eae464c79a2_49323132';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            404 Error Page
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">404 error</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                <p>
                    We could not find the page you were looking for.
                    Meanwhile, you may <a href="<?php echo base_url();?>
">return to dashboard</a> or try using the search form.
                </p>
            </div><!-- /.error-content -->
        </div><!-- /.error-page -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper --><?php }
}
?>