<?php /* Smarty version 3.1.27, created on 2015-12-03 00:49:00
         compiled from "/var/www/html/kemenhantv/belanegara/application/modules/account/views/role_privilege.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:783856265565f2f0cba2ab0_10443308%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6968a19c27b1d8a19970d8b3441e9e2b1218633' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/modules/account/views/role_privilege.tpl',
      1 => 1449045489,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '783856265565f2f0cba2ab0_10443308',
  'variables' => 
  array (
    'role' => 0,
    'privilege_group' => 0,
    'item' => 0,
    'single_item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565f2f0cc21f40_00455546',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565f2f0cc21f40_00455546')) {
function content_565f2f0cc21f40_00455546 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '783856265565f2f0cba2ab0_10443308';
?>
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>

<?php echo js("iCheck/iCheck.min.js");?>


<?php echo '<script'; ?>
 type="text/javascript">

$(document).ready(function () {

    setupViewValues();

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
    });

    
    $('#btnRemove').click(function () {
        var conf = confirm('Are you sure want to delete item?');
        if (!conf) return;


        $(this).attr('disabled', true);
        clearAlert();

        var url = "<?php echo base_url('account/role/delete');?>
";

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: $('#theForm').serialize(),
            success: function (resp) {
                $('#btnRemove').removeAttr('disabled');
                if (resp.rcode === 1) {
                    $('#role_id').val(resp.role_id);

                    $('#response_success').removeClass('hidden');
                    $('#response_success').html(resp.message);

                    location.href = '<?php echo base_url("account/role/");?>
'
                } else {
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(resp.message);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#btnRemove').removeAttr('disabled');
                $('#response_error').removeClass('hidden');
                $('#response_error').html(jqXHR.responseText);
            }
        });
    });

    $('#theFormSubmit').click(function () {
        $(this).attr('disabled', true);
        clearAlert();

        var url = "<?php echo base_url('account/role/save_role');?>
";

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: $('#theForm').serialize(),
            success: function (resp) {
                $('#theFormSubmit').removeAttr('disabled');
                if (resp.rcode === 1) {
                    $('#role_id').val(resp.role_id);

                    $('#response_success').removeClass('hidden');
                    $('#response_success').html(resp.message);

                    location.href = '<?php echo base_url("account/role/");?>
'
                } else {
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(resp.message);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#theFormSubmit').removeAttr('disabled');
                $('#response_error').removeClass('hidden');
                $('#response_error').html(jqXHR.responseText);
            }
        });
    });

function clearAlert() {
    $('#response_error').addClass('hidden');
    $('#response_success').addClass('hidden');
}

function setupViewValues() {
    $('#role_id').val('<?php echo $_smarty_tpl->tpl_vars['role']->value->role_id;?>
');
    $('#new_role_id').val('<?php echo $_smarty_tpl->tpl_vars['role']->value->role_id;?>
');
    $('#role_name').val('<?php echo $_smarty_tpl->tpl_vars['role']->value->role_name;?>
');
}
});
<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Role Privilege
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url('account/role');?>
"><i class="fa fa-dashboard"></i> role</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Role Privilege</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="role_id" name="role_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="new_role_id" class="col-sm-2 control-label">Role ID</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="new_role_id" name="new_role_id" placeholder="Role ID"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="role_name" class="col-sm-2 control-label">Role Name</label>
                                            <div class="col-sm-10">
                                                <input type="role_name" class="form-control" id="role_name" name="role_name" placeholder="Role Name"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="role_privilege" class="col-sm-2 control-label">Privilege</label>
                                            <div class="col-sm-10">
                                                <?php
$_from = $_smarty_tpl->tpl_vars['privilege_group']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                <label for="privilege_group" class="control-label"><?php echo $_smarty_tpl->tpl_vars['item']->key;?>
</label>
                                                <div class="col-sm-offset-1">
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['item']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['single_item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['single_item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['single_item']->value) {
$_smarty_tpl->tpl_vars['single_item']->_loop = true;
$foreach_single_item_Sav = $_smarty_tpl->tpl_vars['single_item'];
?>
                                                    <div class="checkbox icheck">
                                                        <label><input type="checkbox" id="role_privilege_<?php echo $_smarty_tpl->tpl_vars['single_item']->value->role_id;?>
" name="role_privilege[]" value="<?php echo $_smarty_tpl->tpl_vars['single_item']->value->privilege_id;?>
" <?php if ($_smarty_tpl->tpl_vars['single_item']->value->checked) {?>checked<?php }?>>  <?php echo $_smarty_tpl->tpl_vars['single_item']->value->privilege_name;?>
</label>
                                                    </div>
                                                    <?php
$_smarty_tpl->tpl_vars['single_item'] = $foreach_single_item_Sav;
}
?>
                                                </div>
                                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="<?php echo base_url('account/role');?>
"><button type="button" class="btn btn-default">Go Back</button></a>
                        <div class="pull-right">
                            <button type="button" id="btnRemove" class="btn btn-danger">Remove</button>
                            <button type="button" id="theFormSubmit" class="btn btn-info">Submit</button>
                        </div>
                        
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper --><?php }
}
?>