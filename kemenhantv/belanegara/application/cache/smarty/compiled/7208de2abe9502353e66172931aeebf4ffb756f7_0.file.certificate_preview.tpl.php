<?php /* Smarty version 3.1.27, created on 2015-12-03 13:02:48
         compiled from "/var/www/html/kemenhantv/belanegara/application/views/certificate_preview.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2048776492565fdb0865ec24_42948358%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7208de2abe9502353e66172931aeebf4ffb756f7' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/views/certificate_preview.tpl',
      1 => 1449122562,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2048776492565fdb0865ec24_42948358',
  'variables' => 
  array (
    'is_print' => 0,
    'template' => 0,
    'certificate' => 0,
    'customer' => 0,
    'authors' => 0,
    'index' => 0,
    'author' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565fdb08681c07_41910596',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565fdb08681c07_41910596')) {
function content_565fdb08681c07_41910596 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2048776492565fdb0865ec24_42948358';
?>
<!doctype html>
<html>
	<head>
		<style type="text/css" media="print">
			@page 
			{
				size: auto;   /* auto is the current printer page size */
				margin: 0mm;  /* this affects the margin in the printer settings */
			}

			.page
			{
				-webkit-transform: rotate(90deg); 
				-moz-transform:rotate(90deg);
				filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
			}

			html { 
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
			}
			br {
				display: block;
				margin: 10px 0;
				line-height:10px;
				content: " ";
			}
		</style>

		<?php echo '<script'; ?>
 type="text/javascript">

			function onReady() {
			// window.print();
				if (<?php echo $_smarty_tpl->tpl_vars['is_print']->value;?>
) {
					window.print();
				}
			}

			document.addEventListener('DOMContentLoaded', onReady, false);

		<?php echo '</script'; ?>
>
	</head>
	<body class="page" style="margin-top:30px;">
		<div style="width:1024px; height:640px; text-align:center; background: url(<?php echo $_smarty_tpl->tpl_vars['template']->value->background_image;?>
&w=1000) no-repeat center; padding-top:0px;">
			<div style="text-align:center;">
				<div style="padding:70px 50px 50px 100px;">
					<span style="font-size:12px"><b>KEMENTRIAN PERTAHANAN RI</b></span>
					<br/>
					<span style="font-size:12px"><b>BADAN PENDIDIKAN DAN PELATIHAN</b></span>
					<br/>
					<br/>
					<span style="font-size:15px">NOMOR : <?php echo $_smarty_tpl->tpl_vars['certificate']->value->certificate_no;?>
</span>
					<br/>
					<br/>
					<span style="font-size:22px;">PIAGAM PENGHARGAAN</span>
					<br>
					<br>
					<span style="font-size:17px">Diberikan kepada : </span>
					<br>
					<br>
					<div style="text-align:left; margin-left:50px; margin-right:50px; font-size:15px;">
						<table style="width:100%;">
							<tr>
								<td style="width:40%">
									<span>NAMA</span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['customer']->value->full_name, 'UTF-8');?>
</span>
								</td>
							</tr>
							<tr>
								<td style="width:40%;">
									<span>TEMPAT & TANGGAL LAHIR </span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['customer']->value->birth_place, 'UTF-8');?>
, <?php echo $_smarty_tpl->tpl_vars['customer']->value->birth_date;?>
</span>
								</td>
							</tr>
							<tr>
								<td style="width:40%;">
									<span>JABATAN</span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['customer']->value->jabatan, 'UTF-8');?>
</span>
								</td>
							</tr>
						</table>
					</div>
					<br/>
					<span style="font-size:15px">PANITIA FORUM DIALOG WAWASAN KEBANGSAAN DAN BELA NEGARA</span>
					<br/>
					<br/>
					<div style="padding-left:30px; text-align:left;">
						<span style="font-size:15px">Yang diselenggarakan di <?php echo $_smarty_tpl->tpl_vars['customer']->value->pusdiklat_name;?>
 Kota <?php echo $_smarty_tpl->tpl_vars['customer']->value->city_name;?>
, <?php echo $_smarty_tpl->tpl_vars['customer']->value->province_name;?>
, pada tanggal <?php echo $_smarty_tpl->tpl_vars['certificate']->value->last_modified_date;?>
</span>
					</div>
					<br/>
					<br/>
					<div style="font-size:15px;">
						<div align="right" style="padding-right:100px;">
							<span>
									<?php echo $_smarty_tpl->tpl_vars['customer']->value->province_name;?>
, <?php echo $_smarty_tpl->tpl_vars['certificate']->value->last_modified_date;?>

							</span>
						</div>
					</div>
					<br>
					<?php if ($_smarty_tpl->tpl_vars['authors']->value) {?>
					<div align="center" style="text-align:center; padding-left:50px; padding-right:50px; font-size:15px;">
						<table style="">
							<tr>
								<?php $_smarty_tpl->tpl_vars['index'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['index']->step = 1;$_smarty_tpl->tpl_vars['index']->total = (int) ceil(($_smarty_tpl->tpl_vars['index']->step > 0 ? 3+1 - (1) : 1-(3)+1)/abs($_smarty_tpl->tpl_vars['index']->step));
if ($_smarty_tpl->tpl_vars['index']->total > 0) {
for ($_smarty_tpl->tpl_vars['index']->value = 1, $_smarty_tpl->tpl_vars['index']->iteration = 1;$_smarty_tpl->tpl_vars['index']->iteration <= $_smarty_tpl->tpl_vars['index']->total;$_smarty_tpl->tpl_vars['index']->value += $_smarty_tpl->tpl_vars['index']->step, $_smarty_tpl->tpl_vars['index']->iteration++) {
$_smarty_tpl->tpl_vars['index']->first = $_smarty_tpl->tpl_vars['index']->iteration == 1;$_smarty_tpl->tpl_vars['index']->last = $_smarty_tpl->tpl_vars['index']->iteration == $_smarty_tpl->tpl_vars['index']->total;?>
								<td style="width:33%">
									<?php if ($_smarty_tpl->tpl_vars['authors']->value[$_smarty_tpl->tpl_vars['index']->value]) {?>
									<?php $_smarty_tpl->tpl_vars[$_smarty_tpl->tpl_vars['author']->value] = new Smarty_Variable($_smarty_tpl->tpl_vars['authors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['index']['index']], null, 0);?>
									<div>
										<span>
												<?php echo $_smarty_tpl->tpl_vars['authors']->value[$_smarty_tpl->tpl_vars['index']->value]->author_title;?>

										</span>
									</div>
									<div style="height:75px;">
										<?php if ($_smarty_tpl->tpl_vars['authors']->value[$_smarty_tpl->tpl_vars['index']->value]->thumbnail) {?>
										<img src="<?php echo $_smarty_tpl->tpl_vars['authors']->value[$_smarty_tpl->tpl_vars['index']->value]->thumbnail;?>
&w=150" width="150px" height="75px">
										</img>
										<?php } else { ?>
										&nbsp;
										<?php }?>
									</div>
									<br>
									<div>
										<span>
												<?php echo $_smarty_tpl->tpl_vars['authors']->value[$_smarty_tpl->tpl_vars['index']->value]->author_name;?>

										</span>
									</div>
									<div>
										<span style="text-align:center;">
												<?php echo $_smarty_tpl->tpl_vars['authors']->value[$_smarty_tpl->tpl_vars['index']->value]->author_position;?>

										</span>
									</div>
									<?php }?>
								</td>
								<?php }} ?>
							</tr>
						</table>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</body>
</html><?php }
}
?>