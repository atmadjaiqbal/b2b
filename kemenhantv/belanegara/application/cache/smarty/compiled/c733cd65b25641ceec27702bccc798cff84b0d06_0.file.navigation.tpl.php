<?php /* Smarty version 3.1.27, created on 2015-12-02 15:39:34
         compiled from "themes/default/views/layouts/partials/navigation.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:731233033565eae464878b6_30326420%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c733cd65b25641ceec27702bccc798cff84b0d06' => 
    array (
      0 => 'themes/default/views/layouts/partials/navigation.tpl',
      1 => 1449045289,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '731233033565eae464878b6_30326420',
  'variables' => 
  array (
    'navigation_id' => 0,
    'privilege_list' => 0,
    'navigation_menu' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eae464c4052_12430376',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eae464c4052_12430376')) {
function content_565eae464c4052_12430376 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '731233033565eae464878b6_30326420';
?>
        <?php $_smarty_tpl->tpl_vars["privilege_list"] = new Smarty_Variable($_SESSION['user_account']->privilege_list, null, 0);?>

        <ul class="sidebar-menu">
            <li class="header">Main Navigation</li>
            <li <?php if (($_smarty_tpl->tpl_vars['navigation_id']->value == "dashboard")) {?>class="active"<?php }?>>
                <a href="<?php echo base_url();?>
">
                    <i class="fa fa-th"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview<?php if (($_smarty_tpl->tpl_vars['navigation_id']->value == "account")) {?> active<?php }?>" id="account">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Account</span> <i class="fa fa-angle-down pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['account_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "account_list") {?>class="active"<?php }?>><a href="<?php echo base_url('account/account_list');?>
"><i class="fa fa-circle-o"></i> Account</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['role_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "role") {?>class="active"<?php }?>><a href="<?php echo base_url('account/role');?>
"><i class="fa fa-circle-o"></i> Role</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['privilege_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "privilege") {?>class="active"<?php }?>><a href="<?php echo base_url('account/privilege');?>
"><i class="fa fa-circle-o"></i> Privilege</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['account_group_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "account_group") {?>class="active"<?php }?>><a href="<?php echo base_url('account/group');?>
"><i class="fa fa-circle-o"></i> Group</a></li>
                    <?php }?>
                </ul>
            </li>
            <li class="treeview<?php if (($_smarty_tpl->tpl_vars['navigation_id']->value == "master")) {?> active<?php }?>" id="master">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Master Data</span> <i class="fa fa-angle-down pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['province_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "province") {?>class="active"<?php }?>><a href="<?php echo base_url('master/province');?>
"><i class="fa fa-circle-o"></i> Province</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['city_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "city") {?>class="active"<?php }?>><a href="<?php echo base_url('master/city');?>
"><i class="fa fa-circle-o"></i> City</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['pusdiklat_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "pusdiklat") {?>class="active"<?php }?>><a href="<?php echo base_url('master/pusdiklat');?>
"><i class="fa fa-circle-o"></i> Pusdiklat</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['institution_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "institution") {?>class="active"<?php }?>><a href="<?php echo base_url('master/institution');?>
"><i class="fa fa-circle-o"></i> Institution</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['certificate_type_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "certificate_type_list") {?>class="active"<?php }?>><a href="<?php echo base_url('master/certificate/type_list');?>
"><i class="fa fa-circle-o"></i> Certificate Type</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['certificate_template_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "certificate_template_list") {?>class="active"<?php }?>><a href="<?php echo base_url('master/certificate/template_list');?>
"><i class="fa fa-circle-o"></i> Template</a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['privilege_list']->value['certificate_author_view'] == 1) {?>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "certificate_authority") {?>class="active"<?php }?>><a href="<?php echo base_url('master/certificate/certificate_authority');?>
"><i class="fa fa-circle-o"></i> Authority</a></li>
                    <?php }?>
                </ul>
            </li>
            <li class="treeview<?php if (($_smarty_tpl->tpl_vars['navigation_id']->value == "administration")) {?> active<?php }?>" id="administration">
                <a href="#">
                    <i class="fa fa-gift"></i> <span>Administration</span> <i class="fa fa-angle-down pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <!-- Customer -->
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "customer_register") {?>class="active"<?php }?>><a href="<?php echo base_url('administration/customer/registration_form');?>
"><i class="fa fa-circle-o"></i> Register Customer</a></li>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "customer_list") {?>class="active"<?php }?>><a href="<?php echo base_url('administration/customer');?>
"><i class="fa fa-circle-o"></i> Customer List</a></li>
                    <!-- End of Customer -->
                    <!-- Certificate -->
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "certificate_request_list") {?>class="active"<?php }?>><a href="<?php echo base_url('administration/certificate/request_list');?>
"><i class="fa fa-circle-o"></i> Certificate Request</a></li>
                    <li <?php if (($_smarty_tpl->tpl_vars['navigation_menu']->value) == "certificate_list") {?>class="active"<?php }?>><a href="<?php echo base_url('administration/certificate/');?>
"><i class="fa fa-circle-o"></i> Certificate List</a></li>
                </ul>
            </li>
        </ul><?php }
}
?>