<?php /* Smarty version 3.1.27, created on 2015-12-02 15:52:06
         compiled from "themes/default/views/layouts/partials/metadata.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:854124378565eb1361cb137_24416473%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd1afcfe93dd48df02cc287f63c600d410819e1b5' => 
    array (
      0 => 'themes/default/views/layouts/partials/metadata.tpl',
      1 => 1449046322,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '854124378565eb1361cb137_24416473',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eb1361e91d2_02335356',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eb1361e91d2_02335356')) {
function content_565eb1361e91d2_02335356 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '854124378565eb1361cb137_24416473';
?>
<!-- FAVICON -->
<link rel="shortcut icon" href="<?php echo assets_path('img/favicon.ico');?>
" />
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/bootstrap/css/bootstrap.min.css');?>
" media="screen"/>
<!-- Theme style -->
<?php echo css("AdminLTE.min.css");?>

<!-- Font Awesome -->
<?php echo css("font-awesome.min.css");?>

<!-- Ionicons -->
<!-- <?php echo css("ionicons.min.css");?>
 -->

<!-- AdminLTE Skins -->
<?php echo css("skins/skin-red.min.css");?>

<!-- iCheck -->
<!--<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/yellow.css');?>
" media="screen"/>-->
<!-- Morris chart -->
<!--<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/morris/morris.css');?>
" media="screen"/>-->
<!-- jvectormap -->
<!--<link rel="stylesheet" type="text/css" href="<?php echo assets_path('assets/js/jvectormap/jquery-jvectormap-1.2.2.css');?>
" media="screen"/>-->
<!-- Date Picker -->
<!--<link rel="stylesheet" type="text/css" href="<?php echo assets_path('assets/js/datepicker/datepicker3.css');?>
" media="screen"/>-->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/select2/css/select2.min.css');?>
" media="screen"/>
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/datatables/dataTables.bootstrap.css');?>
" media="screen"/>




<!-- Main Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('themes/default/assets/css/main.css?version=1.0');?>
" media="screen"/>
<!-- <?php echo css("main.css?v=1.0");?>
 -->

<!-- Javascript Collections -->
<!-- jQuery 2.1.4 -->
<?php echo js("jQuery/jQuery-2.1.4.min.js");?>

<!-- jQuery UI 1.11.4 -->
<!--<?php echo '<script'; ?>
 src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"><?php echo '</script'; ?>
>-->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!--
<?php echo '<script'; ?>
>
    $.widget.bridge('uibutton', $.ui.button);
<?php echo '</script'; ?>
>
-->
<!-- Bootstrap 3.3.5 -->
<?php echo js("bootstrap/js/bootstrap.min.js");?>

<!-- Morris.js charts -->
<!--<?php echo js("raphael-min.js");?>

<?php echo js("morris/morris.min.js");?>
-->

<?php echo js("app.min.js");?>


<?php echo js("datatables/jquery.dataTables.min.js");?>

<?php echo js("datatables/dataTables.bootstrap.min.js");?>

<?php echo js("select2/js/select2.full.min.js");?>

<?php echo js("iCheck/icheck.min.js");?>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <?php echo js("pages/dashboard.js");?>
 -->
<!-- AdminLTE for demo purposes -->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
<![endif]-->

<?php }
}
?>