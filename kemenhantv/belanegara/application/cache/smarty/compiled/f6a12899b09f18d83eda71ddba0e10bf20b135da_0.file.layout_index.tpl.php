<?php /* Smarty version 3.1.27, created on 2015-12-02 15:39:34
         compiled from "/var/www/html/kemenhantv/belanegara/themes/default/views/layouts/layout_index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:135433166565eae4644c0d4_37090664%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f6a12899b09f18d83eda71ddba0e10bf20b135da' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/themes/default/views/layouts/layout_index.tpl',
      1 => 1449045234,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '135433166565eae4644c0d4_37090664',
  'variables' => 
  array (
    'navigation_title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eae4645c359_00674661',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eae4645c359_00674661')) {
function content_565eae4645c359_00674661 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '135433166565eae4644c0d4_37090664';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bela Negara | <?php echo $_smarty_tpl->tpl_vars['navigation_title']->value;?>
</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <?php echo $_smarty_tpl->getSubTemplate ("partials/metadata.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        
    </head>
    <body class="hold-transition skin-red sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo base_url();?>
" class="logo">
                    <!-- <?php echo image('kemhan_logo_header.png');?>
 -->
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <!-- <span class="logo-mini"><b>Admin</b></span> -->
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Bela</b>Negara</span>
                </a>
                <?php echo $_smarty_tpl->getSubTemplate ("partials/headnavigation.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


            </header>

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <?php echo $_smarty_tpl->getSubTemplate ("partials/navigation.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

                </section>
                <!-- /.sidebar -->
            </aside>

            <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['template_view']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


            <?php echo $_smarty_tpl->getSubTemplate ("partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


        </div><!-- ./wrapper -->
    </body>
</html><?php }
}
?>