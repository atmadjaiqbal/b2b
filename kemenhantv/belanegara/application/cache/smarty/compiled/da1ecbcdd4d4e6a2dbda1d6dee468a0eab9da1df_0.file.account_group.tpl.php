<?php /* Smarty version 3.1.27, created on 2015-11-30 17:51:56
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/modules/account/views/account_group.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:162068301565c2a4caf7465_93432239%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'da1ecbcdd4d4e6a2dbda1d6dee468a0eab9da1df' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/modules/account/views/account_group.tpl',
      1 => 1448828504,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '162068301565c2a4caf7465_93432239',
  'variables' => 
  array (
    'group' => 0,
    'account_list' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565c2a4cb6c687_95056873',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565c2a4cb6c687_95056873')) {
function content_565c2a4cb6c687_95056873 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '162068301565c2a4caf7465_93432239';
?>
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>

<?php echo js("iCheck/iCheck.min.js");?>


<?php echo '<script'; ?>
 type="text/javascript">

$(document).ready(function () {

    setupViewValues();

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

    $('#btnRemove').click(function () {
        $(this).attr('disabled', true);
        clearAlert();

        var url = "<?php echo base_url('account/group/remove_group');?>
";

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: $('#theForm').serialize(),
            success: function (resp) {
                $('#btnRemove').removeAttr('disabled');
                if (resp.rcode === 1) {
                    $('#group_id').val(resp.group_id);

                    $('#response_success').removeClass('hidden');
                    $('#response_success').html(resp.message);

                    location.href = "<?php echo base_url('account/group/');?>
";
                } else {
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(resp.message);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#btnRemove').removeAttr('disabled');
                $('#response_error').removeClass('hidden');
                $('#response_error').html(jqXHR.responseText);
            }
        });
    });

    $('#theFormSubmit').click(function () {
        $(this).attr('disabled', true);
        clearAlert();

        var url = "<?php echo base_url('account/group/save_group');?>
";

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: $('#theForm').serialize(),
            success: function (resp) {
                $('#theFormSubmit').removeAttr('disabled');
                if (resp.rcode === 1) {
                    $('#group_id').val(resp.group_id);

                    $('#response_success').removeClass('hidden');
                    $('#response_success').html(resp.message);
                } else {
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(resp.message);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#theFormSubmit').removeAttr('disabled');
                $('#response_error').removeClass('hidden');
                $('#response_error').html(jqXHR.responseText);
            }
        });
    });

function clearAlert() {
    $('#response_error').addClass('hidden');
    $('#response_success').addClass('hidden');
}

function setupViewValues() {
    <?php if ($_smarty_tpl->tpl_vars['group']->value) {?>
    $('#group_id').val('<?php echo $_smarty_tpl->tpl_vars['group']->value->group_id;?>
');
    $('#group_name').val('<?php echo $_smarty_tpl->tpl_vars['group']->value->group_name;?>
');
    <?php }?>
}
});
<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Account Group
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url('account/group');?>
"><i class="fa fa-dashboard"></i> Group</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Account Group</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <div class="box-body">
                                        <input type="hidden" id="group_id" name="group_id"/>
                                        <div class="form-group">
                                            <label for="group_name" class="col-sm-4 control-label">Group Name</label>
                                            <div class="col-sm-8">
                                                <input type="group_name" class="form-control" id="group_name" name="group_name" placeholder="group_name"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="group_name" class="control-label">Account</label>
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div class="checkbox icheck">
                                                        <label><input type="checkbox" class="check_all" id="check_all" indeterminta="true">&nbsp;</label>
                                                    </div>
                                                </th>
                                                <th>User ID</th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
$_from = $_smarty_tpl->tpl_vars['account_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                <tr role="row">
                                                    <td>
                                                        <div class="checkbox icheck">
                                                            <input type="checkbox" class="check_single" id="customer_check_<?php echo $_smarty_tpl->tpl_vars['item']->value->account_id;?>
" name="account_group[]" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->account_id;?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value->checked_group_id) {?>checked<?php }?>>
                                                        </div>
                                                    </td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->user_id;?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->user_email;?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->user_name;?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value->user_phone;?>
</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-default" name="btnEdit" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->account_id;?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value->user_name;?>
">
                                                                <i class="fa fa-align-left"> Edit</i>
                                                                
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="<?php echo base_url('account/group');?>
"><button type="button" class="btn btn-default">Go Back</button></a>
                        <div class="pull-right">
                            <button type="button" id="btnRemove" class="btn btn-danger">Remove</button>
                            <button type="button" id="theFormSubmit" class="btn btn-info">Submit</button>
                        </div>
                        
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper --><?php }
}
?>