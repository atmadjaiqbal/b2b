<?php /* Smarty version 3.1.27, created on 2015-12-03 11:41:55
         compiled from "/var/www/html/kemenhantv/belanegara/application/modules/master/views/certificate_template_form.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1638262164565fc813589395_61253453%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b95ea4d1573bd1f599357c0d3450bda23054656' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/modules/master/views/certificate_template_form.tpl',
      1 => 1449117614,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1638262164565fc813589395_61253453',
  'variables' => 
  array (
    'template' => 0,
    'authorize_list' => 0,
    'item' => 0,
    'pusdiklat_list' => 0,
    'institution_list' => 0,
    'author_list' => 0,
    'default_content_text' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565fc8135cfd92_82530105',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565fc8135cfd92_82530105')) {
function content_565fc8135cfd92_82530105 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1638262164565fc813589395_61253453';
?>
iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>
<link rel="stylesheet" href="<?php echo assets_path('js/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>
">

<?php echo js("iCheck/iCheck.min.js");?>

<?php echo js("bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js");?>


<?php echo '<script'; ?>
 type="text/javascript">

    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }

    $(document).ready(function () {

        setupViewValues();

        $('#content_text').wysihtml5({
            toolbar : {
                'html' : true
            }
        });

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#pusdiklat_id').select2();
        $('#institution_id').select2();
        $('#position_3').select2();
        $('#position_1').select2();

        $('#btnRemove').click(function () {
            var conf = confirm('Are you sure want to delete item?');
            if (!conf) return;


            $(this).attr('disabled', true);
            clearAlert();

            var url = "<?php echo base_url('master/certificate/template_delete');?>
";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#mainForm').serialize(),
                success: function (resp) {
                    $('#btnRemove').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#role_id').val(resp.role_id);

                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);

                        location.href = '<?php echo base_url("master/certificate/template_list");?>
'
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btnRemove').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(jqXHR.responseText);
                }
            });
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "template_save";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#mainForm').serialize(),
                success: function (resp) {
                    $('#btnSubmitForm').removeAttr('disabled');

                    if (resp.rcode === 1) {
                        $('#template_id').val(resp.template_id);
                        console.log("template_id" + resp.template_id);
                        if (!$('#mainForm input[name=file_name]')[0].files[0]) {
                            // location.reload();
                            $('#response_success').html(resp.message);
                            $('#response_success').removeClass('hidden');
                        } else {
                            uploadThumbnail(resp.template_id);
                        }
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btnSubmitForm').removeAttr('disabled');

                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function uploadThumbnail(template_id) {
            $('#progressModal').removeClass('hidden');

            var url = "<?php echo base_url('/certificate/certificate/template_upload');?>
";

            var xhr = new XMLHttpRequest();
            (xhr.upload || xhr).addEventListener('progress', function (e) {
                var done = e.loaded;
                var total = e.total;
                var percent = Math.round(done / total * 100) + '%';
                $('#progressBar').width(percent);
                $('#progressBar').text(percent);
            });

            xhr.addEventListener('load', function (e) {
                $('#btnSubmitForm').removeAttr('disabled');
                console.log(this.responseText);
                try {
                    var resp = JSON.parse(this.responseText);
                    if (resp.rcode === 1) {
                        // location.reload();

                        $('#response_success').html(resp.message);
                        $('#response_success').removeClass('hidden');
                        
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');

                        hideProgressBar();
                    } 
                } catch (e) {
                    $('#response_error').html(this.responseText);
                    $('#response_error').removeClass('hidden');

                    hideProgressBar();
                }

            });
            xhr.open('post', url, true);

            var data;
            data = new FormData();
            data.append('file', $('#mainForm input[name=file_name]')[0].files[0]);
            data.append('template_id', template_id);

            xhr.send(data);
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');

            $('#response_success').html('');
            $('#response_success').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

        function setupViewValues() {
            <?php if ($_smarty_tpl->tpl_vars['template']->value) {?>
            $('#template_id').val('<?php echo $_smarty_tpl->tpl_vars['template']->value->template_id;?>
');
            $('#template_title').val('<?php echo $_smarty_tpl->tpl_vars['template']->value->template_title;?>
');
            $('#pusdiklat_id').val('<?php echo $_smarty_tpl->tpl_vars['template']->value->pusdiklat_id;?>
');
            $('#institution_id').val('<?php echo $_smarty_tpl->tpl_vars['template']->value->institution_id;?>
');
            <?php if ($_smarty_tpl->tpl_vars['template']->value->is_default) {?>
            $('#is_default').iCheck('check');
            <?php }?>
            <?php }?>

            <?php
$_from = $_smarty_tpl->tpl_vars['authorize_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
            $('#position_<?php echo $_smarty_tpl->tpl_vars['item']->value->position;?>
').val('<?php echo $_smarty_tpl->tpl_vars['item']->value->author_id;?>
');
            <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
        }
    });
<?php echo '</script'; ?>
>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Certificate Template Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url('master/certificate/template_list');?>
"><i class="fa fa-dashboard"></i> Certificate Template</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Certificate Template Form
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="mainForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="template_id" name="template_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="template_title" class="col-sm-4 control-label">Title</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="template_title" name="template_title" placeholder="template_title"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="pusdiklat_id" class="col-sm-4 control-label">Pusdiklat</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="pusdiklat_id" name="pusdiklat_id">
                                                    <option value="">-- Choose Pusdiklat --</option>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['pusdiklat_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_id;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_name;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="institution_id" class="col-sm-4 control-label">Institution</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="institution_id" name="institution_id">
                                                    <option value="">-- Choose Institution --</option>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['institution_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->institution_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->institution_alias;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value->institution_name;?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="file_name" class="col-sm-4 control-label">Background</label>
                                            <div class="col-sm-8">
                                                <input type="file" class="form-control" id="file_name" name="file_name"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="template_title" class="col-sm-4 control-label">&nbsp;</label>
                                            <div class="col-sm-8">
                                                <div class="checkbox icheck">
                                                    <input type="checkbox" class="single_check" id="is_default" name="is_default" value="1"> Set As Default Template</input>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.box-body -->
                                </div>
                                <div class="col-md-6">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="position_3" class="col-sm-4 control-label">Right Author</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="position_3" name="position_3">
                                                    <option value="">-- Choose Authority --</option>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['author_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->author_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->author_name;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value->author_title;?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="position_1" class="col-sm-4 control-label">Left Author</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="position_1" name="position_1">
                                                    <option value="">-- Choose Authority --</option>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['author_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                                    <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value->author_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value->author_name;?>
 - <?php echo $_smarty_tpl->tpl_vars['item']->value->author_title;?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <!-- <div class="box-body">
                                        <textarea id="content_text" name="content_text" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                            <?php if ($_smarty_tpl->tpl_vars['template']->value) {?>
                                            <?php echo $_smarty_tpl->tpl_vars['template']->value->content_text;?>

                                            <?php } else { ?>
                                            <?php echo $_smarty_tpl->tpl_vars['default_content_text']->value;?>

                                            <?php }?>
                                        </textarea>
                                    </div> -->
                                    <div class="callout callout-danger hidden" id="response_error"></div>
                                    <div class="callout callout-success hidden" id="response_success"></div>
                                    <div class="box-footer">
                                        <a href="<?php echo base_url('master/certificate/template_list');?>
"><button type="button" class="btn btn-default">Go Back</button></a>
                                        <div class="pull-right">
                                            <button type="button" id="btnRemove" class="btn btn-danger">Remove</button>
                                            <button type="button" id="btnSubmitForm" class="btn btn-info">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper<?php }
}
?>