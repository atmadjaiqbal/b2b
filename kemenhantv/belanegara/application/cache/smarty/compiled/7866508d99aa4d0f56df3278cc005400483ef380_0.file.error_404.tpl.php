<?php /* Smarty version 3.1.27, created on 2015-11-30 15:53:42
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/views/errors/html/error_404.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:506082197565c0e96225bd0_45733750%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7866508d99aa4d0f56df3278cc005400483ef380' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/views/errors/html/error_404.tpl',
      1 => 1447397774,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '506082197565c0e96225bd0_45733750',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565c0e9627fb53_75704010',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565c0e9627fb53_75704010')) {
function content_565c0e9627fb53_75704010 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '506082197565c0e96225bd0_45733750';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            404 Error Page
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">404 error</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                <p>
                    We could not find the page you were looking for.
                    Meanwhile, you may <a href="<?php echo base_url();?>
">return to dashboard</a> or try using the search form.
                </p>
            </div><!-- /.error-content -->
        </div><!-- /.error-page -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper --><?php }
}
?>