<?php /* Smarty version 3.1.27, created on 2015-11-30 15:53:41
         compiled from "/Users/ichalzbreak/Sites/belanegara/application/views/certificate_preview.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1536173172565c0e958ea7a0_69333157%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad6648827cb07b467e2ef7935aa4c6af62eb309d' => 
    array (
      0 => '/Users/ichalzbreak/Sites/belanegara/application/views/certificate_preview.tpl',
      1 => 1448855482,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1536173172565c0e958ea7a0_69333157',
  'variables' => 
  array (
    'template_background' => 0,
    'certificate' => 0,
    'customer' => 0,
    'author' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565c0e9596c066_54312909',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565c0e9596c066_54312909')) {
function content_565c0e9596c066_54312909 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1536173172565c0e958ea7a0_69333157';
?>
<!doctype html>
<html>
	<head>
		<style type="text/css" media="print">
			@page 
			{
				size: auto;   /* auto is the current printer page size */
				margin: 0mm;  /* this affects the margin in the printer settings */
			}

			.page
			{
				-webkit-transform: rotate(90deg); 
				-moz-transform:rotate(90deg);
				filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
			}

			html { 
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
			}
			br {
				display: block;
				margin: 10px 0;
				line-height:10px;
				content: " ";
			}
		</style>

		<?php echo '<script'; ?>
 type="text/javascript">

			function onReady() {
			// window.print();
			}

			document.addEventListener('DOMContentLoaded', onReady, false);

		<?php echo '</script'; ?>
>
	</head>
	<body class="page" style="margin-top:30px;">
		<div style="width:1024px; height:640px; text-align:center; background: url(<?php echo $_smarty_tpl->tpl_vars['template_background']->value;?>
&w=1000) no-repeat center; padding-top:0px;">
			<div style="text-align:center;">
				<div style="padding:70px 50px 50px 100px;">
					<span style="font-size:12px"><b>KEMENTRIAN PERTAHANAN RI</b></span>
					<br/>
					<span style="font-size:12px"><b>BADAN PENDIDIKAN DAN PELATIHAN</b></span>
					<br/>
					<br/>
					<span style="font-size:15px">NOMOR : <?php echo $_smarty_tpl->tpl_vars['certificate']->value->certificate_no;?>
</span>
					<br/>
					<br/>
					<span style="font-size:22px;">PIAGAM PENGHARGAAN</span>
					<br>
					<br>
					<span style="font-size:17px">Diberikan kepada : </span>
					<br>
					<br>
					<div style="text-align:left; margin-left:50px; margin-right:50px; font-size:15px;">
						<table style="width:100%;">
							<tr>
								<td style="width:40%">
									<span>NAMA</span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['customer']->value->full_name, 'UTF-8');?>
</span>
								</td>
							</tr>
							<tr>
								<td style="width:40%;">
									<span>TEMPAT & TANGGAL LAHIR </span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['customer']->value->birth_place, 'UTF-8');?>
, <?php echo $_smarty_tpl->tpl_vars['customer']->value->birth_date;?>
</span>
								</td>
							</tr>
							<tr>
								<td style="width:40%;">
									<span>JABATAN</span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['customer']->value->jabatan, 'UTF-8');?>
</span>
								</td>
							</tr>
						</table>
					</div>
					<br/>
					<span style="font-size:15px">PANITIA FORUM DIALOG WAWASAN KEBANGSAAN DAN BELA NEGARA</span>
					<br/>
					<br/>
					<div style="padding-left:30px; text-align:left;">
						<span style="font-size:15px">Yang diselenggarakan di <?php echo $_smarty_tpl->tpl_vars['customer']->value->pusdiklat_name;?>
 Kota <?php echo $_smarty_tpl->tpl_vars['customer']->value->city_name;?>
, <?php echo $_smarty_tpl->tpl_vars['customer']->value->province_name;?>
, pada tanggal <?php echo $_smarty_tpl->tpl_vars['certificate']->value->last_modified_date;?>
</span>
					</div>
					<br/>
					<br/>
					<div style="font-size:15px;">
						<div align="right" style="padding-right:100px;">
							<span>
									<?php echo $_smarty_tpl->tpl_vars['customer']->value->province_name;?>
, <?php echo $_smarty_tpl->tpl_vars['certificate']->value->last_modified_date;?>

							</span>
						</div>
					</div>
					<br>
					<div align="center" style="text-align:center; margin-left:50px; margin-right:50px; font-size:15px;">
						<table style="width:100%;">
							<tr>
								<td style="width:33%">
									
								</td>
								<td style="width:33%">
									
								</td>
								<td style="width:33%">
									<div>
										<span>
												<?php echo $_smarty_tpl->tpl_vars['author']->value->author_title;?>

										</span>
									</div>
									<div>
										<img src="<?php echo $_smarty_tpl->tpl_vars['author']->value->thumbnail;?>
&w=150" width="150px">
										</img>
									</div>
									<br>
									<div>
										<span>
												<?php echo $_smarty_tpl->tpl_vars['author']->value->author_name;?>

										</span>
									</div>
									<div>
										<span style="text-align:center;">
												<?php echo $_smarty_tpl->tpl_vars['author']->value->author_position;?>

										</span>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html><?php }
}
?>