<?php /* Smarty version 3.1.27, created on 2015-12-02 15:52:24
         compiled from "/var/www/html/kemenhantv/belanegara/application/modules/administration/views/customer_list.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:339671255565eb1482d40d8_19785088%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b054813a41cb769d7ed0f6695f2134452e00414a' => 
    array (
      0 => '/var/www/html/kemenhantv/belanegara/application/modules/administration/views/customer_list.tpl',
      1 => 1449045491,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '339671255565eb1482d40d8_19785088',
  'variables' => 
  array (
    'customer_list' => 0,
    'item' => 0,
    'certificate_type_list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eb1483009a3_72819086',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eb1483009a3_72819086')) {
function content_565eb1483009a3_72819086 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '339671255565eb1482d40d8_19785088';
?>
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="<?php echo assets_path('js/iCheck/square/blue.css');?>
" media="screen"/>

<?php echo js("iCheck/iCheck.min.js");?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>
"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Customer List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Customer</h3>
                        <div class="box-tools">

                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Add Customer
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="checkbox icheck">
                                            <label><input type="checkbox" class="check_all" id="check_all" indeterminta="true">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th>Name</th>
                                    <th>Pusdiklat</th>
                                    <th>Province</th>
                                    <th>Register Date</th>
                                    <th>Certificates</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['customer_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                    <tr role="row">
                                        <td>
                                            <div class="checkbox icheck">
                                                <input type="checkbox" class="customer_check" id="customer_check_<?php echo $_smarty_tpl->tpl_vars['item']->value->customer_id;?>
" name="customer_id[]" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->customer_id;?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value->checked) {?>checked<?php }?>>
                                            </div>
                                        </td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->full_name;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->pusdiklat_name;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->province_name;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value->register_date;?>
</td>
                                        <td>
                                            <?php if ($_smarty_tpl->tpl_vars['item']->value->certificate_no) {?>
                                            <a href="<?php echo base_url('administration/certificate/detail?id=');?>
$item->certificate_id"><?php echo $_smarty_tpl->tpl_vars['item']->value->certificate_no;?>
</a>
                                            <?php }?>
                                        </td>
                                        <td>
                                            <div class="btn-group-vertical">
                                                <button class="btn btn-default" name="btnEdit" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value->customer_id;?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_name;?>
" data-group="<?php echo $_smarty_tpl->tpl_vars['item']->value->privilege_group;?>
">
                                                    <i class="fa fa-edit"> Edit</i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                            </tbody>
                        </table>
                        <div class="box-footer">
                            <button class="btn btn-sm btn-primary" id="btnRequest">
                                <span class="fa fa-edit"></span> Request Certificate
                            </button>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Request Certificates</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="modal_label" id="modal_label" class="control-label">Are you sure want to request certificate for selected items?</label>
                        </div>
                        <div class="box-footer"></div>
                        <div class="form-group">
                            <label for="certificate_type_id" class="col-sm-4 control-label">Choose Certificate Type</label>
                            <div class="col-sm-8">
                                <?php
$_from = $_smarty_tpl->tpl_vars['certificate_type_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                                <div class="checkbox icheck">
                                    <label><input type="checkbox" class="certificate_type_id" id="certificate_type_id_check_<?php echo $_smarty_tpl->tpl_vars['item']->value->certificate_type_id;?>
" name="certificate_type_id[]" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->certificate_type_id;?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value->checked) {?>checked<?php }?>>
                                        <?php echo $_smarty_tpl->tpl_vars['item']->value->certificate_name;?>
</label>
                                </div>
                                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                            </div>
                        </div>
                    </div>
                    <div class="callout callout-danger hidden" id="response_error"></div>
                        <div class="callout callout-success hidden" id="response_succeed"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Confirm</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div><!-- /.content-wrapper -->

<?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

        $('input.check_all').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('input.customer_check').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('input.certificate_type_id').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#btnCreate').click(function () {
            location.href = "<?php echo base_url('administration/customer/registration_form');?>
";
        });

        $('#btnRequest').click(function() {
            clearModalForm();

            var data = serializeCheckbox();

            if (data) {
                $('#modal_label').text("Are you sure want to request certificate for selected items?");
            } else {
                $('#modal_label').text("Please select customer!");
            }
            
            
            $('#layoutModal').modal('show');

        });

        $('input.check_all').on('ifChecked ifUnchecked', function(event) {    
            if (event.type == 'ifChecked') {
                $('input.customer_check').iCheck('check');
            } else {
                $('input.customer_check').iCheck('uncheck');
            }
        });

        $('button[name="btnEdit"]').on('click', function () {
            var data_id = $(this).attr('data-id');
            location.href = "<?php echo base_url('administration/customer/registration_form');?>
?customer_id=" + data_id;
            return;

            var data_name = $(this).attr('data-name');
            var data_group = $(this).attr('data-group');

            if (data_id === '')
                return;

            clearModalForm();

            $('#layoutModal').modal('show');

            $('#privilege_id').val(data_id);
            $('#new_privilege_id').val(data_id);
            $('#privilege_name').val(data_name);
            $('#privilege_group').val(data_group);
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var data = serializeCheckbox();
            data += serializeCertificateType();

            var url = "customer/request_certificate";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: data,
                success: function (resp) {
                    console.log(resp);
                    if (resp.rcode === 1) {
                        $('#privilege_id').val(resp.privilege_id);

                        $('#response_succeed').html(resp.message);
                        $('#response_succeed').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');

                        // location.reload();
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function serializeCheckbox() {
            var result = "";
            $('input[name="customer_id[]"]:checked').each(function(k, v) {
                result += "&customer_id[]=" + $(this).val();
            });

            return result;
        }

        function serializeCertificateType() {
            var result = "";
            $('input[name="certificate_type_id[]"]:checked').each(function(k, v) {
                result += "&certificate_type_id[]=" + $(this).val();
            });

            return result;
        }

        function clearModalForm() {
            $('#btnSubmitForm').removeAttr("disabled");

            $('input.certificate_type_id').iCheck('uncheck');

            hideErrorMessage();
            hideProgressBar();
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

    });
<?php echo '</script'; ?>
><?php }
}
?>