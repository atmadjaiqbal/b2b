<?php /* Smarty version 3.1.27, created on 2015-12-02 15:39:34
         compiled from "themes/default/views/layouts/partials/headnavigation.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:2140917385565eae46477f94_64765016%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '229be3c0e62997f0f575c88da8cb7ae30e5c9092' => 
    array (
      0 => 'themes/default/views/layouts/partials/headnavigation.tpl',
      1 => 1449045288,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2140917385565eae46477f94_64765016',
  'variables' => 
  array (
    'role' => 0,
    'group' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_565eae46485c51_16022614',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_565eae46485c51_16022614')) {
function content_565eae46485c51_16022614 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '2140917385565eae46477f94_64765016';
?>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--
                        <img src="<?php echo theme_url('assets/img/user2-160x160.jpg');?>
" class="user-image" alt="User Image">
                        -->
                        <span class="hidden-xs"><?php echo $_SESSION['user_account']->user_name;?>
</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <!--
                            <img src="<?php echo theme_url('assets/img/user2-160x160.jpg');?>
" class="img-circle" alt="User Image">
                            -->
                            <p>
                                <?php echo $_SESSION['user_account']->user_name;?>
 - 
                                <?php
$_from = $_SESSION['user_account']->role_list;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['role'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['role']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['role']->value) {
$_smarty_tpl->tpl_vars['role']->_loop = true;
$foreach_role_Sav = $_smarty_tpl->tpl_vars['role'];
?>
                                    <?php echo $_smarty_tpl->tpl_vars['role']->value->role_name;?>
, 
                                <?php
$_smarty_tpl->tpl_vars['role'] = $foreach_role_Sav;
}
?>
                                <br><br>
                                Group : 
                                <?php
$_from = $_SESSION['user_account']->group_list;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['group'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['group']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->_loop = true;
$foreach_group_Sav = $_smarty_tpl->tpl_vars['group'];
?>
                                    <?php echo $_smarty_tpl->tpl_vars['group']->value->group_name;?>
,
                                <?php
$_smarty_tpl->tpl_vars['group'] = $foreach_group_Sav;
}
?>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url('login/doLogout');?>
" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav><?php }
}
?>