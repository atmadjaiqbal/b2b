<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter-Smarty Template Class
 *
 * Build your CodeIgniter - Smarty pages much easier with breadcrumbs, layouts and themes
 *
 * @package			CodeIgniter
 * @subpackage                  Libraries
 * @category                    Libraries
 * @author			Amrizal Sudartama
 * 
 */
class Layout {

    public $frame = true;
    public $layout_index = 'layout_index.tpl';
    private $_ci;
    private $_data = array();
    private $_content_parser = null;

    /**
     * Constructor - Sets Preferences
     *
     * The constructor can be passed an array of config values
     */
    public function __construct($config = array()) {
        $this->_ci = & get_instance();
        $this->_ci->load->library('parser');

        if (!empty($config)) {
            foreach ($config as $key => $val) {
                $this->{'_' . $key} = $val;
            }
        }


        log_message('debug', 'Layout class Initialized');
    }

    // --------------------------------------------------------------------

    public function set($key, $value) {
        $this->_data[$key] = $value;

        if ($key === 'layout_index') {
            $this->layout_index = $value;
        }
        return $this;
    }

    public function set_theme($name) {
        $this->_ci->parser->set_theme($name);

        return $this;
    }

    /**
     * Build the entire HTML output combining partials, layouts and views.
     *
     * @param	string	$view
     * @param	array	$data
     * @param	bool	$return
     * @param	bool	$IE_cache
     * @param 	bool 	$pre_parsed_view
     * @return	string
     */
    public function build($template_view, $data = array(), $return = FALSE, $IE_cache = TRUE) {

        // Set whatever values are given. These will be available to all view files
        is_array($data) OR $data = (array) $data;

        // Merge in what we already have with the specific data
        $this->_data = array_merge($this->_data, $data);

        // We don't need you any more buddy
//        unset($data);
        // Disable sodding IE7's constant cacheing!!		
        // This is in a conditional because otherwise it errors when output is returned instead of output to browser.
        if ($IE_cache) {
            $this->_ci->output->set_header('Expires: Sat, 01 Jan 2000 00:00:01 GMT');
            $this->_ci->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->_ci->output->set_header('Cache-Control: post-check=0, pre-check=0, max-age=0');
            $this->_ci->output->set_header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            $this->_ci->output->set_header('Pragma: no-cache');
        }

        // If no file extension dot has been found default to defined extension for view extensions
        if (!stripos($template_view, '.')) {
            $template_view = $template_view . "." . $this->_ci->smarty->template_ext;
        }

        if ($this->frame) {
            $this->_data['template_view'] = $template_view;

            $this->_content_parser = $this->_ci->parser->parse($this->layout_index, $this->_data, $return);
        } else {
            $this->_content_parser = $this->_ci->parser->parse($template_view, $this->_data, $return);
        }
        return $this->_content_parser;
    }

    /**
     * Build the entire JSON output, setting the headers for response.
     *
     * @param	array	$data
     * @return	void
     */
    public function build_json($data = array()) {
        $this->_ci->output->set_header('Content-Type: application/json; charset=utf-8');

        $this->_ci->output->set_output(json_encode((object) $data));
    }

}

// END Layout class