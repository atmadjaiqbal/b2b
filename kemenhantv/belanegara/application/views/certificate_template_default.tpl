<!doctype html>
<html>
  <head>
    <style type="text/css" media="print">
      @page 
      {
        size: auto;   /* auto is the current printer page size */
        margin: 0mm;  /* this affects the margin in the printer settings */
      }

      .page
      {
        -webkit-transform: rotate(90deg); 
        -moz-transform:rotate(90deg);
        filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
      }

      html { 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      }
      br {
        display: block;
        margin: 10px 0;
        line-height:10px;
        content: " ";
      }
    </style>

    <script type="text/javascript">

      function onReady() {
      // window.print();
      }

      document.addEventListener('DOMContentLoaded', onReady, false);

    </script>
  </head>
  <body class="page" style="margin-top:30px;">
    <div style="width:1024px; height:640px; text-align:center; background: url({$template_background}&w=1000) no-repeat center; padding-top:50px;">
      <div style="text-align:center;">
        <div style="padding:100px;">
          <span style="font-size:15px"><b>KEMENTRIAN PERTAHANAN RI</b></span>
          <br/>
          <span style="font-size:15px"><b>BADAN PENDIDIKAN DAN PELATIHAN</b></span>
          <br/>
          <br/>
          <span style="font-size:18px">NOMOR : {$certificate_no}</span>
          <br/>
          <br/>
          <span style="font-size:28px;">PIAGAM PENGHARGAAN</span>
          <br>
          <br>
          <span style="font-size:20px">Diberikan kepada : </span>
          <br>
          <br>
          <div style="text-align:left; margin-left:50px; margin-right:50px; font-size:18px;">
            <table style="width:100%;">
              <tr>
                <td style="width:40%">
                  <span>NAMA</span>
                </td>
                <td style="width:2%">
                  :
                </td>
                <td width="50%">
                  <span>{$customer->full_name}</span>
                </td>
              </tr>
              <tr>
                <td style="width:40%;">
                  <span>TEMPAT & TANGGAL LAHIR </span>
                </td>
                <td style="width:2%">
                  :
                </td>
                <td width="50%">
                  <span>{$customer->birth_place}, {$customer->birth_date}</span>
                </td>
              </tr>
              <tr>
                <td style="width:40%;">
                  <span>JABATAN</span>
                </td>
                <td style="width:2%">
                  :
                </td>
                <td width="50%">
                  <span>{$certificate->rank}</span>
                </td>
              </tr>
            </table>
          </div>
          <br/>
          <span style="font-size:18px">PANITIA FORUM DIALOG WAWASAN KEBANGSAAN DAN BELA NEGARA</span>
          <br/>
          <br/>
          <div style="padding-left:30px; text-align:left;">
            <span style="font-size:17px">Yang diselenggarakan di {$certificate->address} Kota {$certificate->city_name}, {$certificate->province_name}, pada tanggal {$certificate->event_date}
            </span>
          </div>
          <br/>
          <br/>
          <div align="right">
            <span style="font-size:17px">
                {$certificate->province_name}, {$certificate->signed_date}
            </span>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>