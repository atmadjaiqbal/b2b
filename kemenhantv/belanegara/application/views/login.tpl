<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bela Negara | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- FAVICON -->
        <link rel="shortcut icon" href="{theme_url("assets/img/favicon.ico")}" />
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" type="text/css" href="{theme_url('assets/js/bootstrap/css/bootstrap.min.css')}" media="screen"/>
        <!-- Theme style -->
        {css("AdminLTE.min.css")}
        <!-- Custom -->
        {css("login.css")}

        <!-- jQuery 2.1.4 -->
        {js("jQuery/jQuery-2.1.4.min.js")}
        <!-- Bootstrap 3.3.5 -->
        {js("bootstrap/js/bootstrap.js")}

        <script type="text/javascript">
            $(document).ready(function () {

            {if $error_login }
                $('#login-alert').removeClass('hidden');
            {/if}
            });
        </script>

    </head>
    <body class="hold-transition login-page">
        <div class="login-box">

            <div class="login-box-body">

                <div class="login-logo">
                    <a href="{base_url()}"><b>KEMHAN TV</b></a>
                </div><!-- /.login-logo -->
                <p class="login-box-msg">BELA NEGARA ADMIN</p>

                <form action="{base_url('login/doLogin')}" method="POST">
                    <input type="hidden" name="ret" value="{$smarty.get.ret}"/>
                    <div class="form-group has-feedback">
                        <input type="text" name="username" class="form-control" placeholder="User ID" value="{$username}">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <!--
                        <input type="lang" class="form-control" placeholder="Language">
                        -->
                        <select id="role_id" name="role_id" class="form-control">
                            <option value="">Super User</option>
                            <option value="">Pusdiklat</option>
                            <option value="">Institution</option>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <!--
                        <input type="lang" class="form-control" placeholder="Language">
                        -->
                        <select id="lang" name="lang" class="form-control">
                            <option value="en_US">English</option>
                            <option value="zh_CN">Bahasa Indonesia</option>
                        </select>

                    </div>
                    <div class="callout callout-danger hidden" id="login-alert">
                        {$error_login}
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            
                        </div><!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-warning btn-block btn-flat">Sign In</button>
                        </div><!-- /.col -->
                    </div>
                </form>

                <!--
            <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
            </div>
                -->
                <!-- /.social-auth-links -->
                <!--
            <a href="#">I forgot my password</a><br>
            <a href="register.html" class="text-center">Register a new membership</a>
                -->

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->
    </body>
</html>

