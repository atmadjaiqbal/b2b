<!doctype html>
<html>
	<head>
		<style type="text/css" media="print">
			@page 
			{
				size: auto;   /* auto is the current printer page size */
				margin: 0mm;  /* this affects the margin in the printer settings */
			}

			.page
			{
				-webkit-transform: rotate(90deg); 
				-moz-transform:rotate(90deg);
				filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
			}

			html { 
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
			}
			br {
				display: block;
				margin: 10px 0;
				line-height:10px;
				content: " ";
			}
		</style>

		<script type="text/javascript">

			function onReady() {
			// window.print();
				if ({$is_print}) {
					window.print();
				}
			}

			document.addEventListener('DOMContentLoaded', onReady, false);

		</script>
	</head>
	<body class="page" style="margin-top:30px;">
		<div style="width:1024px; height:640px; text-align:center; background: url({$template->background_image}&w=1000) no-repeat center; padding-top:0px;">
			<div style="text-align:center;">
				<div style="padding:70px 50px 50px 100px;">
					<span style="font-size:12px"><b>KEMENTRIAN PERTAHANAN RI</b></span>
					<br/>
					<span style="font-size:12px"><b>BADAN PENDIDIKAN DAN PELATIHAN</b></span>
					<br/>
					<br/>
					<span style="font-size:15px">NOMOR : {$certificate->certificate_no}</span>
					<br/>
					<br/>
					<span style="font-size:22px;">PIAGAM PENGHARGAAN</span>
					<br>
					<br>
					<span style="font-size:17px">Diberikan kepada : </span>
					<br>
					<br>
					<div style="text-align:left; margin-left:50px; margin-right:50px; font-size:15px;">
						<table style="width:100%;">
							<tr>
								<td style="width:40%">
									<span>NAMA</span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span>{$customer->full_name|upper}</span>
								</td>
							</tr>
							<tr>
								<td style="width:40%;">
									<span>TEMPAT & TANGGAL LAHIR </span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span>{$customer->birth_place|upper}, {$customer->birth_date}</span>
								</td>
							</tr>
							<tr>
								<td style="width:40%;">
									<span>JABATAN</span>
								</td>
								<td style="width:2%">
									:
								</td>
								<td width="50%">
									<span>{$customer->jabatan|upper}</span>
								</td>
							</tr>
						</table>
					</div>
					<br/>
					<span style="font-size:15px">PANITIA FORUM DIALOG WAWASAN KEBANGSAAN DAN BELA NEGARA</span>
					<br/>
					<br/>
					<div style="padding-left:30px; text-align:left;">
						<span style="font-size:15px">Yang diselenggarakan di {$customer->pusdiklat_name} Kota {$customer->city_name}, {$customer->province_name}, pada tanggal {$certificate->last_modified_date}</span>
					</div>
					<br/>
					<br/>
					<div style="font-size:15px;">
						<div align="right" style="padding-right:100px;">
							<span>
									{$customer->province_name}, {$certificate->last_modified_date}
							</span>
						</div>
					</div>
					<br>
					{if $authors}
					<div align="center" style="text-align:center; padding-left:50px; padding-right:50px; font-size:15px;">
						<table style="">
							<tr>
								{for $index = 1 to 3}
								<td style="width:33%">
									{if $authors[$index]}
									{assign $author $authors[index]}
									<div>
										<span>
												{$authors[$index]->author_title}
										</span>
									</div>
									<div style="height:75px;">
										{if $authors[$index]->thumbnail}
										<img src="{$authors[$index]->thumbnail}&w=150" width="150px" height="75px">
										</img>
										{else}
										&nbsp;
										{/if}
									</div>
									<br>
									<div>
										<span>
												{$authors[$index]->author_name}
										</span>
									</div>
									<div>
										<span style="text-align:center;">
												{$authors[$index]->author_position}
										</span>
									</div>
									{/if}
								</td>
								{/for}
							</tr>
						</table>
					</div>
					{/if}
				</div>
			</div>
		</div>
	</body>
</html>