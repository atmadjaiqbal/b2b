<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Certificate extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('certificate_model'));
    }

    public function index() {
        $this->setActiveNavigation("administration", "Certificate Request", "certificate_list");

        $certificate_list = $this->certificate_model->getCertificateListByStatus(2);    //requested

        $this->data['certificate_list'] = $certificate_list;

        $this->layout->build('certificate_list.tpl', $this->data);
    }


    
    /**
    * ======= CERTIFICATE REQUEST ========
    */
    public function request_list() {

        $this->setActiveNavigation("administration", "Certificate Request", "certificate_request_list");

        $certificate_list = $this->certificate_model->getCertificateListByStatus(1);    //requested

        $this->data['certificate_list'] = $certificate_list;

        $this->layout->build('certificate_request_list.tpl', $this->data);
    }

    public function detail() {
        $certificate_id = $this->input->get("certificate_id");

        $certificate = NULL;

        if (empty($certificate_id)) {

        } else {
            $certificate = $this->certificate_model->detail($certificate_id);
        }

        $this->setActiveNavigation("administration", "Certificate Request", "certificate_request_list");

        $this->data['certificate'] = $certificate;

        $this->layout->build('certificate_detail.tpl', $this->data);

    }

    public function validate_certificate() {
        $this->load->model('certificate_counter_model');
        $this->load->helper('new_number');

        $certificate_ids = $this->input->post("certificate_id");
        $certificates = array();
        $response = array();
        $sqlRes = FALSE;

        $userAccount = NULL;

        $session = $this->session->get_userdata('user_account');
        if ($session) {
            $userAccount = $session['user_account'];
        }

        if (!$userAccount) {
            die('You have no privilege to access this page');
        }

        if (empty($certificate_ids)) {
            $response = array(
                    "rcode" => 0,
                    "message" => "Incomplete parameters : certificate_id"
                    );
        } else {
            foreach ($certificate_ids as $certificate_id) {
                $certificate = $this->certificate_model->getCertificateByID($certificate_id);
                if ($certificate) {
                    $certificate_no = $this->certificate_counter_model->lastCertificateNumber();
                    //roman month
                    $certificate_no .= " / " . romanic_number(date('n'));
                    //static for this moment
                    $certificate_no .= " / BDK-KMHN";
                    //year
                    $certificate_no .= " / " . date('Y');

                    $certificate->certificate_no = $certificate_no;
                    $certificate->new_status_id = 2;
                    $certificate->last_modified_date = date("YYYY-m-d");

                    $certificates[] = $certificate;
                }
            }
            $sqlRes = $this->certificate_model->updateCertificatesStatus($certificates, $userAccount->account_id, $remark = "");

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

}
