<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author      Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright       @ibolz 2015
 * @version         1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Customer extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('customer_model', 'pusdiklat_model', 'master_model', 'certificate_model'));

    }

    /**
     * Role List
     */
    public function index() {
        
        $this->setActiveNavigation("administration", "Customer", "customer_list");

        $customer_list = $this->customer_model->getCustomerList();
        $certificate_type_list = $this->certificate_model->getCertificateTypeList();

        $this->data['customer_list'] = $customer_list;
        $this->data['certificate_type_list'] = $certificate_type_list;

        $this->layout->build('customer_list.tpl', $this->data);
    }

    public function registration_form() {
        $this->load->model(array('institution_model'));

        $this->setActiveNavigation("administration", "Registration", "customer_register");
        
        $customer_id = $this->input->get("customer_id");

        $pusdiklat_list = $this->pusdiklat_model->getList();
        $id_type_list = $this->master_model->getCustomerIDTypeList();
        $religion_list = $this->master_model->getReligionList();
        $city_list = $this->master_model->getCityList();
        $institution_list = $this->institution_model->getInstitutionList();


        if (!empty($customer_id)) {
            $customer = $this->customer_model->getCustomerDetail($customer_id);
            $this->data['customer'] = $customer;

            $this->setActiveNavigation("administration", "Registration", "customer_list");
        }

        
        $this->data['pusdiklat_list'] = $pusdiklat_list;
        $this->data['id_type_list'] = $id_type_list;
        $this->data['religion_list'] = $religion_list;
        $this->data['city_list'] = $city_list;
        $this->data['institution_list'] = $institution_list;

        $this->layout->build('registration_form.tpl', $this->data);
    }
    
    public function customer_save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'pusdiklat_id',
                        'label' => 'Pusdiklat',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'id_no',
                        'label' => 'No Identitas',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'id_type',
                        'label' => 'Tipe Identitas',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'full_name',
                        'label' => 'Nama Lengkap',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'religion_id',
                        'label' => 'Agama',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $customer_id = $this->input->post("customer_id");
            $pusdiklat_id = $this->input->post("pusdiklat_id");
            $jabatan = $this->input->post("jabatan");
            $id_no = $this->input->post("id_no");
            $id_type = $this->input->post("id_type");
            $full_name = $this->input->post("full_name");
            $address = $this->input->post("address");
            $phone_no = $this->input->post("phone_no");
            $occupation = $this->input->post("occupation");
            $birth_date = $this->input->post("birth_date");
            $birth_place = $this->input->post("birth_place");
            $religion_id = $this->input->post("religion_id");
            $institution_id = $this->input->post("institution_id");
            $status_id = 1;
            
            $response = array();
            $sqlRes = false;

            if (empty($birth_date)) {
                $birth_date = NULL;
            }

            if (empty($pusdiklat_id)) {
                $pusdiklat_id = NULL;
            }

            if (empty($institution_id)) {
                $institution_id = NULL;
            }

            
            if (empty($customer_id)) {
                //do insert
                $customer_id = uniqid(rand(), false);
                $sqlRes = $this->customer_model->insertCustomer($customer_id, $full_name, $phone_no, $address, $id_no, $birth_date, $occupation, $id_type, $religion_id, $status_id, $pusdiklat_id, $birth_place, $institution_id, $jabatan);
            } else {
                //do update
                $sqlRes = $this->customer_model->updateCustomer($customer_id, $full_name, $phone_no, $address, $id_no, $birth_date, $occupation, $id_type, $religion_id, $status_id, $pusdiklat_id, $birth_place, $institution_id, $jabatan);
            }

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "customer_id" => $customer_id
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function request_certificate() {
        $customer_ids = $this->input->post("customer_id");
        $certificate_type_ids = $this->input->post("certificate_type_id");
        $new_certifcates_to_insert = array();
        $new_certifcates_to_update = array();

        $response = array();
        $sqlRes = FALSE;

        if (empty($customer_ids) || empty($certificate_type_ids)) {
            $response = array(
                    "rcode" => 0,
                    "message" => "Incomplete parameters : customer_id, certificate_type_id"
                    );
        } else {
            foreach ($customer_ids as $customer_id) {
                $customer = $this->customer_model->getCustomerDetail($customer_id);
                $best_certificate_template = $this->certificate_model->getDefaultTemplate($customer->pusdiklat_id, $customer->institution_id);
                // print_r($customer);
                // return;

                $existing_certificates = $this->certificate_model->getCertificateByCustomer($customer_id);
                $certificates_new = array();
                $certificates_update = array();
                foreach ($certificate_type_ids as $certificate_type_id) {
                    $certificate = array(
                        "certificate_type_id" => $certificate_type_id,
                        "template_id" => (!empty($best_certificate_template)?$best_certificate_template->template_id:NULL)
                    );
                    $certificates_new[] = $certificate;
                    foreach ($existing_certificates as $existing_certificate) {
                        if ($existing_certificate->certificate_type_id == $certificate_type_id) {
                            array_pop($certificates_new);
                        } else if ($existing_certificate->status_id == 3) {     //rejected certificate
                            $certificates_update[] = $certificate;
                        }
                    }
                }

                if (!empty($certificates_new)) {
                    $new_certifcates_to_insert[$customer_id] = $certificates_new;
                }
                if (!empty($certificates_update)) {
                    $new_certifcates_to_update[$customer_id][] = $certificates_update;
                }
            }

            if (!empty($new_certifcates_to_insert)) {
                //loggin account id, 564b5e164d0fa
                $sqlRes = $this->certificate_model->insertCertificates($new_certifcates_to_insert, 1, $account_id = NULL, $remark = "");
            }

            if (!empty($new_certifcates_to_update)) {

            }


            $response = array(
                "rcode" => 1,
                "message" => "Okee",
                "insert_certificates" => $new_certifcates_to_insert,
                "update_certificates" => $new_certifcates_to_update
                );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
        
    }

}
