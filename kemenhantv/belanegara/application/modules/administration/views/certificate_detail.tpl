<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="{assets_path('js/iCheck/square/blue.css')}" media="screen"/>

{js("iCheck/iCheck.min.js")}

<script type="text/javascript">

    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }

    $(document).ready(function () {

        setupViewValues();

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#btnPreview').click(function() {
            {if $certificate}
            window.open("{base_url('certificate_preview?certificate_id=')}{$certificate->certificate_id}", '{$certificate->certificate_id}', "width=1024, height=640");
            // window.open("{base_url('certificate_preview?')}");
            {/if}
        });

        $('#theFormSubmit').click(function () {
            $(this).attr('disabled', true);
            clearAlert();

            var url = "{base_url('administration/customer/customer_save')}";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#theForm').serialize(),
                success: function (resp) {
                    $('#theFormSubmit').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#customer_id').val(resp.customer_id);

                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#theFormSubmit').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(jqXHR.responseText);
                }
            });
        });

        function setupViewValues() {
            {if $certificate}
            $('#certificate_id').val('{$certificate->certificate_id}');
            $('#certificate_name').val('{$certificate->certificate_name}');
            $('#last_modified_date').val('{$certificate->last_modified_date}');
            $('#id_no').val('{$certificate->id_no}');
            $('#full_name').val('{$certificate->full_name}');
            $('#address').val('{$certificate->address}');
            $('#phone_no').val('{$certificate->phone_no}');
            $('#occupation').val('{$certificate->occupation}');
            $('#birth_date').val('{$certificate->birth_date}');
            $('#birth_place').val('{$certificate->birth_place}');
            $('#religion_name').val('{$certificate->religion_name}');
            $('#pusdiklat_name').val('{$certificate->pusdiklat_name}');
            $('#province_name').val('{$certificate->province_name}');
            {/if}
        }
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{base_url('administration/customer/')}"><i class="fa fa-dashboard"></i> Customer</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Customer Form</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="certificate_id" name="certificate_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="last_modified_date" class="col-sm-3 control-label">Tanggal</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="last_modified_date" name="last_modified_date" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="certificate_name" class="col-sm-3 control-label">Tipe</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="certificate_name" name="certificate_name" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_no" class="col-sm-3 control-label">No Identitas</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="id_no" name="id_no" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="full_name" class="col-sm-3 control-label">Nama Lengkap</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="full_name" name="full_name" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="address" class="col-sm-3 control-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="address" name="address" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone_no" class="col-sm-3 control-label">No Telp</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="occupation" class="col-sm-3 control-label">Pekerjaan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="occupation" name="occupation" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="birth_date" class="col-sm-3 control-label">Tgl Lahir</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="birth_date" name="birth_date" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="birth_place" class="col-sm-3 control-label">Tempat Lahir</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="birth_place" name="birth_place" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="religion_name" class="col-sm-3 control-label">Agama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="religion_name" name="religion_name" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="pusdiklat_name" class="col-sm-3 control-label">Pusdiklat</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="pusdiklat_name" name="pusdiklat_name" placeholder=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="province_name" class="col-sm-3 control-label">Provinsi</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="province_name" name="province_name" placeholder=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="{base_url('administration/certificate/request_list')}"><button type="button" class="btn btn-default">Go Back</button></a>
                        <button type="button" id="btnPreview" class="btn btn-info pull-right">Preview Certificate</button>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->