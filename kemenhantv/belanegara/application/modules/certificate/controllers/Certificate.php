<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Certificate extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('certificate_model'));
    }


    /**
    * ======= CERTIFICATE REQUEST ========
    */
    public function request_list() {

        $this->setActiveNavigation("administration", "Certificate Request", "certificate_request_list");

        $certificate_list = $this->certificate_model->getCertificateList();

        $this->data['certificate_list'] = $certificate_list;

        $this->layout->build('certificate_request_list.tpl', $this->data);
    }

    public function certificate_type_save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'certificate_name',
                        'label' => 'Certificate Name',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'certificate_acronym',
                        'label' => 'Acronym',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $certificate_type_id = $this->input->post("certificate_type_id");
            $certificate_name = $this->input->post("certificate_name");
            $certificate_acronym = $this->input->post("certificate_acronym");
            
            $response = array();
            $sqlRes = false;

            if (empty($certificate_type_id)) {
                //do insert
                $sqlRes = $this->certificate_model->insertCertificateType($certificate_name, $certificate_acronym);
            } else {
                //do update
                $sqlRes = $this->certificate_model->updateCertificateType($certificate_type_id, $certificate_name, $certificate_acronym);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }


    /**
    * ======== CERTIFICATE TEMPLATE =======
    */
    public function template_list() {

        $this->setActiveNavigation("certificate", "Certificate Type", "certificate_template_list");

        $template_list = $this->certificate_model->getCertificateTemplateList();

        $this->data['template_list'] = $template_list;

        $this->layout->build('certificate_template_list.tpl', $this->data);
    }

    public function template_save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'template_title',
                        'label' => 'Title',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $template_id = $this->input->post("template_id");
            $template_title = $this->input->post("template_title");
            $status = 1;
            $thumbnail = "";
            
            $response = array();
            $sqlRes = false;

            if (empty($template_id)) {
                //do insert
                $template_id = uniqid();
                $sqlRes = $this->certificate_model->insertCertificateTemplate($template_id, $template_title, $status, $thumbnail);
            } else {
                //do update
                $sqlRes = $this->certificate_model->updateCertificateTemplate($template_id, $template_title, $status);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "template_id" => $template_id
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }


    public function template_upload() {
        $this->load->helper('upload_file_helper');

        $template_id = $this->input->post("template_id");
        $target_path = $this->config->item('thumbnail_path');

        $response = array();

        try {

            $newFileName = upload_file($_FILES, $target_path);
            $sqlRes = $this->certificate_model->updateTemplateThumbnail($template_id, $newFileName);
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "target_path" => $target_path,
                    "message" => "Yuhuu..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Error update thumbnail"
                );
            }
        } catch (RuntimeException $e) {
            $response = array(
                "rcode" => 0,
                "message" => $e->getMessage()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function template_delete() {
        $template_id = $this->input->post('template_id');

        $response = array();
        $sqlRes = false;

        if (empty($template_id)) {
            $response = array(
                    'rcode' => 0,
                    'message' => "Incomplete parameters"
                );
        } else {
            $sqlRes = $this->certificate_model->deleteTemplate($template_id);

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }


    /**
    * ======= CERTIFICATE AUTHORITY ======
    */
    public function certificate_authority() {

        $this->setActiveNavigation("certificate", "Certificate Type", "certificate_authority");

        $author_list = $this->certificate_model->getCertificateAuthorityList();


        $this->data['author_list'] = $author_list;

        $this->layout->build('certificate_author_list.tpl', $this->data);
    
    }

    public function author_save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'author_title',
                        'label' => 'Author title',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'author_name',
                        'label' => 'Acronym',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $author_id = $this->input->post("author_id");
            $author_title = $this->input->post("author_title");
            $author_name = $this->input->post("author_name");
            $author_position = $this->input->post("author_position");
            
            $response = array();
            $sqlRes = false;

            if (empty($author_id)) {
                //do insert
                $author_id = uniqid(rand(), false);
                $sqlRes = $this->certificate_model->insertAuthor($author_id, $author_title, $author_name, $author_position);
            } else {
                //do update
                $sqlRes = $this->certificate_model->updateAuthor($author_id, $author_title, $author_name, $author_position);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "author_id" => $author_id
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function author_signature_upload() {
        $this->load->helper('upload_file_helper');

        $author_id = $this->input->post("author_id");
        $target_path = $this->config->item('thumbnail_path');

        $response = array();

        try {

            $newFileName = upload_file($_FILES, $target_path);
            $sqlRes = $this->certificate_model->updateAuthorThumbnail($author_id, $newFileName);
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yuhuu..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Error update thumbnail"
                );
            }
        } catch (RuntimeException $e) {
            $response = array(
                "rcode" => 0,
                "message" => $e->getMessage()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function author_delete() {
        $author_id = $this->input->post('author_id');

        $response = array();
        $sqlRes = false;

        if (empty($author_id)) {
            $response = array(
                    'rcode' => 0,
                    'message' => "Incomplete parameters"
                );
        } else {
            $sqlRes = $this->certificate_model->deleteAuthor($author_id);

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

}
