<script type="text/javascript">
$(document).ready(function () {

    $('#example2').DataTable();

});

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Certificate Type
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Certificate Type</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Certificate Type</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Acronym</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $type_list as $item}
                                <tr role="row">
                                    <td>{$item->certificate_name}</td>
                                    <td>{$item->certificate_acronym}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default" name="btnEdit" data-id="{$item->certificate_type_id}" data-acronym="{$item->certificate_acronym}" data-name="{$item->certificate_name}">
                                                <i class="fa fa-edit"> Edit</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Certificate Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="certificate_type_id" name="certificate_type_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="certificate_name" class="col-sm-2 control-label">Certificate Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="certificate_name" name="certificate_name" placeholder="certificate_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="certificate_acronym" class="col-sm-2 control-label">Acronym</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="certificate_acronym" name="certificate_acronym" placeholder="certificate_acronym"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
$(document).ready(function () {

    $('#btnCreate').click(function () {
        clearModalForm();
        $('#layoutModal').modal('show');
    });

    $('button[name="btnEdit"]').on('click', function () {
        var data_id = $(this).attr('data-id');
        var data_name = $(this).attr('data-name');
        var data_acronym = $(this).attr('data-acronym');


        if (data_id === '')
            return;

        clearModalForm();

        $('#layoutModal').modal('show');
        
        $('#certificate_type_id').val(data_id);
        $('#certificate_name').val(data_name);
        $('#certificate_acronym').val(data_acronym);

    });

    $('#btnSubmitForm').click(function () {

        prepareSubmit();

        var url = "certificate_type_save";

        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: $('#main_form').serialize(),
            success: function (resp) {
                if (resp.rcode === 1) {
                    $('#response_succeed').html(resp.message);
                    $('#response_succeed').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');

                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#response_error').html(jqXHR.responseText);
                $('#response_error').removeClass('hidden');
                $('#btnSubmitForm').removeAttr('disabled');
            }
        });
});

function clearModalForm() {
    $('#btnSubmitForm').removeAttr("disabled");

    $('#certificate_type_id').val('');
    $('#certificate_name').val('');
    $('#certificate_acronym').val('');

    hideErrorMessage();
    hideProgressBar();
}

function hideErrorMessage() {
    $('#response_error').html('');
    $('#response_error').addClass('hidden');
}

function hideProgressBar() {
    $('#progressModal').addClass('hidden');
    $('#progressBar').width(0);
    $('#progressBar').text(0);
}

function prepareSubmit() {
    $('#btnSubmitForm').attr("disabled", "true");
    hideErrorMessage();
}

});
</script>