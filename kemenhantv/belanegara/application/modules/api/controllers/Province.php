<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Province extends UX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('master_model', 'pusdiklat_model'));
    }

    public function list_get() {
        $province_list = $this->master_model->getProvinceListDetail();

        foreach ($province_list as $province) {
            $pusdiklat_list = $this->pusdiklat_model->getListByProvince($province->province_id);
            $province->pusdiklat_list = $pusdiklat_list;

        }

        $response = array(
            "proince_list" => $province_list
        );

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function pusdiklat_list() {
        $province_id = $this->input->get("province_id");

        
        
    }

}
