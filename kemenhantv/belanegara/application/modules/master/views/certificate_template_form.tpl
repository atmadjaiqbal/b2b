iCheck -->
<link rel="stylesheet" type="text/css" href="{assets_path('js/iCheck/square/blue.css')}" media="screen"/>
<link rel="stylesheet" href="{assets_path('js/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}">

{js("iCheck/iCheck.min.js")}
{js("bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}

<script type="text/javascript">

    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }

    $(document).ready(function () {

        setupViewValues();

        $('#content_text').wysihtml5({
            toolbar : {
                'html' : true
            }
        });

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#pusdiklat_id').select2();
        $('#institution_id').select2();
        $('#position_3').select2();
        $('#position_1').select2();

        $('#btnRemove').click(function () {
            var conf = confirm('Are you sure want to delete item?');
            if (!conf) return;


            $(this).attr('disabled', true);
            clearAlert();

            var url = "{base_url('master/certificate/template_delete')}";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#mainForm').serialize(),
                success: function (resp) {
                    $('#btnRemove').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#role_id').val(resp.role_id);

                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);

                        location.href = '{base_url("master/certificate/template_list")}'
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btnRemove').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(jqXHR.responseText);
                }
            });
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "template_save";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#mainForm').serialize(),
                success: function (resp) {
                    $('#btnSubmitForm').removeAttr('disabled');

                    if (resp.rcode === 1) {
                        $('#template_id').val(resp.template_id);
                        console.log("template_id" + resp.template_id);
                        if (!$('#mainForm input[name=file_name]')[0].files[0]) {
                            // location.reload();
                            $('#response_success').html(resp.message);
                            $('#response_success').removeClass('hidden');
                        } else {
                            uploadThumbnail(resp.template_id);
                        }
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btnSubmitForm').removeAttr('disabled');

                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function uploadThumbnail(template_id) {
            $('#progressModal').removeClass('hidden');

            var url = "{base_url('/certificate/certificate/template_upload')}";

            var xhr = new XMLHttpRequest();
            (xhr.upload || xhr).addEventListener('progress', function (e) {
                var done = e.loaded;
                var total = e.total;
                var percent = Math.round(done / total * 100) + '%';
                $('#progressBar').width(percent);
                $('#progressBar').text(percent);
            });

            xhr.addEventListener('load', function (e) {
                $('#btnSubmitForm').removeAttr('disabled');
                console.log(this.responseText);
                try {
                    var resp = JSON.parse(this.responseText);
                    if (resp.rcode === 1) {
                        // location.reload();

                        $('#response_success').html(resp.message);
                        $('#response_success').removeClass('hidden');
                        
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');

                        hideProgressBar();
                    } 
                } catch (e) {
                    $('#response_error').html(this.responseText);
                    $('#response_error').removeClass('hidden');

                    hideProgressBar();
                }

            });
            xhr.open('post', url, true);

            var data;
            data = new FormData();
            data.append('file', $('#mainForm input[name=file_name]')[0].files[0]);
            data.append('template_id', template_id);

            xhr.send(data);
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');

            $('#response_success').html('');
            $('#response_success').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

        function setupViewValues() {
            {if $template}
            $('#template_id').val('{$template->template_id}');
            $('#template_title').val('{$template->template_title}');
            $('#pusdiklat_id').val('{$template->pusdiklat_id}');
            $('#institution_id').val('{$template->institution_id}');
            {if $template->is_default}
            $('#is_default').iCheck('check');
            {/if}
            {/if}

            {foreach $authorize_list as $item}
            $('#position_{$item->position}').val('{$item->author_id}');
            {/foreach}
        }
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Certificate Template Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{base_url('master/certificate/template_list')}"><i class="fa fa-dashboard"></i> Certificate Template</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Certificate Template Form
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="mainForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="template_id" name="template_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="template_title" class="col-sm-4 control-label">Title</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="template_title" name="template_title" placeholder="template_title"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="pusdiklat_id" class="col-sm-4 control-label">Pusdiklat</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="pusdiklat_id" name="pusdiklat_id">
                                                    <option value="">-- Choose Pusdiklat --</option>
                                                    {foreach $pusdiklat_list as $item}
                                                    <option value="{$item->pusdiklat_id}">{$item->pusdiklat_id} - {$item->pusdiklat_name} - {$item->province_name}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="institution_id" class="col-sm-4 control-label">Institution</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="institution_id" name="institution_id">
                                                    <option value="">-- Choose Institution --</option>
                                                    {foreach $institution_list as $item}
                                                    <option value="{$item->institution_id}">{$item->institution_alias} - {$item->institution_name}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="file_name" class="col-sm-4 control-label">Background</label>
                                            <div class="col-sm-8">
                                                <input type="file" class="form-control" id="file_name" name="file_name"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div>
                                        <div class="form-group">
                                            <label for="template_title" class="col-sm-4 control-label">&nbsp;</label>
                                            <div class="col-sm-8">
                                                <div class="checkbox icheck">
                                                    <input type="checkbox" class="single_check" id="is_default" name="is_default" value="1"> Set As Default Template</input>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.box-body -->
                                </div>
                                <div class="col-md-6">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="position_3" class="col-sm-4 control-label">Right Author</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="position_3" name="position_3">
                                                    <option value="">-- Choose Authority --</option>
                                                    {foreach $author_list as $item}
                                                    <option value="{$item->author_id}">{$item->author_name} - {$item->author_title}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="position_1" class="col-sm-4 control-label">Left Author</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="position_1" name="position_1">
                                                    <option value="">-- Choose Authority --</option>
                                                    {foreach $author_list as $item}
                                                    <option value="{$item->author_id}">{$item->author_name} - {$item->author_title}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <!-- <div class="box-body">
                                        <textarea id="content_text" name="content_text" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                            {if $template}
                                            {$template->content_text}
                                            {else}
                                            {$default_content_text}
                                            {/if}
                                        </textarea>
                                    </div> -->
                                    <div class="callout callout-danger hidden" id="response_error"></div>
                                    <div class="callout callout-success hidden" id="response_success"></div>
                                    <div class="box-footer">
                                        <a href="{base_url('master/certificate/template_list')}"><button type="button" class="btn btn-default">Go Back</button></a>
                                        <div class="pull-right">
                                            <button type="button" id="btnRemove" class="btn btn-danger">Remove</button>
                                            <button type="button" id="btnSubmitForm" class="btn btn-info">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper