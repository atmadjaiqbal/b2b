<script type="text/javascript">

    function onBtnEditClicked(data_id) {
        if (data_id === '')
            return;

        clearModalForm();

        $('#layoutModal').modal('show');
        $('#response_error').html('Please wait...');
        $('#response_error').removeClass('hidden');
        $('#btnSubmitForm').attr("disabled", "true");

        $.ajax({
                url: 'pusdiklat/detail?pusdiklat_id=' + data_id,
                success: function (resp) {
                    hideErrorMessage();
                    $('#btnSubmitForm').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        var item = resp.item;

                        $('#pusdiklat_id').val(item.pusdiklat_id);
                        $('#pusdiklat_name').val(item.pusdiklat_name);
                        $('#address').val(item.address);
                        $('#longitude').val(item.longitude);
                        $('#latitude').val(item.latitude);
                        $('#province_id').val(item.province_id);
                        $('#contact_person_name').val(item.contact_person_name);
                        $('#contact_person_email').val(item.contact_person_email);
                        $('#contact_person_phone').val(item.contact_person_phone);
                        
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            })
    }

    function clearModalForm() {
        $('#btnSubmitForm').removeAttr("disabled");

        $('#role_id').val('');
        $('#new_role_id').val('');
        $('#role_name').val('');

        hideErrorMessage();
        hideProgressBar();
    }

    function hideErrorMessage() {
        $('#response_error').html('');
        $('#response_error').addClass('hidden');
    }

    function hideProgressBar() {
        $('#progressModal').addClass('hidden');
        $('#progressBar').width(0);
        $('#progressBar').text(0);
    }

    function prepareSubmit() {
        $('#btnSubmitForm').attr("disabled", "true");
        hideErrorMessage();
    }

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pusdiklat
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pusdiklat</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pusdiklat</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Province</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $pusdiklat_list as $item}
                                <tr role="row">
                                    <td>{$item->pusdiklat_id}</td>
                                    <td>{$item->pusdiklat_name}</td>
                                    <td>{$item->address}</td>
                                    <td>{$item->province_name}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default" name="btnEdit" onclick="onBtnEditClicked('{$item->pusdiklat_id}')">
                                                <i class="fa fa-edit"> Edit</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Pusdiklat Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="pusdiklat_id" name="pusdiklat_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="pusdiklat_name" class="col-sm-2 control-label">Pusdiklat Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="pusdiklat_name" name="pusdiklat_name" placeholder="pusdiklat_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="address" name="address" placeholder="address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="longitude" class="col-sm-2 control-label">Longitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="longitude" name="longitude" placeholder="longitude"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="latitude" class="col-sm-2 control-label">Latitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="latitude" name="latitude" placeholder="latitude"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="province_id" class="col-sm-2 control-label">Province</label>
                                <div class="col-sm-10">
                                    <select id="province_id" name="province_id" class="form-control">
                                        <option value="0">-- Choose Province --</option>
                                        {foreach $province_list as $item}
                                        <option value="{$item->province_id}">{$item->province_name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <h5 class="modal-title">Contact Person</h5>
                            <div class="form-group">
                                <label for="contact_person_name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_name" name="contact_person_name" placeholder="name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_person_email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_email" name="contact_person_email" placeholder="email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_person_phone" class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_phone" name="contact_person_phone" placeholder="phone"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
$(document).ready(function () {

    $('#example2').DataTable();

    $('#btnCreate').click(function () {
        clearModalForm();
        $('#layoutModal').modal('show');
    });

    $('button[name="btnEdit"]').on('click', function () {
        var data_id = $(this).attr('data-id');

        if (data_id === '')
            return;

        clearModalForm();

        $('#layoutModal').modal('show');
        $('#response_error').html('Please wait...');
        $('#response_error').removeClass('hidden');
        $('#btnSubmitForm').attr("disabled", "true");

        $.ajax({
                url: 'pusdiklat/detail?pusdiklat_id=' + data_id,
                success: function (resp) {
                    hideErrorMessage();
                    $('#btnSubmitForm').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        var item = resp.item;

                        $('#pusdiklat_id').val(item.pusdiklat_id);
                        $('#pusdiklat_name').val(item.pusdiklat_name);
                        $('#address').val(item.address);
                        $('#longitude').val(item.longitude);
                        $('#latitude').val(item.latitude);
                        $('#province_id').val(item.province_id);
                        $('#contact_person_name').val(item.contact_person_name);
                        $('#contact_person_email').val(item.contact_person_email);
                        $('#contact_person_phone').val(item.contact_person_phone);
                        
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            })

    });

    $('#btnSubmitForm').click(function () {

        prepareSubmit();

        var url = "pusdiklat/save";

        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: $('#main_form').serialize(),
            success: function (resp) {
                if (resp.rcode === 1) {
                    $('#pusdiklat_id').val(resp.pusdiklat_id);

                    $('#response_succeed').html(resp.message);
                    $('#response_succeed').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');

                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#response_error').html(jqXHR.responseText);
                $('#response_error').removeClass('hidden');
                $('#btnSubmitForm').removeAttr('disabled');
            }
        });
});



});
</script>