<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Certificate Authority
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Authority</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Thumbnail</th>
                                    <th>Title</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($author_list)}
                                    {foreach $author_list as $item}
                                        <tr role="row">
                                            <td>
                                                <span style="width: 220px; height: 220px" class="img-thumbnail">
                                                    <img src="{$item->thumbnail}&w=200" class="img-responsive center-block" style="max-height: 200px; max-width: 200px"/>
                                                </span>
                                            </td>
                                            <td>{$item->author_title}</td>
                                            <td>{$item->author_name}</td>
                                            <td>{$item->author_position}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-block btn-default" name="btnEdit" data-id="{$item->author_id}" data-title="{$item->author_title}"  data-name="{$item->author_name}" data-position="{$item->author_position}">
                                                        <i class="fa fa-edit"> Edit</i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {else}
                                    <tr role="row">
                                        <td colspan="8">No Record found</td>
                                    </tr>
                                {/if}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->



    </section><!-- /.content -->

    <div class="modal" id="mainModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Certificate Author</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="mainForm">
                        <input type="hidden" id="author_id" name="author_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="author_title" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="author_title" name="author_title" placeholder="author_title"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="author_name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="author_name" name="author_name" placeholder="author_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="author_position" class="col-sm-2 control-label">Position</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="author_position" name="author_position" placeholder="author_position"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="file_name" class="col-sm-2 control-label">Thumbnail</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" id="file_name" name="file_name"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error"></div>
                            <div class="callout callout-success hidden" id="response_succeed"></div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="btnDeleteItem">Delete</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        
        $('#btnCreate').click(function () {
            clearMainForm();
            $('#mainModal').modal('show');

        });

        $('button[name="btnEdit"]').on('click', function () {
            var data_id = $(this).attr('data-id');
            var data_title = $(this).attr('data-title');
            var data_name = $(this).attr('data-name');
            var data_position = $(this).attr('data-position');

            if (data_id === '')
                return;

            clearMainForm();
            $('#mainModal').modal('show');
            
            $('#author_id').val(data_id);
            $('#author_title').val(data_title);
            $('#author_name').val(data_name);
            $('#author_position').val(data_position);

        });

        $('#btnDeleteItem').click(function() {
            var resp = confirm("Are you sure?");
            if (resp) {
                var url = "author_delete";
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: url,
                    data: $('#mainForm').serialize(),
                    success: function (resp) {
                        if (resp.rcode === 1) {
                            location.reload();
                        } else {
                            $('#response_error').html(resp.message);
                            $('#response_error').removeClass('hidden');
                            $('#btnSubmitForm').removeAttr('disabled');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#response_error').html(jqXHR.responseText);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                });
            }
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "author_save";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#mainForm').serialize(),
                success: function (resp) {
                    if (resp.rcode === 1) {
                        $('#author_id').val(resp.author_id);
                        console.log("author_id" + resp.author_id);
                        if (!$('#mainForm input[name=file_name]')[0].files[0]) {
                            location.reload();
                        } else {
                            uploadThumbnail(resp.author_id);
                        }
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function uploadThumbnail(author_id) {
            $('#progressModal').removeClass('hidden');

            var url = "{base_url('/certificate/certificate/author_signature_upload')}";

            var xhr = new XMLHttpRequest();
            (xhr.upload || xhr).addEventListener('progress', function (e) {
                var done = e.loaded;
                var total = e.total;
                var percent = Math.round(done / total * 100) + '%';
                $('#progressBar').width(percent);
                $('#progressBar').text(percent);
            });

            xhr.addEventListener('load', function (e) {
                $('#btnSubmitForm').removeAttr('disabled');
                console.log(this.responseText);
                try {
                    var resp = JSON.parse(this.responseText);
                    if (resp.rcode === 1) {
                        location.reload();
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');

                        hideProgressBar();
                    } 
                } catch (e) {
                    $('#response_error').html(this.responseText);
                    $('#response_error').removeClass('hidden');

                    hideProgressBar();
                }

            });
            xhr.open('post', url, true);

            var data;
            data = new FormData();
            data.append('file', $('#mainForm input[name=file_name]')[0].files[0]);
            data.append('author_id', author_id);

            xhr.send(data);
        }

        function clearMainForm() {
            $('#btnSubmitForm').removeAttr("disabled");

            $('#author_id').val('');
            $('#author_title').val('');
            $('#author_name').val('');
            $('#author_position').val('');
            $('#file_name').val('');

            hideErrorMessage();
            hideProgressBar();
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

    });
</script>