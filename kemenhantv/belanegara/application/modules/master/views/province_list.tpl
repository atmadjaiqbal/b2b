<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Province
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Province</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Province</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Longitude</th>
                                    <th>Latitude</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $province_list as $item}
                                <tr role="row">
                                    <td>{$item->province_code}</td>
                                    <td>{$item->province_name}</td>
                                    <td>{$item->longitude}</td>
                                    <td>{$item->latitude}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default" name="btnEdit" data-id="{$item->province_id}" data-code="{$item->province_code}" data-name="{$item->province_name}" data-longitude="{$item->longitude}" data-latitude="{$item->latitude}">
                                                <i class="fa fa-edit"> Edit</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Province Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="province_id" name="province_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="province_code" class="col-sm-2 control-label">Province Code</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="province_code" name="province_code" placeholder="province_code"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="province_name" class="col-sm-2 control-label">Province Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="province_name" name="province_name" placeholder="province_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="longitude" class="col-sm-2 control-label">Longitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="longitude" name="longitude" placeholder="longitude"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="latitude" class="col-sm-2 control-label">Latitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="latitude" name="latitude" placeholder="latitude"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
$(document).ready(function () {

    $('#example2').DataTable({
        "bPaginate": false
    });

    $('#btnCreate').click(function () {
        clearModalForm();
        $('#layoutModal').modal('show');
    });

    $('button[name="btnEdit"]').on('click', function () {
        var data_id = $(this).attr('data-id');
        var data_code = $(this).attr('data-code');
        var data_name = $(this).attr('data-name');
        var data_longitude = $(this).attr('data-longitude');
        var data_latitude = $(this).attr('data-latitude');


        if (data_id === '')
            return;

        clearModalForm();

        $('#layoutModal').modal('show');
        
        $('#province_id').val(data_id);
        $('#province_code').val(data_code);
        $('#province_name').val(data_name);
        $('#longitude').val(data_longitude);
        $('#latitude').val(data_latitude);

    });

    $('#btnSubmitForm').click(function () {

        prepareSubmit();

        var url = "province/save";

        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: $('#main_form').serialize(),
            success: function (resp) {
                if (resp.rcode === 1) {
                    $('#province_id').val(resp.province_id);

                    $('#response_succeed').html(resp.message);
                    $('#response_succeed').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');

                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#response_error').html(jqXHR.responseText);
                $('#response_error').removeClass('hidden');
                $('#btnSubmitForm').removeAttr('disabled');
            }
        });
});

function clearModalForm() {
    $('#btnSubmitForm').removeAttr("disabled");

    $('#province_id').val('');
    $('#province_code').val('');
    $('#province_name').val('');
    $('#longitude').val('');
    $('#latitude').val('');

    hideErrorMessage();
    hideProgressBar();
}

function hideErrorMessage() {
    $('#response_error').html('');
    $('#response_error').addClass('hidden');
}

function hideProgressBar() {
    $('#progressModal').addClass('hidden');
    $('#progressBar').width(0);
    $('#progressBar').text(0);
}

function prepareSubmit() {
    $('#btnSubmitForm').attr("disabled", "true");
    hideErrorMessage();
}

});
</script>