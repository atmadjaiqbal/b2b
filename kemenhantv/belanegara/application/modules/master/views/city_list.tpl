<script type="text/javascript">
$(document).ready(function () {

    $('#example2').DataTable();

});

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            City
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">City</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">City</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Longitude</th>
                                    <th>Latitude</th>
                                    <th>Province</th>
                                    <th width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $city_list as $item}
                                <tr role="row">
                                    <td>{$item->city_code}</td>
                                    <td>{$item->city_name}</td>
                                    <td>{$item->longitude}</td>
                                    <td>{$item->latitude}</td>
                                    <td>{$item->province_name}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default" name="btnEdit" data-id="{$item->city_id}" data-code="{$item->city_code}" data-name="{$item->city_name}" data-longitude="{$item->longitude}" data-latitude="{$item->latitude}" data-province-id="{$item->province_id}">
                                                <i class="fa fa-edit"> Edit</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">City Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="city_id" name="city_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="city_code" class="col-sm-2 control-label">City Code</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="city_code" name="city_code" placeholder="city_code"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="city_name" class="col-sm-2 control-label">City Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="city_name" name="city_name" placeholder="city_name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="longitude" class="col-sm-2 control-label">Longitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="longitude" name="longitude" placeholder="longitude"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="latitude" class="col-sm-2 control-label">Latitude</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="latitude" name="latitude" placeholder="latitude"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="province_id" class="col-sm-2 control-label">Province</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="province_id" name="province_id">
                                        <option value="">-- Choose Province --</option>
                                        {foreach $province_list as $item}
                                        <option value="{$item->province_id}">{$item->province_name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
$(document).ready(function () {

    $('#btnCreate').click(function () {
        clearModalForm();
        $('#layoutModal').modal('show');
    });

    $('button[name="btnEdit"]').on('click', function () {
        var data_id = $(this).attr('data-id');
        var data_code = $(this).attr('data-code');
        var data_name = $(this).attr('data-name');
        var data_longitude = $(this).attr('data-longitude');
        var data_latitude = $(this).attr('data-latitude');
        var data_province_id = $(this).attr('data-province-id');


        if (data_id === '')
            return;

        clearModalForm();

        $('#layoutModal').modal('show');
        
        $('#city_id').val(data_id);
        $('#city_code').val(data_code);
        $('#city_name').val(data_name);
        $('#longitude').val(data_longitude);
        $('#latitude').val(data_latitude);
        $('#province_id').val(data_province_id);

    });

    $('#btnSubmitForm').click(function () {

        prepareSubmit();

        var url = "city/save";

        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: $('#main_form').serialize(),
            success: function (resp) {
                if (resp.rcode === 1) {

                    $('#response_succeed').html(resp.message);
                    $('#response_succeed').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');

                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#response_error').html(jqXHR.responseText);
                $('#response_error').removeClass('hidden');
                $('#btnSubmitForm').removeAttr('disabled');
            }
        });
});

function clearModalForm() {
    $('#btnSubmitForm').removeAttr("disabled");

    $('#city_id').val('');
    $('#city_code').val('');
    $('#city_name').val('');
    $('#longitude').val('');
    $('#latitude').val('');
    $('#province_id').val('');

    hideErrorMessage();
    hideProgressBar();
}

function hideErrorMessage() {
    $('#response_error').html('');
    $('#response_error').addClass('hidden');
}

function hideProgressBar() {
    $('#progressModal').addClass('hidden');
    $('#progressBar').width(0);
    $('#progressBar').text(0);
}

function prepareSubmit() {
    $('#btnSubmitForm').attr("disabled", "true");
    hideErrorMessage();
}

});
</script>