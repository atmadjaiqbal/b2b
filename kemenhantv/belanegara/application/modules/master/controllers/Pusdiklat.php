<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Pusdiklat extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('pusdiklat_model', 'master_model'));
    }

    /**
     * Role List
     */
    public function index() {

        $this->setActiveNavigation("master", "Pusdiklat", "pusdiklat");

        $pusdiklat_list = $this->pusdiklat_model->getList();
        $province_list = $this->master_model->getProvinceList();

        $this->data['pusdiklat_list'] = $pusdiklat_list;
        $this->data['province_list'] = $province_list;

        $this->layout->build('pusdiklat_list.tpl', $this->data);
    }
    
    public function save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
            array(
                array(
                    'field' => 'address',
                    'label' => 'Address',
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'pusdiklat_name',
                    'label' => 'Pusdiklat Name',
                    'rules' => 'trim|required'
                    )
                )
            );

        if ($this->form_validation->run()) {

            $pusdiklat_id = $this->input->post("pusdiklat_id");
            $pusdiklat_name = $this->input->post("pusdiklat_name");
            $address = $this->input->post("address");
            $contact_person_name = $this->input->post("contact_person_name");
            $contact_person_email = $this->input->post("contact_person_email");
            $contact_person_phone = $this->input->post("contact_person_phone");
            $longitude = $this->input->post("longitude");
            $latitude = $this->input->post("latitude");
            $province_id = $this->input->post("province_id");
            $city_id = $this->input->post("city_id");

            if (empty($province_id)) {
                $province_id = null;
            }

            if (empty($city_id)) {
                $city_id = null;
            }

            $response = array();
            $sqlRes = false;

            if (empty($pusdiklat_id)) {
                //do insert
                $pusdiklat_id = uniqid();
                $sqlRes = $this->pusdiklat_model->insertPusdiklat($pusdiklat_id, $pusdiklat_name, $address, $contact_person_name, $contact_person_email, $contact_person_phone, $longitude, $latitude, $province_id, $city_id);
            } else {
                //do update
                $sqlRes = $this->pusdiklat_model->updatePusdiklat($pusdiklat_id, $pusdiklat_name, $address, $contact_person_name, $contact_person_email, $contact_person_phone, $longitude, $latitude, $province_id, $city_id);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "pusdiklat_id" => $pusdiklat_id
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
                );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }
    
    
    public function detail() {
        $pusdiklat_id = $this->input->get('pusdiklat_id');

        $sqlRes = false;
        $response = array();

        if (empty($pusdiklat_id)) {
            $response = array(
                "rcode" => 0,
                "message" => "Incomplete parameters : pusdiklat_id"
                );
        } else {
            $item = $this->pusdiklat_model->pusdiklatDetail($pusdiklat_id);

            $response = array(
                "rcode" => 1,
                "message" => "Yeeahhh...",
                "item" => $item
                );
        }

        

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));

    }

}
