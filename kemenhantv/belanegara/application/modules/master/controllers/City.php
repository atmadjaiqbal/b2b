<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class City extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('master_model'));
    }

    /**
     * Role List
     */
    public function index() {


        $this->setActiveNavigation("master", "Province", "city");

        $province_list = $this->master_model->getProvinceList();
        $city_list = $this->master_model->getCityList();

        $this->data['city_list'] = $city_list;
        $this->data['province_list'] = $province_list;

        $this->layout->build('city_list.tpl', $this->data);
    }
    
    public function save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'city_code',
                        'label' => 'City Code',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'city_name',
                        'label' => 'City Name',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'province_id',
                        'label' => 'Province ID',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $province_id = $this->input->post("province_id");
            $city_id = $this->input->post("city_id");
            $city_code = $this->input->post("city_code");
            $city_name = $this->input->post("city_name");
            $longitude = $this->input->post("longitude");
            $latitude = $this->input->post("latitude");
            
            $response = array();
            $sqlRes = false;

            if (empty($city_id)) {
                //do insert
                $sqlRes = $this->master_model->insertCity($city_code, $city_name, $longitude, $latitude, $province_id);
            } else {
                //do update
                $sqlRes = $this->master_model->updateCity($city_id, $city_code, $city_name, $longitude, $latitude, $province_id);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

}
