<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Province extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('master_model'));
    }

    /**
     * Role List
     */
    public function index() {


        $this->setActiveNavigation("master", "Province", "province");

        $province_list = $this->master_model->getProvinceList();

        $this->data['province_list'] = $province_list;

        $this->layout->build('province_list.tpl', $this->data);
    }
    
    public function save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'province_code',
                        'label' => 'Province Code',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'province_name',
                        'label' => 'Province Name',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $province_id = $this->input->post("province_id");
            $province_code = $this->input->post("province_code");
            $province_name = $this->input->post("province_name");
            $longitude = $this->input->post("longitude");
            $latitude = $this->input->post("latitude");
            
            $response = array();
            $sqlRes = false;

            if (empty($province_id)) {
                //do insert
                $sqlRes = $this->master_model->insertProvince($province_code, $province_name, $longitude, $latitude);
            } else {
                //do update
                $sqlRes = $this->master_model->updateProvince($province_id, $province_code, $province_name, $longitude, $latitude);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

}
