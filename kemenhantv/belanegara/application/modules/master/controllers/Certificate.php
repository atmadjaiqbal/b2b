<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Certificate extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('certificate_model'));
    }


    /**
    * ======= CERTIFICATE TYPE ========
    */
    public function type_list() {

        $this->setActiveNavigation("master", "Certificate Type", "certificate_type_list");

        $type_list = $this->certificate_model->getCertificateTypeList();

        $this->data['type_list'] = $type_list;

        $this->layout->build('certificate_type_list.tpl', $this->data);
    }

    public function certificate_type_save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'certificate_name',
                        'label' => 'Certificate Name',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'certificate_acronym',
                        'label' => 'Acronym',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $certificate_type_id = $this->input->post("certificate_type_id");
            $certificate_name = $this->input->post("certificate_name");
            $certificate_acronym = $this->input->post("certificate_acronym");
            
            $response = array();
            $sqlRes = false;

            if (empty($certificate_type_id)) {
                //do insert
                $sqlRes = $this->certificate_model->insertCertificateType($certificate_name, $certificate_acronym);
            } else {
                //do update
                $sqlRes = $this->certificate_model->updateCertificateType($certificate_type_id, $certificate_name, $certificate_acronym);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }


    /**
    * ======== CERTIFICATE TEMPLATE =======
    */
    public function template_list() {

        $this->setActiveNavigation("master", "Certificate Type", "certificate_template_list");

        $template_list = $this->certificate_model->getCertificateTemplateList();

        $this->data['template_list'] = $template_list;

        $this->layout->build('certificate_template_list.tpl', $this->data);
    }

    public function template_form() {

        $this->load->model(array('pusdiklat_model', 'institution_model'));

        $template_id = $this->input->get("template_id");
        $template = null;
        $default_content_text = NULL;

        $pusdiklat_list = $this->pusdiklat_model->getList();
        $institution_list = $this->institution_model->getInstitutionList();

        $author_list = $this->certificate_model->getCertificateAuthorityList();

        if (!empty($template_id)) {
            $template = $this->certificate_model->getCertificateTemplateList($template_id);
            $authorize_list = $this->certificate_model->getTemplateAuthors($template_id);
        } else {
            /* -- Block Comment for further purpose -- */
            // $default_content_text = file_get_contents(base_url('certificate_preview/default_template'));
        }

        $this->setActiveNavigation("master", "Certificate Type", "certificate_template_list");
        $this->data['template'] = $template;
        $this->data['default_content_text'] = $default_content_text;
        $this->data['pusdiklat_list'] = $pusdiklat_list;
        $this->data['institution_list'] = $institution_list;
        $this->data['author_list'] = $author_list;
        $this->data['authorize_list'] = $authorize_list;


        $this->layout->build('certificate_template_form.tpl', $this->data);
    }    

    public function template_save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'template_title',
                        'label' => 'Title',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $template_id = $this->input->post("template_id");
            $template_title = $this->input->post("template_title");
            $content_text = $this->input->post("content_text");
            $pusdiklat_id = $this->input->post("pusdiklat_id");
            $institution_id = $this->input->post("institution_id");
            $is_default = $this->input->post("is_default");
            /*--- certificate author --*/
            //right position
            $position_3 = $this->input->post("position_3");
            //left position
            $position_1 = $this->input->post("position_1");

            if (empty($pusdiklat_id)) $pusdiklat_id = NULL;
            if (empty($institution_id)) $institution_id = NULL;

            //parse author manually
            //TODO, Make it automatic!
            $template_authorize_list = array();
            if (!empty($position_3)) {
                $template_authorize_3 = [
                    "author_id" => $position_3, 
                    "position" => 3
                ];
                $template_authorize_list[] = $template_authorize_3;
            }

            if (!empty($position_1)) {
                $template_authorize_1 = [
                    "author_id" => $position_1, 
                    "position" => 1
                ];
                $template_authorize_list[] = $template_authorize_1;
            }


            $status = 1;
            $thumbnail = "";
            
            $response = array();
            $sqlRes = false;

            if (empty($template_id)) {
                //do insert
                $template_id = uniqid();
                $sqlRes = $this->certificate_model->insertCertificateTemplate($template_id, $template_title, $status, $content_text, $pusdiklat_id, $institution_id, $is_default);
            } else {
                //do update
                $sqlRes = $this->certificate_model->updateCertificateTemplate($template_id, $template_title, $status, $content_text, $pusdiklat_id, $institution_id, $is_default);
            }
            
            if ($sqlRes) {

                //update template author
                //do manual insert the author position

                $sqlRes = $this->certificate_model->updateTemplateAuthors($template_id, $template_authorize_list);

                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "template_id" => $template_id
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }


    public function template_upload() {
        $this->load->helper('upload_file_helper');

        $template_id = $this->input->post("template_id");
        $target_path = $this->config->item('thumbnail_path');

        $response = array();

        try {

            $newFileName = upload_file($_FILES, $target_path);
            $sqlRes = $this->certificate_model->updateTemplateThumbnail($template_id, $newFileName);
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "target_path" => $target_path,
                    "message" => "Yuhuu..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Error update thumbnail"
                );
            }
        } catch (RuntimeException $e) {
            $response = array(
                "rcode" => 0,
                "message" => $e->getMessage()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function template_delete() {
        $template_id = $this->input->post('template_id');

        $response = array();
        $sqlRes = false;

        if (empty($template_id)) {
            $response = array(
                    'rcode' => 0,
                    'message' => "Incomplete parameters"
                );
        } else {
            $sqlRes = $this->certificate_model->deleteTemplate($template_id);

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }


    /**
    * ======= CERTIFICATE AUTHORITY ======
    */
    public function certificate_authority() {

        $this->setActiveNavigation("master", "Certificate Type", "certificate_authority");

        $author_list = $this->certificate_model->getCertificateAuthorityList();


        $this->data['author_list'] = $author_list;

        $this->layout->build('certificate_author_list.tpl', $this->data);
    
    }

    public function author_save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'author_title',
                        'label' => 'Author title',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'author_name',
                        'label' => 'Acronym',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $author_id = $this->input->post("author_id");
            $author_title = $this->input->post("author_title");
            $author_name = $this->input->post("author_name");
            $author_position = $this->input->post("author_position");
            
            $response = array();
            $sqlRes = false;

            if (empty($author_id)) {
                //do insert
                $author_id = uniqid(rand(), false);
                $sqlRes = $this->certificate_model->insertAuthor($author_id, $author_title, $author_name, $author_position);
            } else {
                //do update
                $sqlRes = $this->certificate_model->updateAuthor($author_id, $author_title, $author_name, $author_position);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "author_id" => $author_id
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function author_signature_upload() {
        $this->load->helper('upload_file_helper');

        $author_id = $this->input->post("author_id");
        $target_path = $this->config->item('thumbnail_path');

        $response = array();

        try {

            $newFileName = upload_file($_FILES, $target_path);
            $sqlRes = $this->certificate_model->updateAuthorThumbnail($author_id, $newFileName);
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yuhuu..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Error update thumbnail"
                );
            }
        } catch (RuntimeException $e) {
            $response = array(
                "rcode" => 0,
                "message" => $e->getMessage()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function author_delete() {
        $author_id = $this->input->post('author_id');

        $response = array();
        $sqlRes = false;

        if (empty($author_id)) {
            $response = array(
                    'rcode' => 0,
                    'message' => "Incomplete parameters"
                );
        } else {
            $sqlRes = $this->certificate_model->deleteAuthor($author_id);

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

}
