<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Institution extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('institution_model'));
    }

    /**
     * Role List
     */
    public function index() {

        $this->setActiveNavigation("master", "Institution", "institution");

        // $pusdiklat_list = $this->pusdiklat_model->getList();
        // $province_list = $this->master_model->getProvinceList();

        $institution_list = $this->institution_model->getInstitutionList();

        $this->data['institution_list'] = $institution_list;

        $this->layout->build('institution_list.tpl', $this->data);
    }
    
    public function save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
            array(
                array(
                    'field' => 'institution_name',
                    'label' => 'Institution Name',
                    'rules' => 'trim|required'
                    )
                )
            );

        if ($this->form_validation->run()) {

            $institution_id = $this->input->post("institution_id");
            $institution_name = $this->input->post("institution_name");
            $institution_alias = $this->input->post("institution_alias");
            $institution_address = $this->input->post("institution_address");
            $contact_person_name = $this->input->post("contact_person_name");
            $contact_person_email = $this->input->post("contact_person_email");
            $contact_person_phone = $this->input->post("contact_person_phone");
            $contact_person_fax = $this->input->post("contact_person_fax");
            $website = $this->input->post("website");

            $response = array();
            $sqlRes = false;

            if (empty($institution_id)) {
                //do insert
                $institution_id = uniqid();
                $sqlRes = $this->institution_model->insertInstitution($institution_id, $institution_name, $institution_address, $institution_alias, $contact_person_name, $contact_person_phone, $contact_person_email, $contact_person_fax, $website);
            } else {
                //do update
                $sqlRes = $this->institution_model->updateInstitution($institution_id, $institution_name, $institution_address, $institution_alias, $contact_person_name, $contact_person_phone, $contact_person_email, $contact_person_fax, $website);
            }
            
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "institution_id" => $institution_id
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
                );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }
    
    
    public function detail() {
        $institution_id = $this->input->get('institution_id');

        $sqlRes = false;
        $response = array();

        if (empty($institution_id)) {
            $response = array(
                "rcode" => 0,
                "message" => "Incomplete parameters : institution_id"
                );
        } else {
            $item = $this->institution_model->getInstitutionList($institution_id);

            $response = array(
                "rcode" => 1,
                "message" => "Yeeahhh...",
                "item" => $item
                );
        }

        

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));

    }

    
    public function delete() {
        $institution_id = $this->input->post('institution_id');

        $sqlRes = false;
        $response = array();

        if (empty($institution_id)) {
            $response = array(
                "rcode" => 0,
                "message" => "Incomplete parameters : institution_id"
                );
        } else {
            $sqlRes = $this->institution_model->deleteInstitution($institution_id);

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        }

        

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));

    }

}
