<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Army Ranks
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Army Ranks</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Rank</th>
                                    <th>Title</th>
                                    <th>Minimum Download</th>
                                    <th>Bonus Point</th>
                                    <th width="50">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($army_rank_list)}
                                    {foreach $army_rank_list as $rank}
                                        <tr role="row">
                                            <td>
                                                {$rank@index+1}
                                            </td>
                                            <td>
                                                <span class="img-thumbnail" style="width: 120px; height: 50px">
                                                    <img src="{$rank->thumbnail}&w=100" alt="{$rank->rank_name}" title="{$rank->rank_name}" class="img-responsive center-block" style="max-height:100px; max-width: 100px;"/>
                                                </span>
                                            </td>
                                            <td>{$rank->rank_name}</td>
                                            <td>{$rank->required_download|number_format:0:",":"."}</td>
                                            <td>{$rank->bonus_point}</td>
                                            <td>
                                                <button class="btn btn-block btn-warning" name="btnEdit" data-id="{$rank->rank_id}" data-title="{$rank->rank_name}" data-required_download="{$rank->required_download}" data-bonus_point="{$rank->bonus_point}">
                                                    <span class="fa fa-edit"></span> Edit
                                                </button>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {else}
                                    <tr role="row">
                                        <td colspan="8">No Record found</td>
                                    </tr>
                                {/if}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->



    </section><!-- /.content -->

    <div class="modal" id="armyRankModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Army Ranks</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="army_rank_form">
                        <input type="hidden" id="rank_id" name="rank_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="title" maxlength="50"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="required_download" class="col-sm-2 control-label">Required Download</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="required_download" name="required_download" placeholder="required download" maxlength="20"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bonus_point" class="col-sm-2 control-label">Bonus Point</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="bonus_point" name="bonus_point" placeholder="bonus point" maxlength="10"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="file_name" class="col-sm-2 control-label">Thumbnail</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" id="file_name" name="file_name"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">

                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {

        $('#btnCreate').click(function () {
            clearForm();
            $('#armyRankModal').modal('show');

        });

        $('button[name="btnEdit"]').on('click', function () {
            var rank_id = $(this).attr('data-id');

            if (rank_id === '')
                return;

            clearForm();

            $('#rank_id').val(rank_id);
            $('#title').val($(this).attr('data-title'));
            $('#required_download').val($(this).attr('data-required_download'));
            $('#bonus_point').val($(this).attr('data-bonus_point'));

            $('#armyRankModal').modal('show');

        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "rank/save_rank";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#army_rank_form').serialize(),
                success: function (resp) {
                    console.log('resp:' + resp.rcode);
                    if (resp.rcode === 1) {
                        $('#rank_id').val(resp.rank_id);
                        if (!$('#army_rank_form input[name=file_name]')[0].files[0]) {
                            location.reload();
                        } else {
                            uploadThumbnail(resp.rank_id);
                        }
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(errorThrown);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function uploadThumbnail(rank_id) {
            $('#progressModal').removeClass('hidden');

            var url = "{base_url('/grandprize/rank/update_thumbnail')}";

            var xhr = new XMLHttpRequest();
            (xhr.upload || xhr).addEventListener('progress', function (e) {
                var done = e.loaded;
                var total = e.total;
                var percent = Math.round(done / total * 100) + '%';
                $('#progressBar').width(percent);
                $('#progressBar').text(percent);
            });

            xhr.addEventListener('load', function (e) {
                $('#btnSubmitForm').removeAttr('disabled');
                console.log(this.responseText);
                var resp = JSON.parse(this.responseText);
                if (resp.rcode === 1) {
                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');

                    hideProgressBar();
                }
            });
            xhr.open('post', url, true);

            var data;
            data = new FormData();
            data.append('file', $('#army_rank_form input[name=file_name]')[0].files[0]);
            data.append('rank_id', rank_id);

            xhr.send(data);
        }

        function clearForm() {
            $('#btnSubmitForm').removeAttr("disabled");

            $('#rank_id').val('');
            $('#title').val('');
            $('#required_download').val('');
            $('#bonus_point').val('');

            hideErrorMessage();
            hideProgressBar();
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

    });
</script>