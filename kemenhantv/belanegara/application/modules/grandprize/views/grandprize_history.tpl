<script type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

    });

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Achievements
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Achievements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">History</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>GrandPrize</th>
                                    <th>Rank</th>
                                    <th>Achieved Date</th>
                                    <th>Title</th>
                                    <th>Army Code</th>
                                    <th>Army Name</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $history_list as $grandprize}
                                    <tr role="row">
                                        <td>
                                            <span style="width: 220px; height: 220px" class="img-thumbnail">
                                                <img src="{$grandprize->thumbnail}&w=200" alt="{$grandprize->title}" title="{$grandprize->title}" class="img-responsive center-block" style="max-height: 200px; max-width: 200px"/>
                                            </span>
                                        </td>
                                        <td>
                                            <img src="{$grandprize->rank_thumbnail}&w=100" alt="{$grandprize->rank_name}" title="{$grandprize->rank_name}" class="img-responsive center-block" style="max-height:100px; max-width: 100px;"/>
                                        </td>
                                        <td>{$grandprize->created_date}</td>
                                        <td>{$grandprize->rank_name}</td>
                                        <td>{$grandprize->reference_number}</td>
                                        <td>{$grandprize->display_name}</td>
                                        <td>
                                            <button class="btn btn-block btn-warning" name="btnEdit" data-id="{$grandprize->referral_number}">
                                                <span class="fa fa-edit"></span> Log
                                            </button>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
