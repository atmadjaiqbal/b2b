<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Grand Prize
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">History</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Thumbnail</th>
                                    <th>Rank</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Minimum Download</th>
                                    <th>Total Stock</th>
                                    <th>Available Stock</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {if !empty($grandprize_list)}
                                    {foreach $grandprize_list as $grandprize}
                                        <tr role="row">
                                            <td>
                                                <span style="width: 220px; height: 220px" class="img-thumbnail">
                                                    <img src="{$grandprize->thumbnail}&w=200" class="img-responsive center-block" style="max-height: 200px; max-width: 200px"/>
                                                </span>
                                            </td>
                                            <td>                                                
                                                <img src="{$grandprize->rank_thumbnail}&w=100" alt="{$grandprize->rank_name}" title="{$grandprize->rank_name}" class="img-responsive center-block" style="max-height:100px; max-width: 100px;"/>
                                            </td>
                                            <td>{$grandprize->title}</td>
                                            <td>{$grandprize->description}</td>
                                            <td>{$grandprize->required_download|number_format:0:",":"."}</td>
                                            <td>{$grandprize->total_stock}</td>
                                            <td>{$grandprize->stock_unit}</td>
                                            <td>
                                                <button class="btn btn-block btn-warning" name="btnEdit" data-id="{$grandprize->grandprize_id}">
                                                    <span class="fa fa-edit"></span> Edit
                                                </button>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {else}
                                    <tr role="row">
                                        <td colspan="8">No Record found</td>
                                    </tr>
                                {/if}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->



    </section><!-- /.content -->

    <div class="modal" id="grandprizeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Grand Prize</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="gand_prize_form">
                        <input type="hidden" id="grandprize_id" name="grandprize_id"/>
                        <input type="hidden" name="stock_unit"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="title"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="description" name="description" placeholder="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="total_stock" class="col-sm-2 control-label">Total Stock</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="total_stock" name="total_stock" placeholder="total stock" data-orig_stock=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="stock_unit" class="col-sm-2 control-label">Available Stock</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="stock_unit" name="stock_unit" placeholder="stock unit" disabled="true"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="order_no" class="col-sm-2 control-label">Order Number</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="order_no" name="order_no" placeholder="order number"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="required_download" class="col-sm-2 control-label" style="padding-top: 0;">Required Rank</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="rank_id" name="rank_id">
                                        <option value="">-- Please select required rank --</option>
                                        {foreach $army_rank_list as $army_rank}
                                            <option value="{$army_rank->rank_id}">{$army_rank->rank_name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="file_name" class="col-sm-2 control-label">Thumbnail</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" id="file_name" name="file_name"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">

                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        
        var original_stock = 0;

        $('#btnCreate').click(function () {
            clearGrandprizeForm();
            $('#grandprizeModal').modal('show');

        });
        
        $('#total_stock').on('change', function() {
            var stock_unit = $(this).val()|0;
            
            var newStock = Number(stock_unit - original_stock) + Number($('#stock_unit').val());
            
            $('input[name="stock_unit"]').val(newStock);
            
        });

        $('button[name="btnEdit"]').on('click', function () {
            var grandprize_id = $(this).attr('data-id');

            if (grandprize_id === '')
                return;

            clearGrandprizeForm();
            $('#grandprizeModal').modal('show');
            $('#response_error').html('Please wait...');
            $('#response_error').removeClass('hidden');
            $('#btnSubmitForm').attr("disabled", "true");

            $.ajax({
                url: 'grandprize_list/detail?id=' + grandprize_id,
                success: function (resp) {
                    hideErrorMessage();
                    $('#btnSubmitForm').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        var grandprize = resp.grandprize;
                        $('#grandprize_id').val(grandprize.grandprize_id);
                        $('#title').val(grandprize.title);
                        $('#description').text(grandprize.description);
                        $('#order_no').val(grandprize.order_no);
                        $('#rank_id').val(grandprize.rank_id);
                        $('#total_stock').val(grandprize.total_stock);
                        $('input[name="stock_unit"]').val(grandprize.stock_unit);
                        
                        original_stock = grandprize.total_stock;
                        
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('Error fetch detail:' + errorThrown);
                    $('#response_error').html("Oppzz.. Something's wrong. Check your internet please");
                }
            })
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "grandprize_list/save_grandprize";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#gand_prize_form').serialize(),
                success: function (resp) {
                    console.log('resp:' + resp.rcode);
                    if (resp.rcode === 1) {
                        $('#grandprize_id').val(resp.grandprize_id);
                        if (!$('#gand_prize_form input[name=file_name]')[0].files[0]) {
                            location.reload();
                        } else {
                            uploadThumbnail(resp.grandprize_id);
                        }
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(errorThrown);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function uploadThumbnail(grandprize_id) {
            $('#progressModal').removeClass('hidden');

            var url = "{base_url('/grandprize/grandprize_list/update_thumbnail')}";

            var xhr = new XMLHttpRequest();
            (xhr.upload || xhr).addEventListener('progress', function (e) {
                var done = e.loaded;
                var total = e.total;
                var percent = Math.round(done / total * 100) + '%';
                $('#progressBar').width(percent);
                $('#progressBar').text(percent);
            });

            xhr.addEventListener('load', function (e) {
                $('#btnSubmitForm').removeAttr('disabled');
                console.log(this.responseText);
                var resp = JSON.parse(this.responseText);
                if (resp.rcode === 1) {
                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');

                    hideProgressBar();
                }
            });
            xhr.open('post', url, true);

            var data;
            data = new FormData();
            data.append('file', $('#gand_prize_form input[name=file_name]')[0].files[0]);
            data.append('grandprize_id', grandprize_id);

            xhr.send(data);
        }

        function clearGrandprizeForm() {
            $('#btnSubmitForm').removeAttr("disabled");

            $('#grandprize_id').val('');
            $('#title').val('');
            $('#description').text('');
            $('#total_stock').val('');
            $('#order_no').val('');
            $('#rank_id').val('');
            
            $('input[name="stock_unit"]').val(grandprize.stock_unit);

            hideErrorMessage();
            hideProgressBar();
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

    });
</script>