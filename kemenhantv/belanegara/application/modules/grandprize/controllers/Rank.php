<?php

/**
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @version 		2.0
 * 
 * */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Rank extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('armyrank_model'));
    }

    public function index() {

        $this->setActiveNavigation("grandprize", "Army Ranks", "grandprize_rank");

        $army_rank_list = $this->armyrank_model->getArmyRankList();

        $this->data['army_rank_list'] = $army_rank_list;

        $this->layout->build('rank.tpl', $this->data);
    }

    public function detail() {
        $grandprize_id = $this->input->get('id');

        $grandprize = $this->armyrank_model->getGrandPrizeDetail($grandprize_id);
        $response = array(
            "rcode" => 1,
            "grandprize" => $grandprize
        );

        $this->layout->build_json($response);
    }

    public function save_rank() {

        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'title',
                        'label' => 'Title',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'required_download',
                        'label' => 'Required Download',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $rank_id = $this->input->post("rank_id");
            $title = $this->input->post("title");
            $required_download = $this->input->post("required_download");
            $bonus_point = $this->input->post("bonus_point");
            if (empty($bonus_point)) { $bonus_point = 0; }

            $response = array();
            $sqlRes = false;

            if (empty($rank_id)) {
                //do insert
                $rank_id = round(microtime(true) * 1000);
                $sqlRes = $this->armyrank_model->insertRank($rank_id, $title, $required_download, $bonus_point);
            } else {
                //do update
                $sqlRes = $this->armyrank_model->updateRank($rank_id, $title, $required_download, $bonus_point);
            }

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "rank_id" => $rank_id
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function update_thumbnail() {

        $this->load->helper('upload_file_helper');

        $rank_id = $this->input->post("rank_id");
        $target_path = $this->config->item('thumbnail_path');

        $response = array();

        try {

            //thanks for the helper
            $newFileName = upload_file($_FILES, $target_path);
            $sqlRes = $this->armyrank_model->update_thumbnail($rank_id, $newFileName);
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "target_path" => $target_path . $newFileName,
                    "message" => "Yuhuu..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Error Man!!! Try again please"
                );
            }
        } catch (Exception $e) {
            $response = array(
                "rcode" => 0,
                "message" => $e->getMessage()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

}
