<?php

/**
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @version 		2.0
 * 
 * */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Grandprize_list extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('grandprize_model', 'armyrank_model'));
    }

    public function index() {

        $this->setActiveNavigation("grandprize", "Grand Prize", "grandprize_list");

        $grandprize_list = $this->grandprize_model->getGrandPrizeList();
        $army_rank_list = $this->armyrank_model->getArmyRankList();

        $this->data['grandprize_list'] = $grandprize_list;
        $this->data['army_rank_list'] = $army_rank_list;

        $this->layout->build('list.tpl', $this->data);
    }

    public function detail() {
        $grandprize_id = $this->input->get('id');

        $grandprize = $this->grandprize_model->getGrandPrizeDetail($grandprize_id);
        $response = array(
            "rcode" => 1,
            "grandprize" => $grandprize
        );

        $this->layout->build_json($response);
    }

    public function save_grandprize() {

        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'title',
                        'label' => 'Title',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'rank_id',
                        'label' => 'Required Rank',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $grandprize_id = $this->input->post("grandprize_id");
            $title = $this->input->post("title");
            $description = $this->input->post("description");
            $stock_unit = !empty($this->input->post("stock_unit")) ? $this->input->post("stock_unit") : 0;
            $total_stock = !empty($this->input->post("total_stock")) ? $this->input->post("total_stock") : 0;
            $order_no = !empty($this->input->post("order_no")) ? $this->input->post("order_no") : 0;
            $rank_id = $this->input->post("rank_id");

            $response = array();
            $sqlRes = false;

            if (empty($grandprize_id)) {
                //do insert
                $grandprize_id = round(microtime(true) * 1000);
                $sqlRes = $this->grandprize_model->insertGrandPrize($grandprize_id, 1, $title, $description, $order_no, $stock_unit, $rank_id, $total_stock);
            } else {
                //do update
                $sqlRes = $this->grandprize_model->updateGrandPrize($grandprize_id, 1, $title, $description, $order_no, $stock_unit, $rank_id, $total_stock);
            }

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "grandprize_id" => $grandprize_id
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function update_thumbnail() {

        $this->load->helper('upload_file_helper');

        $grandprize_id = $this->input->post("grandprize_id");
        $target_path = $this->config->item('thumbnail_path');

        $response = array();

        try {

            $newFileName = upload_file($_FILES, $target_path);
            $sqlRes = $this->grandprize_model->update_thumbnail($grandprize_id, $newFileName);
            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "target_path" => $target_path,
                    "message" => "Yuhuu..."
                );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Error update thumbnail"
                );
            }
        } catch (RuntimeException $e) {
            $response = array(
                "rcode" => 0,
                "message" => $e->getMessage()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function edit_form() {


        $this->layout->build('form.tpl', $this->data);
    }

}
