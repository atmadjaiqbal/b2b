<?php

/** 
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @version 		2.0
 * 
 **/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Index extends UI_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        
        self::grandprize_list();
    }
    
    public function grandprize_list() {
//        die("what the actual fuck??");
        $this->setActiveNavigation("report", "Grand Prize", "grandprize_list");
        
        $this->layout->build('list.tpl', $this->data);
    }
}

