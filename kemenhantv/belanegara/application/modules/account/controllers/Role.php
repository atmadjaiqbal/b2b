<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Role extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('role_model'));
    }

    /**
     * Role List
     */
    public function index() {


        $this->setActiveNavigation("account", "Role", "role");

        $role_list = $this->role_model->getRoleList();

        $this->data['role_list'] = $role_list;

        $this->layout->build('role_list.tpl', $this->data);
    }
    
    public function save_role() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
            array(
                array(
                    'field' => 'new_role_id',
                    'label' => 'Role ID',
                    'rules' => 'trim|required'
                    ),
                array(
                    'field' => 'role_name',
                    'label' => 'Role Name',
                    'rules' => 'trim|required'
                    )
                )
            );

        if ($this->form_validation->run()) {

            $role_id = $this->input->post("role_id");
            $new_role_id = $this->input->post("new_role_id");
            $role_name = $this->input->post("role_name");
            $role_privilege = $this->input->post("role_privilege");
            
            $response = array();
            $sqlRes = false;

            $exist = $this->role_model->getRoleList($new_role_id);
            if ($exist && ($new_role_id != $role_id)) {
                $response = array(
                    "rcode" => 0,
                    "message" => "Role ID already exist. Try another one.",
                    "role_id" => $role_id
                    );
            } else {

                if (empty($role_id)) {
                //do insert
                    $sqlRes = $this->role_model->insertRole($new_role_id, $role_name);
                } else {
                //do update
                    $sqlRes = $this->role_model->updateRole($role_id, $new_role_id, $role_name);
                }

                if ($sqlRes) {

                //update role privilege
                    $this->role_model->updateRolePrivilege($new_role_id, $role_privilege);

                    $response = array(
                        "rcode" => 1,
                        "message" => "Yeaahh...",
                        "role_id" => $new_role_id
                        );
                } else {
                    $response = array(
                        "rcode" => 0,
                        "message" => "Something's wrong... Try again please!"
                        );
                }
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
                );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }
    
    
    public function role_privilege() {

        $this->load->model(array('privilege_model'));
        
        $role_id = $this->input->get('role_id');

        $role = NULL;
        $role_privileges = array();
        $privilege_list = $this->privilege_model->getPrivilegeList();

        if (!empty($role_id)) {
            $role = $this->role_model->getRoleList($role_id);
            $role_privileges = $this->role_model->getRolePrivilege($role_id);
        }
        

        $privilege_group = array();

        foreach ($privilege_list as $item) {

            foreach($role_privileges as $role_privilege) {
                if ($item->privilege_id == $role_privilege->privilege_id) {
                    $item->checked = TRUE;
                }
            }

            if (!in_array(strtoupper($item->privilege_group), $privilege_group)) {
                $privilege_group[strtoupper($item->privilege_group)][] = $item;
            }
        }

        $this->setActiveNavigation("account", "Role Privilege", "role");
        $this->data['privilege_list'] = $privilege_list;
        $this->data['privilege_group'] = $privilege_group;
        $this->data['role'] = $role;


        $this->layout->build('role_privilege.tpl', $this->data);
        
    }

    public function delete() {
        $role_id = $this->input->post('role_id');

        $sqlRes = false;
        $response = array();

        if (empty($role_id)) {
            $response = array(
                "rcode" => 0,
                "message" => "Incomplete parameters : role_id"
                );
        } else {
            $sqlRes = $this->role_model->deleteRole($role_id);

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        }

        

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));

    }

}
