<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Group extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('account_group_model'));
    }

    /**
     * Role List 
     */
    public function index() {

        $group_list = $this->account_group_model->getGroupList();

        $this->setActiveNavigation("account", "Group", "account_group");
        $this->data['group_list'] = $group_list;

        $this->layout->build('group_list.tpl', $this->data);
    }

    public function account_group() {
        $group_id = $this->input->get("group_id");
        $group = NULL;

        if (!empty($group_id)) {
            $group = $this->account_group_model->getGroupList($group_id);
            
        }

        $account_list = $this->account_group_model->getGroupAccountByAccount($group_id);

        $this->setActiveNavigation("account", "Group", "account_group");

        $this->data['group'] = $group;
        $this->data['account_list'] = $account_list;


        $this->layout->build('account_group.tpl', $this->data);
    }
    
    public function save_group() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
            array(
                array(
                    'field' => 'group_name',
                    'label' => 'Group Name',
                    'rules' => 'trim|required'
                    )
                )
            );

        if ($this->form_validation->run()) {

            $group_id = $this->input->post("group_id");
            $group_name = $this->input->post("group_name");
            $account_group = $this->input->post("account_group");
            
            $response = array();
            $sqlRes = false;

            
            if (empty($group_id)) {
            //do insert
                $group_id = uniqid(rand(), false);
                $sqlRes = $this->account_group_model->insertGroup($group_id, $group_name);
            } else {
            //do update
                $sqlRes = $this->account_group_model->updateGroup($group_id, $group_name);
            }

            if ($sqlRes) {

                $this->account_group_model->updateAccountGroup($group_id, $account_group);

                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "group_id" => $group_id
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
                );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function remove_group() {
        $group_id = $this->input->post("group_id");

        $response = array();
        $sqlRes = false;

        if (empty($group_id)) {
            $response = array(
                "rcode" => 0,
                "message" => "Incomplete parameters : group_id"
                );
        } else {
            $sqlRes = $this->account_group_model->removeGroup($group_id);

            if ($sqlRes) {

                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh...",
                    "group_id" => $group_id
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        }

        $this->layout->build_json($response);
    }

}
