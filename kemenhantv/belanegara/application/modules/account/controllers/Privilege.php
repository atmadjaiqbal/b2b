<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

/**
 * Description of Role
 *
 * @author Ichalz Break
 */
class Privilege extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('privilege_model'));
    }

    /**
     * Role List
     */
    public function index() {
        
        $this->setActiveNavigation("account", "Role", "privilege");

        $privilege_list = $this->privilege_model->getPrivilegeList();

        $this->data['privilege_list'] = $privilege_list;

        $this->layout->build('privilege_list.tpl', $this->data);
    }
    
    public function save_privilege() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'new_privilege_id',
                        'label' => 'Privilege ID',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'privilege_name',
                        'label' => 'Privilege Name',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $privilege_id = $this->input->post("privilege_id");
            $new_privilege_id = $this->input->post("new_privilege_id");
            $privilege_name = $this->input->post("privilege_name");
            $privilege_group = $this->input->post("privilege_group");
            
            $response = array();
            $sqlRes = false;

            //check priviledge id
            $exist = $this->privilege_model->getPrivilegeList($new_privilege_id);
            if ($exist && ($new_privilege_id != $privilege_id)) {
                $response = array(
                    "rcode" => 0,
                    "message" => "Privilege ID already exist. Try another one.",
                    "prviledge_id" => $new_privilege_id
                );
            } else {
                if (empty($privilege_id)) {
                //do insert
                    $sqlRes = $this->privilege_model->insertPrivilege($new_privilege_id, $privilege_name, $privilege_group);
                } else {
                //do update
                    $sqlRes = $this->privilege_model->updatePrivilege($new_privilege_id, $privilege_id, $privilege_name, $privilege_group);
                }

                if ($sqlRes) {
                    $response = array(
                        "rcode" => 1,
                        "message" => "Yeaahh...",
                        "prviledge_id" => $new_privilege_id
                        );
                } else {
                    $response = array(
                        "rcode" => 0,
                        "message" => "Something's wrong... Try again please!"
                        );
                }
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

}
