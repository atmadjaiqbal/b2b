<?php

/**
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @version 		2.0
 * 
 * */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account_list extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('account_model', 'role_model'));
    }

    public function index() {

        $this->setActiveNavigation("account", "Account", "account_list");

        $account_list = $this->account_model->getAccountList();

        $this->data['account_list'] = $account_list;

        $this->layout->build('list.tpl', $this->data);
    }

    public function form() {
        $this->load->model(array('pusdiklat_model', 'institution_model'));

        $this->setActiveNavigation("account", "Account", "account_list");

        $account_id = $this->input->get("account_id");
        $account = NULL;

        if (!empty($account_id)) {
            $account = $sqlRes = $this->account_model->getAccountDetail($account_id);
        }
        
        $pusdiklat_list = $this->pusdiklat_model->getList();
        $institution_list = $this->institution_model->getInstitutionList();

        $role_list = $this->role_model->getRoleList();
        $account_roles = $this->account_model->getAccountRoles($account_id);

        foreach ($role_list as $role) {
            foreach($account_roles as $account_role) {
                if ($role->role_id == $account_role->role_id) {
                    $role->checked = TRUE;
                }
            }
        }
        
        $this->data['pusdiklat_list'] = $pusdiklat_list;
        $this->data['institution_list'] = $institution_list;
        $this->data['account'] = $account;
        $this->data['role_list'] = $role_list;
        $this->data['account_roles'] = $account_roles;
        
        $this->layout->build('account_form.tpl', $this->data);
    }

    public function account_list() {
        $this->setActiveNavigation("account", "Account", "account_list");

        $account_list = $this->account_model->getAccountList();

        $this->data['account_list'] = $account_list;

        $this->layout->build('list.tpl', $this->data);
    }

    public function detail() {
        $account_id = $this->input->post("account_id");

        $response = array(
            'rcode' => 0,
            'message' => 'Incomplete Arguments : account_id'
        );

        if (empty($account_id)) {
            $customer = $sqlRes = $this->account_model->getAccountDetail($account_id);
        }
    }

    public function account_save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'user_id',
                        'label' => 'User ID',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'user_password',
                        'label' => 'Password',
                        'rules' => 'trim|required|min_length[6]'
                    ),
                    array(
                        'field' => 'user_name',
                        'label' => 'Name',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'user_email',
                        'label' => 'Email',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $account_id = $this->input->post("account_id");
            $user_id = $this->input->post("user_id");
            $user_password = $this->input->post("user_password");
            $user_name = $this->input->post("user_name");
            $user_email = $this->input->post("user_email");
            $user_phone = $this->input->post("user_phone");
            $pusdiklat_id = $this->input->post("pusdiklat_id");
            $institution_id = $this->input->post("institution_id");
            $status_id = 1;

            $response = array();
            $sqlRes = false;

            $userIDExist = $this->account_model->getAccountByUserIDorEmail($user_id);
            if ($userIDExist) {
                $response = array(
                    "rcode" => 0,
                    "message" => "User ID already exist, try another one please"
                );
            } else {
                if (empty($account_id)) {
                    //do insert
                    $account_id = uniqid();
                    $sqlRes = $this->account_model->insertAccount($account_id, $user_id, $user_password, $user_name, $user_email, $user_phone, $status_id, $pusdiklat_id, $institution_id);
                } else {
                    //do update
                    $sqlRes = $this->account_model->updateAccount($account_id, $user_id, $user_name, $user_email, $user_phone, $status_id, $pusdiklat_id, $institution_id);
                }

                if ($sqlRes) {
                    $response = array(
                        "rcode" => 1,
                        "message" => "Yeaahh...",
                        "account_id" => $account_id
                    );
                } else {
                    $response = array(
                        "rcode" => 0,
                        "message" => "Something's wrong... Try again please!"
                    );
                }
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function save() {
        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'user_name',
                        'label' => 'Name',
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'user_email',
                        'label' => 'Email',
                        'rules' => 'trim|required'
                    )
                )
        );

        if ($this->form_validation->run()) {

            $account_id = $this->input->post("account_id");
            $user_name = $this->input->post("user_name");
            $user_email = $this->input->post("user_email");
            $user_phone = $this->input->post("user_phone");
            $account_role = $this->input->post("account_role");
            $pusdiklat_id = $this->input->post("pusdiklat_id");
            $institution_id = $this->input->post("institution_id");

            if (empty($pusdiklat_id)) {
                $pusdiklat_id = NULL;
            }

            if (empty($institution_id)) {
                $institution_id = NULL;
            }

            $response = array();

            //fetch account
            $account = $this->account_model->getAccountDetail($account_id);
            if ($account) {
                //update account information
                //$account_id, $user_id, $user_name, $user_email, $user_phone, $status_id
                $sqlResAccount = $this->account_model->updateAccount($account_id, $account->user_id, $user_name, $user_email, $user_phone, $account->status_id, $pusdiklat_id, $institution_id);
                //update roles
                $sqlResRoles = $this->account_model->updateAccountRoles($account_id, $account_role);

                if ($sqlResAccount && $sqlResRoles) {
                    $response = array(
                        "rcode" => 1,
                        "message" => "Yeaahh...",
                        "account_id" => $account_id
                    );
                } else {
                    $response = array(
                        "rcode" => 0,
                        "message" => "Something's wrong... Try again please!"
                    );
                }
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Account not found"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function delete() {
        $account_id = $this->input->post('account_id');

        $sqlRes = false;
        $response = array();

        if (empty($account_id)) {
            $response = array(
                "rcode" => 0,
                "message" => "Incomplete parameters : account_id"
                );
        } else {
            $sqlRes = $this->account_model->deleteAccount($account_id);

            if ($sqlRes) {
                $response = array(
                    "rcode" => 1,
                    "message" => "Yeaahh..."
                    );
            } else {
                $response = array(
                    "rcode" => 0,
                    "message" => "Something's wrong... Try again please!"
                    );
            }
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));

    }

}
