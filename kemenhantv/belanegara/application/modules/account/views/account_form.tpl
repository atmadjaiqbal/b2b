<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="{assets_path('js/iCheck/square/blue.css')}" media="screen"/>

{js("iCheck/iCheck.min.js")}

<script type="text/javascript">

    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }
    
    $(document).ready(function () {

        setupViewValues();

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $('#pusdiklat_id').select2();
        $('#institution_id').select2();

        $('#btnRemove').click(function () {
            var conf = confirm('Are you sure want to delete item?');
            if (!conf) return;


            $(this).attr('disabled', true);
            clearAlert();

            var url = "{base_url('account/account_list/delete')}";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#theForm').serialize(),
                success: function (resp) {
                    $('#btnRemove').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#role_id').val(resp.role_id);

                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);

                        location.href = '{base_url("account/account_list/")}'
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btnRemove').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(jqXHR.responseText);
                }
            });
        });

        $('#theFormSubmit').click(function () {
            $(this).attr('disabled', true);
            clearAlert();

            var url = "{base_url('account/account_list/save')}";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#theForm').serialize(),
                success: function (resp) {
                    $('#theFormSubmit').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#theFormSubmit').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(jqXHR.responseText);
                }
            });
        });

        function setupViewValues() {

            $('#account_id').val('{$account->account_id}');
            $('#user_id').val('{$account->user_id}');
            $('#user_email').val('{$account->user_email}');
            $('#user_phone').val('{$account->user_phone}');
            $('#user_name').val('{$account->user_name}');
            $('#pusdiklat_id').val('{$account->pusdiklat_id}');
            $('#institution_id').val('{$account->institution_id}');
        }
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Account Form
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{base_url('account/account_list')}"><i class="fa fa-dashboard"></i> Account</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Account Form</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="account_id" name="account_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="user_id" class="col-sm-3 control-label">User ID</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="user_id" name="user_id" placeholder="User ID" readonly="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_name" class="col-sm-3 control-label">Full Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Full Name"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_phone" class="col-sm-3 control-label">Phone No</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="user_phone" name="user_phone" placeholder="Phone No"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="pusdiklat_id" class="col-sm-3 control-label">Pusdiklat</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="pusdiklat_id" name="pusdiklat_id">
                                                    <option value="">-- Choose Pusdiklat --</option>
                                                    {foreach $pusdiklat_list as $item}
                                                    <option value="{$item->pusdiklat_id}">{$item->pusdiklat_name} - {$item->province_name}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="institution_id" class="col-sm-3 control-label">Institution</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="institution_id" name="institution_id">
                                                    <option value="">-- Choose Institution --</option>
                                                    {foreach $institution_list as $item}
                                                    <option value="{$item->institution_id}">{$item->institution_alias} - {$item->institution_name}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="account_role" class="col-sm-3 control-label">Role</label>
                                            <div class="col-sm-9">
                                                {foreach $role_list as $item}
                                                <div class="checkbox icheck">
                                                    <label><input type="checkbox" id="account_role_{$item->role_id}" name="account_role[]" value="{$item->role_id}" {if $item->checked}checked{/if}> {$item->role_name}</label>
                                                </div>
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="{base_url('account/account_list')}"><button type="button" class="btn btn-default">Go Back</button></a>
                        <div class="pull-right">
                            <button type="button" id="btnRemove" class="btn btn-danger">Remove</button>
                            <button type="button" id="theFormSubmit" class="btn btn-info">Submit</button>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
