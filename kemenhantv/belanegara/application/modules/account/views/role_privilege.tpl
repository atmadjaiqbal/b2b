<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="{assets_path('js/iCheck/square/blue.css')}" media="screen"/>

{js("iCheck/iCheck.min.js")}

<script type="text/javascript">

$(document).ready(function () {

    setupViewValues();

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
    });

    
    $('#btnRemove').click(function () {
        var conf = confirm('Are you sure want to delete item?');
        if (!conf) return;


        $(this).attr('disabled', true);
        clearAlert();

        var url = "{base_url('account/role/delete')}";

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: $('#theForm').serialize(),
            success: function (resp) {
                $('#btnRemove').removeAttr('disabled');
                if (resp.rcode === 1) {
                    $('#role_id').val(resp.role_id);

                    $('#response_success').removeClass('hidden');
                    $('#response_success').html(resp.message);

                    location.href = '{base_url("account/role/")}'
                } else {
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(resp.message);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#btnRemove').removeAttr('disabled');
                $('#response_error').removeClass('hidden');
                $('#response_error').html(jqXHR.responseText);
            }
        });
    });

    $('#theFormSubmit').click(function () {
        $(this).attr('disabled', true);
        clearAlert();

        var url = "{base_url('account/role/save_role')}";

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: $('#theForm').serialize(),
            success: function (resp) {
                $('#theFormSubmit').removeAttr('disabled');
                if (resp.rcode === 1) {
                    $('#role_id').val(resp.role_id);

                    $('#response_success').removeClass('hidden');
                    $('#response_success').html(resp.message);

                    location.href = '{base_url("account/role/")}'
                } else {
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(resp.message);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#theFormSubmit').removeAttr('disabled');
                $('#response_error').removeClass('hidden');
                $('#response_error').html(jqXHR.responseText);
            }
        });
    });

function clearAlert() {
    $('#response_error').addClass('hidden');
    $('#response_success').addClass('hidden');
}

function setupViewValues() {
    $('#role_id').val('{$role->role_id}');
    $('#new_role_id').val('{$role->role_id}');
    $('#role_name').val('{$role->role_name}');
}
});
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Role Privilege
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{base_url('account/role')}"><i class="fa fa-dashboard"></i> role</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Role Privilege</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <input type="hidden" id="role_id" name="role_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="new_role_id" class="col-sm-2 control-label">Role ID</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="new_role_id" name="new_role_id" placeholder="Role ID"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="role_name" class="col-sm-2 control-label">Role Name</label>
                                            <div class="col-sm-10">
                                                <input type="role_name" class="form-control" id="role_name" name="role_name" placeholder="Role Name"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                        <div class="form-group">
                                            <label for="role_privilege" class="col-sm-2 control-label">Privilege</label>
                                            <div class="col-sm-10">
                                                {foreach $privilege_group as $item}
                                                <label for="privilege_group" class="control-label">{$item@key}</label>
                                                <div class="col-sm-offset-1">
                                                    {foreach $item as $single_item}
                                                    <div class="checkbox icheck">
                                                        <label><input type="checkbox" id="role_privilege_{$single_item->role_id}" name="role_privilege[]" value="{$single_item->privilege_id}" {if $single_item->checked}checked{/if}>  {$single_item->privilege_name}</label>
                                                    </div>
                                                    {/foreach}
                                                </div>
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="{base_url('account/role')}"><button type="button" class="btn btn-default">Go Back</button></a>
                        <div class="pull-right">
                            <button type="button" id="btnRemove" class="btn btn-danger">Remove</button>
                            <button type="button" id="theFormSubmit" class="btn btn-info">Submit</button>
                        </div>
                        
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->