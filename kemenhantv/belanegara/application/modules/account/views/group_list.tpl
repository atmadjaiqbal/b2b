<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Group
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Group</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Group</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Group Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $group_list as $item}
                                    <tr role="row">
                                        <td>{$item->group_name}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-default" name="btnEdit" data-id="{$item->group_id}" data-name="{$item->group_name}">
                                                    <i class="fa fa-edit"> Edit</i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Role Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="role_id" name="role_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="role_id" class="col-sm-2 control-label">Role ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="new_role_id" name="new_role_id" placeholder="role_id"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="role_name" class="col-sm-2 control-label">Role Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="role_name" name="role_name" placeholder="role_name"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error">
                            </div>
                            <div class="callout callout-success hidden" id="response_succeed">
                                <p id="successResponse"></p>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

        $('#btnCreate').click(function () {
            location.href = '{base_url("account/group/account_group")}';
        });

        $('button[name="btnPriviledge"').on('click', function() {
            var role_id = $(this).attr('data-id');

            location.href = '{base_url("account/role/role_privilege")}?role_id=' + role_id;

        });

        $('button[name="btnEdit"]').on('click', function () {
            var data_id = $(this).attr('data-id');

            if (data_id === '')
                return;

            location.href = '{base_url("account/group/account_group")}?group_id=' + data_id;
            return;

            clearModalForm();

            $('#layoutModal').modal('show');

            $('#role_id').val(role_id);
            $('#role_name').val(role_name);
            $('#new_role_id').val(role_id);
        });

        $('#btnSubmitForm').click(function () {

            prepareSubmit();

            var url = "role/save_role";

            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#main_form').serialize(),
                success: function (resp) {
                    console.log('resp:' + resp.rcode);
                    if (resp.rcode === 1) {
                        $('#role_id').val(resp.role_id);

                        $('#response_succeed').html(resp.message);
                        $('#response_succeed').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');

                        location.reload();
                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(errorThrown);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function clearModalForm() {
            $('#btnSubmitForm').removeAttr("disabled");

            $('#role_id').val('');
            $('#new_role_id').val('');
            $('#role_name').val('');

            hideErrorMessage();
            hideProgressBar();
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

    });
</script>