<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="{assets_path('js/iCheck/square/blue.css')}" media="screen"/>

{js("iCheck/iCheck.min.js")}

<script type="text/javascript">

$(document).ready(function () {

    setupViewValues();

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

    $('#btnRemove').click(function () {
        $(this).attr('disabled', true);
        clearAlert();

        var url = "{base_url('account/group/remove_group')}";

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: $('#theForm').serialize(),
            success: function (resp) {
                $('#btnRemove').removeAttr('disabled');
                if (resp.rcode === 1) {
                    $('#group_id').val(resp.group_id);

                    $('#response_success').removeClass('hidden');
                    $('#response_success').html(resp.message);

                    location.href = "{base_url('account/group/')}";
                } else {
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(resp.message);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#btnRemove').removeAttr('disabled');
                $('#response_error').removeClass('hidden');
                $('#response_error').html(jqXHR.responseText);
            }
        });
    });

    $('#theFormSubmit').click(function () {
        $(this).attr('disabled', true);
        clearAlert();

        var url = "{base_url('account/group/save_group')}";

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: $('#theForm').serialize(),
            success: function (resp) {
                $('#theFormSubmit').removeAttr('disabled');
                if (resp.rcode === 1) {
                    $('#group_id').val(resp.group_id);

                    $('#response_success').removeClass('hidden');
                    $('#response_success').html(resp.message);
                } else {
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(resp.message);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#theFormSubmit').removeAttr('disabled');
                $('#response_error').removeClass('hidden');
                $('#response_error').html(jqXHR.responseText);
            }
        });
    });

function clearAlert() {
    $('#response_error').addClass('hidden');
    $('#response_success').addClass('hidden');
}

function setupViewValues() {
    {if $group}
    $('#group_id').val('{$group->group_id}');
    $('#group_name').val('{$group->group_name}');
    {/if}
}
});
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Account Group
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{base_url('account/group')}"><i class="fa fa-dashboard"></i> Group</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Account Group</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <form class="form-horizontal" id="theForm">
                                <div class="col-md-6">
                                    <div class="box-body">
                                        <input type="hidden" id="group_id" name="group_id"/>
                                        <div class="form-group">
                                            <label for="group_name" class="col-sm-4 control-label">Group Name</label>
                                            <div class="col-sm-8">
                                                <input type="group_name" class="form-control" id="group_name" name="group_name" placeholder="group_name"/>
                                            </div>
                                        </div>
                                        <div class="box-footer"></div><!-- /.box-footer -->
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="group_name" class="control-label">Account</label>
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div class="checkbox icheck">
                                                        <label><input type="checkbox" class="check_all" id="check_all" indeterminta="true">&nbsp;</label>
                                                    </div>
                                                </th>
                                                <th>User ID</th>
                                                <th>Email</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach $account_list as $item}
                                                <tr role="row">
                                                    <td>
                                                        <div class="checkbox icheck">
                                                            <input type="checkbox" class="check_single" id="customer_check_{$item->account_id}" name="account_group[]" value="{$item->account_id}" {if $item->checked_group_id}checked{/if}>
                                                        </div>
                                                    </td>
                                                    <td>{$item->user_id}</td>
                                                    <td>{$item->user_email}</td>
                                                    <td>{$item->user_name}</td>
                                                    <td>{$item->user_phone}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-default" name="btnEdit" data-id="{$item->account_id}" data-name="{$item->user_name}">
                                                                <i class="fa fa-align-left"> Edit</i>
                                                                {*                                                    <span class="fa fa-edit"></span> Edit*}
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="callout callout-danger hidden" id="response_error"></div>
                    <div class="callout callout-success hidden" id="response_success"></div>
                    <div class="box-footer">
                        <a href="{base_url('account/group')}"><button type="button" class="btn btn-default">Go Back</button></a>
                        <div class="pull-right">
                            <button type="button" id="btnRemove" class="btn btn-danger">Remove</button>
                            <button type="button" id="theFormSubmit" class="btn btn-info">Submit</button>
                        </div>
                        
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->