<script type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

    });

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Account
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Account</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Account</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnCreate">
                                <span class="fa fa-plus"></span> Create
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $account_list as $account}
                                    <tr role="row">
                                        <td>{$account->user_id}</td>
                                        <td>{$account->user_email}</td>
                                        <td>{$account->user_name}</td>
                                        <td>{$account->user_phone}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default" name="btnEdit" data-id="{$account->account_id}" data-name="{$account->user_name}">
                                                    <i class="fa fa-align-left"> Edit</i>
                                                    {*                                                    <span class="fa fa-edit"></span> Edit*}
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

    <div class="modal" id="layoutModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Account Form</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="main_form">
                        <input type="hidden" id="account_id" name="account_id"/>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="user_id" class="col-sm-2 control-label">User ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="user_id" name="user_id" placeholder="User ID"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="user_name" class="col-sm-2 control-label">Full Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Full Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="user_phone" class="col-sm-2 control-label">Phone No</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="user_phone" name="user_phone" placeholder="Phone No"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="user_password" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Password"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="verify_password" class="col-sm-2 control-label">Verify Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="verify_password" name="verify_password" placeholder="Verify Password"/>
                                </div>
                            </div>
                            <div class="callout callout-danger hidden" id="response_error"></div>
                            <div class="callout callout-success hidden" id="response_succes"><p id="successResponse"></p></div>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <div class="progress hidden" id="progressModal">
                    <span class="sr-only">80%</span>
                    <div class="progress-bar progress-bar-red" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100">   
                        20%
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitForm">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {

        $('#btnCreate').click(function () {
            clearForm();
            $('#layoutModal').modal('show');
        });

        $('button[name="btnEdit"]').on('click', function () {
            var account_id = $(this).attr('data-id');

            location.href = '{base_url("account/account_list/form?account_id=")}' + account_id;
            return;

            if (account_id === '')
                return;

            clearForm();

            $('#layoutModal').modal('show');
            $('#response_error').html('Please wait...');
            $('#response_error').removeClass('hidden');
            $('#btnSubmitForm').attr("disabled", "true");

            $.ajax({
                url: 'account_list/detail?id=' + account_id,
                success: function (resp) {
                    hideErrorMessage();
                    $('#btnSubmitForm').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        var grandprize = resp.grandprize;
                        $('#grandprize_id').val(grandprize.grandprize_id);
                        $('#title').val(grandprize.title);
                        $('#description').text(grandprize.description);
                        $('#order_no').val(grandprize.order_no);
                        $('#rank_id').val(grandprize.rank_id);
                        $('#total_stock').val(grandprize.total_stock);
                        $('input[name="stock_unit"]').val(grandprize.stock_unit);

                        original_stock = grandprize.total_stock;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('Error fetch detail:' + errorThrown);
                    $('#response_error').html("Oppzz.. Something's wrong. Check your internet please");
                }
            })
        });

        $('#btnSubmitForm').click(function () {
            var url = "account_list/account_save";
            
            if ($('#user_password').val() !== $('#verify_password').val()) {
                $('#response_error').html("Verify password doesn't match. Please make sure they are match.");
                $('#response_error').removeClass('hidden');
                return;
            }
            
            prepareSubmit();
            
            $.ajax({
                type: "post",
                dataType: "json",
                url: url,
                data: $('#main_form').serialize(),
                success: function (resp) {
                    console.log('resp:' + resp.rcode);
                    if (resp.rcode === 1) {
                        $('#account_id').val(resp.account_id);

                        $('#response_success').html(resp.message);
                        $('#response_success').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                        location.reload();

                    } else {
                        $('#response_error').html(resp.message);
                        $('#response_error').removeClass('hidden');
                        $('#btnSubmitForm').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#response_error').html(jqXHR.responseText);
                    $('#response_error').removeClass('hidden');
                    $('#btnSubmitForm').removeAttr('disabled');
                }
            });
        });

        function uploadThumbnail(grandprize_id) {
            $('#progressModal').removeClass('hidden');

            var url = "{base_url('/grandprize/grandprize_list/update_thumbnail')}";

            var xhr = new XMLHttpRequest();
            (xhr.upload || xhr).addEventListener('progress', function (e) {
                var done = e.loaded;
                var total = e.total;
                var percent = Math.round(done / total * 100) + '%';
                $('#progressBar').width(percent);
                $('#progressBar').text(percent);
            });

            xhr.addEventListener('load', function (e) {
                $('#btnSubmitForm').removeAttr('disabled');
                console.log(this.responseText);
                var resp = JSON.parse(this.responseText);
                if (resp.rcode === 1) {
                    location.reload();
                } else {
                    $('#response_error').html(resp.message);
                    $('#response_error').removeClass('hidden');

                    hideProgressBar();
                }
            });
            xhr.open('post', url, true);

            var data;
            data = new FormData();
            data.append('file', $('#main_form input[name=file_name]')[0].files[0]);
            data.append('grandprize_id', grandprize_id);

            xhr.send(data);
        }

        function clearForm() {
            $('#btnSubmitForm').removeAttr("disabled");

            $('#account_id').val('');
            $('#user_id').val('');
            $('#user_email').val('');
            $('#user_password').val('');
            $('#verify_password').val('');
            $('#user_name').val('');
            $('#user_phone').val('');

            hideErrorMessage();
            hideProgressBar();
        }

        function hideErrorMessage() {
            $('#response_error').html('');
            $('#response_error').addClass('hidden');
        }

        function hideProgressBar() {
            $('#progressModal').addClass('hidden');
            $('#progressBar').width(0);
            $('#progressBar').text(0);
        }

        function prepareSubmit() {
            $('#btnSubmitForm').attr("disabled", "true");
            hideErrorMessage();
        }

    });
</script>