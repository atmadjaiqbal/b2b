<script type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

    });

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            History
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">History</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Army History List</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Army Code</th>
                                    <th>Army User ID</th>
                                    <th>Army Name</th>
                                    <th>Mobile ID</th>
                                    <th>Point</th>
                                    <th>Reward</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $history_list as $history}
                                    <tr>
                                        <td>{$history->log_date}</td>
                                        <td>{$history->referral_number}</td>
                                        <td>{$history->army_user_id}</td>
                                        <td>{$history->display_name}</td>
                                        <td>{$history->mobile_id}</td>
                                        <td>{$history->point_value}</td>
                                        <td>{$history->reward_value}</td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
