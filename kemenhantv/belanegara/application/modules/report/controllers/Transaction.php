<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of transaction
 *
 * @author Ichalz Break
 */
class Transaction extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('transaction_model', 'armyuser_model'));
    }

    public function index() {
        $this->setActiveNavigation("report", "Transaction", "transaction");

        $transaction_list = $this->transaction_model->getTransactionList();

        $this->data['transaction_list'] = $transaction_list;

        $this->layout->build('transaction.tpl', $this->data);
    }

    public function form() {
        $this->setActiveNavigation("report", "Transaction", "transaction");

//        $transaction_list = $this->transaction_model->getTransactionList();
//        
//        $this->data['transaction_list'] = $transaction_list;

        $this->layout->build('transaction_form.tpl', $this->data);
    }

    public function form_user_detail() {
        $search_key = $this->input->get('search_key');

        $resp = array();

        if (empty($search_key)) {
            $resp = array(
                "rcode" => 0,
                "messasge" => "Please enter search key"
            );
        } else {

            $account = $this->armyuser_model->getArmyUser($search_key);

            if ($account) {
                $resp = array(
                    "rcode" => 1,
                    "account" => $account
                );
            } else {
                $resp = array(
                    "rcode" => 0,
                    "message" => "Army not found. Try again using another search."
                );
            }
        }

        $this->layout->build_json($resp);
    }

    public function transaction_submit() {

        $this->load->library(array('form_validation'));

        $this->form_validation->set_rules(
                array(array(
                        'field' => 'referral_id',
                        'label' => 'Army ID',
                        'rules' => 'trim|required'),
                    array(
                        'field' => 'amount',
                        'label' => 'Amount',
                        'rules' => 'trim|required'),
                    array(
                        'field' => 'transaction_no',
                        'label' => 'Transaction Number',
                        'rules' => 'trim|required')));

        if ($this->form_validation->run()) {

            $referral_id = $this->input->post('referral_id');
            $amount = $this->input->post('amount');
            $transaction_number = $this->input->post('transaction_no');

            //fetch army user
            $armyUser = $this->armyuser_model->getArmyUser("", $referral_id);

            if ($armyUser) {
                //recheck user exist

//                if (empty($armyUser->bank_account_number) || empty($armyUser->bank_account_name)) {
                if (false) {
                    //don't proceed if bank field's empty
                    $response = array(
                        "rcode" => 0,
                        "message" => "This army has not completed their bank information. Can't proceed the transaction"
                    );
                } else {
                    //everything's clear. go ahead
                    //$user_referral_id, $transaction_no, $status, $total_point, $total_reward) {
                    $sqlResp = $this->transaction_model->insertTransaction($referral_id, $transaction_number, 1, 0, $amount);
                    
                    if ($sqlResp) {
                        //good job brow
                        $response = array(
                            "rcode" => 1,
                            "message" => "Well done buddy."
                        );

                        //TODO - Push Notification, email, etc.
                    } else {
                        //what the fuck!!!
                        $response = array(
                            "rcode" => 0,
                            "message" => "Something is wrong brow. Try again please"
                        );
                    }
                }
            } else {
                //why is there no army user given by the specific ID
                $response = array(
                    "rcode" => 0,
                    "message" => "Uppz.. Invalid referral_id"
                );
            }
        } else {
            $response = array(
                "rcode" => 0,
                "message" => validation_errors()
            );
        }

        $this->layout->build_json($response);
    }

}
