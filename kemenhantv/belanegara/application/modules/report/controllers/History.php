<?php

/**
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @version 		2.0
 * 
 * */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class History extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('army_model');
    }

    public function index() {

        $this->setActiveNavigation("report", "History", "report_history");

        $history_list = $this->army_model->getReferenceHistory();

        $this->data['history_list'] = $history_list;

        $this->layout->build('history.tpl', $this->data);
    }

}
