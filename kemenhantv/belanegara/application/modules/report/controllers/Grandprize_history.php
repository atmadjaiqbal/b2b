<?php

/** 
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @version 		2.0
 * 
 **/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Grandprize_history extends UI_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('grandprize_model');
    }
    
    public function index() {
        
        $this->setActiveNavigation("report", "Achievements", "report_grandprize");
        
        $history_list = $this->grandprize_model->getAchievementsHistory();
        
        $this->data['history_list'] = $history_list;
        
        $this->layout->build('grandprize_history.tpl', $this->data);
    }
    
    
}