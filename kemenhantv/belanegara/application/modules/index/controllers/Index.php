<?php

/** 
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @version 		2.0
 * 
 **/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Index extends UI_Controller {
    
    public function __construct() {
        parent::__construct();

        $this->load->model(array('master_model'));
    }
    
    public function index() {
        
        $this->setActiveNavigation("dashboard", "Dashboard");

        $province_list = $this->master_model->getProvinceListDetail();

        $this->data["province_list"] = $province_list;
        
        $this->layout->build('index.tpl', $this->data);
    }
    
    public function error404() {
//        die("what the actual fuck??");
        
        $this->layout->build('errors/html/error_404.tpl', $this->data);
    }
    
}

