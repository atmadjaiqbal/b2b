<script type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

        $('#btnGenerate').click(function () {
            var keyword = '';//$('#keyword').val();
            var date_start = '';//$('#date_start').val();
            var date_end = '';//$('#date_end').val();
            var url = '{base_url()}report/generate/finance?&keyword=' + keyword + '&date_start=' + date_start + '&date_end=' + date_end;
            window.open(url);
        });
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Finance Report
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Finance Report</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Army Finance Report</h3>
                        <div class="box-tools">
                            <button class="btn btn-sm btn-default" id="btnGenerate">
                                <span class="fa fa-line-chart"></span> Generate
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr> 
                                    <th>Army Code</th>
                                    <th>Army User ID</th>
                                    <th>Army Name</th>
                                    <th>Date of Birth</th>
                                    <th>Gender</th>
                                    <th>Bank Account Name</th>
                                    <th>Bank Code</th>
                                    <th>Bank Account Number</th>
                                    <th>Reward Value</th>
                                    <th>Total Download</th>
                                    <th>Total Reward</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $report_list as $report}
                                    <tr>
                                        <td>{$report->referral_number}</td>
                                        <td>{$report->army_user_id}</td>
                                        <td>{$report->display_name}</td>
                                        <td>{$report->bod}</td>
                                        <td>{($report->gender != null && $report->gender == 'M') ? 'Male' : 'Female'}</td>
                                        <td>{$report->bank_account_name}</td>
                                        <td>{$report->bank_code}</td>
                                        <td>{$report->bank_account_number}</td>
                                        <td>{$report->reward_value}</td>
                                        <td>{$report->total_reference}</td>
                                        <td>{$report->reward_total}</td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->