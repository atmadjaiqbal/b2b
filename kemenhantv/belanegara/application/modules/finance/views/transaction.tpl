<script type="text/javascript">
    $(document).ready(function () {

        $('#example2').DataTable();

    });

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaction
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Transaction</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Finance Transaction List</h3>
                        <div class="box-tools">
                            <a class="btn btn-default" href="{base_url('report/transaction/form')}"><i class="fa fa-line-chart"> New Transaction</i></a>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Transaction Date</th>
                                    <th>Army Code</th>
                                    <th>Transaction Number</th>
                                    <th>Total Transfer</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $transaction_list as $transaction}
                                    <tr>
                                        <td>{$transaction->transaction_date}</td>
                                        <td>{$transaction->army_code}</td>
                                        <td>{$transaction->transaction_no}</td>
                                        <td>{$transaction->total_reward}</td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
