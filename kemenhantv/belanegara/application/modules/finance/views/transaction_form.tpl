<script type="text/javascript">


    function clearAlert() {
        $('#response_error').addClass('hidden');
        $('#response_success').addClass('hidden');
    }

    $(document).ready(function () {

        $('#theFormSubmit').click(function () {
            $(this).attr('disabled', true);
            clearAlert();

            var url = "{base_url('report/transaction/transaction_submit')}";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#theForm').serialize(),
                success: function (resp) {
                    $('#theFormSubmit').removeAttr('disabled');
                    if (resp.rcode === 1) {
                        $('#theForm')[0].reset();
                        $('#response_success').removeClass('hidden');
                        $('#response_success').html(resp.message);
                    } else {
                        $('#response_error').removeClass('hidden');
                        $('#response_error').html(resp.message);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#theFormSubmit').removeAttr('disabled');
                    $('#response_error').removeClass('hidden');
                    $('#response_error').html(errorThrown);
                }
            });
        });

        $('#searchBtn').click(function () {
            $('#search_error').addClass('hidden');
            var search_key = $('#search_key').val();
            if (search_key !== '') {
                var url = "{base_url('report/transaction/form_user_detail')}";
                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: url,
                    data: "search_key=" + search_key,
                    success: function (resp) {
                        if (resp.rcode === 1) {
                            var account = resp.account;
                            $('#army_name').val(account.army_name);
                            $('#army_code').val(account.army_code);
                            $('#referral_id').val(account.referral_id);
                            $('#bank_account_number').val(account.bank_account_number);
                            $('#bank_account_name').val(account.bank_account_number);
                        } else {
                            $('#search_error').removeClass('hidden');
                            $('#search_error').html(resp.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#search_error').removeClass('hidden');
                        $('#search_error').html(errorThrown);
                    }
                });
            }
        });
    });
</script>
<style type="text/css">
    .left-content-divider {
        border-right: 1px solid rgb(128, 128, 128);
        border-right: 1px solid rgba(128, 128, 128, .5);
        -webkit-background-clip: padding-box; /* for Safari */
        background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaction
        </h1>
        <ol class="breadcrumb">
            <li><a href="{base_url()}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{base_url('report/transaction')}"><i class="fa fa-dashboard"></i> Transaction</a></li>
            <li class="active">Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Submit New Transaction</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-warning">
                            <div class="col-md-6 left-content-divider">
                                <!-- form header -->
                                <div class="box-header with-border">
                                    <h3 class="box-title">Manual Input</h3>
                                </div>
                                <!-- form start -->
                                <form class="form-horizontal" id="theForm">
                                    <input type="hidden" id="referral_id" name="referral_id"/>
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">&nbsp;</label>
                                            <div class="col-sm-9">
                                                <label><i>Step 1 : Insert army code / user id for the desired customer. </i></label>
                                                <label><i>Step 2 : Click search</i></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="search_key" class="col-sm-3 control-label">Search Key</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="search_key" name="search_key" placeholder="Army Code or User ID">
                                            </div>
                                        </div>
                                        <div class="callout callout-danger hidden" id="search_error"></div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="button" id="searchBtn" class="btn btn-info pull-right">Search</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">&nbsp;</label>
                                            <div class="col-sm-9">
                                                <label><i>Step 3 : Make sure all those 4 fields below are fully generated by the system </i></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="army_name" class="col-sm-3 control-label">Army Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="army_name" name="army_name" disabled="true">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="army_code" class="col-sm-3 control-label">Army Code</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="army_code" name="army_code" disabled="true">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="bank_account_number" class="col-sm-3 control-label">Bank Account Number</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="bank_account_number" name="bank_account_number" disabled="true">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="bank_account_name" class="col-sm-3 control-label">Bank Account Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="bank_account_name" name="bank_account_name" disabled="true">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">&nbsp;</label>
                                            <div class="col-sm-9">
                                                <label><i>Step 4 : Please be careful when insert the data. This info will be viewed by the customer</i> <b>instantly</b>.</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="amount" class="col-sm-3 control-label">Amount</label>
                                            <div class="col-sm-9">
                                                <input type="number" class="form-control" id="amount" name="amount" placeholder="Amounts Transferred" max="100000000">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="transaction_no" class="col-sm-3 control-label">Transaction Number</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="transaction_no" name="transaction_no" placeholder="Should be a Transaction ID / Number from the bank, or whatever." maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">&nbsp;</label>
                                            <div class="col-sm-9">
                                                <label><i>Step 5 : Click Submit</i></label>
                                            </div>
                                        </div>
                                    </div><!-- /.box-body -->
                                    <div class="callout callout-danger hidden" id="response_error"></div>
                                    <div class="callout callout-success hidden" id="response_success"></div>
                                    <div class="box-footer">
                                        <button type="button" id="theFormSubmit" class="btn btn-info pull-right">Submit</button>
                                    </div><!-- /.box-footer -->
                                </form>
                            </div>
                            <div class="col-md-6">
                                <!-- form header -->
                                <div class="box-header with-border">
                                    <h3 class="box-title">Automatic Input (Import from excel)</h3>
                                </div>
                                <!-- form start -->
                                <form class="form-horizontal" id="importForm">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">&nbsp;</label>
                                            <div class="col-sm-9">
                                                <label><i>Click</i> <b>Choose File</b> <i>then select your modified report excel file to import into the system</i></label>
                                                <label><b>Double Check</b> <i>your document before uploading, make sure you supply the correct, responsible, and last-updated one </i></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="search_key" class="col-sm-3 control-label">Report File</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" id="search_key" name="search_key" placeholder="Army Code or User ID">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-info pull-right">Submit</button>
                                    </div><!-- /.box-footer -->
                                </form>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" class="btn btn-default"><a href="{base_url('report/transaction')}">Go Back</a></button>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->