<?php

/*
 * iBOLZ Army
 * 
 * iBOLZ Army Adminstration Tools
 * 
 * @author		Amrizal Sudartama. amrizal@ibolz.tv
 * @copyright 		@ibolz 2015
 * @version 		1.0
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of generate
 *
 * @author Ichalz Break
 */
class Generate extends CI_Controller {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('army_model');
        
    }
    
    public function history() {

        $page = (int) ($this->input->get("page") == "" ? 1 : $this->input->get("page"));
        $date_start_s = $this->input->get("date_start");
        $date_end_s = $this->input->get("date_end");
        $keyword = $this->input->get("keyword");

        $date_start = null;
        if (!empty($date_start_s)) {
            $date_start = DateTime::createFromFormat('Y-m-d', $date_start_s);
            $date_start->setTime(0, 0);
        }

        $date_end = null;
        if (!empty($date_end_s)) {
            $date_end = DateTime::createFromFormat('Y-m-d', $date_end_s);
            $date_end->setTime(23, 59, 59);
        }

        $limit = 12;
        $offset = ($page - 1) * $limit;

        //override
        $offset = 0;
        $limit = 0;

        $reference_log = $this->army_model->getReferenceHistory(null, null, $limit, $offset, $date_start, $date_end, $keyword);

        $reportRowData = array();
        foreach ($reference_log as $rowData) {
            $row = array(
                $rowData->log_date,
                $rowData->referral_number,
                $rowData->army_user_id,
                $rowData->app_name,
                $rowData->mobile_id,
                $rowData->user_id,
                ($rowData->status == '1' ? 'Verified' : 'Unverified'),
                $rowData->reward_value
            );
            $reportRowData[] = $row;
        }


        $reportData = array(
            "header" => array(
                "Date",
                "Army Code",
                "Army User ID",
                "Application",
                "Mobile ID",
                "User ID",
                "Status",
                "Reward"
            ),
            "content" => $reportRowData
        );

        $this->generate_xls("History", $date_start, $date_end, $reportData, $keyword);
    }

    public function finance() {
        $page = (int) ($this->input->get("page") == "" ? 1 : $this->input->get("page"));
        $date_start_s = $this->input->get("date_start");
        $date_end_s = $this->input->get("date_end");
        $keyword = $this->input->get("keyword");

        $date_start = null;
        if (!empty($date_start_s)) {
            $date_start = DateTime::createFromFormat('Y-m-d', $date_start_s);
            $date_start->setTime(0, 0);
        }

        $date_end = null;
        if (!empty($date_end_s)) {
            $date_end = DateTime::createFromFormat('Y-m-d', $date_end_s);
            $date_end->setTime(23, 59, 59);
        }

        $limit = 12;
        $offset = ($page - 1) * $limit;

        //override
        $offset = 0;
        $limit = 0;

        $reference_log = $this->army_model->getReferenceReport(null, null, $limit, $offset, $date_start, $date_end, $keyword);

        $reportRowData = array();
        foreach ($reference_log as $rowData) {
            $row = array(
                $rowData->referral_number,
                $rowData->army_user_id,
                $rowData->display_name,
                $rowData->mobile_no,
                $rowData->bod,
                ($rowData->gender == 'M' ? 'Male' : 'Female'),
                $rowData->bank_account_name,
                $rowData->bank_code,
                $rowData->bank_account_number,
                $rowData->reward_value,
                $rowData->total_reference,
                $rowData->reward_total
            );
            $reportRowData[] = $row;
        }


        $reportData = array(
            "header" => array(
                "Army Code",
                "Army User ID",
                "Army Name",
                "Phone No",
                "Date of Birth",
                "Gender",
                "Bank Account Name",
                "Bank Code",
                "Bank Account Number",
                "Reward Value",
                "Total Reference",
                "Total Reward"
            ),
            "content" => $reportRowData
        );

        $this->generate_xls("Report", $date_start, $date_end, $reportData, $keyword);
    }

    private function generate_xls($title, $date_start, $date_end, $data, $keyword = "") {

        $CONTENT_START_ROW = 9;
        //validate
        $from = "";
        if (!empty($date_start)) {
            $from = $date_start->format('d-M-Y');
        }
        $to = "";
        if (!empty($date_end)) {
            $to = $date_end->format('d-M-Y');
        }


        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Asia/Jakarta');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        /** Include PHPExcel */
        require_once FCPATH . '../PHPExcel/PHPExcel.php';


// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

// Set document properties
        $objPHPExcel->getProperties()->setCreator("iBOLZ Administrator")
                ->setLastModifiedBy("iBOLZ Administrator")
                ->setTitle("iBolz Army $title")
                ->setSubject("iBolz Army $title")
                ->setDescription("iBolz Army $title")
                ->setKeywords("iBolz Army $title")
                ->setCategory("iBolz Army $title");


// document header
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', "iBolz Army $title")
                ->setCellValue('A3', "Generated Date")
                ->setCellValue('A4', "$title Period")
                ->setCellValue('B5', 'From')
                ->setCellValue('B6', 'To')
                ->setCellValue('B7', 'Keyword')
                ->setCellValue('C3', new DateTime())
                ->setCellValue('C5', $from)
                ->setCellValue('C6', $to)
                ->setCellValue('C7', $keyword);

// report header
        $workingColumn = 'A';
        $workingRow = $CONTENT_START_ROW;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($workingColumn . $workingRow, 'No.');

        for ($index = 0; $index < count($data["header"]); $index++) {
            $workingColumn++;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($workingColumn . $workingRow, $data["header"][$index]);
            $objPHPExcel->getActiveSheet()->getColumnDimension($workingColumn)->setAutoSize(true);
        }

// report content
        $workingRow++;
        $contentArray = $data["content"];
        if ($data != null && $contentArray != null && count($contentArray) > 0) {

            for ($row = 0; $row < count($contentArray); $row++) {
                $workingColumn = 'A';
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($workingColumn . $workingRow, $row + 1);

                foreach ($contentArray[$row] as $rowData) {

                    $workingColumn ++;
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($workingColumn . $workingRow, $rowData);
                }

                $workingRow++;
            }
        }


// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle($title);

//cell resize, merge, freeze, filter, style

        $objPHPExcel->getActiveSheet()->mergeCells('A1:' . $workingColumn . '1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(18);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle('A3:A4')->getFont()->setBold(true)->setSize(12);

        $objPHPExcel->getActiveSheet()->freezePane('A' . ($CONTENT_START_ROW + 1));

        $objPHPExcel->getActiveSheet()->getStyle('A' . $CONTENT_START_ROW . ':' . $workingColumn . $CONTENT_START_ROW)->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->setAutoFilter('A' . $CONTENT_START_ROW . ':' . $workingColumn . ($workingRow - 1));


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="iBOLZ-Army-' . $title . '_' . $from . '_' . $to . '.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}
