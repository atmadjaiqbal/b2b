<?php

/**
 * iBOLZ Admin
 * 
 * iBOLZ Adminstration Tools
 * 
 * @author		Amrizal Sudartama @ibolz 
 * @copyright 		@ibolz 2015
 * @version 		2.0
 * 
 * */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Report extends UI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('army_model');
    }

    public function index() {

        $this->setActiveNavigation("finance", "History", "report");

        $report_list = $this->army_model->getReferenceReport();

        $this->data['report_list'] = $report_list;

        $this->layout->build('report.tpl', $this->data);
    }

    public function transaction_list() {
        $this->setActiveNavigation("report", "History", "report_finance");

        $report_list = $this->army_model->getReferenceReport();

        $this->data['report_list'] = $report_list;

        $this->layout->build('finance_report.tpl', $this->data);
    }

}
