<?php
if($listcontent)
{
    foreach($listcontent as $val)
    {
        $dt['row'] = $val['channel_list'];
        $dt['content_category_id'] = $val['channel_category_id'];
        if($val['channel_list'])
        {
?>
        <div id="section-slider">
            <h3 class="slider-title">
                <a href="#"><span><?php echo $val['category_name'];?></span> <i class="fa fa-angle-right"></i></a>
            </h3>
<?php
        $this->load->view('content/content_slider', $dt);
?>
        </div>
<?php
        }
    }
}
?>