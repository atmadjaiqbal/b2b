<!-- Slider start -->
<section id="home" class="no-padding" style="margin-top: 80px;">	
	<!-- Carousel -->
	<div id="main-slide" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
		  	<!-- <li data-target="#main-slide" data-slide-to="0" class="active"></li>
		    <li data-target="#main-slide" data-slide-to="1"></li>
		    <li data-target="#main-slide" data-slide-to="2"></li>
		    <li data-target="#main-slide" data-slide-to="3"></li> -->
		</ol><!--/ Indicators end-->

		<!-- Carousel inner -->
		<div class="carousel-inner">
            <?php
			if (config_item('jmlSlider')) {
				for ($i=1; $i <= config_item('jmlSlider'); $i++) { 
					$active = ($i==1 ? 'active' : '');
					?>
					
		            <div class="item <?php echo $active; ?>">
				    	<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider<?php echo $i; ?>.jpg" alt="slider">
				    </div><!--/ Carousel item end -->

					<?php
				}
			}
			?>
		</div><!-- Carousel inner end-->

		<!-- Controls -->
		<a class="left carousel-control" href="#main-slide" data-slide="prev">
	    	<span><i class="fa fa-angle-left"></i></span>
		</a>
		<a class="right carousel-control" href="#main-slide" data-slide="next">
	    	<span><i class="fa fa-angle-right"></i></span>
		</a>
	</div><!-- /carousel -->    	
</section>
<!--/ Slider end -->