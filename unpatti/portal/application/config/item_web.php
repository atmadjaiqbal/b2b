<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider1, slider2, slider3 .jpg

*/

$config['nameLogo'] = "UNPATTI TV";
$config['title'] = "IBOLZ | UNPATTI TV";
$config['warnaHeader'] = "#1E8CBB";
$config['colorMenu'] = "#1E8CBB";
$config['warnaCopyright'] = "#1E8CBB";
$config['warnaNavCarousel'] = "#1E8CBB";
$config['colorListbox'] = "#EEEEf4";
$config['warnaMenuMobile'] = "#1E8CBB";
$config['warnaTab'] = "#1E8CBB";
$config['footerCopyright'] = "UNPATTI TV";
$config['jmlSlider'] = 2;

/* --- About Us --- */

$config['aboutUs'] = "";

/* --- Contact Us --- */

$config['contactUs'] = false;
$config['detailOffice'] = "UNIVERSITAS PATTIMURA";
$config['address'] = "";
$config['tlp'] = "";
$config['fax'] = "";
$config['contactCenter'] = "";
$config['email'] = "";
$config['website'] = "";

/* --- Download --- */

$config['namaFolder'] = "unpatti";
$config['namaAPK'] = "UniversitasPattimuraTV-2.0.5"; // tanpa .apk

?>

