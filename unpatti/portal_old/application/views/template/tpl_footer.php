<?php
//if($this->session->userdata('display_name'))
//{
    $faqlink = base_url().'home/home_faq';
//} else {
//    $faqlink = base_url();
//}
?>


<div class="clear"></div>
<footer id="footer-sec">
    <div class="top-pad"></div>
    <div class="other-link">
        <a href="<?php echo base_url().'home/home_aboutus';?>">About Us</a>&nbsp;|&nbsp;
        <a href="<?php echo $faqlink; ?>">FAQ</a>&nbsp;|&nbsp;
        <a href="<?php echo '#';//base_url().'home/home_usermanual';?>">Help</a>&nbsp;|&nbsp;
        <a href="<?php echo base_url().'home/home_contactus';?>">Contact Us</a>
    </div>
    <div class="belongs">
        Copyright © 2015 UNPATTI TV.All right reserved.
    </div>

</footer>


<script type="text/javascript">

    <?php
       $success = $this->session->flashdata('message_success');
       $error = $this->session->flashdata('message_error');
       if (!empty($success))
       {  ?>
    alertify.alert("<?php echo $success; ?>");
    alertify.log("Success", "success");
    <?php }
       if (!empty($error))
       { ?>
    alertify.alert("<?php echo $error; ?>");
    alertify.log("Error", "error");
    <?php } ?>

    var vod = {
        init: function() {},
        content_view: function(container_id, content_id, menu_id, menu_type, menu_title, callback)
        {
            $.get(Settings.base_url+'content/content_info/'+content_id+'/'+menu_id+'/'+menu_type+'/'+menu_title, function(result) {
                $(container_id).html(result);
                $(container_id).removeClass('bg-loader');
                if(callback) callback();
            });
        }
    }

    $(document).ready(function() {
        $('#slideheadline').carousel({interval: 6000});
        $(".link-item").tooltip({placement : 'bottom'});

        $('.owl-carousel').each(function(idx, slider){
            var item_count = $(slider).data('items');
            if(item_count == undefined) item_count = 3;
            var newcontent_slider = $(slider).owlCarousel({
                navigation: true,
                pagination: false,
                items: item_count,
                itemsDesktop: [1280,3],
                itemsDesktopMedium: [1199,3],
                itemsDesktopSmall: [979,3],
                itemsTablet: [768, 3],
                itemsMobile: [479, 2],
                navigationText: [
                    "<i class='fa fa-2x fa-chevron-left'></i>",
                    "<i class='fa fa-2x fa-chevron-right'></i>"
                ]
            });
        })


        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function(item) {
                    return item.el.attr('title') + '<small>BNN TV, Powered By IBOLZ.</small>';
                }
            }
        });

        $('#click-login').click(function(e){e.preventDefault(); $('#form-login').submit();});
        $('#click-forgetpass').click(function(e){e.preventDefault(); $('#forgot-password').submit();});
        $('#account_password').keypress(function(e){if (e.which == 13) { e.preventDefault(); $('#form-login').submit();}});
        $('#click-saveprofile').click(function(e){e.preventDefault(); $('#change-profile').submit();});
        $('#click-changepass').click(function(e){e.preventDefault(); $('#change-pass').submit();});

    });
</script>

</html>

