<div class="full-container fixed-container">
   <?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>
</div>

<div class="full-container">
    <div class="container">
    <?php
    if($listmenu)
    {
        foreach($listmenu as $row)
        {
            $c_menu_id = $row['menu_id'];
            $c_category_name = $row['category_name'];
?>
        <div class="home-content">
            <h3 class="slider-title">
                <a><span><?php echo $c_category_name;?></span> <i class="fa fa-angle-right"></i></a>
            </h3>
        </div>

        <div class="content-item">
            <div id="channel-categories-list" class="row">
                <?php

                /* 17911070065617885b9514f --> lapor , 17911070065617885b9514f --> statistik  */

                if($c_menu_id == '17911070065617885b9514f')
                {
                ?>
                    <iframe src="http://www.kemenperin.go.id/pengaduan/laporan.php" width="990px" height="855px"></iframe>
                <?php
                } else {
                ?>
                <ul class="playlist">
                <?php
                if($row['submenu'])
                {
                    foreach($row['submenu'] as $rwContent)
                    {
                      if(strtolower($rwContent->title) != 'nomor penting' AND trim(strtolower($rwContent->title)) != 'pengaduan')
                      {

                        $_c_product_id = $rwContent->product_id;
                        $_c_apps_id = $rwContent->apps_id;
                        $_c_menu_id = $rwContent->menu_id;
                        $_c_parent_menu_id = $rwContent->parent_menu_id;
                        $_c_menu_type = $rwContent->menu_type;
                        $_c_title = $rwContent->title;
                        $_c_thumbnail = $rwContent->thumbnail;
                        $_c_link = $rwContent->link;
                        $_c_icon_size = $rwContent->icon_size;
                        $_c_poster_size = $rwContent->poster_size;
                        $short_title = strlen($_c_title) > 14 ? substr($_c_title, 0, 14).'...' : $_c_title;

                        switch(strtolower($_c_menu_type))
                        {
                            case 'menu' :
                                $_link = base_url().'content/content_categories_list/'.$_c_menu_id.'/'.urlencode($_c_title);
                                break;
                            case 'channel' :
                                $_link = base_url().'content/content_channel/'.$_c_menu_id.'/'.$_c_product_id;
                                break;
                            default:
                                $_link = base_url().'content/content_categories/'.$_c_menu_id.'/'.urlencode($_c_title);
                                break;
                        }
                ?>
                     <li>
                        <a href="<?php echo $_link;?>">
                         <div class="product-title">
                            <span class="pull-left"><?php echo $short_title;?></span>
                            <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                         </div>
                         <div class="product-image">
                            <a href="<?php echo $_link;?>">
                                <div class="preview_product related bgfull"
                                     style="border: 1px solid #FFEBD1;background-color:#FCF7EF;background: url('<?php echo $_c_thumbnail;?>&w=300') no-repeat scroll center;width:270px;height:160px;"
                                     data-id="<?php echo $_c_product_id;?>" data-menu-id="<?php echo $_c_menu_id;?>" src="<?php echo $_c_thumbnail;?>"></div>
                            </a>
                         </div>
                        </a>
                     </li>


                <?php
                     }
                    }
                }
                ?>

                </ul>
                <?php
                }
                ?>
            </div>
        </div>

        <?php
        }
    }
?>
    </div>
</div>


<div class="full-container">
    <?php // if (isset($contentslider) && !empty($contentslider)) echo $contentslider; ?>
</div>
