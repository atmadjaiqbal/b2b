<?php
class Download_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    function list_apps_download()
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '4'
        );
        $api_url = config_item('api_donwload_apps');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function download_detail($upload_id)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '4',
            'uploadid' => $upload_id
        );
        $api_url = config_item('api_donwload_detail');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

    function download_count($upload_id, $countdown)
    {
        $post_params = array(
            'apps_id' => config_item('ibolz_app_id'),
            'customer_id' => config_item('ibolz_customer_id'),
            'device_id' => '4',
            'uploadid' => $upload_id,
            'countdown' => $countdown
        );
        $api_url = config_item('api_donwload_count');
        $response = $this->CI->rest->post($api_url, $post_params);
        return $response;
    }

}
?>