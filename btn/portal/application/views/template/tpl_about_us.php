<div class="container" style="margin-top: 140px;">
    <div id="faq-bni">
        <div class="faq-title-place">
            <h3>ABOUT US</h3>
        </div>
        <div class="faq-details">
        	<p><b>Bank Tabungan Negara</b> atau <b>BTN</b> adalah Badan Usaha Milik Negara Indonesia yang 
          berbentuk perseroan terbatas dan bergerak di bidang jasa keuangan perbankan. Sejak tahun 
          2012, BTN dipimpin oleh Maryono sebagai Direktur Utama.</p>
        	<p>Menteri Perindustrian Saleh Husin akan terus mendorong pengembangan industri nasional mengingat sektor tersebut merupakan tulang punggung perekonomian Indonesia, dimana sektor industri masih memberikan kotribusi yang cukup signifikan terhadap ekonomi dengan mencapai lebih dari 23% atau menjadi sektor terbesar penyumbang ekonomi nasional.</p>
        <h2><b>Sejarah Bank BTN</b></h2>
        <table>
          <tr style="line-height: 20px;">
            <td style="width: 30px;">No</td>
            <td style="width: 50px;">Tahun</td>
            <td>Keterangan</td>
          </tr>
          <tr>
            <td>1</td>
            <td>1897</td>
            <td>BTN berdiri dengan nama "Postpaarbank" pada masa pemerintah Belanda</td>
          </tr>
          <tr>
            <td>2</td>
            <td>1950</td>
            <td>Perubahan nama menjadi "Bank Tabungan Pos" oleh Pemerintah RI</td>
          </tr>
          <tr>
            <td>3</td>
            <td>1963</td>
            <td>Berganti nama menjadi Bank Tabungan Negara</td>
          </tr>
          <tr>
            <td>4</td>
            <td>1974</td>
            <td>Ditunjuk pemerintah sebagai satu-satunya institusi yang menyalurkan KPR bagi golongan masyarakat menengah kebawah</td>
          </tr>
          <tr>
            <td>5</td>
            <td>1989</td>
            <td>Memulai operasi sebagai bank komersial dan menerbitkan obligasi pertama</td>
          </tr>
          <tr>
            <td>6</td>
            <td>1994</td>
            <td>Memperoleh izin untuk beroperasi sebagai Bank Devisa</td>
          </tr>
          <tr>
            <td>7</td>
            <td>2002</td>
            <td>Ditunjuk sebagai bank komersial yang fokus pada pembiayaan rumah komersial</td>
          </tr>
          <tr>
            <td>8</td>
            <td>2009</td>
            <td>Sekuritisasi KPR melalui Kontrak Investasi Kolektif Efek Beragun Aset (KIK EBA) pertama di Indonesia</td>
          </tr>
          <tr>
            <td>9</td>
            <td>2009</td>
            <td>Bank BTN melakukan Penawaran Umum Saham Perdana (IPO) dan listing di Bursa Efek Indonesia</td>
          </tr>
          <tr>
            <td>10</td>
            <td>2012</td>
            <td>Bank BTN melakukan Right Issue</td>
          </tr>


        </table>

        </div>
    </div>
</div>