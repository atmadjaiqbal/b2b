<div class="container" style="margin-top: 140px;">
<div id="search-onl">
    <ul class="playlist">
<?php
if($listcontent->product_list)
{
    foreach($listcontent->product_list as $row)
    {
        $menu_id = $row->menu_id;
        $apps_id = $row->apps_id;
        $product_id = $row->product_id;
        $channel_category_id = $row->channel_category_id;
        $channel_id = $row->channel_id;
        $channel_type_id = $row->channel_type_id;
        $content_id = $row->content_id;
        $content_type = $row->content_type;
        $title = $row->title;
        $short_title = strlen($title) > 14 ? substr($title, 0, 14).'...' : $title;
        $thumbnail = $row->thumbnail;
        $poster = $thumbnail . '&w=200';
        $description = $row->description;
        $prod_year = $row->prod_year;
        $video_duration = $row->video_duration;
        $countlike = $row->countlike;
        $countviewer = $row->countviewer;
        $icon_size = $row->icon_size;
        $poster_size = $row->poster_size;
        $created = $row->created;
        $crew_category_list = $row->crew_category_list;
?>
        <li>
            <a href="<?php echo base_url();?>content/content_detail/<?php echo $content_id; ?>/<?php echo $menu_id;?>">
                <div class="product-title">
                    <span class="pull-left"><?php echo $short_title;?></span>
                    <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                </div>
                <div class="product-image">
                    <div class="preview_product related bgfull"
                         style="background-color:#cecece;background: url('<?php echo $poster;?>') no-repeat scroll center;width:160px;height:240px;"
                         id="'<?php echo $menu_id;?>'-'<?php echo $product_id;?>'"
                         data-id="'<?php echo $product_id;?>'" data-menu-id="'<?php echo $menu_id;?>'"></div>
                </div>
            </a>
        </li>
<?php
    }
?>
    </ul>
<?php
} else {
?>
    <div id="section-movie-detail" style="text-align: center !important;margin-top:70px;">
        <h3> Not found </h3>
    </div>
<?php
}
?>
</div>
</div>

