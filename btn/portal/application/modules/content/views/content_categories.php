<div class="container" style="margin-top: 140px;">

    <div class="span8" id="content-categories">
        <h3 class="slider-title">
          <span><?php echo $menu_title;?></span> <i class="fa fa-angle-right"></i>
        </h3>
        <?php
        if($list_content)
        {
            $crew_category_list = array(); $genre_list = array();
            foreach($list_content->product_list as $val)
            {
                $menu_id = $val->menu_id;
                $apps_id = $val->apps_id;
                $product_id = $val->product_id;
                $channel_category_id = $val->channel_category_id;
                $channel_id = $val->channel_id;
                $channel_type_id = $val->channel_type_id;
                $content_id = $val->content_id;
                $content_type = $val->content_type;
                $title = $val->title;
                $short_title = strlen($title) > 30 ? substr($title, 0, 30). '...' : $title;
                $thumbnail = $val->thumbnail.'&w=200';
                $description = $val->description;
                $prod_year = $val->prod_year;
                $video_duration = $val->video_duration;
                $countlike = $val->countlike;
                $countcomment = $val->countcomment;
                $countviewer = $val->countviewer;
                $icon_size = $val->icon_size;
                $poster_size = $val->poster_size;
                $crew_category_list = $val->crew_category_list;
                $genre_list = $val->genre_list;
                $related_list = $val->related_list;
                if($val->crew_category_list) { $lecture = $val->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}

                ?>


        <script type="text/javascript">
            $(document).ready(function() {
                $('#like-<?php echo $content_id; ?>').click(function(e){
                    e.preventDefault();
                    var id = $(this).data('id');
                    $.post(Settings.base_url+'content/post_like/', {'content_id':id}, function(data){
                        if(data.rcode != 'ok'){
                            alert(data.msg);
                        } else {
                            liketotal('#totallike-<?php echo $content_id; ?>');
                        }
                    });
                });
            });
        </script>

        <div class="categories-item">
            <div class="image-place">
                <div style="width: 200px;height: 300px;background: url(<?php echo $thumbnail;?>) no-repeat scroll center;background-color: #cecece;"></div>
            </div>
            <div class="detail-place">
                <div class="content">
                    <h3><?php echo $short_title;?></h3>
                    <p class="sub-title">DESCRIPTION</p>
                    <p class="sub-description"><?php echo (strlen($description) > 135 ? substr($description, 0, 135).'...' : $description);?></p>
                    <dl>
                        <dt>DATE</dt><dd>:&nbsp;&nbsp;<?php echo $prod_year;?></dd>
                        <dt>TIME</dt><dd>:&nbsp;&nbsp;</dd>
                    </dl>

                </div>
                <div class="content-other">
                    <div class="sosmed-sec">
                        <div class="button-view">
                            <a href="<?php echo base_url();?>content/content_detail/<?php echo $content_id; ?>/<?php echo $menu_id;?>/<?php echo urlencode($menu_title);?>">VIEW</a>
                        </div>

                        <div class="social">
                            <ul class="content-score">
                                <li>
                                    <a class="score-btn" data-tipe='like' id='like-<?php echo $content_id; ?>' data-id='<?php echo $content_id; ?>'>
                                        <img class="pull-left" src="<?php echo base_url('assets/images/like_icon_small.png'); ?>">
                                        <div id="totallike-<?php echo $content_id; ?>" data-id='<?php echo $content_id; ?>' style="width: 24px;"><?php echo $countlike; ?></div>
                                    </a>
                                </li>
                                <!-- li>
                                    <a class="score-btn" data-tipe='dislike' data-id='<?php echo $content_id; ?>'>
                                        <img class="pull-left" src="< ?php echo base_url('assets/images/dislike_icon_small.png'); ?>">
                                    </a>
                                    <div style="width: 24px;">0<?php // echo $unlike; ?></div>
                                </li -->
                                <li>
                                    <a class="score-btn" data-tipe='comment' data-id='<?php echo $content_id; ?>'>
                                        <img class="pull-left" src="<?php echo base_url('assets/images/comment_icon_small.png'); ?>">
                                    </a>
                                    <div style="width: 24px;"><?php echo $countcomment; ?></div>
                                </li>
                                <li>

                                    <?php
                                    $_message = "Hi, saya ingin share materi ini, materi yang menarik dan bagus untuk dipelajari. %0D%0A";
                                    $_message .= "Link urlnya sebagai berikut ini : %0D%0A%0D%0A";
                                    $_message .= current_url();
                                    $_message .= "%0D%0A%0D%0ASelamat belajar.";
                                    //$_message .= "Atau %3Ca%20href%3D%22".current_url()."%22%3Eklik link ini%3C%2Fa%3E untuk langsung ke materi.";
                                    ?>
                                    <a href="mailto:?subject=<?php echo $title;?>&amp;body=<?php echo $_message; ?>" title="Share By Email" class="score-btn">
                                        <img class="pull-left" src="<?php echo base_url('assets/images/share_by_email.png'); ?>" style="height:25px;">
                                    </a>
                                    <div style="width: 24px;">Email</div>
                                </li>


                            </ul>
                        </div>

                    </div>
                    <div class="related-sec">
                        <?php
                        $r_thumbnail = base_url().'assets/images/no_video.png';
                        if($related_list)
                        {
                            $r_content_id = $related_list[0]->content_id;
                            $r_content_related_id = $related_list[0]->content_related_id;
                            $r_title = $related_list[0]->title;
                            $r_title = strlen($r_title) > 14 ? substr($r_title, 0, 14).'...' : $r_title;
                            $r_video_duration = $related_list[0]->video_duration;
                            $r_thumbnail = $related_list[0]->thumbnail.'&w=240';
                        ?>
                            <a href="<?php echo base_url().'content/content_detail/'.$r_content_related_id.'/'.$menu_id;?>">
                                <div class="product-title">
                                    <span class="pull-left"><?php echo $r_title;?></span>
                                    <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                </div>
                                <div class="product-image">
                                    <div class="preview_product related bgfull"
                                         style="background-color:#cecece;background: url('<?php echo $r_thumbnail;?>') no-repeat;width:auto;height:90px;">
                                    </div>
                                </div>
                            </a>
                        <?php
                        } else {
                        ?>
                            <div><img src="<?php echo $r_thumbnail;?>"></div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>


        <?php
            }
        }
        ?>

    </div>

    <div class="span4">
        <div class="row" style="height: 50px;">

        </div>
        <!-- div class="row"><img src="< ?php echo base_url('assets/images/onl-promo1.gif');?>"></div>
        <div class="row" style="margin-top:10px;"><img src="< ?php echo base_url('assets/images/onl-promo2.jpg');?>"></div -->

        <div class="row">
            <?php if(isset($content_popular)) echo $content_popular; ?>
        </div>

        <div class="row">
            <?php if(isset($content_latest)) echo $content_latest; ?>
        </div>

    </div>


</div>

<div class="container">
 <div class="row">
    <div id="content_container" data-page='1' data-categories="<?php echo $menu_id;?>"></div>

    <div class="clear row loadmore-contentplace">
        <div class="text-center">
            <div id="load_more_place" class="loadmore loadmore-bg">
                <a id="content_load_more" class="loadmore-text" href="#">LOAD MORE</>
            </div>
        </div>
    </div>
 </div>
</div>