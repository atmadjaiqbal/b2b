<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider1, slider2, slider3 .jpg

*/

// $config['nameLogo'] = "BURU TV";
// $config['title'] = "IBOLZ | BURU TV";
// $config['warnaHeader'] = "#4BA100";
// $config['colorMenu'] = "#4BA100";
// $config['warnaCopyright'] = "#4BA100";
// $config['warnaNavCarousel'] = "#4BA100";
// $config['colorListbox'] = "#e1e8ed";
// $config['warnaMenuMobile'] = "#4BA100";
// $config['footerCopyright'] = "BURU TV";
// $config['jmlSlider'] = 2;

/* --- About Us --- */

$config['aboutUs'] = "<p><b>Masyarakat Kabupaten Buru yang Sejahtera dan Demokratis</b><br>Kabupaten Buru Yang Maju Menuju Masyarakat Bupolo Yang Mandiri, Sejahtera, Demokratis Dan Berkeadilan</p>";

/* --- Contact Us --- */

$config['contactUs'] = false;
$config['detailOffice'] = "Terimakasih atas minat anda pada Greenpeace Asia Tenggara. Jika anda mempunyai pertanyaan, silakan hubungi kami :";
$config['address'] = "Mega Plaza Building Lt. 5, Jl. HR. Rasuna Said Kav. C3, Kuningan, Jakarta Selatan, Indonesia 12920";
$config['tlp'] = "+62 21 521 2552";
$config['fax'] = "+62 21 521 2553";
$config['contactCenter'] = "";
$config['email'] = "info.id@greenpeace.org , supporterservices.id@greenpeace.org";
$config['website'] = "";

/* --- Download --- */

$config['namaFolder'] = "buru";
$config['namaAPK'] = "IbukotaTV-1.1.18-1431337131";

?>

