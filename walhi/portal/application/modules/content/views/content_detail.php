<?php
if($product_detail)
{
    $player_wrap_width = "620"; $player_width = "620"; $player_height = "400";
    $crew_category_list = array(); $genre_list = array(); $photo_list = array(); $related_list = array();
    $rcode = $product_detail->rcode;
    $channel_id = $product_detail->channel_id;
    $content_id = $product_detail->content_id;
    $title = $product_detail->title;
    $video_duration = $product_detail->video_duration;
    $filename = $product_detail->filename;
    $thumbnail = $product_detail->thumbnail;
    $description = $product_detail->description;
    $prod_year = $product_detail->prod_year;
    $trailer_id = $product_detail->trailer_id;
    $price = $product_detail->price;
    $ulike = $product_detail->ulike;
    $unlike = $product_detail->unlike;
    $comment = $product_detail->comment;
    if($product_detail->crew_category_list) { $lecture = $product_detail->crew_category_list[0]->crew_list[0]->crew_name;} else { $lecture = '';}
    $genre_list = $product_detail->genre_list;
    $crew_category_list = $product_detail->crew_category_list;
    $photo_list = $product_detail->photo_list;
    $related_list = $product_detail->related_list;
    $document_list = $product_detail->document_list;
    $player = 'http://ply001.3d.ibolztv.net/player-testing/flash/id/b2b.php?w='.$player_width.'&h='.$player_height;
    $player .= '&apps_id='.config_item('ibolz_app_id').'&version='.config_item('ibolz_version').'&type=content';
    $player .= '&channel_id='.$channel_id.'&vid='.$content_id.'&id='.$content_id.'&customer_id='.$customer_id;
    ?>

<section id="featured-video" class="featured-video" style="padding-top: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h3 class="classic"><i class="fa fa-play-circle"></i><div class="title"></div><?php echo $title;?></h3>
            </div>

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div style="text-align: center;width:100%;background-color: black;">
                            <!-- Change the url -->
                            <iframe seamless wmode="Opaque" class="player bg-loader" scrolling="no"
                              style='width: <?php echo $player_width;?>px;min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;'
                              src="<?php echo $player;?>">
                            </iframe>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <h4 class="black">DESCRIPTION</h4>
                        <p><?php echo $description; ?></p>
                        <p class="black">
                            <?php
                            if ($genre_list) {
                                ?>
                                <i class="fa fa-folder-open"></i> Gendre : <?php for ($i=0; $i < count($genre_list); $i++) { echo $genre_list[$i]->genre_name." &nbsp "; }?> &nbsp | &nbsp 
                                <i class="fa fa-calendar"></i> Year : <?php echo $prod_year; ?> 
                                <?php
                            }
                            ?>
                        </p>
                        <p class="black">
                            <?php
                            if ($crew_category_list) { ?>
                                <i class="fa fa-list"></i>
                                <?php
                                for ($i=0; $i < count($crew_category_list); $i++) { 
                                    echo $crew_category_list[$i]->category_name." : ";
                                    for ($x=0; $x < count($crew_category_list[$i]->crew_list); $x++) { 
                                        echo $crew_category_list[$i]->crew_list[0]->crew_name." &nbsp | &nbsp ";
                                    }
                                }
                            }
                            ?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
              <div class="row">
                <div class="col-md-12 col-xs-6">
                  <div class="poster">
                    <img src="<?php echo $thumbnail;?>&w=300px" class="img-responsive">
                  </div>
                </div>

                <div class="col-md-12 col-xs-6">
                    <div class="video-block-content">
                    <?php
                    if($photo_list) {
                        ?>
                        <h4 class="black">PHOTO</h4>
                        <div class="popup-gallery">
                        <?php
                        $y = 1; $photo_filename = array();
                        foreach($photo_list as $value)
                        {
                            $photonya = $value->thumbnail.'&w=200';
                            $photofull = $value->thumbnail.'&w=300';
                            $photo_content_id[$y] = $value->content_id;

                            if(isset($value->thumbnail)) {
                                ?>
                                <a href="<?php echo $photofull; ?>" title="<?php echo $title; ?>"><img src="<?php echo $photonya; ?>" width="75" height="75" style="margin-bottom: 5px;"></a>
                                <?php
                            } else {
                               $photo_filename[$y] = '';
                            }

                            //$photo_filename[$y] = isset($value->thumbnail) ? 'style="background: url('.$photonya.') no-repeat;background-size: 100% 100%;"' : '';
                            $photo_thumbnail[$y] = $value->filename;
                            $y++;
                        }
                        ?>
                        
                        </div>
                        <?php } ?>
                    </div><!--/ End 1st block -->
                </div>

              </div>

              
            </div>



        </div><!-- Content row end -->
    </div><!-- Container end -->
</section>

<section id="clients" class="clients">
    <div class="container">
      <div class="row wow fadeInLeft animated" style="visibility: visible;">
        <div class="col-md-12 col-xs-12">
            <?php
            $default_r_thumb = base_url().'assets/images/no_video.png';
            if($related_list) { ?>
            <h3>SESSIONS</h3>
            <div id="owl-demo" class="owl-carousel owl-theme">
                <?php
                foreach($related_list as $value) {
                    $other_trailer = $value->thumbnail.'&w=200';
                    $r_content_id = $value->content_id;
                    $r_content_related_id = $value->content_related_id;
                    $r_title = $value->title;
                    $r_title = strlen($r_title) > 22 ? substr($r_title, 0, 22).'...' : $r_title;
                    $r_video_duration = $value->video_duration;
                    $r_thumbnail = isset($value->thumbnail) ? $other_trailer : $default_r_thumb;
                    ?>

                    <div class="item">

                        <div class="col-sm-12 col-xs-12 portfolio-static-item" style="padding: 0px;">
                            <a href="<?php echo base_url().'content/content_detail/'.$r_content_related_id.'/'.$menu_id;?>">
                                <div class="grid">
                                    <figure class="itemV" style="background: #D7D7D7;height: 100px;">
                                        <img src="<?php echo $r_thumbnail;?>" alt="" style="position: absolute;top: 50%;transform: translateY(-50%);">        
                                    </figure>
                                    <div class="portfolio-static-desc">
                                        <h3><?php echo $r_title;?></h3>
                                    </div>                  
                                </div><!--/ grid end -->
                            </a>
                        </div><!--/ item 1 end -->

                    </div>
                    <?php
                }
                ?>           
            </div>
            <?php } ?>
        </div>
      </div><!-- Main row end -->
    </div><!--/ Container end -->
</section>

<?php } ?>