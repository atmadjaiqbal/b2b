<?php
if($channel_detail) {

  $player_wrap_width = "620"; $player_width = "620"; $player_height = "400";
  $chan_channel_id = $channel_detail->channel_id;
  $chan_content_id = $channel_detail->channel_id;
  $chan_channel_name = $channel_detail->channel_name;
  $chan_alias = $channel_detail->alias;
  $chan_thumbnail = $channel_detail->thumbnail.'&w=300';
  $chan_description = $channel_detail->description;
  $chan_type_name = $channel_detail->type_name;
  $chan_status = $channel_detail->status;
  $chan_countviewer = $channel_detail->countviewer;
  $chan_order_number = $channel_detail->order_number;
  $chan_link = $channel_detail->link;
  $chan_created = $channel_detail->created;
  $chan_createdby = $channel_detail->createdby;
  $chan_apps_id = $channel_detail->apps_id;
  $chan_app_name = $channel_detail->app_name;

  if($chan_status == '0') redirect('/');
  $player = 'http://ply001.3d.ibolztv.net/player-testing/flash/id/b2b.php?w='.$player_width.'&h='.$player_height;
  $player .= '&apps_id='.config_item('ibolz_app_id').'&version='.config_item('ibolz_version').'&type=channel';
  $player .= '&channel_id='.$chan_channel_id.'&vid='.$chan_content_id.'&id='.$chan_content_id.'&customer_id='.config_item('ibolz_customer_id');
  ?>

  <section id="featured-video" class="featured-video" style="padding-top: 100px;">
    <div class="container">
      <div class="row">

        <div class="col-md-1 col-xs-12"></div>
        <div class="col-md-7 col-xs-12">
          <iframe seamless wmode="Opaque" class="player bg-loader" scrolling="no"
                 style='width: <?php echo $player_width;?>px;min-height:<?php echo $player_height;?>px;min-width:<?php echo $player_wrap_width;?>px;'
                 src="<?php echo $player;?>">
         </iframe>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="row">
            <div class="col-md-12 col-xs-6">
              <div class="video-block-head">
                <h3 style="margin-top: 10px;"><?php echo $chan_channel_name;?></h3>
                <img src="<?php echo $chan_thumbnail;?>" class="img-responsive">
              </div>
            </div>


            <div class="col-md-12 col-xs-6">
              <div class="video-block-content">
                <?php
                if($schedule_today->schedule_list) { ?>
                  <h4>SCHEDULE</h4>
                  <div class="scroll">
                    <?php
                    foreach($schedule_today->schedule_list as $sche) {
                      $sch_schedule_id = $sche->schedule_id;
                      $sch_channel_id = $sche->channel_id;
                      $sch_program_id = $sche->program_id;
                      $sch_duration = $sche->duration;
                      $sch_program_name = $sche->program_name;
                      $sch_thumbnail = $sche->thumbnail;
                      $sch_start_date_time = $sche->start_date_time;
                      $sch_start_time_text = $sche->start_time_text;
                      $sch_end_time_text = $sche->end_time_text;
                      $sch_schedule_type_id = $sche->schedule_type_id;
                      $sch_server_time = $sche->server_time;
                      $sch_enable_record = $sche->enable_record;
                      $sch_enable_remove = $sche->enable_remove;
                      $sch_enable_play = $sche->enable_play;
                      ?>

                      <p><?php echo $sch_end_time_text; ?> &nbsp &nbsp <?php echo $sch_program_name; ?></p>

                      <?php
                    }
                    ?>
                  </div>
                <?php } ?>
              </div><!--/ End 1st block -->
            </div>

          </div>
          
        </div>

          <div class="clearfix"></div>

          <div class="col-md-1 col-xs-12"></div>
          <div class="col-md-11 col-xs-12">
          <div class="video-block-head" style="margin-top: 10px;">
            <h3>DESCRIPTION</h3>
            <p><?php echo $chan_description; ?></p>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section id="clients" class="clients">
    <div class="container">
      <div class="row wow fadeInLeft animated" style="visibility: visible;">
        <div class="col-md-1 col-xs-12"></div>
        <div class="col-md-11 col-xs-12">
          <h3>OTHER CHANNEL</h3>
          <div id="owl-demo" class="owl-carousel owl-theme">

            <?php
            if($list_content) {
                $crew_category_list = array(); $genre_list = array();

                foreach($list_content->menu_list as $val) {
                  if ((strpos($val->title, $chan_alias) !== FALSE)) {
                    continue;
                  }
                    $product_id = $val->product_id;
                    $apps_id = $val->apps_id;
                    $menu_id = $val->menu_id;
                    $parent_menu_id = $val->parent_menu_id;
                    $menu_type = $val->menu_type;
                    $title = strlen($val->title) > 14 ? substr($val->title, 0, 14).'...' : $val->title;

                    $thumbnail = $val->thumbnail . '&w=300';
                    $count_product = $val->count_product;

                    switch($menu_type)
                    {
                        case 'menu' :
                            $_link = base_url().'content/content_categories_list/'.$menu_id.'/'.urlencode($title);
                            break;
                        case 'channel' :
                            $_link = base_url().'content/content_channel/'.$menu_id.'/'.$product_id;
                            break;
                        default:
                            $_link = base_url().'content/content_categories/'.$menu_id.'/'.urlencode($title);
                            break;
                    }

                    if($title != 'Nomor Penting') { ?>

                      <div class="item">

                        <div class="col-sm-12 col-xs-12 portfolio-static-item" style="padding: 0px;">
                            <a href="<?php echo $_link;?>">
                                <div class="grid">
                                    <figure class="itemV">
                                        <img src="<?php echo $thumbnail;?>" alt="">        
                                    </figure>
                                    <div class="portfolio-static-desc">
                                        <h3><?php echo $title;?></h3>
                                    </div>                  
                                </div><!--/ grid end -->
                            </a>
                        </div><!--/ item 1 end -->

                      </div>
                        <?php
                    }
                }
            }
            ?>
           
          </div>
        </div>
      </div><!-- Main row end -->
    </div><!--/ Container end -->
  </section>

  <?php
} else { ?>

  <div style="text-align: center;font-family: helvetica, arial, sans-serif; font-size: 28px;margin-top: 150px;margin-bottom: 200px;">
      Channel Not Available
  </div>

  <?php
}
?>

