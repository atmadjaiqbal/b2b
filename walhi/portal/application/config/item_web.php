<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider1, slider2, slider3 .jpg

*/

$config['nameLogo'] = "";
$config['title'] = "IBOLZ | WALHI TV";
$config['warnaHeader'] = "#1CA64A";
$config['colorMenu'] = "#1CA64A";
$config['warnaCopyright'] = "#1CA64A";
$config['warnaNavCarousel'] = "#1CA64A";
$config['colorListbox'] = "28,166,74";
$config['warnaMenuMobile'] = "#1CA64A";
$config['footerCopyright'] = "WALHI TV";
$config['jmlSlider'] = 2;

/* --- About Us --- */

$config['aboutUs'] = "<p>about us</p>";

/* --- Contact Us --- */

$config['detailOffice'] = "";
$config['address'] = "";
$config['tlp'] = "";
$config['fax'] = "";
$config['contactCenter'] = "";
$config['email'] = "";
$config['website'] = "";

/* --- Download --- */

$config['namaFolder'] = "walhi";
$config['namaAPK'] = "GreenpeaceTV-2.0.5-1445434003";

?>

