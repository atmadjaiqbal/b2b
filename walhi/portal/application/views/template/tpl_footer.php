	<section id="copyright" class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="footers">
						<a href="<?php echo base_url().'home/home_aboutus';?>">About Us</a> &nbsp | &nbsp
						<a href="<?php echo base_url().'home/home_faq'; ?>">FAQ</a> &nbsp | &nbsp
						<a href="<?php echo '#';//base_url().'home/home_usermanual';?>">Help</a> &nbsp | &nbsp
						<a href="<?php echo base_url().'home/home_contactus';?>">Contact Us</a>
        			</div>
				</div>
				<div class="col-md-12 text-center">
					<div class="copyright-info">
         			 © Copyright 2015 <?php echo config_item('footerCopyright'); ?>. <span>All Rights Reserved</span>
        			</div>
				</div>
			</div><!--/ Row end -->
		   <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix" data-original-title="" title="" style="display: block;">
				<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-up"></i></button>
			</div>
		</div><!--/ Container end -->
	</section>

	</div><!-- Body inner end -->
</body>
</html>