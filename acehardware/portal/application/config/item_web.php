<?php 
$config["nameLogo"] = "ACE TV";
$config["title"] = "IBOLZ | ACE HARDWARE";
$config["warnaHeader"] = "#c51313";
$config["colorMenu"] = "#f2e8e5";
$config["warnaCopyright"] = "#c51313";
$config["warnaNavCarousel"] = "#000000";
$config["colorListbox"] = "#eb420d";
$config["warnaMenuMobile"] = "#eb420d";
$config["footerCopyright"] = "ACE HARDWARE TV";
$config["aboutUs"] = "<b>Tentang ACE<b><br/>
<p>Didirikan pada tahun 1995 sebagai anak perusahaan dari PT. Kawan Lama Sejahtera - pusat perlengkapan teknik dan 
industri #1 di Indonesia, PT. Ace Hardware Indonesia, Tbk. ialah pemegang lisensi tunggal ACE Hardware di negeri 
ini, yang ditunjuk secara langsung oleh ACE Hardware Corporation, Amerika.</p>

<p>Toko ACE Hardware Indonesia pertama dibuka di Supermal Karawaci, Tangerang, pada tahun yang sama, 
diikuti rentetan toko lain di berbagai wilayah secara cepat. Pertumbuhan pesat ini ditunjang penuh oleh berbagai 
gudang logistik di titik-titik sentral, sistem distribusi modern yang terintegrasi, beserta para staf profesional 
yang kemampuannya senantiasa ditingkatkan via berbagai training, seminar, dan sistem peningkatan keterampilan 
yang lain.</p>";
$config["detailOffice"] = "OUR OFFICE";
$config["address"] = "";
$config["tlp"] = "";
$config["fax"] = "";
$config["contactCenter"] = "";
$config["email"] = "";
$config["website"] = "";
$config["namaFolder"] = "acehardware";
$config["namaAPK"] = "ACEHARDWARETV-2.1.2-1448445120.apk";
$config["jmlSlider"] = "2";
$config["ibolz_app_id"] = "com.balepoint.ibolz.acehardware";
$config["ibolz_customer_id"] = "-2";
$config["ibolz_device_id"] = "4";
$config["ibolz_lang"] = "en_US";
$config["ibolz_version"] = "2.1.2";
$config["ibolz_mobile"] = ""; 
?>