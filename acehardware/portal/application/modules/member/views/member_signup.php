<?php

?>
<div class="full-container fixed-container">
    <?php $this->load->view('template/tpl_search');?>
</div>

<div class="full-container">
    <div id="section-member">
        <h3 class="member-title">
            <a href="#"><i class="fa fa-lock"></i>&nbsp;<span>登记册 | Register</span></a>
        </h3>
        <div class="member-subtitle">
            <p>创建帐户 | Create an acount</p>
        </div>

        <?php if(!empty($msg)){ ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $msg; ?>.
            </div>
        <?php } ?>
        <form class="form" action='<?php echo base_url(); ?>member/signup' method='post' role="form" novalidate="novalidate">
        <input type="hidden" name="submitted" value="submitted" />
        <input type="hidden" name="redirect" value="<?php //echo $redirect; ?>" />
        <div style="float:left;width:40%;margin-left:10%;">
            <div class="form-group">
                <label for="account_firstname">全名 | Full Name</label>
                <input type="text" name="fullname" class="inputtext" id="account_firstname" value="" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="account_password">密码 | Password</label>
                <input type="password" name="password" class="inputtext" id="account_password" autocomplete="off" />
            </div>

            <div class="form-group">
                <label for="account_confirm">确认密码 | Confirm Password</label>
                <input type="password" name="confirm" class="inputtext" id="account_confirm" autocomplete="off" />
            </div>
        </div>

        <div style="float:left;width:40%;margin-left:2%;">
            <div class="form-group">
                <label for="account_email">电子邮件 | Email</label>
                <input type="text" name="email" class="inputtext" id="account_email" value="" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="account_phone">电话 | Phone</label>
                <input type="text" name="phone" class="inputtext" id="account_phone" value="" autocomplete="off">
            </div>
        </div>

        <div style="clear: both;margin-top:20px;margin-left: 10%;">
            <button type="submit" class="btn btn-primary"><i class="fa fa-user"></i> Create an account</button>
        </div>

        </form>


    </div>