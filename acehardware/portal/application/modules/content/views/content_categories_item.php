<?php
if($result)
{
    foreach($result->product_list as $rwcontent)
    {
        $sub_menu_id = $rwcontent->menu_id;
        $sub_apps_id = $rwcontent->apps_id;
        $sub_product_id = $rwcontent->product_id;
        $sub_channel_category_id = $rwcontent->channel_category_id;
        $sub_channel_id = $rwcontent->channel_id;
        $sub_channel_type_id = $rwcontent->channel_type_id;
        $sub_content_id = $rwcontent->content_id;
        $sub_content_type = $rwcontent->content_type;
        $sub_title = $rwcontent->title;
        $short_title = strlen($sub_title) > 14 ? substr($sub_title, 0, 14).'...' : $sub_title;
        $sub_thumbnail = $rwcontent->thumbnail.'&w=200px';
        $sub_description = $rwcontent->description;
        $sub_prod_year = $rwcontent->prod_year;
        $sub_video_duration = $rwcontent->video_duration;
        $sub_countlike = $rwcontent->countlike;
        $sub_countviewer = $rwcontent->countviewer;
        $sub_icon_size = $rwcontent->icon_size;
        $sub_poster_size = $rwcontent->poster_size;
        $sub_crew_category_list = $rwcontent->crew_category_list;
        $sub_genre_list = $rwcontent->genre_list;

    ?>
    
    <div class="col-sm-3 col-xs-6 portfolio-static-item" style="padding-right: 0px;">
        <a href="<?php echo base_url();?>content/content_detail/<?php echo $sub_content_id; ?>/<?php echo $sub_menu_id;?>">
            <div class="grid">
                <figure class="effect-oscar" style="float: left;width: 50%;height: 100px;">
                    <img src="<?php echo $sub_thumbnail;?>&w=160" alt="" id="'<?php echo $sub_menu_id;?>'-'<?php echo $sub_product_id;?>'"
                     data-id="'<?php echo $sub_product_id;?>'" data-menu-id="'<?php echo $sub_menu_id;?>'">
                    <!--bottom image-->
                    <img style='border:0;
                        display:block;
                        margin:auto;
                        -moz-transform: scale(1,-1);
                        -o-transform: scale(1,-1);
                        -webkit-transform: scale(1,-1);
                        transform:scale(1,-1);
                        opacity:0.7;'
                        src='<?php echo $sub_thumbnail;?>' />
                </figure>
                <div class="portfolio-static-desc" style="float: left;width: 50%;">
                    <h5 style="font-size: 12px;margin: 0px;color: black;font-weight: bold;"><?php echo (strlen($short_title) > 10 ? substr($short_title, 0, 10).'...' : $short_title);?></h5>
                    <p style="font-size: 12px;margin-bottom: 0px;"><i class="fa fa-eye"></i> &nbsp;<?php echo $sub_countviewer; ?> viewer<br>
                    <?php echo (strlen($sub_description) > 45 ? substr($sub_description, 0, 45).'...' : $sub_description);?></p>
                </div>                  
            </div><!--/ grid end -->
        </a>
    </div><!--/ item 1 end -->
    <?php
    }
}
?>