<?php
/*
echo "<pre>";
print_r($vodlist->content_list);
echo "</pre>";
exit;
*/
if($vodlist->content_list)
{
?>
<div id="vod-list">
<?php
    foreach($vodlist->content_list as $row)
    {
        $apps_id = $row->apps_id;
        $channel_category_id = $row->channel_category_id;
        $channel_id = $row->channel_id;
        $channel_type_id = $row->channel_type_id;
        $content_id = $row->content_id;
        $content_type = $row->content_type;
        $title = $row->title;
        $thumbnail = $row->thumbnail.'&w=200';
        $description = $row->description;
        $prod_year = $row->prod_year;
        $video_duration = $row->video_duration;

        ?>
        <div class="item-movie" data-apps-id = "<?php echo $row->apps_id; ?>" data-channel-category-id = "<?php echo $channel_category_id; ?>"
             data-channel-id = "<?php echo $channel_id; ?>" data-channel-type-id = "<?php echo $channel_type_id;?>" data-content_id = "<?php echo $content_id;?>"
             data-content-type = "<?php echo $content_type; ?>" id="select-item-<?php echo $content_id;?>" data-img="<?php echo $thumbnail; ?>">
           <!-- a href="<?php //echo base_url().'content/contentvoddetail/'.$content_type.'/'.$channel_id.'/'.$content_id.'/'.$channel_type_id; ?>" class="link-item"
              data-toggle="tooltip" data-original-title="<?php //echo $description;?>" -->
           <a class="link-item" data-toggle="tooltip" data-original-title="<?php echo $description;?>">
               <img src="<?php echo $thumbnail; ?>"/>
               <p class="overlay-text"><?php echo $title;?></p>
           </a>
        </div>


        <script>

            $(document).ready(function() {

                $('#select-item-<?php echo $content_id;?>').click(function(e){
                    e.preventDefault();
                    var img = $(this).data('img');
                    $('.poster').removeAttr('style').attr('style', 'background: url('+img+') no-repeat scroll 0 0 rgba(0, 0, 0, 0);width:278px;height:180px;');
                    $.ajax({
                        url: Settings.base_url+'content/get_content_info',
                        type: 'POST',
                        data: {'content_type': '<?php echo $content_type;?>', 'channel_id': '<?php echo $channel_id;?>', 'content_id': '<?php $content_id;?>', 'channel_type_id': '<?php echo $channel_type_id;?>'},
                        dataType: "json",
                        success: function(data)
                        {
                            console.log(data);

                            if(data.rcode == 'ok')
                            {
                                $('#poster-info-title').val(data.title);
                                /*
                                $('#usermobile_id').val(data.mobile_id);
                                $('#usercustomer_id').val(data.customer_id);
                                $('#useremail').val(data.email);
                                $('#useruser_id').val(data.user_id);
                                $('#userfirstname').val(data.first_name);
                                $('#userlastname').val(data.last_name);
                                $('#userdisplay').val(data.display_name);
                                $('#userstatus').val(data.status);
                                $('#userverify'),val(data.verify_status);
                                $('#usermobile_no').val(data.mobile_no);
                                $('#usercredit_balance').val(data.credit_balance);
                                $('#userphoto').val(data.photo);
                                $('#userthumbnail').val(data.thumbnail);
                                */
                            }

                        }
                    });







                });


            });

        </script>

    <?php
    }
?>
</div>
<?php
} else {
?>

<div class="full-container">
    <div id="section-movie-detail" style="text-align: center !important;">
        <h3>没有可供选择 | Not available </h3>
    </div>
</div>



<?php
}
?>



