<?php
if($result)
{
    foreach($result->comment_list as $val)
    {
        $custname = !empty($val->customer_name) ? $val->customer_name.'&nbsp;|&nbsp;' : '';
        ?>
        <div class="user-comment">
            <h3><?php echo $custname;?> <span class="comment-date"><?php echo date('d, F Y', strtotime($val->created)); ?></span></h3>
            <p><?php echo $val->comment_text; ?></p>
        </div>
    <?php
    }
}
?>