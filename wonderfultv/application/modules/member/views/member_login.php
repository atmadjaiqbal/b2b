<div id="member" style="margin-top: 140px;">
    <div class="login-box">
        <form id="form-login" class="form" action='<?php echo base_url(); ?>member/login' method='post' role="form" novalidate="novalidate">
            <div class="form-elem">
               <input type="text" name="email" class="inputtext" id="login_email" value="" placeholder="Email" autocomplete="off">
               <input type="password" name="password" class="inputtext" id="account_password" placeholder="Password" autocomplete="off" />
            </div>

            <div class="button-forget">
                <a href="<?php echo base_url(); ?>member/forgotpassword" id="click-forget">FORGOT PASSWORD</a>
            </div>

            <div class="button-login">
                <a href="#" id="click-login">LOGIN</a>
            </div>


        </form>
    </div>
</div>
