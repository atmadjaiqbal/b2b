<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* --- Config file --- 

Logo header	:	logo_header.png
Slider 		:	slider1, slider2, slider3 .jpg

*/

$config['nameLogo'] = "";
$config['title'] = "IBOLZ | WONDERFUL TV";
$config['warnaHeader'] = "#8FBFEE";
$config['colorMenu'] = "#4083C3";
$config['warnaCopyright'] = "#8FBFEE";
$config['warnaNavCarousel'] = "#4083C3";
$config['colorListbox'] = "blue";
$config['warnaMenuMobile'] = "#8FBFEE";
$config['footerCopyright'] = "WONDERFUL TV";
$config['jmlSlider'] = 2;

/* --- About Us --- */

$config['aboutUs'] = "<p>-</p>";

/* --- Contact Us --- */

$config['detailOffice'] = "-";
$config['address'] = "-";
$config['tlp'] = "";
$config['fax'] = "";
$config['contactCenter'] = "";
$config['email'] = "";
$config['website'] = "";

/* --- Download --- */

$config['namaFolder'] = "wonderfultv";
$config['namaAPK'] = "WonderfulTV-2.0.4-1439956929";

?>

