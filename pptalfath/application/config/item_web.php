<?php 
$config["nameLogo"] = "AL FATH TV";
$config["title"] = "IBOLZ | AL FATH TV";
$config["warnaHeader"] = "#81632c";
$config["colorMenu"] = "#c4c4c4";
$config["warnaCopyright"] = "#81632c";
$config["warnaNavCarousel"] = "#c4c4c4";
$config["colorListbox"] = "#c4c4c4";
$config["warnaMenuMobile"] = "#c4c4c4";
$config["footerCopyright"] = "AL FATH TV";
$config["aboutUs"] = "ABOUT US";
$config["detailOffice"] = "OUR OFFICE";
$config["address"] = "";
$config["tlp"] = "";
$config["fax"] = "";
$config["contactCenter"] = "";
$config["email"] = "";
$config["website"] = "";
$config["namaFolder"] = "pptalfath";
$config["namaAPK"] = "Al-FathTV-1.1.0-1424773165.apk";
$config["jmlSlider"] = "1";
$config["ibolz_app_id"] = "com.balepoint.ibolz.pptalfath";
$config["ibolz_customer_id"] = "-2";
$config["ibolz_device_id"] = "4";
$config["ibolz_lang"] = "en_US";
$config["ibolz_version"] = "1.1.21";//"2.0.5"
$config["ibolz_mobile"] = ""; 
?>