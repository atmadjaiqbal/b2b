<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
    <title><?php echo config_item('title'); ?></title>
    <?php
    	$_metadesc = 'Ibolz';
    ?>
    <meta name="description" content="<?php echo $_metadesc; ?>"/>
    <meta name="keywords" content="Streaming, television, mobile, vod, video on demand, ibolz"/>
    <meta name="author" content="Ibolz Team" />
    <meta name="webcrawlers" content="all" />
    <meta name="rating" content="general" />
    <meta name="spiders" content="all" />
    <meta name="robots" content="index,follow">

    <meta name="google-site-verification" content="" />
    <meta charset="utf-8" />

    <!-- Open Graph Tag -->
    <meta property="og:title" content="Ibolz"/>
    <meta property="og:type" content="media"/>
    <meta property="og:url" content="http://www.ibolz.tv"/>
    <meta property="og:image" content=""/>
    <meta property="og:site_name" content="www.ibolz.tv"/>
    <meta property="og:description" content=""/>
    <!-- End Graph Tag -->

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" CONTENT="private, max-age=5400, pre-check=5400"/>
    <meta http-equiv="Expires" CONTENT="<?php echo date(DATE_RFC822,strtotime("1 day")); ?>"/>

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Favicons
	================================================== -->
	<link rel="icon" href="<?php echo base_url(); ?>assets/images/logo_header.png" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/logo_header.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/logo_header.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/logo_header.png">
	
	<!-- CSS
	================================================== -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<!-- Template styles-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<!-- Responsive styles-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
	<!-- FontAwesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Animation -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
	<!-- Prettyphoto -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/prettyPhoto.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.theme.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flexslider.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cd-hero.css">
	<!-- PopUp Gallery -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
	<!-- Style Swicther -->
	<link id="style-switch" href="<?php echo base_url(); ?>assets/css/preset3.css" media="screen" rel="stylesheet" type="text/css">
	<!-- Font -->
	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

	<!-- Style Custom -->
	<style type="text/css">
		/* Color blok header */
		.header4{
			background: <?php echo config_item('warnaHeader'); ?>;
			box-shadow: 0px 1px 7px 0 rgba(0, 0, 0, 0.2);
		}

		/* Color hover menu header */
		.dropdown-menu > ul > li>a:hover, 
		.dropdown-menu > ul > li>a:focus{
			background: none;
			color: <?php echo config_item('colorMenu'); ?> !important;
		}

		/* Color footer */
		#copyright{
			padding: 30px 0;
			background: <?php echo config_item('warnaCopyright'); ?>;
			color: #fff;
		}

		/* Color hover navigator carousel slider */
		#main-slide .carousel-control .fa-angle-right:hover, #main-slide .carousel-control .fa-angle-left:hover{
			background-color: <?php echo config_item('warnaNavCarousel'); ?> !important;
		}

		/* List box border */
		.portfolio-static-item .grid{
		    padding: 5px;
		    height: auto;
		    box-shadow: 5px 5px 10px <?php echo config_item('colorListbox'); ?>;
		}

		/* Featured Mobile */
		.navbar-toggle{
			background: <?php echo config_item('warnaMenuMobile'); ?>;
		}

		/* Featured Tab */
		.featured-tab .nav-tabs>li.active>a{
			background: <?php echo config_item('warnaTab'); ?>;
		}

		.featured-tab .nav-tabs>li.active>a:after{
			border-left-color: <?php echo config_item('warnaTab'); ?>;
		}

		.featured-tab .nav-tabs>li>a:hover{
			background: <?php echo config_item('warnaTab'); ?>;
			color: #fff;
		}
		.featured-tab .nav-tabs>li>a:hover:after{
			border-left-color: <?php echo config_item('warnaTab'); ?>;
		}

		/* Color Button */
		.btn.btn-primary.solid{
			background: <?php echo config_item('warnaTab'); ?>;
		}
		.btn.btn-primary{
			border: 2px solid <?php echo config_item('warnaTab'); ?>;
		}
	</style>

	<!-- Base URL JS -->
	<script type="text/javascript">
        var Settings = <?php
        if(str_replace("http://","",current_url()) == $_SERVER['SERVER_ADDR'])
        {
           $settings = array('base_url' => $_SERVER['SERVER_ADDR']);
        } else {
	       $settings = array('base_url' => base_url());
        }
	    echo json_encode($settings);
	    ?>;
    </script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

	<!-- Javascript Files
	================================================== -->

	<!-- initialize jQuery Library -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<!-- Bootstrap jQuery -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<!-- Style Switcher -->
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/style-switcher.js"></script>
	<!-- PopUp Gallery -->
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.magnific-popup.js"></script>
	<!-- Owl Carousel -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.js"></script>
	<!-- PrettyPhoto -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js"></script>
	<!-- Bxslider -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
	<!-- CD Hero slider -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/cd-hero.js"></script>
	<!-- Isotope -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/isotope.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ini.isotope.js"></script>
	<!-- Wow Animation -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<!-- SmoothScroll -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
	<!-- Eeasing -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
	<!-- Counter -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>
	<!-- Waypoints -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/waypoints.min.js"></script>
	<!-- Template custom -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>

	<?php if (!empty($scripts)) : ?>
        <?php foreach ($scripts as $script) : ?>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>

</head>
	
<body>

	<div class="body-inner">
	<!-- Header start -->
		<header id="header" class="navbar-fixed-top header4" role="banner">
			<div class="container">
				<div class="row">
					<!-- Logo start -->
					<div class="navbar-header">
					    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					    </button>
					    <div class="navbar-brand">
						    <a href="<?php echo base_url(); ?>">
						    	<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/logo_header.png" alt="logo">
						    	<?php if (!empty(config_item('nameLogo'))) echo "<h2>".config_item('nameLogo')."</h2>"; ?>
						    </a> 
					    </div>                   
					</div><!--/ Logo end -->
					<nav class="collapse navbar-collapse clearfix" role="navigation">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="<?php echo base_url(); ?>">Home</a></li>

							<?php foreach($headermenu->menu_list as $val) {
								if ((strpos($val->title, "Maps") !== FALSE)) {
					            	continue;
								}

								if(preg_match('/fav/i',$val->title)){
									continue;
								}
				            ?>
							<li class="dropdown">
	                       		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo strtoupper($val->title);?> <i class="fa fa-angle-down"></i></a>
	                       		<?php if(count($val->sub_menu_list) > 0) { ?>
	                       		<div class="dropdown-menu">
									<ul>
										<?php foreach($val->sub_menu_list as $submenu) { ?>
				                            <?php
				                            $head_product_id = $submenu->product_id;
				                            $head_menu_id = $submenu->menu_id;
				                            $head_menu_type = $submenu->menu_type;
				                            $head_title = $submenu->title;

				                            $_link = '';
				                            switch(strtolower($submenu->menu_type)) {
				                                case 'menu' :
				                                    if($submenu->count_product == 0)
				                                    {
				                                        $_link = base_url().'content/content_categories_list/'.$head_menu_id.'/'.urlencode($submenu->title);
				                                    } else {
				                                        $_link = base_url().'content/content_categories/'.$head_menu_id.'/'.urlencode($submenu->title);
				                                    }
													$_link = base_url().'content/content_categories_list/'.$head_menu_id.'/'.urlencode($submenu->title);

				                                    break;
				                                case 'channel' :
				                                    $_link = base_url().'content/content_channel/'.$head_menu_id.'/'.$head_product_id;
				                                    break;
				                                default:
				                                    $_link = base_url().'content/content_categories/'.$head_menu_id.'/'.urlencode($submenu->title);
				                                    break;
				                            }
					                        ?>
				                            <li><a href="<?php echo $_link; ?>"><?php echo strtoupper($submenu->title);?></a></li>
			                            <?php } ?>
			                        </ul>
		                    	</div>
		                    	<?php } ?>
		                    </li>
		                    <?php } ?>

		                    <li><a href="<?php echo base_url().'download/install';?>">Download</a></li>
	                    </ul>
					</nav><!--/ Navigation end -->
				</div><!--/ Row end -->
			</div><!--/ Container end -->
		</header>