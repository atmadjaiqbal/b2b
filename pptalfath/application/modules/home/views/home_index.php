<?php if (isset($bannerslider) && !empty($bannerslider)) echo $bannerslider; ?>
<style type="text/css">
    #homeframe {
        height: 321px;
        width: 561px;
        position: absolute;
        top: 280px;
        left: 7%;
        border: 0;
        frameborder: 0;
        allowTransparency: true;
    }    

</style>
<iframe id="homeframe" seamless wmode="Opaque" class="player bg-loader" scrolling="no"
src="http://ply001.3d.ibolztv.net/player-testing/flash/id/b2b.php?w=620&h=400&apps_id=com.balepoint.ibolz.pptalfath&version=1.1.21&type=channel&channel_id=109122156954a2281f1ad06&vid=109122156954a2281f1ad06&id=109122156954a2281f1ad06&customer_id=-2">
</iframe>




<div class="full-container">
<section id="main-container" class="portfolio-static">
	<div class="container">
		<div class="row">
			<?php 
			if($listcontent){
				$hiddenMenu = ['37837567954a37fdf55fb1'];
				foreach($listcontent as $val){
					$menu_id = $val['menu_id'];
			        $menu_title = $val['title'];
			        $menu_thumbnail = $val['thumbnail'];
			        $menu_type = $val['menu_type'];
			        $product_id = $val['product_id'];
			        if(in_array($menu_id, $hiddenMenu)){
			        	continue;
			        }
			?>
			<div class="col-md-12 heading">
				<h3 class="classic">
					<i class="fa fa-list-alt"></i>
					<div class="title"></div><?php echo $menu_title;?>
				</h3>
			</div>
			<?php 
			if($val['list_content']){
				foreach($val['list_content'] as $rwcontent){
					$default_menu_id = $rwcontent->menu_id;
					$default_product_id = $rwcontent->product_id;
					$default_thumbnail = $rwcontent->thumbnail;
					$sub_menu_id = $rwcontent->menu_id;
					$sub_menu_type = $rwcontent->menu_type;
					$sub_apps_id = $rwcontent->apps_id;
					$sub_product_id = $rwcontent->product_id;
					$sub_title = $rwcontent->title;
					$short_title = strlen($sub_title) > 14 ? substr($sub_title, 0, 14).'...' : $sub_title;
					$sub_thumbnail = $rwcontent->thumbnail;
					//$link = base_url('content/content_channel/'.$sub_menu_id.'/'.$sub_product_id);
					$info = '';
					if(preg_match('/ibolz/i',$menu_title)){
						$info = '';
					}else{
						$info = '<div class="portfolio-static-desc">';
						$info .= '<p>'.$short_title.'</p>';
						$info .= '</div>';
					}

                    switch(strtolower($sub_menu_type))
                    {
                        case 'menu' :
                            $link = base_url('content/content_categories_list/'.$sub_menu_id.'/'.urlencode($sub_title));
                            break;
                        case 'channel' :
                            $link = base_url('content/content_channel/'.$sub_menu_id.'/'.$sub_product_id);
                            break;
                        default:
                            $link = base_url('content/content_categories/'.$sub_menu_id.'/'.urlencode($sub_title));
                            break;
                    }

			?>
			<div class="col-sm-2 col-xs-6 portfolio-static-item">
				<a href="<?php echo $link;?>">
					<div class="grid">
						<figure class="effect-oscar">
							<img src="<?php echo $sub_thumbnail;?>&w=300" alt="">		
						</figure>
						<?php echo $info;?>
					</div><!--/ grid end -->
				</a>
			</div><!--/ item 1 end -->

			<?php
				}
				echo '<div class="clearfix"></div>';				
			}
			?>




			<?php
				};
			};
			?>



					

            <div class="clearfix"></div>
		            				
		</div><!-- Content row end -->
	</div><!-- Container end -->
</section>
    <?php //if (isset($contentslider) && !empty($contentslider)) echo $contentslider; ?>
</div>


<?php /*
<section id="main-container" class="portfolio-static">
	<div class="container">

		<div class="row">
			<div class="featured-tab clearfix">
				<ul class="nav nav-tabs nav-stacked col-md-3 col-sm-5">
					<?php
				    if($listmenu) {
				    	$i=1;
				        foreach($listmenu as $row) {
				            $c_menu_id = $row['menu_id'];
				            $c_category_name = $row['category_name'];
				            $active = ($i==1 ? 'class="active"' : '');
				            ?>
						  	<li <?php echo $active;?>>
						  		<a class="animated fadeIn" href="#tab_<?php echo $c_menu_id;?>" data-toggle="tab">
						  			<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/logo_header.png" alt="logo" style="width: 25px;float: left;margin-top: 12px;">
						  			<!-- <i class="fa fa-bank"></i> -->
						  			<div class="tab-info"><h6><?php echo $c_category_name;?></h6></div>
						  		</a>
						  	</li>
						  	<?php
						  	$i++;
				        }
				    }	
					?>
				</ul>
				<div class="tab-content col-md-9 col-sm-7">

					<?php
				    if($listmenu) {
				    	$x=1;
				        foreach($listmenu as $row) {
				        	$c_menu_id = $row['menu_id'];
				            $c_category_name = $row['category_name'];
				            $active = ($x==1 ? 'active ' : ''); ?>

				            <div class="tab-pane <?php echo $active;?>animated fadeInLeft" id="tab_<?php echo $c_menu_id;?>">

				            <?php
				            if($row['submenu']) {
	                            foreach($row['submenu'] as $rwContent) {

	                                $_c_product_id = $rwContent->product_id;
	                                $_c_apps_id = $rwContent->apps_id;
	                                $_c_menu_id = $rwContent->menu_id;
	                                $_c_parent_menu_id = $rwContent->parent_menu_id;
	                                $_c_menu_type = $rwContent->menu_type;
	                                $_c_title = $rwContent->title;
	                                $_c_thumbnail = $rwContent->thumbnail;
	                                $_c_link = $rwContent->link;
	                                $_c_icon_size = $rwContent->icon_size;
	                                $_c_poster_size = $rwContent->poster_size;
	                                $short_title = strlen($_c_title) > 14 ? substr($_c_title, 0, 14).'...' : $_c_title;

	                                switch(strtolower($_c_menu_type))
	                                {
	                                    case 'menu' :
	                                        $_link = base_url().'content/content_categories_list/'.$_c_menu_id.'/'.urlencode($_c_title);
	                                        break;
	                                    case 'channel' :
	                                        $_link = base_url().'content/content_channel/'.$_c_menu_id.'/'.$_c_product_id;
	                                        break;
	                                    default:
	                                        $_link = base_url().'content/content_categories/'.$_c_menu_id.'/'.urlencode($_c_title);
	                                        break;
	                                }

	                                $info = '';
									if(!preg_match('/LIVE CHANNEL/i',$c_category_name)){
										$info = '<div class="portfolio-static-desc">';
										$info .= '<h3>'.$short_title.'</h3>';
										$info .= '<p>'.$c_category_name.'</p>';
										$info .= '</div>';					
									};
	                                ?>

							        	<div class="col-sm-3 col-xs-6 portfolio-static-item">
											<a href="<?php echo $_link;?>">
												<div class="grid">
													<figure class="effect-oscar">
														<img src="<?php echo $_c_thumbnail;?>&w=300" alt="">		
													</figure>
													<?php echo $info;?>					
												</div><!--/ grid end -->
											</a>
										</div><!--/ item 1 end -->

	                                <?php
	                            }
	                        }
				            ?>
				        	</div>
				            <?php
				            $x++;
				        }
				    }
					?>
				</div><!-- tab content -->
			</div><!-- Featured tab end -->
		</div>

	</div><!-- Container end -->
</section>
*/?>